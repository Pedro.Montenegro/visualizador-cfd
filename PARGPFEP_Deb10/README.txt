Para preparar una simulación:

- Crear la geometria con GMSH y obtener un archivo .msh
- Crear la carpeta del caso y copiar el archivo .msh ahi.
- Copiar los archivos de configuración gpfep.cfg y genbcc.cfg de un caso ejemplo. 
- Dentro de estos archivos modificar los nombres y los parametros numericos para cada caso particular 
- Para continuar el caso es necesario crear los archivos .vwm .sur .bcc . Para ello correr msh2vwm (que se encuentra en la carpeta src/util) sobre el archivo .msh . Esto genera el archivo .vwm y el .sur
- Usar genbcc en la carpeta del caso para generar el archivo .bcc
- Modificar el archivo en su parte final (grupos) para colocar las condiciones de contorno. 
- Una vez colocadas las condiciones de contorno correr gpboco (se encuentra en la carpeta src/util)
- Esto ultimo genera el archivo gp.bco que contiene las condiciones de contorno. 

Commandos para corre la simulación:

- Para correr en serie:

con la terminal en la carpeta del caso (ej. cd /PARGPFEP_Deb10/src/nslst/Elbow) correr el ejecutable que se encuentra en PARGPFEP_Deb10/src/nslst/bin/gpmain_opt

- Para correr en paralelo:

con la terminal en la carpeta del caso (ej. cd /PARGPFEP_Deb10/src/nslst/Elbow).

escribir en la terminal. "mpirun -np num_process PARGPFEP_Deb10/src/nslst/bin/gpmain_opt"

Donde num_process es un entero correspondiende al numero de procesadores. Si se quiere usar numero de Threads (en caso de tener procesadores con mas de un nucleo)

"mpirun --use-hwthread-cpus -np num_threads PARGPFEP_Deb10/src/nslst/bin/gpmain_opt

c$$$      PROGRAM TSTADDVER
c$$$      CHARACTER*80 ADDVER
c$$$      CHARACTER*80 FILORIG, FILVER
c$$$      FILORIG = 're_____.dat'
c$$$      do i = 1, 103
c$$$         FILVER = ADDVER(FILORIG)
c$$$         OPEN(10, FILE=FILVER, STATUS='NEW')
c$$$         write (10,*) '-',FILORIG,'-'
c$$$         write (10,*) '-',FILVER,'-'
c$$$         CLOSE(10)
c$$$      end do
c$$$      stop
c$$$      END
C
C     Addver: adds "Version" substring to a file name, such that the
C     new file doesn't exist.
C     Old behaviour: adds AA, AB, AC, ... before the extension (first dot)
C     New behaviour: replace "_" in original file name by digits, if no
C     "_" were found, change to old behaviour.
C
      CHARACTER*80 FUNCTION ADDVER(NOMBRE)
      CHARACTER*80 NOMBRE
      CHARACTER*80 FILNAM
      CHARACTER*4  EXT
C     Count number of "_" in string
      ndigits = 0
      L = 0
      DO I = 1,80
         IF (NOMBRE(I:I).EQ.'_') ndigits = ndigits + 1
         IF (NOMBRE(I:I).EQ.' ' .AND. L.EQ.0) L=I-1
      END DO
C
      IF (ndigits .eq. 0) THEN
C
         IC1 = 65               ! 'A'
         IC2 = 65               ! 'A'
         DO I = 1,80
            IF (NOMBRE(I:I).EQ.'.') GOTO 5
            IF (NOMBRE(I:I).EQ.' ' .AND. I.NE.1) GOTO 4
         END DO
 4       EXT = '.dat'
         GOTO 6
 5       EXT = NOMBRE(I:I+3)
 6       L = I-1
C      IF (L.GT.6) L = 6
 10      FILNAM = NOMBRE(1:L)//CHAR(IC2)//CHAR(IC1)//EXT
         OPEN (10, FILE=FILNAM, STATUS='OLD', ERR=100) ! Si da error->no existe
         CLOSE (10)
         IC1 = IC1 + 1
         IF (IC1.GT.90) THEN    ! 90: 'Z'
            IC1 = 65
            IC2 = IC2+1
            IF (IC2.GT.90) GO TO 15
         END IF
         GO TO 10
 15      CONTINUE
C
C     No more file names available ... and now?
C
C
      ELSE
C     Some "_" in the file: replace by digits
         FILNAM = NOMBRE(1:L)
         NTOT = 10**ndigits
         indfile = 0
         DO WHILE (indfile .LT. NTOT)
            DO idig = 1, ndigits
               ldigit = indfile / 10**(ndigits-idig)
               ldigit = mod(ldigit,10)
C     Search idig occurrence of "_" in file and replace by ldigit
               J = 0
               DO I = 1,80
                  IF (NOMBRE(I:I).EQ.'_') THEN
                     J = J+1
                     IF (J .EQ. idig) EXIT
                  END IF
               END DO
               FILNAM(I:I) = CHAR(48+ldigit)
            END DO
            OPEN (10,FILE=FILNAM,STATUS='OLD',ERR=100) ! Si da error->no existe
            CLOSE (10)
            indfile = indfile + 1
         END DO
C
C     No more file names available ... and now?
C
C
      END IF
C
 100  ADDVER = FILNAM
      RETURN
      END

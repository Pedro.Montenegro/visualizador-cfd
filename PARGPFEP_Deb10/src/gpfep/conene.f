      SUBROUTINE CONENE (NEL,IE,JE,IET,JET,IP,IENE,JENE,MEJENE)
      DIMENSION IE(1),JE(1),IET(1),JET(1),IP(1),IENE(1),JENE(1)
      IENE(1) = 1
      DO I=1,NEL
         IP(I) = 0
      END DO
      DO IEL=1,NEL
         IENE(IEL+1) = IENE(IEL)
         IEA = IE(IEL)
         IEB = IE(IEL+1)-1
         DO IJE = IEA,IEB
            INO = JE(IJE)
            IETA = IET(INO)
            IETB = IET(INO+1) - 1
            DO IJET = IETA,IETB
               IEL2 = JET(IJET)
               IF (IEL2.NE.IEL) THEN
                  IF (IP(IEL2).NE.IEL) THEN
                     IJENE = IENE(IEL+1)
                     IF (IJENE.GT.MEJENE) STOP 'CONENE. INSUFF. MEMORY'
                     JENE(IJENE) = IEL2
                     IENE(IEL+1) = IJENE + 1
                     IP(IEL2) = IEL
                  END IF
               END IF
            END DO
         END DO
      END DO
      MEJENE = IENE(NEL+1) - 1
      RETURN
      END

c-----------------------------------------------------------------------
      subroutine dmultsp (ia, ja, ad, an, u, v, n)
c-----------------------------------------------------------------------
c     multiplica una matriz rala simetrica por un vector lleno
c
c     datos de entrada:
c     ia, ja  estructura de la matriz rala
c     ad      elementos diagonales de la matriz rala
c     an      elementos extradiagonales de la matriz rala
c     u       vector factor
c     n       orden de la matriz
c
c     resultados:
c     v  vector producto (v = a * u)
c
c     programador : dari enzo alberto
c
c     fecha       : junio 1988
c
c-----------------------------------------------------------------------
c
      implicit real*8 (a-h,o-z)
c
      dimension ia (1), ja (1), an (1), ad (1), u (1), v (1)
      do i = 1 , n
         v (i) = ad (i) * u (i)
      end do
      do i = 1, n
         iaa = ia (i)
         iab = ia (i + 1) - 1
         if (iab .ge. iaa) then
            do jp = iaa, iab
               j = ja (jp)
               z = an (jp)
               v (i) = v (i) + z * u (j)
               v (j) = v (j) + z * u (i)
            end do
         end if
      end do
      return
      end

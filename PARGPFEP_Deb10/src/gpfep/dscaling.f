c-----------------------------------------------------------------------
      subroutine dscaling (ia, ja, adsc, an, adns, n, rhs, irm)
c-----------------------------------------------------------------------
c n:              orden de la matriz
c ia, ja:         estructura de la matriz en formato ralo
c adsc, adns, an: elementos de la matriz en formato ralo
c rhs:            vector termino independiente
c irm:            orden del vector termino independiente
c
      implicit real*8 (a-h,o-z)
c
      dimension ia(*), ja(*), an(*), adsc(*), adns(*), rhs(*)
c
c Diagonal scaling
c
      do i = 1 , n
         adns(i) = adsc(i)
         adsc(i) = 1.0d+00 
      end do
c
c Extradiagonal scaling
c
      do i = 1, n
         iaa = ia(i)
         iab = ia(i + 1) - 1
         if (iab .ge. iaa) then
            do jp = iaa, iab
               j      = ja(jp)
               an(jp) = an(jp)/dsqrt(adns(i)*adns(j))
            end do
         end if
      end do
c
c Right hand side scaling
c
      do i = 0, irm-1
         do j = 1, n
            index = i*n + j
            rhs(index) = rhs(index)/dsqrt(adns(j))
         end do
      end do
c
      return
      end

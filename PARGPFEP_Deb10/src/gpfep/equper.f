      SUBROUTINE EQUPER (NFIELD,NOD,KCOMP,IPER,IINV)
      DIMENSION KCOMP(1),IPER(1),IINV(1)
      DIMENSION INDXXX(50)
      INDXXX(1) = 0
      NSCAF     = 0
      DO IFIELD=1,NFIELD
         indxxx(ifield+1) = indxxx(ifield)
         DO ICOMP=1,KCOMP(IFIELD)
            INDXXX(IFIELD+1) = INDXXX(IFIELD+1)+NOD
            NSCAF = NSCAF + 1
         END DO
      END DO
      KAUX = 1
      DO IFIELD=1,NFIELD
         DO ICOMP=1,KCOMP(IFIELD)
            DO INO=1,NOD
               IPER((INO-1)*KCOMP(IFIELD)+ICOMP+INDXXX(IFIELD))=
     *           NSCAF*(INO-1)+KAUX
               IINV(NSCAF*(INO-1)+KAUX)=
     *           (INO-1)*KCOMP(IFIELD)+ICOMP+INDXXX(IFIELD)
            END DO
            KAUX = KAUX + 1
         END DO
      END DO
      RETURN 
      END

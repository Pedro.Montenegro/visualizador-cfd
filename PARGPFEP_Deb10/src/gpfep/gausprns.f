C-----------------------------------------------------------------------
      SUBROUTINE GAUSPRNS (NOD,IU,JU,UNS,UNI,DI,BK)
C-----------------------------------------------------------------------
C
C     REDUCE EL VECTOR DE CARGA Y EFECTUA LA RETROSUSTITUCION
C     (MATRIZ ALMACENADA EN FORMATO RALO, FACTORIZADA POR NUMFAKNS)
C
C     DATOS DE ENTRADA:
C     NOD     NUMERO DE INCOGNITAS
C     IU, JU  ESTRUCTURA DE LA MATRIZ TRIANGULARIZADA
C     UNS,UNI ELEMENTOS EXTRADIAGONALES DE LA MATRIZ TRIANGULARIZADA
C     DI      ELEMENTOS DIAGONALES DE LA MATRIZ TRIANGULARIZADA
C     BK      VECTOR TERMINO INDEPENDIENTE
C
C     RESULTADOS:
C     BK  SOLUCION
C
C     Programador : DARI Enzo Alberto
C
C     Fecha       : Octubre 1991
C
C-----------------------------------------------------------------------
C
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION IU(1), JU(1), UNS(1), UNI(1), DI(1), BK(1)
C
      NM = NOD - 1
C
C     Reduccion del vector de carga
C
      DO 20 K = 1, NM
         IUA = IU (K)
         IUB = IU (K + 1) - 1
         ZZ = BK (K)
         IF (IUB .LT. IUA) GO TO 10
         DO 30 I = IUA, IUB
            BK (JU (I)) = BK (JU (I)) - UNI (I) * ZZ
   30    CONTINUE
   10    BK (K) = ZZ * DI (K)
   20 CONTINUE
      BK (NOD) = BK (NOD) * DI (NOD)
C
C     Retrosustitucion
C
      K = NM
   40 IUA = IU (K)
      IUB = IU (K + 1) - 1
      IF (IUB .LT. IUA) GO TO 60
      ZZ = BK (K)
      DO 50 I = IUA, IUB
         ZZ = ZZ - UNS (I) * BK (JU (I))
   50 CONTINUE
      BK (K) = ZZ
   60 K = K - 1
      IF (K .GT. 0) GO TO 40
      RETURN
      END

C----------------------------------------------------------------------      
      SUBROUTINE GPSASS (NUNK,NEL,NFIELD,NODTOT,KFTYP,KCOMP,
     *             INDIE,INDJE,INDIET,INDJET,INDXXX,
     *             IEGLO,JEGLO,IETGL,JETGL,LDIR,
     *             IPER,IINV,IP,IAAA,JAAA,MEDJA)
C----------------------------------------------------------------------      
C PERFORMS THE SYMBOLIC ASSEMBLY OF A SPARSE MATRIX OF NUNKxNUNK
C COMPONENTS. 
      DIMENSION NODTOT(1),KFTYP(1),KCOMP(1),INDIE(1),INDJE(1)
      DIMENSION INDIET(1),INDJET(1)
      DIMENSION INDXXX(1),IEGLO(1),JEGLO(1),IETGL(1),JETGL(1)
      DIMENSION LDIR(1),IPER(1),IP(1),IINV(1),IAAA(1),JAAA(1)
C
      DO I=1,NUNK
         IP(I) = LDIR(I)*NUNK
      END DO
      IAAA(1) = 1
C LOOP OVER ROWS
      DO IFIL = 1,NUNK-1
         IAAA(IFIL+1) = IAAA(IFIL)
         IF (LDIR(IFIL).EQ.0) THEN
            IORIG = IINV(IFIL)
C IDENTIFIES FIELD OF UNKNOWN
            IFIELD = -10
            DO IAUX=1,NFIELD+1
               IF(IORIG.LE.INDXXX(IAUX).AND.IFIELD.EQ.-10)IFIELD=IAUX-1
            END DO
            IF (IFIELD.EQ.-10) THEN
               WRITE (6,*) ' GPSASS. IFIELD.EQ.-10'
               WRITE (6,*) ' IFIL       = ',IFIL
               WRITE (6,*) ' IINV(IFIL) = ',IORIG
               STOP
            END IF
C IDENTIFIES NODE OF THE UNKNOWN
            INOD = (IORIG-INDXXX(IFIELD)-1)/KCOMP(IFIELD) + 1
C            write (6,*) ' ----------------------------------- '
C            write (6,*) ' fila ',ifil,' ord ',iorig
C            write (6,*) ' campo ',ifield,' nodo ',inod
C LOOP OVER ELEMENTS ASSOCIATED TO NODE INOD
            IJETA = IETGL(INOD+INDIET(KFTYP(IFIELD)))
            IJETB = IETGL(INOD+1+INDIET(KFTYP(IFIELD)))-1
C            write (6,*) ' elementos asociados: ',ijetb-ijeta+1
            DO IIEL = IJETA,IJETB
               IEL = JETGL(IIEL+INDJET(KFTYP(IFIELD)))
C               write (6,*) iel
C LOOP OVER FIELDS
               DO KFIELD=1,NFIELD
                  IMESH = KFTYP(KFIELD)
C LOOP OVER NODES OF THE ASSOCIATED MESH
                  IJEA = IEGLO(IEL+INDIE(IMESH))
                  IJEB = IEGLO(IEL+1+INDIE(IMESH))-1
                  DO IINO = IJEA, IJEB
                     INNO = JEGLO(IINO+INDJE(IMESH))
C IDENTIFICATION OF THE UNKNOWN NUMBER, LOOP OVER COMPONENTS
                     DO ICOMP=1,KCOMP(KFIELD)
                        INUNK = INDXXX(KFIELD) + 
     *                          (INNO-1)*KCOMP(KFIELD) + ICOMP
                        INUNK = IPER(INUNK)
                        IF (INUNK.GT.IFIL.AND.IP(INUNK).LT.IFIL) THEN
                           JAAA(IAAA(IFIL+1)) = INUNK
                           IAAA(IFIL+1) = IAAA(IFIL+1)+1
                           IP(INUNK) = IFIL
                        END IF !UPPER PART AND NOT ASSEMBLED
                     END DO !COMPONENTS
                  END DO !NODES
               END DO !FIELDS
            END DO !ELEMENTS ASSOCIATED TO NODE INOD
C 
         END IF ! LDIR.EQ.0
      END DO ! ROWS
      IAAA(NUNK+1) = IAAA(NUNK)
      MEDJA = IAAA(NUNK+1)-1
      END

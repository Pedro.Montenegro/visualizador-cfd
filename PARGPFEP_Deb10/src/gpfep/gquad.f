c -----------------------------------------------------------------------------
      subroutine gquad (nint,x,w)
c -----------------------------------------------------------------------------
c Set coordinates and weights of gauss points.
c
      implicit real*8 (a-h,o-z)
c
      dimension x(*), w(*)
c
      if (nint.eq.1) then
c
         x(1) = 0.d0
         w(1) = 2.d0
c
      else if (nint.eq.2) then
c
         x(1) = -0.5773502691896257d0
         x(2) = -x(1)
         w(1) = 1.d0
         w(2) = w(1)
c
      else if (nint.eq.3) then
c
         x(1) = -0.774596669241483d0
         x(2) = 0.d0
         x(3) = -x(1)
         w(1) = 0.5555555555555556d0
         w(2) = 0.8888888888888889d0
         w(3) = w(1)
c
      else if (nint.eq.4) then
c
         x(1) = -0.861136311594953d0
         x(2) = -0.339981043584856d0
         x(3) = -x(2)
         x(4) = -x(1)
         w(1) = 0.347854845137454d0
         w(2) = 0.652145154862546d0
         w(3) = w(2)
         w(4) = w(1)
c
      else
         write(6,360) nint
         stop
      end if
      return
c
 360  format (' Error in GQUAD. ',i5)
      end

c-----------------------------------------------------------------------
      subroutine grcspr (x, bk, ia,ja, ad,an, r, w, aw, ninc, tol, nmax)
c-----------------------------------------------------------------------
c     resuelve un sistema lineal de ecuaciones,
c     con matriz simetrica semidefinida positiva
c     por metodo de gradientes conjugados
c     (almacenamiento ralo)
c     Basada en Lecture Notes in Mathematics: Sparse Matrix Techniques Edited by A.Dold & 
c     B.Eckmann (Copenhagen 1976) Springer-Verlag. para nu = 2
c
c     datos de entrada:
c     ia, ja  estructura de la matriz del sistema
c     ad      elementos diagonales de la matriz del sistema
c     an      elementos extradiagonales de la matriz del sistema
c     bk      vector termino independiente
c     ninc    numero de incognitas
c     tol     tolerancia (seminormalizada)
c     nmax    numero maximo de iteraciones
c
c     resultados:
c     x  vector solucion
c
c     espacio de trabajo:
c     w  direccion de minimizacion (ninc)
c     r  vector residuo (ninc)
c     aw vector auxiliar (a.w)(ninc)
c
c     programador : dari enzo alberto
c
c     fecha       : junio 1988
c
c-----------------------------------------------------------------------
c
      implicit real*8 (a-h,o-z)
      real*8 nr2, nrprev
c
      dimension ia (*), ja (*), ad (*), an (*), bk (*), x (*),
     *          r (*), w (*), aw (*)
c
      n = 0
c
c     calcula residuo inicial
c                   (en el primer paso = a la direccion de minimizacion)
      call dmultsp (ia, ja, ad, an, x, w, ninc)
      do i = 1, ninc
        r (i) = w (i) - bk (i)
        w (i) = r (i)
      end do
c
c     nr2: norma cuadratica del residuo inicial
c
      nr2 = 0.0d+00
      do i = 1, ninc
        nr2 = nr2 + r (i) * r (i)
      end do
c
c     comienza lazo
c
      write(*,*) '    iter          delmax          ',
     *           'err               norma'
    1 continue
c
c     calcula descenso
c
      call dmultsp (ia, ja, ad, an, w, aw, ninc)
      prodes = 0.0d+00
      do i = 1, ninc
        prodes = prodes + w (i) * aw (i)
      end do
      ron = nr2 / prodes
c
c     calcula nuevo valor aproximado (minimo en la direccion w)
c
      delmax = 0.0d+00
      xmax   = 0.0d+00
      do i = 1, ninc
        delta = ron * w (i)
        if (dabs(delta) .gt. delmax) delmax = dabs(delta)
        x (i) = x (i) - delta
        if (dabs (x(i)) .gt. xmax) xmax = dabs (x(i))
      end do
c      err = delmax / xmax
c
c     si la correccion de la solucion es menor que la tolerancia, termina
c
      write(6,*) 'IT ',n,' DEL ',delmax,' RES ', nr2
      if (delmax .lt. tol) go to 3
      if (n .gt. nmax) go to 2
c
c     calcula nuevo residuo
c
      do i = 1, ninc
        r (i) = r (i) - ron * aw (i)
      end do
c
c     norma del nuevo residuo
c
      nrprev = nr2
      nr2 = 0.0d+00
      do i = 1, ninc
        nr2 = nr2 + r (i) * r (i)
      end do
c
c     calcula nueva direccion de minimizacion
c
      gan = nr2 / nrprev
      do i = 1, ninc
        w (i) = r (i) + gan * w (i)
      end do
c
c     otro paso
c
      n = n + 1
      go to 1
c
    2 write (*,*)'  grcspr. not enough iterations ',nmax ,delmax
c      stop
    3 continue
      return
      end

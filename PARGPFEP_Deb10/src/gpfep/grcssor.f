c-----------------------------------------------------------------------
      subroutine grcssor (x, g, ia,ja, ad,an, d, h, e, ninc, tol, nmax)
c-----------------------------------------------------------------------
c     resuelve un sistema lineal de ecuaciones,
c     con matriz simetrica semidefinida positiva
c     por metodo de gradientes conjugados
c     (almacenamiento ralo)
c     precondicionador SSOR
c     Referencia: Axelsson, O. and Barker, V.A., Finite Element Solution
c     of Boundary Value Problems, Academic Press, Orlando.
c
c     datos de entrada:
c     ia, ja  estructura de la matriz del sistema
c     ad      elementos diagonales de la matriz del sistema
c     an      elementos extradiagonales de la matriz del sistema
c     g       vector termino independiente
c     ninc    numero de incognitas
c     tol     tolerancia (absoluta)
c     nmax    numero maximo de iteraciones
c
c     resultados:
c     x vector solucion
c
c     espacio de trabajo:
c     d direccion de minimizacion (ninc)
c     h ? (ninc)
c     e ? (ninc)
c
c     programador : dari enzo alberto
c
c     fecha       : octubre 1995
c
c-----------------------------------------------------------------------
c
      implicit real*8 (a-h,o-z)
c
      dimension ia (*), ja (*), ad (*), an (*), g (*), x (*),
     *          h (*), d (*), e (*)
c
      n = 0
      omega = 1.2D0
      uso = 1.0 / omega
      um2so = 1.0 - 2.0 / omega
c
c	x := x0, g := b
c
c	    -1                                             -1
c	g:=F  (Hx - g) ( h := Hx  -  g := h - g  --  g := F  g
c
      call dmultsp (ia, ja, ad, an, x, h, ninc)
      do i = 1, ninc
        g (i) = h (i) - g (i)
      end do
      call dfrwdsbs (ia, ja, ad, an, omega, g, ninc)
c             T
c	x := F x
      call dmultsup (ia, ja, ad, an, omega, x, ninc)
c	h := G g
      do i = 1, ninc
        h (i) = uso * ad (i) * g (i)
      end do
c	d := -h
      do i = 1, ninc
        d (i) = - h (i)
      end do
c                 T
c	delta0 = g  h
      delta0 = 0.0
      do i = 1, ninc
        delta0 = delta0 + g (i) * h (i)
      end do
c
      write(*,*) 'IT ',n,' RES ', delta0
c
      if (delta0 .lt. tol) go to 3
c
    1 continue
      n = n + 1
c             -T                       -T
c	e := F   d ( e := d  --  e := F   e )
      do i = 1, ninc
        e (i) = d (i)
      end do
      call dbkwdsbs (ia, ja, ad, an, omega, e, ninc)
c	h := Ee + d
      do i = 1, ninc
        h (i) = um2so * ad (i) * e (i) + d (i)
      end do
c             -1
c	h := F   h
      call dfrwdsbs (ia, ja, ad, an, omega, h, ninc)
c	h := h + e
      do i = 1, ninc
        h (i) = h (i) + e (i)
      end do
c                       T
c	tau = delta0 / d  h
      prodesc = 0.0d0
      do i = 1, ninc
        prodesc = prodesc + d (i) * h (i)
      end do
      tau = delta0 / prodesc
c	x := x + tau d
c	g := g + tau h
      do i = 1, ninc
        x (i) = x (i) + tau * d (i)
        g (i) = g (i) + tau * h (i)
      end do
c	h := G g
      do i = 1, ninc
        h (i) = uso * ad (i) * g (i)
      end do
c                  T
c	delta1 := g  h
      delta1 = 0
      do i = 1, ninc
        delta1 = delta1 + g (i) * h (i)
      end do
c
      write(*,*) 'IT ',n,' RES ', delta1
      if (delta1 .lt. tol) go to 3
      if (n .gt. nmax) go to 2
c
      beta = delta1 / delta0
      delta0 = delta1
c	d := -h + beta d
      do i = 1, ninc
        d (i) = - h (i) + beta * d (i)
      end do
c
      go to 1
c
    2 write (*,*)'  grcspr. not enough iterations ',nmax ,delta0
c      stop
    3 continue
c	      -T
c	x := F   x
      call dbkwdsbs (ia, ja, ad, an, omega, x, ninc)
      return
      end
c
      subroutine dmultsup (ia, ja, ad, an, omega, x, ninc)
      implicit real*8 (a-h,o-z)
      dimension ia (*), ja (*), ad (*), an (*), x (*)
c
      j = 1
      uso = 1.0D+00 / omega
      do i = 1, ninc
         acc = ad (i) * uso * x(i)
         do while (j .lt. ia(i+1))
            acc = acc + an(j) * x(ja(j))
            j = j + 1
         end do
         x(i) = acc
      end do
c
      return
      end
c
      subroutine dfrwdsbs (ia, ja, ad, an, omega, x, ninc)
      implicit real*8 (a-h,o-z)
      dimension ia (*), ja (*), ad (*), an (*), x (*)
c
      j = 1
      do i = 1, ninc
         x(i) = x(i) * omega / ad (i)
         do while (j .lt. ia(i+1))
            x(ja(j)) = x(ja(j)) - an(j) * x(i)
            j = j + 1
         end do
      end do
c
      return
      end
c
      subroutine dbkwdsbs (ia, ja, ad, an, omega, x, ninc)
      implicit real*8 (a-h,o-z)
      dimension ia (*), ja (*), ad (*), an (*), x (*)
c
      j = ia(ninc+1) - 1
      do ii = 1, ninc
         i = ninc + 1 - ii
         do while (j .ge. ia(i))
            x(i) = x(i) - an(j) * x(ja(j))
            j = j - 1
         end do
         x(i) = x(i) * omega / ad (i)
      end do
c
      return
      end

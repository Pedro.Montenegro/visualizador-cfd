c -----------------------------------------------------------------------------
      subroutine isopar (nvar,kint,ipgau,nsd,nodgeo,intgeo,iel,
     *                   detj,dpl,dp,cogeo,t,ti,nod,maxnpe,maxgau)
c -----------------------------------------------------------------------------
c calcula las derivadas globales de las funciones de forma y el determinante
c de la matrix jacobiana en el punto de integracion "ipgau".
c
      implicit real*8 (a-h,o-z)
c
      dimension dpl(maxnpe,maxgau,3,*), dp(maxnpe,3,*), cogeo(maxnpe,*),
     *          kint(*), nod(*)
      dimension t(3,3), ti(3,3)
c
c jacobian matrix
c
      do i=1,nsd
         do j=1,nsd
            t(i,j) = 0.d0
            do jl=1,nodgeo
               t(i,j) = t(i,j) + dpl(jl,ipgau,i,intgeo)*cogeo(jl,j)
            end do
         end do
      end do
c      
c determinant and inverse of jacobian matrix
c
      call jacinv (t,nsd,ti,detj)
c
      if (detj.le.0.0d0) then
         write(*,*)
     *        ' isopar. negative jacobian (element, value, ipgau):',
     *        iel,detj,ipgau
         write (*,*) 'nsd, nodgeo:', nsd, nodgeo
         write (*,*) 'Jacobian Matrix:'
         do i = 1, nsd
            do j = 1, nsd
               t(i,j) = 0.d0
               do jl = 1, nodgeo
                  t(i,j) = t(i,j) + dpl(jl,ipgau,i,intgeo)*cogeo(jl,j)
                  write (*,*)'i,j,dpl,cogeo,t(i,j)',
     *                 i,j,dpl(jl,ipgau,i,intgeo),cogeo(jl,j),t(i,j)
               end do
            end do
         end do
         write (*,*) 't, ti, detj:', t, ti, detj
         stop
      end if
c
c global first derivatives
c
      kxx = 0
      do ivar = 1,nvar
         itype = kint(ivar)
         if (itype .ne. kxx) then
            kxx = itype
            do i = 1, nod(ivar)
               do j = 1, nsd
                  dp(i,j,itype) = 0.d0
                  do k = 1, nsd
                     dp(i,j,itype) = dp(i,j,itype) +
     #                    dpl(i,ipgau,k,itype)*ti(j,k)
                  end do
               end do
            end do
         end if
      end do
c
      return
      end

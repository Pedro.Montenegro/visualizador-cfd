c -----------------------------------------------------------------------------
      subroutine isopar (nvar,kint,ipgau,nsd,nodgeo,intgeo,iel,
     *                   detj,dpl,dp,cogeo,t,ti,nod,maxnpe,maxgau)
c -----------------------------------------------------------------------------
c calcula las derivadas globales de las funciones de forma y el determinante
c de la matrix jacobiana en el punto de integracion "ipgau".
c
      implicit real*8 (a-h,o-z)
c
      dimension dpl(maxnpe,maxgau,3,*), dp(maxnpe,3,*), cogeo(maxnpe,*),
     *          kint(*), nod(*)
      dimension t(3,3), ti(3,3)
      integer lastnegel
      save lastnegel
      data lastnegel /-1/
c
c     jacobian matrix
c
      do i=1,nsd
         do j=1,nsd
            t(i,j) = 0.d0
            do jl=1,nodgeo
               t(i,j) = t(i,j) + dpl(jl,ipgau,i,intgeo)*cogeo(jl,j)
            end do
         end do
      end do
c      
c     determinant
c
      if (nsd.eq.1) then
         detj = t(1,1)
      else if (nsd.eq.2) then
         detj = t(1,1)*t(2,2) - t(2,1)*t(1,2)
      else
         d11 = t(2,2)*t(3,3)-t(3,2)*t(2,3)
         d12 = -(t(2,1)*t(3,3)-t(3,1)*t(2,3))
         d13 = t(2,1)*t(3,2)-t(3,1)*t(2,2)
         detj = t(1,1)*d11 + t(1,2)*d12 + t(1,3)*d13
      end if
c
c     Check if negative
c
      if (detj .le. 0.0d0) then
         if (iel .ne. lastnegel) then
            lastnegel = iel
            write(*,*)
     *        ' isopar. non-positive jacobian (iel, value, ipgau):',
     *        iel,detj,ipgau
c            write (*,*) 'nsd, nodgeo:', nsd, nodgeo
            write (*,*) 'Jacobian Matrix:'
            do i = 1, nsd
               write(*,*) i,':',(t(i,j), j=1,nsd)
            end do
            write (*,*) 'Nodal coordinates:'
            do jl = 1, nodgeo
               write(*,*) jl,':',(cogeo(jl,j), j=1,nsd)
            end do
         end if
! Unfold if simplex, else stop
         if (nodgeo .ne. nsd+1) stop 'Fatal error in isopar'
         if (nsd.eq.1) then
            cogeo(2,1) = cogeo(1,1) + cogeo(1,1) - cogeo(2,1)
         else if (nsd.eq.2) then
            x12 = cogeo(2,1) - cogeo(1,1)
            x23 = cogeo(3,1) - cogeo(2,1)
            x31 = cogeo(1,1) - cogeo(3,1)
            y12 = cogeo(2,2) - cogeo(1,2)
            y23 = cogeo(3,2) - cogeo(2,2)
            y31 = cogeo(1,2) - cogeo(3,2)
            a12 = sqrt(x12*x12 + y12*y12)
            a23 = sqrt(x23*x23 + y23*y23)
            a31 = sqrt(x31*x31 + y31*y31)
            if (a12 .ge. a23 .and. a12 .ge. a31) then ! edge 12 is the largest
               h = -detj / a12 ! distance from node 3 to edge a12
               if (h .lt. 0.0001 * a12) then
                  disp = 0.0001 * a12 ! Displacement: minimal positive area
               else
                  disp = 2*h ! Displacement: reflexion
               end if
               dirx = -y12 / a12 ! Direction of displacement:
               diry =  x12 / a12 ! normal to edge
               cogeo(3,1) = cogeo(3,1) + disp * dirx
               cogeo(3,2) = cogeo(3,2) + disp * diry
            else if (a23 .ge. a31) then ! edge 23 is the largest
               h = -detj / a23 ! distance from node 1 to edge a23
               if (h .lt. 0.0001 * a23) then
                  disp = 0.0001 * a23 ! Displacement: minimal positive area
               else
                  disp = 2*h ! Displacement: reflection
               end if
               dirx = -y23 / a23 ! Direction of displacement:
               diry =  x23 / a23 ! normal to edge
               cogeo(1,1) = cogeo(1,1) + disp * dirx
               cogeo(1,2) = cogeo(1,2) + disp * diry
            else ! edge 31 is the largest
               h = -detj / a31 ! distance from node 2 to edge a31
               if (h .lt. 0.0001 * a31) then
                  disp = 0.0001 * a31 ! Displacement: minimal positive area
               else
                  disp = 2*h ! Displacement: reflection
               end if
               dirx = -y31 / a31 ! Direction of displacement:
               diry =  x31 / a31 ! normal to edge
               cogeo(2,1) = cogeo(2,1) + disp * dirx
               cogeo(2,2) = cogeo(2,2) + disp * diry
            end if
         else ! 3D: Pending....
            stop 'Unfolding of 3D simplices not programmed yet'
         end if
c     Re-compute Jacobian matrix
         do i=1,nsd
            do j=1,nsd
               t(i,j) = 0.d0
               do jl=1,nodgeo
                  t(i,j) = t(i,j) + dpl(jl,ipgau,i,intgeo)*cogeo(jl,j)
               end do
            end do
         end do
!
         call jacinv (t,nsd,ti,detj)
c     Mark element
         detj = -detj
!         stop
      else
c
c     Inverse Jacobian matrix
c
         if (nsd.eq.1) then
            ti(1,1) = 1.0d0 / t(1,1)
         else if (nsd.eq.2) then
            ti(1,1) =  t(2,2)/detj
            ti(2,1) = -t(2,1)/detj
            ti(1,2) = -t(1,2)/detj
            ti(2,2) =  t(1,1)/detj
         else
            ti(1,1) =  (t(2,2)*t(3,3)-t(3,2)*t(2,3))/detj
            ti(2,1) = -(t(2,1)*t(3,3)-t(3,1)*t(2,3))/detj
            ti(3,1) =  (t(2,1)*t(3,2)-t(3,1)*t(2,2))/detj
            ti(1,2) = -(t(1,2)*t(3,3)-t(3,2)*t(1,3))/detj
            ti(2,2) =  (t(1,1)*t(3,3)-t(3,1)*t(1,3))/detj
            ti(3,2) = -(t(1,1)*t(3,2)-t(3,1)*t(1,2))/detj
            ti(1,3) =  (t(1,2)*t(2,3)-t(2,2)*t(1,3))/detj
            ti(2,3) = -(t(1,1)*t(2,3)-t(2,1)*t(1,3))/detj
            ti(3,3) =  (t(1,1)*t(2,2)-t(2,1)*t(1,2))/detj
         end if
      end if
c
c global first derivatives
c
      kxx = 0
      do ivar = 1,nvar
         itype = kint(ivar)
         if (itype .ne. kxx) then
            kxx = itype
            do i = 1, nod(ivar)
               do j = 1, nsd
                  dp(i,j,itype) = 0.d0
                  do k = 1, nsd
                     dp(i,j,itype) = dp(i,j,itype) +
     &                    dpl(i,ipgau,k,itype)*ti(j,k)
                  end do
               end do
            end do
         end if
      end do
c
      return
      end

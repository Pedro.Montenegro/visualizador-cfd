c -----------------------------------------------------------------------------
      subroutine isopar (nvar,kint,ipgau,nsd,nodgeo,intgeo,iel,
     *                   detj,dpl,dp,cogeo,t,ti,nod,maxnpe,maxgau)
c -----------------------------------------------------------------------------
c calcula las derivadas globales de las funciones de forma y el determinante
c de la matrix jacobiana en el punto de integracion "ipgau".
c
      implicit real*8 (a-h,o-z)
c
      dimension dpl(maxnpe,maxgau,3,*), dp(maxnpe,3,*), cogeo(maxnpe,*),
     *          kint(*), nod(*)
      dimension t(3,3), ti(3,3)
      integer lastnegel
      save lastnegel
      data lastnegel /-1/
c
c     jacobian matrix
c
      do i=1,nsd
         do j=1,nsd
            t(i,j) = 0.d0
            do jl=1,nodgeo
               t(i,j) = t(i,j) + dpl(jl,ipgau,i,intgeo)*cogeo(jl,j)
            end do
         end do
      end do
c      
c     determinant
c
      if (nsd.eq.1) then
         detj = t(1,1)
      else if (nsd.eq.2) then
         detj = t(1,1)*t(2,2) - t(2,1)*t(1,2)
      else
         d11 = t(2,2)*t(3,3)-t(3,2)*t(2,3)
         d12 = -(t(2,1)*t(3,3)-t(3,1)*t(2,3))
         d13 = t(2,1)*t(3,2)-t(3,1)*t(2,2)
         detj = t(1,1)*d11 + t(1,2)*d12 + t(1,3)*d13
      end if
c
c     Check if negative
c
      if (detj .le. 0.0d0) then
         if (iel .ne. lastnegel) then ! Only one warning for each element
            lastnegel = iel
            write(*,*)
     *        ' isopar. non-positive jacobian (iel, value, ipgau):',
     *        iel,detj,ipgau
c            write (*,*) 'nsd, nodgeo:', nsd, nodgeo
            write (*,*) 'Jacobian Matrix:'
            do i = 1, nsd
               write(*,*) i,':',(t(i,j), j=1,nsd)
            end do
            write (*,*) 'Nodal coordinates:'
            do jl = 1, nodgeo
               write(*,*) jl,':',(cogeo(jl,j), j=1,nsd)
            end do
         end if
! Unfold if simplex, else stop
         if (nodgeo .ne. nsd+1) stop 'Fatal error in isopar'
         if (nsd.eq.1) then
            cogeo(2,1) = cogeo(1,1) + cogeo(1,1) - cogeo(2,1)
         else if (nsd.eq.2) then
            x12 = cogeo(2,1) - cogeo(1,1)
            x23 = cogeo(3,1) - cogeo(2,1)
            x31 = cogeo(1,1) - cogeo(3,1)
            y12 = cogeo(2,2) - cogeo(1,2)
            y23 = cogeo(3,2) - cogeo(2,2)
            y31 = cogeo(1,2) - cogeo(3,2)
            a12 = sqrt(x12*x12 + y12*y12)
            a23 = sqrt(x23*x23 + y23*y23)
            a31 = sqrt(x31*x31 + y31*y31)
            if (a12 .ge. a23 .and. a12 .ge. a31) then ! edge 12 is the largest
               h = -detj / a12 ! distance from node 3 to edge a12
               if (h .lt. 0.0001 * a12) then
                  disp = 0.0001 * a12 ! Displacement: minimal positive area
               else
                  disp = 2*h ! Displacement: reflexion
               end if
               dirx = -y12 / a12 ! Direction of displacement:
               diry =  x12 / a12 ! normal to edge
               cogeo(3,1) = cogeo(3,1) + disp * dirx
               cogeo(3,2) = cogeo(3,2) + disp * diry
            else if (a23 .ge. a31) then ! edge 23 is the largest
               h = -detj / a23 ! distance from node 1 to edge a23
               if (h .lt. 0.0001 * a23) then
                  disp = 0.0001 * a23 ! Displacement: minimal positive area
               else
                  disp = 2*h ! Displacement: reflection
               end if
               dirx = -y23 / a23 ! Direction of displacement:
               diry =  x23 / a23 ! normal to edge
               cogeo(1,1) = cogeo(1,1) + disp * dirx
               cogeo(1,2) = cogeo(1,2) + disp * diry
            else ! edge 31 is the largest
               h = -detj / a31 ! distance from node 2 to edge a31
               if (h .lt. 0.0001 * a31) then
                  disp = 0.0001 * a31 ! Displacement: minimal positive area
               else
                  disp = 2*h ! Displacement: reflection
               end if
               dirx = -y31 / a31 ! Direction of displacement:
               diry =  x31 / a31 ! normal to edge
               cogeo(2,1) = cogeo(2,1) + disp * dirx
               cogeo(2,2) = cogeo(2,2) + disp * diry
            end if
         else ! 3D: Find largest face....
            x12 = cogeo(2,1) - cogeo(1,1)
            x13 = cogeo(3,1) - cogeo(1,1)
            x14 = cogeo(4,1) - cogeo(1,1)
            y12 = cogeo(2,2) - cogeo(1,2)
            y13 = cogeo(3,2) - cogeo(1,2)
            y14 = cogeo(4,2) - cogeo(1,2)
            z12 = cogeo(2,3) - cogeo(1,3)
            z13 = cogeo(3,3) - cogeo(1,3)
            z14 = cogeo(4,3) - cogeo(1,3)
            x23 = cogeo(3,1) - cogeo(2,1)
            x24 = cogeo(4,1) - cogeo(2,1)
            y23 = cogeo(3,2) - cogeo(2,2)
            y24 = cogeo(4,2) - cogeo(2,2)
            z23 = cogeo(3,3) - cogeo(2,3)
            z24 = cogeo(4,3) - cogeo(2,3)
            x34 = cogeo(4,1) - cogeo(3,1)
            y34 = cogeo(4,2) - cogeo(3,2)
            z34 = cogeo(4,3) - cogeo(3,3)
            a4x = y12*z13 - z12*y13
            a4y = z12*x13 - x12*z13
            a4z = x12*y13 - y12*x13
            a4mod = sqrt(a4x*a4x + a4y*a4y + a4z*a4z)
            a2x = y13*z14 - z13*y14
            a2y = z13*x14 - x13*z14
            a2z = x13*y14 - y13*x14
            a2mod = sqrt(a2x*a2x + a2y*a2y + a2z*a2z)
            a3x = y14*z12 - z14*y12
            a3y = z14*x12 - x14*z12
            a3z = x14*y12 - y14*x12
            a3mod = sqrt(a3x*a3x + a3y*a3y + a3z*a3z)
            a1x = y24*z23 - z24*y23
            a1y = z24*x23 - x24*z23
            a1z = x24*y23 - y24*x23
            a1mod = sqrt(a1x*a1x + a1y*a1y + a1z*a1z)
            if (a1mod .ge. a2mod .and. a1mod .ge. a3mod .and.
     &           a1mod .ge. a4mod) then         ! face 243 is the largest
               h = -detj / a1mod ! distance from node 1 to face 243
               if (h .lt. 0.0001 * sqrt(a1mod)) then
                  disp = 0.0001 * sqrt(a1mod) ! Displacement: minimal positive area
               else
                  disp = 2*h ! Displacement: reflexion
               end if
               dirx = a1x / a1mod ! Direction of displacement:
               diry = a1y / a1mod ! normal to face
               dirz = a1z / a1mod
               cogeo(1,1) = cogeo(1,1) + disp * dirx
               cogeo(1,2) = cogeo(1,2) + disp * diry
               cogeo(1,3) = cogeo(1,3) + disp * dirz
            else if (a2mod .ge. a3mod .and. a2mod .ge. a4mod) then ! face 134
               h = -detj / a2mod ! distance from node 2 to face 134
               if (h .lt. 0.0001 * sqrt(a2mod)) then
                  disp = 0.0001 * sqrt(a2mod) ! Displacement: minimal positive area
               else
                  disp = 2*h ! Displacement: reflexion
               end if
               dirx = a2x / a2mod ! Direction of displacement:
               diry = a2y / a2mod ! normal to face
               dirz = a2z / a2mod
               cogeo(2,1) = cogeo(2,1) + disp * dirx
               cogeo(2,2) = cogeo(2,2) + disp * diry
               cogeo(2,3) = cogeo(2,3) + disp * dirz
            else if (a3mod .ge. a4mod) then ! face 142 is the largest
               h = -detj / a3mod ! distance from node 3 to face 142
               if (h .lt. 0.0001 * sqrt(a3mod)) then
                  disp = 0.0001 * sqrt(a3mod) ! Displacement: minimal positive area
               else
                  disp = 2*h ! Displacement: reflexion
               end if
               dirx = a3x / a3mod ! Direction of displacement:
               diry = a3y / a3mod ! normal to face
               dirz = a3z / a3mod
               cogeo(3,1) = cogeo(3,1) + disp * dirx
               cogeo(3,2) = cogeo(3,2) + disp * diry
               cogeo(3,3) = cogeo(3,3) + disp * dirz
            else ! face 123 is the largest
               h = -detj / a4mod ! distance from node 4 to face 123
               if (h .lt. 0.0001 * sqrt(a4mod)) then
                  disp = 0.0001 * sqrt(a4mod) ! Displacement: minimal positive area
               else
                  disp = 2*h ! Displacement: reflexion
               end if
               dirx = a4x / a4mod ! Direction of displacement:
               diry = a4y / a4mod ! normal to face
               dirz = a4z / a4mod
               cogeo(4,1) = cogeo(4,1) + disp * dirx
               cogeo(4,2) = cogeo(4,2) + disp * diry
               cogeo(4,3) = cogeo(4,3) + disp * dirz
c--------------------------------------------------------------------------
            end if
c
         end if
c     Re-compute Jacobian matrix
         do i=1,nsd
            do j=1,nsd
               t(i,j) = 0.d0
               do jl=1,nodgeo
                  t(i,j) = t(i,j) + dpl(jl,ipgau,i,intgeo)*cogeo(jl,j)
               end do
            end do
         end do
!
         call jacinv (t,nsd,ti,detj)
!         stop
      else
c
c     Inverse Jacobian matrix
c
         if (nsd.eq.1) then
            ti(1,1) = 1.0d0 / t(1,1)
         else if (nsd.eq.2) then
            ti(1,1) =  t(2,2)/detj
            ti(2,1) = -t(2,1)/detj
            ti(1,2) = -t(1,2)/detj
            ti(2,2) =  t(1,1)/detj
         else
            ti(1,1) =  (t(2,2)*t(3,3)-t(3,2)*t(2,3))/detj
            ti(2,1) = -(t(2,1)*t(3,3)-t(3,1)*t(2,3))/detj
            ti(3,1) =  (t(2,1)*t(3,2)-t(3,1)*t(2,2))/detj
            ti(1,2) = -(t(1,2)*t(3,3)-t(3,2)*t(1,3))/detj
            ti(2,2) =  (t(1,1)*t(3,3)-t(3,1)*t(1,3))/detj
            ti(3,2) = -(t(1,1)*t(3,2)-t(3,1)*t(1,2))/detj
            ti(1,3) =  (t(1,2)*t(2,3)-t(2,2)*t(1,3))/detj
            ti(2,3) = -(t(1,1)*t(2,3)-t(2,1)*t(1,3))/detj
            ti(3,3) =  (t(1,1)*t(2,2)-t(2,1)*t(1,2))/detj
         end if
      end if
c
c global first derivatives
c
      kxx = 0
      do ivar = 1,nvar
         itype = kint(ivar)
         if (itype .ne. kxx) then
            kxx = itype
            do i = 1, nod(ivar)
               do j = 1, nsd
                  dp(i,j,itype) = 0.d0
                  do k = 1, nsd
                     dp(i,j,itype) = dp(i,j,itype) +
     #                    dpl(i,ipgau,k,itype)*ti(j,k)
                  end do
               end do
            end do
         end if
      end do
c
      return
      end

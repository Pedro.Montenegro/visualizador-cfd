c -----------------------------------------------------------------------------
      subroutine jacinv (t,nsd,ti,detj)
c -----------------------------------------------------------------------------
c compute inverse and determinant of jacobian matrix.
c
      implicit real*8 (a-h,o-z)
c
      dimension t(3,3), ti(3,3)
c
      if (nsd.eq.1) then
c
         detj = t(1,1)
c
         ti(1,1) = 1.0d0 / t(1,1)
c
      else if (nsd.eq.2) then
c
         detj = t(1,1)*t(2,2) - t(2,1)*t(1,2)
c
         ti(1,1) =  t(2,2)/detj
         ti(2,1) = -t(2,1)/detj
         ti(1,2) = -t(1,2)/detj
         ti(2,2) =  t(1,1)/detj
c
      else
c
         d11 = t(2,2)*t(3,3)-t(3,2)*t(2,3)
         d12 = -(t(2,1)*t(3,3)-t(3,1)*t(2,3))
         d13 = t(2,1)*t(3,2)-t(3,1)*t(2,2)
c
         detj = t(1,1)*d11 + t(1,2)*d12 + t(1,3)*d13
c
         ti(1,1) = d11/detj
         ti(2,1) = d12/detj
         ti(3,1) = d13/detj
         ti(1,2) = -(t(1,2)*t(3,3)-t(3,2)*t(1,3))/detj
         ti(2,2) =  (t(1,1)*t(3,3)-t(3,1)*t(1,3))/detj
         ti(3,2) = -(t(1,1)*t(3,2)-t(3,1)*t(1,2))/detj
         ti(1,3) =  (t(1,2)*t(2,3)-t(2,2)*t(1,3))/detj
         ti(2,3) = -(t(1,1)*t(2,3)-t(2,1)*t(1,3))/detj
         ti(3,3) =  (t(1,1)*t(2,2)-t(2,1)*t(1,2))/detj
c
      end if
c
      return
      end

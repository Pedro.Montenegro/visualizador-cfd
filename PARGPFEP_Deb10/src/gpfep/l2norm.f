      SUBROUTINE L2NORM (N,V,RNOR)
      REAL*8 V(1),RNOR
      RNOR=0.0D0
      DO I=1,N
         RNOR = RNOR + V(I)**2
      END DO
      RNOR = SQRT(RNOR)
      RETURN
      END

      SUBROUTINE REACONSUR (lusur,nelsur,nne,ie,je)
      dimension ie(*), je(*)
      character*80 REC
      CALL SEARSTR (LUSUR, 'INCIDENCE')
      READ (LUSUR, '(A80)') REC
      IF (REC(1:6) .NE. '<NONE>') STOP 'PROBLEMAS...'
      READ (LUSUR, *) (je(I), I = 1, NELSUR*NNE)
      ie(1)=1
      do iel=1,nelsur
         ie(iel+1)=ie(iel)+nne
      end do
      return
      end
C
      SUBROUTINE orig_REAELTYP (LU, NGR, NEL, ELTYPE)
      CHARACTER *80 ELTYPE, REC
!     write (*,*) 'entro en reaeltype'
      READ (LU, '(A80)') REC
!     write (*,*) 'record:'
!     write (*,*) 'record:'
!     write (*,'(a70)') rec(1:70)
      OPEN  (3, FILE='SMORED.AUX',STATUS='UNKNOWN')
      WRITE (3,'(A80)') REC
      CLOSE (3)
      OPEN (3, FILE='SMORED.AUX',STATUS='OLD')
      READ (3, *) IGR, NEL
      CLOSE(3, STATUS='DELETE')
C
!     write (*,*) 'antes de backspace. lu = ', lu
!     write (*,*) 'antes de backspace. lu = ', lu
!     BACKSPACE(LU)
!     write (*,*) 'despues de backspace'
!     write (*,*) 'despues de backspace'
!     READ (LU,*) IGR, NEL
!     write (*,*) 'grupo:', igr, '# elementos:', nel
      IF (IGR.NE.NGR) THEN
         STOP 'Unexpected Group number'
      END IF
      I = 80
      DO WHILE (REC(I:I) .EQ. ' ')
         I = I - 1
      END DO
      ILAST = I
      DO WHILE (REC(I:I) .NE. ' ')
         I = I - 1
      END DO
      IFIRST = I+1
!     write (*,*) 'IFirst, ILast:', ifirst, ilast
      LENELTYPE = ILAST - IFIRST + 1
      ELTYPE(1:LENELTYPE) = REC(IFIRST:ILAST)
      RETURN
      END
C
      SUBROUTINE SEARSTR(LU, STR)
      LOGICAL REW
      CHARACTER *(*) STR
      CHARACTER *80 REC
      INTEGER STRLEN
      REW = .FALSE.
      STRLEN = LEN(STR)
   10 READ (LU, '(A80)', END=20) REC
      IF (REC(1:1) .NE. '*') GOTO 10
      IF (REC(2:STRLEN+1) .EQ. STR) RETURN
      IF (REC(2:4) .EQ. 'END') THEN
         IF (REW) THEN
            GOTO 20
         ELSE
            REW =.TRUE.
            REWIND (LU)
            GOTO 10
         ENDIF
      END IF
      GOTO 10
   20 STOP 'Can''t find Keyword'
      END
C
C     ------------------------------------------------------------------
      INTEGER FUNCTION IFINDKEY (LU, STR)
C     ------------------------------------------------------------------
      LOGICAL REW
      CHARACTER *(*) STR
      CHARACTER *80 REC
      INTEGER STRLEN
C
      REW = .FALSE.
      STRLEN = LEN(STR)
C
C Reads Record
C
   10 READ (LU, '(A80)', END=20) REC
      IF (REC(1:1) .NE. '*') GOTO 10
      IF (REC(2:STRLEN+1) .EQ. STR) THEN
         IFINDKEY = 1
         RETURN
      END IF
      IF (REC(2:4) .EQ. 'END') GO TO 20
      GOTO 10
C
C End of file or Keyword END found, try rewind and repeat search
C
   20 CONTINUE
      IF (REW) THEN
         GOTO 30
      ELSE
         REW =.TRUE.
         REWIND (LU)
         GOTO 10
      ENDIF
C
C End of file or END Keyword and already tried rewind
C
   30 IFINDKEY = 0
      RETURN
      END

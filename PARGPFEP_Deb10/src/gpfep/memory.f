C     ------------------------------------------------------------------
      SUBROUTINE MEMORY (LAST)
C     ------------------------------------------------------------------
C
      COMMON /STORE/MAX
  100 FORMAT (///'  ------------------------------------ '/
     *           ' !        INSUFICIENTE MEMORIA        !'/
     *           ' !                                    !'/
     *           ' !  Reservada (MAX)  : ',I7 ,' palab. !'/
     *           ' !  Necesaria (LAST) : ',I7 ,' palab. !'/
     *           ' !                                    !'/
     *           ' !  modificar dicho valor en el MAIN  !'/
     *           '  ------------------------------------'//)
C
      IF (LAST.LE.MAX) RETURN
      WRITE (6,100)MAX,LAST
      STOP
      END


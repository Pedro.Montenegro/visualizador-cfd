C     -----------------------------------------------------------------
      SUBROUTINE MOVECD (VN,VO,N)
C     -----------------------------------------------------------------
C
      real*8 VN(1),VO(1)
C
      IF (N.GT.0) THEN
         DO 10 I = 1,N
            VN(I) = VO(I)
   10    CONTINUE
      ELSE
         DO 20 I = ABS(N),1,-1
            VN(I) = VO(I)
   20    CONTINUE
      END IF
C
      RETURN
      END

C     -----------------------------------------------------------------
      SUBROUTINE MOVECT (IVN,IVO,N)
C     -----------------------------------------------------------------
C
      DIMENSION IVN(1),IVO(1)
C
      IF (N.GT.0) THEN
         DO 10 I = 1,N
            IVN(I) = IVO(I)
   10    CONTINUE
      ELSE
         DO 20 I = ABS(N),1,-1
            IVN(I) = IVO(I)
   20    CONTINUE
      END IF
C
      RETURN
      END

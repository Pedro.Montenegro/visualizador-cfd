C     ------------------------------------------------------------------
      SUBROUTINE NUMFAKNS 
     *          (IA,JA,ANS,ANI,AD,IU,JU,UNS,UNI,DI,DICOL,IP,IUP,N)
C     ------------------------------------------------------------------
C  REALIZA NUMERICAMENTE LA FACTORIZACION TRIANGULAR L*D*U DE UNA MATRIZ
C  NO SIMETRICA A DE N*N. METODO SHERMAN.
C
C  DATOS.
C  IA,JA,ANS,ANI,AD  MATRIZ A.
C  IU,JU  ESTRUCTURA PARA U.
C  N  ORDEN DE A Y DE U.
C
C  RESULTADOS.
C  UNS,UNI,DI  FACTOR SUPERIOR E INFERIOR DE U. 
C              EN DI QUEDAN LAS INVERSAS DE LOS ELEMENTOS DIAGONALES DE U.
C
C  VECTORES AUXILIARES.
C  IP(N)  LISTAS DE NO-CEROS PRIMEROS DE PEDACITO DE FILA DE CADA COLUMNA
C  IUP(N)  PUNTEROS PARA LOS PEDACITOS DE FILA.
C  DICOL(N) COLUMNA EXPANDIDA
C
C  SUBPROGRAMAS=NINGUNO.
C  STOP=0
C
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION IA(1),JA(1),ANS(1),ANI(1),AD(1),
     * IU(1),JU(1),UNS(1),UNI(1),DI(1),DICOL(1),IP(1),IUP(1)
C
      PIVMIN = 1.0D+30
      PIVMAX = 0.0D0
      NEGDIA = 0
C
      DO 10 J=1,N
 10      IP(J)=0
      DO 150 I=1,N
         IH=I+1
         IUA=IU(I)
         IUB=IU(IH)-1
         IF(IUB.LT.IUA) GOTO 40 ! No hay elementos extradiagonales en U
         DO 20 J=IUA,IUB        ! Pone en 0 los vectores expandidos de
            DI(JU(J))=0.D0      ! fila y columna del pivot
 20         DICOL(JU(J))=0.D0
         IAA=IA(I)
         IAB=IA(IH)-1
         IF(IAB.LT.IAA) GOTO 40 ! No hay elementos extradiagonales en A
         DO 30 J=IAA,IAB
            DI   (JA(J))=ANS(J) ! DI(I:N) : Fila siendo procesada
 30         DICOL(JA(J))=ANI(J) ! Su parte `simetrica'
 40      DI(I)=AD(I)
         IF(AD(I).LT.1.0D-38) THEN
C     WRITE (6,*)'NUMFAKNS.WARNING ',I,AD(I)
            NEGDIA = NEGDIA + 1
         END IF
         LAST=IP(I)             ! Lista circular de filas c/no-ceros en col.I
         IF(LAST.EQ.0) GOTO 120
         LN=IP(LAST)
 50      L=LN                   ! L: `Nodo actual' de la lista circular (fila)
         LN=IP(L)               ! LN: `Proximo Nodo'
         IUC=IUP(L)             ! IUC,IUD: Lista en UNS de no ceros de fila L /
         IUD=IU(L+1)-1          !         con indice de columna >= I
         UM   =UNI(IUC)*DI(L)   ! Factor a multiplicar * L y restar en I
         UMCOL=UNS(IUC)*DI(L)   ! Factor p/adelantarse a eliminacion de(JJ,L)
         IF(ABS(UM).LT.1.0D-38) GOTO 70 ! Saltea los ceros numericos
         DO 60 J=IUC,IUD
            JJ=JU(J)
 60         DI   (JJ)=DI   (JJ)-UNS(J)*UM
 70      IF(ABS(UMCOL).LT.1.0D-38) GOTO 90 ! Saltea los ceros numericos
         DO 80 J=IUC+1,IUD
            JJ=JU(J)
 80         DICOL(JJ)=DICOL(JJ)-UNI(J)*UMCOL
 90      UNI(IUC)=UM         ! Guarda elemento de matriz `Lower'
         UNS(IUC)=UMCOL         ! Guarda elemento de matriz `Upper'
         IUP(L)=IUC+1
         IF(IUC.EQ.IUD)GOTO 110
         J=JU(IUC+1)
         JJ=IP(J)
         IF(JJ.EQ.0)GOTO 100
         IP(L)=IP(JJ)
         IP(JJ)=L
         GOTO 110
 100     IP(J)=L
         IP(L)=L
 110     IF(L.NE.LAST)GOTO 50
 120     UM=DI(I)
C
         IF (ABS(UM).LT.PIVMIN) THEN
            PIVMIN = ABS(UM)
         ELSE IF (ABS(UM).GT.PIVMAX) THEN
            PIVMAX = ABS(UM)
         ENDIF
C
C     IF(UM.LT.0)WRITE (6,*)'PIVOT NEGATIVO:',I,UM
         IF(ABS(UM).LT.1.0D-38)GOTO 360
         DI(I)=1./UM            ! Guarda elemento de matriz `Diagonal'
         IF(IUB.LT.IUA)GOTO 150
         DO 130 J=IUA,IUB       ! Guarda filas y columnas que habian sido
            UNS(J)=DI(JU(J))    ! expandidas nuevamente en su lugar
 130        UNI(J)=DICOL(JU(J))
         J=JU(IUA)
         JJ=IP(J)
         IF(JJ.EQ.0)GOTO 140
         IP(I)=IP(JJ)
         IP(JJ)=I
         GOTO 150
 140     IP(J)=I
         IP(I)=I
 150     IUP(I)=IUA
c     WRITE (6,1000) PIVMIN, PIVMAX
      IF (NEGDIA.GT.0) WRITE (6,*) ' NUMFAKNS.WARNING: ', NEGDIA,
     *        ' NEG. DIAG.'
      RETURN
C
 360  WRITE(6,365)I,UM,AD(I)
      STOP
 365  FORMAT(1X,'NUMFAKNS.ERROR',I5,2E16.8)
c     1000 FORMAT(1X,'NUMFAKNS.PIVOTS: MINIMO:',E16.8,' MAXIMO:',E16.8)
      END

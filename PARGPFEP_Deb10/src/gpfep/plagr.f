c -----------------------------------------------------------------------------
      function plagr (i,j,p,x)
c -----------------------------------------------------------------------------
c Evaluate the "i" Lagrange polynomial of order j at x.
c i: polynomial number i.
c j: order of polynomial.
c p: local coordinates of nodes "i".
c
      implicit real*8 (a-h,o-z)
c
      dimension p(*)
c
      if (j.eq.0) then
         plagr = 1.
      else if (j.eq.1) then
c
         if (i.eq.1) then
            plagr = (x-p(2))/(p(1)-p(2))
         else if (i.eq.2) then
            plagr = (x-p(1))/(p(2)-p(1))
         else
            write(6,360)i,j
            stop
         end if
c
      else if (j.eq.2) then
c
         if (i.eq.1) then
            plagr = (x-p(2))*(x-p(3))/((p(1)-p(2))*(p(1)-p(3)))
         else if (i.eq.2) then
            plagr = (x-p(1))*(x-p(3))/((p(2)-p(1))*(p(2)-p(3)))
         else if (i.eq.3) then
            plagr = (x-p(1))*(x-p(2))/((p(3)-p(1))*(p(3)-p(2)))
         else
            write(6,360)i,j
            stop
         end if
c
      else if (j.eq.3) then
c
         if (i.eq.1) then
            plagr = (x-p(2))*(x-p(3))*(x-p(4))/
     *           ((p(1)-p(2))*(p(1)-p(3))*(p(1)-p(4)))
         else if (i.eq.2) then
            plagr = (x-p(1))*(x-p(3))*(x-p(4))/
     *           ((p(2)-p(1))*(p(2)-p(3))*(p(2)-p(4)))
         else if (i.eq.3) then
            plagr = (x-p(1))*(x-p(2))*(x-p(4))/
     *           ((p(3)-p(1))*(p(3)-p(2))*(p(3)-p(4)))
         else if (i.eq.4) then
            plagr = (x-p(1))*(x-p(2))*(x-p(3))/
     *           ((p(4)-p(1))*(p(4)-p(2))*(p(4)-p(3)))
         else
            write(6,360)i,j
            stop
         end if
c
      else
         write(6,360)i,j
         stop
      end if
      return
c
 360  format (' Error in function PLAGR.',2i5)
      end

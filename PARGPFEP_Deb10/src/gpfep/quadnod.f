c -----------------------------------------------------------------------------
      subroutine quadnod (nsd,intdeg,maxgau,ngau,cog,wg)
c -----------------------------------------------------------------------------
c Nodal integration rules for 1D segment, 2D quadrilateral and 3D cube.
c nint: number of gauss points in each dimension.
c
      implicit real*8 (a-h,o-z)
c
      dimension x(10), w(10), cog(maxgau,nsd), wg(maxgau)
c
      if (intdeg.eq.0.or.intdeg.eq.1) then
         nint = 2
         x(1) = -1
         w(1) = 1
         x(2) = 1
         w(2) = 1
      else
         write(6,361)intdeg
         stop
      end if
c
      ngau = nint**nsd
c
      if (ngau.gt.maxgau) then
         write(6,360)nsd,nint,ngau
         stop
      end if
c
      if (nsd.eq.1) then
c
         do i = 1, nint
            ig = i
            cog(ig,1) = x(i)
            wg(ig) = w(i)
         end do
c
      else if (nsd.eq.2) then
c
         do j = 1, nint
            do i = 1, nint
               ig = (j-1)*nint + i
               cog(ig,1) = x(i)
               cog(ig,2) = x(j)
               wg(ig) = w(i)*w(j)
            end do
         end do
c
      else if (nsd.eq.3) then
c
         do k=1,nint
            do j=1,nint
               do i=1,nint
                  ig = (k-1)*nint*nint + (j-1)*nint + i
                  cog(ig,1) = x(i)
                  cog(ig,2) = x(j)
                  cog(ig,3) = x(k)
                  wg(ig) = w(i)*w(j)*w(k)
               end do
            end do
         end do
c
      else
         write(6,360)nsd,nint,ngau
         stop
      end if
c
      return
 360  format(' Error in QUADLAGR. ',3i5)
 361  format(' QUADLAGR. Integration degree not implemented ',i5)
      end

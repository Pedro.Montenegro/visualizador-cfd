c     ------------------------------------------------------------------
      subroutine quadrul (ieltyp,itgdeg,itgtyp,maxgau,ngau,nsd,cog,wgp)
c     ------------------------------------------------------------------
c Quadrature rules.  Return coordinates and weights of integration points.
c
      implicit real*8 (a-h,o-z)
c
      dimension cog(maxgau,3), wgp(maxgau)
c
      if (ieltyp.eq.1.or.ieltyp.eq.2.or.ieltyp.eq.4) then
c
c 1D segment, 2D quadrilateral or 3D hexahedron.
c
         if (itgtyp.eq.1) then
            call quadlagr (nsd,itgdeg,maxgau,ngau,cog,wgp)
         else if (itgtyp.eq.10) then
            call quadnod (nsd,itgdeg,maxgau,ngau,cog,wgp)
         end if
c
      else if (ieltyp.eq.3) then
c
c 2D triangle
c
         call quadtria (itgdeg,itgtyp,maxgau,ngau,cog,wgp)
c
      else if (ieltyp.eq.5) then
c
c 3D tetrahedron
c
         call quadtetr (itgdeg,itgtyp,maxgau,ngau,cog,wgp)
c
      else
         write(6,360)ieltyp
         stop
      end if
c
      return
 360  format(/' QUADRUL.  Element type not defined. ',i5)
      end

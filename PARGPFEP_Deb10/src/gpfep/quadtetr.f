c -----------------------------------------------------------------------------
      subroutine quadtetr (intdeg,inty,maxgau,ngau,cog,w)
c -----------------------------------------------------------------------------
c Integration rules for tetrahedra.
c intdeg: maximum order of polynomial integrated exactly.
c inty: integration type.
c
      implicit real*8 (a-h,o-z)
c
      dimension cog(maxgau,3), w(maxgau)
c
      if (intdeg.eq.1) then
c
         if (inty.eq.1) then
            ngau = 1
c
            cog(1,1) = 0.25d0
            cog(1,2) = cog(1,1)
            cog(1,3) = cog(1,1)
c
            w(1) = 0.16666666666666667D+00
c
         else if (inty.eq.2) then
            ngau = 4
            cog(1,1) = 0.0
            cog(1,2) = 0.0
            cog(1,3) = 0.0
            cog(2,1) = 1.0
            cog(2,2) = 0.0
            cog(2,3) = 0.0
            cog(3,1) = 0.0
            cog(3,2) = 1.0
            cog(3,3) = 0.0
            cog(4,1) = 0.0
            cog(4,2) = 0.0
            cog(4,3) = 1.0
            w(1)     = 0.041666666666666667D+00
            w(2)     = 0.041666666666666667D+00
            w(3)     = 0.041666666666666667D+00
            w(4)     = 0.041666666666666667D+00
         else 
            write (6,360) intdeg, inty
            stop
         end if
c
      else if (intdeg.eq.2) then
c
         if (inty.eq.1) then
            ngau = 4
c
            a = 0.58541020D+00
            b = 0.13819660D+00
            cog(1,1) = a
            cog(1,2) = b
            cog(1,3) = b
            cog(2,1) = b
            cog(2,2) = a
            cog(2,3) = b
            cog(3,1) = b
            cog(3,2) = b
            cog(3,3) = a
            cog(4,1) = b
            cog(4,2) = b
            cog(4,3) = b
c
            do i = 1, 4
               w(i) = 0.25d0 / 6.0d0
            end do
c
         else
            write(6,360) intdeg,inty
            stop
         end if
c
      else if (intdeg.eq.3) then
c
         if (inty.eq.1) then
            ngau = 5
c
            a = 0.25d0
            b = 0.5d0
            c = 0.16666666666666667D+00
            cog(1,1) = a
            cog(1,2) = a
            cog(1,3) = a
            cog(2,1) = b
            cog(2,2) = c
            cog(2,3) = c
            cog(3,1) = c
            cog(3,2) = b
            cog(3,3) = c
            cog(4,1) = c
            cog(4,2) = c
            cog(4,3) = b
            cog(5,1) = c
            cog(5,2) = c
            cog(5,3) = c
c
            w(1) = -0.1333333333333333D+00
            do i = 2, 5
               w(i) = 0.075d0
            end do
c
         else
            write(6,360)intdeg,inty
            stop
         end if
c
      else
         write(6,360) intdeg,inty
         stop
      end if
      return
c
 360  format (' QUADTETR. Integration type not defined. ',2i5)
      end

c -----------------------------------------------------------------------------
      subroutine quadtria (intdeg,inty,maxgau,ngau,cog,w)
c -----------------------------------------------------------------------------
c Integration rules for triangles.
c intdeg: maximum order of polynomial integrated exactly.
c inty: integration type.
c
      implicit real*8 (a-h,o-z)
c
      dimension cog(maxgau,2), w(maxgau)
c
      if (intdeg.le.1) then
c
         if (inty.eq.1) then
            ngau = 1
c
            cog(1,1) = 0.333333333333333D+00
            cog(1,2) = cog(1,1)
c
            w(1) = 0.5d+00
         else
            write(6,360)intdeg,inty
            stop
         end if
c
      else if (intdeg.eq.2) then
c
         if (inty.eq.1) then
            ngau = 3
c
            a = 0.6666666666666667D+00
            b = 0.16666666666666667D+00
            cog(1,1) = a
            cog(1,2) = b
            cog(2,1) = b
            cog(2,2) = a
            cog(3,1) = b
            cog(3,2) = b
c
            w(1) = b
            w(2) = b
            w(3) = b
         else if (inty.eq.2) then
            ngau = 3
c
            a = 0.5d0
            b = 0.0d0
            cog(1,1) = a
            cog(1,2) = b
            cog(2,1) = b
            cog(2,2) = a
            cog(3,1) = a
            cog(3,2) = a
c
            w(1) = 0.16666666666666667D+00
            w(2) = w(1)
            w(3) = w(1)
         else
            write(6,360) intdeg,inty
            stop
         end if
c
      else if (intdeg.eq.3) then
c
         if (inty.eq.1) then
            ngau = 4
c
            a = 0.3333333333333333D+00
            b = 0.6d0
            c = 0.2d0
            cog(1,1) = a
            cog(1,2) = a
            cog(2,1) = b
            cog(2,2) = c
            cog(3,1) = c
            cog(3,2) = b
            cog(4,1) = c
            cog(4,2) = c
c
            w(1) = -0.5625D+00*0.5d0
            w(2) =  0.52083333333333333D+00*0.5d0
            w(3) = w(2)
            w(4) = w(2)
         else if (inty.eq.2) then
            ngau = 6
c
            a = 0.659027622374092D+00
            b = 0.231933368553031D+00
            c = 0.109039009072877D+00
            cog(1,1) = a
            cog(1,2) = b
            cog(2,1) = b
            cog(2,2) = a
            cog(3,1) = a
            cog(3,2) = c
            cog(4,1) = c
            cog(4,2) = a
            cog(5,1) = b
            cog(5,2) = c
            cog(6,1) = c
            cog(6,2) = b
c
            do i = 1, 6
               w(i) = 0.16666666666666667D+00*0.5d0
            end do
         else
            write(6,360)intdeg,inty
            stop
         end if
c
      else if (intdeg.eq.4) then
c
         if (inty.eq.1) then
            ngau = 6
c
            a = 0.81684 75729 80459D+00
            b = 0.09157 62135 09771D+00
            c = 0.10810 30181 68070D+00
            d = 0.44594 84909 15965D+00
c
            cog(1,1) = a
            cog(1,2) = b
            cog(2,1) = b
            cog(2,2) = a
            cog(3,1) = b
            cog(3,2) = b
            cog(4,1) = c
            cog(4,2) = d
            cog(5,1) = d
            cog(5,2) = c
            cog(6,1) = d
            cog(6,2) = d
c
            do i = 1, 3
               w(i) = 0.10995 17436 55322D+00 * 0.5d0
            end do
            do i = 4, 6
               w(i) = 0.22338 15896 78011D+00 * 0.5d0
            end do
c
         else if (inty.eq.2) then
            ngau = 7
c
            a = 0.333333333333333D+00
            b = 0.73671 24989 68435D+00
            c = 0.23793 23664 72434D+00
            d = 0.02535 51345 51932D+00
c
            cog(1,1) = a
            cog(1,2) = a
            cog(2,1) = b
            cog(2,2) = c
            cog(3,1) = c
            cog(3,2) = b
            cog(4,1) = b
            cog(4,2) = d
            cog(5,1) = d
            cog(5,2) = b
            cog(6,1) = c
            cog(6,2) = d
            cog(7,1) = d
            cog(7,2) = c
c
            w(1) = 0.375D+00 * 0.5d0
            do i = 2, 7
               w(i) = 0.10416666666666667D+00 * 0.5d0
            end do
         else
            write(6,360)intdeg,inty
            stop
         end if
c
      else if (intdeg.eq.5) then
c
         if (inty.eq.1) then
            ngau = 7
c
            a = 0.33333 33333 33333D+00
            b1 = 0.79742 69853 53087D+00
            b2 = 0.10128 65073 23456D+00
            c1 = 0.47014 20641 05115D+00
            c2 = 0.05971 58717 89770D+00
c
            cog(1,1) = a
            cog(1,2) = a
            cog(2,1) = b1
            cog(2,2) = b2
            cog(3,1) = b2
            cog(3,2) = b1
            cog(4,1) = b2
            cog(4,2) = b2
            cog(5,1) = c1
            cog(5,2) = c2
            cog(6,1) = c2
            cog(6,2) = c1
            cog(7,1) = c1
            cog(7,2) = c1
c
            w(1) = 0.22503 00003 0000D+00 * 0.5d0
            do i = 2, 4
               w(i) = 0.12593 91805 44827D+00 * 0.5d0
            end do
            do i = 5, 7
               w(i) = 0.13239 41527 88506D+00 * 0.5d0
            end do
         else
            write(6,360)intdeg,inty
            stop
         end if
         
      else if (intdeg.eq.6) then

         ngau = 12
         cog(1,1) = 0.87382197101700d0
         cog(1,2) = 0.06308901449150d0
         cog(2,1) = 0.06308901449150d0
         cog(2,2) = 0.87382197101700d0
         cog(3,1) = 0.06308901449150d0
         cog(3,2) = 0.06308901449150d0 
         cog(4,1) = 0.50142650965818d0
         cog(4,2) = 0.24928674517091d0 
         cog(5,1) = 0.24928674517091d0
         cog(5,2) = 0.50142650965818d0
         cog(6,1) = 0.24928674517091d0
         cog(6,2) = 0.24928674517091d0
         cog(7,1) = 0.05314504984482d0 
         cog(7,2) = 0.31035245103378d0
         cog(8,1) = 0.05314504984482d0
         cog(8,2) = 0.63650249912140d0
         cog(9,1) = 0.31035245103378d0 
         cog(9,2) = 0.05314504984482d0
         cog(10,1) = 0.31035245103378d0
         cog(10,2) = 0.63650249912140d0
         cog(11,1) = 0.63650249912140d0
         cog(11,2) = 0.05314504984482d0
         cog(12,1) = 0.63650249912140d0 
         cog(12,2) = 0.31035245103378d0

         w(1) = 0.02542245318510d0
         w(2) = 0.02542245318510d0
         w(3) = 0.02542245318510d0
         w(4) = 0.05839313786319d0
         w(5) = 0.05839313786319d0
         w(6) = 0.05839313786319d0
         w(7) = 0.04142553780919d0
         w(8) = 0.04142553780919d0
         w(9) = 0.04142553780919d0
         w(10) = 0.04142553780919d0
         w(11) = 0.04142553780919d0
         w(12) = 0.04142553780919d0
c
      else
c
         write(6,360) intdeg,inty
         stop
c
      end if
      return
c
 360  format (' QUADTRIA. Integration type not defined. ',2i5)
      end

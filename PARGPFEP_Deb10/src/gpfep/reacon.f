      SUBROUTINE REACON (LU,NEL,MEDJE,IE,JE,NNODE,IMESH)
      DIMENSION IE(*),JE(*)
      READ (LU,*) (IE(I),I=1,NEL+1)
      READ (LU,*) (JE(I),I=1,MEDJE)
      IF (IE(2)-IE(1).NE.NNODE) THEN
         WRITE (6,*) ' REACON INCOMPATIBILITY, MESH  ',IMESH
         STOP
      END IF
      RETURN
      END
C
      SUBROUTINE RCONEC (LU,NEL,MEDJE,IE,JE,NNODE,IMESH)
      CHARACTER *80 rmode
      DIMENSION IE(*),JE(*)
c
      read (LU,'(A80)') rmode
c
      if (rmode(1:6) .EQ. '<NONE>') then
         DO iel = 1, NEL
            read (LU, *) (JE(NNODE*(IEL-1)+in), in=1,NNODE)
         END DO
      else if(rmode(1:8) .EQ. '<IGNORE>' .OR.
     *        rmode(1:8) .EQ. '<GLOBAL>' .OR.
     *        rmode(1:7) .EQ. '<LOCAL>' .OR.
     *        rmode(1:6) .EQ. '<TEST>') then
         DO iel = 1, NEL
            read (LU, *) IDUMMY, (JE(NNODE*(IEL-1)+in), in=1,NNODE)
         END DO
      else
         stop 'Unrecognized read mode in incidences'
      end if
c
      DO iel = 1, NEL + 1
         IE(iel) = NNODE * (iel - 1) + 1
      END DO
c
      RETURN
      END
c
      SUBROUTINE REAELTYP (LU, IGR, NELGR, ELTYPE)
      CHARACTER *80 ELTYPE, LINE
C
      READ (LU,'(A80)') LINE
      IPOS = 80
      DO WHILE (IPOS .GT. 0 .AND. LINE(IPOS:IPOS) .EQ. ' ')
         IPOS = IPOS - 1
      END DO
      IF (IPOS .EQ. 0) STOP 'Error reading Element Group'
      IEND = IPOS
      DO WHILE (IPOS .GT. 0 .AND. LINE(IPOS:IPOS) .NE. ' ')
         IPOS = IPOS - 1
      END DO
      ISTART = IPOS + 1
      READ (LINE(1:IPOS), *) IGR, NELGR
      ELTYPE = LINE(ISTART:IEND)
      RETURN
      END

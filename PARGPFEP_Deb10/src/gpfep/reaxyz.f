      SUBROUTINE REAXYZ (LU,NDIM,NOD,COOR)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION COOR(1)
      write (6,*) 'Reading coordinates of',NOD,' nodes'
      DO IDIM=1,NDIM
         READ (LU,*)(COOR(I+(IDIM-1)*NOD),I=1,NOD)
      END DO
      RETURN
      END
c
      SUBROUTINE REACOO (LU,NDIM,NOD,COOR)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION COOR(1)
      write (6,*) 'Reading coordinates of',NOD,' nodes'
      DO I=1,NOD
         READ (LU,*) IDUMMY, (COOR(I+(IDIM-1)*NOD), IDIM=1,NDIM)
      END DO
      RETURN
      END

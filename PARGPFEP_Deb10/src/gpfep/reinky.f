      SUBROUTINE REINKY (LU,KEYWRD,IVALUE)
      CHARACTER*(*) KEYWRD
      CHARACTER*80 LINE
      READ (LU,'(A80)',END=3010,ERR=3020) LINE
      LKEY = LEN(KEYWRD)
      IF (LINE(1:LKEY) .NE. KEYWRD) GOTO 3030
      DO I = 1,LKEY
         LINE(I:I) = ' '
      ENDDO
      READ (LINE(LKEY+2:LKEY+11),1011) AUX
      IF (AUX.GT.0) THEN
          IVALUE = NINT(AUX + 0.1)
      ELSE
          IVALUE = NINT(AUX - 0.1)
      END IF
      RETURN
 3010 WRITE (*,1010) KEYWRD
 1010 FORMAT (' End of file reading: ',A)
 1011 FORMAT (F18.10)
      STOP
 3020 WRITE (*,1020) KEYWRD
 1020 FORMAT (' Error reading: ',A)
      STOP
 3030 WRITE (*,1030) LINE, KEYWRD
      STOP
 1030 FORMAT (' Found:',/,1X,A80,/,'while expecting:',A)
      END
c
      SUBROUTINE REREKY (LU,KEYWRD,VALUE)
      IMPLICIT REAL*8 (A-H,O-Z)
      CHARACTER*(*) KEYWRD
      CHARACTER*80 LINE
      READ (LU,'(A80)',END=3010,ERR=3020) LINE
      LKEY = LEN(KEYWRD)
      IF (LINE(1:LKEY) .NE. KEYWRD) GOTO 3030
      DO I = 1,LKEY
         LINE(I:I) = ' '
      ENDDO
      READ (LINE(LKEY+1:LKEY+20),1011) VALUE
      RETURN
 3010 WRITE (*,1010) KEYWRD
 1010 FORMAT (' End of file reading: ',A)
 1011 FORMAT (F18.10)
      STOP
 3020 WRITE (*,1020) KEYWRD
 1020 FORMAT (' Error reading: ',A)
      STOP
 3030 WRITE (*,1030) LINE, KEYWRD
      STOP
 1030 FORMAT (' Found:',/,1X,A80,/,'while expecting:',A)
      END
c
      SUBROUTINE RECHKY (LU,KEYWRD,WORD)
      CHARACTER*(*) KEYWRD,WORD
      CHARACTER*80 LINE
      READ (LU,'(A80)',END=3010,ERR=3020) LINE
      LKEY = LEN(KEYWRD)
      IF (LINE(1:LKEY) .NE. KEYWRD) GOTO 3030
      DO I = 1,LKEY
         LINE(I:I) = ' '
      ENDDO
      WORD = LINE(LKEY+1:80)//LINE(1:LKEY)
      RETURN
 3010 WRITE (*,1010) KEYWRD
 1010 FORMAT (' End of file reading: ',A)
      STOP
 3020 WRITE (*,1020) KEYWRD
 1020 FORMAT (' Error reading: ',A)
      STOP
 3030 WRITE (*,1030) LINE, KEYWRD
      STOP
 1030 FORMAT (' Found:',/,1X,A80,/,'while expecting:',A)
      END

c-----------------------------------------------------------------------
      subroutine rscaling (adns, n, x, irm)
c-----------------------------------------------------------------------
c n:    orden de la matriz
c adns: elementos diagonales de la matriz en formato ralo
c x:    vector incognita
c irm:  orden del vector termino independiente
c
      implicit real*8 (a-h,o-z)
c
      dimension adns(*), x(*)
c
      do i = 0, irm-1
         do j = 1, n
            index = i*n + j
            x(index) = x(index)/dsqrt(adns(j))
         end do
      end do
c
      return
      end

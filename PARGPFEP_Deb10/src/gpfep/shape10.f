c -----------------------------------------------------------------------------
      subroutine shape10 (ngau,maxgau,maxnpe,p,dpl,cog)
c -----------------------------------------------------------------------------
c evaluates the shape functions and local derivatives at the gaussian points
c for the 2-node segment with linear interpolation.
c
      implicit real*8 (a-h,o-z)
c
      dimension p(maxnpe,maxgau), dpl(maxnpe,maxgau,3), cog(maxgau,3)
c
      do i = 1, ngau
c
         p(1,i) = 0.5d0*(1.d0-cog(i,1))
         p(2,i) = 0.5d0*(1.d0+cog(i,1))
c
         dpl(1,i,1) = -0.5d0
         dpl(2,i,1) =  0.5d0
c
      end do
      return
      end

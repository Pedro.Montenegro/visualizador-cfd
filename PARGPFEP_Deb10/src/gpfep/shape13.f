c -----------------------------------------------------------------------------
      subroutine shape13 (ngau,maxgau,maxnpe,p,dpl,dpl2,cog)
c -----------------------------------------------------------------------------
c evaluates the shape functions and local derivatives at the gaussian points
c for the 4-node lagrangian segment, cubic interpolation.
c
      implicit real*8 (a-h,o-z)
c
      dimension p(maxnpe,maxgau), dpl(maxnpe,maxgau,3), 
     *     dpl2(maxnpe,maxgau,3,3), cog(maxgau,3)
      dimension r(4)
      data r/-1.d0, -0.333333333333333d0, 0.333333333333333d0, 1.d0/
c
      do i = 1, 4
         do ig = 1, ngau
c
            x = cog(ig,1)
            p(i,ig) = plagr(i,3,r,x)
            dpl(i,ig,1) = plagrd1(i,3,r,x)
            dpl2(i,ig,1,1) = plagrd2(i,3,r,x)
c
         end do
      end do
c
      return
      end

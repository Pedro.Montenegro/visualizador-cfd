c -----------------------------------------------------------------------------
      subroutine shape20 (ngau,maxgau,maxnpe,p,dpl,cog)
c -----------------------------------------------------------------------------
c evaluates the shape functions and local derivatives at the gaussian points
c for the 4-node rectangle.
c
      implicit real*8 (a-h,o-z)
c
      dimension p(maxnpe,maxgau), dpl(maxnpe,maxgau,3), cog(maxgau,3)
c
      do i = 1,ngau
c
         xgp = cog(i,1)
         ygp = cog(i,2)
c
         p(1,i) = 0.25d0*(1.-xgp)*(1.-ygp)
         p(2,i) = 0.25d0*(1.+xgp)*(1.-ygp)
         p(3,i) = 0.25d0*(1.+xgp)*(1.+ygp)
         p(4,i) = 0.25d0*(1.-xgp)*(1.+ygp)

         dpl(1,i,1) = -0.25d0*(1.-ygp)
         dpl(1,i,2) = -0.25d0*(1.-xgp)
         dpl(2,i,1) =  0.25d0*(1.-ygp)
         dpl(2,i,2) = -0.25d0*(1.+xgp)
         dpl(3,i,1) =  0.25d0*(1.+ygp)
         dpl(3,i,2) =  0.25d0*(1.+xgp)
         dpl(4,i,1) = -0.25d0*(1.+ygp)
         dpl(4,i,2) =  0.25d0*(1.-xgp)
c
      end do
      return
      end

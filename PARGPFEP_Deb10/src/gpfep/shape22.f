c -----------------------------------------------------------------------------
      subroutine shape22 (ngau,maxgau,maxnpe,p,dpl,dpl2,cog)
c -----------------------------------------------------------------------------
c evaluates the shape functions and local derivatives at the gaussian points
c for the 9-node quadrilateral.
c
      implicit real*8 (a-h,o-z)
c
      dimension p(maxnpe,maxgau), dpl(maxnpe,maxgau,3), cog(maxgau,3),
     *     dpl2(maxnpe,maxgau,3,3)
      dimension ip(9,2), r(3)
      data ip/1,3,3,1,2,3,2,1,2,1,1,3,3,1,2,3,2,2/
      data r /-1.d0, 0.d0, 1.d0/
c
      do i = 1, 9
         do ig = 1, ngau
c
            x = cog(ig,1)
            y = cog(ig,2)
            i1 = ip(i,1)
            i2 = ip(i,2)
            p(i,ig) = plagr(i1,2,r,x)*plagr(i2,2,r,y)
            dpl(i,ig,1) = plagrd1(i1,2,r,x)*plagr(i2,2,r,y)
            dpl(i,ig,2) = plagr(i1,2,r,x)*plagrd1(i2,2,r,y) 
            dpl2(i,ig,1,1) = plagrd2(i1,2,r,x)*plagr(i2,2,r,y)
            dpl2(i,ig,2,2) = plagr(i1,2,r,x)*plagrd2(i2,2,r,y)
            dpl2(i,ig,1,2) = plagrd1(i1,2,r,x)*plagrd1(i2,2,r,y)
            dpl2(i,ig,2,1) = dpl2(i,ig,1,2)
c
         end do
      end do
c
      return
      end

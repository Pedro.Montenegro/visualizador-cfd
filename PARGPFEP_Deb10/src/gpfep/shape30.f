c -----------------------------------------------------------------------------
      subroutine shape30 (ngau,maxgau,maxnpe,p,dpl,cog)
c -----------------------------------------------------------------------------
c evaluates the shape functions and local derivatives at the gaussian points
c for the 3-node triangle with linear interpolation.
c
      implicit real*8 (a-h,o-z)
c
      dimension p(maxnpe,maxgau), dpl(maxnpe,maxgau,3), cog(maxgau,3)
c
      do i = 1,ngau
c
         xgp = cog(i,1)
         ygp = cog(i,2)
c
         p(1,i) = 1.d+00 - xgp - ygp
         p(2,i) = xgp
         p(3,i) = ygp
c
         dpl(1,i,1) = -1.d+00
         dpl(1,i,2) = -1.d+00
         dpl(2,i,1) = 1.d+00
         dpl(2,i,2) = 0.d+00
         dpl(3,i,1) = 0.d+00
         dpl(3,i,2) = 1.d+00
c
      end do
      return
      end

c -----------------------------------------------------------------------------
      subroutine shape32 (ngau,maxgau,maxnpe,p,dpl,dpl2,cog)
c -----------------------------------------------------------------------------
c evaluates the shape functions and local derivatives at the gaussian points
c for the 6-node triangle with quadratic interpolation.
c
      implicit real*8 (a-h,o-z)
c
      dimension p(maxnpe,maxgau), dpl(maxnpe,maxgau,3), 
     *     dpl2(maxnpe,maxgau,3,3), cog(maxgau,3)
c
      do ig = 1, ngau
c
         r = cog(ig,1)
         s = cog(ig,2)
         t = 1.0D+00 - r - s
c
         p(1,ig) = r*(2*r-1)
         p(2,ig) = 4*r*s
         p(3,ig) = s*(2*s-1)
         p(4,ig) = 4*s*t
         p(5,ig) = t*(2*t-1)
         p(6,ig) = 4*r*t
c
         dpl(1,ig,1) = 4*r - 1
         dpl(1,ig,2) = 0.d0
         dpl(2,ig,1) = 4*s
         dpl(2,ig,2) = 4*r
         dpl(3,ig,1) = 0.d0
         dpl(3,ig,2) = 4*s-1
         dpl(4,ig,1) = -4*s
         dpl(4,ig,2) = 4*(t-s)
         dpl(5,ig,1) = 1 - 4*t
         dpl(5,ig,2) = 1 - 4*t
         dpl(6,ig,1) = 4*(t-r)
         dpl(6,ig,2) = -4*r
c
         dpl2(1,ig,1,1) = 4.d0
         dpl2(1,ig,1,2) = 0.d0
         dpl2(1,ig,2,1) = dpl2(1,ig,1,2)
         dpl2(1,ig,2,2) = 0.d0
         dpl2(2,ig,1,1) = 0.d0
         dpl2(2,ig,1,2) = 4.d0
         dpl2(2,ig,2,1) = dpl2(2,ig,1,2)
         dpl2(2,ig,2,2) = 0.d0
         dpl2(3,ig,1,1) = 0.d0
         dpl2(3,ig,1,2) = 0.d0
         dpl2(3,ig,2,1) = dpl2(3,ig,1,2)
         dpl2(3,ig,2,2) = 4.d0
         dpl2(4,ig,1,1) = 0.d0
         dpl2(4,ig,1,2) = -4.d0
         dpl2(4,ig,2,1) = dpl2(4,ig,1,2)
         dpl2(4,ig,2,2) = -8.d0
         dpl2(5,ig,1,1) = 4.d0
         dpl2(5,ig,1,2) = 4.d0
         dpl2(5,ig,2,1) = dpl2(5,ig,1,2)
         dpl2(5,ig,2,2) = 4.d0
         dpl2(6,ig,1,1) = -8.d0
         dpl2(6,ig,1,2) = -4.d0
         dpl2(6,ig,2,1) = dpl2(6,ig,1,2)
         dpl2(6,ig,2,2) = 0.d0
c
      end do
c
      return
      end

c -----------------------------------------------------------------------------
      subroutine shape40 (ngau,maxgau,maxnpe,p,dpl,cog)
c -----------------------------------------------------------------------------
c evaluates the shape functions and local derivatives at the gaussian points
c for the 8-node cube with trilinear interpolation.
c
      implicit real*8 (a-h,o-z)
c
      dimension p(maxnpe,maxgau), dpl(maxnpe,maxgau,3), cog(maxgau,3)
c
      do i = 1, ngau
c
         xgp = cog(i,1)
         ygp = cog(i,2)
         zgp = cog(i,3)
c
         p(1,i) = 0.125d0*(1.d0-xgp)*(1.d0-ygp)*(1.d0-zgp)
         p(2,i) = 0.125d0*(1.d0+xgp)*(1.d0-ygp)*(1.d0-zgp)
         p(3,i) = 0.125d0*(1.d0+xgp)*(1.d0+ygp)*(1.d0-zgp)
         p(4,i) = 0.125d0*(1.d0-xgp)*(1.d0+ygp)*(1.d0-zgp)
         p(5,i) = 0.125d0*(1.d0-xgp)*(1.d0-ygp)*(1.d0+zgp)
         p(6,i) = 0.125d0*(1.d0+xgp)*(1.d0-ygp)*(1.d0+zgp)
         p(7,i) = 0.125d0*(1.d0+xgp)*(1.d0+ygp)*(1.d0+zgp)
         p(8,i) = 0.125d0*(1.d0-xgp)*(1.d0+ygp)*(1.d0+zgp)
c
         dpl(1,i,1) = -0.125d0*(1.d0-ygp)*(1.d0-zgp)
         dpl(1,i,2) = -0.125d0*(1.d0-xgp)*(1.d0-zgp)
         dpl(1,i,3) = -0.125d0*(1.d0-xgp)*(1.d0-ygp)
         dpl(2,i,1) =  0.125d0*(1.d0-ygp)*(1.d0-zgp)
         dpl(2,i,2) = -0.125d0*(1.d0+xgp)*(1.d0-zgp)
         dpl(2,i,3) = -0.125d0*(1.d0+xgp)*(1.d0-ygp)
         dpl(3,i,1) =  0.125d0*(1.d0+ygp)*(1.d0-zgp)
         dpl(3,i,2) =  0.125d0*(1.d0+xgp)*(1.d0-zgp)
         dpl(3,i,3) = -0.125d0*(1.d0+xgp)*(1.d0+ygp)
         dpl(4,i,1) = -0.125d0*(1.d0+ygp)*(1.d0-zgp)
         dpl(4,i,2) =  0.125d0*(1.d0-xgp)*(1.d0-zgp)
         dpl(4,i,3) = -0.125d0*(1.d0-xgp)*(1.d0+ygp)
c
         dpl(5,i,1) = -0.125d0*(1.d0-ygp)*(1.d0+zgp)
         dpl(5,i,2) = -0.125d0*(1.d0-xgp)*(1.d0+zgp)
         dpl(5,i,3) =  0.125d0*(1.d0-xgp)*(1.d0-ygp)
         dpl(6,i,1) =  0.125d0*(1.d0-ygp)*(1.d0+zgp)
         dpl(6,i,2) = -0.125d0*(1.d0+xgp)*(1.d0+zgp)
         dpl(6,i,3) =  0.125d0*(1.d0+xgp)*(1.d0-ygp)
         dpl(7,i,1) =  0.125d0*(1.d0+ygp)*(1.d0+zgp)
         dpl(7,i,2) =  0.125d0*(1.d0+xgp)*(1.d0+zgp)
         dpl(7,i,3) =  0.125d0*(1.d0+xgp)*(1.d0+ygp)
         dpl(8,i,1) = -0.125d0*(1.d0+ygp)*(1.d0+zgp)
         dpl(8,i,2) =  0.125d0*(1.d0-xgp)*(1.d0+zgp)
         dpl(8,i,3) =  0.125d0*(1.d0-xgp)*(1.d0+ygp)
c
      end do
      return
      end

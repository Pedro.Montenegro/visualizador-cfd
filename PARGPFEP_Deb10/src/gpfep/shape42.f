c -----------------------------------------------------------------------------
      subroutine shape42 (ngau,maxgau,maxnpe,p,dpl,dpl2,cog)
c -----------------------------------------------------------------------------
c evaluates the shape functions and local derivatives at the gaussian points
c for the 27-node triquadratic hexahedron.
c Nodal numbering as in Hughes, The Finite Element Method, pag. 136.
c     
      implicit real*8 (a-h,o-z)
c
      dimension p(maxnpe,maxgau), dpl(maxnpe,maxgau,3), 
     *     dpl2(maxnpe,maxgau,3,3), cog(maxgau,3)
      dimension ip(27,3), r(3)
      data ip/1,3,3,1,1,3,3,1,2,3,2,1,2,3,2,1,1,3,3,1,2,2,2,2,1,3,2,
     *     1,1,3,3,1,1,3,3,1,2,3,2,1,2,3,2,1,1,3,3,2,2,1,3,2,2,2,
     *     1,1,1,1,3,3,3,3,1,1,1,1,3,3,3,3,2,2,2,2,1,3,2,2,2,2,2/
      data r/-1.,0.,1./
c
      do i = 1,27
         do ig = 1,ngau
c
            x = cog(ig,1)
            y = cog(ig,2)
            z = cog(ig,3)
            i1 = ip(i,1)
            i2 = ip(i,2)
            i3 = ip(i,3)
            p(i,ig) = plagr(i1,2,r,x)*plagr(i2,2,r,y)*plagr(i2,2,r,z)
            dpl(i,ig,1) =
     #           plagrd1(i1,2,r,x)*plagr(i2,2,r,y)*plagr(i3,2,r,z)
            dpl(i,ig,2) =
     #           plagr(i1,2,r,x)*plagrd1(i2,2,r,y)*plagr(i3,2,r,z)
            dpl(i,ig,3) =
     #           plagr(i1,2,r,x)*plagr(i2,2,r,y)*plagrd1(i3,2,r,z)
            dpl2(i,ig,1,1) = plagrd2(i1,2,r,x)*plagr(i2,2,r,y)*
     *           plagr(i3,2,r,z)
            dpl2(i,ig,2,2) = plagr(i1,2,r,x)*plagrd2(i2,2,r,y)*
     *           plagr(i3,2,r,z)
            dpl2(i,ig,3,3) = plagr(i1,2,r,x)*plagr(i2,2,r,y)*
     *           plagrd2(i3,2,r,z)
            dpl2(i,ig,1,2) = plagrd1(i1,2,r,x)*plagrd1(i2,2,r,y)*
     *           plagr(i3,2,r,z)
            dpl2(i,ig,2,1) = dpl2(i,ig,1,2)
            dpl2(i,ig,1,3) = plagrd1(i1,2,r,x)*plagr(i2,2,r,y)*
     *           plagrd1(i3,2,r,z) 
            dpl2(i,ig,3,1) = dpl2(i,ig,1,3)
            dpl2(i,ig,2,3) = plagr(i1,2,r,x)*plagrd1(i2,2,r,y)*
     *           plagrd1(i3,2,r,z) 
            dpl2(i,ig,3,2) = dpl2(i,ig,2,3)
c
         end do
      end do
c
      return
      end

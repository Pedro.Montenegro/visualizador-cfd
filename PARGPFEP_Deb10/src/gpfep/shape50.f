c -----------------------------------------------------------------------------
      subroutine shape50 (ngau,maxgau,maxnpe,p,dpl,cog)
c -----------------------------------------------------------------------------
c evaluates the shape functions and local derivatives at the gaussian points
c for the 4-node linear tetrahedron.
c
      implicit real*8 (a-h,o-z)
c
      dimension p(maxnpe,maxgau), dpl(maxnpe,maxgau,3), cog(maxgau,3) 
c
      do i = 1,ngau
c
         xgp = cog(i,1)
         ygp = cog(i,2)
         zgp = cog(i,3)
c
         p(1,i) = 1.0D+00 - xgp - ygp - zgp
         p(2,i) = xgp
         p(3,i) = ygp
         p(4,i) = zgp
c
         dpl(1,i,1) = -1.0D+00
         dpl(1,i,2) = -1.0D+00
         dpl(1,i,3) = -1.0D+00
         dpl(2,i,1) = 1.0D+00
         dpl(2,i,2) = 0.0D+00
         dpl(2,i,3) = 0.0D+00
         dpl(3,i,1) = 0.0D+00
         dpl(3,i,2) = 1.0D+00
         dpl(3,i,3) = 0.0D+00
         dpl(4,i,1) = 0.0D+00
         dpl(4,i,2) = 0.0D+00
         dpl(4,i,3) = 1.0D+00
c
      end do
      return
      end

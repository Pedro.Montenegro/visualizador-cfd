c -----------------------------------------------------------------------------
      subroutine shape52 (ngau,maxgau,maxnpe,p,dpl,dpl2,cog)
c -----------------------------------------------------------------------------
c evaluates the shape functions and local derivatives at the gaussian points
c for the 10-node quadratic tetrahedron.
c Numbering as in Hughes, The Finite Element Method, pag. 171.
c
      implicit real*8 (a-h,o-z)
c
      dimension p(maxnpe,maxgau), dpl(maxnpe,maxgau,3), 
     *     dpl2(maxnpe,maxgau,3,3), cog(maxgau,3)
      dimension ip(10,3), r(3)
      data ip/3,1,1,1,2,1,1,2,2,1,1,3,1,1,2,2,1,1,1,2,1,1,3,1,1,2,
     *     2,1,2,1/, r/0.,0.5,1./
c
      write(6,*)'SHAPE52. this element not implemented'
      stop
c
c the following does not work.  do as in shape32.
      do i = 1, 10
         do ig = 1, ngau
c
            i1 = ip(i,1)
            i2 = ip(i,2)
            i3 = ip(i,3)
            i4 = 6 - i1 - i2 - i3
            g1 = cog(ig,1)
            g2 = cog(ig,2)
            g3 = cog(ig,3)
            g4 = 1.0D+00 - g1 - g2 - g3
c
            p(i,ig) = plagr(i1,i1-1,r,g1)*plagr(i2,i2-1,r,g2)*
     *           plagr(i3,i3-1,r,g3)*plagr(i4,i4-1,r,g4)
c
            dpl(i,ig,1) = plagrd1(i1,i1-1,r,g1)*plagr(i2,i2-1,r,g2)*
     *           plagr(i3,i3-1,r,g3)*plagr(i4,i4-1,r,g4) 
            dpl(i,ig,2) = plagr(i1,i1-1,r,g1)*plagrd1(i2,i2-1,r,g2)*
     *           plagr(i3,i3-1,r,g3)*plagr(i4,i4-1,r,g4) 
            dpl(i,ig,3) = plagr(i1,i1-1,r,g1)*plagr(i2,i2-1,r,g2)*
     *           plagrd1(i3,i3-1,r,g3)*plagr(i4,i4-1,r,g4) 
c
            dpl2(i,ig,1,1) = plagrd2(i1,i1-1,r,g1)*plagr(i2,i2-1,r,g2)*
     *           plagr(i3,i3-1,r,g3)*plagr(i4,i4-1,r,g4) 
            dpl2(i,ig,2,2) = plagr(i1,i1-1,r,g1)*plagrd2(i2,i2-1,r,g2)*
     *           plagr(i3,i3-1,r,g3)*plagr(i4,i4-1,r,g4) 
            dpl2(i,ig,3,3) = plagr(i1,i1-1,r,g1)*plagr(i2,i2-1,r,g2)*
     *           plagrd2(i3,i3-1,r,g3)*plagr(i4,i4-1,r,g4) 
            dpl2(i,ig,1,2) =plagrd1(i1,i1-1,r,g1)*plagrd1(i2,i2-1,r,g2)*
     *           plagr(i3,i3-1,r,g3)*plagr(i4,i4-1,r,g4) 
            dpl2(i,ig,2,1) = dpl2(i,ig,1,2)
            dpl2(i,ig,1,3) = plagrd1(i1,i1-1,r,g1)*plagr(i2,i2-1,r,g2)*
     *           plagrd1(i3,i3-1,r,g3)*plagr(i4,i4-1,r,g4) 
            dpl2(i,ig,3,1) = dpl2(i,ig,1,3)
            dpl2(i,ig,2,3) = plagr(i1,i1-1,r,g1)*plagrd1(i2,i2-1,r,g2)*
     *           plagrd1(i3,i3-1,r,g3)*plagr(i4,i4-1,r,g4) 
            dpl2(i,ig,3,2) = dpl2(i,ig,2,3)
         end do
      end do
c
      return
      end

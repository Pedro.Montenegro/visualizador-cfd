c -----------------------------------------------------------------------------
      subroutine shapef (cog,p,dpl,dpl2,ngau,maxgau,maxnpe,ktyp)
c -----------------------------------------------------------------------------
c evaluates the shape functions and local derivatives at the gaussian points.
c     
      dimension cog(maxgau,3), p(maxnpe,maxgau), dpl(maxnpe,maxgau,3),
     *     dpl2(maxnpe,maxgau,3,3)
c     
      if (ktyp.eq.10) then
c     
c     2-node segment, linear interpolation.
c     
         call shape10(ngau,maxgau,maxnpe,p,dpl,cog)
c     
      else if (ktyp.eq.12) then
c     
c     3-node segment, quadratic interpolation.
c     
         call shape12 (ngau,maxgau,maxnpe,p,dpl,dpl2,cog)
c     
      else if (ktyp.eq.13) then
c     
c     4-node segment, cubic lagrangian interpolation.
c     
         call shape13 (ngau,maxgau,maxnpe,p,dpl,dpl2,cog)
c     
      else if (ktyp.eq.20) then
c     
c     4-node rectangle, bilinear interpolation
c     
         call shape20 (ngau,maxgau,maxnpe,p,dpl,cog)
c     
      else if (ktyp.eq.22) then
c     
c     9-node quadrilateral, biquadratic interpolation
c     
         call shape22 (ngau,maxgau,maxnpe,p,dpl,dpl2,cog)
c     
      else if (ktyp.eq.30) then
c     
c     3-node triangle, linear interpolation.
c     
         call shape30 (ngau,maxgau,maxnpe,p,dpl,cog)
c     
      else if (ktyp.eq.32) then
c     
c     6-node triangle, quadratic interpolation.
c     
         call shape32 (ngau,maxgau,maxnpe,p,dpl,dpl2,cog)
c     
      else if (ktyp.eq.40) then
c     
c     8-node hexahedron, trilinear interpolation
c     
         call shape40 (ngau,maxgau,maxnpe,p,dpl,cog)
c     
      else if (ktyp.eq.42) then
c     
c     27-node hexahedron, triquadratic interpolation.
c     
         call shape42 (ngau,maxgau,maxnpe,p,dpl,dpl2,cog)
c     
      else if (ktyp.eq.50) then
c     
c     4-node tetrahedron, linear interpolation.
c     
         call shape50(ngau,maxgau,maxnpe,p,dpl,cog)
c     
      else if (ktyp.eq.52) then
c     
c     10-node tetrahedron, quadratic interpolation.
c     
         call shape52 (ngau,maxgau,maxnpe,p,dpl,dpl2,cog)
c     
      else
c     
         write(*,*)' Error at shapef: interpolation type not defined:',
     *        ktyp
         stop
c     
      end if
c     
      return
      end

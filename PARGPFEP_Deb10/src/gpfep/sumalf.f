      SUBROUTINE SUMALF (N,A,B,ALPH,C)
      REAL*8 A(1),B(1),C(1),ALPH
      DO I=1,N
         C(I) = A(I) + ALPH * B(I)
      END DO
      RETURN
      END

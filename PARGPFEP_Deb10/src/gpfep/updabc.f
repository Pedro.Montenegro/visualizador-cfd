      SUBROUTINE UPDABC (N,NFIELD,LDIR,VDIR,IINV,PARCON,INDXXX,X)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION LDIR(1),VDIR(1),IINV(1),INDXXX(1),
     *          X(1)
      DO I=1,N
C IDENTIFIES FIELD OF UNKNOWN
         IORIG = IINV(I)
         KFIELD = 0
         DO IX=2,NFIELD+1
            IF (IORIG.LE.INDXXX(IX).AND.KFIELD.EQ.0) KFIELD = IX-1
         END DO
         IF (KFIELD.EQ.0) STOP ' UPDABC. KFIELD=0 '
         IF (LDIR(I).NE.0) THEN
            IF (KFIELD.EQ.1) THEN
               X(I) = VDIR(I)
            ELSE
               X(I) = VDIR(I)
            END IF
         END IF
      END DO
      RETURN
      END

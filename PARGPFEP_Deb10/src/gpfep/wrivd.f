      SUBROUTINE WRIVD (LU,N,X)
      REAL*8 X(1)
      DO I=1,N
         IF (ABS(X(I)).LT.1.1D-99) X(I)=0
      END DO
      WRITE (LU,111) (X(I),I=1,N)
      RETURN
  111 FORMAT (' ',E19.11,' ',E19.11,' ',E19.11)
      END

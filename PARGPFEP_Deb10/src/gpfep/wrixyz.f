      SUBROUTINE WRIXYZ (LU,NDIM,NOD,COOR)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION COOR(1)
      DO IDIM=1,NDIM
         WRITE (LU,111)(COOR(I+(IDIM-1)*NOD),I=1,NOD)
      END DO
      RETURN
  111 FORMAT (' ',E14.8,' ',E14.8,' ',E14.8,' ',E14.8)
      END

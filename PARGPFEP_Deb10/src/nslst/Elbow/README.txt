CASO DE TUBERIA con 2 entradas y una salida
Parametros del fluido
rho = 1000
mu = 10
nu = 0.01

BC
entrada inferior vx=0 vy =3
entrada izquierda vx=1 vy=0

Parametros de calculo en fortran
Longitudes en metros
Longitud caracteristica grilla = 0.5
DeltaT=0.1
T_final= 75 seg

Para visualizar la solución abrir res.case con paraview

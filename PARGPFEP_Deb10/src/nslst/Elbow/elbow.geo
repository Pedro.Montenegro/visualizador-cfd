//Geometria del caso tutorial/incompressible/icofoam/elbow de OpenFoam

//------------------------------------------------------------------------
//PUNTOS
lc=0.5;
//Puntos cara atras. Se recorre la cañeria en sentido antihorario
Point(1) = {0,0,0,lc};  // centro de la circunferencia
Point(2) = {32,0,0,lc}; 
Point(3) = {32,32,0,lc};
Point(4) = {16,32,0,lc};
Point(5) = {16,0,0,lc};
Point(6) = {0,-16,0,lc};
Point(7) = {-32,-16,0,lc};
Point(8) = {-32,-32,0,lc};
Point(9) = {0,-32,0,lc};
Point(10) = {20.5,-24.5,0,lc};
Point(11) = {20.5,-36.5,0,lc};
Point(12) = {24.5,-36.5,0,lc};
Point(13) = {24.5,-20.5,0,lc};

//-------------------------------------------------------------------------
//LINEAS Y CURVAS

//Lineas cara atras //contruccion en sentido antihorario

Line(1) = {2,3};
Line(2) = {3,4};
Line(3) = {4,5};
Circle(4) = {5,1,6};
Line(5) = {6,7};
Line(6) = {7,8};
Line(7) = {8,9};
Circle(8) = {9,1,10};
Line(9) = {10,11};
Line(10) = {11,12};
Line(11) = {12,13};
Circle(12) = {13,1,2};

//---------------------------------------------------------------------------
//SUPERFICIES 

//se definen las normales hacia afuera

//superficie atras
Curve Loop(13)={1,2,3,4,5,6,7,8,9,10,11,12};
Plane Surface(14)={13};


Mesh(2);



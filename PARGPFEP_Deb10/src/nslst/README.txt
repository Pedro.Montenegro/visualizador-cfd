Navier-Stokes, con level-set y conducción/convección térmica (+Boussinesq).

Pasos fraccionados: (total 9)
1-Level-set function
2-k (for k-epsilon turbulence model)
3-epsilon (idem)
4-p_,x (pressure gradient projection, x component)
5-p_,y (pressure gradient projection, y component)
6-p_,z (pressure gradient projection, z component)
7-nu_t (turbulent viscosity)
8-Temperature
9-Navier-Stokes

Variables por nodo: (1D: 8, 2D: 10, 3D: 12)
1-V_x [V_y [V_z]] (velocity)
2-p (pressure)
3-p_,x [p_,y [p_,z]] (projected pressure gradient)
4-lset (level-set function)
5-k (for k-epsilon turbulence model)
6-epsilon (idem)
7-nu_t (turbulent viscosity)
8-Temperature

Integrales en paso de Navier-Stokes:
 1-Volume          (dj)
 2-Kinetic energy  (0.5 rho u_i u_i dj)
 3-Momentum_x      (rho u_x dj)
 4-[Momentum_y]    (rho u_y dj)
 5-[Momentum_z]    (rho u_z dj)
 6-Pressure^2      (p^2 dj)
 7-Fluid volume    (fvol dj)
 8-u_x fluctuation (rho u_x^2 dj)
 9-u_y fluctuation (rho u_y^2 dj)
10-u_z fluctuation (rho u_z^2 dj)
11-Dissipation     ((u_i,j+u_j,i) u_i,j dj)
12-Temperature     (T dj)
13-dT/dy           (dT/dy dj)
14-DT/Dt - f

Integrales en paso de Temperatura
 1-Surface         (dj)
 2-Temperature     (T dj)
 3-Volume          (dj)
 4-Temperature     (T dj)

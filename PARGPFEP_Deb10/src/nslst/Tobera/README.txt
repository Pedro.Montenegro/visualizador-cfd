Tobera

Radio entrada = 0.5 m
Radio salida =0.25 m
Largo = 1 m

Malla

Tobera_malla_fina : lc= 0.02
Tobera_malla_gruesa : lc= 0.05

Fluido

rho = 1000 kg/m3
mu = 10 kg/(m*s)


Archivo para generar geomtria .msh : Tobera.geo (abrir con gmsh y exportar como archivo.msh, ASCII 2, sin marcar casillas)

Queremos un Re = 500 a la salida

Re=0.5*u/0.01 =500
u_medio_salida=10
Caudal salida = 10 * pi * 0.25^2 =5*pi/8 =1.963495408 [m3/s]
Fijamos la velocidad de entrada para el mismo caudal

Caudal entrada = 5*pi/8 = u_entrada * pi * 0.5^2
u_entrada = 2.5 m/s 

Condiciones de borde:
u_entrada = 2.5 (homogeneo)
u_en_paredes = 0

Condicion_inicial:
u = 0 

Spam de tiempo: 0 a 2 segundos
Fijamos un courant maximo =0.8

Co_malla_fina = u_salida * delta_t_malla_fina / lc_malla_fina
Co_malla_gruesa =u_salida * delta_t_malla_gruesa / lc_malla_gruesa

delta_t_malla_fina = 1.6 e-3
delta_t_malla_gruesa = 4.0 e-3

Los archivos de salida se escriben cada 0.2 segundos

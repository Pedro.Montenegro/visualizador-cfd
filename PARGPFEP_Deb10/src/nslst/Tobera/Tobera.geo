 
//Tobera 3D

//------------------------------------------------------------------------
//PUNTOS
lc=0.02; //Tamaño grilla

//puntos circulo izquierda
Point(1) = {0,0,0,lc};  // centro de la circunferencia
Point(2) = {0.5,0,0,lc}; 
Point(3) = {0,0.5,0,lc};
Point(4) = {-0.5,0,0,lc};
Point(5) = {0,-0.5,0,lc};

//Puntos circulo derecha
Point(6) = {0,0,1,lc};
Point(7) = {0.25,0,1,lc};
Point(8) = {0,0.25,1,lc};
Point(9) = {-0.25,0,1,lc};
Point(10) = {0,-0.25,1,lc};


//-------------------------------------------------------------------------
//LINEAS Y CURVAS


//lineas circulo izq
Circle(1) = {2,1,3};
Circle(2) = {3,1,4};
Circle(3) = {4,1,5};
Circle(4) = {5,1,2};

//lineas circulo der
Circle(5) = {7,6,8};
Circle(6) = {8,6,9};
Circle(7) = {9,6,10};
Circle(8) = {10,6,7};

//lineas conección entre circulos
//siempre van de izq a der
Line(9) = {2,7};
Line(10) = {3,8};
Line(11) = {4,9};
Line(12) = {5,10};


//---------------------------------------------------------------------------
//SUPERFICIES 

//se definen las normales hacia afuera

//superficie circ izq (Entrada)
Curve Loop(13)={1,2,3,4};
Plane Surface(14)={-13}; 

//superficie circulo der (salida)
Curve Loop(15)={5,6,7,8};
Plane Surface(16)={15};

//lateral superior externa
Curve Loop(17)={-9,1,10,-5};
Ruled Surface(18)={17};

//lateral superior interna
Curve Loop(19)={-10,2,11,-6};
Ruled Surface(20)={19};

//lateral inferior interna
Curve Loop(21)={-11,3,12,-7};
Ruled Surface(22)={21};

//lateral inferior externa
Curve Loop(23)={-12,4,9,-8};
Ruled Surface(24)={23};

//--------------------------------------------------------------------------
//VOLUMEN

Surface Loop(25)={14,16,18,20,22,24};
Volume(26)={25};

Mesh.Algorithm3D = 1;
Mesh (3);

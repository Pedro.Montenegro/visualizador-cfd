C-----------------------------------------------------------------------
      SUBROUTINE BCFEMCO(NOD, NFIELD, LDIR, VDIR, NVAR, KCOMP, 
     *     PARCON, PARMAT, PARNUM, PARGEN, VA, COGEO, NSD)
C-----------------------------------------------------------------------
C NOD: Number of local nodes
C NFIELD: Number of scalars in each node
C LDIR: Array with the kind of BC in each node
C VDIR: Array with the value of the BC in each node
C NVAR: Number of different fields (not scalars) in each node
C KCOMP: Number of components of each field.(SUM(I,1,NVAR)KCOMP(I)=NFIELD)
C VA: Array with tho local unknowns values in the last time step
C COGEO: Coordinates of the nodes
C NSD: number of spatial dimensions
c
#undef  __SFUNC__
#define __SFUNC__ "updatebc  "
c
      IMPLICIT REAL*8 (A-H,O-Z)
!      REAL*8    inercia
C-----------------------------------------------------------------------
C LOOK IF THESE VALUES AGREE WITH YOUR PROBLEM
C-----------------------------------------------------------------------
      parameter (maxcomp_=10, maxnfield_=18)
C-----------------------------------------------------------------------
      DIMENSION LDIR(*), VDIR(*), KCOMP(*), VA(*), PARMAT(*), PARNUM(*)
      DIMENSION COGEO(*), COOLOC(3), PARGEN(*)
      DIMENSION VARA(maxcomp_, maxnfield_) ! It''s oversized
C-----------------------------------------------------------------------
C   Define here your own variables
!      dimension force(3)
C-----------------------------------------------------------------------
      tmax = 120.
C-----------------------------------------------------------------------
      IUNK=1
      DO I = 1, NOD
C BUILDING VARA
         IFIELD = 1
         DO IVAR = 1, NVAR
            DO ICO = 1, KCOMP(IVAR)
               VARA(ICO,IVAR) = VA((I-1)*NFIELD+IFIELD)
               IFIELD = IFIELD + 1
            END DO
         END DO
C BUILDING COOLOC
         DO J = 1, NSD
            COOLOC(J) = COGEO(NSD*(I-1)+J)
         END DO
C-----------------------------------------------------------------------
C INSERT HERE ANY CALCULUS BEFORE THE LOOP OVER THE UNKNOWNS
C-----------------------------------------------------------------------
c         rr = 0
c         do isd = 1, nsd
c            rr = rr + cooloc(isd)**2
c         end do
C-----------------------------------------------------------------------
C LOOP
         DO IVAR = 1, NVAR
            DO ICOMP = 1, KCOMP(IVAR)
               IF (LDIR(IUNK) .NE. 0) THEN
C-----------------------------------------------------------------------
C PROGRAMMABLE MODULE
C-----------------------------------------------------------------------
C
                  rho = parmat(1)
                  rmul = parmat(2)
                  yodelta = parmat(20)
                  rnul = rmul/rho
                  cmu = 0.09
                  if (pargen(1) .eq. 1) then      ! k-epsilon turbulence model
                     IF (IVAR .EQ. 5 .and. ldir(iunk) .gt. 10) THEN
                        ut = 0.
                        do ico=1,kcomp(1)
                           ut = ut + vara(ico,1)**2
                        end do
                        ut = sqrt(ut)
c if rough set xnul to -rnul, the ks must be given in g
                        xnul = rnul
                        g    = 0.d0
c                     xnul = -rnul
c                     g    = parmat(14)
                        call derivut(xnul,ut,g,dgdut,yodelta)
                        ustar = sqrt(g)
                        VDIR(IUNK)  = ustar**2/sqrt(cmu)
                     else if (IVAR .eq. 6 .and. ldir(iunk) .gt. 10) then
                        ut = 0.
                        do ico=1,kcomp(1)
                           ut = ut + vara(ico,1)**2
                        end do
                        ut = sqrt(ut)
c if rough set xnul to -rnul, the ks must be given in g
                        xnul = rnul
                        g    = 0.d0
c                     xnul = -rnul
c                     g    = parmat(14)
                        call derivut(rnul,ut,g,dgdut,yodelta)
                        ustar = sqrt(g)
                        VDIR(IUNK)  = ustar**3/yodelta/0.41
                     end if
                  end if        ! end of k-epsilon turbulence model
c verify that this is OK
!                  if (ivar.eq.4) then
!                     rlevel  = parnum(12)
!                     if (rlevel.lt.0) rlevel = -rlevel
!                     dismax  = parnum(13)
!                     hinflow = parnum(14)
!                     phi = hinflow - cooloc(nsd)+rlevel
!                     if (phi.lt.rlevel-dismax) phi = rlevel-dismax
!                     if (phi.gt.rlevel+dismax) phi = rlevel+dismax
!c                     write (6,*) ' y , phi = ',cooloc(2),phi
!                     vdir(iunk) = phi
!                  end if
                  if (ivar .eq. 8 .and. parmat(25) .eq. 19.0) then
c rampa temporal
                     if (parcon .lt. 50) then
                        VDIR(IUNK) = parcon * 20.0/50.0
                     else
                        VDIR(IUNK) = 20.0
                     end if
                  end if
C
C----------------------------------------------------------------------------
C     END OF PROGRAMMABLE MODULE
C----------------------------------------------------------------------------
               END IF
               IUNK = IUNK + 1      
            END DO
         END DO
      END DO
      RETURN
      END

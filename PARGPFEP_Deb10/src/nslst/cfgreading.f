C
C      WRITE (6,*) ' READING OF GENERAL DATA: Starting...'
      OPEN  (1, FILE=FILEGD, STATUS='OLD')
      OPEN (16,FILE='cfgread.msg',STATUS='UNKNOWN')
C
      IF (IFINDKEY (1, 'TITLE') .NE. 0) THEN
         READ  (1,'(A80)') TITLE
      ELSE
         TITLE = 'Untitled gpfep problem'
      END IF
      WRITE (16,'(A80)') TITLE
C
      IF (IFINDKEY (1, 'ELEMENTS') .NE. 0) THEN
         READ (1, *) NEL
      ELSE
         NEL = 0
      END IF
      WRITE (16,*) '*ELEMENTS '
      write (16,*) NEL
C
      IF (IFINDKEY (1, 'ELEMENTS_IN_SURFACE_MESH') .NE. 0) THEN
         READ (1, *) NELSUR
      ELSE
         NELSUR = 0
      END IF
      write (16,*) '*ELEMENTS_IN_SURFACE_MESH'
      write (16,*) NELSUR
C
      IF (IFINDKEY (1, 'SPACE_DIMENSIONS') .NE. 0) THEN
         READ (1, *) NDIM
      ELSE
         STOP 'SPACE_DIMENSIONS not found'
      END IF
      WRITE (16,*) '*SPACE_DIMENSIONS'
      WRITE (16,*) NDIM
C
      if (ndim .lt. 1 .or. ndim .gt. 3)
     *   stop 'unrecognizable space dimensions (use: 0 < ndim < 4)'
C
      IF (IFINDKEY (1, 'SYMMETRY_INDICATOR') .NE. 0) THEN
         READ (1, *) JSIM
      ELSE
         JSIM = 0
      END IF
      WRITE (16,*) '*SYMMETRY_INDICATOR'
      WRITE (16,*) JSIM
      IF (JSIM .GT. NDIM) STOP 'WRONG SYMMETRY INDICATOR'
C
      IF (IFINDKEY (1,
     *     'DEGREE_OF_POLYNOMIAL_INTEGRATED_EXACTLY') .NE. 0) THEN
         READ (1, *) ITGDEG
      ELSE
         ITGDEG = 2
      END IF
      WRITE (16,*) '*DEGREE_OF_POLYNOMIAL_INTEGRATED_EXACTLY'
      WRITE (16,*) ITGDEG
C
      IF (IFINDKEY (1, 'INTEGRATION_RULE') .NE. 0) THEN
         READ (1, *) ITGTYP
      ELSE
         ITGTYP = 1
      END IF
      WRITE (16,*) '*INTEGRATION_RULE'
      WRITE (16,*) ITGTYP
C
      IF (IFINDKEY (1, 'NUMBER_OF_UNKNOWN_FIELDS') .NE. 0) THEN
         READ (1, *) NFIELD
      ELSE
         NFIELD = 1
      END IF
      WRITE (16, *) '*NUMBER_OF_UNKNOWN_FIELDS'
      WRITE (16,*) NFIELD
C
      IF (NFIELD .GT. MAX_NUMB_FIELDS) STOP 'EXCESSIVE NUMBER OF FIELDS'
C
      IF (IFINDKEY (1,
     *     'INTERPOLATION_AND_COMPONENTS_OF_EACH_FIELD') .NE. 0) THEN
         DO IFIELD = 1,NFIELD
            READ (1,*) KSHAP(IFIELD),KCOMP(IFIELD)
         END DO
      ELSE
         STOP 'INTERPOLATION_AND_COMPONENTS_OF_EACH_FIELD not found'
      END IF
      WRITE (16,*) '*INTERPOLATION_AND_COMPONENTS_OF_EACH_FIELD'
         DO IFIELD = 1,NFIELD
            WRITE (16,*) KSHAP(IFIELD),KCOMP(IFIELD)
         END DO
C
      IF (IFINDKEY (1, 'NUMBER_OF_MESHES_TO_READ') .NE. 0) THEN
         READ (1, *) NMESH
      ELSE
         NMESH = 1
      END IF
      WRITE (16,*) '*NUMBER_OF_MESHES_TO_READ'
      WRITE (16,*) NMESH
      IF (NMESH .GT. MAX_NUMB_MESHES) STOP 'EXCESSIVE NUMBER OF MESHES'
C
      IF (IFINDKEY (1, 'FILES_CONTAINING_MESHES') .NE. 0) THEN
         DO IMESH=1,NMESH
            READ (1,'(A80)') FILEMS(IMESH)
         END DO
      ELSE
         STOP 'FILES_CONTAINING_MESHES not found'
      END IF
      WRITE (16,*) '*FILES_CONTAINING_MESHES'
      WRITE (16,'(A80)') FILEMS(1)
C
      IF (IFINDKEY (1,'FILES_CONTAINING_SURFACE_MESHES') .NE. 0) THEN
         DO IMESH=1,NMESH
            READ (1,'(A80)') FILEMSSUR(IMESH)
         END DO
         iswsur = 1
      ELSE
         iswsur = 0
         FILEMSSUR(1)='NONE'
C         STOP 'FILES_CONTAINING_SURFACE_MESHES not found'
      END IF
      IF (ISWSUR.EQ.1) THEN
       WRITE (16,*) '*FILES_CONTAINING_SURFACE_MESHES'
       WRITE (16,'(A80)') FILEMSSUR(1)
      END IF
C
      IF (IFINDKEY (1,
     *  'ELEMENT_TYPE_AND_NODES_PER_ELEMENT_FOR_EACH_MESH') .NE. 0) THEN
         DO IMESH = 1,NMESH
            READ (1, *) IELTYP(IMESH),NNODE(IMESH)
         END DO
      ELSE
         IF (NMESH .NE. 1) STOP
     *    'ELEMENT_TYPE_AND_NODES_PER_ELEMENT_FOR_EACH_MESH not found'
         WRITE (16, *) 'LINEAR ELEMENTS'
         IF (NDIM .EQ. 1) THEN
            IELTYP(1) = 1
            NNODE(1) = 2
         ELSE IF (NDIM .EQ. 2) THEN
            IELTYP(1) = 3
            NNODE(1) = 3
         ELSE IF (NDIM .EQ. 3) THEN
            IELTYP(1) = 5
            NNODE(1) = 4
         END IF
      END IF
      WRITE (16,*) '*ELEMENT_TYPE_AND_NODES_PER_ELEMENT_FOR_EACH_MESH'
      WRITE (16,*) IELTYP(1),NNODE(1)
C
      IF (IFINDKEY (1,'INTERPOLATION_CODE_FOR_EACH_MESH') .NE. 0) THEN
         DO IMESH = 1,NMESH
            READ (1,*) MESINT(IMESH)
         END DO
      ELSE
         IF (NMESH .NE. 1) 
     *        STOP 'INTERPOLATION_CODE_FOR_EACH_MESH not found'
         WRITE (6, *) 'LINEAR INTERPOLATION'
         IF (NDIM .EQ. 1) THEN
            MESINT(1) = 10
         ELSE IF (NDIM .EQ. 2) THEN
            MESINT(1) = 30
         ELSE IF (NDIM .EQ. 3) THEN
            MESINT(1) = 50
         END IF
      END IF
      WRITE (16,*) '*INTERPOLATION_CODE_FOR_EACH_MESH'
      WRITE (16,*) MESINT(1)
      ITPGEO = MESINT(1)
C
      IF (IFINDKEY (1,'MESH_CORRESPONDING_TO_EACH_FIELD') .NE. 0) THEN
         DO IFIELD = 1,NFIELD
            READ (1,*) KFTYP(IFIELD)
         END DO
      ELSE
         IF (NMESH .NE. 1)
     *        STOP 'MESH_CORRESPONDING_TO_EACH_FIELD not found'
         DO IFIELD = 1,NFIELD
            KFTYP(IFIELD) = 1
         END DO
      END IF
      WRITE (16,*) '*MESH_CORRESPONDING_TO_EACH_FIELD'
      DO IFIELD=1,NFIELD
         WRITE (16,*) KFTYP(IFIELD)
      END DO
C THE PERMUTATION VECTORS PROVIDE THE RENUMBERING OF UNKNOWNS
      IF (IFINDKEY (1,'PERMUTATION_VECTORS_FILE') .NE. 0) THEN
         READ (1, '(A80)') FILPER
         IF (FILPER(1:4).EQ.'NONE') FILPER=' NONE'
      ELSE
         FILPER=' NONE'
      END IF
      WRITE (16,*) '*PERMUTATION_VECTORS_FILE'
      WRITE (16,'(A80)') FILPER
C
      IF (IFINDKEY (1,'INITIAL-GUESS_FILE') .NE. 0) THEN
         READ (1, '(A80)') FILEGS
         IF (FILEGS(1:4).EQ.'NONE') FILEGS=' NONE'
      ELSE
         FILEGS=' NONE'
      END IF
      WRITE (16,*) '*INITIAL-GUESS_FILE'
      WRITE (16,'(A80)') FILEGS
C
      IF (IFINDKEY (1,'FILE_WITH_DATA_TO_CONSTRUCT_B.C.') .NE. 0) THEN
         READ (1, '(A80)') FILBCC
      ELSE
         FILBCC='bcc.dat'
      END IF
      WRITE (16,*) '*FILE_WITH_DATA_TO_CONSTRUCT_B.C.'
      WRITE (16,'(A80)') FILBCC
C
      IF (IFINDKEY (1,'BOUNDARY-CONDITIONS_FILE') .NE. 0) THEN
         READ (1, '(A80)') FILEBC
      ELSE
         FILEBC='gp.bco'
      END IF
      WRITE (16,*) '*BOUNDARY-CONDITIONS_FILE'
      WRITE (16,'(A80)') FILEBC
C
      IF (IFINDKEY (1,'OBLIQUE_NODES_SWITCH') .NE. 0) THEN
         READ (1, *) ISWNOB
      ELSE
         ISWNOB = 0
      END IF
      WRITE (16,*) '*OBLIQUE_NODES_SWITCH'
      WRITE (16,*) ISWNOB
C
      IF (IFINDKEY (1,'NUMBER_OF_OBLIQUE_NODES') .NE. 0) THEN
         READ (1, *) (NUMNOB(IMESH),IMESH=1,NMESH)
      ELSE
         if (iswnob.ne.0) STOP 'NUMBER_OF_OBLIQUE_NODES REQUIRED'
      END IF
      WRITE (16,*) '*NUMBER_OF_OBLIQUE_NODES'
      WRITE (16,*) NUMNOB(1)
C
      IF (IFINDKEY (1,'PERIODIC_NODES_SWITCH') .NE. 0) THEN
         READ (1, *) ISWPERIO
      ELSE
         ISWPERIO = 0
      END IF
C      WRITE (6,*) '*PERIODIC_NODES_SWITCH (ATTENTION: NOT IMPLEMENTED!)'
C      WRITE (6,*) ISWPERIO
C
      IF (IFINDKEY (1,'STATISTICS_FILE') .NE. 0) THEN
         READ (1, '(A80)') FILEST
      ELSE
         FILEST='NONE'
      END IF
C      WRITE (6,*) '*STATISTICS_FILE'
C      WRITE (6,'(A80)') FILEST
C
      IF (IFINDKEY (1,'RESTART_FILE') .NE. 0) THEN
         READ (1, '(A80)') FILREE
      ELSE
         FILREE='gp.res'
      END IF
      WRITE (16,*) '*RESTART_FILE'
      WRITE (16,'(A80)') FILREE
C
      IF (IFINDKEY (1,'INITIAL_CONTINUATION_PARAMETER') .NE. 0) THEN
         READ (1, *) PARCI
      ELSE
         PARCI = 0.D0
      END IF
      WRITE (16,*) '*INITIAL_CONTINUATION_PARAMETER'
      WRITE (16,*) PARCI
C
      IF (IFINDKEY (1,'FINAL_CONTINUATION_PARAMETER') .NE. 0) THEN
         READ (1, *) PARCF
      ELSE
         WRITE (6,*) '  Warning: No final continuation parameter given'
         PARCF = 9999999.
      END IF
      WRITE (16,*) '*FINAL_CONTINUATION_PARAMETER'
      WRITE (16,*) PARCF
C
      IF (IFINDKEY (1,'NUMBER_OF_CONTINUATION_STEPS') .NE. 0) THEN
         READ (1, *) NCONT
      ELSE
         NCONT = 0
      END IF
      WRITE (16,*) '*NUMBER_OF_CONTINUATION_STEPS'
      WRITE (16,*) NCONT
C
      IF (IFINDKEY (1,'MAXIMUM_NONLINEAR_ITERATIONS') .NE. 0) THEN
         READ (1, *) NITMAX
      ELSE
         NITMAX = 6
      END IF
C      WRITE (6,*) '*MAXIMUM_NONLINEAR_ITERATIONS'
C      WRITE (6,*) NITMAX
C
      IF (IFINDKEY (1,'TOLERANCE_NONLINEAR') .NE. 0) THEN
         READ (1, *) TOLNLI
      ELSE
         TOLNLI = 1.0D-06
      END IF
C      WRITE (6,*) '*TOLERANCE_NONLINEAR'
C      WRITE (6,*) TOLNLI
C
      IF (IFINDKEY (1,'SWITCH_FOR_NONLINEAR_SOLVER') .NE. 0) THEN
         READ (1, *) ISWNLI
      ELSE
         ISWNLI = 0
      END IF
C      WRITE (6,*) '*SWITCH_FOR_NONLINEAR_SOLVER'
C      WRITE (6,*) ISWNLI
C
      IF (IFINDKEY (1,'MAXIMUM_LINE-SEARCH_ITERATIONS') .NE. 0) THEN
         READ (1, *) NITLS
      ELSE
         NITLS = 1
      END IF
C      WRITE (6,*) '*MAXIMUM_LINE-SEARCH_ITERATIONS'
C      WRITE (6,*) NITLS
C
      IF (IFINDKEY (1,'LINE-SEARCH_PARAMETER') .NE. 0) THEN
         READ (1, *) PARLS
      ELSE
         PARLS = 0.8
      END IF
C      WRITE (6,*) '*LINE-SEARCH_PARAMETER'
C      WRITE (6,*) PARLS
C
      IF (IFINDKEY (1,'LINEAR_SYSTEM_SOLVER') .NE. 0) THEN
         READ (1, '(A80)') SOLVER
      ELSE
         SOLVER = 'DIRECT'
      END IF
C
      IF (SOLVER(1:6).EQ.'ITERNS') THEN
C         WRITE (6,*) 'CGS SOLVER'
         PARNUM(30)=-1.D0
      END IF
C      WRITE(6,*) '*LINEAR_SYSTEM_SOLVER'
C      WRITE (6,'(A80)') SOLVER
      
      IF (IFINDKEY (1,'MAXIMUM_LINEAR_ITERATIONS') .NE. 0) THEN
         READ (1, *) NSOLV
      ELSE
         NSOLV = 500
      END IF
C      WRITE (6,*) '*MAXIMUM_LINEAR_ITERATIONS'
C      WRITE (6,*) NSOLV
C
      IF (IFINDKEY (1,'TOLERANCE_LINEAR') .NE. 0) THEN
         READ (1, *) TOLSOL
      ELSE
         TOLSOL = 1.0D-07
      END IF
C      WRITE (6,*) '*TOLERANCE_LINEAR'
C      WRITE (6,*) TOLSOL
C
      IF (IFINDKEY (1,'MATERIAL_PARAMETERS') .NE. 0) THEN
         READ (1, *) NPARM
      ELSE
         NPARM = 0
      END IF
      IF (NPARM .GT. MAX_PARAMS) STOP 'EXCESSIVE NUMBER OF MAT. PARAMS'
C
      DO I=1,NPARM
         READ (1,*) PARMAT(I)
      END DO
      WRITE (16,*) '*MATERIAL_PARAMETERS'
      WRITE (16,*) NPARM
      DO I=1,NPARM
         WRITE (16,*) PARMAT(I)
      END DO
      IF (IFINDKEY (1,'NUMERICAL_PARAMETERS') .NE. 0) THEN
         READ (1, *) NPARN
      ELSE
         NPARN = 0
      END IF
      IF (NPARN .GT. MAX_PARAMS) STOP 'EXCESSIVE NUMBER OF NUM. PARAMS'
C
      DO I=1,NPARN
         READ (1,*) PARNUM(I)
      END DO
      WRITE (16,*) '*NUMERICAL_PARAMETERS'
      WRITE (16,*) NPARN
      DO I=1,NPARN
         WRITE (16,*) PARNUM(I)
      END DO
C
      IF (IFINDKEY (1,'FIRST_OUTPUT') .NE. 0) THEN
         READ (1, *) KOUTI
      ELSE
         KOUTI = 1
      END IF
      WRITE (16,*) '*FIRST_OUTPUT'
      WRITE (16,*) KOUTI
C
      IF (IFINDKEY (1,'STEPS_BETWEEN_OUTPUTS') .NE. 0) THEN
         READ (1, *) KOUTD
      ELSE
         KOUTD = 1
      END IF
      WRITE (16,*) '*STEPS_BETWEEN_OUTPUTS'
      WRITE (16,*) KOUTD
C
      FRACSTEPS = MAX_FSTEPS
      DO I = 1, FRACSTEPS
         ISWFRAC(I) = 1
      END DO
      IF (IFINDKEY (1,'FRACTIONAL_STEPS_SWITCHES') .NE. 0) THEN
         READ (1,*) FRACSTEPS
         IF (FRACSTEPS .LE. MAX_FSTEPS) THEN
            DO I = 1, FRACSTEPS
               READ (1,*) ISWFRAC(I)
            END DO
         ELSE
            STOP 'EXCESSIVE NUMBER OF FRACT. STEPS'
         END IF
      END IF
      WRITE (16,*) '*FRACTIONAL_STEPS_SWITCHES'
      WRITE (16,*) FRACSTEPS
      DO I=1,FRACSTEPS
         WRITE (16,*) ISWFRAC(I)
      END DO
C
      CLOSE (1)
      close(16)
C      WRITE (6,*) ' READING OF GENERAL DATA: ...Complete.'



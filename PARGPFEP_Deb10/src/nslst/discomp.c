#include <stdlib.h>
#include "assembly.h"
#include "prof_utils.h"
#include "petsclog.h"

#define MAX_INTS_PROC   128000
#define MAX_INTS        512000
/* #define MAX_INTS_MEAS   1024 */
/* #define MAX_TOUCHED     32000 */
/* #define ROUND_ZERO      1.0E-5 */
/* #define NMAX 4 */

double dp2seg (double *coop, double *coos, int nsd);
double dp2tri (double *coop, double *coot, int nsd);
double dp2qua (double *coop, double *cooq, int nsd);
int periodic_corr(PetscScalar *coordaux, int nnpe, int nsd, double *rl);
void interdist2 (double *coords, int nsd, double *v, double level, 
		 double *dists, double *parvol);
double fracvol2_(double *coords, int *nsd, double *v, double *level, 
		 double *volume, int *nintp, double *intpoints, 
		 double *sizeint);
double parvol2(double *coords, int nsd, double *v, double level, 
	       double *volume, double *sizeint);
int edge_distance2 (double *coords, int nsd, double *d);
int shadow_distance2 (double *coords, int nsd, double *d);

/*------------------------------------------------------------------------
        Function discomp - Smooth an indicator function, by re-computation
	of the distance to a given contour line.
	Programmed by E.Dari, 2003-08-06

	This function is usually called from FORTRAN.
	Arguments:
	Sy: general data about mesh and partitioning.          (input)
	v:  last step solution (all fields)                    (input-output)
	nforig: field in v with the characteristic function.   (input)
	nfdist: field in v containing the computed distance.   (workspace)
	nfedist: field in v for saving computed edge distance. (input)
	newfc: field in v for saving computed distance.        (input)
	level: the level for tetermining phase.                (input)
	parnum, parmat, pargen: just in case.                  (input)
-----------------------------------------------------------------------------*/

#undef __SFUNC__
#define __SFUNC__ "discomp"

/*#define NOT_DEF 1.0e+30*/

void discomp_(systemdata **fSy, Vec *fv,
	      int *fnforig, int *fnfdist, int *fnfedist, double *flevel,
	      int *fnewfc,
	      double *parnum, double *parmat, double *pargen,
	      double *parcon)
{
  Vec v;

  MPI_Comm Comm;        /* MPI Communicator */
  int      ierr;        /* error checking variable */
  int      i;           /* loop counters */

  PetscScalar *coordaux; /* vector for passing the element nodes coordinates */
  PetscScalar *varscan;  /* pointer for scanning the v values */

  PetscScalar /* oldval,  */newval;
  PetscScalar *funcval, *distval, *nextval;
  PetscScalar max_distance;
  double *dvprop, dmassi, dmass, dmassoo, dmasso;
  double alfa, alfao, alfaoo, volele, vref, sizeint, delvol;
  int *nvprop;
  double level, *rl;
  double *volelei, totvolint, minval, maxval;
  double daux;
  /* int inolocs[4]; */

  int iel, /* ngr,  */inbe, k, inoloc, posfunc, j, nnpe;
  int nforig, nfdist, /* nfedist, nvar, maxcomp,  */nsd;
  int newfc;
  int /* locelem,  */totelem, intelem;
  int nlow, nhigh, changes;
  int nodloc, nfields;
  int ed_count, gd_count, sg_count;
  int changed_since_scatter, ch_glob, rank, sizecomm;
  int max_ed_count, max_gd_count;
  int *iele, last_changed, comp_dfc, italfa, swmass;
  PetscBool pt_flg;
  systemdata   *Sy;

  Vec distfield; /* Local vectors: temporary distance field */
  Vec dfcorr;
  PetscScalar *dfscan;
  PetscScalar *dfcscan;
  Vec lv, ldistf, ldfcorr;

  v  = *fv;
  Sy = *fSy;
  level = *flevel;

  nnpe    = Sy->Pa->nnpe;
  Comm    = Sy->Pa->Comm;
  MPI_Comm_rank(Comm, &rank);
  MPI_Comm_size(Comm, &sizecomm);
  nsd     = Sy->Pa->nsd;
  /* maxcomp = Sy->Sq->maxcomp; */
  /* nvar    = Sy->Sq->nvar; */
  nfields = Sy->nfields;
  nodloc  = Sy->Pa->nodloc;

  nforig = *fnforig - 1;
  nfdist = *fnfdist - 1;
  /* nfedist = *fnfedist - 1; */
  newfc = *fnewfc - 1;

  rl = &parnum[15];

  ierr = VecGetArray(v, &varscan);        CHKERRABORT(PETSC_COMM_WORLD,ierr);

  /* Set mass correction switch */
  PetscOptionsGetInt (NULL, "pgpfep_", "-wmc", &swmass, &pt_flg);
  if (pt_flg != PETSC_TRUE)
    swmass = 1;
  if (level < -999.) swmass = 0;

  PetscPrintf (Comm, "Mass correction option: %d\n", swmass);

  /* Set distance values to max_dist */
  /*  PetscOptionsGetReal ("pgpfep_", "-maxdist", &max_distance, &pt_flg);
  if (pt_flg != PETSC_TRUE)
  max_distance = 1.0e+30;*/
  max_distance = parnum[12];
  if (max_distance < 0) max_distance = 1.0e+30;
  if (level < -999.) max_distance = 1.0e+30;

  for (i = 0; i < nodloc; i++) 
    varscan[i*nfields+nfdist] =  max_distance;
  
  ierr = VecRestoreArray(v, &varscan);    CHKERRABORT(PETSC_COMM_WORLD, ierr);

  VecGhostUpdateBegin(v, INSERT_VALUES, SCATTER_FORWARD);
  VecGhostUpdateEnd(v, INSERT_VALUES, SCATTER_FORWARD);

  /* Create distfield ghosted vector */
  ierr = VecCreateGhost(Comm, Sy->Pa->nodloc, Sy->Pa->nodtot, Sy->Pa->nodghost,
			Sy->Pa->Glob, &distfield); CHKERRABORT(PETSC_COMM_WORLD,ierr);

  ierr = VecGhostGetLocalForm(v,&lv);    CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGetArray(lv,&varscan);       CHKERRABORT(PETSC_COMM_WORLD,ierr);

  /* Small vectors (element scope) */
  funcval = (PetscScalar *) malloc (nnpe*sizeof(PetscScalar));
  if (funcval == (PetscScalar *)NULL)
    error(EXIT, "Not enough memory for funcval");
  nextval = (PetscScalar *) malloc (nnpe*sizeof(PetscScalar));
  if (nextval == (PetscScalar *)NULL)
    error(EXIT, "Not enough memory for nextval");

  distval = (PetscScalar *) malloc (nnpe*sizeof(PetscScalar));
  if (distval == (PetscScalar *)NULL)
    error(EXIT, "Not enough memory for distval");
  
  coordaux = (PetscScalar *) malloc (nnpe*nsd*sizeof(PetscScalar));
  if(coordaux == (PetscScalar *)NULL)
    error(EXIT, "Not enough memory for coordaux");
  /* ngr = Sy->Pa->ngroups;  */

  /* ------------------------------------------------------------------------------- */
  /* --- Variables for brute force computation of exact distance ------------------- */

  int p;
  double fvol;

  int *nintp;
  nintp = (int *) malloc (MAX_INTS_PROC * sizeof(int));
  if (nintp == (int *)NULL)
    error(EXIT, "Not enough memory for nintp");

  int *ind_patchs;
  ind_patchs  = (int *) malloc (MAX_INTS_PROC * sizeof(int));
  if (ind_patchs == (int *)NULL)
    error(EXIT, "Not enough memory for ind_patchs");

  PetscScalar *auxintpoints;
  auxintpoints = (PetscScalar *) malloc (MAX_INTS_PROC*nnpe*nsd*sizeof(PetscScalar));
  if(auxintpoints == (PetscScalar *)NULL)
    error(EXIT, "Not enough memory for auxintpoints");

  int *tot_nintp;
  tot_nintp = (int *) malloc (MAX_INTS * sizeof(int));
  if (tot_nintp == (int *)NULL)
    error(EXIT, "Not enough memory for tot_nintp");

  int *tot_ind_patchs;
  tot_ind_patchs  = (int *) malloc (MAX_INTS * sizeof(int));
  if (tot_ind_patchs == (int *)NULL)
    error(EXIT, "Not enough memory for tot_ind_patchs");

  PetscScalar *tot_auxintpoints;
  tot_auxintpoints = (PetscScalar *) malloc (MAX_INTS*nnpe*nsd*sizeof(PetscScalar));
  if(tot_auxintpoints == (PetscScalar *)NULL)
    error(EXIT, "Not enough memory for tot_auxintpoints");

  PetscScalar *p_auxintpoints;
  p_auxintpoints = (PetscScalar *) malloc (nnpe*nsd*sizeof(PetscScalar));
  if(p_auxintpoints == (PetscScalar *)NULL)
    error(EXIT, "Not enough memory for p_auxintpoints"); 

  PetscScalar *coordnode;
  coordnode = (PetscScalar *) malloc (nnpe*nsd*sizeof(PetscScalar));
  if(coordnode == (PetscScalar *)NULL)
    error(EXIT, "Not enough memory for coordnode");

  int *recvco1, *recvco2;
  recvco1  = (int * ) malloc (sizecomm * sizeof(int));
  recvco2  = (int * ) malloc (sizecomm * sizeof(int));
  
  int *displ1, *displ2;
  displ1  = (int * ) malloc (sizecomm * sizeof(int));
  displ2  = (int * ) malloc (sizecomm * sizeof(int));
  
  /* --------------------------------------------------------------------- */
  /* --------------------------------------------------------------------- */

  /* alloc iele (sorted elements) */
  /* locelem = Sy->Pa->nelemloc; */
  totelem = Sy->Pa->nelemloc + Sy->Pa->nelemext;
  iele = (int *) malloc (totelem*sizeof(int));
  if (iele == (int *)NULL)
    error(EXIT, "Not enough memory for iele");
  for (i = 0; i < totelem; i++)
    iele[i] = i;
  volelei = (double *) malloc (totelem*sizeof(double));
  if (volelei == (double *)NULL)
    error(EXIT, "Not enough memory for volelei");

  nvprop = (int *) malloc ((Sy->Pa->nodloc+Sy->Pa->nodghost) * sizeof(int));
  if (nvprop == (int *)NULL)
    error(EXIT, "Not enough memory for nvprop");
  for (i = 0; i < Sy->Pa->nodloc+Sy->Pa->nodghost; i++)
    nvprop[i] = 0;
  dvprop = (double *) malloc (Sy->Pa->nodloc * sizeof(double));
  if (dvprop == (double *)NULL)
    error(EXIT, "Not enough memory for dvprop");
  for (i = 0; i < Sy->Pa->nodloc; i++)
    dvprop[i] = 0.0;

  VecDuplicate(distfield, &dfcorr);

  /* ---------------------------------------------------------------------- */
  /* --- OJO - RFA (30-07-09) ORIGINAL COMPUTATION WITH INTERDIST --------- */
  if(0) {
    /*
     * First loop over elements
     * locate interface elements
     * (compute mass)
     * start building iele (sorted elements)
     */
    last_changed = 0;
    for (i = 0; i < totelem; i++) /* Loop over elements of this group */
      {
	iel = iele[i];
	inbe = iel * nnpe;
	for (k = 0; k < nnpe; k++){  /* Loop over nodes of this element */
	  inoloc = Sy->Pa->Mesh[inbe+k];
	  posfunc = inoloc * Sy->nfields + nforig;
	  funcval[k] = varscan[posfunc];
	}

	/* Identify interface elements */
	nlow = nhigh = 0;
	for (k = 0; k < nnpe; k++) {
	  if (funcval[k] > level) nhigh++;
	  else nlow++;
	}
	if (!(nhigh && nlow))
	  continue;

	/* YES, set coordinates and compute good distances */
	if (i > last_changed)
	  {
	    k = iele[i];
	    iele[i] = iele[last_changed];
	    iele[last_changed] = k;
	    last_changed++;
	  }
	for (k = 0; k < nnpe; k++) {
	  inoloc = Sy->Pa->Mesh[inbe+k];
	  for (j = 0; j < nsd; j++)
	    coordaux[k+j*nnpe] = (Sy->Pa->cogeo)[inoloc*nsd+j];
	}

	/* Correction of geometric coordinates if periodic */
	periodic_corr(coordaux, nnpe, nsd, rl);
	interdist2 (coordaux, nsd, funcval, level, distval, &volelei[iel]);

	for (k = 0; k < nnpe; k++) {
	  posfunc = Sy->Pa->Mesh[inbe+k] * Sy->nfields + nfdist;
	  if (distval[k] < varscan[posfunc])
	    varscan[posfunc] = distval[k];
	  if (!swmass)
	    nvprop[Sy->Pa->Mesh[inbe+k]] = 1;
	}
      } /* End loop elements of this mesh */
    intelem = last_changed;
    PetscSynchronizedPrintf (Comm, "%d elements in the interface\n", intelem);
    PetscSynchronizedFlush (PETSC_COMM_WORLD, stdout);

    /* Pass distance to distfield vector */
    ierr=VecGetArray(distfield, &dfscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
    for (i = 0; i < Sy->Pa->nodloc; i++)
      {
	if (varscan[i*Sy->nfields + nforig] > level)
	  dfscan[i] = varscan[i*Sy->nfields + nfdist];
	else
	  dfscan[i] = -varscan[i*Sy->nfields + nfdist];
      }
    ierr=VecRestoreArray(distfield, &dfscan); CHKERRABORT(PETSC_COMM_WORLD,ierr);

    /* Update ghost node's distances */
    VecGhostUpdateBegin(distfield, INSERT_VALUES, SCATTER_FORWARD);
    VecGhostUpdateEnd(distfield, INSERT_VALUES, SCATTER_FORWARD);
  }

  /* --- END ORIGINAL COMPUTATION WITH INTERDIST -------------------------- */
  /* --- NEW COMPUTATION WITH BRUTE FORCE --------------------------------- */
  /* --- OJO - RFA 30/07/09 */

  else {
    /*
     * First loop over elements
     * locate interface elements
     * (compute mass)
     * start building iele (sorted elements)
     */
    
    for (i = 0; i < Sy->Pa->nodloc + Sy->Pa->nodghost; i++)
      nvprop[i] = 0;

    PetscPrintf (Comm, "STARTING PULMON DISTANCE ON INTERFACE ELEMENTS\n");
    int nints = 0;
    int tot_nints = 0;
    ind_patchs[nints] = 0; 
    last_changed = 0;
    for (i = 0; i < totelem; i++) /* Loop over elements of this group */
      {
	iel = iele[i];
	inbe = iel * nnpe;
	for (k = 0; k < nnpe; k++) {  /* Loop over nodes of this element */
	  inoloc = Sy->Pa->Mesh[inbe+k];
	  posfunc = inoloc * Sy->nfields + nforig;
	  funcval[k] = varscan[posfunc];
	}
	/* Identify interface elements */
	nlow = nhigh = 0;
	for (k = 0; k < nnpe; k++) {
	  if (funcval[k] > level) nhigh++;
	  else nlow++;
	}
	if (!(nhigh && nlow))
	  continue;

	/* YES, set coordinates and compute good distances */
	if (i > last_changed)
	  {
	    k = iele[i];
	    iele[i] = iele[last_changed];
	    iele[last_changed] = k;
	    last_changed++;
	  }
	for (k = 0; k < nnpe; k++) {
	  inoloc = Sy->Pa->Mesh[inbe+k];
	  for (j = 0; j < nsd; j++)
	    coordaux[k+j*nnpe] = (Sy->Pa->cogeo)[inoloc*nsd+j];
	}
	/* Check memory */
	if (nints+1 > MAX_INTS_PROC)
	  {
	    PetscPrintf (PETSC_COMM_SELF,
			 "Need to increase MAX_INTS_PROC: %d\n", nints+1);
	    error(EXIT, "Not enough memory for nintp, ind_patchs");
	  }
	if (ind_patchs[nints]+nnpe*nsd > MAX_INTS_PROC*nnpe*nsd)
	  {
	    PetscPrintf (PETSC_COMM_SELF,
			 "Need to increase MAX_INTS_PROC: %d\n", MAX_INTS_PROC);
	    error(EXIT, "Not enough memory for auxintpoints");
	  }
	// Build segment, triangle or quadrilateral
	fvol =  fracvol2_(coordaux, &nsd, funcval, &level, &volelei[iel], 
			  &nintp[nints], &auxintpoints[ind_patchs[nints]],
			  &sizeint);

	//PetscSynchronizedPrintf (Comm, "%lf\t%lf\n", fvol, volelei[iel]);
	volelei[iel] = fvol*volelei[iel];

	ind_patchs[nints+1] = ind_patchs[nints] + nintp[nints]*nsd;
      
	nints++;
      
	/* Correction of geometric coordinates if periodic */
	periodic_corr(coordaux, nnpe, nsd, rl);
	//interdist2 (coordaux, nsd, funcval, level, distval, &volelei[iel]);
	for (k = 0; k < nnpe; k++) 
	  {
	    if (swmass)
	      nvprop[Sy->Pa->Mesh[inbe+k]] = 1;
	  }
	/* else                /\* Inter-processor element *\/ */
	/* 	{ */
	/* 	  for (k = 0; k < nnpe; k++) { */
	/* 	    inoloc = Sy->Pa->ExtMesh[inbe+k] - 1; */
	/* 	    /\* Store local AND external distances *\/ */
	/* 	    if (inoloc < 0) { */
	/* 	      if (!swmass) */
	/* 		nveprop[(-inoloc-1)] = 1; */
	/* 	    } */
	/* 	    else { */
	/* 	      if (swmass) */
	/* 		nvprop[(Sy->Pa->ExtMesh[inbe+k] - Sy->Pa->first)] = 1; */
	/* 	    } */
	/* 	  } */
	/* 	} */
      } /* End loop elements of this mesh */

    intelem = last_changed;
    PetscSynchronizedPrintf (Comm, "%d elements in the interface\n", intelem);
    PetscSynchronizedFlush (PETSC_COMM_WORLD, stdout);

    /*----------------------------------------------------------------------- */
    /*--- Communication of interface data begins */

    /* Computar numero total global de interfaces */
    ierr = MPI_Allreduce (&nints, &tot_nints, 1, MPI_INT, MPI_SUM, Comm);
  
    /*  Todos los procesos deben conocer cuantos patches hay en cada proceso */
    ierr = MPI_Allgather (&nints, 1, MPI_INT, recvco1, 1, MPI_INT, Comm); 
  
    displ1[0] = 0;
    for (i = 1; i < sizecomm; i++)
      displ1[i] = displ1[i-1] + recvco1[i-1];

    /* Check memory */
    if (tot_nints > MAX_INTS)
      {
	PetscPrintf (PETSC_COMM_SELF,
		     "Need to increase MAX_INTS to %d\n", tot_nints);
	error(EXIT, "Not enough memory for tot_nintp");
      }
    ierr = MPI_Allgatherv (nintp, nints, MPI_INT,
			   tot_nintp, recvco1, displ1, MPI_INT, Comm);
  
    tot_ind_patchs[0] = 0;
    for (i = 1; i < tot_nints; i++)
      tot_ind_patchs[i] = tot_ind_patchs[i-1] + tot_nintp[i-1]*nsd;

    int auxcom;
    if(nints != 0)
      auxcom = ind_patchs[nints-1] + nintp[nints-1]*nsd;
    else
      auxcom = 0;

    /* Todos los procesos deben conocer cuantos valores vienen de cada proceso */
    ierr = MPI_Allgather (&auxcom, 1, MPI_INT, recvco2, 1, MPI_INT, Comm); 

    displ2[0] = 0;
    for (i = 1; i < sizecomm; i++)
      displ2[i] = displ2[i-1] + recvco2[i-1];

    /* Check memory */
    if (displ2[sizecomm-1]+recvco2[sizecomm-1] > MAX_INTS*nnpe*nsd)
      {
	PetscPrintf (PETSC_COMM_SELF,
		     "Need to increase MAX_INTS to at least: %d\n",
		     displ2[sizecomm-1]+recvco2[sizecomm-1]);
	error(EXIT, "Not enough memory for tot_auxintpoints");
      }

    ierr = MPI_Allgatherv (auxintpoints, auxcom, MPI_DOUBLE,
			   tot_auxintpoints, recvco2, displ2, MPI_DOUBLE, Comm);

    /* --- Communication of interface data ends */
    /* ------------------------------------------------------------------------- */

    double nd;
    for (i = 0; i < Sy->Pa->nodloc; i++) // Loop over local nodes
      {
	if(nvprop[i] == 1) // Node in the interface 
	  {
	    for (j = 0; j < nsd; j++)
	      coordnode[j] = (Sy->Pa->cogeo)[i*nsd+j];
	  
	    for (p = 0; p < tot_nints; p++) // Loop over all patches
	      {
		for (j = 0; j < tot_nintp[p]*nsd; j++)
		  {
		    p_auxintpoints[j] = tot_auxintpoints[tot_ind_patchs[p] + j]; 
		  }
		//PetscSynchronizedPrintf (Comm, "%lf\t%lf\t\n", p_auxintpoints[0], p_auxintpoints[2]);
		//PetscSynchronizedPrintf (Comm, "%lf\t%lf\t\n", p_auxintpoints[1], p_auxintpoints[3]);
		//PetscSynchronizedPrintf (Comm, "\n");

		if (tot_nintp[p] == 2)
		  {
		    nd = dp2seg (coordnode, p_auxintpoints, nsd);
		  }
		else if (tot_nintp[p] == 3)
		  {
		    nd = dp2tri (coordnode, p_auxintpoints, nsd);
		  }
		else if (tot_nintp[p] == 4)
		  {
		    nd = dp2qua (coordnode, p_auxintpoints, nsd);
		  }
		else
		  {
		    fprintf (stderr, "Bad nintp!!\n");
		    exit (EXIT_FAILURE);
		  }
	      
		if (nd < varscan[i*nfields+nfdist])
		  varscan[i*nfields+nfdist] = nd;	     
	      } // End loop over all patches
	    //PetscSynchronizedPrintf (Comm, "%d\t%lf\n", i, varscan[i*nfields+nfdist]);
	  }
      } // End loop over local nodes

    /* Pass distance to distfield vector */
    ierr=VecGetArray(distfield, &dfscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
    for (i = 0; i < Sy->Pa->nodloc; i++)
      {
	if (varscan[i*Sy->nfields + nforig] > level)
	  dfscan[i] = varscan[i*Sy->nfields + nfdist];
	else
	  dfscan[i] = -varscan[i*Sy->nfields + nfdist];
      }
    ierr=VecRestoreArray(distfield, &dfscan); CHKERRABORT(PETSC_COMM_WORLD,ierr);

    /* Scatter-gather updating external node's distances */
    /* Scatter distfield -> distfieldext */
    /* ierr = VecScatterBegin (sgc_df, distfield, distfieldext, INSERT_VALUES, */
    /* 			SCATTER_FORWARD); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
    /* ierr = VecScatterEnd (sgc_df, distfield, distfieldext, INSERT_VALUES, */
    /* 			SCATTER_FORWARD); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
    ierr = VecGhostUpdateBegin(distfield, INSERT_VALUES, SCATTER_FORWARD
			       ); CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr = VecGhostUpdateEnd(distfield, INSERT_VALUES, SCATTER_FORWARD
			     ); CHKERRABORT(PETSC_COMM_WORLD,ierr);

    //--- RFA 30/07/09 --------------------------
    for (i = 0; i < Sy->Pa->nodloc + Sy->Pa->nodghost; i++)
      nvprop[i] = 0;
    //-------------------------------------------
  }
  //--- END NEW COMPUTATION WITH BRUTE FORCE -----------------------------
  //----------------------------------------------------------------------

  if (swmass) {
    /*
     * Mass correction: sweep elements over the interface, check
     * new mass vs original mass and compute suggested correction
     */
    ierr=VecGhostGetLocalForm(distfield,
			      &ldistf);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr=VecGetArray(ldistf, &dfscan);   CHKERRABORT(PETSC_COMM_WORLD,ierr);
    
    totvolint = 0.0;
    dmassi = 0.0;
    for (i = 0; i < intelem; i++) {  /* Loop over interface elements */
      iel = iele[i];
      maxval = -1.0e+30;
      minval = +1.0e+30;
      inbe = iel * nnpe;
      for (k = 0; k < nnpe; k++){  /* Loop over nodes of this element */
	inoloc = Sy->Pa->Mesh[inbe+k];
	/* inolocs[k] = inoloc; */
	nextval[k] = funcval[k] = dfscan[inoloc];
	if (nextval[k] < minval) minval = nextval[k];
	if (nextval[k] > maxval) maxval = nextval[k];
	for (j = 0; j < nsd; j++)
	  coordaux[k+j*nnpe] = (Sy->Pa->cogeo)[inoloc*nsd+j];
      }

      /* Correction of geometric coordinates if periodic */
      periodic_corr(coordaux, nnpe, nsd, rl);

      /* Compute new mass in this element */
      for (j = 0; j < 10; j++) {
	volele = parvol2 (coordaux, nsd, nextval, 0, &vref, &sizeint);
	delvol = volele - volelei[iel];
	if (j == 0) /* First iteration, compute initial mass defect */
	  {
	    if (Sy->Pa->GOS[iel] > 0)
	      {
		dmassi += delvol; /* Accumulate if element is internal or
				   * assigned to this processor */
		totvolint += vref;
	      }
	  }
	if (fabs(delvol) < 1.0e-8 * vref)
	  break;
	if (sizeint < 1.0e-50)
	  break;
	for (k = 0; k < nnpe; k++) {
	  newval = nextval[k] - delvol / sizeint;

	  if (nextval[k] > 0.0) {
	    if (newval > 0.0)
	      nextval[k] = newval;
	    else
	      nextval[k] = 1.0e-50;
	  }
	  else {
	    if (!(newval > 0.0))
	      nextval[k] = newval;
	    else
	      nextval[k] = -1.0e-50;
	  }
	}
      }

      for (k = 0; k < nnpe; k++) {
	inoloc = Sy->Pa->Mesh[inbe+k];
	if (inoloc < Sy->Pa->nodloc)
	  dvprop[inoloc] += nextval[k] - funcval[k];
	nvprop[inoloc] ++;
      }
    } /* End loop elements of this mesh */
    
  PetscSynchronizedPrintf (Comm, "Initial mass defect: %.18g\n", dmassi);
  ierr = MPI_Allreduce(&dmassi, &daux, 1, MPI_DOUBLE, MPI_SUM, Comm);
  dmassi = daux;
  PetscSynchronizedPrintf (Comm, "Volume of interface elements %g\n", 
			   totvolint);
  ierr = MPI_Allreduce(&totvolint, &daux, 1, MPI_DOUBLE, MPI_SUM, Comm);
  totvolint = daux;

  /* Suggested modifications computed */
  /* Find optimal values using "line search" */
  alfao = alfaoo = 0.0;
  dmasso = dmassoo = dmassi;
  ierr = VecGetArray(dfcorr, &dfcscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);

  alfa = 1.0; 
  for (italfa = 0; italfa < 8; italfa++) {
    /* Descent: new distfield using alfa */
    for (i = 0; i < Sy->Pa->nodloc; i++) {
      if (nvprop[i] > 0) {
	newval = dfscan[i] + dvprop[i]/nvprop[i] * alfa;
	if (dfscan[i] > 0.0) {
	  if (newval > 0.0)
	    dfcscan[i] = newval;
	  else
	    dfcscan[i] = 1.0e-50;
	}
	else {
	  if (!(newval > 0.0))
	    dfcscan[i] = newval;
	  else
	    dfcscan[i] = -1.0e-50;
	}
      }
      else {
	dfcscan[i] = dfscan[i];
      }
    }
    ierr = VecRestoreArray(dfcorr,
			   &dfcscan);    CHKERRABORT(PETSC_COMM_WORLD,ierr);
    
    VecGhostUpdateBegin(dfcorr, INSERT_VALUES, SCATTER_FORWARD);
    VecGhostUpdateEnd(dfcorr, INSERT_VALUES, SCATTER_FORWARD);

    ierr = VecGhostGetLocalForm(dfcorr, &ldfcorr);
    ierr = VecGetArray(ldfcorr, &dfcscan);   CHKERRABORT(PETSC_COMM_WORLD,ierr);

    /* Compute mass with the new distance field */
    dmass = 0.0;
    for (i = 0; i < intelem; i++) {  /* Loop over interface elements */
      iel = iele[i];
      inbe = iel * nnpe;
      for (k = 0; k < nnpe; k++){  /* Loop over nodes of this element */
	inoloc = Sy->Pa->Mesh[inbe+k];
	funcval[k] = dfcscan[inoloc];
	for (j = 0; j < nsd; j++)
	  coordaux[k+j*nnpe] = (Sy->Pa->cogeo)[inoloc*nsd+j];
      }
      
      /* Correction of geometric coordinates if periodic */
      periodic_corr(coordaux, nnpe, nsd, rl);

      /* Compute new mass in this element */
      volele = parvol2 (coordaux, nsd, funcval, 0, &vref, &sizeint);
      delvol = volele - volelei[iel];                /* Mass defect */
      
      if (Sy->Pa->GOS[iel] > 0)
	dmass += delvol;       /* Accumulate if element is internal or
				* assigned to this processor */

    } /* End loop elements of this mesh */
    
    ierr = MPI_Allreduce(&dmass, &daux, 1, MPI_DOUBLE, MPI_SUM, Comm);
    dmass = daux;
    PetscPrintf (Comm, "Iter: %d, alpha: %g, mass defect: %g\n",
		 italfa, alfa, dmass);

    if (fabs(dmass) < totvolint * 1.0e-08) {
      comp_dfc = 0;
      break;
    }

    if (dmass * dmasso < 0) { /* Interpolate new value of alfa */
      dmassoo = dmasso; dmasso = dmass;
      alfaoo = alfao; alfao = alfa;
      alfa = alfaoo + (alfao - alfaoo) * (-dmassoo) / (dmasso-dmassoo);
      comp_dfc = 1;
    }
    else if (dmass * dmassoo < 0) { /* Interpolate new value of alfa */
      dmasso = dmass;
      alfao = alfa;
      alfa = alfaoo + (alfao - alfaoo) * (-dmassoo) / (dmasso-dmassoo);
      comp_dfc = 1;
    }
    else {               /* sign[dmassoo] = sign[dmasso] = sign[dmass] */
      if (fabs(dmass) < fabs(dmasso)) {
	alfaoo = alfao; alfao = alfa;
	dmassoo = dmasso; dmasso = dmass;
	alfa = 2*alfao - alfaoo;
	comp_dfc = 0;
      }
      else {	
	alfa = alfao;
	comp_dfc = 1;
	break;
      }
    }

  } /* Next alfa iteration */

  /* End alfa iteration, re-compute with last alfa ? */
  if (comp_dfc == 1)
    for (i = 0; i < Sy->Pa->nodloc; i++) {
      if (nvprop[i] > 0) {
	newval = dfscan[i] + dvprop[i]/nvprop[i] * alfa;
	if (dfscan[i] > 0.0) {
	  if (newval > 0.0)
	    dfcscan[i] = newval;
	  else
	    dfcscan[i] = 1.0e-50;
	}
	else {
	  if (!(newval > 0.0))
	    dfcscan[i] = newval;
	  else
	    dfcscan[i] = -1.0e-50;
	}
      }
      else {
	dfcscan[i] = dfscan[i];
      }
    }
  ierr =VecRestoreArray(ldfcorr, &dfcscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
  VecGhostRestoreLocalForm(dfcorr, &ldfcorr);

  ierr =VecRestoreArray(distfield, &dfscan);CHKERRABORT(PETSC_COMM_WORLD,ierr);
  VecCopy(dfcorr, distfield);
  VecGhostUpdateBegin(distfield, INSERT_VALUES, SCATTER_FORWARD);
  VecGhostUpdateEnd(distfield, INSERT_VALUES, SCATTER_FORWARD);

  /* End mass correction */
  }
  VecDestroy(&dfcorr);
  free (dvprop);

  /* Newfunc OK for all interface elements, propagate using edge distance */
  ierr= VecGhostGetLocalForm(distfield, &ldistf);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr=VecGetArray(ldistf, &dfscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);

  /* Pass fixed distances to var */
  for (i = 0; i < Sy->Pa->nodloc + Sy->Pa->nodghost; i++)
    {
      if (varscan[i*Sy->nfields + nforig] > level)
	varscan[i*Sy->nfields + nfdist] = dfscan[i];
      else 
	{
	  varscan[i*Sy->nfields + nfdist] = -dfscan[i];
	  dfscan[i] = -dfscan[i];
	}
    }

  ed_count = sg_count = 0;
  changed_since_scatter = 0;
  changes = 1;
  while (changed_since_scatter || changes /* && ed_count < 500 */) {
    changes = 0;
    ed_count++;
    /* Loop over elements, process elements not over the interface */
    for (i = intelem; i < totelem; i++) {  /* Loop over elements of the mesh
					    * skip interface elements */
      iel = iele[i];
      nlow = nhigh = 0;
      inbe = iel * nnpe;
      for (k = 0; k < nnpe; k++) {   /* Loop over nodes of this element */
	inoloc = Sy->Pa->Mesh[inbe+k];
	funcval[k] = dfscan[inoloc]; /* Use two-fields for sorting */
	for (j = 0; j < nsd; j++)
	  coordaux[k+j*nnpe] = (Sy->Pa->cogeo)[inoloc*nsd+j];
	posfunc = inoloc * Sy->nfields + nforig;
	/* oldval = varscan[posfunc]; */
      }

      /* Correction of geometric coordinates if periodic */
      periodic_corr(coordaux, nnpe, nsd, rl);

      if (edge_distance2 (coordaux, nsd, funcval)) {
	if (i > last_changed)
	  {
	    k = iele[i];
	    iele[i] = iele[last_changed];
	    iele[last_changed] = k;
	    last_changed++;
	  }
	for (k = 0; k < nnpe; k++) {  /* Loop over nodes of this element */
	  /* Use two-fields for sorting */
	  inoloc = Sy->Pa->Mesh[inbe+k];
	  if (nvprop[inoloc] == 0 &&             /* skip interface nodes */
	      funcval[k] < varscan[inoloc*Sy->nfields + nfdist]) {
	    varscan[inoloc*Sy->nfields + nfdist] = funcval[k];
	    changes = 1;
	  }
	}
      }
    } /* End loop over elements of the mesh */

    /* End sweep over elements (internal and external) */
    if (changes) changed_since_scatter = 1;

    /* Pass distance to distfield vector */
    for (i = 0; i < Sy->Pa->nodloc + Sy->Pa->nodghost; i++)
      dfscan[i] = varscan[i*Sy->nfields + nfdist];

    PetscOptionsGetInt (NULL, "pgpfep_", "-maxedcount", &max_ed_count, &pt_flg);
    if (pt_flg != PETSC_TRUE)
      max_ed_count = 10000000;

    if (!changes || (ed_count%max_ed_count) == 0)
      {
	ierr = MPI_Allreduce(&changed_since_scatter, &ch_glob, 1, MPI_INT,
		      MPI_SUM, Comm);
	if (!ch_glob) break; /* All without changes */
	ierr = VecRestoreArray(ldistf,
			       &dfscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
	VecGhostRestoreLocalForm(distfield, &ldistf);

	/* Scatter-gather updating external node's distances */
	/* Scatter distfield -> distfieldext */
	VecGhostUpdateBegin(distfield, INSERT_VALUES, SCATTER_FORWARD);
	VecGhostUpdateEnd(distfield, INSERT_VALUES, SCATTER_FORWARD);

	ierr = VecGhostGetLocalForm(distfield, &ldistf
				    ); CHKERRABORT(PETSC_COMM_WORLD,ierr);
	ierr = VecGetArray (ldistf,
			    &dfscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
	changes = 1;
	changed_since_scatter = 0;
	sg_count++;
      }
  } /* End while (changes) */
  PetscSynchronizedPrintf (Comm, "Edge distance: %d sweeps over mesh\n",
			   ed_count);
  PetscSynchronizedFlush (PETSC_COMM_WORLD, stdout);
  PetscPrintf (Comm, "Edge distance: %d scatter operations\n", sg_count);

  /* calculo de masa (temporario) BEGIN */
    dmass = 0.0;
    for (i = 0; i < intelem; i++) {
      iel = iele[i];
      inbe = iel * nnpe;
      for (k = 0; k < nnpe; k++){
	inoloc = Sy->Pa->Mesh[inbe+k];
	if (varscan[inoloc * Sy->nfields + nforig] > level)
	  funcval[k] = dfscan[inoloc];
	else
	  funcval[k] = -dfscan[inoloc];
	for (j = 0; j < nsd; j++)
	  coordaux[k+j*nnpe] = (Sy->Pa->cogeo)[inoloc*nsd+j];
      }
      
      periodic_corr(coordaux, nnpe, nsd, rl);

      volele = parvol2 (coordaux, nsd, funcval, 0, &vref, &sizeint);
      delvol = volele - volelei[iel];
      if (Sy->Pa->GOS[iel] > 0)
	dmass += delvol;
    }
    ierr = MPI_Allreduce(&dmass, &daux, 1, MPI_DOUBLE, MPI_SUM, Comm);
    dmass = daux;
    PetscPrintf (Comm, "After edge_distance, mass defect: %g\n", dmass);
    /*   calculo de masa (temporario) END */

  /* Edge distance OK, try to improve, use distfield vectors */
  changes = 1;
  changed_since_scatter = 1;
  gd_count = sg_count = 0;
  while (changes || changed_since_scatter /*&& gd_count < 100*/) {
    changes = 0;
    gd_count++;
    /* Loop over elements, process elements not over the interface */
    for (i = intelem; i < totelem; i++) { /* Loop over elements of the mesh */
      iel = iele[i];
      nlow = nhigh = 0;
      inbe = iel * nnpe;
      for (k = 0; k < nnpe; k++) {   /* Loop over nodes of this element */
	inoloc = Sy->Pa->Mesh[inbe+k];
	funcval[k] = dfscan[inoloc];
	for (j = 0; j < nsd; j++)
	  coordaux[k+j*nnpe] = (Sy->Pa->cogeo)[inoloc*nsd+j];
	posfunc = inoloc * Sy->nfields + nforig;
      }
    
      /* Correction of geometric coordinates if periodic */
      periodic_corr(coordaux, nnpe, nsd, rl);

      /* OK, we found an element to be processed */
      if (shadow_distance2 (coordaux, nsd, funcval)) {
	/* shadow distance improves previous values */
	for (k = 0; k < nnpe; k++) {
	  inoloc = Sy->Pa->Mesh[inbe+k];
	  newval = funcval[k];
	  if (nvprop[inoloc] == 0 &&           /* skip interface nodes */
	      newval < dfscan[inoloc]) {
	    dfscan[inoloc] = newval;
	    changes = 1;
	  }
	}
      }
    } /* End loop elements of this mesh */

    /* End sweep over elements (internal and external) */
    if (changes) changed_since_scatter = 1;

    /* printf("Element sweep concluded: %d changes\n", changes); */

    PetscOptionsGetInt (NULL, "pgpfep_", "-maxgdcount", &max_gd_count, &pt_flg);
    if (pt_flg != PETSC_TRUE)
      max_gd_count = 10000000;

    if (!changes || (gd_count%max_gd_count) == 0)
      {
	ierr = MPI_Allreduce(&changed_since_scatter, &ch_glob, 1,
		      MPI_INT, MPI_SUM, Comm);
	if (!ch_glob) break; /* All without changes */
	ierr = VecRestoreArray(ldistf,
			       &dfscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
	VecGhostRestoreLocalForm(distfield, &ldistf);
	VecGhostUpdateBegin(distfield, INSERT_VALUES, SCATTER_FORWARD);
	VecGhostUpdateEnd(distfield, INSERT_VALUES, SCATTER_FORWARD);

	VecGhostGetLocalForm(distfield, &ldistf);
	ierr = VecGetArray(ldistf,
			   &dfscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
	changes = 1;
	changed_since_scatter = 0;
	sg_count++;
      }
  } /* End while (changes) */

  /* calculo de masa (temporario) BEGIN */
  /* Compute mass with the new distance field */
  dmass = 0.0;
  for (i = 0; i < intelem; i++) {  /* Loop over interface elements */
    iel = iele[i];
    inbe = iel * nnpe;
    for (k = 0; k < nnpe; k++){  /* Loop over nodes of this element */
      inoloc = Sy->Pa->Mesh[inbe+k];
      if (varscan[inoloc * Sy->nfields + nforig] > level)
	funcval[k] = dfscan[inoloc];
      else
	funcval[k] = -dfscan[inoloc];
      for (j = 0; j < nsd; j++)
	coordaux[k+j*nnpe] = (Sy->Pa->cogeo)[inoloc*nsd+j];
    }

    /* Correction of geometric coordinates if periodic */
    periodic_corr(coordaux, nnpe, nsd, rl);

    /* Compute new mass in this element */
    volele = parvol2 (coordaux, nsd, funcval, 0, &vref, &sizeint);
    delvol = volele - volelei[iel];                /* Mass defect */
    if (Sy->Pa->GOS[iel] > 0)
      dmass += delvol;       /* Accumulate if element is internal or
				* assigned to this processor */
  } /* End loop elements of this mesh */
  ierr = MPI_Allreduce(&dmass, &daux, 1, MPI_DOUBLE, MPI_SUM, Comm);
  dmass = daux;
  PetscPrintf (Comm, "After shadow_distance: mass defect: %g\n", dmass);
  /* calculo de masa (temporario) END */

  free(nvprop);
  free(volelei);

  /* Pass distfield to distance field in unknowns vector */
  for (i = 0; i < nodloc + Sy->Pa->nodghost; i++)
    {
      if (varscan[i*Sy->nfields + nforig] > level)
	varscan[i*nfields+nfdist] = dfscan[i];
      else
	varscan[i*nfields+nfdist] = -dfscan[i];
    }

  PetscSynchronizedPrintf (Comm, "Final distance: %d sweeps over mesh\n",
			   gd_count);
  PetscSynchronizedFlush (PETSC_COMM_WORLD, stdout);
  PetscPrintf(Comm, "Final distance: %d scatter operations\n", sg_count);

  /* Pass distance to characteristic fuction */
  if (newfc > -1 && newfc != nforig)
    for (i = 0; i < nodloc; i++)
      {
	newval = varscan[i*nfields+newfc];
	/* newval = max_distance * tanh(newval/max_distance*2.5); */
	varscan[i*nfields+nforig] = newval+level;
      }
  ierr = VecRestoreArray(lv, &varscan);    CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGhostRestoreLocalForm(v,&lv);  CHKERRABORT(PETSC_COMM_WORLD,ierr);

  ierr = VecRestoreArray(ldistf, &dfscan); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  VecGhostRestoreLocalForm(distfield, &ldistf);
  VecDestroy(&distfield);

  free(iele);

  free(displ2);
  free(displ1);
  free(recvco2);
  free(recvco1);
  free(coordnode);
  free(p_auxintpoints);
  free(tot_auxintpoints);
  free(tot_ind_patchs);
  free(tot_nintp);
  free(auxintpoints);
  free(ind_patchs);
  free(nintp);
  
  free(coordaux);
  free(distval);
  free(nextval);
  free(funcval);
}

double dp2seg (double *coop, double *coos, int nsd)
{
  double a, aa;
  double a1, a2, a3;
  double b1, b2, b3;
  double dx, dy, dz;
  double sp, d;

  if (nsd == 2)
    {
      a1 = coos[1] - coos[0];
      a2 = coos[3] - coos[2];

      b1 = coop[0] - coos[0];
      b2 = coop[1] - coos[2];

      aa = a1*a1 + a2*a2; 
      a = sqrt (aa); /* Longitud del segmento */
      
      if (a > 0)
	{ /* Segmento no-nulo */
	  
	  sp = (a1*b1 + a2*b2) / aa;
	  
	  if (sp <= 0)
	    {
	      d = sqrt( (coop[0] - coos[0])*(coop[0] - coos[0])+ 
			(coop[1] - coos[2])*(coop[1] - coos[2])  );
	    }
	  else if (sp >= 1)
	    {
	      d = sqrt( (coop[0] - coos[1])*(coop[0] - coos[1])+
			(coop[1] - coos[3])*(coop[1] - coos[3])  );
	    }
	  else
	    {
	      dx = 0;
	      dy = 0;
	      dz = (a1*b2 - a2*b1);
	      
	      d = sqrt (dx*dx + dy*dy + dz*dz);
	      d = d / a;
	    }
	}
      else
	{ /* Segmento nulo */
	  d = sqrt ((coop[0] - coos[0])*(coop[0] - coos[0]) +
		    (coop[1] - coos[2])*(coop[1] - coos[2]));
	}
    }
  else
    {
      a1 = coos[1] - coos[0];
      a2 = coos[3] - coos[2];
      a3 = coos[5] - coos[4];

      b1 = coop[0] - coos[0];
      b2 = coop[1] - coos[2];
      b3 = coop[2] - coos[4];

      aa = a1*a1 + a2*a2 + a3*a3; 
      a = sqrt (aa); /* Longitud del segmento */

      if (a > 0)
	{ /* Segmento no-nulo */
	  
	  sp = (a1*b1 + a2*b2 + a3*b3) / aa;

	  if (sp <= 0)
	    {
	      d = sqrt( (coop[0] - coos[0])*(coop[0] - coos[0])+ 
			(coop[1] - coos[2])*(coop[1] - coos[2])+
			(coop[2] - coos[4])*(coop[2] - coos[4])  );
	    }
	  else if (sp >= 1)
	    {
	      d = sqrt( (coop[0] - coos[1])*(coop[0] - coos[1])+
			(coop[1] - coos[3])*(coop[1] - coos[3])+
			(coop[2] - coos[5])*(coop[2] - coos[5])  );
	    }
	  else
	    {
	      dx = a2*b3 - a3*b2;
	      dy = a3*b1 - a1*b3;
	      dz = a1*b2 - a2*b1;
	      
	      d = sqrt (dx*dx + dy*dy + dz*dz);
	      d = d / a;
	    }
	}
      else
	{ /* Segmento nulo */
	  d = sqrt ((coop[0] - coos[0])*(coop[0] - coos[0]) +
		    (coop[1] - coos[2])*(coop[1] - coos[2]) +
		    (coop[2] - coos[4])*(coop[2] - coos[4]));
	}

    }

  
  return d;
}

double dp2tri (double *coop, double *coot, int nsd)
{
  double aa, bb;
  double a1, a2, a3, ap1, ap2, ap3;
  double b1, b2, b3, bp1, bp2, bp3;
  double c1, c2, c3, cp1, cp2, cp3;
  double n1, n2 ,n3, q1, q2, q3;
  double s11, s12, s13, s21, s22, s23;
  double s31, s32, s33, s1, s2, s3;
  double nn, ct, nd, d;
  double coos[6];
  
  a1 = coot[1+3*0] - coot[0+3*0];
  a2 = coot[1+3*1] - coot[0+3*1];
  a3 = coot[1+3*2] - coot[0+3*2];

  b1 = coot[2+3*0] - coot[0+3*0];
  b2 = coot[2+3*1] - coot[0+3*1];
  b3 = coot[2+3*2] - coot[0+3*2];

  n1 = a2*b3 - a3*b2;
  n2 = a3*b1 - a1*b3;
  n3 = a1*b2 - a2*b1;

  nn = n1*n1 + n2*n2 + n3*n3;

  if (nn > 0)
    { /* Triangulo no-nulo */

      ct = ( n1*(coop[0]-coot[0+3*0]) + 
	     n2*(coop[1]-coot[0+3*1]) + 
	     n3*(coop[2]-coot[0+3*2])   );
      
      q1 = coop[0] - n1*ct/nn;
      q2 = coop[1] - n2*ct/nn;
      q3 = coop[2] - n3*ct/nn;
      
      a1 = coot[1+3*0] - coot[0+3*0];
      a2 = coot[1+3*1] - coot[0+3*1];
      a3 = coot[1+3*2] - coot[0+3*2];
      ap1 = q1 - coot[0+3*0];
      ap2 = q2 - coot[0+3*1];
      ap3 = q3 - coot[0+3*2];
      
      b1 = coot[2+3*0] - coot[1+3*0];
      b2 = coot[2+3*1] - coot[1+3*1];
      b3 = coot[2+3*2] - coot[1+3*2];
      bp1 = q1 - coot[1+3*0];
      bp2 = q2 - coot[1+3*1];
      bp3 = q3 - coot[1+3*2];
      
      c1 = coot[0+3*0] - coot[2+3*0];
      c2 = coot[0+3*1] - coot[2+3*1];
      c3 = coot[0+3*2] - coot[2+3*2];
      cp1 = q1 - coot[2+3*0];
      cp2 = q2 - coot[2+3*1];
      cp3 = q3 - coot[2+3*2];
      
      s31 = a2*ap3-a3*ap2;
      s32 = a3*ap1-a1*ap3;
      s33 = a1*ap2-a2*ap1;
      
      s11 = b2*bp3-b3*bp2;
      s12 = b3*bp1-b1*bp3;
      s13 = b1*bp2-b2*bp1;
      
      s21 = c2*cp3-c3*cp2;
      s22 = c3*cp1-c1*cp3;
      s23 = c1*cp2-c2*cp1;
      
      s3 = sqrt(s31*s31 + s32*s32 + s33*s33)/sqrt(nn);
      s1 = sqrt(s11*s11 + s12*s12 + s13*s13)/sqrt(nn);
      s2 = sqrt(s21*s21 + s22*s22 + s23*s23)/sqrt(nn);
      
      if (s1+s2+s3 > 1.0)
	{
	  coos[0+2*0] = coot[0+3*0];
	  coos[0+2*1] = coot[0+3*1];
	  coos[0+2*2] = coot[0+3*2];
	  coos[1+2*0] = coot[1+3*0];
	  coos[1+2*1] = coot[1+3*1];
	  coos[1+2*2] = coot[1+3*2];
	  
	  d = dp2seg (coop, coos, nsd);
	  
	  coos[0+2*0] = coot[1+3*0];
	  coos[0+2*1] = coot[1+3*1];
	  coos[0+2*2] = coot[1+3*2];
	  coos[1+2*0] = coot[2+3*0];
	  coos[1+2*1] = coot[2+3*1];
	  coos[1+2*2] = coot[2+3*2];
	  
	  nd = dp2seg (coop, coos, nsd);
	  
	  if (nd < d)
	    d = nd;
	  
	  coos[0+2*0] = coot[2+3*0];
	  coos[0+2*1] = coot[2+3*1];
	  coos[0+2*2] = coot[2+3*2];
	  coos[1+2*0] = coot[0+3*0];
	  coos[1+2*1] = coot[0+3*1];
	  coos[1+2*2] = coot[0+3*2];
	  
	  nd = dp2seg (coop, coos, nsd);
	  
	  if (nd < d)
	    d = nd;
	}
      else
	{
	  d = sqrt(ct*ct/nn);
	}
    }
  else
    { /* Triangulo nulo */
      aa = a1*a1 + a2*a2 + a3*a3;
      bb = b1*b1 + b2*b2 + b3*b3;

      if ((aa+bb) > 0)
	{ /* Existe algun segmento no-nulo */

	  if (aa > 0)
	    {
	      coos[0+2*0] = coot[0+3*0];
	      coos[0+2*1] = coot[0+3*1];
	      coos[0+2*2] = coot[0+3*2];
	      coos[1+2*0] = coot[1+3*0];
	      coos[1+2*1] = coot[1+3*1];
	      coos[1+2*2] = coot[1+3*2];
	      
	      d = dp2seg (coop, coos, nsd);
	    }
	  else
	    {
	      coos[0+2*0] = coot[2+3*0];
	      coos[0+2*1] = coot[2+3*1];
	      coos[0+2*2] = coot[2+3*2];
	      coos[1+2*0] = coot[0+3*0];
	      coos[1+2*1] = coot[0+3*1];
	      coos[1+2*2] = coot[0+3*2];
	      
	      d = dp2seg (coop, coos, nsd);
	    }
	}
      else
	{ /* El triangulo colapsa en un punto */
	  d = sqrt ((coop[0] - coot[0+3*0])*(coop[0] - coot[0+3*0]) +
		    (coop[1] - coot[0+3*1])*(coop[1] - coot[0+3*1]) +
		    (coop[2] - coot[0+3*2])*(coop[2] - coot[0+3*2]));
	}
    }

  return d;
}

double dp2qua (double *coop, double *cooq, int nsd)
{
  double d, nd;
  double coot[9];

  coot[0+3*0] = cooq[0+4*0];
  coot[1+3*0] = cooq[1+4*0];
  coot[2+3*0] = cooq[2+4*0];
  coot[0+3*1] = cooq[0+4*1];
  coot[1+3*1] = cooq[1+4*1];
  coot[2+3*1] = cooq[2+4*1];
  coot[0+3*2] = cooq[0+4*2];
  coot[1+3*2] = cooq[1+4*2];
  coot[2+3*2] = cooq[2+4*2];

  d = dp2tri (coop, coot, nsd);

  coot[0+3*0] = cooq[0+4*0];
  coot[1+3*0] = cooq[2+4*0];
  coot[2+3*0] = cooq[3+4*0];
  coot[0+3*1] = cooq[0+4*1];
  coot[1+3*1] = cooq[2+4*1];
  coot[2+3*1] = cooq[3+4*1];
  coot[0+3*2] = cooq[0+4*2];
  coot[1+3*2] = cooq[2+4*2];
  coot[2+3*2] = cooq[3+4*2];

  nd = dp2tri (coop, coot, nsd);

  if (nd < d)
    d = nd;

  return d;
}

/*
 * Coordinate correction for periodic meshes
 * coordaux[n + i * nnpe]: coordinate "i" of the node "n"
 * nnpe: number of nodes per element
 * nsd: number of space dimensions
 * rl[i]: if > 0: size of the periodic mesh in the "i" direction
 *        if < 0: no periodicity in the "i" direction
 */
int periodic_corr(PetscScalar *coordaux, int nnpe, int nsd, double *rl)
{
  int i, j;
  double aux, width;

  for (i = 0; i < nsd; i++)
    if (rl[i] > 0)
      {
	width = fabs(coordaux[0+i*nnpe] - coordaux[1+i*nnpe]);
	for (j = 2; j<nnpe; j++)
	  {
	    aux = fabs(coordaux[0+i*nnpe] - coordaux[j+i*nnpe]);
	    if (aux > width)
	      width = aux;
	  }
	if (width > rl[i]/2.0)
	  for (j = 0; j <nnpe; j++)
	    if (coordaux[j+i*nnpe] < rl[i]/2.0)
	      coordaux[j+i*nnpe] = coordaux[j+i*nnpe] + rl[i];
      }
  return 0;
}

/* Interdist2: distance for the nodes in the interface element */
/* computes distance point - triangle                         */
/* coords[i*nnpe + j]: Coordinate "i" of the "j"th node        */
/* nsd: Number of space dimensions                            */
/* v: Characteristic function in each node                    */
/* lv: Level Value                                            */
/* dists: Result                                              */
/* parvol: partial volume of fill in the simplex              */
void interdist2 (double *coords, int nsd, double *v, double level, 
		 double *dists, double *parvol)
{
  int i;
  int nnpe;
  int id[4];
  double volume, fvol;

  /* Simplex: number of nodes per element: number of space dimensions + 1 */
  nnpe = nsd + 1;

  /* Identifying nodes */
  for (i = 0; i < nnpe; i++)
    if (v[i] > level)
	id[i] = +1;
    else
	id[i] = -1;

  if (nsd == 1)
    {  /* nsd == 1 */
      double s01, s10;
      volume = (coords[1]-coords[0])*(coords[1]-coords[0]);
      volume = sqrt(volume);

      if (id[0] < id[1])
	{
	  s01 = (level - v[0]) / (v[1] - v[0]);
	  dists[0] = s01 * volume;
	  dists[1] = (1.0 - s01) * volume;
	  fvol = s01;
	}
      else if (id[1] < id[0])
	{
	  s10 = (level - v[1]) / (v[0] - v[1]);
	  dists[0] = (1.0 - s10) * volume;
	  dists[1] = s10 * volume;
	  fvol = s10;
	}
    }
  else if (nsd == 2)
    { /* nsd == 2 */
      double ax, ay, bx, by;
      double s01, s12, s20, s10, s21, s02;
      double coos[4], coop[2];

      ax = (coords[0*nnpe+1]-coords[0*nnpe+0]);
      ay = (coords[1*nnpe+1]-coords[1*nnpe+0]);
      bx = (coords[0*nnpe+2]-coords[0*nnpe+0]);
      by = (coords[1*nnpe+2]-coords[1*nnpe+0]);

      volume = 0.5 * (ax*by - bx*ay);

      i = 0;
      if (id[0] < id[1])
	{
	  s01 = (level - v[0]) / (v[1] - v[0]);
	  ax = coords[0*nnpe+1] * s01 + coords[0*nnpe+0] * (1.0 - s01);
	  ay = coords[1*nnpe+1] * s01 + coords[1*nnpe+0] * (1.0 - s01);
	  i+=1;
	}
      else if (id[1] < id[0])
	{
	  s10 = (level - v[1]) / (v[0] - v[1]);
	  ax = coords[0*nnpe+0] * s10 + coords[0*nnpe+1] * (1.0 - s10);
	  ay = coords[1*nnpe+0] * s10 + coords[1*nnpe+1] * (1.0 - s10);
	  i+=1;
	}
      if (id[1] < id[2])
	{
	  s12 = (level - v[1]) / (v[2] - v[1]);
	  bx = coords[0*nnpe+2] * s12 + coords[0*nnpe+1] * (1.0 - s12);
	  by = coords[1*nnpe+2] * s12 + coords[1*nnpe+1] * (1.0 - s12);
	  if (i == 0)
	    { ax = bx; ay = by; }
	  i+=2;
	}
      else if (id[2] < id[1])
	{
	  s21 = (level - v[2]) / (v[1] - v[2]);
	  bx = coords[0*nnpe+1] * s21 + coords[0*nnpe+2] * (1.0 - s21);
	  by = coords[1*nnpe+1] * s21 + coords[1*nnpe+2] * (1.0 - s21);
	  if (i == 0)
	    { ax = bx; ay = by; }
	  i+=2;
	}
      if (id[2] < id[0])
	{
	  s20 = (level - v[2]) / (v[0] - v[2]);
	  bx = coords[0*nnpe+0] * s20 + coords[0*nnpe+2] * (1.0 - s20);
	  by = coords[1*nnpe+0] * s20 + coords[1*nnpe+2] * (1.0 - s20);
	  i+=4;
	}
      else if (id[0] < id[2])
	{
	  s02 = (level - v[0]) / (v[2] - v[0]);
	  bx = coords[0*nnpe+2] * s02 + coords[0*nnpe+0] * (1.0 - s02);
	  by = coords[1*nnpe+2] * s02 + coords[1*nnpe+0] * (1.0 - s02);
	  i+=4;
	}

      if (i == 1+2)  /* node 1 / node 0, node 2 */
	{
	  if (id[1] < 0)
	    fvol = s12 * s10;
	  else 
	    fvol = 1.0 - (1.0 - s21)*(1.0 - s01);
	}
      else if (i == 2+4)  /* node 2 / node 0, node 1 */
	{
	  if (id[2] < 0)
	    fvol = s20 * s21;
	  else
	    fvol = 1.0 - (1.0 - s02)*(1.0 - s12);
	}
      else if (i == 4+1)  /* node 0 / node 1, node 2 */
	{
	  if (id[0] < 0)
	    fvol = s02 * s01;
	  else
	    fvol = 1.0 - (1.0 - s20)*(1.0 - s10);
	}

      fvol = 1.0 - fvol;
      *parvol = volume * fvol;

      coos[0] = ax; coos[1] = bx;
      coos[2] = ay; coos[3] = by;

      coop[0] = coords[0*nnpe+0]; coop[1] = coords[1*nnpe+0];
      dists[0] = dp2seg (coop, coos, nsd);

      coop[0] = coords[0*nnpe+1]; coop[1] = coords[1*nnpe+1];
      dists[1] = dp2seg (coop, coos, nsd);

      coop[0] = coords[0*nnpe+2]; coop[1] = coords[1*nnpe+2];
      dists[2] = dp2seg (coop, coos, nsd);
      
    }
  else if (nsd == 3)
    { /* nsd == 3 */
      int j;
      double ax,ay,az, bx,by,bz, cx,cy,cz, dx,dy,dz;
      double s01,s02,s03, s10,s12,s13, s20,s21,s23, s30,s31,s32; 
      double coop[4], coot[9], cooq[12];

      ax = (coords[0*nnpe+1] - coords[0*nnpe+0]);
      bx = (coords[0*nnpe+2] - coords[0*nnpe+0]);
      cx = (coords[0*nnpe+3] - coords[0*nnpe+0]);
      ay = (coords[1*nnpe+1] - coords[1*nnpe+0]);
      by = (coords[1*nnpe+2] - coords[1*nnpe+0]);
      cy = (coords[1*nnpe+3] - coords[1*nnpe+0]);
      az = (coords[2*nnpe+1] - coords[2*nnpe+0]);
      bz = (coords[2*nnpe+2] - coords[2*nnpe+0]);
      cz = (coords[2*nnpe+3] - coords[2*nnpe+0]);

      volume = ax*(by*cz-bz*cy)+ay*(bz*cx-bx*cz)+az*(bx*cy-by*cx);
      volume = volume / 6.0;

      i = 0;
      j = 0;
      if (id[0] < id[1])
	{
	  s01 = (level - v[0]) / (v[1] - v[0]);
	  ax = coords[0*nnpe+1] * s01 + coords[0*nnpe+0] * (1.0 - s01);
	  ay = coords[1*nnpe+1] * s01 + coords[1*nnpe+0] * (1.0 - s01);
	  az = coords[2*nnpe+1] * s01 + coords[2*nnpe+0] * (1.0 - s01);
	  i+=1;
	  j++;
	}
      else if (id[1] < id[0])
	{
	  s10 = (level - v[1]) / (v[0] - v[1]);
	  ax = coords[0*nnpe+0] * s10 + coords[0*nnpe+1] * (1.0 - s10);
	  ay = coords[1*nnpe+0] * s10 + coords[1*nnpe+1] * (1.0 - s10);
	  az = coords[2*nnpe+0] * s10 + coords[2*nnpe+1] * (1.0 - s10);
	  i+=1;
	  j++;
	}
      if (id[1] < id[2])
	{
	  s12 = (level - v[1]) / (v[2] - v[1]);
	  bx = coords[0*nnpe+2] * s12 + coords[0*nnpe+1] * (1.0 - s12);
	  by = coords[1*nnpe+2] * s12 + coords[1*nnpe+1] * (1.0 - s12);
	  bz = coords[2*nnpe+2] * s12 + coords[2*nnpe+1] * (1.0 - s12);
	  if (j == 0)
	    { ax = bx; ay = by; az = bz; }
	  i+=2;
	  j++;
	}
      else if (id[2] < id[1])
	{
	  s21 = (level - v[2]) / (v[1] - v[2]);
	  bx = coords[0*nnpe+1] * s21 + coords[0*nnpe+2] * (1.0 - s21);
	  by = coords[1*nnpe+1] * s21 + coords[1*nnpe+2] * (1.0 - s21);
	  bz = coords[2*nnpe+1] * s21 + coords[2*nnpe+2] * (1.0 - s21);
	  if (j == 0)
	    { ax = bx; ay = by; az = bz; }
	  i+=2;
	  j++;
	}
      if (id[2] < id[0])
	{
	  s20 = (level - v[2]) / (v[0] - v[2]);
	  bx = coords[0*nnpe+0] * s20 + coords[0*nnpe+2] * (1.0 - s20);
	  by = coords[1*nnpe+0] * s20 + coords[1*nnpe+2] * (1.0 - s20);
	  bz = coords[2*nnpe+0] * s20 + coords[2*nnpe+2] * (1.0 - s20);
	  i+=4;
	  j++;
	}
      else if (id[0] < id[2])
	{
	  s02 = (level - v[0]) / (v[2] - v[0]);
	  bx = coords[0*nnpe+2] * s02 + coords[0*nnpe+0] * (1.0 - s02);
	  by = coords[1*nnpe+2] * s02 + coords[1*nnpe+0] * (1.0 - s02);
	  bz = coords[2*nnpe+2] * s02 + coords[2*nnpe+0] * (1.0 - s02);
	  i+=4;
	  j++;
	}
      if (id[3] < id[0])
	{
	  s30 = (level - v[3]) / (v[0] - v[3]);
	  cx = coords[0*nnpe+0] * s30 + coords[0*nnpe+3] * (1.0 - s30);
	  cy = coords[1*nnpe+0] * s30 + coords[1*nnpe+3] * (1.0 - s30);
	  cz = coords[2*nnpe+0] * s30 + coords[2*nnpe+3] * (1.0 - s30);
	  if (j == 0)
	    { ax = cx; ay = cy; az = cz; }
	  i+=8;
	  j++;
	}
      else if (id[0] < id[3])
	{
	  s03 = (level - v[0]) / (v[3] - v[0]);
	  cx = coords[0*nnpe+3] * s03 + coords[0*nnpe+0] * (1.0 - s03);
	  cy = coords[1*nnpe+3] * s03 + coords[1*nnpe+0] * (1.0 - s03);
	  cz = coords[2*nnpe+3] * s03 + coords[2*nnpe+0] * (1.0 - s03);
	  if (j == 0)
	    { ax = cx; ay = cy; az = cz; }
	  i+=8;
	  j++;
	}
      if (id[3] < id[1])
	{
	  s31 = (level - v[3]) / (v[1] - v[3]);
	  dx = coords[0*nnpe+1] * s31 + coords[0*nnpe+3] * (1.0 - s31);
	  dy = coords[1*nnpe+1] * s31 + coords[1*nnpe+3] * (1.0 - s31);
	  dz = coords[2*nnpe+1] * s31 + coords[2*nnpe+3] * (1.0 - s31);
	  if (j == 1)
	    { bx = dx; by = dy; bz = dz; }
	  else if (j == 2)
	    { cx = dx; cy = dy; cz = dz; }
	  i+=16;
	  j++;
	}
      else if (id[1] < id[3])
	{
	  s13 = (level - v[1]) / (v[3] - v[1]);
	  dx = coords[0*nnpe+3] * s13 + coords[0*nnpe+1] * (1.0 - s13);
	  dy = coords[1*nnpe+3] * s13 + coords[1*nnpe+1] * (1.0 - s13);
	  dz = coords[2*nnpe+3] * s13 + coords[2*nnpe+1] * (1.0 - s13);
	  if (j == 1)
	    { bx = dx; by = dy; bz = dz; }
	  else if (j == 2)
	    { cx = dx; cy = dy; cz = dz; }
	  i+=16;
	  j++;
	}
      if (id[3] < id[2])
	{
	  s32 = (level - v[3]) / (v[2] - v[3]);
	  dx = coords[0*nnpe+2] * s32 + coords[0*nnpe+3] * (1.0 - s32);
	  dy = coords[1*nnpe+2] * s32 + coords[1*nnpe+3] * (1.0 - s32);
	  dz = coords[2*nnpe+2] * s32 + coords[2*nnpe+3] * (1.0 - s32);
	  if (j == 2)
	    { cx = dx; cy = dy; cz = dz; }
	  i+=32;
	  j++;
	}
      else if (id[2] < id[3])
	{
	  s23 = (level - v[2]) / (v[3] - v[2]);
	  dx = coords[0*nnpe+3] * s23 + coords[0*nnpe+2] * (1.0 - s23);
	  dy = coords[1*nnpe+3] * s23 + coords[1*nnpe+2] * (1.0 - s23);
	  dz = coords[2*nnpe+3] * s23 + coords[2*nnpe+2] * (1.0 - s23);
	  if (j == 2)
	    { cx = dx; cy = dy; cz = dz; }
	  i+=32;
	  j++;
	}

      if (j == 3) /* triangle */
	{
	  if (i == 1+2+16)  /* node 1 / node 0, node 2, node 3 */
	    {
	      if (id[1] < 0)
		fvol = s10 * s12 * s13;
	      else
		fvol = 1.0 - (1.0 - s01)*(1.0 - s21)*(1.0 - s31);
	    }
	  else if (i == 2+4+32)  /* node 2 / node 0, node 1, node 3 */
	    {
	      if (id[2] < 0)
		fvol = s20 * s21 * s23;
	      else
		fvol = 1.0 - (1.0 - s02)*(1.0 - s12)*(1.0 - s32);
	    }
	  else if (i == 1+4+8)  /* node 0 / node 1, node 2, node 3 */
	    {
	      if (id[0] < 0)
		fvol = s01 * s02 * s03;
	      else
		fvol = 1.0 - (1.0 - s10)*(1.0 - s20)*(1.0 - s30);
	    }
	  else if (i == 8+16+32)  /* node 3 / node 0, node 1, node 2 */
	    {
	      if (id[3] < 0)
		fvol = s30 * s31 * s32;
	      else
		fvol = 1.0 - (1.0 - s03)*(1.0 - s13)*(1.0 - s23);
	    }
	  
	  coot[0] = ax; coot[1] = bx; coot[2] = cx;
	  coot[3] = ay; coot[4] = by; coot[5] = cy;
	  coot[6] = az; coot[7] = bz; coot[8] = cz;

	  coop[0] = coords[0*nnpe+0]; 
	  coop[1] = coords[1*nnpe+0];
	  coop[2] = coords[2*nnpe+0];
	  dists[0] = dp2tri (coop, coot, nsd);

	  coop[0] = coords[0*nnpe+1]; 
	  coop[1] = coords[1*nnpe+1];
	  coop[2] = coords[2*nnpe+1];
	  dists[1] = dp2tri (coop, coot, nsd);

	  coop[0] = coords[0*nnpe+2]; 
	  coop[1] = coords[1*nnpe+2];
	  coop[2] = coords[2*nnpe+2];
	  dists[2] = dp2tri (coop, coot, nsd);

	  coop[0] = coords[0*nnpe+3]; 
	  coop[1] = coords[1*nnpe+3];
	  coop[2] = coords[2*nnpe+3];
	  dists[3] = dp2tri (coop, coot, nsd);

	} 
      else   /* quadrilateral */
	{
	  if (i == 1+2+32+8)  /* node 1, node 3 / node 0, node 2 */
	    {
	      if (id[1] < 0)
		fvol = s10*s12 + s30*s32 + s10*s32 -
		        s10*s12*s32 - s10*s30*s32;
	      else
		fvol = 1.0 - ((1.0 - s01)*(1.0 - s21)+
			      (1.0 - s03)*(1.0 - s23)+
			      (1.0 - s01)*(1.0 - s23)-
			      (1.0 - s01)*(1.0 - s21)*(1.0 - s23)-
			      (1.0 - s01)*(1.0 - s03)*(1.0 - s23));

	      cooq[0] = ax; cooq[1] = bx; cooq[2] = dx; cooq[3] = cx;
	      cooq[4] = ay; cooq[5] = by; cooq[6] = dy; cooq[7] = cy;
	      cooq[8] = az; cooq[9] = bz; cooq[10] = dz; cooq[11] = cz;

	    }
	  else if (i == 2+4+8+16)  /* node 2, node 3 / node 0, node 1 */
	    {
	      if (id[2] < 0)
		fvol = s21*s20 + s31*s30 + s21*s30 -
                        s21*s20*s30 - s21*s31*s30;
	      else
		fvol = 1.0 - ((1.0 - s12)*(1.0 - s02)+
			      (1.0 - s13)*(1.0 - s03)+
			      (1.0 - s12)*(1.0 - s03)-
			      (1.0 - s12)*(1.0 - s02)*(1.0 - s03)-
			      (1.0 - s12)*(1.0 - s13)*(1.0 - s03));

	      cooq[0] = ax; cooq[1] = bx; cooq[2] = cx; cooq[3] = dx;
	      cooq[4] = ay; cooq[5] = by; cooq[6] = cy; cooq[7] = dy;
	      cooq[8] = az; cooq[9] = bz; cooq[10] = cz; cooq[11] = dz;

	    }
	  else if (i == 1+4+32+16)  /* node 1, node 2 / node 0, node 3 */
	    {
	      if (id[1] < 0)
		fvol = s13*s10 + s23*s20 + s13*s20 -
                        s13*s10*s20 - s13*s23*s20;
	      else 
		fvol = 1.0 - ((1.0 - s31)*(1.0 - s01)+
			      (1.0 - s32)*(1.0 - s02)+
			      (1.0 - s31)*(1.0 - s02)-
			      (1.0 - s31)*(1.0 - s01)*(1.0 - s02)-
			      (1.0 - s31)*(1.0 - s32)*(1.0 - s02));

	      cooq[0] = ax; cooq[1] = bx; cooq[2] = dx; cooq[3] = cx;
	      cooq[4] = ay; cooq[5] = by; cooq[6] = dy; cooq[7] = cy;
	      cooq[8] = az; cooq[9] = bz; cooq[10] = dz; cooq[11] = cz;

	    }

	  coop[0] = coords[0*nnpe+0]; 
	  coop[1] = coords[1*nnpe+0];
	  coop[2] = coords[2*nnpe+0];
	  dists[0] = dp2qua (coop, cooq, nsd);

	  coop[0] = coords[0*nnpe+1]; 
	  coop[1] = coords[1*nnpe+1];
	  coop[2] = coords[2*nnpe+1];
	  dists[1] = dp2qua (coop, cooq, nsd);

	  coop[0] = coords[0*nnpe+2]; 
	  coop[1] = coords[1*nnpe+2];
	  coop[2] = coords[2*nnpe+2];
	  dists[2] = dp2qua (coop, cooq, nsd);

	  coop[0] = coords[0*nnpe+3]; 
	  coop[1] = coords[1*nnpe+3];
	  coop[2] = coords[2*nnpe+3];
	  dists[3] = dp2qua (coop, cooq, nsd);

	}

      fvol = 1.0 - fvol;
      *parvol = fvol * volume;

    }
}

/* fracvol2_: compute partial volume in the interface element         */
/* coords[i*nnpe + j]: coordinate 'i' of the 'j'th node               */
/* nsd: number of space dimensions                                    */
/* v: characteristic function in each node                            */
/* level: level set value                                             */
/* volume: total volume of the element (return value)                 */
/* nintp: number of interpolate points of level set                   */
/* intpoints[i*nnpe + j]: coordinate 'i' of the 'j'th interface point */
/* sizeint: size of the interface                                     */
double fracvol2_(double *coords, int *nsd, double *v, double *level, 
		 double *volume, int *nintp, double *intpoints, 
		 double *sizeint)
{
  int i;
  int nnpe;
  int max, min, id[4];
  double fvol;

  /* Check for number of space dimension */
  if ((*nsd < 1)||(*nsd >3))
    return -1;

  /* Simplex: number of nodes per element: number of space dimensions + 1 */
  nnpe = *nsd + 1;
  
  /* Identifying nodes */
  max = min = 0;
  for (i = 0; i < nnpe; i++)
    if (v[i] > *level)
      {
	max++;
	id[i] = +1;
      }
    else
      {
	min++;
	id[i] = -1;
      }
  
  if (*nsd == 1)
    {  /* *nsd == 1 */
      double ax;
      double s01, s10;

      *volume = (coords[1]-coords[0])*(coords[1]-coords[0]);
      *volume = sqrt(*volume);
      
      if (max == nnpe)
	{
	  *nintp = 0;
	  *sizeint = 0.0;
	  fvol = 0.0;
	  fvol = 1.0 - fvol;
	  return fvol;
	}
      else if (min == nnpe)
	{
	  *nintp = 0;
	  *sizeint = 0.0;
	  fvol = 1.0;
	  fvol = 1.0 - fvol;
	  return fvol;
	}

      if (id[0] < id[1])
	{
	  s01 = (*level - v[0]) / (v[1] - v[0]);
	  ax = coords[0*nnpe+1] * s01 + coords[0*nnpe+0] * (1.0 - s01);
	  fvol = s01;
	}
      else if (id[1] < id[0])
	{
	  s10 = (*level - v[1]) / (v[0] - v[1]);
	  ax = coords[0*nnpe+0] * s10 + coords[0*nnpe+1] * (1.0 - s10);
	  fvol = s10;
	}
      
      *nintp = 1;
      intpoints[0] = ax;
      *sizeint = 1;

    }
  else if (*nsd == 2)
    { /* *nsd == 2 */
      double ax, ay, bx, by;
      double s01, s12, s20, s10, s21, s02;

      ax = (coords[0*nnpe+1]-coords[0*nnpe+0]);
      ay = (coords[1*nnpe+1]-coords[1*nnpe+0]);
      bx = (coords[0*nnpe+2]-coords[0*nnpe+0]);
      by = (coords[1*nnpe+2]-coords[1*nnpe+0]);

      *volume = 0.5 * (ax*by - bx*ay);

      if (max == nnpe)
	{
	  *nintp = 0;
	  *sizeint = 0.0;
	  fvol = 0.0;
	  fvol = 1.0 - fvol;
	  return fvol;
	}
      else if (min == nnpe)
	{
	  *nintp = 0;
	  *sizeint = 0.0;
	  fvol = 1.0;
	  fvol = 1.0 - fvol;
	  return fvol;
	}

      *nintp = 2;

      i = 0;
      if (id[0] < id[1])
	{
	  s01 = (*level - v[0]) / (v[1] - v[0]);
	  ax = coords[0*nnpe+1] * s01 + coords[0*nnpe+0] * (1.0 - s01);
	  ay = coords[1*nnpe+1] * s01 + coords[1*nnpe+0] * (1.0 - s01);
	  i+=1;
	}
      else if (id[1] < id[0])
	{
	  s10 = (*level - v[1]) / (v[0] - v[1]);
	  ax = coords[0*nnpe+0] * s10 + coords[0*nnpe+1] * (1.0 - s10);
	  ay = coords[1*nnpe+0] * s10 + coords[1*nnpe+1] * (1.0 - s10);
	  i+=1;
	}
      if (id[1] < id[2])
	{
	  s12 = (*level - v[1]) / (v[2] - v[1]);
	  bx = coords[0*nnpe+2] * s12 + coords[0*nnpe+1] * (1.0 - s12);
	  by = coords[1*nnpe+2] * s12 + coords[1*nnpe+1] * (1.0 - s12);
	  if (i == 0)
	    { ax = bx; ay = by; }
	  i+=2;
	}
      else if (id[2] < id[1])
	{
	  s21 = (*level - v[2]) / (v[1] - v[2]);
	  bx = coords[0*nnpe+1] * s21 + coords[0*nnpe+2] * (1.0 - s21);
	  by = coords[1*nnpe+1] * s21 + coords[1*nnpe+2] * (1.0 - s21);
	  if (i == 0)
	    { ax = bx; ay = by; }
	  i+=2;
	}
      if (id[2] < id[0])
	{
	  s20 = (*level - v[2]) / (v[0] - v[2]);
	  bx = coords[0*nnpe+0] * s20 + coords[0*nnpe+2] * (1.0 - s20);
	  by = coords[1*nnpe+0] * s20 + coords[1*nnpe+2] * (1.0 - s20);
	  i+=4;
	}
      else if (id[0] < id[2])
	{
	  s02 = (*level - v[0]) / (v[2] - v[0]);
	  bx = coords[0*nnpe+2] * s02 + coords[0*nnpe+0] * (1.0 - s02);
	  by = coords[1*nnpe+2] * s02 + coords[1*nnpe+0] * (1.0 - s02);
	  i+=4;
	}

      if (i == 1+2)   /* node 1 / node 0, node 2 */
	{
	  if (id[1] < 0)
	    {
	      fvol = s10 * s12;
	      intpoints[0*(*nintp)+0] = ax;
	      intpoints[1*(*nintp)+0] = ay;
	      intpoints[0*(*nintp)+1] = bx;
	      intpoints[1*(*nintp)+1] = by;
	    }
	  else
	    {
	      fvol = 1.0 - (1.0 - s01) * (1.0 - s21);
	      intpoints[0*(*nintp)+0] = bx;
	      intpoints[1*(*nintp)+0] = by;
	      intpoints[0*(*nintp)+1] = ax;
	      intpoints[1*(*nintp)+1] = ay;
	    }
	}
      else if (i == 2+4)   /* node 2 / node 1, node 0 */  
	{
	  if (id[2] < 0)
	    {
	      fvol = s21 * s20;
	      intpoints[0*(*nintp)+0] = ax;
	      intpoints[1*(*nintp)+0] = ay;
	      intpoints[0*(*nintp)+1] = bx;
	      intpoints[1*(*nintp)+1] = by;
	    }
	  else
	    {
	      fvol = 1.0 - (1.0 - s12) * (1.0 - s02);
	      intpoints[0*(*nintp)+0] = bx;
	      intpoints[1*(*nintp)+0] = by;
	      intpoints[0*(*nintp)+1] = ax;
	      intpoints[1*(*nintp)+1] = ay;
	    }
	}
      if (i == 4+1)   /* node 0 / node 2, node 1 */  
	{
	  if (id[0] < 0)
	    {
	      fvol = s02 * s01;
	      intpoints[0*(*nintp)+0] = bx;
	      intpoints[1*(*nintp)+0] = by;
	      intpoints[0*(*nintp)+1] = ax;
	      intpoints[1*(*nintp)+1] = ay;
	    }
	  else
	    {
	      fvol = 1.0 - (1.0 - s20) * (1.0 - s10);
	      intpoints[0*(*nintp)+0] = ax;
	      intpoints[1*(*nintp)+0] = ay;
	      intpoints[0*(*nintp)+1] = bx;
	      intpoints[1*(*nintp)+1] = by;
	    }
	}

      fvol = 1.0 - fvol;
      *sizeint = sqrt((bx-ax)*(bx-ax) + (by-ay)*(by-ay));
      
    }
  else if (*nsd == 3)
    { /* *nsd == 3 */
      int j;
      double ax,ay,az, bx,by,bz, cx,cy,cz, dx,dy,dz;
      double s01,s02,s03, s10,s12,s13, s20,s21,s23, s30,s31,s32; 

      ax = (coords[0*nnpe+1] - coords[0*nnpe+0]);
      bx = (coords[0*nnpe+2] - coords[0*nnpe+0]);
      cx = (coords[0*nnpe+3] - coords[0*nnpe+0]);
      ay = (coords[1*nnpe+1] - coords[1*nnpe+0]);
      by = (coords[1*nnpe+2] - coords[1*nnpe+0]);
      cy = (coords[1*nnpe+3] - coords[1*nnpe+0]);
      az = (coords[2*nnpe+1] - coords[2*nnpe+0]);
      bz = (coords[2*nnpe+2] - coords[2*nnpe+0]);
      cz = (coords[2*nnpe+3] - coords[2*nnpe+0]);

      *volume = ax*(by*cz-bz*cy)+ay*(bz*cx-bx*cz)+az*(bx*cy-by*cx);
      *volume = *volume / 6.0;

      if (max == nnpe)
	{
	  *nintp = 0;
	  *sizeint = 0.0;
	  fvol = 0.0;
	  fvol = 1.0 - fvol;
	  return fvol;
	}
      else if (min == nnpe)
	{
	  *nintp = 0;
	  *sizeint = 0.0;
	  fvol = 1.0;
	  fvol = 1.0 - fvol;
	  return fvol;
	}

      i = 0;
      j = 0;
      if (id[0] < id[1])
	{
	  s01 = (*level - v[0]) / (v[1] - v[0]);
	  ax = coords[0*nnpe+1] * s01 + coords[0*nnpe+0] * (1.0 - s01);
	  ay = coords[1*nnpe+1] * s01 + coords[1*nnpe+0] * (1.0 - s01);
	  az = coords[2*nnpe+1] * s01 + coords[2*nnpe+0] * (1.0 - s01);
	  i+=1;
	  j++;
	}
      else if (id[1] < id[0])
	{
	  s10 = (*level - v[1]) / (v[0] - v[1]);
	  ax = coords[0*nnpe+0] * s10 + coords[0*nnpe+1] * (1.0 - s10);
	  ay = coords[1*nnpe+0] * s10 + coords[1*nnpe+1] * (1.0 - s10);
	  az = coords[2*nnpe+0] * s10 + coords[2*nnpe+1] * (1.0 - s10);
	  i+=1;
	  j++;
	}
      if (id[1] < id[2])
	{
	  s12 = (*level - v[1]) / (v[2] - v[1]);
	  bx = coords[0*nnpe+2] * s12 + coords[0*nnpe+1] * (1.0 - s12);
	  by = coords[1*nnpe+2] * s12 + coords[1*nnpe+1] * (1.0 - s12);
	  bz = coords[2*nnpe+2] * s12 + coords[2*nnpe+1] * (1.0 - s12);
	  if (j == 0)
	    { ax = bx; ay = by; az = bz; }
	  i+=2;
	  j++;
	}
      else if (id[2] < id[1])
	{
	  s21 = (*level - v[2]) / (v[1] - v[2]);
	  bx = coords[0*nnpe+1] * s21 + coords[0*nnpe+2] * (1.0 - s21);
	  by = coords[1*nnpe+1] * s21 + coords[1*nnpe+2] * (1.0 - s21);
	  bz = coords[2*nnpe+1] * s21 + coords[2*nnpe+2] * (1.0 - s21);
	  if (j == 0)
	    { ax = bx; ay = by; az = bz; }
	  i+=2;
	  j++;
	}
      if (id[2] < id[0])
	{
	  s20 = (*level - v[2]) / (v[0] - v[2]);
	  bx = coords[0*nnpe+0] * s20 + coords[0*nnpe+2] * (1.0 - s20);
	  by = coords[1*nnpe+0] * s20 + coords[1*nnpe+2] * (1.0 - s20);
	  bz = coords[2*nnpe+0] * s20 + coords[2*nnpe+2] * (1.0 - s20);
	  i+=4;
	  j++;
	}
      else if (id[0] < id[2])
	{
	  s02 = (*level - v[0]) / (v[2] - v[0]);
	  bx = coords[0*nnpe+2] * s02 + coords[0*nnpe+0] * (1.0 - s02);
	  by = coords[1*nnpe+2] * s02 + coords[1*nnpe+0] * (1.0 - s02);
	  bz = coords[2*nnpe+2] * s02 + coords[2*nnpe+0] * (1.0 - s02);
	  i+=4;
	  j++;
	}
      if (id[3] < id[0])
	{
	  s30 = (*level - v[3]) / (v[0] - v[3]);
	  cx = coords[0*nnpe+0] * s30 + coords[0*nnpe+3] * (1.0 - s30);
	  cy = coords[1*nnpe+0] * s30 + coords[1*nnpe+3] * (1.0 - s30);
	  cz = coords[2*nnpe+0] * s30 + coords[2*nnpe+3] * (1.0 - s30);
	  if (j == 0)
	    { ax = cx; ay = cy; az = cz; }
	  i+=8;
	  j++;
	}
      else if (id[0] < id[3])
	{
	  s03 = (*level - v[0]) / (v[3] - v[0]);
	  cx = coords[0*nnpe+3] * s03 + coords[0*nnpe+0] * (1.0 - s03);
	  cy = coords[1*nnpe+3] * s03 + coords[1*nnpe+0] * (1.0 - s03);
	  cz = coords[2*nnpe+3] * s03 + coords[2*nnpe+0] * (1.0 - s03);
	  if (j == 0)
	    { ax = cx; ay = cy; az = cz; }
	  i+=8;
	  j++;
	}
      if (id[3] < id[1])
	{
	  s31 = (*level - v[3]) / (v[1] - v[3]);
	  dx = coords[0*nnpe+1] * s31 + coords[0*nnpe+3] * (1.0 - s31);
	  dy = coords[1*nnpe+1] * s31 + coords[1*nnpe+3] * (1.0 - s31);
	  dz = coords[2*nnpe+1] * s31 + coords[2*nnpe+3] * (1.0 - s31);
	  if (j == 1)
	    { bx = dx; by = dy; bz = dz; }
	  else if (j == 2)
	    { cx = dx; cy = dy; cz = dz; }
	  i+=16;
	  j++;
	}
      else if (id[1] < id[3])
	{
	  s13 = (*level - v[1]) / (v[3] - v[1]);
	  dx = coords[0*nnpe+3] * s13 + coords[0*nnpe+1] * (1.0 - s13);
	  dy = coords[1*nnpe+3] * s13 + coords[1*nnpe+1] * (1.0 - s13);
	  dz = coords[2*nnpe+3] * s13 + coords[2*nnpe+1] * (1.0 - s13);
	  if (j == 1)
	    { bx = dx; by = dy; bz = dz; }
	  else if (j == 2)
	    { cx = dx; cy = dy; cz = dz; }
	  i+=16;
	  j++;
	}
      if (id[3] < id[2])
	{
	  s32 = (*level - v[3]) / (v[2] - v[3]);
	  dx = coords[0*nnpe+2] * s32 + coords[0*nnpe+3] * (1.0 - s32);
	  dy = coords[1*nnpe+2] * s32 + coords[1*nnpe+3] * (1.0 - s32);
	  dz = coords[2*nnpe+2] * s32 + coords[2*nnpe+3] * (1.0 - s32);
	  if (j == 2)
	    { cx = dx; cy = dy; cz = dz; }
	  i+=32;
	  j++;
	}
      else if (id[2] < id[3])
	{
	  s23 = (*level - v[2]) / (v[3] - v[2]);
	  dx = coords[0*nnpe+3] * s23 + coords[0*nnpe+2] * (1.0 - s23);
	  dy = coords[1*nnpe+3] * s23 + coords[1*nnpe+2] * (1.0 - s23);
	  dz = coords[2*nnpe+3] * s23 + coords[2*nnpe+2] * (1.0 - s23);
	  if (j == 2)
	    { cx = dx; cy = dy; cz = dz; }
	  i+=32;
	  j++;
	}

      if (j == 3)   /* triangle */
	{
	  *nintp = 3;
	  if (i == 1+2+16)  /* node 1 / node 0, node 2, node 3 */
	    {
	      if (id[1] < 0)
		{
		  fvol = s10 * s12 * s13;
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = bx;
		  intpoints[1*(*nintp)+1] = by;
		  intpoints[2*(*nintp)+1] = bz;
		  intpoints[0*(*nintp)+2] = cx;
		  intpoints[1*(*nintp)+2] = cy;
		  intpoints[2*(*nintp)+2] = cz;
		}
	      else
		{
		  fvol = 1.0 - (1.0 - s01)*(1.0 - s21)*(1.0 - s31);
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = cx;
		  intpoints[1*(*nintp)+1] = cy;
		  intpoints[2*(*nintp)+1] = cz;
		  intpoints[0*(*nintp)+2] = bx;
		  intpoints[1*(*nintp)+2] = by;
		  intpoints[2*(*nintp)+2] = bz;
		}
	    }
	  if (i == 2+4+32) /* node 2 / node 1, node 0, node 3 */
	    {
	      if (id[2] < 0)
		{	
		  fvol = s21 * s20 * s23;
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = bx;
		  intpoints[1*(*nintp)+1] = by;
		  intpoints[2*(*nintp)+1] = bz;
		  intpoints[0*(*nintp)+2] = cx;
		  intpoints[1*(*nintp)+2] = cy;
		  intpoints[2*(*nintp)+2] = cz;

		}
	      else
		{
		  fvol = 1.0 - (1.0 - s12)*(1.0 - s02)*(1.0 - s32);
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = cx;
		  intpoints[1*(*nintp)+1] = cy;
		  intpoints[2*(*nintp)+1] = cz;
		  intpoints[0*(*nintp)+2] = bx;
		  intpoints[1*(*nintp)+2] = by;
		  intpoints[2*(*nintp)+2] = bz;
		}
	    }
	  if (i == 4+1+8) /* node 0 / node 2, node 1, node 3 */
	    {
	      if (id[0] < 0)
		{
		  fvol = s02 * s01 * s03;
		  intpoints[0*(*nintp)+0] = bx;
		  intpoints[1*(*nintp)+0] = by;
		  intpoints[2*(*nintp)+0] = bz;
		  intpoints[0*(*nintp)+1] = ax;
		  intpoints[1*(*nintp)+1] = ay;
		  intpoints[2*(*nintp)+1] = az;
		  intpoints[0*(*nintp)+2] = cx;
		  intpoints[1*(*nintp)+2] = cy;
		  intpoints[2*(*nintp)+2] = cz;
		}
	      else
		{
		  fvol = 1.0 - (1.0 - s20)*(1.0 - s10)*(1.0 - s30);
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = bx;
		  intpoints[1*(*nintp)+1] = by;
		  intpoints[2*(*nintp)+1] = bz;
		  intpoints[0*(*nintp)+2] = cx;
		  intpoints[1*(*nintp)+2] = cy;
		  intpoints[2*(*nintp)+2] = cz;
		}
	    }
      	  if (i == 8+16+32) /* node 3 / node 0, node 1, node 2 */
	    {
	      if (id[3] < 0)
		{
		  fvol = s30 * s31 * s32;
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = bx;
		  intpoints[1*(*nintp)+1] = by;
		  intpoints[2*(*nintp)+1] = bz;
		  intpoints[0*(*nintp)+2] = cx;
		  intpoints[1*(*nintp)+2] = cy;
		  intpoints[2*(*nintp)+2] = cz;
		}
	      else
		{
		  fvol = 1.0 - (1.0 - s03)*(1.0 - s13)*(1.0 - s23);
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = cx;
		  intpoints[1*(*nintp)+1] = cy;
		  intpoints[2*(*nintp)+1] = cz;
		  intpoints[0*(*nintp)+2] = bx;
		  intpoints[1*(*nintp)+2] = by;
		  intpoints[2*(*nintp)+2] = bz;
		}
	    }
	
	  ax = intpoints[0*(*nintp)+1] - intpoints[0*(*nintp)+0];
	  ay = intpoints[1*(*nintp)+1] - intpoints[1*(*nintp)+0];
	  az = intpoints[2*(*nintp)+1] - intpoints[2*(*nintp)+0];
	  bx = intpoints[0*(*nintp)+2] - intpoints[0*(*nintp)+0];
	  by = intpoints[1*(*nintp)+2] - intpoints[1*(*nintp)+0];
	  bz = intpoints[2*(*nintp)+2] - intpoints[2*(*nintp)+0];
	  
	  *sizeint = 0.5 * sqrt((ay*bz-az*by)*(ay*bz-az*by) + 
				(az*bx-ax*bz)*(az*bx-ax*bz) +
				(ax*by-ay*bx)*(ax*by-ay*bx));
	}
      else   /* quadrilateral */ 
	{
	  *nintp = 4;
	  if (i == 1+2+32+8)  /* node 1, node 3 / node 0, node 2 */
	    {
	      if (id[1] < 0)
		{		
		  fvol = s10*s12 + s30*s32 + s10*s32 - 
		            s10*s12*s32 - s10*s30*s32;
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = bx;
		  intpoints[1*(*nintp)+1] = by;
		  intpoints[2*(*nintp)+1] = bz;
		  intpoints[0*(*nintp)+2] = dx;
		  intpoints[1*(*nintp)+2] = dy;
		  intpoints[2*(*nintp)+2] = dz;
		  intpoints[0*(*nintp)+3] = cx;
		  intpoints[1*(*nintp)+3] = cy;
		  intpoints[2*(*nintp)+3] = cz;
		}
	      else
		{
		  fvol = 1.0 - ((1.0 - s01)*(1.0 - s21)+
				   (1.0 - s03)*(1.0 - s23)+
				   (1.0 - s01)*(1.0 - s23)-
				   (1.0 - s01)*(1.0 - s21)*(1.0 - s23)-
				   (1.0 - s01)*(1.0 - s03)*(1.0 - s23));
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = cx;
		  intpoints[1*(*nintp)+1] = cy;
		  intpoints[2*(*nintp)+1] = cz;
		  intpoints[0*(*nintp)+2] = dx;
		  intpoints[1*(*nintp)+2] = dy;
		  intpoints[2*(*nintp)+2] = dz;
		  intpoints[0*(*nintp)+3] = bx;
		  intpoints[1*(*nintp)+3] = by;
		  intpoints[2*(*nintp)+3] = bz;
		}
	    }
	  if (i == 2+4+8+16)  /* node 2, node 3 / node 0, node 1 */
	    {
	      if (id[2] < 0)
		{
		  fvol = s21*s20 + s31*s30 + s21*s30 -
		            s21*s20*s30 - s21*s31*s30;
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = bx;
		  intpoints[1*(*nintp)+1] = by;
		  intpoints[2*(*nintp)+1] = bz;
		  intpoints[0*(*nintp)+2] = cx;
		  intpoints[1*(*nintp)+2] = cy;
		  intpoints[2*(*nintp)+2] = cz;
		  intpoints[0*(*nintp)+3] = dx;
		  intpoints[1*(*nintp)+3] = dy;
		  intpoints[2*(*nintp)+3] = dz;
		}
	      else
		{
		  fvol = 1.0 - ((1.0 - s12)*(1.0 - s02)+
				   (1.0 - s13)*(1.0 - s03)+
				   (1.0 - s12)*(1.0 - s03)-
				   (1.0 - s12)*(1.0 - s02)*(1.0 - s03)-
				   (1.0 - s12)*(1.0 - s13)*(1.0 - s03));
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = dx;
		  intpoints[1*(*nintp)+1] = dy;
		  intpoints[2*(*nintp)+1] = dz;
		  intpoints[0*(*nintp)+2] = cx;
		  intpoints[1*(*nintp)+2] = cy;
		  intpoints[2*(*nintp)+2] = cz;
		  intpoints[0*(*nintp)+3] = bx;
		  intpoints[1*(*nintp)+3] = by;
		  intpoints[2*(*nintp)+3] = bz;
		}
	    }
	  if (i == 1+4+32+16)  /* node 1, node 2 / node 0, node 3 */
	    {
	      if (id[1] < 0)
		{
		  fvol = s13*s10 + s23*s20 + s13*s20 -
		            s13*s10*s20 - s13*s23*s20;
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = bx;
		  intpoints[1*(*nintp)+1] = by;
		  intpoints[2*(*nintp)+1] = bz;
		  intpoints[0*(*nintp)+2] = dx;
		  intpoints[1*(*nintp)+2] = dy;
		  intpoints[2*(*nintp)+2] = dz;
		  intpoints[0*(*nintp)+3] = cx;
		  intpoints[1*(*nintp)+3] = cy;
		  intpoints[2*(*nintp)+3] = cz;
		}
	      else
		{
		  fvol = 1.0 - ((1.0 - s31)*(1.0 - s01)+
				   (1.0 - s32)*(1.0 - s02)+
				   (1.0 - s31)*(1.0 - s02)-
				   (1.0 - s31)*(1.0 - s01)*(1.0 - s02)-
				   (1.0 - s31)*(1.0 - s32)*(1.0 - s02));
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = cx;
		  intpoints[1*(*nintp)+1] = cy;
		  intpoints[2*(*nintp)+1] = cz;
		  intpoints[0*(*nintp)+2] = dx;
		  intpoints[1*(*nintp)+2] = dy;
		  intpoints[2*(*nintp)+2] = dz;
		  intpoints[0*(*nintp)+3] = bx;
		  intpoints[1*(*nintp)+3] = by;
		  intpoints[2*(*nintp)+3] = bz;
		}
	    }
	  
	  ax = intpoints[0*(*nintp)+1] - intpoints[0*(*nintp)+0];
	  ay = intpoints[1*(*nintp)+1] - intpoints[1*(*nintp)+0];
	  az = intpoints[2*(*nintp)+1] - intpoints[2*(*nintp)+0];
	  bx = intpoints[0*(*nintp)+2] - intpoints[0*(*nintp)+0];
	  by = intpoints[1*(*nintp)+2] - intpoints[1*(*nintp)+0];
	  bz = intpoints[2*(*nintp)+2] - intpoints[2*(*nintp)+0];
	  cx = intpoints[0*(*nintp)+3] - intpoints[0*(*nintp)+0];
	  cy = intpoints[1*(*nintp)+3] - intpoints[1*(*nintp)+0];
	  cz = intpoints[2*(*nintp)+3] - intpoints[2*(*nintp)+0];

	  *sizeint = 0.5 * sqrt((ay*bz-az*by)*(ay*bz-az*by) + 
				(az*bx-ax*bz)*(az*bx-ax*bz) +
				(ax*by-ay*bx)*(ax*by-ay*bx));

	  *sizeint += 0.5 * sqrt((by*cz-bz*cy)*(by*cz-bz*cy) + 
				 (bz*cx-bx*cz)*(bz*cx-bx*cz) +
				 (bx*cy-by*cx)*(bx*cy-by*cx));
	}

      fvol = 1.0 - fvol;

    }

  return fvol;
}

/* parvol2_: compute partial volume in the interface element          */
/* coords[i*nnpe + j]: coordinate 'i' of the 'j'th node               */
/* nsd: number of space dimensions                                    */
/* v: characteristic function in each node                            */
/* level: level set value                                             */
/* volume: total volume of the element (return value)                 */
/* sizeint: size of the interface                                     */
double parvol2(double *coords, int nsd, double *v, double level, 
	       double *volume, double *sizeint)
{
  int i;
  int nnpe;
  int id[4];
  double fvol, pvol;

  /* Simplex: number of nodes per element: number of space dimensions + 1 */
  nnpe = nsd + 1;
  
  /* Identifying nodes */
  for (i = 0; i < nnpe; i++)
    if (v[i] > level)
      id[i] = +1;
    else
      id[i] = -1;
  

  if (nsd == 1)
    {  /* nsd == 1 */
      double s01, s10;
      *volume = (coords[1]-coords[0])*(coords[1]-coords[0]);
      *volume = sqrt(*volume);
      
      if (id[0] < id[1])
	{
	  s01 = (level - v[0]) / (v[1] - v[0]);
	  fvol = s01;
	}
      else if (id[1] < id[0])
	{
	  s10 = (level - v[1]) / (v[0] - v[1]);
	  fvol = s10;
	}

      *sizeint = 1;

    }
  else if (nsd == 2)
    { /* nsd == 2 */
      double ax, ay, bx, by;
      double s01, s12, s20, s10, s21, s02;

      ax = (coords[0*nnpe+1]-coords[0*nnpe+0]);
      ay = (coords[1*nnpe+1]-coords[1*nnpe+0]);
      bx = (coords[0*nnpe+2]-coords[0*nnpe+0]);
      by = (coords[1*nnpe+2]-coords[1*nnpe+0]);

      *volume = 0.5 * (ax*by - bx*ay);
	
      i = 0;
      if (id[0] < id[1])
	{
	  s01 = (level - v[0]) / (v[1] - v[0]);
	  ax = coords[0*nnpe+1] * s01 + coords[0*nnpe+0] * (1.0 - s01);
	  ay = coords[1*nnpe+1] * s01 + coords[1*nnpe+0] * (1.0 - s01);
	  i+=1;
	}
      else if (id[1] < id[0])
	{
	  s10 = (level - v[1]) / (v[0] - v[1]);
	  ax = coords[0*nnpe+0] * s10 + coords[0*nnpe+1] * (1.0 - s10);
	  ay = coords[1*nnpe+0] * s10 + coords[1*nnpe+1] * (1.0 - s10);
	  i+=1;
	}
      if (id[1] < id[2])
	{
	  s12 = (level - v[1]) / (v[2] - v[1]);
	  bx = coords[0*nnpe+2] * s12 + coords[0*nnpe+1] * (1.0 - s12);
	  by = coords[1*nnpe+2] * s12 + coords[1*nnpe+1] * (1.0 - s12);
	  if (i == 0)
	    { ax = bx; ay = by; }
	  i+=2;
	}
      else if (id[2] < id[1])
	{
	  s21 = (level - v[2]) / (v[1] - v[2]);
	  bx = coords[0*nnpe+1] * s21 + coords[0*nnpe+2] * (1.0 - s21);
	  by = coords[1*nnpe+1] * s21 + coords[1*nnpe+2] * (1.0 - s21);
	  if (i == 0)
	    { ax = bx; ay = by; }
	  i+=2;
	}
      if (id[2] < id[0])
	{
	  s20 = (level - v[2]) / (v[0] - v[2]);
	  bx = coords[0*nnpe+0] * s20 + coords[0*nnpe+2] * (1.0 - s20);
	  by = coords[1*nnpe+0] * s20 + coords[1*nnpe+2] * (1.0 - s20);
	  i+=4;
	}
      else if (id[0] < id[2])
	{
	  s02 = (level - v[0]) / (v[2] - v[0]);
	  bx = coords[0*nnpe+2] * s02 + coords[0*nnpe+0] * (1.0 - s02);
	  by = coords[1*nnpe+2] * s02 + coords[1*nnpe+0] * (1.0 - s02);
	  i+=4;
	}

      if (i == 1+2)   /* node 1 / node 0, node 2 */
	{
	  if (id[1] < 0)
	    fvol = s10 * s12;
	  else
	    fvol = 1.0 - (1.0 - s01) * (1.0 - s21);
	}
      if (i == 2+4)   /* node 2 / node 1, node 0 */  
	{
	  if (id[2] < 0)
	    fvol = s21 * s20;
	  else
	    fvol = 1.0 - (1.0 - s12) * (1.0 - s02);
	}
      if (i == 4+1)   /* node 0 / node 2, node 1 */  
	{
	  if (id[0] < 0)
	    fvol = s02 * s01;
	  else
	    fvol = 1.0 - (1.0 - s20) * (1.0 - s10);
	}
      
      fvol = 1.0 - fvol;
      pvol = *volume * fvol;
      *sizeint = sqrt((bx-ax)*(bx-ax) + (by-ay)*(by-ay));

    }
  else if (nsd == 3)
    { /* nsd == 3 */
      int j;
      double ax,ay,az, bx,by,bz, cx,cy,cz, dx,dy,dz;
      double x1,x2,x3, y1,y2,y3, z1,z2,z3;
      double s01,s02,s03, s10,s12,s13, s20,s21,s23, s30,s31,s32; 

      ax = (coords[0*nnpe+1] - coords[0*nnpe+0]);
      bx = (coords[0*nnpe+2] - coords[0*nnpe+0]);
      cx = (coords[0*nnpe+3] - coords[0*nnpe+0]);
      ay = (coords[1*nnpe+1] - coords[1*nnpe+0]);
      by = (coords[1*nnpe+2] - coords[1*nnpe+0]);
      cy = (coords[1*nnpe+3] - coords[1*nnpe+0]);
      az = (coords[2*nnpe+1] - coords[2*nnpe+0]);
      bz = (coords[2*nnpe+2] - coords[2*nnpe+0]);
      cz = (coords[2*nnpe+3] - coords[2*nnpe+0]);

      *volume = ax*(by*cz-bz*cy)+ay*(bz*cx-bx*cz)+az*(bx*cy-by*cx);
      *volume = *volume / 6.0;

      i = 0;
      j = 0;
      if (id[0] < id[1])
	{
	  s01 = (level - v[0]) / (v[1] - v[0]);
	  ax = coords[0*nnpe+1] * s01 + coords[0*nnpe+0] * (1.0 - s01);
	  ay = coords[1*nnpe+1] * s01 + coords[1*nnpe+0] * (1.0 - s01);
	  az = coords[2*nnpe+1] * s01 + coords[2*nnpe+0] * (1.0 - s01);
	  i+=1;
	  j++;
	}
      else if (id[1] < id[0])
	{
	  s10 = (level - v[1]) / (v[0] - v[1]);
	  ax = coords[0*nnpe+0] * s10 + coords[0*nnpe+1] * (1.0 - s10);
	  ay = coords[1*nnpe+0] * s10 + coords[1*nnpe+1] * (1.0 - s10);
	  az = coords[2*nnpe+0] * s10 + coords[2*nnpe+1] * (1.0 - s10);
	  i+=1;
	  j++;
	}
      if (id[1] < id[2])
	{
	  s12 = (level - v[1]) / (v[2] - v[1]);
	  bx = coords[0*nnpe+2] * s12 + coords[0*nnpe+1] * (1.0 - s12);
	  by = coords[1*nnpe+2] * s12 + coords[1*nnpe+1] * (1.0 - s12);
	  bz = coords[2*nnpe+2] * s12 + coords[2*nnpe+1] * (1.0 - s12);
	  if (j == 0)
	    { ax = bx; ay = by; az = bz; }
	  i+=2;
	  j++;
	}
      else if (id[2] < id[1])
	{
	  s21 = (level - v[2]) / (v[1] - v[2]);
	  bx = coords[0*nnpe+1] * s21 + coords[0*nnpe+2] * (1.0 - s21);
	  by = coords[1*nnpe+1] * s21 + coords[1*nnpe+2] * (1.0 - s21);
	  bz = coords[2*nnpe+1] * s21 + coords[2*nnpe+2] * (1.0 - s21);
	  if (j == 0)
	    { ax = bx; ay = by; az = bz; }
	  i+=2;
	  j++;
	}
      if (id[2] < id[0])
	{
	  s20 = (level - v[2]) / (v[0] - v[2]);
	  bx = coords[0*nnpe+0] * s20 + coords[0*nnpe+2] * (1.0 - s20);
	  by = coords[1*nnpe+0] * s20 + coords[1*nnpe+2] * (1.0 - s20);
	  bz = coords[2*nnpe+0] * s20 + coords[2*nnpe+2] * (1.0 - s20);
	  i+=4;
	  j++;
	}
      else if (id[0] < id[2])
	{
	  s02 = (level - v[0]) / (v[2] - v[0]);
	  bx = coords[0*nnpe+2] * s02 + coords[0*nnpe+0] * (1.0 - s02);
	  by = coords[1*nnpe+2] * s02 + coords[1*nnpe+0] * (1.0 - s02);
	  bz = coords[2*nnpe+2] * s02 + coords[2*nnpe+0] * (1.0 - s02);
	  i+=4;
	  j++;
	}
      if (id[3] < id[0])
	{
	  s30 = (level - v[3]) / (v[0] - v[3]);
	  cx = coords[0*nnpe+0] * s30 + coords[0*nnpe+3] * (1.0 - s30);
	  cy = coords[1*nnpe+0] * s30 + coords[1*nnpe+3] * (1.0 - s30);
	  cz = coords[2*nnpe+0] * s30 + coords[2*nnpe+3] * (1.0 - s30);
	  if (j == 0)
	    { ax = cx; ay = cy; az = cz; }
	  i+=8;
	  j++;
	}
      else if (id[0] < id[3])
	{
	  s03 = (level - v[0]) / (v[3] - v[0]);
	  cx = coords[0*nnpe+3] * s03 + coords[0*nnpe+0] * (1.0 - s03);
	  cy = coords[1*nnpe+3] * s03 + coords[1*nnpe+0] * (1.0 - s03);
	  cz = coords[2*nnpe+3] * s03 + coords[2*nnpe+0] * (1.0 - s03);
	  if (j == 0)
	    { ax = cx; ay = cy; az = cz; }
	  i+=8;
	  j++;
	}
      if (id[3] < id[1])
	{
	  s31 = (level - v[3]) / (v[1] - v[3]);
	  dx = coords[0*nnpe+1] * s31 + coords[0*nnpe+3] * (1.0 - s31);
	  dy = coords[1*nnpe+1] * s31 + coords[1*nnpe+3] * (1.0 - s31);
	  dz = coords[2*nnpe+1] * s31 + coords[2*nnpe+3] * (1.0 - s31);
	  if (j == 1)
	    { bx = dx; by = dy; bz = dz; }
	  else if (j == 2)
	    { cx = dx; cy = dy; cz = dz; }
	  i+=16;
	  j++;
	}
      else if (id[1] < id[3])
	{
	  s13 = (level - v[1]) / (v[3] - v[1]);
	  dx = coords[0*nnpe+3] * s13 + coords[0*nnpe+1] * (1.0 - s13);
	  dy = coords[1*nnpe+3] * s13 + coords[1*nnpe+1] * (1.0 - s13);
	  dz = coords[2*nnpe+3] * s13 + coords[2*nnpe+1] * (1.0 - s13);
	  if (j == 1)
	    { bx = dx; by = dy; bz = dz; }
	  else if (j == 2)
	    { cx = dx; cy = dy; cz = dz; }
	  i+=16;
	  j++;
	}
      if (id[3] < id[2])
	{
	  s32 = (level - v[3]) / (v[2] - v[3]);
	  dx = coords[0*nnpe+2] * s32 + coords[0*nnpe+3] * (1.0 - s32);
	  dy = coords[1*nnpe+2] * s32 + coords[1*nnpe+3] * (1.0 - s32);
	  dz = coords[2*nnpe+2] * s32 + coords[2*nnpe+3] * (1.0 - s32);
	  if (j == 2)
	    { cx = dx; cy = dy; cz = dz; }
	  i+=32;
	  j++;
	}
      else if (id[2] < id[3])
	{
	  s23 = (level - v[2]) / (v[3] - v[2]);
	  dx = coords[0*nnpe+3] * s23 + coords[0*nnpe+2] * (1.0 - s23);
	  dy = coords[1*nnpe+3] * s23 + coords[1*nnpe+2] * (1.0 - s23);
	  dz = coords[2*nnpe+3] * s23 + coords[2*nnpe+2] * (1.0 - s23);
	  if (j == 2)
	    { cx = dx; cy = dy; cz = dz; }
	  i+=32;
	  j++;
	}

      if (j == 3)   /* triangle */
	{
	  if (i == 1+2+16)  /* node 1 / node 0, node 2, node 3 */
	    {
	      if (id[1] < 0)
		{
		  fvol = s10 * s12 * s13;
		  x1 = bx - ax; y1 = by - ay; z1 = bz - az;
		  x2 = cx - ax; y2 = cy - ay; z2 = cz - az;
		}
	      else
		{
		  fvol = 1.0 - (1.0 - s01)*(1.0 - s21)*(1.0 - s31);
		  x1 = cx - ax; y1 = cy - ay; z1 = cz - az;
		  x2 = bx - ax; y2 = by - ay; z2 = bz - az;
		}
	    }
	  if (i == 2+4+32) /* node 2 / node 1, node 0, node 3 */
	    {
	      if (id[2] < 0)
		{	
		  fvol = s21 * s20 * s23;
		  x1 = bx - ax; y1 = by - ay; z1 = bz - az;
		  x2 = cx - ax; y2 = cy - ay; z2 = cz - az;
		}
	      else
		{
		  fvol = 1.0 - (1.0 - s12)*(1.0 - s02)*(1.0 - s32);
		  x1 = cx - ax; y1 = cy - ay; z1 = cz - az;
		  x2 = bx - ax; y2 = by - ay; z2 = bz - az;
		}
	    }
	  if (i == 4+1+8) /* node 0 / node 2, node 1, node 3 */
	    {
	      if (id[0] < 0)
		{
		  fvol = s02 * s01 * s03;
		  x1 = ax - bx; y1 = ay - by; z1 = az - bz;
		  x2 = cx - bx; y2 = cy - by; z2 = cz - bz;
		}
	      else
		{
		  fvol = 1.0 - (1.0 - s20)*(1.0 - s10)*(1.0 - s30);
		  x1 = bx - ax; y1 = by - ay; z1 = bz - az;
		  x2 = cx - ax; y2 = cy - ay; z2 = cz - az;
		}
	    }
      	  if (i == 8+16+32) /* node 3 / node 0, node 1, node 2 */
	    {
	      if (id[3] < 0)
		{
		  fvol = s30 * s31 * s32;
		  x1 = bx - ax; y1 = by - ay; z1 = bz - az;
		  x2 = cx - ax; y2 = cy - ay; z2 = cz - az;
		}
	      else
		{
		  fvol = 1.0 - (1.0 - s03)*(1.0 - s13)*(1.0 - s23);
		  x1 = cx - ax; y1 = cy - ay; z1 = cz - az;
		  x2 = bx - ax; y2 = by - ay; z2 = bz - az;
		}
	    }
	
	  *sizeint = 0.5 * sqrt ((y1*z2-z1*y2)*(y1*z2-z1*y2) +
				 (z1*x2-x1*z2)*(z1*x2-x1*z2) +
				 (x1*y2-y1*x2)*(x1*y2-y1*x2));
	}
      else   /* quadrilateral */ 
	{
	  if (i == 1+2+32+8)  /* node 1, node 3 / node 0, node 2 */
	    {
	      if (id[1] < 0)
		{		
		  fvol = s10*s12 + s30*s32 + s10*s32 - 
		          s10*s12*s32 - s10*s30*s32;
		  x1 = bx - ax; y1 = by - ay; z1 = bz - az;
		  x2 = dx - ax; y2 = dy - ay; z2 = dz - az;
		  x3 = cx - ax; y3 = cy - ay; z3 = cz - az;
		}
	      else
		{
		  fvol = 1.0 - ((1.0 - s01)*(1.0 - s21)+
				(1.0 - s03)*(1.0 - s23)+
				(1.0 - s01)*(1.0 - s23)-
				(1.0 - s01)*(1.0 - s21)*(1.0 - s23)-
				(1.0 - s01)*(1.0 - s03)*(1.0 - s23));
		  x1 = cx - ax; y1 = cy - ay; z1 = cz - az;
		  x2 = dx - ax; y2 = dy - ay; z2 = dz - az;
		  x3 = bx - ax; y3 = by - ay; z3 = bz - az;
		}
	    }
	  if (i == 2+4+8+16)  /* node 2, node 3 / node 0, node 1 */
	    {
	      if (id[2] < 0)
		{
		  fvol = s21*s20 + s31*s30 + s21*s30 -
		          s21*s20*s30 - s21*s31*s30;
		  x1 = bx - ax; y1 = by - ay; z1 = bz - az;
		  x2 = cx - ax; y2 = cy - ay; z2 = cz - az;
		  x3 = dx - ax; y3 = dy - ay; z3 = dz - az;
		}
	      else
		{
		  fvol = 1.0 - ((1.0 - s12)*(1.0 - s02)+
				   (1.0 - s13)*(1.0 - s03)+
				   (1.0 - s12)*(1.0 - s03)-
				   (1.0 - s12)*(1.0 - s02)*(1.0 - s03)-
				   (1.0 - s12)*(1.0 - s13)*(1.0 - s03));
		  x1 = dx - ax; y1 = dy - ay; z1 = dz - az;
		  x2 = cx - ax; y2 = cy - ay; z2 = cz - az;
		  x3 = bx - ax; y3 = by - ay; z3 = bz - az;
		}
	    }
	  if (i == 1+4+32+16)  /* node 1, node 2 / node 0, node 3 */
	    {
	      if (id[1] < 0)
		{
		  fvol = s13*s10 + s23*s20 + s13*s20 -
		          s13*s10*s20 - s13*s23*s20;
		  x1 = bx - ax; y1 = by - ay; z1 = bz - az;
		  x2 = dx - ax; y2 = dy - ay; z2 = dz - az;
		  x3 = cx - ax; y3 = cy - ay; z3 = cz - az;
		}
	      else
		{
		  fvol = 1.0 - ((1.0 - s31)*(1.0 - s01)+
				(1.0 - s32)*(1.0 - s02)+
				(1.0 - s31)*(1.0 - s02)-
				(1.0 - s31)*(1.0 - s01)*(1.0 - s02)-
				(1.0 - s31)*(1.0 - s32)*(1.0 - s02));
		  x1 = cx - ax; y1 = cy - ay; z1 = cz - az;
		  x2 = dx - ax; y2 = dy - ay; z2 = dz - az;
		  x3 = bx - ax; y3 = by - ay; z3 = bz - az;
		}
	    }
	  
	  *sizeint = 0.5 * sqrt ((y1*z2-z1*y2)*(y1*z2-z1*y2) +
				 (z1*x2-x1*z2)*(z1*x2-x1*z2) +
				 (x1*y2-y1*x2)*(x1*y2-y1*x2));

	  *sizeint += 0.5 * sqrt ((y2*z3-z2*y3)*(y2*z3-z2*y3) +
				  (z2*x3-x2*z3)*(z2*x3-x2*z3) +
				  (x2*y3-y2*x3)*(x2*y3-y2*x3));
	}

      fvol = 1.0 - fvol;
      pvol = *volume * fvol;
      
    }

  return pvol;
}

/* Edge distance: given distances on nodes, try to improve
 *    moving over edges
 * coords[i*nnpe + j]: Coordinate "i" of the "j"th node
 * nsd: number of space dimensions
 * d[i]: distance computed at node "i"
 * returns true if any value gets improved
 */
int edge_distance2 (double *coords, int nsd, double *d)
{
  int i, j, k;
  int nnpe, retval;
  double eij;

  /* Simplex: number of nodes per element: number of space dimensions + 1 */
  nnpe = nsd + 1;

  retval = 0;
  for (i = 0; i < nnpe; i++)
    for (j = i+1; j < nnpe; j++)
      {
	/* Edge distance nodes (i,j) */
	eij = 0;
	for (k = 0; k < nsd; k++)
	  eij += ((coords[i+k*nnpe]-coords[j+k*nnpe])*
		  (coords[i+k*nnpe]-coords[j+k*nnpe]));
	eij = sqrt (eij);

	if (d[i] > d[j] + eij)
	  {
	    d[i] = d[j] + eij;
	    retval = 1;
	  }
	if (d[j] > d[i] + eij)
	  {
	    d[j] = d[i] + eij;
	    retval = 1;
	  }
      }
  return retval;
}

/* Shadow distance: given distances on nodes, try to improve
 *    moving over the simplex
 * coords[i*nnpe + j]: Coordinate "i" of the "j"th node
 * nsd: number of space dimensions
 * d[i]: distance computed at node "i"
 * returns true if any value gets improved
 */
int shadow_distance2 (double *coords, int nsd, double *d)
{
  int i, imin;
  int nnpe, retval;
  double dmin;
    

  /* Simplex: number of nodes per element: number of space dimensions + 1 */
  nnpe = nsd + 1;

  if (nsd == 1) /* 1D case: shadow == edge */
    return edge_distance2 (coords, nsd, d);

  /* Computes node with the minimum distance */
  imin = 0;
  dmin = d[0];
  for (i = 1; i < nnpe; i++)
    if (d[i] < dmin)
      {
	imin = i;
	dmin = d[i];
      }

  /* Try to improve node "i" (all except node with minimum distance) */
  retval = 0;
  for (i = 0; i < nnpe; i++)
    {
      /* 2D case: */
      if (nsd == 2)
	{
	  int n1, n2;
	  double dx, dy, ex, ey;
	  double edgelength, sp, newval;
	  double tgrad, ngrad, height;
	  double ct2, ct3, cn3, f2, f3;

	  /* n1, n2: opposite side */
	  n1 = (i+1)%3;
	  n2 = (i+2)%3;

	  dx = (coords[0*nnpe+n2]-coords[0*nnpe+n1]);
	  dy = (coords[1*nnpe+n2]-coords[1*nnpe+n1]);
	  edgelength = sqrt(dx*dx + dy*dy);

	  /* Tangential gradient */
	  tgrad = (d[n2]-d[n1]) / edgelength;

	  if (tgrad*tgrad > 1.0)
	    {        /* edgedistance improves d[n1] or d[n2] */
	      if (d[n1] < d[n2])
		d[n2] = d[n1] + edgelength;
	      else
		d[n1] = d[n2] + edgelength;
	      retval = 1;
	      continue;
	    }

	  if (i == imin)
	    continue;

	  /* Normal gradient */
	  ngrad = sqrt (1.0 - tgrad*tgrad);

	  ex = (coords[0*nnpe+i]-coords[0*nnpe+n1]);
	  ey = (coords[1*nnpe+i]-coords[1*nnpe+n1]);

	  /* Projection of node 'i' over side 'n1,n2'
	   * sp: parameter, 0->n1, 1->n2 */
	  sp = (dx*ex + dy*ey) / (edgelength * edgelength);

	  /* Distance from node 'i' to edge 'n1,n2' */	  
	  height = (dx*ey - dy*ex) / edgelength;

	  /* Coordinates normal and tangential:
	   * origin in n1: ct1=cn1=0
	   * tangential axis through n2: cn2=0 */
	  ct2 = edgelength;
	  ct3 = sp*ct2;
	  cn3 = height;

	  /* Function with isolines parallel to gradient
	   * (used to find if node 'i' is in the shadow of side 'n1,n2')
	   */
	  f2 = ct2 * ngrad;
	  f3 = ct3 * ngrad - cn3 * tgrad;

	  if (f3/f2 > 1.0 || f3/f2 < 0.0)
	    continue;

	  /* YES, it's in the shadow, set newval so that
	   * module of the gradient is 1.0
	   */
	  newval = ngrad * cn3 + (d[n1]*(1-sp)+d[n2]*sp);

	  /* See if the new value improves the previous one */
	  if (newval < d[i])
	    {
	      d[i] = newval;
	      retval = 1;
	    }
	}
      else if (nsd == 3)  /* Three dimensional case */
	{
	  int n1, n2, n3;
	  double ax,ay,az, bx,by,bz, cx,cy,cz, hx,hy,hz, dx,dy,dz;
	  double pix,piy,piz, px,py, a1x,a1y, a2x,a2y;
	  double a3x,a3y, b1x,b1y, b2x,b2y, b3x,b3y; 
	  double ct1,ct2,ct3, gt, gn, sp, s, dp;
	  double tgrad, ngrad, height, edge12, newval;
	  
	  /* Opposite face of the tetrahedron, numbered in
	   * counter-clockwise direction seen from 'i' node
	   */
	  if (i%2)
	    {
	      n1 = (i+1)%4;
	      n2 = (i+2)%4;
	      n3 = (i+3)%4;
	    }
	  else
	    {
	      n1 = (i+1)%4;
	      n2 = (i+3)%4;
	      n3 = (i+2)%4;
	    }

	  ax = (coords[0*nnpe+n2] - coords[0*nnpe+n1]);
	  ay = (coords[1*nnpe+n2] - coords[1*nnpe+n1]);
	  az = (coords[2*nnpe+n2] - coords[2*nnpe+n1]);
	  edge12 = sqrt(ax*ax + ay*ay + az*az);

	  tgrad = (d[n2] - d[n1]) / edge12;
	  
	  bx = (coords[0*nnpe+n3] - coords[0*nnpe+n1]);
	  by = (coords[1*nnpe+n3] - coords[1*nnpe+n1]);
	  bz = (coords[2*nnpe+n3] - coords[2*nnpe+n1]);

	  sp = (ax*bx + ay*by + az*bz) / (edge12 * edge12);

	  hx = (ay*bz - az*by);
	  hy = (az*bx - ax*bz);
	  hz = (ax*by - ay*bx);

	  height = sqrt (hx*hx + hy*hy + hz*hz) / edge12;

	  ngrad = (d[n3] - (d[n1]*(1-sp)+d[n2]*sp)) / height;
	  
	  gt = sqrt (tgrad*tgrad + ngrad*ngrad);

	  if (gt > 1.0)
	    { 
	      /* It's possible to improve the distance in the face:
	       * project over the face and call shadow_distance
	       * for the 2-D case */
	      double auxcoords[6], auxd[3];

	      auxcoords[0*(nnpe-1)+0] = 0;
	      auxcoords[1*(nnpe-1)+0] = 0;

	      auxcoords[0*(nnpe-1)+1] = edge12;
	      auxcoords[1*(nnpe-1)+1] = 0;

	      auxcoords[0*(nnpe-1)+2] = sp*edge12;
	      auxcoords[1*(nnpe-1)+2] = height;

	      auxd[0] = d[n1];
	      auxd[1] = d[n2];
	      auxd[2] = d[n3];
	      
	      if (shadow_distance2 (auxcoords, nsd-1, auxd))
		{
		  d[n1] = auxd[0];
		  d[n2] = auxd[1];
		  d[n3] = auxd[2];
		  retval = 1;
		}
	      continue;
	    }
	  
	  if (i == imin)
	    continue;
	  
	  /* Normal gradient so that mod(grad) = 1.0 */
	  gn = sqrt (1 - gt*gt);
	  
	  cx = (coords[0*nnpe+i] - coords[0*nnpe+n1]);
	  cy = (coords[1*nnpe+i] - coords[1*nnpe+n1]);
	  cz = (coords[2*nnpe+i] - coords[2*nnpe+n1]);

	  dx = (coords[0*nnpe+n3] -
		(coords[0*nnpe+n1]*(1-sp) + coords[0*nnpe+n2]*sp));
	  dy = (coords[1*nnpe+n3] -
		(coords[1*nnpe+n1]*(1-sp) + coords[1*nnpe+n2]*sp));
	  dz = (coords[2*nnpe+n3] -
		(coords[2*nnpe+n1]*(1-sp) + coords[2*nnpe+n2]*sp));

	  /* Find intersection between:
	   * line passing trhough node 'i', with direction = new gradient
	   * opposite face */

	  /* Proyection of the point i in a new coordinate system */
	  /* origin: vertice n1                                   */
	  /* x axis: side n1,n2 of the tetrahedron                */
	  /* y axis: perpendicular to side n1,n2                  */
	  /* z axis: perpendicular to the face n1,n2,n3           */
	  pix = (cx*ax + cy*ay + cz*az) / edge12;
	  piy = (cx*dx + cy*dy + cz*dz) / height;
	  piz = (cx*hx + cy*hy + cz*hz) / (height * edge12);

	  /* Intersection point */
	  s = piz / gn;
	  px = pix - s*tgrad;
	  py = piy - s*ngrad;

	  a1x = edge12;          a1y = 0;
	  a2x = (sp-1)*edge12;   a2y = height;
	  a3x = -sp*edge12;      a3y = -height;

	  b1x = px;              b1y = py;
	  b2x = px-edge12;       b2y = py;
	  b3x = px-sp*edge12;    b3y = py-height;

	  /* Triangular coordinates of point px,py over face n1,n2,n3 */
	  ct3 = (a1x*b1y - a1y*b1x) / (edge12 * height);
	  ct1 = (a2x*b2y - a2y*b2x) / (edge12 * height);
	  ct2 = (a3x*b3y - a3y*b3x) / (edge12 * height);

	  /* Is node 'i' in the shadow of face (n1,n2,n3) ???? */
	  if ((ct1 < 0.0)||(ct2 < 0.0)||(ct3 < 0.0))
	    continue;

	  /* Interpolated distance on intersection point */
	  dp = ct1*d[n1] + ct2*d[n2] + ct3*d[n3];
	  
	  newval = dp + s;

	  /* See if the new value improves the previous one */
	  if (newval < d[i])
	    {
	      d[i] = newval;
	      retval = 1;
	    }

	}
    }

  return retval;
}

#include <math.h>

#define ERROR  -1

/* fracvol_: compute partial volume in the interface element          */
/* coords[i*nnpe + j]: coordinate 'i' of the 'j'th node               */
/* nsd: number of space dimensions                                    */
/* v: characteristic function in each node                            */
/* level: level set value                                             */
/* volume: total volume of the element (return value)                 */
/* nintp: number of interpolate points of level set                   */
/* intpoints[i*nnpe + j]: coordinate 'i' of the 'j'th interface point */
/* sizeint: size of the interface                                     */
double fracvol_(double *coords, int *nsd, double *v, double *level, 
		double *volume, int *nintp, double *intpoints, 
		double *sizeint)
{
  int i, j;
  int nnpe;
  int max, min, id[4];
  double fvol;
  double s01,s02,s03, s12,s13, s23;
  double s10,s20,s30, s21,s31, s32;
  double ax,ay,az, bx,by,bz, cx,cy,cz, dx,dy,dz;

  /* Initialization */
  i = j = 0;
  fvol = 0.0;
  s01=s02=s03 = s12=s13 = s23 = 0.0;
  s10=s20=s30 = s21=s31 = s32 = 0.0;
  ax=ay=az = bx=by=bz = cx=cy=cz = dx=dy=dz = 0.0;

  /* Check for number of space dimension */
  if ((*nsd < 1)||(*nsd >3))
    return ERROR;

  nnpe = *nsd + 1;  /* spline */
  
  /* Identifying nodes */
  max = min = 0;
  for (i = 0; i < nnpe; i++)
    if (v[i] > *level)
      {
	max++;
	id[i] = +1;
      }
    else
      {
	min++;
	id[i] = -1;
      }

  if (*nsd == 1)
    {  /* *nsd == 1 */
      ax=coords[1]-coords[0];
      *volume = ax*ax;
      *volume = sqrt(*volume);
      
      if (max == nnpe)
	{
	  *nintp = 0;
	  *sizeint = 0.0;
	  return 1.0;
	}
      else if (min == nnpe)
	{
	  *nintp = 0;
	  *sizeint = 0.0;
	  return 0.0;
	}

      if (id[1] > id[0])
	{
	  s10 = (v[1] - *level) / (v[1] - v[0]);
	  ax = coords[0] * s10 + coords[1] * (1.0 - s10);
	}
      else
	{
	  s01 = (v[0] - *level) / (v[0] - v[1]);
	  ax = coords[1] * s01 + coords[0] * (1.0 - s01);
	}
      
      if (id[1] > *level)
	fvol = s10;
      else
	fvol = s01;

      *nintp = 1;
      intpoints[0] = ax;
      *sizeint = 1;

    }
  else if (*nsd == 2)
    { /* *nsd == 2 */
      ax=coords[1]-coords[0]; ay=coords[4]-coords[3];
      bx=coords[2]-coords[0]; by=coords[5]-coords[3];
      *volume = 0.5 * (ax*by - bx*ay);
	
      if (max == nnpe)
	{
	  *nintp = 0;
	  *sizeint = 0.0;
	  return 1.0;
	}
      else if (min == nnpe)
	{
	  *nintp = 0;
	  *sizeint = 0.0;
	  return 0.0;
	}

      *nintp = 2;

      i = 0;
      if (id[1] > id[0])
	{
	  s10 = (v[1] - *level) / (v[1] - v[0]);
	  ax = coords[0*nnpe+0] * s10 + coords[0*nnpe+1] * (1.0 - s10);
	  ay = coords[1*nnpe+0] * s10 + coords[1*nnpe+1] * (1.0 - s10);
	  i+=1;
	}
      else if (id[0] > id[1])
	{
	  s01 = (v[0] - *level) / (v[0] - v[1]);
	  ax = coords[0*nnpe+1] * s01 + coords[0*nnpe+0] * (1.0 - s01);
	  ay = coords[1*nnpe+1] * s01 + coords[1*nnpe+0] * (1.0 - s01);
	  i+=1;
	}
      if (id[2] > id[1])
	{
	  s21 = (v[2] - *level) / (v[2] - v[1]);
	  bx = coords[0*nnpe+1] * s21 + coords[0*nnpe+2] * (1.0 - s21);
	  by = coords[1*nnpe+1] * s21 + coords[1*nnpe+2] * (1.0 - s21);
	  if (i == 0)
	    { ax = bx; ay = by; }
	  i+=2;
	}
      else if (id[1] > id[2])
	{
	  s12 = (v[1] - *level) / (v[1] - v[2]);
	  bx = coords[0*nnpe+2] * s12 + coords[0*nnpe+1] * (1.0 - s12);
	  by = coords[1*nnpe+2] * s12 + coords[1*nnpe+1] * (1.0 - s12);
	  if (i == 0)
	    { ax = bx; ay = by; }
	  i+=2;
	}
      if (id[0] > id[2])
	{
	  s02 = (v[0] - *level) / (v[0] - v[2]);
	  bx = coords[0*nnpe+2] * s02 + coords[0*nnpe+0] * (1.0 - s02);
	  by = coords[1*nnpe+2] * s02 + coords[1*nnpe+0] * (1.0 - s02);
	  i+=4;
	}
      else if (id[2] > id[0])
	{
	  s20 = (v[2] - *level) / (v[2] - v[0]);
	  bx = coords[0*nnpe+0] * s20 + coords[0*nnpe+2] * (1.0 - s20);
	  by = coords[1*nnpe+0] * s20 + coords[1*nnpe+2] * (1.0 - s20);
	  i+=4;
	}

      if (i == 1+2)   /* node 1 / node 0, node 2 */
	{
	  if (id[1] > 0)
	    {
	      fvol = s10 * s12;
	      intpoints[0*(*nintp)+0] = ax;
	      intpoints[1*(*nintp)+0] = ay;
	      intpoints[0*(*nintp)+1] = bx;
	      intpoints[1*(*nintp)+1] = by;
	    }
	  else
	    {
	      fvol = 1.0 - (1.0 - s01) * (1.0 - s21);
	      intpoints[0*(*nintp)+0] = bx;
	      intpoints[1*(*nintp)+0] = by;
	      intpoints[0*(*nintp)+1] = ax;
	      intpoints[1*(*nintp)+1] = ay;
	    }
	}
      if (i == 2+4)   /* node 2 / node 1, node 0 */  
	{
	  if (id[2] > 0)
	    {
	      fvol = s21 * s20;
	      intpoints[0*(*nintp)+0] = ax;
	      intpoints[1*(*nintp)+0] = ay;
	      intpoints[0*(*nintp)+1] = bx;
	      intpoints[1*(*nintp)+1] = by;
	    }
	  else
	    {
	      fvol = 1.0 - (1.0 - s12) * (1.0 - s02);
	      intpoints[0*(*nintp)+0] = bx;
	      intpoints[1*(*nintp)+0] = by;
	      intpoints[0*(*nintp)+1] = ax;
	      intpoints[1*(*nintp)+1] = ay;
	    }
	}
      if (i == 4+1)   /* node 0 / node 2, node 1 */  
	{
	  if (id[0] > 0)
	    {
	      fvol = s02 * s01;
	      intpoints[0*(*nintp)+0] = bx;
	      intpoints[1*(*nintp)+0] = by;
	      intpoints[0*(*nintp)+1] = ax;
	      intpoints[1*(*nintp)+1] = ay;
	    }
	  else
	    {
	      fvol = 1.0 - (1.0 - s20) * (1.0 - s10);
	      intpoints[0*(*nintp)+0] = ax;
	      intpoints[1*(*nintp)+0] = ay;
	      intpoints[0*(*nintp)+1] = bx;
	      intpoints[1*(*nintp)+1] = by;
	    }
	}
     
      *sizeint = sqrt((bx-ax)*(bx-ax) + (by-ay)*(by-ay));

    }
  else 
    { /* *nsd == 3 */
      ax=coords[1]-coords[0]; ay=coords[5]-coords[4]; az=coords[9]-coords[8]; 
      bx=coords[2]-coords[0]; by=coords[6]-coords[4]; bz=coords[10]-coords[8];
      cx=coords[3]-coords[0]; cy=coords[7]-coords[4]; cz=coords[11]-coords[8];

      *volume = ax*(by*cz-bz*cy)+ay*(bz*cx-bx*cz)+az*(bx*cy-by*cx);
      *volume = *volume / 6.0;

      if (max == nnpe)
	{
	  *nintp = 0;
	  *sizeint = 0.0;
	  return 1.0;
	}
      else if (min == nnpe)
	{
	  *nintp = 0;
	  *sizeint = 0.0;
	  return 0.0;
	}

      i = 0;
      j = 0;
      if (id[1] > id[0])
	{
	  s10 = (v[1] - *level) / (v[1] - v[0]);
	  ax = coords[0*nnpe+0] * s10 + coords[0*nnpe+1] * (1.0 - s10);
	  ay = coords[1*nnpe+0] * s10 + coords[1*nnpe+1] * (1.0 - s10);
	  az = coords[2*nnpe+0] * s10 + coords[2*nnpe+1] * (1.0 - s10);
	  j++; i+=1;
	}
      else if (id[0] > id[1])
	{
	  s01 = (v[0] - *level) / (v[0] - v[1]);
	  ax = coords[0*nnpe+1] * s01 + coords[0*nnpe+0] * (1.0 - s01);
	  ay = coords[1*nnpe+1] * s01 + coords[1*nnpe+0] * (1.0 - s01);
	  az = coords[2*nnpe+1] * s01 + coords[2*nnpe+0] * (1.0 - s01);
	  j++; i+=1;
	}
      if (id[2] > id[1])
	{
	  s21 = (v[2] - *level) / (v[2] - v[1]);
	  bx = coords[0*nnpe+1] * s21 + coords[0*nnpe+2] * (1.0 - s21);
	  by = coords[1*nnpe+1] * s21 + coords[1*nnpe+2] * (1.0 - s21);
	  bz = coords[2*nnpe+1] * s21 + coords[2*nnpe+2] * (1.0 - s21);
	  if (i == 0)
	    { ax = bx; ay = by; az = bz; }
	  j++; i+=2;
	}
      else if (id[1] > id[2])
	{
	  s12 = (v[1] - *level) / (v[1] - v[2]);
	  bx = coords[0*nnpe+2] * s12 + coords[0*nnpe+1] * (1.0 - s12);
	  by = coords[1*nnpe+2] * s12 + coords[1*nnpe+1] * (1.0 - s12);
	  bz = coords[2*nnpe+2] * s12 + coords[2*nnpe+1] * (1.0 - s12);
	  if (i == 0)
	    { ax = bx; ay = by; az = bz; }
	  j++; i+=2;
	}
      if (id[0] > id[2])
	{
	  s02 = (v[0] - *level) / (v[0] - v[2]);
	  bx = coords[0*nnpe+2] * s02 + coords[0*nnpe+0] * (1.0 - s02);
	  by = coords[1*nnpe+2] * s02 + coords[1*nnpe+0] * (1.0 - s02);
	  bz = coords[2*nnpe+2] * s02 + coords[2*nnpe+0] * (1.0 - s02);
	  j++; i+=4;
	}
      else if (id[2] > id[0])
	{
	  s20 = (v[2] - *level) / (v[2] - v[0]);
	  bx = coords[0*nnpe+0] * s20 + coords[0*nnpe+2] * (1.0 - s20);
	  by = coords[1*nnpe+0] * s20 + coords[1*nnpe+2] * (1.0 - s20);
	  bz = coords[2*nnpe+0] * s20 + coords[2*nnpe+2] * (1.0 - s20);
	  j++; i+=4;
	}
      if (id[3] > id[0])
	{
	  s30 = (v[3] - *level) / (v[3] - v[0]);
	  cx = coords[0*nnpe+0] * s30 + coords[0*nnpe+3] * (1.0 - s30);
	  cy = coords[1*nnpe+0] * s30 + coords[1*nnpe+3] * (1.0 - s30);
	  cz = coords[2*nnpe+0] * s30 + coords[2*nnpe+3] * (1.0 - s30);
	  if (j == 0)
	    { ax = cx; ay = cy; az = cz; } 
	  j++; i+=8;
	}
      else if (id[0] > id[3])
	{
	  s03 = (v[0] - *level) / (v[0] - v[3]);
	  cx = coords[0*nnpe+3] * s03 + coords[0*nnpe+0] * (1.0 - s03);
	  cy = coords[1*nnpe+3] * s03 + coords[1*nnpe+0] * (1.0 - s03);
	  cz = coords[2*nnpe+3] * s03 + coords[2*nnpe+0] * (1.0 - s03);
	  if (j == 0)
	    { ax = cx; ay = cy; az = cz; } 
	  j++; i+=8;
	}
      if (id[3] > id[1])
	{
	  s31 = (v[3] - *level) / (v[3] - v[1]);
	  dx = coords[0*nnpe+1] * s31 + coords[0*nnpe+3] * (1.0 - s31);
	  dy = coords[1*nnpe+1] * s31 + coords[1*nnpe+3] * (1.0 - s31);
	  dz = coords[2*nnpe+1] * s31 + coords[2*nnpe+3] * (1.0 - s31);
	  if (j == 1)
	    { bx = dx; by = dy; bz = dz; } 
	  if (j == 2)
	    { cx = dx, cy = dy, cz = dz; }
	  j++; i+=16;
	}
      else if (id[1] > id[3])
	{
	  s13 = (v[1] - *level) / (v[1] - v[3]);
	  dx = coords[0*nnpe+3] * s13 + coords[0*nnpe+1] * (1.0 - s13);
	  dy = coords[1*nnpe+3] * s13 + coords[1*nnpe+1] * (1.0 - s13);
	  dz = coords[2*nnpe+3] * s13 + coords[2*nnpe+1] * (1.0 - s13);
	  if (j == 1)
	    { bx = dx; by = dy; bz = dz; } 
	  if (j == 2)
	    { cx = dx, cy = dy, cz = dz; }
	  j++; i+=16;
	}
      if (id[3] > id[2])
	{
	  s32 = (v[3] - *level) / (v[3] - v[2]);
	  dx = coords[0*nnpe+2] * s32 + coords[0*nnpe+3] * (1.0 - s32);
	  dy = coords[1*nnpe+2] * s32 + coords[1*nnpe+3] * (1.0 - s32);
	  dz = coords[2*nnpe+2] * s32 + coords[2*nnpe+3] * (1.0 - s32);
	  if (j == 2)
	    { cx = dx; cy = dy; cz = dz; } 
	  j++; i+=32;
	}
      else if (id[2] > id[3])
	{
	  s23 = (v[2] - *level) / (v[2] - v[3]);
	  dx = coords[0*nnpe+3] * s23 + coords[0*nnpe+2] * (1.0 - s23);
	  dy = coords[1*nnpe+3] * s23 + coords[1*nnpe+2] * (1.0 - s23);
	  dz = coords[2*nnpe+3] * s23 + coords[2*nnpe+2] * (1.0 - s23);
	  if (j == 2)
	    { cx = dx, cy = dy, cz = dz; }
	  j++; i+=32;
	}

      if (j == 3)   /* triangle */
	{
	  *nintp = 3;
	  if (i == 1+2+16)  /* node 1 / node 0, node 2, node 3 */
	    {
	      if (id[1] > 0)
		{
		  fvol = s10 * s12 * s13;
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = bx;
		  intpoints[1*(*nintp)+1] = by;
		  intpoints[2*(*nintp)+1] = bz;
		  intpoints[0*(*nintp)+2] = cx;
		  intpoints[1*(*nintp)+2] = cy;
		  intpoints[2*(*nintp)+2] = cz;
		}
	      else
		{
		  fvol = 1.0 - (1.0 - s01)*(1.0 - s21)*(1.0 - s31);
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = cx;
		  intpoints[1*(*nintp)+1] = cy;
		  intpoints[2*(*nintp)+1] = cz;
		  intpoints[0*(*nintp)+2] = bx;
		  intpoints[1*(*nintp)+2] = by;
		  intpoints[2*(*nintp)+2] = bz;
		}
	    }
	  if (i == 2+4+32) /* node 2 / node 1, node 0, node 3 */
	    {
	      if (id[2] > 0)
		{	
		  fvol = s21 * s20 * s23;
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = bx;
		  intpoints[1*(*nintp)+1] = by;
		  intpoints[2*(*nintp)+1] = bz;
		  intpoints[0*(*nintp)+2] = cx;
		  intpoints[1*(*nintp)+2] = cy;
		  intpoints[2*(*nintp)+2] = cz;

		}
	      else
		{
		  fvol = 1.0 - (1.0 - s12)*(1.0 - s02)*(1.0 - s32);
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = cx;
		  intpoints[1*(*nintp)+1] = cy;
		  intpoints[2*(*nintp)+1] = cz;
		  intpoints[0*(*nintp)+2] = bx;
		  intpoints[1*(*nintp)+2] = by;
		  intpoints[2*(*nintp)+2] = bz;
		}
	    }
	  if (i == 4+1+8) /* node 0 / node 2, node 1, node 3 */
	    {
	      if (id[0] > 0)
		{
		  fvol = s02 * s01 * s03;
		  intpoints[0*(*nintp)+0] = bx;
		  intpoints[1*(*nintp)+0] = by;
		  intpoints[2*(*nintp)+0] = bz;
		  intpoints[0*(*nintp)+1] = ax;
		  intpoints[1*(*nintp)+1] = ay;
		  intpoints[2*(*nintp)+1] = az;
		  intpoints[0*(*nintp)+2] = cx;
		  intpoints[1*(*nintp)+2] = cy;
		  intpoints[2*(*nintp)+2] = cz;
		}
	      else
		{
		  fvol = 1.0 - (1.0 - s20)*(1.0 - s10)*(1.0 - s30);
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = bx;
		  intpoints[1*(*nintp)+1] = by;
		  intpoints[2*(*nintp)+1] = bz;
		  intpoints[0*(*nintp)+2] = cx;
		  intpoints[1*(*nintp)+2] = cy;
		  intpoints[2*(*nintp)+2] = cz;
		}
	    }
      	  if (i == 8+16+32) /* node 3 / node 0, node 1, node 2 */
	    {
	      if (id[3] > 0)
		{
		  fvol = s30 * s31 * s32;
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = bx;
		  intpoints[1*(*nintp)+1] = by;
		  intpoints[2*(*nintp)+1] = bz;
		  intpoints[0*(*nintp)+2] = cx;
		  intpoints[1*(*nintp)+2] = cy;
		  intpoints[2*(*nintp)+2] = cz;
		}
	      else
		{
		  fvol = 1.0 - (1.0 - s03)*(1.0 - s13)*(1.0 - s23);
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = cx;
		  intpoints[1*(*nintp)+1] = cy;
		  intpoints[2*(*nintp)+1] = cz;
		  intpoints[0*(*nintp)+2] = bx;
		  intpoints[1*(*nintp)+2] = by;
		  intpoints[2*(*nintp)+2] = bz;
		}
	    }
	
	  ax = intpoints[0*(*nintp)+1] - intpoints[0*(*nintp)+0];
	  ay = intpoints[1*(*nintp)+1] - intpoints[1*(*nintp)+0];
	  az = intpoints[2*(*nintp)+1] - intpoints[2*(*nintp)+0];
	  bx = intpoints[0*(*nintp)+2] - intpoints[0*(*nintp)+0];
	  by = intpoints[1*(*nintp)+2] - intpoints[1*(*nintp)+0];
	  bz = intpoints[2*(*nintp)+2] - intpoints[2*(*nintp)+0];
	  
	  *sizeint = 0.5 * sqrt((ay*bz-az*by)*(ay*bz-az*by) + 
				(az*bx-ax*bz)*(az*bx-ax*bz) +
				(ax*by-ay*bx)*(ax*by-ay*bx));
	}
      else   /* quadrilateral */ 
	{
	  *nintp = 4;
	  if (i == 1+2+32+8)  /* node 1, node 3 / node 0, node 2 */
	    {
	      if (id[1] > 0)
		{		
		  fvol = s10*s12 + s30*s32 + s10*s32 - 
		            s10*s12*s32 - s10*s30*s32;
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = bx;
		  intpoints[1*(*nintp)+1] = by;
		  intpoints[2*(*nintp)+1] = bz;
		  intpoints[0*(*nintp)+2] = dx;
		  intpoints[1*(*nintp)+2] = dy;
		  intpoints[2*(*nintp)+2] = dz;
		  intpoints[0*(*nintp)+3] = cx;
		  intpoints[1*(*nintp)+3] = cy;
		  intpoints[2*(*nintp)+3] = cz;
		}
	      else
		{
		  fvol = 1.0 - ((1.0 - s01)*(1.0 - s21)+
				   (1.0 - s03)*(1.0 - s23)+
				   (1.0 - s01)*(1.0 - s23)-
				   (1.0 - s01)*(1.0 - s21)*(1.0 - s23)-
				   (1.0 - s01)*(1.0 - s03)*(1.0 - s23));
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = cx;
		  intpoints[1*(*nintp)+1] = cy;
		  intpoints[2*(*nintp)+1] = cz;
		  intpoints[0*(*nintp)+2] = dx;
		  intpoints[1*(*nintp)+2] = dy;
		  intpoints[2*(*nintp)+2] = dz;
		  intpoints[0*(*nintp)+3] = bx;
		  intpoints[1*(*nintp)+3] = by;
		  intpoints[2*(*nintp)+3] = bz;
		}
	    }
	  if (i == 2+4+8+16)  /* node 2, node 3 / node 0, node 1 */
	    {
	      if (id[2] > 0)
		{
		  fvol = s21*s20 + s31*s30 + s21*s30 -
		            s21*s20*s30 - s21*s31*s30;
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = bx;
		  intpoints[1*(*nintp)+1] = by;
		  intpoints[2*(*nintp)+1] = bz;
		  intpoints[0*(*nintp)+2] = cx;
		  intpoints[1*(*nintp)+2] = cy;
		  intpoints[2*(*nintp)+2] = cz;
		  intpoints[0*(*nintp)+3] = dx;
		  intpoints[1*(*nintp)+3] = dy;
		  intpoints[2*(*nintp)+3] = dz;
		}
	      else
		{
		  fvol = 1.0 - ((1.0 - s12)*(1.0 - s02)+
				   (1.0 - s13)*(1.0 - s03)+
				   (1.0 - s12)*(1.0 - s03)-
				   (1.0 - s12)*(1.0 - s02)*(1.0 - s03)-
				   (1.0 - s12)*(1.0 - s13)*(1.0 - s03));
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = dx;
		  intpoints[1*(*nintp)+1] = dy;
		  intpoints[2*(*nintp)+1] = dz;
		  intpoints[0*(*nintp)+2] = cx;
		  intpoints[1*(*nintp)+2] = cy;
		  intpoints[2*(*nintp)+2] = cz;
		  intpoints[0*(*nintp)+3] = bx;
		  intpoints[1*(*nintp)+3] = by;
		  intpoints[2*(*nintp)+3] = bz;
		}
	    }
	  if (i == 1+4+32+16)  /* node 1, node 2 / node 0, node 3 */
	    {
	      if (id[1] > 0)
		{
		  fvol = s13*s10 + s23*s20 + s13*s20 -
		            s13*s10*s20 - s13*s23*s20;
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = bx;
		  intpoints[1*(*nintp)+1] = by;
		  intpoints[2*(*nintp)+1] = bz;
		  intpoints[0*(*nintp)+2] = dx;
		  intpoints[1*(*nintp)+2] = dy;
		  intpoints[2*(*nintp)+2] = dz;
		  intpoints[0*(*nintp)+3] = cx;
		  intpoints[1*(*nintp)+3] = cy;
		  intpoints[2*(*nintp)+3] = cz;
		}
	      else
		{
		  fvol = 1.0 - ((1.0 - s31)*(1.0 - s01)+
				   (1.0 - s32)*(1.0 - s02)+
				   (1.0 - s31)*(1.0 - s02)-
				   (1.0 - s31)*(1.0 - s01)*(1.0 - s02)-
				   (1.0 - s31)*(1.0 - s32)*(1.0 - s02));
		  intpoints[0*(*nintp)+0] = ax;
		  intpoints[1*(*nintp)+0] = ay;
		  intpoints[2*(*nintp)+0] = az;
		  intpoints[0*(*nintp)+1] = cx;
		  intpoints[1*(*nintp)+1] = cy;
		  intpoints[2*(*nintp)+1] = cz;
		  intpoints[0*(*nintp)+2] = dx;
		  intpoints[1*(*nintp)+2] = dy;
		  intpoints[2*(*nintp)+2] = dz;
		  intpoints[0*(*nintp)+3] = bx;
		  intpoints[1*(*nintp)+3] = by;
		  intpoints[2*(*nintp)+3] = bz;
		}
	    }
	  
	  ax = intpoints[0*(*nintp)+1] - intpoints[0*(*nintp)+0];
	  ay = intpoints[1*(*nintp)+1] - intpoints[1*(*nintp)+0];
	  az = intpoints[2*(*nintp)+1] - intpoints[2*(*nintp)+0];
	  bx = intpoints[0*(*nintp)+2] - intpoints[0*(*nintp)+0];
	  by = intpoints[1*(*nintp)+2] - intpoints[1*(*nintp)+0];
	  bz = intpoints[2*(*nintp)+2] - intpoints[2*(*nintp)+0];
	  cx = intpoints[0*(*nintp)+3] - intpoints[0*(*nintp)+0];
	  cy = intpoints[1*(*nintp)+3] - intpoints[1*(*nintp)+0];
	  cz = intpoints[2*(*nintp)+3] - intpoints[2*(*nintp)+0];

	  *sizeint = 0.5 * sqrt((ay*bz-az*by)*(ay*bz-az*by) + 
				(az*bx-ax*bz)*(az*bx-ax*bz) +
				(ax*by-ay*bx)*(ax*by-ay*bx));

	  *sizeint += 0.5 * sqrt((by*cz-bz*cy)*(by*cz-bz*cy) + 
				 (bz*cx-bx*cz)*(bz*cx-bx*cz) +
				 (bx*cy-by*cx)*(bx*cy-by*cx));
	}
    }

  return fvol;
}



#include <math.h>

#define ERROR  -1.0

/*
 * fracvolex_: compute partial volume in the interface element
 * (fraction of the elemental volume over the reference level)

 *   Input arguments:
 * coords[i*nnpe + j]: coordinate 'i' of the 'j'th node
 * nsd: number of space dimensions
 * v: characteristic function in each node
 * level: level set value

 *   Output arguments:
 * volume: total volume of the element
 * nintp: number of interfase points
 * intpoints[i*nnpe + j]: coordinate 'i' of the 'j'th interface point
 * sizeint: size of the interface
 * (1d: 1, 2d: length of the segment, 3d: area of the triangle/rectangle)
 *
 * ksub: number of subparts
 * nsubp: number of 'phi>level' subparts
 * subpoints: [i*(nnpe*(*nsd))+j*nnpe+k]: coordinate 'j' of the 'k'th
 *            node of the 'i'th subpart
 *
 *   Input:
 * ve: 2th characteristic function in each node
 *   Output:
 * vei[i*nnpe + j]: 'j'th node value of the 'i'th subpart
 */
double fracvolex_(double *coords, int *nsd, double *v, double *level, 
		  double *volume, int *nintp, double *intpoints, 
		  double *sizeint, int *ksub, int *nsubp, double *subpoints,
		  double *ve, double *vei)
{
  int i, j;
  int nnpe;
  int max, min, id[4];
  double fvol;
  double s01,s02,s03, s12,s13, s23;
  double s10,s20,s30, s21,s31, s32;
  double ax,ay,az, bx,by,bz, cx,cy,cz, dx,dy,dz;

  /* Initialization */
  i = j = 0;
  fvol = 0.0;
  s01=s02=s03 = s12=s13 = s23 = 0.0;
  s10=s20=s30 = s21=s31 = s32 = 0.0;
  ax=ay=az = bx=by=bz = cx=cy=cz = dx=dy=dz = 0.0;

  /* Check number of space dimensions */
  if ((*nsd < 1) || (*nsd > 3))
    return ERROR;

  nnpe = *nsd + 1;  /* simplex */
  
  /* Identifying nodes */
  max = min = 0;
  for (i = 0; i < nnpe; i++)
    if (v[i] > *level)
      {
	max++;
	id[i] = +1;
      }
    else
      {
	min++;
	id[i] = -1;
      }

  if (*nsd == 1)
    {  /* One - dimensional case: */
      ax = coords[1] - coords[0];
      *volume = ax*ax;
      *volume = sqrt(*volume);
      
      if (max == nnpe) /* All nodes over level */
	{
	  *nintp = 0;
	  *sizeint = 0.0;
	  *ksub = 1;
	  *nsubp = 1;
	  for (i = 0; i < nnpe; i++) {
	    for (j = 0; j < *nsd; j++)
	      subpoints[j*nnpe+i] = coords[j*nnpe+i];
	    vei[0*nnpe+i] = ve[i];
	  }
	  return 1.0;
	}
      else if (min == nnpe) /* All nodes under level */
	{
	  *nintp = 0;
	  *sizeint = 0.0;
	  *ksub = 1;
	  *nsubp = 0;
	  for (i = 0; i < nnpe; i++) {
	    for (j = 0; j < *nsd; j++)
	      subpoints[j*nnpe+i] = coords[j*nnpe+i];
	    vei[0*nnpe+i] = ve[i];
	  }
	  return 0.0;
	}

      if (id[1] > id[0]) /* 2nd node over level, 1st under */
	{
	  s10 = (v[1] - *level) / (v[1] - v[0]);
	  ax = coords[0] * s10 + coords[1] * (1.0 - s10);
	  fvol = s10;
	}
      else /* 1st node over level, 2nd under */
	{
	  s01 = (v[0] - *level) / (v[0] - v[1]);
	  ax = coords[1] * s01 + coords[0] * (1.0 - s01);
	  fvol = s01;
	}
      
      *nintp = 1;
      intpoints[0] = ax;
      *sizeint = 1;
      *ksub = 2;
      *nsubp = 1;
      if (id[1] > id[0])
	{
	  subpoints[0] = ax;
	  subpoints[1] = coords[1];
	  subpoints[2] = coords[0];
	  subpoints[3] = ax;
	  vei[0] = ve[0] * s10 + ve[1] * (1.0 - s10);
	  vei[1] = ve[1];
	  vei[2] = ve[0];
	  vei[3] = vei[0];
	}
      else
	{
	  subpoints[0] = coords[0];
	  subpoints[1] = ax;
	  subpoints[2] = ax;
	  subpoints[3] = coords[1];
	  vei[0] = ve[0];
	  vei[1] = ve[1] * s01 + ve[0] * (1.0 - s01);
	  vei[2] = vei[1];
	  vei[3] = ve[1];
	}
    }
  else if (*nsd == 2)
    { /* Two - dimensional case */
      int rot;
      ax=coords[1]-coords[0]; ay=coords[4]-coords[3];
      bx=coords[2]-coords[0]; by=coords[5]-coords[3];
      *volume = 0.5 * (ax*by - bx*ay);

      if (max == nnpe)
	{
	  *nintp = 0;
	  *sizeint = 0.0;
	  *ksub = 1;
	  *nsubp = 1;
	  for (i = 0; i < nnpe; i++) {
	    for (j = 0; j < *nsd; j++)
	      subpoints[j*nnpe+i] = coords[j*nnpe+i];
	    vei[i] = ve[i];
	  }
	  return 1.0;
	}
      else if (min == nnpe)
	{
	  *nintp = 0;
	  *sizeint = 0.0;
	  *ksub = 1;
	  *nsubp = 0;
	  for (i = 0; i < nnpe; i++) {
	    for (j = 0; j < *nsd; j++)
	      subpoints[j*nnpe+i] = coords[j*nnpe+i];
	    vei[i] = ve[i];
	  }
 	  return 0.0;
	}

      *nintp = 2;
      *ksub = 3;

      /* Rotation: let's make node 1 the different */
      rot = 0;
      if (id[1] * id[2] > 0) /* The different is 0 */
	{
	  double aux;
	  rot = 1;
	  /* Rotate v */
	  aux = v[2]; v[2] = v[1]; v[1] = v[0]; v[0] = aux;
	  /* Rotate id */
	  i = id[2]; id[2] = id[1]; id[1] = id[0]; id[0] = i; 
	  /* Rotate ve */
	  aux = ve[2]; ve[2] = ve[1]; ve[1] = ve[0]; ve[0] = aux;
	  /* Rotate coordinates */
	  for (j = 0; j < *nsd; j++)
	    {
	      aux = coords[j*nnpe+2];
	      coords[j*nnpe+2] = coords[j*nnpe+1];
	      coords[j*nnpe+1] = coords[j*nnpe+0];
	      coords[j*nnpe+0] = aux;
	    }
	}
      else if (id[0]*id[1] > 0) /* The different is 2 */
	{
	  double aux;
	  rot = -1;
	  /* Rotate v */
	  aux = v[0]; v[0] = v[1]; v[1] = v[2]; v[2] = aux;
	  /* Rotate id */
	  i = id[0]; id[0] = id[1]; id[1] = id[2]; id[2] = i; 
	  /* Rotate ve */
	  aux = ve[0]; ve[0] = ve[1]; ve[1] = ve[2]; ve[2] = aux;
	  /* Rotate coordinates */
	  for (j = 0; j < *nsd; j++)
	    {
	      aux = coords[j*nnpe+0];
	      coords[j*nnpe+0] = coords[j*nnpe+1];
	      coords[j*nnpe+1] = coords[j*nnpe+2];
	      coords[j*nnpe+2] = aux;
	    }
	}

      i = 0;
      if (id[1] > id[0])
	{
	  s10 = (v[1] - *level) / (v[1] - v[0]);
	  ax = coords[0*nnpe+0] * s10 + coords[0*nnpe+1] * (1.0 - s10);
	  ay = coords[1*nnpe+0] * s10 + coords[1*nnpe+1] * (1.0 - s10);
	  s01 = 1.0 - s10;
	  i+=1;
	}
      else if (id[0] > id[1])
	{
	  s01 = (v[0] - *level) / (v[0] - v[1]);
	  ax = coords[0*nnpe+1] * s01 + coords[0*nnpe+0] * (1.0 - s01);
	  ay = coords[1*nnpe+1] * s01 + coords[1*nnpe+0] * (1.0 - s01);
	  s10 = 1.0 - s01;
	  i+=1;
	}
      if (id[2] > id[1])
	{
	  s21 = (v[2] - *level) / (v[2] - v[1]);
	  bx = coords[0*nnpe+1] * s21 + coords[0*nnpe+2] * (1.0 - s21);
	  by = coords[1*nnpe+1] * s21 + coords[1*nnpe+2] * (1.0 - s21);
	  if (i == 0)
	    { ax = bx; ay = by; }
	  s12 = 1.0 - s21;
	  i+=2;
	}
      else if (id[1] > id[2])
	{
	  s12 = (v[1] - *level) / (v[1] - v[2]);
	  bx = coords[0*nnpe+2] * s12 + coords[0*nnpe+1] * (1.0 - s12);
	  by = coords[1*nnpe+2] * s12 + coords[1*nnpe+1] * (1.0 - s12);
	  if (i == 0)
	    { ax = bx; ay = by; }
	  s21 = 1.0 - s12;
	  i+=2;
	}
      /*
      if (id[0] > id[2])
	{
	  s02 = (v[0] - *level) / (v[0] - v[2]);
	  bx = coords[0*nnpe+2] * s02 + coords[0*nnpe+0] * (1.0 - s02);
	  by = coords[1*nnpe+2] * s02 + coords[1*nnpe+0] * (1.0 - s02);
	  s20 = 1.0 - s02;
	  i+=4;
	}
      else if (id[2] > id[0])
	{
	  s20 = (v[2] - *level) / (v[2] - v[0]);
	  bx = coords[0*nnpe+0] * s20 + coords[0*nnpe+2] * (1.0 - s20);
	  by = coords[1*nnpe+0] * s20 + coords[1*nnpe+2] * (1.0 - s20);
	  s02 = 1.0 - s20;
	  i+=4;
	}
      */

      /* i indicates which edge is cut by the level set, edge 0 is 0-1,
	 edge 1 is 1-2, edge 2 is 2-0. if an edge is cut, i is increased by 
	 2**(# of edge). a and b are the intersections */

      if (id[1] > 0)
	{
	  double mod0a, mod2b;
	  *nsubp = 1;
	  fvol = s10 * s12;
	  /* Interfase: a-b (over-level on the right) */
	  intpoints[0*(*nintp)+0] = ax; /* 1 */
	  intpoints[1*(*nintp)+0] = ay; /* 1 */
	  intpoints[0*(*nintp)+1] = bx; /* 2 */
	  intpoints[1*(*nintp)+1] = by; /* 2 */

	  /* sub-part over level: triangle (a 1 b) */
	  subpoints[0*(nnpe*(*nsd)) + 0*nnpe + 0] = ax; /* 1 */
	  subpoints[0*(nnpe*(*nsd)) + 1*nnpe + 0] = ay; /* 1 */
	  subpoints[0*(nnpe*(*nsd)) + 0*nnpe + 1] = coords[0*nnpe+1];
	  subpoints[0*(nnpe*(*nsd)) + 1*nnpe + 1] = coords[1*nnpe+1];
	  subpoints[0*(nnpe*(*nsd)) + 0*nnpe + 2] = bx; /* 2 */
	  subpoints[0*(nnpe*(*nsd)) + 1*nnpe + 2] = by; /* 2 */

	  vei[0*nnpe + 0] = ve[0]*s10 + ve[1]*(1.0 - s10); /* 1 */
	  vei[0*nnpe + 1] = ve[1];
	  vei[0*nnpe + 2] = ve[2]*s12 + ve[1]*(1.0 - s12); /* 2 */

	  /* sub-parts under level:
	   * triangles (a b 2) and (a 2 0) (if |0a| < |2b|)
	   * or:       (a b 0) and (0 b 2) */
	  mod0a = (coords[0*nnpe+0] - ax) * (coords[0*nnpe+0] - ax) +
	    (      coords[1*nnpe+0] - ay) * (coords[1*nnpe+0] - ay);
	  mod2b = (coords[0*nnpe+2] - bx) * (coords[0*nnpe+2] - bx) +
	    (      coords[1*nnpe+2] - by) * (coords[1*nnpe+2] - by);

	  if (mod0a < mod2b)
	    {
	      /* sub-parts under level: triangles (a b 2) and (a 2 0) */
	      vei[1*nnpe + 0] = ve[0]*s10 + ve[1]*(1.0 - s10); /* 1 */
	      vei[1*nnpe + 1] = ve[2]*s12 + ve[1]*(1.0 - s12); /* 2 */
	      vei[1*nnpe + 2] = ve[2];

	      vei[2*nnpe + 0] = ve[0]*s10 + ve[1]*(1.0 - s10); /* 1 */
	      vei[2*nnpe + 1] = ve[2];
	      vei[2*nnpe + 2] = ve[0];

	      subpoints[1*(nnpe*(*nsd)) + 0*nnpe + 0] = ax; /* 1 */
	      subpoints[1*(nnpe*(*nsd)) + 1*nnpe + 0] = ay; /* 1 */
	      subpoints[1*(nnpe*(*nsd)) + 0*nnpe + 1] = bx; /* 2 */
	      subpoints[1*(nnpe*(*nsd)) + 1*nnpe + 1] = by; /* 2 */
	      subpoints[1*(nnpe*(*nsd)) + 0*nnpe + 2] = coords[0*nnpe+2];
	      subpoints[1*(nnpe*(*nsd)) + 1*nnpe + 2] = coords[1*nnpe+2];

	      subpoints[2*(nnpe*(*nsd)) + 0*nnpe + 0] = ax; /* 1 */
	      subpoints[2*(nnpe*(*nsd)) + 1*nnpe + 0] = ay; /* 1 */
	      subpoints[2*(nnpe*(*nsd)) + 0*nnpe + 1] = coords[0*nnpe+2];
	      subpoints[2*(nnpe*(*nsd)) + 1*nnpe + 1] = coords[1*nnpe+2];
	      subpoints[2*(nnpe*(*nsd)) + 0*nnpe + 2] = coords[0*nnpe+0];
	      subpoints[2*(nnpe*(*nsd)) + 1*nnpe + 2] = coords[1*nnpe+0];
	    }
	  else
	    {
	      /* sub-parts under level: triangles (a b 0) and (b 2 0) */
	      vei[1*nnpe + 0] = ve[0]*s10 + ve[1]*(1.0 - s10); /* 1 */
	      vei[1*nnpe + 1] = ve[2]*s12 + ve[1]*(1.0 - s12); /* 2 */
	      vei[1*nnpe + 2] = ve[0];

	      vei[2*nnpe + 0] = ve[2]*s12 + ve[1]*(1.0 - s12); /* 2 */
	      vei[2*nnpe + 1] = ve[2];
	      vei[2*nnpe + 2] = ve[0];

	      subpoints[1*(nnpe*(*nsd)) + 0*nnpe + 0] = ax; /* 1 */
	      subpoints[1*(nnpe*(*nsd)) + 1*nnpe + 0] = ay; /* 1 */
	      subpoints[1*(nnpe*(*nsd)) + 0*nnpe + 1] = bx; /* 2 */
	      subpoints[1*(nnpe*(*nsd)) + 1*nnpe + 1] = by; /* 2 */
	      subpoints[1*(nnpe*(*nsd)) + 0*nnpe + 2] = coords[0*nnpe+0];
	      subpoints[1*(nnpe*(*nsd)) + 1*nnpe + 2] = coords[1*nnpe+0];

	      subpoints[2*(nnpe*(*nsd)) + 0*nnpe + 0] = bx; /* 1 */
	      subpoints[2*(nnpe*(*nsd)) + 1*nnpe + 0] = by; /* 1 */
	      subpoints[2*(nnpe*(*nsd)) + 0*nnpe + 1] = coords[0*nnpe+2];
	      subpoints[2*(nnpe*(*nsd)) + 1*nnpe + 1] = coords[1*nnpe+2];
	      subpoints[2*(nnpe*(*nsd)) + 0*nnpe + 2] = coords[0*nnpe+0];
	      subpoints[2*(nnpe*(*nsd)) + 1*nnpe + 2] = coords[1*nnpe+0];
	    }
	}
      else /* node 0, node 2 / node 1 */
	{
	  double mod0a, mod2b;
	  *nsubp = 2;
	  fvol = 1.0 - (1.0 - s01) * (1.0 - s21);
	  /* Interfase: b-a (over-level on the right) */
	  intpoints[0*(*nintp)+0] = bx; /* 2 */
	  intpoints[1*(*nintp)+0] = by; /* 2 */
	  intpoints[0*(*nintp)+1] = ax; /* 1 */
	  intpoints[1*(*nintp)+1] = ay; /* 1 */

	  /* sub-parts over level:
	   * triangles (a b 2) and (a 2 0) (if |0a| < |2b|)
	   * or:       (a b 0) and (0 b 2) */
	  mod0a = (coords[0*nnpe+0] - ax) * (coords[0*nnpe+0] - ax) +
	    (      coords[1*nnpe+0] - ay) * (coords[1*nnpe+0] - ay);
	  mod2b = (coords[0*nnpe+2] - bx) * (coords[0*nnpe+2] - bx) +
	    (      coords[1*nnpe+2] - by) * (coords[1*nnpe+2] - by);

	  if (mod0a < mod2b)
	    {
	      /* sub-parts over level: triangles (a b 2) and (a 2 0) */
	      vei[0*nnpe + 0] = ve[1]*s01 + ve[0]*(1.0 - s01); /* 1 */
	      vei[0*nnpe + 1] = ve[1]*s21 + ve[2]*(1.0 - s21); /* 2 */
	      vei[0*nnpe + 2] = ve[2];

	      vei[1*nnpe + 0] = ve[1]*s01 + ve[0]*(1.0 - s01); /* 1 */
	      vei[1*nnpe + 1] = ve[2];
	      vei[1*nnpe + 2] = ve[0];

	      subpoints[0*(nnpe*(*nsd)) + 0*nnpe + 0] = ax; /* 1 */
	      subpoints[0*(nnpe*(*nsd)) + 1*nnpe + 0] = ay; /* 1 */
	      subpoints[0*(nnpe*(*nsd)) + 0*nnpe + 1] = bx; /* 2 */
	      subpoints[0*(nnpe*(*nsd)) + 1*nnpe + 1] = by; /* 2 */
	      subpoints[0*(nnpe*(*nsd)) + 0*nnpe + 2] = coords[0*nnpe+2];
	      subpoints[0*(nnpe*(*nsd)) + 1*nnpe + 2] = coords[1*nnpe+2];

	      subpoints[1*(nnpe*(*nsd)) + 0*nnpe + 0] = ax; /* 1 */
	      subpoints[1*(nnpe*(*nsd)) + 1*nnpe + 0] = ay; /* 1 */
	      subpoints[1*(nnpe*(*nsd)) + 0*nnpe + 1] = coords[0*nnpe+2];
	      subpoints[1*(nnpe*(*nsd)) + 1*nnpe + 1] = coords[1*nnpe+2];
	      subpoints[1*(nnpe*(*nsd)) + 0*nnpe + 2] = coords[0*nnpe+0];
	      subpoints[1*(nnpe*(*nsd)) + 1*nnpe + 2] = coords[1*nnpe+0];
	    }
	  else
	    {
	      /* sub-parts over level: triangles (a b 0) and (b 2 0) */
	      vei[0*nnpe + 0] = ve[1]*s01 + ve[0]*(1.0 - s01); /* 1 */
	      vei[0*nnpe + 1] = ve[1]*s21 + ve[2]*(1.0 - s21); /* 2 */
	      vei[0*nnpe + 2] = ve[0];

	      vei[1*nnpe + 0] = ve[1]*s21 + ve[2]*(1.0 - s21); /* 2 */
	      vei[1*nnpe + 1] = ve[2];
	      vei[1*nnpe + 2] = ve[0];

	      subpoints[0*(nnpe*(*nsd)) + 0*nnpe + 0] = ax; /* 1 */
	      subpoints[0*(nnpe*(*nsd)) + 1*nnpe + 0] = ay; /* 1 */
	      subpoints[0*(nnpe*(*nsd)) + 0*nnpe + 1] = bx; /* 2 */
	      subpoints[0*(nnpe*(*nsd)) + 1*nnpe + 1] = by; /* 2 */
	      subpoints[0*(nnpe*(*nsd)) + 0*nnpe + 2] = coords[0*nnpe+0];
	      subpoints[0*(nnpe*(*nsd)) + 1*nnpe + 2] = coords[1*nnpe+0];

	      subpoints[1*(nnpe*(*nsd)) + 0*nnpe + 0] = bx; /* 1 */
	      subpoints[1*(nnpe*(*nsd)) + 1*nnpe + 0] = by; /* 1 */
	      subpoints[1*(nnpe*(*nsd)) + 0*nnpe + 1] = coords[0*nnpe+2];
	      subpoints[1*(nnpe*(*nsd)) + 1*nnpe + 1] = coords[1*nnpe+2];
	      subpoints[1*(nnpe*(*nsd)) + 0*nnpe + 2] = coords[0*nnpe+0];
	      subpoints[1*(nnpe*(*nsd)) + 1*nnpe + 2] = coords[1*nnpe+0];
	    }

	  /* sub-part under level: triangle (a 1 b) */
	  vei[2*nnpe + 0] = ve[1]*s01 + ve[0]*(1.0 - s01); /* 1 */
	  vei[2*nnpe + 1] = ve[1];
	  vei[2*nnpe + 2] = ve[1]*s21 + ve[2]*(1.0 - s21); /* 2 */

	  subpoints[2*(nnpe*(*nsd)) + 0*nnpe + 0] = ax; /* 1 */
	  subpoints[2*(nnpe*(*nsd)) + 1*nnpe + 0] = ay; /* 1 */
	  subpoints[2*(nnpe*(*nsd)) + 0*nnpe + 1] = coords[0*nnpe+1];
	  subpoints[2*(nnpe*(*nsd)) + 1*nnpe + 1] = coords[1*nnpe+1];
	  subpoints[2*(nnpe*(*nsd)) + 0*nnpe + 2] = bx; /* 2 */
	  subpoints[2*(nnpe*(*nsd)) + 1*nnpe + 2] = by; /* 2 */
	}
      /* Restore rotation */
      if (rot == 1)
	{
	  double aux;
	  /* Rotate v */
	  aux = v[0]; v[0] = v[1]; v[1] = v[2]; v[2] = aux;
	  /* Rotate id */
	  i = id[0]; id[0] = id[1]; id[1] = id[2]; id[2] = i; 
	  /* Rotate ve */
	  aux = ve[0]; ve[0] = ve[1]; ve[1] = ve[2]; ve[2] = aux;
	  /* Rotate coordinates */
	  for (j = 0; j < *nsd; j++)
	    {
	      aux = coords[j*nnpe+0];
	      coords[j*nnpe+0] = coords[j*nnpe+1];
	      coords[j*nnpe+1] = coords[j*nnpe+2];
	      coords[j*nnpe+2] = aux;
	    }
	}
      else if (rot == -1)
	{
	  double aux;
	  /* Rotate v */
	  aux = v[2]; v[2] = v[1]; v[1] = v[0]; v[0] = aux;
	  /* Rotate id */
	  i = id[2]; id[2] = id[1]; id[1] = id[0]; id[0] = i; 
	  /* Rotate ve */
	  aux = ve[2]; ve[2] = ve[1]; ve[1] = ve[0]; ve[0] = aux;
	  /* Rotate coordinates */
	  for (j = 0; j < *nsd; j++)
	    {
	      aux = coords[j*nnpe+2];
	      coords[j*nnpe+2] = coords[j*nnpe+1];
	      coords[j*nnpe+1] = coords[j*nnpe+0];
	      coords[j*nnpe+0] = aux;
	    }
	}
    }
  else 
    { /* nsd == 3 */
      int rot, k, istp, istother, ist;
      int perm[5][4] = {{1,2,0,3},{2,0,1,3},{3,0,2,1},{1,3,2,0},{2,3,0,1}};

      ax=coords[1]-coords[0]; ay=coords[5]-coords[4]; az=coords[9]-coords[8]; 
      bx=coords[2]-coords[0]; by=coords[6]-coords[4]; bz=coords[10]-coords[8];
      cx=coords[3]-coords[0]; cy=coords[7]-coords[4]; cz=coords[11]-coords[8];

      *volume = ax*(by*cz-bz*cy)+ay*(bz*cx-bx*cz)+az*(bx*cy-by*cx);
      *volume = *volume / 6.0;

      if (max == nnpe)
	{
	  *nintp = 0;
	  *sizeint = 0.0;
	  *ksub = 1;
	  *nsubp = 1;
	  for (i = 0; i < nnpe; i++) {
	    for (j = 0; j < *nsd; j++)
	      subpoints[j*nnpe+i] = coords[j*nnpe+i];
	    vei[i] = ve[i];
	  }
	  return 1.0;
	}
      else if (min == nnpe)
	{
	  *nintp = 0;
	  *sizeint = 0.0;
	  *ksub = 1;
	  *nsubp = 0;
	  for (i = 0; i < nnpe; i++) {
	    for (j = 0; j < *nsd; j++)
	      subpoints[j*nnpe+i] = coords[j*nnpe+i];
	    vei[j] = ve[j];
	  }
	  return 0.0;
	}

      /* Decide rotations */
      rot = -1;
      if (max == 1 || max == 3) /* Case triangle */
	{  /* Rotate such that first node is the different */
	  if (id[0]+id[2]+id[3] == 3 ||
	      id[0]+id[2]+id[3] == -3) /* 1 -1 1 1 or -1 1 -1 -1 */
	    rot = 0;
	  else if (id[0]+id[1]+id[3] == 3 ||
		   id[0]+id[1]+id[3] == -3) /* 1 1 -1 1 or -1 -1 1 -1 */
	    rot = 1;
	  else if (id[0]+id[1]+id[2] == 3 ||
		   id[0]+id[1]+id[2] == -3) /* 1 1 1 -1 or -1 -1 -1 1 */
	    rot = 2;
	}
      else /* Case quadrilateral */
	{ /* Rotate such that first two nodes are over level */
	  if (id[0]+id[2] == 2) /* 1 -1 1 -1 */
	    rot = 1;
	  else if (id[0]+id[3] == 2) /* 1 -1 -1 1 */
	    rot = 2;
	  else if (id[1]+id[2] == 2) /* -1 1 1 -1 */
	    rot = 0;
	  else if (id[1]+id[3] == 2) /* -1 1 -1 1 */
	    rot = 3;
	  else if (id[2]+id[3] == 2) /* -1 -1 1 1 */
	    rot = 4;
	}
      /* Rotate if needed */
      if (rot > -1)
	{
	  double vaux[4];
	  int iaux[4];

	  /* Rotate v */
	  for (k = 0; k < 4; k++)
	    vaux[k] = v[perm[rot][k]];
	  for (k = 0; k < 4; k++)
	    v[k] = vaux[k];
	  /* Rotate id */
	  for (k = 0; k < 4; k++)
	    iaux[k] = id[perm[rot][k]];
	  for (k = 0; k < 4; k++)
	    id[k] = iaux[k];
	  /* Rotate ve */
	  for (k = 0; k < 4; k++)
	    vaux[k] = ve[perm[rot][k]];
	  for (k = 0; k < 4; k++)
	    ve[k] = vaux[k];
	  /* Rotate coordinates */
	  for (j = 0; j < *nsd; j++)
	    {
	      for (k = 0; k < 4; k++)
		vaux[k] = coords[j*nnpe+perm[rot][k]];
	      for (k = 0; k < 4; k++)
		coords[j*nnpe+k] = vaux[k];
	    }
	}

      /* Compute edge cuts */
      i = 0;
      j = 0;
      if (id[1] > id[0])
	{
	  s10 = (v[1] - *level) / (v[1] - v[0]);
	  ax = coords[0*nnpe+0] * s10 + coords[0*nnpe+1] * (1.0 - s10);
	  ay = coords[1*nnpe+0] * s10 + coords[1*nnpe+1] * (1.0 - s10);
	  az = coords[2*nnpe+0] * s10 + coords[2*nnpe+1] * (1.0 - s10);
	  j++; i+=1;
	}
      else if (id[0] > id[1])
	{
	  s01 = (v[0] - *level) / (v[0] - v[1]);
	  ax = coords[0*nnpe+1] * s01 + coords[0*nnpe+0] * (1.0 - s01);
	  ay = coords[1*nnpe+1] * s01 + coords[1*nnpe+0] * (1.0 - s01);
	  az = coords[2*nnpe+1] * s01 + coords[2*nnpe+0] * (1.0 - s01);
	  j++; i+=1;
	}
      if (id[2] > id[1])
	{
	  s21 = (v[2] - *level) / (v[2] - v[1]);
	  bx = coords[0*nnpe+1] * s21 + coords[0*nnpe+2] * (1.0 - s21);
	  by = coords[1*nnpe+1] * s21 + coords[1*nnpe+2] * (1.0 - s21);
	  bz = coords[2*nnpe+1] * s21 + coords[2*nnpe+2] * (1.0 - s21);
	  if (i == 0)
	    { ax = bx; ay = by; az = bz; }
	  j++; i+=2;
	}
      else if (id[1] > id[2])
	{
	  s12 = (v[1] - *level) / (v[1] - v[2]);
	  bx = coords[0*nnpe+2] * s12 + coords[0*nnpe+1] * (1.0 - s12);
	  by = coords[1*nnpe+2] * s12 + coords[1*nnpe+1] * (1.0 - s12);
	  bz = coords[2*nnpe+2] * s12 + coords[2*nnpe+1] * (1.0 - s12);
	  if (i == 0)
	    { ax = bx; ay = by; az = bz; }
	  j++; i+=2;
	}
      if (id[0] > id[2])
	{
	  s02 = (v[0] - *level) / (v[0] - v[2]);
	  bx = coords[0*nnpe+2] * s02 + coords[0*nnpe+0] * (1.0 - s02);
	  by = coords[1*nnpe+2] * s02 + coords[1*nnpe+0] * (1.0 - s02);
	  bz = coords[2*nnpe+2] * s02 + coords[2*nnpe+0] * (1.0 - s02);
	  j++; i+=4;
	}
      else if (id[2] > id[0])
	{
	  s20 = (v[2] - *level) / (v[2] - v[0]);
	  bx = coords[0*nnpe+0] * s20 + coords[0*nnpe+2] * (1.0 - s20);
	  by = coords[1*nnpe+0] * s20 + coords[1*nnpe+2] * (1.0 - s20);
	  bz = coords[2*nnpe+0] * s20 + coords[2*nnpe+2] * (1.0 - s20);
	  j++; i+=4;
	}
      if (id[3] > id[0])
	{
	  s30 = (v[3] - *level) / (v[3] - v[0]);
	  cx = coords[0*nnpe+0] * s30 + coords[0*nnpe+3] * (1.0 - s30);
	  cy = coords[1*nnpe+0] * s30 + coords[1*nnpe+3] * (1.0 - s30);
	  cz = coords[2*nnpe+0] * s30 + coords[2*nnpe+3] * (1.0 - s30);
	  if (j == 0)
	    { ax = cx; ay = cy; az = cz; } 
	  j++; i+=8;
	}
      else if (id[0] > id[3])
	{
	  s03 = (v[0] - *level) / (v[0] - v[3]);
	  cx = coords[0*nnpe+3] * s03 + coords[0*nnpe+0] * (1.0 - s03);
	  cy = coords[1*nnpe+3] * s03 + coords[1*nnpe+0] * (1.0 - s03);
	  cz = coords[2*nnpe+3] * s03 + coords[2*nnpe+0] * (1.0 - s03);
	  if (j == 0)
	    { ax = cx; ay = cy; az = cz; } 
	  j++; i+=8;
	}
      if (id[3] > id[1])
	{
	  s31 = (v[3] - *level) / (v[3] - v[1]);
	  dx = coords[0*nnpe+1] * s31 + coords[0*nnpe+3] * (1.0 - s31);
	  dy = coords[1*nnpe+1] * s31 + coords[1*nnpe+3] * (1.0 - s31);
	  dz = coords[2*nnpe+1] * s31 + coords[2*nnpe+3] * (1.0 - s31);
	  if (j == 1)
	    { bx = dx; by = dy; bz = dz; } 
	  if (j == 2)
	    { cx = dx, cy = dy, cz = dz; }
	  j++; i+=16;
	}
      else if (id[1] > id[3])
	{
	  s13 = (v[1] - *level) / (v[1] - v[3]);
	  dx = coords[0*nnpe+3] * s13 + coords[0*nnpe+1] * (1.0 - s13);
	  dy = coords[1*nnpe+3] * s13 + coords[1*nnpe+1] * (1.0 - s13);
	  dz = coords[2*nnpe+3] * s13 + coords[2*nnpe+1] * (1.0 - s13);
	  if (j == 1)
	    { bx = dx; by = dy; bz = dz; } 
	  if (j == 2)
	    { cx = dx, cy = dy, cz = dz; }
	  j++; i+=16;
	}
      if (id[3] > id[2])
	{
	  s32 = (v[3] - *level) / (v[3] - v[2]);
	  dx = coords[0*nnpe+2] * s32 + coords[0*nnpe+3] * (1.0 - s32);
	  dy = coords[1*nnpe+2] * s32 + coords[1*nnpe+3] * (1.0 - s32);
	  dz = coords[2*nnpe+2] * s32 + coords[2*nnpe+3] * (1.0 - s32);
	  if (j == 2)
	    { cx = dx; cy = dy; cz = dz; } 
	  j++; i+=32;
	}
      else if (id[2] > id[3])
	{
	  s23 = (v[2] - *level) / (v[2] - v[3]);
	  dx = coords[0*nnpe+3] * s23 + coords[0*nnpe+2] * (1.0 - s23);
	  dy = coords[1*nnpe+3] * s23 + coords[1*nnpe+2] * (1.0 - s23);
	  dz = coords[2*nnpe+3] * s23 + coords[2*nnpe+2] * (1.0 - s23);
	  if (j == 2)
	    { cx = dx, cy = dy, cz = dz; }
	  j++; i+=32;
	}

      if (j == 3)   /* triangle */
	{
	  double mod1a, mod2b, mod3c;
	  *nintp = 3;
	  *ksub = 4;

	  if (id[0] > 0)
	    {
	      *nsubp = 1;
	      istp = 0;
	      istother = 1;
	      fvol = s02 * s01 * s03;

	      /* Counter clockwise from "positive" side */
	      intpoints[0*(*nintp)+0] = bx; /* 4 */
	      intpoints[1*(*nintp)+0] = by; /* 4 */
	      intpoints[2*(*nintp)+0] = bz; /* 4 */
	      intpoints[0*(*nintp)+1] = ax; /* 1 */
	      intpoints[1*(*nintp)+1] = ay; /* 1 */
	      intpoints[2*(*nintp)+1] = az; /* 1 */
	      intpoints[0*(*nintp)+2] = cx; /* 8 */
	      intpoints[1*(*nintp)+2] = cy; /* 8 */
	      intpoints[2*(*nintp)+2] = cz; /* 8 */
	    }
	  else
	    {
	      *nsubp = 3;
	      istp = 3;
	      istother = 0;
	      fvol = 1.0 - (1.0 - s20)*(1.0 - s10)*(1.0 - s30);
	      s01 = 1.0 - s10;
	      s02 = 1.0 - s20;
	      s03 = 1.0 - s30;

	      /* Counter clockwise from "positive" side */
	      intpoints[0*(*nintp)+0] = ax; /* 1 */
	      intpoints[1*(*nintp)+0] = ay; /* 1 */
	      intpoints[2*(*nintp)+0] = az; /* 1 */
	      intpoints[0*(*nintp)+1] = bx; /* 4 */
	      intpoints[1*(*nintp)+1] = by; /* 4 */
	      intpoints[2*(*nintp)+1] = bz; /* 4 */
	      intpoints[0*(*nintp)+2] = cx; /* 8 */
	      intpoints[1*(*nintp)+2] = cy; /* 8 */
	      intpoints[2*(*nintp)+2] = cz; /* 8 */
	    }

	  /* "first" sub-tetrahedron: b a c 0 */
	  vei[istp*nnpe + 0] = ve[2]*s02 + ve[0]*(1.0 - s02); /* 4 */
	  vei[istp*nnpe + 1] = ve[1]*s01 + ve[0]*(1.0 - s01); /* 1 */
	  vei[istp*nnpe + 2] = ve[3]*s03 + ve[0]*(1.0 - s03); /* 8 */
	  vei[istp*nnpe + 3] = ve[0];

	  subpoints[istp*(nnpe*(*nsd)) + 0*nnpe + 0] = bx; /* 4 */
	  subpoints[istp*(nnpe*(*nsd)) + 1*nnpe + 0] = by; /* 4 */
	  subpoints[istp*(nnpe*(*nsd)) + 2*nnpe + 0] = bz; /* 4 */
	  subpoints[istp*(nnpe*(*nsd)) + 0*nnpe + 1] = ax; /* 1 */
	  subpoints[istp*(nnpe*(*nsd)) + 1*nnpe + 1] = ay; /* 1 */
	  subpoints[istp*(nnpe*(*nsd)) + 2*nnpe + 1] = az; /* 1 */
	  subpoints[istp*(nnpe*(*nsd)) + 0*nnpe + 2] = cx; /* 8 */
	  subpoints[istp*(nnpe*(*nsd)) + 1*nnpe + 2] = cy; /* 8 */
	  subpoints[istp*(nnpe*(*nsd)) + 2*nnpe + 2] = cz; /* 8 */
	  subpoints[istp*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+0];
	  subpoints[istp*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+0];
	  subpoints[istp*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+0];

	  /* Decide how to split prism: 1a 2b 3c */
	  mod1a =
	    (coords[0*nnpe+1]-ax)*(coords[0*nnpe+1]-ax) +
	    (coords[1*nnpe+1]-ay)*(coords[1*nnpe+1]-ay) +
	    (coords[2*nnpe+1]-az)*(coords[2*nnpe+1]-az);
	  mod2b =
	    (coords[0*nnpe+2]-bx)*(coords[0*nnpe+2]-bx) +
	    (coords[1*nnpe+2]-by)*(coords[1*nnpe+2]-by) +
	    (coords[2*nnpe+2]-bz)*(coords[2*nnpe+2]-bz);
	  mod3c =
	    (coords[0*nnpe+3]-cx)*(coords[0*nnpe+3]-cx) +
	    (coords[1*nnpe+3]-cy)*(coords[1*nnpe+3]-cy) +
	    (coords[2*nnpe+3]-cz)*(coords[2*nnpe+3]-cz);

	  if (mod1a <= mod2b && mod1a <= mod3c) /* 1a is the smallest edge */
	    {
	      /* create tetrahedron: (2 1 3 a) */
	      vei[istother*nnpe + 0] = ve[2];
	      vei[istother*nnpe + 1] = ve[1];
	      vei[istother*nnpe + 2] = ve[3];
	      vei[istother*nnpe + 3] = ve[1]*s01 + ve[0]*(1.0 - s01); /* a */

	      subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+2];
	      subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+2];
	      subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+2];
	      subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 1] = coords[0*nnpe+1];
	      subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 1] = coords[1*nnpe+1];
	      subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 1] = coords[2*nnpe+1];
	      subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 2] = coords[0*nnpe+3];
	      subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 2] = coords[1*nnpe+3];
	      subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 2] = coords[2*nnpe+3];
	      subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 3] = ax;
	      subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 3] = ay;
	      subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 3] = az;
	      istother++;

	      if (mod2b <= mod3c) /* 2b is smaller than 3c */
		{
		  /* create tetrahedra: (b 2 3 a) and (b 3 c a) */
		  vei[istother*nnpe + 0] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		  vei[istother*nnpe + 1] = ve[2];
		  vei[istother*nnpe + 2] = ve[3];
		  vei[istother*nnpe + 3] = ve[1]*s01 + ve[0]*(1.0 - s01); /* a */

		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 0] = bx;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 0] = by;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 0] = bz;
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 1] = coords[0*nnpe+2];
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 1] = coords[1*nnpe+2];
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 1] = coords[2*nnpe+2];
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 2] = coords[0*nnpe+3];
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 2] = coords[1*nnpe+3];
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 2] = coords[2*nnpe+3];
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 3] = ax; /* a */
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 3] = ay; /* a */
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 3] = az; /* a */
		  istother++;

		  vei[istother*nnpe + 0] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		  vei[istother*nnpe + 1] = ve[3];
		  vei[istother*nnpe + 2] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		  vei[istother*nnpe + 3] = ve[1]*s01 + ve[0]*(1.0 - s01); /* a */

		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 0] = bx;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 0] = by;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 0] = bz;
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 1] = coords[0*nnpe+3];
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 1] = coords[1*nnpe+3];
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 1] = coords[2*nnpe+3];
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 2] = cx;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 2] = cy;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 2] = cz;
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 3] = ax;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 3] = ay;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 3] = az;
		}
	      else
		{
		  /* create tetrahedra: (b 2 c a) and (2 3 c a) */
		  vei[istother*nnpe + 0] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		  vei[istother*nnpe + 1] = ve[2];
		  vei[istother*nnpe + 2] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		  vei[istother*nnpe + 3] = ve[1]*s01 + ve[0]*(1.0 - s01); /* a */

		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 0] = bx;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 0] = by;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 0] = bz;
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 1] = coords[0*nnpe+2];
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 1] = coords[1*nnpe+2];
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 1] = coords[2*nnpe+2];
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 2] = cx;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 2] = cy;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 2] = cz;
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 3] = ax;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 3] = ay;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 3] = az;
		  istother++;

		  vei[istother*nnpe + 0] = ve[2];
		  vei[istother*nnpe + 1] = ve[3];
		  vei[istother*nnpe + 2] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		  vei[istother*nnpe + 3] = ve[1]*s01 + ve[0]*(1.0 - s01); /* a */

		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+2];
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+2];
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+2];
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 1] = coords[0*nnpe+3];
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 1] = coords[1*nnpe+3];
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 1] = coords[2*nnpe+3];
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 2] = cx;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 2] = cy;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 2] = cz;
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 3] = ax;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 3] = ay;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 3] = az;
		}
	    } /* end if(mod1a is the smallest edge) */
	  else if (mod2b <= mod3c) /* mod2b is the smallest edge */
	    {
	      	      /* create tetrahedron: (2 1 3 b) */
	      vei[istother*nnpe + 0] = ve[2];
	      vei[istother*nnpe + 1] = ve[1];
	      vei[istother*nnpe + 2] = ve[3];
	      vei[istother*nnpe + 3] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */

	      subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+2];
	      subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+2];
	      subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+2];
	      subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 1] = coords[0*nnpe+1];
	      subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 1] = coords[1*nnpe+1];
	      subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 1] = coords[2*nnpe+1];
	      subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 2] = coords[0*nnpe+3];
	      subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 2] = coords[1*nnpe+3];
	      subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 2] = coords[2*nnpe+3];
	      subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 3] = bx;
	      subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 3] = by;
	      subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 3] = bz;
	      istother++;

	      if (mod1a <= mod3c)
		{
		  /* create tetrahedra: (3 1 a b) and (b 3 c a) */
		  vei[istother*nnpe + 0] = ve[3];
		  vei[istother*nnpe + 1] = ve[1];
		  vei[istother*nnpe + 2] = ve[1]*s01 + ve[0]*(1.0 - s01); /* a */
		  vei[istother*nnpe + 3] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */

		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+3];
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+3];
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+3];
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 1] = coords[0*nnpe+1];
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 1] = coords[1*nnpe+1];
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 1] = coords[2*nnpe+1];
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 2] = ax;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 2] = ay;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 2] = az;
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 3] = bx;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 3] = by;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 3] = bz;
		  istother++;

		  vei[istother*nnpe + 0] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		  vei[istother*nnpe + 1] = ve[3];
		  vei[istother*nnpe + 2] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		  vei[istother*nnpe + 3] = ve[1]*s01 + ve[0]*(1.0 - s01); /* a */

		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 0] = bx;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 0] = by;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 0] = bz;
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 1] = coords[0*nnpe+3];
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 1] = coords[1*nnpe+3];
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 1] = coords[2*nnpe+3];
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 2] = cx;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 2] = cy;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 2] = cz;
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 3] = ax;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 3] = ay;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 3] = az;
		}
	      else
		{
		  /* create tetrahedra: (3 1 c b) and (1 a c b) */
		  vei[istother*nnpe + 0] = ve[3];
		  vei[istother*nnpe + 1] = ve[1];
		  vei[istother*nnpe + 2] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		  vei[istother*nnpe + 3] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */

		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+3];
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+3];
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+3];
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 1] = coords[0*nnpe+1];
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 1] = coords[1*nnpe+1];
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 1] = coords[2*nnpe+1];
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 2] = cx;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 2] = cy;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 2] = cz;
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 3] = bx;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 3] = by;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 3] = bz;
		  istother++;

		  vei[istother*nnpe + 0] = ve[1];
		  vei[istother*nnpe + 1] = ve[1]*s01 + ve[0]*(1.0 - s01); /* a */
		  vei[istother*nnpe + 2] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		  vei[istother*nnpe + 3] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */

		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+1];
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+1];
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+1];
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 1] = ax;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 1] = ay;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 1] = az;
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 2] = cx;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 2] = cy;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 2] = cz;
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 3] = bx;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 3] = by;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 3] = bz;
		}
	    } /* end if(2b is the smallest edge) */
	  else /* then -> 3c is the smallest edge */
	    {
	      /* create tetrahedron: (2 1 3 c) */
	      vei[istother*nnpe + 0] = ve[2];
	      vei[istother*nnpe + 1] = ve[1];
	      vei[istother*nnpe + 2] = ve[3];
	      vei[istother*nnpe + 3] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */

	      subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+2];
	      subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+2];
	      subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+2];
	      subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 1] = coords[0*nnpe+1];
	      subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 1] = coords[1*nnpe+1];
	      subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 1] = coords[2*nnpe+1];
	      subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 2] = coords[0*nnpe+3];
	      subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 2] = coords[1*nnpe+3];
	      subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 2] = coords[2*nnpe+3];
	      subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 3] = cx;
	      subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 3] = cy;
	      subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 3] = cz;
	      istother++;

	      if (mod1a <= mod2b)
		{
		  /* create tetrahedra: (1 2 a c) and (b 2 c a) */
		  vei[istother*nnpe + 0] = ve[1];
		  vei[istother*nnpe + 1] = ve[2];
		  vei[istother*nnpe + 2] = ve[1]*s01 + ve[0]*(1.0 - s01); /* a */
		  vei[istother*nnpe + 3] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */

		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+1];
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+1];
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+1];
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 1] = coords[0*nnpe+2];
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 1] = coords[1*nnpe+2];
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 1] = coords[2*nnpe+2];
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 2] = ax;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 2] = ay;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 2] = az;
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 3] = cx;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 3] = cy;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 3] = cz;
		  istother++;

		  vei[istother*nnpe + 0] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		  vei[istother*nnpe + 1] = ve[2];
		  vei[istother*nnpe + 2] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		  vei[istother*nnpe + 3] = ve[1]*s01 + ve[0]*(1.0 - s01); /* a */

		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 0] = bx;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 0] = by;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 0] = bz;
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 1] = coords[0*nnpe+2];
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 1] = coords[1*nnpe+2];
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 1] = coords[2*nnpe+2];
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 2] = cx;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 2] = cy;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 2] = cz;
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 3] = ax;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 3] = ay;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 3] = az;
		}
	      else
		{
		  /* create tetrahedra: (1 2 b c) and (1 a c b) */
		  vei[istother*nnpe + 0] = ve[1];
		  vei[istother*nnpe + 1] = ve[2];
		  vei[istother*nnpe + 2] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		  vei[istother*nnpe + 3] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */

		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+1];
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+1];
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+1];
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 1] = coords[0*nnpe+2];
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 1] = coords[1*nnpe+2];
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 1] = coords[2*nnpe+2];
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 2] = bx;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 2] = by;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 2] = bz;
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 3] = cx;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 3] = cy;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 3] = cz;
		  istother++;

		  vei[istother*nnpe + 0] = ve[1];
		  vei[istother*nnpe + 1] = ve[1]*s01 + ve[0]*(1.0 - s01); /* a */
		  vei[istother*nnpe + 2] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		  vei[istother*nnpe + 3] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */

		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+1];
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+1];
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+1];
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 1] = ax;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 1] = ay;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 1] = az;
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 2] = cx;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 2] = cy;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 2] = cz;
		  subpoints[istother*(nnpe*(*nsd)) + 0*nnpe + 3] = bx;
		  subpoints[istother*(nnpe*(*nsd)) + 1*nnpe + 3] = by;
		  subpoints[istother*(nnpe*(*nsd)) + 2*nnpe + 3] = bz;
		}

	    } /* end block mod3c is the smallest edge */

	  ax = intpoints[0*(*nintp)+1] - intpoints[0*(*nintp)+0];
	  ay = intpoints[1*(*nintp)+1] - intpoints[1*(*nintp)+0];
	  az = intpoints[2*(*nintp)+1] - intpoints[2*(*nintp)+0];
	  bx = intpoints[0*(*nintp)+2] - intpoints[0*(*nintp)+0];
	  by = intpoints[1*(*nintp)+2] - intpoints[1*(*nintp)+0];
	  bz = intpoints[2*(*nintp)+2] - intpoints[2*(*nintp)+0];
	  
	  *sizeint = 0.5 * sqrt((ay*bz-az*by)*(ay*bz-az*by) + 
				(az*bx-ax*bz)*(az*bx-ax*bz) +
				(ax*by-ay*bx)*(ax*by-ay*bx));
	}
      else   /* quadrilateral */ 
	{
	  double mod0b, mod0c, mod1a, mod1d;
	  double mod2a, mod2b, mod3c, mod3d;
	  double modac, modbd;
	  *nintp = 4;
	  *ksub = 6;
	  *nsubp = 3;

	  /* Ok, abajo va simplificado:
	    fvol = 1.0 - ((1.0 - s12)*(1.0 - s02)+
	    (1.0 - s13)*(1.0 - s03)+
	    (1.0 - s12)*(1.0 - s03)-
	    (1.0 - s12)*(1.0 - s02)*(1.0 - s03)-
	    (1.0 - s12)*(1.0 - s13)*(1.0 - s03));

	    vol(adcb01) = vol(a1d0) + vol(c0ba) + vol(cd0a)
	    vol(a1d0) = s12*s13

	    vol(c0b2) = 0
	    vol(c0b1) = s02*s03
	    a in (12) => vol(c0ba) = (1-s12) * s02*s03

	    vol(0d20) = 0
	    vol(0d23) = (1 - s13)
	    c in (03) => vol(0d2c) = s03 * (1 - s13)

	    vol(cd01) = 0
	    vol(cd02) = vol(0d2d) = s03 * (1 - s13)
	    a in (12) => vol(cd0a) = s12 * s03 * (1 - s13)

	    operando:...
	  */
	  fvol = s02 * s03 + s03 * s12 + s12 * s13
	    - s03 * s12 * (s02 + s13);

	  /* Counter clockwise from "positive" side */
	  intpoints[0*(*nintp)+0] = ax; /* 2 */
	  intpoints[1*(*nintp)+0] = ay; /* 2 */
	  intpoints[2*(*nintp)+0] = az; /* 2 */
	  intpoints[0*(*nintp)+1] = dx; /* 16 */
	  intpoints[1*(*nintp)+1] = dy; /* 16 */
	  intpoints[2*(*nintp)+1] = dz; /* 16 */
	  intpoints[0*(*nintp)+2] = cx; /* 8 */
	  intpoints[1*(*nintp)+2] = cy; /* 8 */
	  intpoints[2*(*nintp)+2] = cz; /* 8 */
	  intpoints[0*(*nintp)+3] = bx; /* 4 */
	  intpoints[1*(*nintp)+3] = by; /* 4 */
	  intpoints[2*(*nintp)+3] = bz; /* 4 */

	  /* Consider prism: 10 ab dc,  Decide how to split */
	  mod0c =
	    (cx-coords[0*nnpe+0])*(cx-coords[0*nnpe+0]) +
	    (cy-coords[1*nnpe+0])*(cy-coords[1*nnpe+0]) +
	    (cz-coords[2*nnpe+0])*(cz-coords[2*nnpe+0]);
	  mod1d =
	    (dx-coords[0*nnpe+1])*(dx-coords[0*nnpe+1]) +
	    (dy-coords[1*nnpe+1])*(dy-coords[1*nnpe+1]) +
	    (dz-coords[2*nnpe+1])*(dz-coords[2*nnpe+1]);
	  mod0b =
	    (bx-coords[0*nnpe+0])*(bx-coords[0*nnpe+0]) +
	    (by-coords[1*nnpe+0])*(by-coords[1*nnpe+0]) +
	    (bz-coords[2*nnpe+0])*(bz-coords[2*nnpe+0]);
	  mod1a =
	    (ax-coords[0*nnpe+1])*(ax-coords[0*nnpe+1]) +
	    (ay-coords[1*nnpe+1])*(ay-coords[1*nnpe+1]) +
	    (az-coords[2*nnpe+1])*(az-coords[2*nnpe+1]);

	  ist = 0;
	  if (mod0c <= mod1d) /* Edge 1c should exists */
	    {
	      if (mod0b <= mod1a) /* Edge 1b should exists */
		{ /* form tetrahedron (0 b c 1) */
		  vei[ist*nnpe + 0] = ve[0];
		  vei[ist*nnpe + 1] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		  vei[ist*nnpe + 2] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		  vei[ist*nnpe + 3] = ve[1];

		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+0];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+0];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+0];
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = bx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = by;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = bz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = cx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = cy;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = cz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+1];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+1];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+1];
		  ist++;

		  modac = (ax-cx)*(ax-cx)+(ay-cy)*(ay-cy)+(az-cz)*(az-cz);
		  modbd = (bx-dx)*(bx-dx)+(by-dy)*(by-dy)+(bz-dz)*(bz-dz);
		  if (modac <= modbd)
		    { /* form tetrahedra: (c b a 1) (a d c 1) */
		      vei[ist*nnpe + 0] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		      vei[ist*nnpe + 1] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		      vei[ist*nnpe + 2] = ve[2]*s12 + ve[1]*(1.0 - s12); /* a */
		      vei[ist*nnpe + 3] = ve[1];

		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = cx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = cy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = cz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = bx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = by;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = bz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = ax;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = ay;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = az;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+1];
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+1];
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+1];
		      ist++;

		      vei[ist*nnpe + 0] = ve[2]*s12 + ve[1]*(1.0 - s12); /* a */
		      vei[ist*nnpe + 1] = ve[3]*s13 + ve[1]*(1.0 - s13); /* d */
		      vei[ist*nnpe + 2] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		      vei[ist*nnpe + 3] = ve[1];

		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = ax;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = ay;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = az;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = dx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = dy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = dz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = cx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = cy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = cz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+1];
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+1];
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+1];
		      ist++;
		    }
		  else
		    { /* form tetrahedra: (b a d 1) (d c b 1) */
		      vei[ist*nnpe + 0] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		      vei[ist*nnpe + 1] = ve[2]*s12 + ve[1]*(1.0 - s12); /* a */
		      vei[ist*nnpe + 2] = ve[3]*s13 + ve[1]*(1.0 - s13); /* d */
		      vei[ist*nnpe + 3] = ve[1];

		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = bx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = by;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = bz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = ax;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = ay;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = az;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = dx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = dy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = dz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+1];
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+1];
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+1];
		      ist++;

		      vei[ist*nnpe + 0] = ve[3]*s13 + ve[1]*(1.0 - s13); /* d */
		      vei[ist*nnpe + 1] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		      vei[ist*nnpe + 2] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		      vei[ist*nnpe + 3] = ve[1];

		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = dx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = dy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = dz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = cx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = cy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = cz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = bx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = by;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = bz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+1];
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+1];
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+1];
		      ist++;
		    }
		}
	      else /* Edge 0a should exists */
		{ /* Form tetrahedra: (0 b c a) (0 c 1 a) (1 c d a = a d c 1) */
		  vei[ist*nnpe + 0] = ve[0];
		  vei[ist*nnpe + 1] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		  vei[ist*nnpe + 2] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		  vei[ist*nnpe + 3] = ve[2]*s12 + ve[1]*(1.0 - s12); /* a */

		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+0];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+0];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+0];
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = bx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = by;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = bz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = cx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = cy;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = cz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = ax;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = ay;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = az;
		  ist++;

		  vei[ist*nnpe + 0] = ve[0];
		  vei[ist*nnpe + 1] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		  vei[ist*nnpe + 2] = ve[1];
		  vei[ist*nnpe + 3] = ve[2]*s12 + ve[1]*(1.0 - s12); /* a */

		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+0];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+0];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+0];
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = cx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = cy;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = cz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = coords[0*nnpe+1];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = coords[1*nnpe+1];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = coords[2*nnpe+1];
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = ax;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = ay;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = az;
		  ist++;

		  vei[ist*nnpe + 0] = ve[2]*s12 + ve[1]*(1.0 - s12); /* a */
		  vei[ist*nnpe + 1] = ve[3]*s13 + ve[1]*(1.0 - s13); /* d */
		  vei[ist*nnpe + 2] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		  vei[ist*nnpe + 3] = ve[1];

		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = ax;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = ay;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = az;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = dx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = dy;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = dz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = cx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = cy;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = cz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+1];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+1];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+1];
		  ist++;
		}
	    }
	  else /* Edge 0d should exists */
	    {
	      if (mod0b <= mod1a) /* Edge 1b should exists */
		{ /* form tetrahedra: (0 b c d) (1 a b d = b a d 1) (b 0 1 d = 0 b d 1) */
		  vei[ist*nnpe + 0] = ve[0];
		  vei[ist*nnpe + 1] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		  vei[ist*nnpe + 2] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		  vei[ist*nnpe + 3] = ve[3]*s13 + ve[1]*(1.0 - s13); /* d */

		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+0];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+0];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+0];
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = bx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = by;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = bz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = cx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = cy;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = cz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = dx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = dy;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = dz;
		  ist++;

		  vei[ist*nnpe + 0] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		  vei[ist*nnpe + 1] = ve[2]*s12 + ve[1]*(1.0 - s12); /* a */
		  vei[ist*nnpe + 2] = ve[3]*s13 + ve[1]*(1.0 - s13); /* d */
		  vei[ist*nnpe + 3] = ve[1];

		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = bx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = by;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = bz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = ax;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = ay;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = az;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = dx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = dy;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = dz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+1];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+1];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+1];
		  ist++;

		  vei[ist*nnpe + 0] = ve[0];
		  vei[ist*nnpe + 1] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		  vei[ist*nnpe + 2] = ve[3]*s13 + ve[1]*(1.0 - s13); /* d */
		  vei[ist*nnpe + 3] = ve[1];

		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+0];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+0];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+0];
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = bx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = by;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = bz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = dx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = dy;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = dz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+1];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+1];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+1];
		  ist++;
		}
	      else /* Edge 0a should exists */
		{ /* form tetrahedron (1 d a 0) */
		  vei[ist*nnpe + 0] = ve[1];
		  vei[ist*nnpe + 1] = ve[3]*s13 + ve[1]*(1.0 - s13); /* d */
		  vei[ist*nnpe + 2] = ve[2]*s12 + ve[1]*(1.0 - s12); /* a */
		  vei[ist*nnpe + 3] = ve[0];

		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+1];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+1];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+1];
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = dx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = dy;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = dz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = ax;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = ay;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = az;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+0];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+0];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+0];
		  ist++;

		  modac = (ax-cx)*(ax-cx)+(ay-cy)*(ay-cy)+(az-cz)*(az-cz);
		  modbd = (bx-dx)*(bx-dx)+(by-dy)*(by-dy)+(bz-dz)*(bz-dz);
		  if (modac <= modbd)
		    { /* form tetrahedra: (a d c 0) (c b a 0) */
		      vei[ist*nnpe + 0] = ve[2]*s12 + ve[1]*(1.0 - s12); /* a */
		      vei[ist*nnpe + 1] = ve[3]*s13 + ve[1]*(1.0 - s13); /* d */
		      vei[ist*nnpe + 2] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		      vei[ist*nnpe + 3] = ve[0];

		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = ax;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = ay;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = az;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = dx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = dy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = dz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = cx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = cy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = cz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+0];
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+0];
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+0];
		      ist++;

		      vei[ist*nnpe + 0] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		      vei[ist*nnpe + 1] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		      vei[ist*nnpe + 2] = ve[2]*s12 + ve[1]*(1.0 - s12); /* a */
		      vei[ist*nnpe + 3] = ve[0];

		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = cx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = cy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = cz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = bx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = by;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = bz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = ax;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = ay;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = az;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+0];
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+0];
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+0];
		      ist++;
		    }
		  else
		    { /* form tetrahedra: (b a d 0) (d c b 0) */
		      vei[ist*nnpe + 0] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		      vei[ist*nnpe + 1] = ve[2]*s12 + ve[1]*(1.0 - s12); /* a */
		      vei[ist*nnpe + 2] = ve[3]*s13 + ve[1]*(1.0 - s13); /* d */
		      vei[ist*nnpe + 3] = ve[0];

		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = bx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = by;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = bz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = ax;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = ay;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = az;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = dx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = dy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = dz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+0];
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+0];
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+0];
		      ist++;

		      vei[ist*nnpe + 0] = ve[3]*s13 + ve[1]*(1.0 - s13); /* d */
		      vei[ist*nnpe + 1] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		      vei[ist*nnpe + 2] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		      vei[ist*nnpe + 3] = ve[0];

		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = dx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = dy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = dz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = cx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = cy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = cz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = bx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = by;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = bz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+0];
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+0];
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+0];
		      ist++;
		    }
		}
	    }

	  /* Consider prism: 32 cb da,  Decide how to split */
	  mod2a =
	    (ax-coords[0*nnpe+2])*(ax-coords[0*nnpe+2]) +
	    (ay-coords[1*nnpe+2])*(ay-coords[1*nnpe+2]) +
	    (az-coords[2*nnpe+2])*(az-coords[2*nnpe+2]);
	  mod3d =
	    (dx-coords[0*nnpe+3])*(dx-coords[0*nnpe+3]) +
	    (dy-coords[1*nnpe+3])*(dy-coords[1*nnpe+3]) +
	    (dz-coords[2*nnpe+3])*(dz-coords[2*nnpe+3]);
	  mod2b =
	    (bx-coords[0*nnpe+2])*(bx-coords[0*nnpe+2]) +
	    (by-coords[1*nnpe+2])*(by-coords[1*nnpe+2]) +
	    (bz-coords[2*nnpe+2])*(bz-coords[2*nnpe+2]);
	  mod3c =
	    (cx-coords[0*nnpe+3])*(cx-coords[0*nnpe+3]) +
	    (cy-coords[1*nnpe+3])*(cy-coords[1*nnpe+3]) +
	    (cz-coords[2*nnpe+3])*(cz-coords[2*nnpe+3]);

	  if (mod2a <= mod3d) /* Edge 3a should exists */
	    {
	      if (mod2b <= mod3c) /* Edge 3b should exists */
		{ /* form tetrahedron (2 b a 3) */
		  vei[ist*nnpe + 0] = ve[2];
		  vei[ist*nnpe + 1] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		  vei[ist*nnpe + 2] = ve[2]*s12 + ve[1]*(1.0 - s12); /* a */
		  vei[ist*nnpe + 3] = ve[3];

		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+2];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+2];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+2];
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = bx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = by;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = bz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = ax;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = ay;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = az;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+3];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+3];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+3];
		  ist++;

		  modac = (ax-cx)*(ax-cx)+(ay-cy)*(ay-cy)+(az-cz)*(az-cz);
		  modbd = (bx-dx)*(bx-dx)+(by-dy)*(by-dy)+(bz-dz)*(bz-dz);
		  if (modac <= modbd)
		    { /* form tetrahedra: (a b c 3) (c d a 3) */
		      vei[ist*nnpe + 0] = ve[2]*s12 + ve[1]*(1.0 - s12); /* a */
		      vei[ist*nnpe + 1] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		      vei[ist*nnpe + 2] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		      vei[ist*nnpe + 3] = ve[3];

		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = ax;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = ay;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = az;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = bx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = by;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = bz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = cx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = cy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = cz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+3];
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+3];
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+3];
		      ist++;

		      vei[ist*nnpe + 0] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		      vei[ist*nnpe + 1] = ve[3]*s13 + ve[1]*(1.0 - s13); /* d */
		      vei[ist*nnpe + 2] = ve[2]*s12 + ve[1]*(1.0 - s12); /* a */
		      vei[ist*nnpe + 3] = ve[3];

		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = cx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = cy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = cz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = dx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = dy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = dz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = ax;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = ay;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = az;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+3];
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+3];
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+3];
		      ist++;
		    }
		  else
		    { /* form tetrahedra: (b c d 3) (d a b 3) */
		      vei[ist*nnpe + 0] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		      vei[ist*nnpe + 1] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		      vei[ist*nnpe + 2] = ve[3]*s13 + ve[1]*(1.0 - s13); /* d */
		      vei[ist*nnpe + 3] = ve[3];

		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = bx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = by;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = bz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = cx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = cy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = cz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = dx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = dy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = dz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+3];
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+3];
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+3];
		      ist++;

		      vei[ist*nnpe + 0] = ve[3]*s13 + ve[1]*(1.0 - s13); /* d */
		      vei[ist*nnpe + 1] = ve[2]*s12 + ve[1]*(1.0 - s12); /* a */
		      vei[ist*nnpe + 2] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		      vei[ist*nnpe + 3] = ve[3];

		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = dx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = dy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = dz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = ax;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = ay;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = az;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = bx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = by;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = bz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+3];
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+3];
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+3];
		      ist++;
		    }
		}
	      else /* Edge 2c should exists */
		{ /* Form tetrahedra: (2 b a c) (2 a 3 c) (3 a d c) */
		  vei[ist*nnpe + 0] = ve[2];
		  vei[ist*nnpe + 1] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		  vei[ist*nnpe + 2] = ve[2]*s12 + ve[1]*(1.0 - s12); /* a */
		  vei[ist*nnpe + 3] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */

		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+2];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+2];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+2];
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = bx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = by;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = bz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = ax;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = ay;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = az;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = cx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = cy;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = cz;
		  ist++;

		  vei[ist*nnpe + 0] = ve[2];
		  vei[ist*nnpe + 1] = ve[2]*s12 + ve[1]*(1.0 - s12); /* a */
		  vei[ist*nnpe + 2] = ve[3];
		  vei[ist*nnpe + 3] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */

		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+2];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+2];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+2];
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = ax;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = ay;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = az;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = coords[0*nnpe+3];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = coords[1*nnpe+3];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = coords[2*nnpe+3];
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = cx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = cy;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = cz;
		  ist++;

		  vei[ist*nnpe + 0] = ve[3];
		  vei[ist*nnpe + 1] = ve[2]*s12 + ve[1]*(1.0 - s12); /* a */
		  vei[ist*nnpe + 2] = ve[3]*s13 + ve[1]*(1.0 - s13); /* d */
		  vei[ist*nnpe + 3] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */

		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+3];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+3];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+3];
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = ax;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = ay;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = az;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = dx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = dy;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = dz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = cx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = cy;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = cz;
		  ist++;
		}
	    }
	  else /* Edge 2d should exists */
	    {
	      if (mod2b <= mod3c) /* Edge 3b should exists */
		{ /* form tetrahedra: (2 b a d) (3 c b d) (b 2 3 d) */
		  vei[ist*nnpe + 0] = ve[2];
		  vei[ist*nnpe + 1] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		  vei[ist*nnpe + 2] = ve[2]*s12 + ve[1]*(1.0 - s12); /* a */
		  vei[ist*nnpe + 3] = ve[3]*s13 + ve[1]*(1.0 - s13); /* d */

		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+2];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+2];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+2];
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = bx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = by;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = bz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = ax;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = ay;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = az;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = dx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = dy;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = dz;
		  ist++;

		  vei[ist*nnpe + 0] = ve[3];
		  vei[ist*nnpe + 1] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		  vei[ist*nnpe + 2] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		  vei[ist*nnpe + 3] = ve[3]*s13 + ve[1]*(1.0 - s13); /* d */

		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+3];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+3];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+3];
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = cx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = cy;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = cz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = bx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = by;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = bz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = dx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = dy;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = dz;
		  ist++;

		  vei[ist*nnpe + 0] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		  vei[ist*nnpe + 1] = ve[2];
		  vei[ist*nnpe + 2] = ve[3];
		  vei[ist*nnpe + 3] = ve[3]*s13 + ve[1]*(1.0 - s13); /* d */

		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = bx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = by;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = bz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = coords[0*nnpe+2];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = coords[1*nnpe+2];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = coords[2*nnpe+2];
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = coords[0*nnpe+3];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = coords[1*nnpe+3];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = coords[2*nnpe+3];
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = dx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = dy;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = dz;
		  ist++;
		}
	      else /* Edge 2c should exists */
		{ /* form tetrahedron (3 d c 2) */
		  vei[ist*nnpe + 0] = ve[3];
		  vei[ist*nnpe + 1] = ve[3]*s13 + ve[1]*(1.0 - s13); /* d */
		  vei[ist*nnpe + 2] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		  vei[ist*nnpe + 3] = ve[2];

		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = coords[0*nnpe+3];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = coords[1*nnpe+3];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = coords[2*nnpe+3];
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = dx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = dy;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = dz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = cx;
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = cy;
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = cz;
		  subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+2];
		  subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+2];
		  subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+2];
		  ist++;
 
		  modac = (ax-cx)*(ax-cx)+(ay-cy)*(ay-cy)+(az-cz)*(az-cz);
		  modbd = (bx-dx)*(bx-dx)+(by-dy)*(by-dy)+(bz-dz)*(bz-dz);
		  if (modac <= modbd)
		    { /* form tetrahedra: (c d a 2) (a b c 2) */
		      vei[ist*nnpe + 0] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		      vei[ist*nnpe + 1] = ve[3]*s13 + ve[1]*(1.0 - s13); /* d */
		      vei[ist*nnpe + 2] = ve[2]*s12 + ve[1]*(1.0 - s12); /* a */
		      vei[ist*nnpe + 3] = ve[2];

		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = cx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = cy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = cz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = dx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = dy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = dz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = ax;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = ay;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = az;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+2];
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+2];
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+2];
		      ist++;

		      vei[ist*nnpe + 0] = ve[2]*s12 + ve[1]*(1.0 - s12); /* a */
		      vei[ist*nnpe + 1] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		      vei[ist*nnpe + 2] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		      vei[ist*nnpe + 3] = ve[2];

		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = ax;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = ay;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = az;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = bx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = by;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = bz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = cx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = cy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = cz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+2];
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+2];
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+2];
		      ist++;
		    }
		  else
		    { /* form tetrahedra: (b c d 2) (d a b 2) */
		      vei[ist*nnpe + 0] = ve[2]*s02 + ve[0]*(1.0 - s02); /* b */
		      vei[ist*nnpe + 1] = ve[3]*s03 + ve[0]*(1.0 - s03); /* c */
		      vei[ist*nnpe + 2] = ve[3]*s13 + ve[1]*(1.0 - s13); /* d */
		      vei[ist*nnpe + 3] = ve[2];

		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 0] = bx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 0] = by;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 0] = bz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 1] = cx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 1] = cy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 1] = cz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 2] = dx;
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 2] = dy;
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 2] = dz;
		      subpoints[ist*(nnpe*(*nsd)) + 0*nnpe + 3] = coords[0*nnpe+2];
		      subpoints[ist*(nnpe*(*nsd)) + 1*nnpe + 3] = coords[1*nnpe+2];
		      subpoints[ist*(nnpe*(*nsd)) + 2*nnpe + 3] = coords[2*nnpe+2];
		      ist++;
		    }
		}
	    }
	  
	  ax = intpoints[0*(*nintp)+1] - intpoints[0*(*nintp)+0];
	  ay = intpoints[1*(*nintp)+1] - intpoints[1*(*nintp)+0];
	  az = intpoints[2*(*nintp)+1] - intpoints[2*(*nintp)+0];
	  bx = intpoints[0*(*nintp)+2] - intpoints[0*(*nintp)+0];
	  by = intpoints[1*(*nintp)+2] - intpoints[1*(*nintp)+0];
	  bz = intpoints[2*(*nintp)+2] - intpoints[2*(*nintp)+0];
	  cx = intpoints[0*(*nintp)+3] - intpoints[0*(*nintp)+0];
	  cy = intpoints[1*(*nintp)+3] - intpoints[1*(*nintp)+0];
	  cz = intpoints[2*(*nintp)+3] - intpoints[2*(*nintp)+0];

	  *sizeint = 0.5 * sqrt((ay*bz-az*by)*(ay*bz-az*by) + 
				(az*bx-ax*bz)*(az*bx-ax*bz) +
				(ax*by-ay*bx)*(ax*by-ay*bx));

	  *sizeint += 0.5 * sqrt((by*cz-bz*cy)*(by*cz-bz*cy) + 
				 (bz*cx-bx*cz)*(bz*cx-bx*cz) +
				 (bx*cy-by*cx)*(bx*cy-by*cx));
	} /* end block "quadrilateral" */

      /* Restore rotation */
      if (rot > -1)
	{
	  double vaux[4];
	  int iaux[4];

	  /* Rotate v */
	  for (k = 0; k < 4; k++)
	    vaux[perm[rot][k]] = v[k];
	  for (k = 0; k < 4; k++)
	    v[k] = vaux[k];
	  /* Rotate id */
	  for (k = 0; k < 4; k++)
	    iaux[perm[rot][k]] = id[k];
	  for (k = 0; k < 4; k++)
	    id[k] = iaux[k];
	  /* Rotate ve */
	  for (k = 0; k < 4; k++)
	    vaux[perm[rot][k]] = ve[k];
	  for (k = 0; k < 4; k++)
	    ve[k] = vaux[k];
	  /* Rotate coordinates */
	  for (j = 0; j < *nsd; j++)
	    {
	      for (k = 0; k < 4; k++)
		vaux[perm[rot][k]] = coords[j*nnpe+k];
	      for (k = 0; k < 4; k++)
		coords[j*nnpe+k] = vaux[k];
	    }
	}
    }

  return fvol;
}

c---------------------------------------------------------------------
c Calculo del volumen de fluido
      rinte(1)=rinte(1)+dj
c---------------------------------------------------------------------
c Calculo energia cinetica (K n)
      rkin = 0.
      do isd=1,nsd
       rkin = rkin+rhoi*0.5*var(isd,1)*var(isd,1)
      end do
      rinte(2)=rinte(2)+rkin*dj
c---------------------------------------------------------------------
c Integral of velocity
       if (nsd.eq.2) then
         ux = var(1,1)
         uy = var(2,1)
         rinte(3) = rinte(3)+rhoi*ux*dj
         rinte(4) = rinte(4)+rhoi*uy*dj
       else if (nsd.eq.3) then
         ux = var(1,1)
         uy = var(2,1)
         uz = var(3,1)
         rinte(3) = rinte(3)+rhoi*ux*dj
         rinte(4) = rinte(4)+rhoi*uy*dj
         rinte(5) = rinte(5)+rhoi*uz*dj
       end if
c---------------------------------------------------------------------
c Integral de la presion
      rinte(6) = rinte(6) + var(1,2)**2*dj
c
      rinte(7) = rinte(7) + fvol*dj
c---------------------------------------------------------------------
c Velocidades al cuadrado (fluctuaciones)
       if (nsd.eq.2) then
         ux = var(1,1)
         uy = var(2,1)
         rinte(8) = rinte(8)+rhoi*ux*ux*dj
         rinte(9) = rinte(9)+rhoi*uy*uy*dj
       else if (nsd.eq.3) then
         ux = var(1,1)
         uy = var(2,1)
         uz = var(3,1)
         rinte(8) = rinte(8)+rhoi*ux*ux*dj
         rinte(9) = rinte(9)+rhoi*uy*uy*dj
         rinte(10) = rinte(10)+rhoi*uz*uz*dj
       end if
c
c Disipación
       do isd = 1, nsd
          do jsd = 1, nsd
             rinte(11) = rinte(11) + dj*
     *            (dvar(isd,jsd,1)+dvar(jsd,isd,1))*dvar(isd,jsd,1)
          end do
       end do
c
c Temperatura media
       rinte(12) = rinte(12) + dj*var(1,8)
c
c Volumen, Temperatura en canales central y perifericos
       rr = sqrt(coopg(1)*coopg(1)+coopg(2)*coopg(2))
       ppii = 3.1415926
       pang = atan2(coopg(2), coopg(1))
       if (rr .lt. 0.003589) then
          rinte(13) = rinte(13) + dj
          rinte(17) = rinte(17) + dj*var(1,8)
       else if (pang .gt. ppii / 6 .and. pang .le. ppii * 5 / 6) then
          rinte(14) = rinte(14) + dj
          rinte(18) = rinte(18) + dj*var(1,8)
       else if (pang .gt. -ppii / 2 .and. pang .le. ppii / 6) then
          rinte(15) = rinte(15) + dj
          rinte(19) = rinte(19) + dj*var(1,8)
       else
          rinte(16) = rinte(16) + dj
          rinte(20) = rinte(20) + dj*var(1,8)
       end if
c
c Volumen, Temperatura en seccion de canales central y perifericos
       rr = sqrt(coopg(1)*coopg(1)+coopg(2)*coopg(2))
       ppii = 3.1415926
       pang = atan2(coopg(2), coopg(1))
       if (coopg(3) .lt. 0.0013481) then
          rinte(21) = rinte(21) + dj
          if (rr .lt. 0.003589) then
             rinte(25) = rinte(25) + dj*var(1,8)
          else if (pang .gt. ppii / 6 .and. pang .le. ppii * 5 / 6) then
             rinte(22) = rinte(22) + rhoi*dj*var(3,1)
             rinte(26) = rinte(26) + dj*var(1,8)
          else if (pang .gt. -ppii / 2 .and. pang .le. ppii / 6) then
             rinte(23) = rinte(23) + rhoi*dj*var(3,1)
             rinte(27) = rinte(27) + dj*var(1,8)
          else
             rinte(24) = rinte(24) + rhoi*dj*var(3,1)
             rinte(28) = rinte(28) + dj*var(1,8)
          end if
       end if

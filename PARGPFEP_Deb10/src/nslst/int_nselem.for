C
C     Integrales en malla de volumen, paso de Navier-Stokes
C
c---------------------------------------------------------------------
c Calculo del volumen del dominio
      rinte(1)=rinte(1)+dj
c---------------------------------------------------------------------
c Calculo energia cinetica (K n)
      rkin = 0.
      do isd=1,nsd
       rkin = rkin+rhoi*0.5*var(isd,1)*var(isd,1)
      end do
      rinte(2)=rinte(2)+rkin*dj
c---------------------------------------------------------------------
c Momento
      ux = var(1,1)
      rinte(3) = rinte(3)+rhoi*ux*dj
       if (nsd .ge. 2) then
         uy = var(2,1)
         rinte(4) = rinte(4)+rhoi*uy*dj
      end if
      if (nsd .ge. 3) then
         uz = var(3,1)
         rinte(5) = rinte(5)+rhoi*uz*dj
       end if
c---------------------------------------------------------------------
c Integral de la presion
      rinte(6) = rinte(6) + var(1,2)**2*dj
c Fluid volume
      rinte(7) = rinte(7) + fvol*dj
c---------------------------------------------------------------------
c Velocidades al cuadrado (fluctuaciones)
      rinte(8) = rinte(8)+rhoi*ux*ux*dj
      if (nsd .ge. 2) then
         rinte(9) = rinte(9)+rhoi*uy*uy*dj
      end if
      if (nsd .ge. 3) then
         rinte(10) = rinte(10)+rhoi*uz*uz*dj
      end if
c
c Disipación
      do isd = 1, nsd
         do jsd = 1, nsd
            rinte(11) = rinte(11) + dj*
     *           (dvar(isd,jsd,1)+dvar(jsd,isd,1))*dvar(isd,jsd,1)
         end do
      end do
c
c Temperatura media
C     rinte(12) = rinte(12) + dj*var(1,8)
C Componente "y" del gradiente de temperatura
C     rinte(13) = rinte(13) + dj*dvar(1,2,8)
C dift div( grad T) = dT/dt + (v.grad) T - ff
c      prandtl = parmat(8)
c      ysdift = coopg(2) / (rmu / rhoi / prandtl)
c      rinte(14) = rinte(14) + dj*
c     &     (var(1,8)-vara(1,8))/delcon * ysdift
c      do isd = 1, nsd
c         rinte(14) = rinte(14) + dj*var(isd,1)*dvar(1,isd,8) * ysdift
c      end do
c      rinte(14) = rinte(14) - dj*parmat(12) * ysdift

c---------------------------------------------------------------------
C Divergencia
c---------------------------------------------------------------------
      div = 0
      do isd = 1, nsd
        div = div + dvar(isd,isd,1)*dj
      end do
C Divergencia total (=div(fluido1)+div(fluido2))
      rinte(12) = rinte(12)+div
C Divergencia total en fluido 1
      if (phi.lt.rlevel) then ! fluido 1 (phi<rlevel) (gas)
            rinte(13) = rinte(13) + div
      else ! fluido 2 (phi>rlevel)
C Divergencia total en fluido 2      
            rinte(14) = rinte(14) + div   
      end if    

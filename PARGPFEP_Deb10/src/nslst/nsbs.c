#include <stdlib.h>
#include "petsc.h"
#ifdef USE_NSBS
#include "src/mat/impls/aij/mpi/mpiaij.h"
#endif
#include "data_structures.h"
/*
void setpetscviewerformat_ ()
{
  PetscViewerSetFormat(PETSC_VIEWER_STDOUT_WORLD, PETSC_VIEWER_ASCII_MATLAB);
}
*/
#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#define nsbsSolve  nsbssolve_
#elif defined (PETSC_HAVE_FORTRAN_PRE_DOT)
#define nsbsSolve  .nsbssolve
#else
#define nsbsSolve  nsbssolve
#endif

struct _nsbsctx{
  Mat        Ag;
  Vec        local;
  double     sgn;
  int        nnodt;
  int        nnodl;
  int        nnode;
  int        ibloque;          /* 0,0=Vxx, 1,1=Vyy, 2,2=Vzz, 3,3=P */
  int        jbloque; 
  int        nbloques;           /* 3=2D, 4=3D */
  VecScatter G2Lscat;
};
typedef struct _nsbsctx *nsbsCtx;

int mimul (Mat, Vec, Vec);
int midiag (Mat, Vec);
int mimuladd (Mat, Vec, Vec, Vec);
int nsbsSolve(Mat *fAbig, Vec *fbbig, Vec *fxbig, systemdata **fSy);

#undef __FUNC__
#define __FUNC__ "nsbsSolve"
int nsbsSolve (Mat *fAbig, Vec *fbbig, Vec *fxbig, systemdata **fSy)
{
  //  systemdata *Sy;
  //  Mat     Abig;
  //  Vec     bbig, xbig;
  //  PetscBool pt1, pt2;

#ifdef USE_NSBS
/* static data (must survive between calls) */
  static nsbsCtx param;
  static int first_time = 1;
  static Vec vlocal, *b, *x, *rhs, deltax;
  static Mat A;
/*  static SLES sles; */
  static KSP ksp;

  int nbl, i, j, its, ierr;
  int bjnits, bjits, vmax_it, pmax_it;
  int rank;
  static int trueMats;
  static Mat *VMat, PMat;

  PetscReal nordx;
  PetscScalar vrtol, prtol;
  PetscScalar *pbbig, *pxbig, *pb, *px;
/*  PetscScalar media; */
  IS     IS_aux1, IS_aux2;
/*  KSP ksp;
  PC pc; */
  PetscScalar omega;
  PetscBool pt_flg;
  MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
/* Arguments from fortran */
  Sy   = *fSy;
  Abig = *fAbig;
  bbig = *fbbig;
  xbig = *fxbig;

  if (first_time)
    {
      first_time = 0;
      PetscOptionsHasName (NULL, "nsbs_", "-trueMats", &pt_flg);
      trueMats = (pt_flg == PETSC_TRUE);

      param = (nsbsCtx) malloc (sizeof (struct _nsbsctx));
      param->nnodl    = Sy->Pa->nodloc;
      param->nnodt    = Sy->Pa->nodtot;
      param->nnode    = Sy->Pa->nodext;
      param->Ag       = Abig;
      param->sgn      = -1.0;
      param->nbloques = nbl = Sy->Pa->nsd + 1;

      if (trueMats)
	{
	  VMat = (Mat *) malloc ((nbl-1) * sizeof(Mat));
	  for (i = 0; i < nbl-1; i++)
	    {
/*	      ierr = ISCreateStride (MPI_COMM_SELF, param->nnodl,
				     i, nbl, &IS_aux1);   CHKERRA(ierr);*/
	      ierr = ISCreateStride (PETSC_COMM_WORLD, param->nnodl,
				     (Sy->Pa->first-1)*nbl+i, nbl, &IS_aux1);   CHKERRABORT(PETSC_COMM_WORLD,ierr);
/*	      ierr = ISCreateStride (MPI_COMM_SELF, param->nnodt,
				     i, nbl, &IS_aux2);   CHKERRA(ierr);*/
	      ierr = ISCreateStride (PETSC_COMM_WORLD, param->nnodt,
				     i, nbl, &IS_aux2);   CHKERRABORT(PETSC_COMM_WORLD,ierr);
	      ierr = MatGetSubMatrix (Abig, IS_aux1, IS_aux2, param->nnodl,
				      MAT_INITIAL_MATRIX, &VMat[i]);
	      ierr = ISDestroy(IS_aux1);                     CHKERRABORT(PETSC_COMM_WORLD,ierr);
	      ierr = ISDestroy(IS_aux2);                     CHKERRABORT(PETSC_COMM_WORLD,ierr);
    	    }
/*	  ierr = ISCreateStride (MPI_COMM_SELF, param->nnodl,
				 i, nbl, &IS_aux1);   CHKERRA(ierr);
	  ierr = ISCreateStride (MPI_COMM_SELF, param->nnodt,
				 i, nbl, &IS_aux2);   CHKERRA(ierr);*/
	  ierr = ISCreateStride (PETSC_COMM_WORLD, param->nnodl,
				     (Sy->Pa->first-1)*nbl+i, nbl, &IS_aux1);   CHKERRABORT(PETSC_COMM_WORLD,ierr);
	  ierr = ISCreateStride (PETSC_COMM_WORLD, param->nnodt,
				     i, nbl, &IS_aux2);   CHKERRABORT(PETSC_COMM_WORLD,ierr);
	  ierr = MatGetSubMatrix (Abig, IS_aux1, IS_aux2, param->nnodl,
				  MAT_INITIAL_MATRIX, &PMat);
	  ierr = ISDestroy(IS_aux1);                     CHKERRABORT(PETSC_COMM_WORLD,ierr);
	  ierr = ISDestroy(IS_aux2);                     CHKERRABORT(PETSC_COMM_WORLD,ierr);
	}
      
      PetscTypeCompare((PetscObject)Abig, MATSEQAIJ, &pt1);
      PetscTypeCompare((PetscObject)Abig, MATMPIAIJ, &pt2);

      if (pt1 != PETSC_TRUE && pt2 != PETSC_TRUE)
	error (EXIT, "Matrix type not supported in nsbsSolve");

      ierr = MatCreateShell (Sy->Pa->Comm, param->nnodl, param->nnodl,
			     param->nnodt, param->nnodt, (void *)param, &A);
      CHKERRABORT(PETSC_COMM_WORLD,ierr);

      ierr = MatShellSetOperation (A, MATOP_MULT, (void *)mimul);
      CHKERRABORT(PETSC_COMM_WORLD,ierr);
      ierr = MatShellSetOperation (A, MATOP_GET_DIAGONAL, (void *)midiag);
      CHKERRABORT(PETSC_COMM_WORLD,ierr);
      ierr = MatShellSetOperation (A, MATOP_MULT_ADD, (void *)mimuladd);
      CHKERRABORT(PETSC_COMM_WORLD,ierr);
	
      ierr = VecCreateSeq (PETSC_COMM_SELF, param->nnode, &vlocal);
      CHKERRABORT(PETSC_COMM_WORLD,ierr);
      param->local = vlocal;

      ierr = VecCreateMPI (Sy->Pa->Comm, param->nnodl, param->nnodt,
			   &deltax); CHKERRABORT(PETSC_COMM_WORLD,ierr);

      rhs = (Vec *) malloc (nbl * sizeof(Vec));
      b   = (Vec *) malloc (nbl * sizeof(Vec));
      x   = (Vec *) malloc (nbl * sizeof(Vec));

      for (i = 0; i < nbl; i++)
	{
	  ierr = VecDuplicate (deltax, &rhs[i]); CHKERRABORT(PETSC_COMM_WORLD,ierr);
	  ierr = VecDuplicate (deltax, &b  [i]); CHKERRABORT(PETSC_COMM_WORLD,ierr);
	  ierr = VecDuplicate (deltax, &x  [i]); CHKERRABORT(PETSC_COMM_WORLD,ierr);
	}

      /*
       * Scatter creation
       */
      ierr = ISCreateGeneral (MPI_COMM_SELF, Sy->Pa->nodext,
			      Sy->Pa->Glob, &IS_aux1);       CHKERRABORT(PETSC_COMM_WORLD,ierr);
      ierr = ISCreateStride (MPI_COMM_SELF, Sy->Pa->nodext,
			     0, 1, &IS_aux2);                CHKERRABORT(PETSC_COMM_WORLD,ierr);

      ierr = VecScatterCreate (b[0], IS_aux1, vlocal, IS_aux2,
			       &(param->G2Lscat));          CHKERRABORT(PETSC_COMM_WORLD,ierr);
      ierr = ISDestroy(IS_aux1);                            CHKERRABORT(PETSC_COMM_WORLD,ierr);
      ierr = ISDestroy(IS_aux2);                            CHKERRABORT(PETSC_COMM_WORLD,ierr);
    }
  else
    {
      nbl = param->nbloques;
      if (trueMats)
	{
	  for (i = 0; i < nbl-1; i++)
	    {
/*	      ierr = ISCreateStride (MPI_COMM_SELF, param->nnodl,
				     i, nbl, &IS_aux1);   CHKERRA(ierr);
	      ierr = ISCreateStride (MPI_COMM_SELF, param->nnodl,
				     i, nbl, &IS_aux2);   CHKERRA(ierr);
	      ierr = MatGetSubMatrix (Abig, IS_aux1, IS_aux2, param->nnodl,
				      MAT_REUSE_MATRIX, &VMat[i]);*/
	      ierr = ISCreateStride (PETSC_COMM_WORLD, param->nnodl,
				     (Sy->Pa->first-1)*nbl+i, nbl, &IS_aux1);   CHKERRABORT(PETSC_COMM_WORLD,ierr);
	      ierr = ISCreateStride (PETSC_COMM_WORLD, param->nnodt,
				     i, nbl, &IS_aux2);   CHKERRABORT(PETSC_COMM_WORLD,ierr);
	      ierr = MatGetSubMatrix (Abig, IS_aux1, IS_aux2, param->nnodl,
				      MAT_REUSE_MATRIX, &VMat[i]);
	      ierr = ISDestroy(IS_aux1);                     CHKERRABORT(PETSC_COMM_WORLD,ierr);
	      ierr = ISDestroy(IS_aux2);                     CHKERRABORT(PETSC_COMM_WORLD,ierr);
    	    }
/*	  ierr = ISCreateStride (MPI_COMM_SELF, param->nnodl,
				 i, nbl, &IS_aux1);   CHKERRA(ierr);
	  ierr = ISCreateStride (MPI_COMM_SELF, param->nnodl,
				 i, nbl, &IS_aux2);   CHKERRA(ierr);
	  ierr = MatGetSubMatrix (Abig, IS_aux1, IS_aux2, param->nnodl,
				  MAT_REUSE_MATRIX, &PMat);*/
	  ierr = ISCreateStride (PETSC_COMM_WORLD, param->nnodl,
				     (Sy->Pa->first-1)*nbl+i, nbl, &IS_aux1);   CHKERRABORT(PETSC_COMM_WORLD,ierr);
	  ierr = ISCreateStride (PETSC_COMM_WORLD, param->nnodt,
				     i, nbl, &IS_aux2);   CHKERRABORT(PETSC_COMM_WORLD,ierr);
	  ierr = MatGetSubMatrix (Abig, IS_aux1, IS_aux2, param->nnodl,
				      MAT_REUSE_MATRIX, &PMat);
	  ierr = ISDestroy(IS_aux1);                     CHKERRABORT(PETSC_COMM_WORLD,ierr);
	  ierr = ISDestroy(IS_aux2);                     CHKERRABORT(PETSC_COMM_WORLD,ierr);
	}
    }

/*
 * Build blocked RHSides and solutions
 */
  VecGetArray (bbig, &pbbig);
/*  VecGetArray (xbig, &pxbig);*/
  for (i = 0; i < nbl; i++)
    {
      VecGetArray (b[i], &pb);
      VecGetArray (x[i], &px);
      for (j = 0; j < param->nnodl; j++)
	{
	  pb[j] = pbbig[j*nbl + i];
/*	  px[j] = pxbig[j*nbl + i]; */
          px[j] = 0;
	}
      VecRestoreArray (b[i], &pb);
      VecRestoreArray (x[i], &px);
    }
  VecRestoreArray (bbig, &pbbig);
/*  VecRestoreArray (xbig, &pxbig); */

/*
 * Block Jacobi iterations
 */
  PetscOptionsGetInt ("nsbs_", "-bjnits", &bjnits, &pt_flg);
  if (pt_flg == PETSC_FALSE)
    bjnits = 1;
  PetscOptionsGetInt ("nsbs_", "-vmax_it", &vmax_it, &pt_flg);
  if (pt_flg == PETSC_FALSE)
    vmax_it = 1000;
  PetscOptionsGetScalar ("nsbs_", "-vrtol", &vrtol, &pt_flg);
  if (pt_flg == PETSC_FALSE)
    vrtol = 0.0001;
  PetscOptionsGetInt ("nsbs_", "-pmax_it", &pmax_it, &pt_flg);
  if (pt_flg == PETSC_FALSE)
    pmax_it = 1000;
  PetscOptionsGetScalar ("nsbs_", "-prtol", &prtol, &pt_flg);
  if (pt_flg == PETSC_FALSE)
    prtol = 0.0001;

  for (bjits = 0; bjits < bjnits; bjits++)
    {
 /*
 * Arma RHSides de las Vii y resuelve
 *
      ierr = SLESCreate (PETSC_COMM_WORLD, &sles); CHKERRABORT(PETSC_COMM_WORLD,ierr);
 */
      ierr = KSPCreate (PETSC_COMM_WORLD, &ksp); CHKERRABORT(PETSC_COMM_WORLD,ierr);
/*      ierr = SLESSetFromOptions(sles); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
      ierr = KSPSetFromOptions(ksp); CHKERRABORT(PETSC_COMM_WORLD,ierr);
/*      ierr = SLESGetKSP(sles, &ksp);
      ierr = SLESGetPC(sles, &pc); */
      ierr = KSPSetTolerances(ksp, vrtol, 1.0e-10, 1.0e+10, vmax_it);

      for (i = 0; i < nbl-1; i++)
	{
	  VecCopy (b[i], rhs[i]);
          if (bjits > 0)
	  {
	  for (j = 0; j < nbl; j++)
	    {
	      /* if (i == j) continue; */
	      param->ibloque = i;
	      param->jbloque = j;
	      param->sgn = -1.0;
	      mimuladd (A, x[j], rhs[i], rhs[i]);
	    }
          }
	}

      for (i = 0; i < nbl-1; i++)
	{
	  if (trueMats)
	    {
	      /* ierr = SLESSetOperators (sles, VMat[i], VMat[i],
		 DIFFERENT_NONZERO_PATTERN);
		 CHKERRABORT(PETSC_COMM_WORLD,ierr); */
	      ierr = KSPSetOperators
		(ksp, VMat[i], VMat[i]);CHKERRABORT(PETSC_COMM_WORLD,ierr);
	    }
	  else
	    {
	      param->ibloque = i;
	      param->jbloque = i;
	      /* ierr = SLESSetOperators (sles, A, A,
		 DIFFERENT_NONZERO_PATTERN);
		 CHKERRABORT(PETSC_COMM_WORLD,ierr);*/
	      ierr = KSPSetOperators
		(ksp, A, A);CHKERRABORT(PETSC_COMM_WORLD,ierr);
	    }
	  /*	  ierr = SLESSolve (sles, rhs[i], deltax, &its);
		  CHKERRABORT(PETSC_COMM_WORLD,ierr); */
	  /* ierr = KSPSetRhs(ksp,rhs[i]); */
	  /* ierr = KSPSetSolution(ksp,deltax); */
	  ierr = KSPSolve(ksp, rhs[i], deltax);
	  ierr = KSPGetIterationNumber(ksp, &its);

	  VecNormBegin (deltax, NORM_MAX, &nordx);
	  omega = 1.0;
	  VecAXPY (x[i], omega, deltax);
	  VecNormEnd   (deltax, NORM_MAX, &nordx);

	  if(rank==0)printf("\n---------> Iteraciones para el bloque %d : %d\n"
		 "----------> NormMax(Deltax): %g\n", i, its, nordx);
	}

      /* Arma RHS de PP y resuelve*/
      i = param->ibloque = nbl-1;

      /* le resto a b su valor medio
       * y con esto filtro la componente constante.
	ierr = VecSum(b[i], &media);
	CHKERRA(ierr);
        media = media / param->nnodl;
	ierr=VecGetArray(b[i],&pb);CHKERRA(ierr);
	for (j = 0; j < param->nnodl; j++);
		pb[j] = pb[j] - media;
	ierr=VecRestoreArray(b[i],&pb);CHKERRA(ierr);       
       * fin de la truchada.
       */

      VecCopy (b[i], rhs[i]);
      for (j = 0; j < nbl -1; j++)
	{
	  param->jbloque = j;
	  param->sgn = -1.0;
	  mimuladd (A, x[j], rhs[i], rhs[i]);
	}

      param->jbloque = i;
      /* si resuelve P habria que cambiar de GMRES a GC */
      if (trueMats)
	{
/*	  ierr = SLESSetOperators (sles, PMat, PMat, DIFFERENT_NONZERO_PATTERN);*/
	  ierr = KSPSetOperators (ksp, PMat, PMat);
	  CHKERRABORT(PETSC_COMM_WORLD,ierr);
	}
      else
	{
/*	  ierr = SLESSetOperators (sles, A, A, DIFFERENT_NONZERO_PATTERN); */
	  ierr = KSPSetOperators (ksp, A, A);
	  CHKERRABORT(PETSC_COMM_WORLD,ierr);
	}

/*      ierr = SLESSetFromOptions(sles); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
      ierr = KSPSetFromOptions(ksp); CHKERRABORT(PETSC_COMM_WORLD,ierr);
/*      ierr = SLESGetKSP(sles, &ksp);
      ierr = SLESGetPC(sles, &pc);

      ierr = KSPSetType(ksp,KSPCG);
      ierr = PCSetType(pc,PCBJACOBI);
*/
/**/
      ierr = KSPSetTolerances(ksp, prtol, 1.0e-10, 1.0e+10, pmax_it);
/* ierr = SLESSolve (sles, rhs[i], deltax, &its);
   CHKERRABORT(PETSC_COMM_WORLD,ierr); */
      /* ierr = KSPSetRhs (ksp, rhs[i]); */
      /* ierr = KSPSetSolution (ksp, deltax); */
      ierr = KSPSolve(ksp, rhs[i], deltax);
      ierr = KSPGetIterationNumber(ksp, &its);

      VecNormBegin (deltax, NORM_MAX, &nordx);
      omega = 1.0;
      VecAXPY (x[i], omega, deltax);
      VecNormEnd   (deltax, NORM_MAX, &nordx);

      if (rank==0)
	printf("\n---------> Iteraciones para el bloque %d, : %d \n"
	       "----------> NormMax(Deltax): %g\n", i, its, nordx);

/*      ierr = SLESDestroy(sles); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
      ierr = KSPDestroy(ksp); CHKERRABORT(PETSC_COMM_WORLD,ierr);

      if (rank==0)printf("\n\n %d th / %d  Jacobi iteration finished\n", bjits, bjnits);
    }
/*
 * Build-back blocked RHSides and solutions
 */
/*  VecGetArray (bbig, &pbbig);*/
  VecGetArray (xbig, &pxbig);
  for (i = 0; i < nbl; i++)
    {
/*      VecGetArray (b[i], &pb); */
      VecGetArray (x[i], &px);
      for (j = 0; j < param->nnodl; j++)
	{
/*	  pbbig[j*nbl + i] = pb[j];*/
	  pxbig[j*nbl + i] = px[j];
	}
/*      VecRestoreArray (b[i], &pb);*/
      VecRestoreArray (x[i], &px);
    }
/*  VecRestoreArray (bbig, &pbbig);*/
  VecRestoreArray (xbig, &pxbig);

#endif
  return 0;
}

#ifdef USE_NSBS
/*---------------------------------------------------------------------------*/
int mimuladd (Mat A, Vec u, Vec b, Vec c)  /* c = b + A u */
{
  PetscScalar       *valc, *valu, *valb, *valmat, sgn;
  int          i, j, nb, nnodt, nnodl, ierr, *ia, *ja, jstart, jend,
               ib, jb, indA;
  int          parallel;
  PetscBool   pt_par;
  nsbsCtx      par;
  Vec          loc;
  VecScatter   scatter;		
  Mat_MPIAIJ   *a;
  Mat_SeqAIJ   *as;
  Mat Ao, diag;

  ierr = MatShellGetContext (A, (void *)&par); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  nb = par->nbloques;
  nnodt = par->nnodt;
  nnodl = par->nnodl;
  Ao = par->Ag;
  PetscTypeCompare((PetscObject)Ao, MATMPIAIJ, &pt_par);
  parallel = (pt_par == PETSC_TRUE);

  scatter = par->G2Lscat;

  ib = par->ibloque;
  jb = par->jbloque;
  loc = par->local;
  sgn = par->sgn;

  if (parallel)
    {
      a = (Mat_MPIAIJ *)Ao->data;
      diag = a->A;
      as = (Mat_SeqAIJ *)diag->data;
    }
  else
    as = (Mat_SeqAIJ *)Ao->data;

  valmat = as->a;
  ia = as->i;
  ja = as->j;

  if (parallel)
    ierr = VecScatterBegin (u, loc, INSERT_VALUES, SCATTER_FORWARD,
			    scatter); CHKERRABORT(PETSC_COMM_WORLD,ierr);

  ierr = VecGetArray (u, &valu); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGetArray (c, &valc); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  if (b != c)
    { ierr = VecGetArray (b, &valb); CHKERRABORT(PETSC_COMM_WORLD,ierr); }
  else
    valb = valc;

  /* i, ja[j], valmat[j]  - fila,columna,elemento */

/*
 *  Bloque "diagonal"
 */
  indA = ib;
  for (i = 0; i < nnodl; i++)
    {
      jstart = ia[indA];
      jend   = ia[indA + 1];
      valc[i] = valb[i];
      for (j = jstart; j < jend; j++)
	{                         
	  if (ja[j] % nb != jb) continue;
	  valc[i] += sgn * valmat[j] * valu[ja[j] / nb];
	}
      indA += nb;
    }
  ierr = VecRestoreArray (u, &valu); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  if (b != c)
    ierr = VecRestoreArray (b, &valu); CHKERRABORT(PETSC_COMM_WORLD,ierr);

  if (parallel)
    {
      ierr = VecScatterEnd (u, loc, INSERT_VALUES, SCATTER_FORWARD,
			    scatter); CHKERRABORT(PETSC_COMM_WORLD,ierr);

      /*
       *  Bloque "extra diagonal"
       */
      diag = a->B;
      as = (Mat_SeqAIJ *)diag->data;
      valmat = as->a;
      ia = as->i;
      ja = as->j;
      ierr = VecGetArray (loc, &valu); CHKERRABORT(PETSC_COMM_WORLD,ierr);

      indA = ib;
      for (i = 0; i < nnodl; i++)
	{
	  jstart = ia[indA];
	  jend   = ia[indA+1];
	  for (j = jstart; j < jend; j++)
	    {                         
	      if (ja[j]%nb != jb) continue;
	      valc[i] += sgn * valmat[j] * valu[ja[j] / nb];
	    }
	  indA += nb;
	}
      ierr = VecRestoreArray (loc, &valu); CHKERRABORT(PETSC_COMM_WORLD,ierr);
    }
  ierr = VecRestoreArray (c, &valc); CHKERRABORT(PETSC_COMM_WORLD,ierr);
return 0;
}

/*---------------------------------------------------------------------------*/
int mimul (Mat A, Vec u, Vec b)       /* b = A u */
{
  PetscScalar       *valu, *valb, *valmat;
  int          i, j, nb, nnodt, nnodl, ierr, *ia, *ja,
    jstart, jend, ib, jb, indA;
  int parallel;
  PetscBool pt_par;
  nsbsCtx      par;
  Vec          loc;
  VecScatter   scatter;
  Mat_MPIAIJ   *a;
  Mat_SeqAIJ   *as;
  Mat          Ao, diag, ediag;

  ierr = MatShellGetContext (A, (void *)&par); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  nb = par->nbloques;
  nnodt = par->nnodt;
  nnodl = par->nnodl;
  Ao = par->Ag;

  PetscTypeCompare((PetscObject)Ao, MATMPIAIJ, &pt_par);
  parallel = (pt_par == PETSC_TRUE);

  scatter = par->G2Lscat;
  ib = par->ibloque;
  jb = par->jbloque;
  loc = par->local;

  if (parallel)
    {
      a = (Mat_MPIAIJ *) Ao->data;
      diag = a->A;
      as = (Mat_SeqAIJ *)diag->data;
    }
  else
    as = (Mat_SeqAIJ *) Ao->data;

  valmat = as->a;
  ia = as->i;
  ja = as->j;

  if (parallel)
    ierr = VecScatterBegin (u, loc, INSERT_VALUES, SCATTER_FORWARD,
			    scatter);                       CHKERRABORT(PETSC_COMM_WORLD,ierr);

  ierr = VecGetArray (u,&valu); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGetArray (b,&valb); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  /* i, ja[j], valmat[j]  - fila,columna,elemento */

/*
 *  Bloque "diagonal"
 */
  indA = ib;
  for (i = 0; i < nnodl; i++)
    {
      valb[i] = 0.0;
      jstart = ia[indA];
      jend = ia[indA + 1];
      for (j = jstart; j < jend; j++)
	{                         
	  if (ja[j] % nb != jb) continue;
	  valb[i] += valmat[j] * valu[ja[j] / nb];
	}
      indA += nb;
    }

  ierr = VecRestoreArray (u, &valu); CHKERRABORT(PETSC_COMM_WORLD,ierr);

  if (parallel)
    {
      ierr = VecScatterEnd (u, loc, INSERT_VALUES, SCATTER_FORWARD,
			    scatter);                          CHKERRABORT(PETSC_COMM_WORLD,ierr);
      ierr = VecGetArray (loc, &valu); CHKERRABORT(PETSC_COMM_WORLD,ierr);

      ediag = a->B;
      as = (Mat_SeqAIJ *) ediag->data;
      valmat = as->a;
      ia = as->i;
      ja = as->j;

      indA = ib;
      for (i=0; i < nnodl; i++)
	{
	  jstart = ia[indA];
	  jend   = ia[indA+1];
	  for (j = jstart; j < jend; j++)
	    { 
	      if (ja[j] % nb != jb) continue;
	      valb[i] += valmat[j] * valu[ja[j] / nb];
	    }
	  indA += nb;
	}
 
      ierr = VecRestoreArray (loc, &valu); CHKERRABORT(PETSC_COMM_WORLD,ierr);
    }

  ierr = VecRestoreArray (b, &valb); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  return 0;
}

/*-------------------------------------------------------------------*/
int midiag (Mat A, Vec b)
{
  PetscScalar     *valmat, *valb;
  int        i, j, nb, ierr, *ia, *ja, jstart, jend, ib, indA;
  int parallel;
  PetscBool pt_par;
  nsbsCtx    par;
  Mat_MPIAIJ *a;
  Mat_SeqAIJ *as;
  Mat        Ao, diag;

  ierr = MatShellGetContext (A, (void *)&par); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  nb = par->nbloques;
  Ao = par->Ag;

  PetscTypeCompare((PetscObject)Ao, MATMPIAIJ, &pt_par);
  parallel = (pt_par == PETSC_TRUE);

  ib=par->ibloque;
  if (parallel)
    {
      a = (Mat_MPIAIJ *)Ao->data;
      diag = a->A;
      as = (Mat_SeqAIJ *)diag->data;
    }
  else
    as = (Mat_SeqAIJ *) Ao->data;

  valmat = as->a;
  ia = as->i;
  ja = as->j;

  ierr = VecGetArray (b, &valb); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  indA = ib;
  for (i = 0; i < par->nnodl; i++)
    {
/*      valb[i] = 0; */
      jstart = ia[indA];
      jend = ia[indA + 1];
      for (j = jstart; j < jend; j++)                         
	if (ja[j] == indA)
	  {
	    valb[i] = valmat[j];
	    break;
	  }
      indA += nb;
    }
  ierr = VecRestoreArray (b, &valb); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  return 0;
}
#endif
/*-------------------------------------------------------------------*/
void leeopciondesdec_ (int *insbs)
{
  PetscBool pt_flg;
  PetscOptionsHasName (NULL, "nsbs_", "-on", &pt_flg);
  *insbs = (pt_flg == PETSC_TRUE);
}

c -----------------------------------------------------------------------------
      subroutine nssurf
     * (v, va, cogeo, nvar, icomp, npe, nsd, ae, rhse, parcon, delcon, 
     *  parnum, parmat, pargen, ngau, kint, itypegeo, iel,
     *  nod, maxnpe, maxgau, wgp, isim, p, dpl, dp, dimrhse, Aflag, igr,
     *  rinte, nintgr,iswnob,base)
c -----------------------------------------------------------------------------
c input:
c v: vector with the data actualized to the last fractional step to 
c     construct the ae and rhse.
c va: vector with the data of the last continuation step to construct 
c     the ae and rhse.
c cogeo: nodes coordinates
c nvar: number of fields
c icomp(ivar): number of components of field "ivar".
c npe(mesh): number of nodes per element in mesh
c nsd: number of space dimensions.
c ae: elemental matrix
c rhse: elemental right hand side 
c parcon: continuation parameter.
c delcon: step of the continuation parameter
c parnum: vector of numerical parameters.
c parmat: vector of material parameters.
c ngau: number of gauss points
c kint(ivar): interpolation type of ivar. 
c itypegeo: interpolation number for geometry
c iel: number of the element in the local processor
c nod(ivar): number of nodes per element for each ivar
c maxnpe: maximum of npe
c maxgau: constant utilized for dimensioning some arrays
c wgp: weights of the gauss points
c isim: simmetry indicator.  1 if axisymmetric, 0 otherwise.
c p: interpolation functions values at gauss points
c dpl: derivatives of p in the reference element
c dimrhse: dimension of rhse
c Aflag: flag that indicates with 0 that ae has to be computed, and
c        with 1 that there is no need of recomputing that
c igr: group number of the element
c rinte: elemental contribution to each integral
c nintgr: number of integrals that will be done here
#define MAT_A_VARIABLE    0
#define MAT_A_INVARIABLE  1
#undef  __SFUNC__
#define __SFUNC__ "nssurf  "
c
      implicit real*8 (a-h,o-z)
C---------------------------------------------------------------------
C CHECK THAT THE FOLLOWING DATA AGREE WIH YOUR PROBLEM
C---------------------------------------------------------------------
      parameter(maxcomp_=10, maxvar_=18, maxtype_=2)
C---------------------------------------------------------------------
c
      real*8      v, va, cogeo, ae, rhse, parcon, delcon,parnum, parmat
      real*8      wgp, p, dpl, rinte, pargen
      real*8      normal(3)
      integer*4   nvar, icomp, npe, nsd, ngau, kint, itypegeo, iel, nod,
     *            maxnpe, maxgau, isim, dimrhse, Aflag, nintgr
c
      dimension  listv(maxvar_), listx(3)
C     *           , listva(maxvar_)
c
      dimension var(maxcomp_,maxvar_)
      dimension vara(maxcomp_,maxvar_)
      dimension coopg (3), ae(dimrhse, dimrhse), cogeo(maxnpe,nsd)
      dimension icomp(*), npe(*), rhse(*), va(maxnpe, nvar, maxcomp_)
      dimension parmat(*), parnum(*), kint(*), nod(*), pargen(*)
      dimension v(maxnpe, nvar, maxcomp_)
      dimension wgp(*), p(maxnpe, maxgau, *), dpl(maxnpe, maxgau, 3, *)
      dimension rinte(*)
      dimension base(maxnpe,nsd,nsd)
c
C----------------------------------------------------------------------      
C INSERT HERE ANY DEFINITION OF ADDITIONAL VARIABLES OR ARRAYS
c
C----------------------------------------------------------------------      
c      dimension vecaux(10)
C----------------------------------------------------------------------      
C verify THAT THE FOLLOWING DATA SENTENCES AGREE WITH YOUR PROBLEM
c if a variable is used in the boundary condition, it must be activated
      data listv /1,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0/
      data listx /1,2,3/
c
      nrhse = nsd + 1
C----------------------------------------------------------------------      
      nodgeo = npe(1)
      nvar0=nvar
      if (nvar0.gt.maxvar_) nvar0=maxvar_
c make zero the ae and rhse
      if(Aflag.eq. MAT_A_VARIABLE) then
         do i=1,dimrhse
            do j=1,dimrhse
               ae(i,j) = 0.d0
            enddo
         enddo
      endif
      do i=1,dimrhse
         rhse(i) = 0.d0
      enddo
c make zero rinte
      do i=1,nintgr
         rinte(i)=0.0
      enddo
C----------------------------------------------------------------------
C INSERT HERE ANY CALCULUS BEFORE THE LOOP OVER THE GAUSS POINTS
C----------------------------------------------------------------------
      rho = parmat(1)
      rhog = parmat(3)
      rmul = parmat(2)
      rnul = rmul/rho 
      grav = -parmat(4+nsd)
      iswall=0
      do iii=1,40
         if (igr.eq.pargen(10+iii).and.pargen(2).eq.1) iswall=1
      end do
C----------------------------------------------------------------------
C loop over gaussian points
      do 600 ipgau = 1,ngau
c
      call isosur (ipgau,nsd,nodgeo,itypegeo,iel,
     *             dpl,cogeo,nod,maxnpe,maxgau,normal,dets)

      dj = dets*wgp(ipgau)
      if (isim.eq.1.or.isim.eq.2) then
         r = 0.
         do i = 1,nodgeo
            r = r + p(i,ipgau,itypegeo)*cogeo(i,isim)
         end do
c     IF (R.EQ.0) WRITE(6,*) 'R=0  IEL = ',IEL,' IPGAU = ',IPGAU
         djr = dj
         dj = dj*r
      end if
c calculate switched on variables in listv() at gauss points.
      do iv = 1, nvar0
         ivar = listv(iv)
         if (ivar.ne.0) then
            ivar = iv
            itype = kint(ivar)
            do k = 1, icomp(ivar)
               var(k,ivar) = 0.0
               vara(k,ivar) = 0.0
               do jl = 1, nod(ivar)
                  var(k,ivar) = var(k,ivar) + v(jl,ivar,k)*
     *                 p(jl,ipgau,itype)
                  vara(k,ivar) = vara(k,ivar) + va(jl,ivar,k)*
     *                 p(jl,ipgau,itype)
               end do
            end do
         end if
      end do
c calculate switched on coordinates in listx() at gauss points.
      do iv = 1, nsd
         icoo = listx(iv)
         if (icoo.ne.0) then
            icoo = iv
            coopg(icoo) = 0
            do i = 1,nodgeo
               coopg(icoo) = coopg(icoo) + 
     *              p(i,ipgau,itypegeo)*cogeo(i,icoo)
            end do
         end if
      end do
c----------------------------------------------------------------------
c INSERT HERE THE LINES TO CALCULATE ELEMENTARY MATRIX AND R.H.S.
c
c level set modification
c if the element has one node in the air, the wall law is NOT applied
c verify that this is what you want (in true two-phase flow this could be wrong)
      rlevel = parnum(12)
      kdonth = 0
      if (rlevel.gt.0.0.and.rlevel.lt.100.) then
         clsmin = 1000.
         do iii=1,nod(1)
            if (v(iii,4,1).lt.clsmin) clsmin=v(iii,4,1)
         end do
         if (clsmin.lt.rlevel) kdonth = 1
      end if
c
      yodelta = parmat(20)
c WALL LAW
      if (iswall.eq.1.and.kdonth.eq.0) then
         velmod = 0.
         do isd=1,nsd
            velmod = velmod + var(isd,1)**2
         end do
         velmod = sqrt(velmod)
         if (velmod.gt.0.0000001) then
            xnul = rnul
            g    = 0.d0
c            if (igr.eq.6) then
c               xnul = -rnul
c               g    = parmat(14)
c            end if
            call derivut (xnul, velmod, g,dgdut, yodelta)
            do in=1,nod(1)
               pin = p(in,ipgau,kint(1))
               do ico=1,icomp(1)
                  ik = (in-1)*nrhse+ico
                  if (iswnob.eq.0) then
                     rhse(ik)=rhse(ik)
     *                    -rho*g*var(ico,1)*pin*dj/velmod
                  else
                     do isd=1,nsd
                        rhse(ik)=rhse(ik)
     *                       -rho*g*var(isd,1)*pin*dj/velmod
     *                       *base(in,ico,isd)
                     end do
                  end if
                  
                  if (Aflag.eq.MAT_A_VARIABLE) then
                     if (iswnob.eq.0) then
                        do jn=1,nod(1)
                           pjn = p(jn,ipgau,kint(1))
                           jk = (jn-1)*nrhse+ico
                           ae(ik,jk)=ae(ik,jk)+rho*g*pjn*pin*dj/velmod
                           do jco=1,icomp(1)
                              jk = (jn-1)*nrhse+jco
                              ae(ik,jk)=ae(ik,jk)
     *                             +rho*(dgdut-g/velmod)
     *                             *var(ico,1)*var(jco,1)
     *                             *pjn*pin*dj/velmod/velmod
                           end do
                        end do
                     else
                        upin = 0.
                        do isd=1,nsd
                           upin = upin+var(isd,1)*pin*base(in,ico,isd)
                        end do
                        do jn=1,nod(1)
                           pjn = p(jn,ipgau,kint(1))
                           do jco=1,icomp(1)
                              jk = (jn-1)*nrhse+jco
                              upjn = 0.
                              bjdotbi = 0.
                              do isd=1,nsd
                                 upjn= upjn+
     *                                var(isd,1)*pjn*base(jn,jco,isd)
                                 bjdotbi = bjdotbi
     *                                +base(jn,jco,isd)*base(in,ico,isd)
                              end do
                              ae(ik,jk) = ae(ik,jk) 
     *                             +rho*(dgdut-g/velmod)*upin*upjn*dj
     *                             /velmod/velmod
     *                             +rho*g*pjn*pin*dj/velmod*bjdotbi
                           end do
                        end do    
                     end if
                  end if
               end do
            end do
         end if
      end if
C
C     Non-zero pressure:
C
      i_outflowGroup = NInt(parnum(32)) ! define el grupo de salida
      i_dirOutflow = NInt(parnum(33)) ! define la dirección normal a la salida
      if (igr .eq. i_outflowGroup) then
         do in=1,nod(1)
            pin = p(in,ipgau,kint(1))
            press = (1.0-coopg(nsd)) * (rho-rhog) * grav
C            ico = i_dirOutflow
            ik = (in-1)*nrhse+1
            rhse(ik) = rhse(ik) + press * parnum(33) * pin * dj
            ik = (in-1)*nrhse+2
            rhse(ik) = rhse(ik) + press * parnum(34) * pin * dj
         end do
      end if
c  
c Condicion de borde que tiene que ver con gradiente de presiones      
      
c verify that the applied surface force is the one you want
c here igrf is the group, to which a force (parnum(20)) in the nsd
c direction is applied. if you want a force in the x2 direction,
c put ik = (in-1)*nrhse+2, for example (do NOT change nsd!)
c      igrf = NInt(parnum(21))
c      if (igr.eq.igrf) then
c         do in=1,nod(1)
c            ik = (in-1)*nrhse+nsd
c            rhse(ik)=rhse(ik)+dj*parnum(20)*p(in,ipgau,kint(1))
c         end do
c      end if
c      if (igr.eq.igrf) then
c         rho1 = parmat(1)
c         rho2 = parmat(3)
c         grav = -parmat(4+nsd)
c         zres = 0.15
c         pres = (rho1-rho2)*grav*(zres-coopg(nsd))
c         if (pres.lt.0) pres = 0.d0
c         do in=1,nod(1)
c            ik = (in-1)*nrhse+1
c            rhse(ik)=rhse(ik)-dj*pres*p(in,ipgau,kint(1))
c         end do
c      end if
      
c ---------------------------------------------------------------------

c                   Integrales para cálculo de caudal*dt (v*A*dt)

c ---------------------------------------------------------------------      

c rinte(15) caudal*dt inflow fluido 1 (phi<rlevel)
c rinte(16) caudal*dt inflow fluido 2 (phi>rlevel) - solo si hay fluido 2
c rinte(17) caudal*dt outflow fluido 1 (phi<rlevel)
c rinte(18) caudal*dt outflow fluido 2 (phi>rlevel) - solo si hay fluido 2
c rinte(19) area inflow
c rinte(20) area outflow
      ! definidos con i_ como enteros!
      i_inflowGroup = NInt(parnum(30)) ! define el grupo de entrada
      i_dirInflow = NInt(parnum(31)) ! define la dirección normal a la entrada
      i_outflowGroup = NInt(parnum(32)) ! define el grupo de salida
      i_dirOutflow = NInt(parnum(33)) ! define la dirección normal a la salida
      mixFlow = NInt(parnum(34)) ! mix = 1 define si existe flujo mixto (para calcular integrales de fluido 2)
      phi = var(1,4) ! level set para la posición dentro del elemento
      vnIn = abs(var(i_dirInflow,1)) ! velocidad normal de ingreso
      vnOut = abs(var(i_dirOutflow,1)) !  velocidad normal de salida
      vnormal = var(1,1)*normal(1) + var(2,1)*normal(2) + var(3,1)*normal(3)

      boundArea = dj ! area elemento borde * wpg

      rinte(15) = rinte(15) + vnormal*boundArea

      if (mixFlow.eq.1) then ! si existe flujo mixto

        if (igr.eq.i_inflowGroup) then ! volumen de ingreso
          if (phi.lt.rlevel) then ! ingresa fluido 1 (phi<rlevel)
            rinte(15) = rinte(15) + vnIn*boundArea*delcon 
          else ! ingresa fluido 2 (phi>rlevel)
            rinte(16) = rinte(16) + vnIn*boundArea*delcon    
          end if
          rinte(19) = rinte(19) + boundArea ! area del inflow
        end if

        if (igr.eq.i_outflowGroup) then ! volumen de salida
          if (phi.lt.rlevel) then ! sale fluido 1 (phi<rlevel)
            rinte(17) = rinte(17) + vnOut*boundArea*delcon
          else ! sale fluido 2 (phi>rlevel)
            rinte(18) = rinte(18) + vnOut*boundArea*delcon
          end if
          rinte(20) = rinte(20) + boundArea ! area del outflow
        end if

      else ! no existe flujo mixto

        if (igr.eq.i_inflowGroup) then ! volumen de ingreso
          rinte(15) = rinte(15) + vnIn*boundArea*delcon
          rinte(19) = rinte(19) + boundArea ! area del inflow
        end if

        if (igr.eq.i_outflowGroup) then ! volumen de salida
            rinte(17) = rinte(17) + vnOut*boundArea*delcon
            rinte(20) = rinte(20) + boundArea ! area del outflow
        end if

      end if


c----------------------------------------------------------------------
  600 continue   ! END LOOP OVER GAUSS POINTS

C----------------------------------------------------------------------
C INSERT HERE ANY INSTRUCTION YOU NEED JUST BEFORE LEAVING ROUTINE

C----------------------------------------------------------------------
       goto 987
c cilindro
       kmix = 99
       radio = parnum(7)
       do kl=1,nodgeo
        rr = (cogeo(kl,1)-1.)**2 + (cogeo(kl,2)-0.25)**2
        rr = sqrt(rr)
        if (rr.gt.radio.and.kmix.eq.99) then
         kmix = 1
        else if (rr.gt.radio.and.kmix.eq.-1) then
         kmix = 0
        end if
        if (rr.lt.radio.and.kmix.eq.99) then
         kmix = -1
        else if (rr.lt.radio.and.kmix.eq.1) then
         kmix = 0
        end if
       end do
c kmix = 1: outside cylinder, kmix=0: interface, kmix = -1...
c cilindro
      if (kmix.ne.1) then
         do i=1,dimrhse
            do j=1,dimrhse
               ae(i,j) = 0.d0
            enddo
         enddo
         do i=1,dimrhse
            rhse(i) = 0.d0
         enddo
      end if
c
 987  return
      end

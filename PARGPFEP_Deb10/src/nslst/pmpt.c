/*
 * pmpt.c : Point Monitoring and Particle tracing
 *  Utilities for following physical particles
 *  and monitoring fields at specified points
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "data_structures.h"
#include "interp.h"

struct t_nodo
{
  struct t_nodo *p_anterior;
  struct t_nodo *p_siguiente;
  double *coordenadas;				
};

struct t_static
{
  int n_s_lines;                                     //Numero de lineas de humo
  double *origin_s_lines;         //Vector de los puntos de origen de las l d h
  int init_Ok;                    //Indica si se ha producido la inicializacion
  int sbrp;                              //Pasos entre liberacion de particulas
  int cont_step;
  double nis;                             //Cantidad de subpasos de integracion
  long n_registros;
  struct t_nodo **heads;  //Almacena siempre la particula mas vieja de cada ldh
  struct t_nodo **tails;  //Almacena siempre la particula mas joven de cada ldh
};                        //es decir laque esta en el origen

static struct t_static datos;                                 //Variable global
static int periodic_linea_h, periodic_trayect;

/*Se pasa un vector con puntos, velocidades, cantidad de puntos,
 * cantidad de componentes y deltaT, 
 * se devuelven los puntos actualizados
 */

void Integrador_1er_O(double *point, double *result, int ncomps, double deltaT)
     // Euler hacia adelante
{
  int i;

  for (i = 0; i < ncomps; i++)
    {
      point[i] = point[i] + result[i] * deltaT;		
    }
}

void Integrador_2do_O(double *point, double *result, double *result2,
		      int ncomps, double deltaT)
     // Runge-Kutta de Orden 2, o
{    // Predictor-Corrector
  int i;

  for (i = 0; i < ncomps; i++)
    {
      point[i] = point[i] + 0.5 * deltaT * (result[i] + result2[i]);
    }
}

/* Funcion de busqueda de tag dentro de un archivo,
   devuelve el puntero a archivo en la posicion siguiente a la del tag.
   - Si la busqueda fue exitosa devuelve 0, sino -1		
*/

int busca_tag (FILE *p_arch, char cadena[100])
{
  char f_origen[100];
  rewind(p_arch);

  do
    {
      fgets(f_origen, 100, p_arch);
    } while ((strcmp(f_origen, cadena) != 0) && (feof(p_arch)==0));	

  if (feof(p_arch))
    {
      // printf("El archivo de configuracion no posee el tag %s\n", cadena);
      return -1;
    }
  else
    {
      return 0;
    }
}

void rect_perio(double *datos, int n, int nsd, double *periodicidad)
{
  int i, j;

  for (j = 0; j < nsd; j++)
    if (periodicidad[j] > 0.0)
      {
	for (i = 0; i < n; i++)
	  datos[i*nsd+j] = (datos[i*nsd+j] / periodicidad[j] -
			    floor(datos[i*nsd+j] / periodicidad[j])) *
	    periodicidad[j];
      }
}

void gpoints_compute (int mode, int nsd, int npoints,
		      double *pos_prev, double *pos,
		      int *swpoint, double *rl,
		      int nfields, double *flds_prev, double *flds,
		      double tini, double tfin,
		      int nvalspp, double *values, int *pstatus);

#define GPMODE_EVAL 0
#define GPMODE_EVAL_INTEG 1

/*
 * Manage particles (points that move with velocity field,
 *                   logging their position)
 * and generic points (locations where values are computed and logged,
 *                     posibly also moving with a velocity field) 
 */
void ptrace_ (systemdata **s_data, Vec *fv,
	      double *time, double *deltime, double *rl)
{
#define IMETH_EULER 0              // First order Euler
#define IMETH_RK2A 1               // Runge-Kutta O2 (Dt/2, Dt)
#define IMETH_RK2B 2               // Runge-Kutta O2 (Dt, 1/2(f(t)+f(t+Dt)))

  // Variables estaticas
  static int first_time = 1;       // Para identificar el primer ingreso
  static int int_method=0;         // Tipo de integración
  //  static double t=0.0;             // Contador continuo de tiempo
  static int n_paths = 0;          // Numero de trayectorias a calcular
  static int n_gpoints = 0;        // Numero de puntos genericos a calcular
  static double *points = NULL;    // Current position of particles
  static double *points_aux;
  static double *vels, *vels_prev;
  static double *gpoints = NULL;   // Generic points current position
  static double *gpoints_aux;
  static double *gvels_prev, *gvels;
  static double *pfields_prev, *pfields;
  static int *swgpoints;           // Generic points switches
  static double *values;           // Current values at generic points
  static int maxvalcomp, anymove;
  static int *estadop, *estadog, vfpos;

  FILE *p_arch_cfg, *p_paths, *p_gpoints;
  char cadena[300];
  int i, j;
  int n_points;         // Numero de puntos
  double point[3], vel[3], vel_prev[3];

  partition *Pa;
  double *VAct_Loc, *VAnt_Loc;
  double t = *time;
  double Dt = *deltime;
  Vec lv, lva;

  int rank, nprocs;

  Pa = (*s_data)->Pa;

  // Obtenemos los vectores de campos en el paso actual
  VecGhostGetLocalForm (*fv, &lv);
  VecGetArray(lv, &VAct_Loc);

  // Obtenemos los vectores de campos en el paso anterior
  VecGhostGetLocalForm ((*s_data)->va, &lva);
  VecGetArray(lva, &VAnt_Loc);

  // Obtenemos el identificador del proceso actual
  MPI_Comm_rank (Pa->Comm, &rank);

  // Obtenemos la cantidad de procesos corriendo
  MPI_Comm_size (Pa->Comm, &nprocs);

  if (first_time) // Primera llamada: se abre el archivo de configuracion
    {
      first_time = 0;
      if (rank == 0) 
    	{
	  p_arch_cfg = fopen("pmpt.cfg", "r");

	  if (p_arch_cfg)
	    {
	      strcpy(cadena, "*PATHS\n");
	      if (busca_tag(p_arch_cfg, cadena) == 0)
		{
		  fscanf(p_arch_cfg, "%d", &n_paths);
		  points = (double *) malloc (n_paths*Pa->nsd * sizeof(double));
		  for (i = 0; i < n_paths; i++)
		    for (j = 0; j < Pa->nsd; j++)
		      fscanf(p_arch_cfg, "%lf", &points[i*Pa->nsd+j]);

		  strcpy(cadena, "*OUTPUT_FOR_PERIODIC_MESH\n");
		  if (busca_tag (p_arch_cfg, cadena) == 0)
		    fscanf(p_arch_cfg, "%d", &periodic_trayect);	
		}
	      else
		n_paths = 0;

	      strcpy(cadena, "*GENERIC_POINTS\n");
	      n_gpoints = 0;
	      maxvalcomp = 0;
	      anymove = 0;
	      if (busca_tag(p_arch_cfg, cadena) == 0)
		{
		  fscanf(p_arch_cfg, "%d", &n_gpoints);
		  gpoints = (double *) malloc (n_gpoints * Pa->nsd *
					       sizeof(double));
		  swgpoints = (int *) malloc (n_gpoints * 3 * sizeof(int));
		  for (i = 0; i < n_gpoints; i++)
		    {
		      for (j = 0; j < Pa->nsd; j++)
			fscanf(p_arch_cfg, "%lf", &gpoints[i*Pa->nsd+j]);
		      fscanf(p_arch_cfg, "%d", &swgpoints[i*3]);
		      if (swgpoints[i*3] != 0)
			anymove = 1;
		      fscanf(p_arch_cfg, "%d", &swgpoints[i*3+1]);
		      if (swgpoints[i*3+1] > maxvalcomp)
			maxvalcomp = swgpoints[i*3+1];
		      fscanf(p_arch_cfg, "%d", &swgpoints[i*3+2]);
		    }
		}

	      if (n_paths > 0 || anymove)
		{
		  strcpy(cadena, "*VELOCITY_FIELD_POSITION\n");
		  if (busca_tag(p_arch_cfg, cadena) == 0)
		    fscanf(p_arch_cfg, "%d", &vfpos);
		  else
		    vfpos = 0;
		}

	      int_method = IMETH_RK2A;
	      strcpy(cadena, "*INTEGRATION_METHOD\n");
	      if (busca_tag(p_arch_cfg, cadena) == 0)
		fscanf(p_arch_cfg, "%d", &int_method);
	      switch (int_method)
		{
		case IMETH_EULER:
		  PetscPrintf(PETSC_COMM_SELF,
			      "Using Euler method for point tracing\n");
		  break;
		case IMETH_RK2A:
		  PetscPrintf(PETSC_COMM_SELF,
			      "Using midpoint method for point tracing\n");
		  break;
		case IMETH_RK2B:
		  PetscPrintf(PETSC_COMM_SELF,
			      "Using improved Euler's method for point tracing\n");
		  break;
		default:
		  PetscPrintf(PETSC_COMM_SELF,
			      "Warning: invalid %d method for point tracing.",
			      int_method);
		  PetscPrintf(PETSC_COMM_SELF,
			      "\nSwitching to default: midpoint method\n");
		  int_method = IMETH_RK2A;
		  break;
		}

	      fclose(p_arch_cfg);
	    }
	  else
	    {
	      n_paths = -1;
	      n_gpoints = -1;
	    }
	}

      MPI_Bcast(&n_paths, 1, MPI_INT, 0, Pa->Comm);
      MPI_Bcast(&n_gpoints, 1, MPI_INT, 0, Pa->Comm);
      MPI_Bcast(&int_method, 1, MPI_INT, 0, Pa->Comm);

      // Si n_paths < 0 no pudo abrir el archivo
      if (n_paths < 0)
	{
	  // Devolvemos los vectores de campos en el paso actual
	  VecRestoreArray(lv, &VAct_Loc);
	  VecGhostRestoreLocalForm (*fv, &lv);

	  // Devolvemos los vectores de campos en el paso anterior
	  VecRestoreArray(lva, &VAnt_Loc);
	  VecGhostRestoreLocalForm ((*s_data)->va, &lva);
	  return;
	}
		
      // Impresion de los datos a t inicial, el proceso cero
      // es el encargado de realizar la impresion	
      if (rank == 0) 
	{
	  /*
	   * Particles: only position
	   */
	  if (n_paths > 0)
	    {
	      p_paths = fopen("pospart.dat", "w");
	      fprintf (p_paths, 
		       "# Position of %d particles, dimension: %d\n",
		       n_paths, Pa->nsd);

	      // Datos a t = 0 (no hace falta calcular nada)
	      for (i = 0; i < n_paths * Pa->nsd; i++)
		fprintf(p_paths," %g", points[i]);
	      fprintf (p_paths, "\n");

	      fclose(p_paths);

	      // Impresion de cabecera de archivo en formato periodico
	      if (periodic_trayect == 1)
		{
		  p_paths=fopen("pospart_per.dat", "w");

		  // Cabecera del archivo
		  fprintf (p_paths, 
			   "# Position of %d particles, dimension: %d\n",
			   n_paths, Pa->nsd);

		  // Datos a t = 0 (no hace falta calcular nada)
		  for (i = 0; i < n_paths; i++)
		    for (j = 0; j < Pa->nsd; j++)
		      fprintf (p_paths," %g", points[i*Pa->nsd+j]);

		  fprintf(p_paths,"\n");
		  fclose(p_paths);
		}

	      // Vector para pasar posiciones (corregidas si periodic)
	      points_aux = (double *) malloc(n_paths*Pa->nsd*sizeof(double));

	      // Vector para obtener velocidades (predictor)
	      vels_prev = (double *) malloc(n_paths*Pa->nsd*sizeof(double));

	      // Vector para obtener velocidades (corrector)
	      if (int_method != IMETH_EULER)
		vels = (double *) malloc(n_paths*Pa->nsd*sizeof(double));

	      // Vector de estado de la interpolacion (paths)
	      estadop = (int *)malloc(n_paths*sizeof(int));
	    }

	  /*
	   * Generic points: maybe position, some other values
	   */
	  if (n_gpoints > 0)
	    {
	      p_gpoints = fopen("gpoints.dat", "w");
	      fprintf (p_gpoints, 
		       "# %d values computed at %d specified points\n",
		       maxvalcomp, n_gpoints);

	      values = (double *) malloc (n_gpoints * maxvalcomp *
					  sizeof(double));
	      for (i = 0; i < n_gpoints * maxvalcomp; i++)
		values[i] = 0.0;

	      fprintf (p_gpoints, "\n");
	      fclose (p_gpoints);
	      // Vector para pasar posiciones (corregidas si periodic)
	      gpoints_aux = (double *) malloc (n_gpoints * Pa->nsd *
					       sizeof(double));
	      // Vector para obtener velocidades (predictor)
	      gvels_prev = (double *) malloc(n_gpoints*Pa->nsd*sizeof(double));
	      // Vector para obtener velocidades (corrector)
	      if (int_method != IMETH_EULER)
		gvels = (double *)malloc(n_gpoints*Pa->nsd*sizeof(double));

	      // Vector de campos en los generic points:
	      pfields_prev = (double *) malloc (n_gpoints * (*s_data)->nfields *
						sizeof(double));
	      pfields = (double *) malloc (n_gpoints * (*s_data)->nfields *
					   sizeof(double));
	      // Vector de estado de la interpolacion
	      estadog = (int *) malloc (n_gpoints * sizeof(int));
	    }
	} // El proceso de rango 0 escribe encabezados

      /*
       * First time: compute values, 1st process writes
       */
      if (n_gpoints > 0)
	{
	  int mode = GPMODE_EVAL;
	  // Interpolate all fields in all points
	  n_points = (rank == 0) ? n_gpoints : 0;
	  interp_global (Pa, gpoints, n_points, VAnt_Loc,
			 (*s_data)->nfields, 0, (*s_data)->nfields,
			 pfields_prev, estadog);
	  // Pass computed field to "gpoints_compute" to get "values"
	  // at time "t"
	  gpoints_compute (mode, Pa->nsd, n_points, gpoints, gpoints,
			   swgpoints, rl,
			   (*s_data)->nfields, pfields_prev, pfields,
			   t, t + Dt,
			   maxvalcomp, values, estadog);

	  if (rank == 0)
	    {
	      p_gpoints = fopen("gpoints.dat", "a");
	      for (i = 0; i < n_gpoints; i++)
		{
		  if (anymove)
		    for (j = 0; j < Pa->nsd; j++)
		      fprintf(p_gpoints," %g", gpoints[i*Pa->nsd+j]);
		  for (j = 0; j < maxvalcomp; j++)
		    fprintf(p_gpoints," %g", values[i*maxvalcomp+j]);
		}
	      fprintf (p_gpoints, "\n");
	      fclose(p_gpoints);
	    } // Only 1st process
	} // There are generic points

    } // Solo se ejecuta la primera vez

  /*
   * New step, integrate trajectory, compute new values
   */
  if (rank == 0 && n_paths > 0)
    {
      /*
       * New step, integrate trajectory, compute new values
       */

      // Se piden las velocidades en los puntos originales
      for (i = 0; i < n_paths; i++)	
	for (j = 0; j < Pa->nsd; j++)
	  points_aux[i*Pa->nsd+j] = points[i*Pa->nsd+j];

      // Eventualmente corregida su posición (si periódica)
      rect_perio(points_aux, n_paths, Pa->nsd, rl);
      n_points = n_paths;
    } 
  else
    {
      // El resto de los procesesadores no va a tener ningun punto
      n_points = 0;
      //      posi_veli_orig=NULL;
      estadop = NULL;
    }

  // Obtencion de las velocidades para dichos puntos
  interp_global (Pa, points_aux, n_points, VAnt_Loc,
		 (*s_data)->nfields, vfpos, Pa->nsd, vels_prev, estadop);
  for (i = 0; i < n_points; i++)
    if (estadop[i])
      for (j = 0; j < Pa->nsd; j++)
	vels_prev[i*Pa->nsd+j] = 0.0;

  switch (int_method)
    {
    case IMETH_EULER:
      // Solo proceso de rank == 0 (en el resto n_points == 0)
      for (i = 0; i < n_points; i++)
	{
	  for (j = 0; j < Pa->nsd; j++) // Cargamos las coordenas del punto
	    { // donde se interpolara el campo en el tipo de var
	      // que requiere interp_local
	      point[j] = points[i*Pa->nsd+j];
	      vel_prev[j] = vels_prev[i*Pa->nsd+j];
	    }

	  // Predecimos la nuevas posiciones a partir de
	  // las posiciones originales y las velocidades en ellas
	  Integrador_1er_O(point, vel_prev, Pa->nsd, Dt);
	  // Calculo del paso de Euler
	  // Las posiciones predichas son almacenadas en
	  // el vector correspondiente
	  for (j = 0; j < Pa->nsd; j++)
	    points[i*Pa->nsd+j] = point[j];
	}
      break;
    case IMETH_RK2A:
      // Solo proceso de rank == 0 (en el resto n_points == 0)
      for (i = 0; i < n_points; i++)
	{
	  for (j = 0; j < Pa->nsd; j++) // Cargamos las coordenas del punto
	    { // donde se interpolara el campo en el tipo de var
	      // que requiere interp_local
	      point[j] = points[i*Pa->nsd+j];
	      vel_prev[j] = vels_prev[i*Pa->nsd+j];
	    }

	  // Predecimos la posiciones en t + Dt/2 a partir de
	  // las posiciones originales y las velocidades en ellas
	  Integrador_1er_O(point, vel_prev, Pa->nsd, Dt/2);
	  // Calculo del paso predictor
	  // Las posiciones predichas son almacenadas en
	  // el vector correspondiente
	  for (j = 0; j < Pa->nsd; j++)
	    points_aux[i*Pa->nsd+j]=point[j];
	}
      rect_perio (points_aux, n_points, Pa->nsd, rl);
      // 2-PASO CORRECTOR
      // Evaluamos las velocidades vels_prev y vels en
      // todos los puntos predichos
      interp_global (Pa, points_aux, n_points, VAnt_Loc,
		     (*s_data)->nfields, vfpos, Pa->nsd, vels_prev, estadop);
      interp_global (Pa, points_aux, n_points, VAct_Loc,
		     (*s_data)->nfields, vfpos, Pa->nsd, vels, estadop);
      for (i = 0; i < n_points; i++)
	if (estadop[i])
	  for (j = 0; j < Pa->nsd; j++)
	    vels_prev[i*Pa->nsd+j] = vels[i*Pa->nsd+j] = 0.0;

      // Se avanza paso completo con la velocidad media en el punto "medio"
      for (i = 0; i < n_points; i++)
	{
	  for (j = 0; j < Pa->nsd; j++)
	    {
	      point[j] = points[i*Pa->nsd+j];
	      vel_prev[j] = 0.5 * (vels_prev[i*Pa->nsd+j] + vels[i*Pa->nsd+j]);
	    }

	  // Calculamos la posiciones en t + Dt a partir de
	  // las posiciones originales y las velocidades en t + Dt/2
	  Integrador_1er_O(point, vel_prev, Pa->nsd, Dt);
	  // Las posiciones calculadas son almacenadas en
	  // el vector correspondiente
	  for (j = 0; j < Pa->nsd; j++)
	    points[i*Pa->nsd+j] = point[j];
	}
      break;
    case IMETH_RK2B:
      // Solo proceso de rank == 0 (en el resto n_points == 0)
      for (i = 0; i < n_points; i++)
	{
	  for (j = 0; j < Pa->nsd; j++) // Cargamos las coordenas del punto
	    { // donde se interpolara el campo en el tipo de var
	      // que requiere interp_local
	      point[j] = points[i*Pa->nsd+j];
	      vel_prev[j] = vels_prev[i*Pa->nsd+j];
	    }
	  
	  // Predecimos la nuevas posiciones a partir de
	  // las posiciones originales y las velocidades en ellas
	  Integrador_1er_O(point, vel_prev, Pa->nsd, Dt);
	  // Calculo del paso predictor
	  // Las posiciones predichas son almacenadas en
	  // el vector correspondiente
	  for (j = 0; j < Pa->nsd; j++)
	    points_aux[i*Pa->nsd+j]=point[j];
	}
      rect_perio (points_aux, n_points, Pa->nsd, rl);
      // 2-PASO CORRECTOR
      // Evaluamos vels en los puntos predichos (a t + Dt)
      interp_global (Pa, points_aux, n_points, VAct_Loc,
		     (*s_data)->nfields, vfpos, Pa->nsd, vels, estadop);
      for (i = 0; i < n_points; i++)
	if (estadop[i]) // si cayo fuera de la malla se toma la vel. previa
	  for (j = 0; j < Pa->nsd; j++)
	    vels[i*Pa->nsd+j] = vels_prev[i*Pa->nsd+j];

      // Podemos ahora efectuar el paso corrector
      for (i = 0; i < n_points; i++)
	{
	  for (j = 0; j < Pa->nsd; j++) // Cargamos las coordenadas del punto
	    { // donde se interpolara el campo en el tipo de var
	      // que requiere interp_local
	      point[j] = points[i*Pa->nsd+j];
	      vel_prev[j] = vels_prev[i*Pa->nsd+j];
	      vel[j] = vels[i*Pa->nsd+j];
	    }

	  // Predecimos la nuevas posiciones a partir de
	  // las posiciones originales y las velocidades en ellas
	  Integrador_2do_O (point, vel_prev, vel, Pa->nsd, Dt);
	  // Calculo del paso corrector
	  // Las posiciones corregidas son almacenadas en el vector de puntos
	  // (STATIC)
	  for (j = 0; j < Pa->nsd; j++)
	    points[i*Pa->nsd+j] = point[j];
	}
      break;
    }

  // Impresion de los puntos actualizados,
  // solo se ejecuta en el proceso de rank=0
  if (rank == 0 && n_paths > 0)
    {
      p_paths = fopen("pospart.dat", "a");
      for (i = 0; i < n_paths; i++)
	// Impresion de los puntos a t + Dt
	{
	  /* if (estadop[i] == 0) */
	  /*   { */
	  for (j = 0; j < Pa->nsd; j++)
	    fprintf (p_paths, " %g", points[i*Pa->nsd+j]);
	  /*   } */
	  /* else */
	  /*   { */
	  /*     for (j = 0; j < (Pa->nsd); j++) */
	  /* 	fprintf (p_paths," 314e-12"); */
	  /*   } */
	}

      // Al finalizar la linea en el archivo se escribe un \n.
      fprintf (p_paths, "\n");
      //      t = t + *deltaT;  // Se actualiza el tiempo
      fclose (p_paths);
      // Se cierra el archivo de almacenamiento de las trayectorias

      // Impresion en formato periodico, solo si el flag esta activo
      if (periodic_trayect == 1)
	{
	  p_paths = fopen ("pospart_per.dat", "a");
	  for (i = 0; i < n_paths; i++)
	    {
	      /* if (estadop[i] == 0) */
	      for (j = 0; j < Pa->nsd; j++)
		{
		  if (rl[j] > 0.0)
		    fprintf (p_paths, " %g",
			     (points[i*Pa->nsd+j] / rl[j] -
			      floor(points[i*Pa->nsd+j] / rl[j])) * rl[j]);
		  else
		    fprintf (p_paths, " %g", points[i*Pa->nsd+j]);
		}
	    }
	  fprintf (p_paths, "\n");
	  fclose(p_paths);
	} // if (periodic...
    } // rank 0 process

  if (n_gpoints < 1)
    return;
  /*
   * Generic points treatment...
   * first the moving points
   */
  if (rank == 0 && n_gpoints > 0 && anymove)
    {
      /*
       * Integrate trajectory (points with swgpoints[0] != 0)
       */
      n_points = 0;
      for (i = 0; i < n_gpoints; i++)
	if (swgpoints[3*i])           // point follows fluid
	  {
	    for (j = 0; j < Pa->nsd; j++)
	      gpoints_aux[n_points*Pa->nsd+j] = gpoints[i*Pa->nsd+j];
	    n_points++;
	  }
      
      rect_perio(gpoints_aux, n_points, Pa->nsd, rl);
    }
  else
    {
      n_points = 0;
      if (rank != 0)
	estadog = NULL;
    }

  // Obtencion de las velocidades para dichos puntos
  interp_global (Pa, gpoints_aux, n_points, VAnt_Loc,
		 (*s_data)->nfields, vfpos, Pa->nsd, gvels_prev, estadog);
  for (i = 0; i < n_points; i++)
    if (estadog[i])
      for (j = 0; j < Pa->nsd; j++)
	gvels_prev[i*Pa->nsd+j] = 0.0;

  switch (int_method)
    {
    case IMETH_EULER:
      n_points = 0;
      for (i = 0; i < n_gpoints && rank == 0; i++)
	{
	  for (j = 0; j < Pa->nsd; j++) // save previous position
	    gpoints_aux[i*Pa->nsd+j] = gpoints[i*Pa->nsd+j];
	  if (swgpoints[3*i])           // point follows fluid
	    {
	      for (j = 0; j < Pa->nsd; j++)
		{
		  point[j] = gpoints[i*Pa->nsd+j];
		  vel_prev[j] = gvels_prev[n_points*Pa->nsd+j];
		}
	      Integrador_1er_O(point, vel_prev, Pa->nsd, Dt);
	      for (j = 0; j < Pa->nsd; j++)
		gpoints[i*Pa->nsd+j] = point[j];
	      n_points++;
	    }
	  }
      break;
    case IMETH_RK2A:
      n_points = 0;
      for (i = 0; i < n_gpoints && rank == 0; i++)
	if (swgpoints[3*i])           // point follows fluid
	  {
	    for (j = 0; j < Pa->nsd; j++)
	      {
		point[j] = gpoints[i*Pa->nsd+j];
		vel_prev[j] = gvels_prev[n_points*Pa->nsd+j];
	      }
	    Integrador_1er_O(point, vel_prev, Pa->nsd, Dt/2);
	    for (j = 0; j < Pa->nsd; j++)
	      gpoints_aux[n_points*Pa->nsd+j] = point[j];
	    n_points++;
	  }
      rect_perio (gpoints_aux, n_points, Pa->nsd, rl);

      // 2-PASO CORRECTOR
      // Evaluamos las velocidades gvels_prev y gvels en
      // todos los puntos predichos
      interp_global (Pa, gpoints_aux, n_points, VAnt_Loc,
		     (*s_data)->nfields, vfpos, Pa->nsd, gvels_prev, estadog);
      interp_global (Pa, gpoints_aux, n_points, VAct_Loc,
		     (*s_data)->nfields, vfpos, Pa->nsd, gvels, estadog);
      for (i = 0; i < n_points; i++)
	if (estadog[i])
	  for (j = 0; j < Pa->nsd; j++)
	    gvels_prev[i*Pa->nsd+j] = gvels[i*Pa->nsd+j] = 0.0;

      // Se avanza paso completo con la velocidad media en el punto "medio"
      n_points = 0;
      for (i = 0; i < n_gpoints && rank == 0; i++)
	{
	  for (j = 0; j < Pa->nsd; j++) // save previous position
	    gpoints_aux[i*Pa->nsd+j] = gpoints[i*Pa->nsd+j];
	  if (swgpoints[3*i])           // point follows fluid
	    {
	      for (j = 0; j < Pa->nsd; j++)
		{
		  point[j] = gpoints[i*Pa->nsd+j];
		  vel_prev[j] = 0.5 * (gvels_prev[n_points*Pa->nsd+j] +
				       gvels[n_points*Pa->nsd+j]);
		}

	    // Calculamos la posiciones en t + Dt a partir de
	    // las posiciones originales y las velocidades en t + Dt/2
	    Integrador_1er_O(point, vel_prev, Pa->nsd, Dt);

	    for (j = 0; j < Pa->nsd; j++)
	      gpoints[i*Pa->nsd+j] = point[j];
	    n_points++;
	  }
	}
      break;
    case IMETH_RK2B:
      n_points = 0;
      for (i = 0; i < n_gpoints && rank == 0; i++)
	if (swgpoints[3*i])           // point follows fluid
	  {
	    for (j = 0; j < Pa->nsd; j++)
	      {
		point[j] = gpoints[i*Pa->nsd+j];
		vel_prev[j] = gvels_prev[n_points*Pa->nsd+j];
	      }
	    // Predecimos la nuevas posiciones a partir de
	    // las posiciones originales y las velocidades en ellas
	    Integrador_1er_O(point, vel_prev, Pa->nsd, Dt);

	    // Las posiciones predichas son almacenadas en
	    // el vector correspondiente
	    for (j = 0; j < Pa->nsd; j++)
	      gpoints_aux[n_points*Pa->nsd+j] = point[j];
	    n_points++;
	  }
      rect_perio (gpoints_aux, n_points, Pa->nsd, rl);

      // 2-PASO CORRECTOR
      // Evaluamos gvels en los puntos predichos (a t + Dt)
      interp_global (Pa, gpoints_aux, n_points, VAct_Loc,
		     (*s_data)->nfields, vfpos, Pa->nsd, gvels, estadog);
      for (i = 0; i < n_points; i++)
	if (estadog[i]) // si cayo fuera de la malla se toma la vel. previa
	  for (j = 0; j < Pa->nsd; j++)
	    gvels[i*Pa->nsd+j] = gvels_prev[i*Pa->nsd+j];

      // Podemos ahora efectuar el paso corrector
      n_points = 0;
      for (i = 0; i < n_gpoints && rank == 0; i++)
	{
	  for (j = 0; j < Pa->nsd; j++) // save previous position
	    gpoints_aux[i*Pa->nsd+j] = gpoints[i*Pa->nsd+j];
	  if (swgpoints[3*i])           // point follows fluid
	    {
	      for (j = 0; j < Pa->nsd; j++)
		{
		  point[j] = gpoints[i*Pa->nsd+j];
		  vel_prev[j] = gvels_prev[n_points*Pa->nsd+j];
		  vel[j] = gvels[n_points*Pa->nsd+j];
		}

	      // Predecimos la nuevas posiciones a partir de
	      // las posiciones originales y las velocidades en ellas
	      Integrador_2do_O (point, vel_prev, vel, Pa->nsd, Dt);
	      // Calculo del paso corrector
	      // Se almacenan las posiciones corregidas
	      for (j = 0; j < Pa->nsd; j++)
		gpoints[i*Pa->nsd+j] = point[j];
	      n_points++;
	    }
	}
      break;
    }

  /*
   * Ahora los generic points, cálculo y/o integración de otras cosas:
   */
  int mode = GPMODE_EVAL_INTEG;
  n_points = (rank == 0) ? n_gpoints : 0;
  interp_global (Pa, gpoints, n_points, VAct_Loc,
		 (*s_data)->nfields, 0, (*s_data)->nfields,
		 pfields, estadog);
  gpoints_compute (mode, Pa->nsd, n_points, gpoints_aux, gpoints,
		   swgpoints, rl,
		   (*s_data)->nfields, pfields_prev, pfields, t, t+Dt,
		   maxvalcomp, values, estadog);
  for (i = 0; i < n_points * (*s_data)->nfields; i++)
    pfields_prev[i] = pfields[i];
  /*
   * Generic points output
   */
  if (rank == 0)
    {
      p_gpoints = fopen("gpoints.dat", "a");
      for (i = 0; i < n_gpoints; i++)
	{
	  if (anymove)
	    for (j = 0; j < Pa->nsd; j++)
	      fprintf(p_gpoints," %g", gpoints[i*Pa->nsd+j]);
	  for (j = 0; j < maxvalcomp; j++)
	    fprintf(p_gpoints," %g", values[i*maxvalcomp+j]);
	}
      fprintf (p_gpoints, "\n");
      fclose(p_gpoints);
    } // Only 1st process

  // Devolvemos los vectores de campos en el paso actual
  VecRestoreArray(lv, &VAct_Loc);
  VecGhostRestoreLocalForm (*fv, &lv);

  // Devolvemos los vectores de campos en el paso anterior
  VecRestoreArray(lva, &VAnt_Loc);
  VecGhostRestoreLocalForm ((*s_data)->va, &lva);

  return;
} // Fin de ptrace_

/*
 *
 * gpoints_compute: Input:
 * mode: GPMODE_EVAL->only evaluate at a fixed time
 *                    (use tini, pos_prev, flds_prev)
 *       GPMODE_EVAL_INTEG->evaluate at fixed time
 *                    (tfin, pos, flds) and/or
 *                    integrate from tini to tfin using
 *                    pos_prev -> pos, flds_prev -> flds
 *
 * npoints:   number of points
 * pos_prev:  position of point at time tini
 * pos:       position of point at time tfin
 * swpoint:   3 integers per point
 *          - Indicator of physical particle (should move with velocity field)
 *          - Number of values to compute at point
 *          - Label (or case indicator)
 * rl:        indicator for periodic mesh
 * nfields:   Number of fields (scalars) in flds[_prev] vectors
 * flds_prev: Value of fields at time tini
 * flds:      Value of fields at time tfin
 * tini:      Initial time
 * tfin:      Final time
 * nvalspp:   Maximum number of values to return at each point
 * values:    return values at each point, size: nvalspp*npoints
 * pstatus:   error indicator at each point
 *
 */
void gpoints_compute (int mode, int nsd, int npoints,
		      double *pos_prev, double *pos,
		      int *swpoint, double *rl,
		      int nfields, double *flds_prev, double *flds,
		      double tini, double tfin,
		      int nvalspp, double *values, int *pstatus)
{
  int i, j, swparticle, nvals, p_case;
  double /* *cpos,  */*cflds;

  if (mode == GPMODE_EVAL)
    {
      /* cpos = pos_prev; */
      cflds = flds_prev;
    }
  else
    {
      /* cpos = pos; */
      cflds = flds;
    }

  for (i = 0; i < npoints; i++)
    {
      swparticle = swpoint[3*i];
      nvals = swpoint[3*i+1];
      p_case = swpoint[3*i+2];

      switch (p_case)
	{
	  // Example: integrate time (residence time?)
	case 1:
	  if (!pstatus[i] && mode == GPMODE_EVAL_INTEG)
	    values[nvalspp*i] += tfin - tini;
	  break;
	  // Example: "cook time" (\int_0^t Temp dt)
	case 2:
	  if (!pstatus[i] && mode == GPMODE_EVAL_INTEG)
	    {
	      int ktemp = 7;
	      values[nvalspp*i] +=
		(flds_prev[nfields*i+ktemp] + flds[nfields*i+ktemp]) / 2 *
		(tfin - tini);
	    }
	  break;
	  // Example: integrate space (distance travelled)
	  //  simple and velocity corrected computation
	case 3:
	  if (!pstatus[i] && swparticle && mode == GPMODE_EVAL_INTEG)
	    {
	      int vfpos = 0; // Velocity field position
	      double d, dd, d1, p1[3], p2[3], vv;

	      for (j = 0; j < nsd; j++)
		{
		  vv = flds_prev[nfields*i + vfpos + j];
		  p1[j] = pos_prev[i*nsd+j] + (tfin-tini)/2 * vv;
		  vv = flds[nfields*i + vfpos + j];
		  p2[j] = pos[i*nsd+j] - (tfin-tini)/2 * vv;
		}
	      for (j = 0; j < nsd; j++)
		p1[j] = (p1[j] + p2[j]) / 2;

	      for (j = 0, dd = 0; j < nsd; j++)
		{
		  d = (p1[j] - pos_prev[i*nsd+j]);
		  dd += d*d;
		}
	      d1 = sqrt(dd);
	      for (j = 0, dd = 0.0; j < nsd; j++)
		{
		  d = pos[i*nsd+j] - p1[j];
		  dd += d*d;
		}
	      values[nvalspp*i] += d1 + sqrt(dd);

	      for (j = 0, dd = 0.0; j < nsd; j++)
		{
		  d = pos[i*nsd+j] - pos_prev[i*nsd+j];
		  dd += d*d;
		}
	      values[nvalspp*i+1] += sqrt(dd);
	    }
	  break;
	  // Example: integrate time (residence time)
	  //          integrate space (distance travelled) with
	  //               velocity corrected computation
	  //          monitor fields: vx, vy, vz, pressure, temperature
	case 4:
	  if (!pstatus[i] && mode == GPMODE_EVAL_INTEG)
	    {
	      int vfpos = 0; // Velocity field position
	      double d, dd, d1, p1[3], p2[3], vv;

	      values[nvalspp*i] += tfin - tini; // time

	      for (j = 0; j < nsd; j++)
		{
		  vv = flds_prev[nfields*i + vfpos + j];
		  p1[j] = pos_prev[i*nsd+j] + (tfin-tini)/2 * vv;
		  vv = flds[nfields*i + vfpos + j];
		  p2[j] = pos[i*nsd+j] - (tfin-tini)/2 * vv;
		}
	      for (j = 0; j < nsd; j++)
		p1[j] = (p1[j] + p2[j]) / 2;

	      for (j = 0, dd = 0; j < nsd; j++)
		{
		  d = (p1[j] - pos_prev[i*nsd+j]);
		  dd += d*d;
		}
	      d1 = sqrt(dd);
	      for (j = 0, dd = 0.0; j < nsd; j++)
		{
		  d = pos[i*nsd+j] - p1[j];
		  dd += d*d;
		}
	      values[nvalspp*i+1] += d1 + sqrt(dd);
	    }
	  if (!pstatus[i])
	    {
	      values[nvalspp * i + 2] = cflds[nfields*i];
	      values[nvalspp * i + 3] = cflds[nfields*i+1];
	      values[nvalspp * i + 4] = cflds[nfields*i+2];
	      values[nvalspp * i + 5] = cflds[nfields*i+3];
	      values[nvalspp * i + 6] = cflds[nfields*i+11];
	    }
	  else
	    {
	      values[nvalspp * i + 2] = 0.0;
	      values[nvalspp * i + 3] = 0.0;
	      values[nvalspp * i + 4] = 0.0;
	      values[nvalspp * i + 5] = 0.0;
	      values[nvalspp * i + 6] = 0.0;
	    }
	  break;
	  // Example: integrate time (residence time)
	  //          integrate space (distance travelled) with
	  //               velocity corrected computation
	  //          monitor fields: vx, vy, pressure, temperature, lci
	case 5:
	  if (!pstatus[i] && mode == GPMODE_EVAL_INTEG)
	    {
	      int vfpos = 0; // Velocity field position
	      double d, dd, d1, p1[3], p2[3], vv;

	      values[nvalspp*i] += tfin - tini; // time

	      for (j = 0; j < nsd; j++)
		{
		  vv = flds_prev[nfields*i + vfpos + j];
		  p1[j] = pos_prev[i*nsd+j] + (tfin-tini)/2 * vv;
		  vv = flds[nfields*i + vfpos + j];
		  p2[j] = pos[i*nsd+j] - (tfin-tini)/2 * vv;
		}
	      for (j = 0; j < nsd; j++)
		p1[j] = (p1[j] + p2[j]) / 2;

	      for (j = 0, dd = 0; j < nsd; j++)
		{
		  d = (p1[j] - pos_prev[i*nsd+j]);
		  dd += d*d;
		}
	      d1 = sqrt(dd);
	      for (j = 0, dd = 0.0; j < nsd; j++)
		{
		  d = pos[i*nsd+j] - p1[j];
		  dd += d*d;
		}
	      values[nvalspp*i+1] += d1 + sqrt(dd);
	    }
	  if (!pstatus[i])
	    {
	      values[nvalspp * i + 2] = cflds[nfields*i];
	      values[nvalspp * i + 3] = cflds[nfields*i+1];
	      values[nvalspp * i + 4] = cflds[nfields*i+2];
	      values[nvalspp * i + 5] = cflds[nfields*i+9];
	      values[nvalspp * i + 6] = cflds[nfields*i+10];
	    }
	  else
	    {
	      values[nvalspp * i + 2] = 0.0;
	      values[nvalspp * i + 3] = 0.0;
	      values[nvalspp * i + 4] = 0.0;
	      values[nvalspp * i + 5] = 0.0;
	      values[nvalspp * i + 6] = 0.0;
	    }
	  break;
	default:  // Monitor all fields
	  for (j = 0; j < nvals && j < nfields; j++)
	    if (!pstatus[i])
	      values[nvalspp*i+j] = cflds[nfields*i+j];
	    else
	      values[nvalspp*i+j] = 0.0;
	}
    } /* End loop over points */
  
} /* End function gpoints_compute */


void lineashumoinit_ (systemdata **s_data, Vec *fv)
{
  partition *Pa;
  /* double *VLoc; */
  FILE *p_arch_cfg;
  char cadena[100];
  int i, j;
  int rank, nprocs;
  /* Vec lv; */

  Pa=(*s_data)->Pa;
  /* VecGhostGetLocalForm(*fv, &lv); */
  /* VecGetArray(lv, &VLoc); */

  // Obtenemos el identificador del proceso actual
  MPI_Comm_rank(Pa->Comm, &rank);

  // Obtenemos la cantidad de procesos corriendo
  MPI_Comm_size(Pa->Comm, &nprocs);

  // Solamente el proceso cero posee las estructuras de datos
  datos.n_s_lines = 0;

  // Solo el proceso cero sera el que eventualmente tendra el valor correcto
  if (rank == 0)
    {
      p_arch_cfg = fopen ("gpfep.cfg", "r");
      // En la primera llamada se abre el archivo de configuracion
      strcpy (cadena, "*NUMBER_OF_STREAKLINES\n"); // Numero de lineas de humo
      if (busca_tag(p_arch_cfg, cadena) == 0)
    	{
	  fscanf (p_arch_cfg, "%d", &datos.n_s_lines);
	  // Puntos de origen de las lineas de humo
	  strcpy (cadena, "*ORIGIN_OF_STREAKLINES\n");

	  if (busca_tag (p_arch_cfg, cadena) == 0)
	    {
	      datos.origin_s_lines =
		(double *) malloc(datos.n_s_lines*Pa->nsd*sizeof(double));
	      // Se reserva memoria para los puntos de origen
	      for (i = 0; i < datos.n_s_lines; i++)
		// Bucle de lectura de los puntos
		{
		  for (j = 0; j < Pa->nsd; j++)
		    {
		      fscanf (p_arch_cfg, "%lf",
			      &datos.origin_s_lines[i*Pa->nsd+j]);
		    }
		}

	      strcpy (cadena, "*STEPS_BETWEEN_RELEASING_PARTICLES\n");
	      // Lectura de STEPS_BETWEEN_RELEASING_PARTICLES
	      if (busca_tag(p_arch_cfg, cadena) == 0)
		{
		  fscanf (p_arch_cfg, "%d", &datos.sbrp);		
		  strcpy (cadena, "*NUMBER_OF_INTEGRATION_SUBSTEPS\n");
		  // Lectura de NUMBER_OF_INTEGRATION_SUBSTEPS
		  if (busca_tag(p_arch_cfg, cadena) == 0)
		    {
		      fscanf (p_arch_cfg, "%lf", &datos.nis);
		      strcpy (cadena, "*OUTPUT_FOR_PERIODIC_MESH\n");
		      // Setea cadena a buscar;
		      if (busca_tag(p_arch_cfg, cadena) == 0)
			{
			  fscanf (p_arch_cfg, "%d", &periodic_linea_h);
			  // En el caso que la configuracion haya sido leida
			  // correctamente se crean las estructuras
			  datos.heads = (struct t_nodo **)
			    malloc(datos.n_s_lines*sizeof(struct t_nodo*));
			  datos.tails = (struct t_nodo **)
			    malloc(datos.n_s_lines*sizeof(struct t_nodo*));

			  // La cantidad de registros (particulas) actual es 0
			  datos.n_registros = 0;
			  // Inicializacion de cabezas y colas,
			  // en este caso las cabezas y las colas son iguales.
			  for (i = 0; i < datos.n_s_lines; i++)
			    {
			      // Pedimos memoria para el unico registro de
			      // la cola actual
			      datos.heads[i] = (struct t_nodo *)
				malloc(sizeof(struct t_nodo));		
			      // Como se ha creado un registro debe
			      // incrementarse su contador correspondiente
			      datos.n_registros++;

			      // Pedimos memoria para las cordenadas
			      datos.heads[i]->coordenadas = (double *)
				malloc(Pa->nsd*sizeof(double));
			      // Dado que la lista posee un solo registro
			      // los punteros al anterior y el posterior deben
			      // ser NULL
			      datos.heads[i]->p_anterior = NULL;
			      datos.heads[i]->p_siguiente = NULL;
			      // Cargamos en cada registro las coordenadas
			      // que le corresponden
			      for (j = 0; j < (Pa->nsd); j++)
				{
				  datos.heads[i]->coordenadas[j] =
				    datos.origin_s_lines[i*Pa->nsd+j];
				}

			      // El registro es tanto cabeza como cola,
			      // por lo tanto deben igualarse
			      datos.tails[i] = datos.heads[i];
			    } // Fin del bucle en cantidad de lineas de humo

			  datos.init_Ok = 1;
			  // Avisa que debe calcular las lineas de humo
			}
		    }
		}
	    }
	}

      fclose(p_arch_cfg);
      // En cualquiera de las opciones se debe
      // cerrar el archivo de configuracion
    } // Fin de rank==0

  MPI_Bcast (&datos.nis, 1, MPI_DOUBLE, 0, Pa->Comm);
  // Comunica al resto de los procesos el valor de datos.nis
  MPI_Bcast (&datos.init_Ok, 1, MPI_INT, 0, Pa->Comm);
  // Comunica la situacion de inicializacion
} // Fin de lineashumoinit_

void lineashumointegrate_ (systemdata **s_data, Vec *fv, double *deltaT,
			   double *rl)
{
  struct t_nodo *p_temp, *p_libera;
  int i, j, k, n;

  double *point, *point_temp;
  double *posi_orig, *posi_veli, *posi_veli_orig, *veloc, *veloc2;
  double delta;
  double *x, *dx;
  double dist;
  int *estadop, *estadoc;
  int rank, nprocs;

  partition *Pa;
  double *VAct_Loc, *VAnt_Loc;
  Vec lv, lva;

  Pa=(*s_data)->Pa;
  MPI_Comm_rank(Pa->Comm, &rank);
  MPI_Comm_size(Pa->Comm, &nprocs);

  // Obtenemos los vectores de velocidades en el paso actual
  VecGhostGetLocalForm(*fv, &lv);
  VecGetArray(lv, &VAct_Loc);

  // Obtenemos los vectores de velocidades en el paso anterior
  VecGhostGetLocalForm((*s_data)->va, &lva);
  VecGetArray(lva, &VAnt_Loc);

  // Verificacion de inicializacion del sistema de calculo de lineas de humo
  if (datos.init_Ok != 1)
    {
      // printf("ERROR Sist. de calculo de lineas de humo no inicializado\n");
      return;
    }

  // Se actualizan las posiciones por integracion
  if (rank == 0)
    {
      point = (double *)malloc(Pa->nsd*sizeof(double));  // Punto a interpolar
      veloc = (double *)malloc(Pa->nsd*sizeof(double));  // Velocidad en el
      // paso predictor
      veloc2 = (double *)malloc(Pa->nsd*sizeof(double)); // Velocidad en el
      // paso corrector

      posi_orig = (double *)malloc(datos.n_registros*Pa->nsd*sizeof(double));
      // Vector con las posiciones originales de todos los puntos
      posi_veli = (double *)malloc(datos.n_registros*Pa->nsd*sizeof(double));
      // Paso corrector
      posi_veli_orig = (double *)
	malloc(datos.n_registros*Pa->nsd*sizeof(double));  // Paso predictor

      estadop = (int *)malloc(datos.n_registros*sizeof(int));
      // Vector de estados de interpolacion	(predictor)
      estadoc = (int *)malloc(datos.n_registros*sizeof(int));
      // Vector de estados de interpolacion	(predictor)		
    }
  else
    {
      //Preparation for dummy callings	
      point=NULL;
      veloc=NULL;
      veloc2=NULL;

      posi_orig=NULL;
      posi_veli=NULL; 
      posi_veli_orig=NULL;

      estadop=NULL;
      estadoc=NULL;
    }

  delta = *deltaT/datos.nis;

  // Este bucle lo deben hacer todos los procesos para
  // sincronizar la cantidad de llamadas a interp_global
  for (n = 1; n <= datos.nis; n++)
    {
      // 1-PASO PREDICTOR	
      // Cargamos las posiciones de todos los registros en
      // el vector correspondiente

      // El contador de registros se inicializa
      if (rank == 0)
    	{
	  j = 0;
	  for (i = 0; i < datos.n_s_lines; i++)
	    {
	      // Comenzamos a recorrer la linea de humo,
	      // tomamos el puntero hacia la cabecera
	      p_temp=datos.heads[i];

	      while (p_temp != NULL)
		{
		  for (k = 0; k < Pa->nsd; k++)
		    // Cargamos las coordenas del punto
		    { // donde se interpolara el campo en el tipo de var
		      // que requiere interp_local
		      posi_orig[j*Pa->nsd+k] = posi_veli_orig[j*Pa->nsd+k]
			= p_temp->coordenadas[k];
		    }
		  // Apuntamos al proximo registro en la linea de humo
		  p_temp = p_temp->p_siguiente;
		  // Incrementamos el contador de registros
		  j++;
		}
	    } // Fin del bucle en lineas de humo

	  rect_perio (posi_veli_orig, datos.n_registros, Pa->nsd, rl);
	}
      // Se pide la interpolacion de los puntos de todas las lineas de humo
      // El proceso cero la hace con datos, el resto solo para buscar
      // los puntos que este le pida.

      interp_global (Pa, posi_veli_orig, datos.n_registros, VAnt_Loc,
		     (*s_data)->nfields, 0, Pa->nsd, posi_veli_orig,
		     estadop);

      // Calculamos el paso predictor para todas las posiciones
      if (rank == 0)
	{	
	  for (i = 0; i < datos.n_registros; i++)
	    {
	      for (j = 0; j < Pa->nsd; j++) // Cargamos las coordenas del punto
		{ // donde se interpolara el campo en el tipo de var
		  // que requiere interp_local
		  point[j] = posi_orig[i*Pa->nsd+j];
		  veloc[j] = posi_veli_orig[i*Pa->nsd+j];
		}

	      // Predecimos la nuevas posiciones a partir de las
	      // posiciones originales y las velocidades en ellas

	      Integrador_1er_O (point, veloc, Pa->nsd, delta);
	      // Calculo del paso predictor
	      // Las posiciones predichas son almacenadas en
	      // el vector correspondiente
	      for (j = 0; j < Pa->nsd; j++)		
		{
		  posi_veli[i*Pa->nsd+j]=point[j];
		}
	    }

	  // 2-PASO CORRECTOR
	  rect_perio (posi_veli, datos.n_registros, Pa->nsd, rl);
    	}

      // Ahora en posi_veli tendremos las velocidades en
      // todos los puntos predichos
      interp_global (Pa, posi_veli, datos.n_registros, VAct_Loc,
		     (*s_data)->nfields, 0, Pa->nsd, posi_veli, estadoc);

      // Podemos ahora efectuar el paso corrector
      if (rank == 0)
    	{
	  for (i = 0; i < datos.n_registros; i++)
	    {
	      for (j = 0; j < Pa->nsd; j++) // Cargamos las coordenas del punto
		{ // donde se interpolara el campo en el tipo de var
		  // que requiere interp_local
		  point[j] = posi_orig[i*Pa->nsd+j];
		  veloc[j] = posi_veli_orig[i*Pa->nsd+j];
		  veloc2[j] = posi_veli[i*Pa->nsd+j];
		}

	      // Predecimos la nuevas posiciones a partir de
	      // las posiciones originales y las velocidades en ellas
	      Integrador_2do_O (point, veloc, veloc2, Pa->nsd, delta);
	      // Calculo del paso corrector

	      // Las posiciones corregidas son almacenadas en
	      // el vector correspondiente
	      for (j = 0; j < Pa->nsd; j++)		
		{
		  posi_veli[i*Pa->nsd+j]=point[j];
		}
	    }

	  // 3-ACTUALIZACION DE LISTAS	
	  // El contador de registros se inicializa
	  j=0;
	  for (i = 0; i < datos.n_s_lines; i++)
	    {
	      // Comenzamos a recorrer la linea de humo,
	      // tomamos el puntero hacia la cabecera
	      p_temp=datos.heads[i];
	      while (p_temp != NULL)
		{
		  for (k = 0; k < Pa->nsd; k++)	// Cargamos las coordenadas
		    // del punto
		    { //donde se interpolara el campo en el tipo de var
		      // que requiere interp_local
		      p_temp->coordenadas[k] = posi_veli[j*Pa->nsd+k];
		    }
		  
		  // Apuntamos al proximo registro en la linea de humo
		  p_temp = p_temp->p_siguiente;

		  //Incrementamos el contador de registros
		  j++;
		}
	    } // Fin del bucle en lineas de humo

	  // 4-LIMPIEZA DE REGISTROS 
	  // El contador de registros se inicializa
	  j=0;
	  for (i = 0; i < datos.n_s_lines; i++)
	    {
	      p_temp = datos.heads[i];
	      while (p_temp != NULL)
		{
		  if ((estadop[j]!=0) || (estadoc[j]!=0))
		    {
		      if (p_temp == datos.heads[i])
			{
			  // Extraccion de registro en el caso de
			  // que sea la cabeza
			  datos.heads[i] = p_temp->p_siguiente;
			  // La nueva cabecera es la posterior a la actual
			  datos.heads[i]->p_anterior = NULL;
			  // El anterior a la nueva cabecera es nulo
			  (datos.heads[i]->p_siguiente)->p_anterior =
			    datos.heads[i];
			  // El anterior del nuevo siguiente es heads
			  free(p_temp);
			  // Se libera memoria del registro eliminado	
			  // p_temp=datos.heads[i]->p_siguiente;
			  // El proximo registro a mirar es
			  // el siguiente a la cabecera
			  p_temp = datos.heads[i];
			  // Dejamos apuntando a la cabeza que es
			  // el siguiente del que se borro
			}
		      else
			{
			  if (p_temp == datos.tails[i])
			    {
			      // Extraccion de registro en el caso de
			      // que sea la cola
			      datos.tails[i] = p_temp->p_anterior;
			      datos.tails[i]->p_siguiente = NULL;
			      (p_temp->p_anterior)->p_siguiente =
				datos.tails[i];
			      free(p_temp);
			      p_temp = datos.tails[i];
			      // Dejamos apuntando a la cola que
			      // era el siguiente del que se borro
			    }
			  else
			    {
			      (p_temp->p_anterior)->p_siguiente =
				p_temp->p_siguiente;	
			      (p_temp->p_siguiente)->p_anterior =
				p_temp->p_anterior;		
			      p_libera = p_temp;
			      p_temp = p_temp->p_siguiente;
			      free(p_libera);	
			    } // Fin de limpieza de registro cualquiera
			} // Fin de limpieza de cola

		      // En todos los casos debe eliminarse
		      // un registro del total
		      datos.n_registros--;
		    } // Fin de limpieza de cabeza
		  else
		    {
		      p_temp=p_temp->p_siguiente;
		    }

		  // Incrementamos el contador de registros
		  j++;
		}
	    } // Fin del bucle en lineas de humo
	}
    }// Fin de bucle en delta fino

  if (rank == 0)
    {
      free(veloc);
      free(veloc2);

      free(posi_orig);
      free(posi_veli);
      free(posi_veli_orig);

      free(estadop);
      free(estadoc);

      // Se agrega una particula en la fuente de cada linea de humo si
      // se han dado los pasos de tiempo correspondientes
      datos.cont_step++; // Incrementa el contador de pasos de continuacion

      x = (double *)malloc(Pa->nsd*sizeof(double));
      dx = (double *)malloc(Pa->nsd*sizeof(double));

      if ((datos.cont_step % datos.sbrp)==0)
    	{
	  for (i = 0; i < datos.n_s_lines; i++)  // Bucle en lineas de humo
	    {
	      // Creamos un registro nuevo por linea de humo
	      // (una nueva cola, debe actualizarse tails).
	      p_temp = (struct t_nodo *)malloc(sizeof(struct t_nodo));
	      // Pedimos memoria para el vector de coordenadas del registro
	      p_temp->coordenadas = (double *)malloc(Pa->nsd*sizeof(double));

	      // Linkeamos los registros
	      p_temp->p_siguiente = NULL;
	      p_temp->p_anterior = datos.tails[i];
	      datos.tails[i]->p_siguiente = p_temp;
	      datos.tails[i] = p_temp;

	      // Aumentamos el contador de registros
	      datos.n_registros++;
	      for (j = 0; j < Pa->nsd; j++)
		{
		  datos.tails[i]->coordenadas[j] = 
		    datos.origin_s_lines[i*Pa->nsd+j];
		  x[j] = (datos.tails[i]->p_anterior)->coordenadas[j];
		  dx[j] = (x[j]-datos.tails[i]->coordenadas[j])/4.0;
		  // Se divide por n, se agregan n-1 particulas
		}

	      for (n = 0; n < 3; n++)
		// Bucle en cantidad de particulas extra insertadas
		{	
		  // Creamos un registro nuevo por linea de humo
		  // (una nueva cola, debe actualizarse tails).
		  p_temp = (struct t_nodo *)malloc(sizeof(struct t_nodo));

		  // Pedimos memoria para el vector de coordenadas del registro
		  p_temp->coordenadas = (double *)
		    malloc(Pa->nsd*sizeof(double));

		  // Linkeamos los registros
		  p_temp->p_siguiente = datos.tails[i];
		  p_temp->p_anterior = datos.tails[i]->p_anterior;
		  (datos.tails[i]->p_anterior)->p_siguiente = p_temp;
		  datos.tails[i]->p_anterior = p_temp;

		  // Aumentamos el contador de registros
		  datos.n_registros++;

		  for (j = 0; j < Pa->nsd; j++)
		    {
		      p_temp->coordenadas[j] = x[j] - dx[j] * (n+1);
		    }
		} // Fin de bucle en cantidad de particulas extra insertadas
	    } // Fin del bucle en lineas de humo
    	} // Fin de condicional de agregado de particulas

      // Agregado de particulas si la linea se estira
      free(x);
      free(dx);

      // Modulo para visualizacion correcta,
      // se puede anular si no hay problemas de visualizacion
      point_temp = (double *)malloc(Pa->nsd*sizeof(double));

      if (datos.heads[0]->p_anterior != datos.heads[0]->p_siguiente)
	// Checkeamos que la linea tenga al menos dos particulas
    	{  // Es igual en todas, puede verificarse cualquiera
	  for (i = 0; i < datos.n_s_lines; i++) // Bucle en lineas de humo
	    {
	      p_temp = datos.heads[i];
	      while (p_temp->p_siguiente != NULL)
		// Recorremos hasta el anteultimo registro
		{ // Calculo de la distancia entre particulas
		  dist = 0.0;
		  for (j = 0; j < Pa->nsd; j++)
		    {
		      dist = pow(p_temp->coordenadas[j]-
				 (p_temp->p_siguiente)->coordenadas[j], 2.0) +
			dist;
		    }

		  dist = sqrt(dist);

		  if (dist > 0.001) // 0.001
		    // Si dos particulas se alejan mas de lo debido se agregan
		    { // tantas como veces se haya superado esta distancia
		      for (j = 0; j < Pa->nsd; j++)
			// Cargamos posicion de la particula actual y
			// distancias con la proxima	
			{
			  point[j] = p_temp->coordenadas[j];
			  point_temp[j] = point[j] -
			    (p_temp->p_siguiente)->coordenadas[j];
			}
		      for (k = 1; k <= dist/0.001; k++)
			{
			  p_libera = (struct t_nodo *)
			    malloc(sizeof(struct t_nodo));
			  // p_libera aca no se usa para liberar
			  p_libera->coordenadas = (double *)
			    malloc(Pa->nsd*sizeof(double));
			  // Si para crear una particula nueva
			  // Se calculan las coordenadas del nuevo registro
			  for (j = 0; j < Pa->nsd; j++)
			    {
			      p_libera->coordenadas[j] = point[j] -
				point_temp[j]/((int)(dist/0.001)+1.0)*k;
			    }

			  // Linkeamos el nuevo registro con los existentes
			  p_libera->p_anterior = p_temp;
			  p_libera->p_siguiente = p_temp->p_siguiente;
			  (p_temp->p_siguiente)->p_anterior = p_libera;
			  p_temp->p_siguiente = p_libera;

			  // Aumentamos el contador de registros
			  datos.n_registros++;
			  // Apuntamos al proximo
			  p_temp = p_libera;
			}
		    } // Fin de condicional de distancia entre particulas

		  p_temp = p_temp->p_siguiente;
		} // Fin de bucle dentro de una linea de humo
	    } // Fin de bucle en lineas de humo
    	} // Fin de condicional de agregado por estiramiento

      free(point);
    }

  // Devolvemos los vectores de velocidades en el paso actual
  VecRestoreArray(lv, &VAct_Loc);
  VecGhostRestoreLocalForm(*fv, &lv);

  // Devolvemos los vectores de velocidades en el paso anterior
  VecRestoreArray(lva, &VAnt_Loc);
  VecGhostRestoreLocalForm((*s_data)->va, &lva);
  
} //Fin de lineashumointegrate_

void lineashumodump_ (systemdata **s_data, Vec *fv, double *rl)
{
  int i, j;
  int rank, nprocs;
  FILE *p_lineas_humo;
  struct t_nodo *p_temp;
  char nomArch[100];
  partition *Pa;

  Pa=(*s_data)->Pa;

  MPI_Comm_rank(Pa->Comm, &rank);
  MPI_Comm_size(Pa->Comm, &nprocs);

  if (datos.init_Ok != 1)
    {
      // printf("ERROR Sist. de calculo de lineas de humo no inicializado\n");
      return;
    }

  if (rank == 0)
    {
      for (i = 0; i < datos.n_s_lines; i++)
    	{
	  sprintf (nomArch, "lineashumo%d.dat", i+1);
	  p_lineas_humo = fopen(nomArch, "w");

	  // Cabecera del archivo
	  fprintf (p_lineas_humo,
		   "#Lineas de humo calculadas, formato GNUPlot\n");
	  fprintf (p_lineas_humo,
		   "# %d dimensiones\n", Pa->nsd);
	  p_temp = datos.tails[i];
	  // Comenzamos a recorrer a partir del ultimo registro de la lista i
	  // Su posicion en memoria se encuentra en tails.
	  while (p_temp != NULL)
	    {
	      for (j = 0; j < (Pa->nsd-1); j++)
		// Se imprimen todas las componentes menos la ultima
		{
		  fprintf (p_lineas_humo, "%g ", p_temp->coordenadas[j]);
		}

	      fprintf (p_lineas_humo, "%g\n", p_temp->coordenadas[j]);
	      //Se imprime el ultimo
	      p_temp = p_temp->p_anterior;
	    }

	  fclose(p_lineas_humo);
	}

      // Impresion de las lineas de humo en formato periodico,
      // se debe detectar que componente corregir antes de imprimir
      if (periodic_linea_h == 1)
	{
	  for (i = 0; i < datos.n_s_lines; i++)
	    {
	      sprintf (nomArch, "lineashumo_per%d.dat", i+1);
	      p_lineas_humo = fopen(nomArch, "w");

	      // Cabecera del archivo
	      fprintf (p_lineas_humo,
		       "#Lineas de humo calculadas, formato GNUPlot\n");
	      fprintf (p_lineas_humo,"# %d dimensiones\n", Pa->nsd);

	      p_temp=datos.tails[i];
	      // Comenzamos a recorrer a partir del ultimo registro
	      // de la lista i
	      // Su posicion en memoria se encuentra en tails.
	      while (p_temp != NULL)	
		{
		  for (j = 0; j < (Pa->nsd); j++)	
		    {
		      if (rl[j] > 0.0)
			{
			  fprintf(p_lineas_humo, "% g",
				  (p_temp->coordenadas[j]/rl[j] -
				   floor(p_temp->coordenadas[j]/rl[j]))*rl[j]);
			}
		      else
			{
			  fprintf(p_lineas_humo, "% g",
				  p_temp->coordenadas[j]);
			}
		    }
		  fprintf(p_lineas_humo,"\n");

		  p_temp = p_temp->p_anterior;
		}
	      fclose(p_lineas_humo);
	    }
	}
    }
} // Fin lineashumodump_ 

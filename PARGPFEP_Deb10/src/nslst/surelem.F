c -----------------------------------------------------------------------------
      subroutine surelem
     * (v, va, cogeo0, nvar, icomp, npe, nsd, ae, rhse, parcon, delcon, 
     *  parnum, parmat, pargen, ngau, kint, itypegeo, iel,
     *  nod, maxnpe, maxgau, wgp, isim, p, dpl, dp, dimrhse, Aflag, igr,
     *  rinte, nintgr,iswnob,base)
c -----------------------------------------------------------------------------
c
c input:
c v: vector with the data actualized to the last fractional step to 
c     construct the ae and rhse.
c va: vector with the data of the last continuation step to construct 
c     the ae and rhse.
c cogeo: nodes coordinates
c nvar: number of fields
c icomp(ivar): number of components of field "ivar".
c npe(mesh): number of nodes per element in mesh
c nsd: number of space dimensions.
c ae: elemental matrix
c rhse: elemental right hand side 
c parcon: continuation parameter.
c delcon: step of the continuation parameter
c parnum: vector of numerical parameters.
c parmat: vector of material parameters.
c pargen: vector of general parameters.
c ngau: number of gauss points
c kint(ivar): interpolation type of ivar. 
c itypegeo: interpolation number for geometry
c iel: number of the element in the local processor
c nod(ivar): number of nodes per element for each ivar
c maxnpe: maximum of npe
c maxgau: constant utilized for dimensioning some arrays
c wgp: weights of the gauss points
c isim: simmetry indicator.  1 if axisymmetric, 0 otherwise.
c p: interpolation functions values at gauss points
c dpl: derivatives of p in the reference element
c dp: array for contruct the derivatives of p at gauss points
c dimrhse: dimension of rhse
c Aflag: flag that indicates with 0 that ae has to be computed, and
c        with 1 that there is no need of recomputing that
c igr: group number of the element
c rinte: elemental contribution to each integral
c nintgr: number of integrals that will be done here
#define MAT_A_VARIABLE    0
#define MAT_A_INVARIABLE  1
#undef  __SFUNC__
#define __SFUNC__ "surelem  "


      implicit real*8 (a-h,o-z)
C---------------------------------------------------------------------
C CHECK THAT THE FOLLOWING DATA AGREE WIH YOUR PROBLEM
C---------------------------------------------------------------------
      parameter(maxcomp_=6, maxvar_=10)
C---------------------------------------------------------------------

      real*8      v, va, cogeo, cogeo0, ae, rhse, parcon, 
     *            delcon,parnum, parmat,
     *            wgp, p, dpl, rinte, pargen
      real*8      normal(3)
      integer*4   nvar, icomp, npe, nsd, ngau, kint, itypegeo, iel, nod,
     *            maxnpe, maxgau, isim, dimrhse, Aflag, nintgr

      dimension  listv(maxvar_), listva(maxvar_), listx(3)
  
      dimension var(maxcomp_,maxvar_)
      dimension vara(maxcomp_,maxvar_)
      dimension coopg (3), ae(dimrhse, dimrhse), cogeo(maxnpe,nsd)
      dimension cogeo0(maxnpe,nsd), rl(3)
      dimension icomp(*), npe(*), rhse(*), va(maxnpe, nvar, maxcomp_)
      dimension parmat(*), parnum(*), kint(*), nod(*), pargen(*)
      dimension v(maxnpe, nvar, maxcomp_)
      dimension wgp(*), p(maxnpe, maxgau, *), dpl(maxnpe, maxgau, 3, *)
      dimension rinte(*)
      dimension base(maxnpe,nsd,nsd)
C----------------------------------------------------------------------      
C INSERT HERE ANY DEFINITION OF ADDITIONAL VARIABLES OR ARRAYS

C----------------------------------------------------------------------      
      dimension vecaux(10)
C----------------------------------------------------------------------      
C CHECK THAT THE FOLLOWING DATA SENTENCES AGREE WITH YOUR PROBLEM
      data listv/1,2,3,0,0,0,0,0,0,0/
      data listva/0,0,0,0,0,0,0,0,0,0/
      data listx/1,2,3/

      nrhse = nsd + 1
C----------------------------------------------------------------------      

      nodgeo = npe(1)
c correction of geometric coordinates if periodic
      rl(1) = parnum(16)
      rl(2) = parnum(17)
      rl(3) = parnum(18)
      do idim=1,nsd
         do in=1,nodgeo
            cogeo(in,idim)=cogeo0(in,idim)
         end do
      end do
      do idim=1,nsd
       if (rl(idim).gt.0) then
         ancho=abs(cogeo(1,idim)-cogeo(2,idim))
         do in=3,nodgeo
            aux=abs(cogeo(1,idim)-cogeo(in,idim))
            if (aux.gt.ancho) ancho = aux
         end do
         if (ancho.gt.rl(idim)/2.) then
            do in=1,nodgeo
               if (cogeo(in,idim).lt.rl(idim)/2.) then
                  cogeo(in,idim)=cogeo(in,idim)+rl(idim)
               end if
            end do
         end if
       end if
      end do
c end correction

c make zero the ae and rhse

      if(Aflag.eq. MAT_A_VARIABLE) then
         do i=1,dimrhse
            do j=1,dimrhse
               ae(i,j) = 0.d0
            enddo
         enddo
      endif

      do i=1,dimrhse
         rhse(i) = 0.d0
      enddo
c make zero rinte
      do i=1,nintgr
         rinte(i)=0.0
      enddo
C----------------------------------------------------------------------
C INSERT HERE ANY CALCULUS BEFORE THE LOOP OVER THE GAUSS POINTS
C----------------------------------------------------------------------
C----------------------------------------------------------------------
C loop over gaussian points
      do 600 ipgau = 1,ngau
c
         call isosur (ipgau,nsd,nodgeo,itypegeo,iel,
     *        dpl,cogeo,nod,maxnpe,maxgau,normal,dets)
c
         dj = dets*wgp(ipgau)
c
c calculate switched on variables in listv() at gauss points.
c
      do iv = 1, nvar
c
         ivar = listv(iv)
         if (ivar .ne. 0) then
            itype = kint(ivar)
            do k = 1, icomp(ivar)
               var(k,ivar) = 0.0
            end do
            do k = 1, icomp(ivar)
               do jl = 1, nod(ivar)
                  var(k,ivar) = var(k,ivar) + v(jl,ivar,k)*
     *                 p(jl,ipgau,itype)
               end do
            end do
         end if
      end do
c
c calculate switched on variables in listva() at gauss points.
c
      do iv = 1, nvar
         ivar = listva(iv)
         if (ivar .ne. 0) then
            itype = kint(ivar)
            do k = 1, icomp(ivar)
               vara(k,ivar) = 0.0
            end do
            do k = 1, icomp(ivar)
               do jl = 1, nod(ivar)
                  vara(k,ivar) = vara(k,ivar) + va(jl,ivar,k)*
     *                 p(jl,ipgau,itype)
               end do
            end do
         end if
      end do
c
c calculate switched on coordinates in listx() at gauss points.
c
      do iv = 1, nsd
         icoo = listx(iv)
         if (icoo .ne. 0) then
            coopg(icoo) = 0
            do i = 1, nodgeo
               coopg(icoo) = coopg(icoo) +
     *              p(i,ipgau,itypegeo)*cogeo(i,icoo)
            end do
         end if
      end do
c----------------------------------------------------------------------
c INSERT HERE THE LINES TO CALCULATE ELEMENTARY MATRIX AND R.H.S.
c test: adding a uniform surface force to Navier-Stokes
      vecaux(1) = 0.
      vecaux(2) = 0.
      vecaux(3) = 0.
c
c First equation
      do in = 1, nod(1)
         pin = p(in,ipgau,kint(1))
         do ico = 1, icomp(1)
            ik=(in-1)*nrhse+ico
            rhse(ik)=rhse(ik)+vecaux(ico)*pin*dj
         end do
      end do
c----------------------------------------------------------------------
  600 continue   ! END LOOP OVER GAUSS POINTS
C----------------------------------------------------------------------
C INSERT HERE ANY INSTRUCTION YOU NEED JUST BEFORE LEAVING ROUTINE
c
C----------------------------------------------------------------------
      return
      end
c
C------------------------------------------------------------------------
      subroutine tradtyp (ieltyp,ieltyps)
C------------------------------------------------------------------------
      if (ieltyp.eq.1) then 
         stop 'error in surmat for 1D'
      else if (ieltyp.eq.2.or.ieltyp.eq.3) then
         ieltyps = 1
      else if (ieltyp.eq.4) then
         ieltyps = 2
      else if (ieltyp.eq.5) then
         ieltyps = 3
      else
         stop 'mistaken ieltyp in surmat'
      end if
      return
      end
c
C-------------------------------------------------------------------------
      subroutine tradcod(icodv,icods)
C-------------------------------------------------------------------------
      if (icodv.lt.20) then
         stop 'error in surmat for 1D'
      else if (icodv.eq.20.or.icodv.eq.30) then
         icods = 10
      else if (icodv.eq.22.or.icodv.eq.32) then
         icods = 12
      else if (icodv.eq.40) then
         icods = 20
      else if (icodv.eq.42) then
         icods = 22
      else if (icodv.eq.50) then
         icods = 30
      else if (icodv.eq.52) then
         icods = 32
      else
         stop 'mistaken itpcod in surmat'
      end if
      return
      end
c -----------------------------------------------------------------------------
      subroutine isosur (ipgau,nsd,nodgeo,intgeo,iel,
     *                   dpl,cogeo,nod,maxnpe,maxgau,normal,dets)
c -----------------------------------------------------------------------------
c calcula la normal y el determinante superficial en isoparametricos en
c el punto de integracion ipgau
c
      implicit real*8 (a-h,o-z)
      real*8 normal(3)
      dimension nod(*)
      dimension dpl(maxnpe,maxgau,3,*), cogeo(maxnpe,*)
      dimension t(3,3)
c
c transformation matrix (the transpose of F)
c
      do i=1,nsd-1
         do j=1,nsd
            t(i,j) = 0.
            do jl=1,nodgeo
               t(i,j) = t(i,j) + dpl(jl,ipgau,i,intgeo)*cogeo(jl,j)
            end do
         end do
      end do
c image of normal unit vector      
      if (nsd.eq.2) then
         normal(1)=-t(1,2)
         normal(2)=t(1,1)
         dets = sqrt(normal(1)**2 + normal(2)**2)
         if (dets.le.0.00000001) then
            write (6,*) 'surface determinant = ',dets,' element = ',iel
            stop
         end if
         normal(1)=normal(1)/dets
         normal(2)=normal(2)/dets
      else if (nsd.eq.3) then
         normal(1)=t(1,2)*t(2,3)-t(1,3)*t(2,2)
         normal(2)=t(1,3)*t(2,1)-t(1,1)*t(2,3)
         normal(3)=t(1,1)*t(2,2)-t(1,2)*t(2,1)
         dets = sqrt(normal(1)**2+normal(2)**2+normal(3)**2)
         if (dets.le.0.00000001) then
            write (6,*) 'surface determinant = ',dets,' element = ',iel
            stop
         end if
         normal(1)=normal(1)/dets
         normal(2)=normal(2)/dets
         normal(3)=normal(3)/dets
      else
         stop 'invalid nsd in isosur'
      end if
      return
      end
c -----------------------------------------------------------------------------
      subroutine isosurdp (nvar, kint, ipgau, nsd, nodgeo, intgeo, iel,
     *     dpl, dp, cogeo, nod, maxnpe, maxgau, normal, dets)
c -----------------------------------------------------------------------------
c calcula la normal y el determinante superficial en isoparametricos en
c el punto de integracion ipgau
c
      implicit real*8 (a-h, o-z)
      real*8 normal(3), t1(3), t2(3)
      dimension dpl(maxnpe,maxgau,3,*), dp(maxnpe,3,*), cogeo(maxnpe,*)
      dimension kint(*), nod(*)
      dimension t(3,3), t2D(3,3), ti2D(3,3)
c
c transformation matrix (the transpose of F)
c
      do i = 1, nsd-1
         do j = 1, nsd
            t(i,j) = 0.
            do jl = 1,nodgeo
               t(i,j) = t(i,j) + dpl(jl,ipgau,i,intgeo) * cogeo(jl,j)
            end do
         end do
      end do
c image of normal unit vector      
      if (nsd .eq. 2) then
         normal(1) = -t(1,2)
         normal(2) = t(1,1)
         dets = sqrt(normal(1)**2 + normal(2)**2)
         if (dets .le. 0.00000001) then
            write (6,*) 'surface determinant = ',dets,' element = ',iel
            stop
         end if
         normal(1) = normal(1) / dets
         normal(2) = normal(2) / dets
c     Tangent
         t1(1) = normal(2)
         t1(2) = -normal(1)
c     Transformation 1D
         t1D = t(1,1) * t1(1) + t(1,2) * t1(2)
         ti1D = 1.0D+00 / t1D
c     global first derivatives
         kxx = 0
         do ivar = 1, nvar
            itype = kint(ivar)
            if (itype .ne. kxx) then
               kxx = itype
               do i = 1, nod(ivar)
                  dp(i,1,itype) = 0.
                  dp(i,1,itype) = dp(i,1,itype) +
     #                 dpl(i,ipgau,1,itype) * ti1D
               end do
            end if
         end do
c
      else if (nsd .eq. 3) then
         normal(1) = t(1,2) * t(2,3) - t(1,3) * t(2,2)
         normal(2) = t(1,3) * t(2,1) - t(1,1) * t(2,3)
         normal(3) = t(1,1) * t(2,2) - t(1,2) * t(2,1)
         dets = sqrt(normal(1)**2 + normal(2)**2 + normal(3)**2)
         if (dets .le. 0.00000001) then
            write (6,*) 'surface determinant = ',dets,' element = ',iel
            stop
         end if
         normal(1) = normal(1) / dets
         normal(2) = normal(2) / dets
         normal(3) = normal(3) / dets
c
c     Compute tangents
c
         if (abs(normal(1)) .le. abs(normal(2)) .and.
     #           abs(normal(1)) .le. abs(normal(3))) then
c     t1 = n x (1,0,0)
            t1(1) = 0.0D+00
            t1(2) = normal(3)
            t1(3) = -normal(2)
         else if (abs(normal(2)) .le. abs(normal(1)) .and.
     #           abs(normal(2)) .le. abs(normal(3))) then
c     t1 = n x (0,1,0)
            t1(1) = -normal(3)
            t1(2) = 0.0D+00
            t1(3) = normal(1)
         else
c     t1 = n x (0,0,1)
            t1(1) = normal(2)
            t1(2) = -normal(1)
            t1(3) = 0.0D+00
         end if
         t1mod = sqrt(t1(1)**2 + t1(2)**2 + t1(3)**2)
         t1(1) = t1(1) / t1mod
         t1(2) = t1(2) / t1mod
         t1(3) = t1(3) / t1mod
c     t2 = n x t1
         t2(1) = normal(2) * t1(3) - normal(3) * t1(2)
         t2(2) = normal(3) * t1(1) - normal(1) * t1(3)
         t2(3) = normal(1) * t1(2) - normal(2) * t1(1)
c         |  t1 |
c     tm =|  t2 |  transforma a terna (t1, t2, n)
c         |  n  |
c     Multiplicamos el jacobiano por su inversa p/obtener la t2D:
c               |         |
c     t2D = J x | t1 t2 n |
c               |         |
c     (la direccion normal no interesa)
c
         do i = 1, 2
            t2D(i,1) = 0.0D+00
            do k = 1, 3
               t2D(i,1) = t2D(i,1) + t(i,k) * t1(k)
            end do
            t2D(i,2) = 0.0D+00
            do k = 1, 3
               t2D(i,2) = t2D(i,2) + t(i,k) * t2(k)
            end do
         end do
c     Inversa del Jacobiano en 2D
         call jacinv (t2D,2,ti2D,detj)
c     global first derivatives
         kxx = 0
         do ivar = 1, nvar
            itype = kint(ivar)
            if (itype .ne. kxx) then
               kxx = itype
               do i = 1, nod(ivar)
                  do j = 1, 2
                     dp(i,j,itype) = 0.
                     do k = 1, 2
                        dp(i,j,itype) = dp(i,j,itype) +
     #                       dpl(i,ipgau,k,itype)*ti2D(j,k)
                     end do
                  end do
               end do
            end if
         end do
      else
         stop 'invalid nsd in isosur'
      end if
      return
      end

c -----------------------------------------------------------------------------
      subroutine telem
     * (v, va, cogeo0, nvar, icomp, npe, nsd, ae, rhse, parcon, delcon, 
     *  parnum, parmat, pargen, ngau, kint, itypegeo, iel,
     *  nod, maxnpe, maxgau, wgp, isim, p, dpl, dp, dimrhse, Aflag, igr,
     *  rinte, nintgr,iswnob,base)
c -----------------------------------------------------------------------------
c
c input:
c v: vector with the data actualized to the last fractional step to 
c     construct the ae and rhse.
c va: vector with the data of the last continuation step to construct 
c     the ae and rhse.
c cogeo: nodes coordinates
c nvar: number of fields
c icomp(ivar): number of components of field "ivar".
c npe(mesh): number of nodes per element in mesh
c nsd: number of space dimensions.
c ae: elemental matrix
c rhse: elemental right hand side 
c parcon: continuation parameter.
c delcon: step of the continuation parameter
c parnum: vector of numerical parameters.
c parmat: vector of material parameters.
c pargen: vector of general parameters.
c ngau: number of gauss points
c kint(ivar): interpolation type of ivar. 
c itypegeo: interpolation number for geometry
c iel: number of the element in the local processor
c nod(ivar): number of nodes per element for each ivar
c maxnpe: maximum of npe
c maxgau: constant utilized for dimensioning some arrays
c wgp: weights of the gauss points
c isim: simmetry indicator.  1 if axisymmetric, 0 otherwise.
c p: interpolation functions values at gauss points
c dpl: derivatives of p in the reference element
c dp: array for contruct the derivatives of p at gauss points
c dimrhse: dimension of rhse
c Aflag: flag that indicates with 0 that ae has to be computed, and
c        with 1 that there is no need of recomputing that
C igr: element_group number of the element 
c rinte: elemental contribution to each integral
c nintgr: number of integrals that will be done here
#define MAT_A_VARIABLE    0
#define MAT_A_INVARIABLE  1
#define LUMPED            0
#define NOT_LUMPED        1
#undef  __SFUNC__
#define __SFUNC__ "kelem  "


      implicit real*8 (a-h,o-z)

C---------------------------------------------------------------------
C CHECK THAT THE FOLLOWING DATA AGREE WIH YOUR PROBLEM
C---------------------------------------------------------------------
      parameter(maxcomp_=10, maxvar_=18,maxtype_=2)
      parameter(one12=0.08333333333333333,one6=0.1666666666666)
C---------------------------------------------------------------------

      real*8      v, va, cogeo, ae, rhse, parcon, delcon,parnum, parmat,
     *            wgp, p, dpl, rinte, pargen
      logical iswmass
      integer*4   nvar, icomp, npe, nsd, ngau, kint, itypegeo, iel, nod,
     *            maxnpe, maxgau, isim, dimrhse, Aflag, nintgr

  
      dimension dp(maxnpe,3,maxtype_+1),var(maxcomp_,maxvar_), 
     *          dvar(maxcomp_,3,maxvar_),tm(3,3), tmi(3,3)
C      dimension vara(maxcomp_,maxvar_),dvara(maxcomp_,3,maxvar_)
      dimension coopg (3), ae(dimrhse, dimrhse), cogeo(maxnpe,nsd)
      dimension cogeo0(maxnpe,nsd), rl(3)
      dimension icomp(*), npe(*), rhse(*), va(maxnpe, nvar, maxcomp_)
      dimension parmat(*), parnum(*), kint(*), nod(*), pargen(*)
      dimension v(maxnpe, nvar, maxcomp_)
      dimension wgp(*), p(maxnpe, maxgau, *), dpl(maxnpe, maxgau, 3, *)
      dimension rinte(*)
C----------------------------------------------------------------------      
C INSERT HERE ANY DEFINITION OF ADDITIONAL VARIABLES OR ARRAYS

C----------------------------------------------------------------------      
      dimension veloc(3),proy(4)
C ,velconv(3),sigma(3),ggf(3)
      dimension bijmass(8,8),bijtri(9),bijtet(16)
C ,bijlump(8)
C      dimension diff(3,3),diff0(3,3),force(3)
C      dimension rhon(30),drhon(3)
      save konce
C      save listv,listd,listx
      save bijmass,iswmass
C----------------------------------------------------------------------      
C CHECK THAT THE FOLLOWING DATA SENTENCES AGREE WITH YOUR PROBLEM
      data konce/0/
      data bijtri/one6,one12,one12,one12,one6,one12,one12,
     *            one12,one6/
      data bijtet/0.1,0.05,0.05,0.05,0.05,0.1,0.05,0.05,
     *            0.05,0.05,0.1,0.05,0.05,0.05,0.05,0.1/
      data iswmass/.false./
      nodgeo = npe(1)

c correction of geometric coordinates if periodic
      rl(1) = parnum(16)
      rl(2) = parnum(17)
      rl(3) = parnum(18)
      do idim=1,nsd
         do in=1,nodgeo
            cogeo(in,idim)=cogeo0(in,idim)
         end do
      end do
      do idim=1,nsd
       if (rl(idim).gt.0) then
         ancho=abs(cogeo(1,idim)-cogeo(2,idim))
         do in=3,nodgeo
            aux=abs(cogeo(1,idim)-cogeo(in,idim))
            if (aux.gt.ancho) ancho = aux
         end do
         if (ancho.gt.rl(idim)/2.) then
            do in=1,nodgeo
               if (cogeo(in,idim).lt.rl(idim)/2.) then
                  cogeo(in,idim)=cogeo(in,idim)+rl(idim)
               end if
            end do
         end if
       end if
      end do
c end correction

      nvar0 = nvar
      if (nvar0.gt.maxvar_) nvar0=maxvar_
      if (konce.eq.0) then
         if (ngau.eq.1) then
            if (nsd.eq.2.and.npe(1).eq.3.and.isim.eq.0) then
               iswmass = .true.
               do ii=1,3
                  do jj=1,3
                     bijmass(ii,jj)=bijtri(3*(ii-1)+jj)
                  end do
               end do
            else if (nsd.eq.3.and.npe(1).eq.4) then
               iswmass = .true.
               do ii=1,4
                 do jj=1,4
                   bijmass(ii,jj)=bijtet(4*(ii-1)+jj)
                 end do
               end do
            else if (nsd.eq.2.and.npe(1).eq.3.and.(isim.eq.1.or.
     *               isim.eq.2).and.ngau.eq.1) then
             iswmass = .true.
             rprom=(cogeo(1,isim)+cogeo(2,isim)+cogeo(3,isim))
     *             *.333333333333
             do ii=1,3
              bijmass(ii,ii)=0.1d0+.06666666666666*cogeo(ii,isim)/rprom
             end do
             bijmass(1,2)=0.1d0-0.01666666666*cogeo(3,isim)/rprom
             bijmass(2,1)=bijmass(1,2)
             bijmass(1,3)=0.1d0-0.01666666666*cogeo(2,isim)/rprom
             bijmass(3,1)=bijmass(1,3)
             bijmass(2,3)=0.1d0-0.01666666666*cogeo(1,isim)/rprom
             bijmass(3,2)=bijmass(2,3)
            else
               stop 'ngau = 1, mass matrix not implemented'
            end if
         end if
      end if
      konce = 1      
C----------------------------------------------------------------------      

      nodgeo = npe(1)
c free surface - disconnection

c evaluate hmin
      hmin = 9999999.
      do jl=1,nodgeo
         do kl=jl+1,nodgeo
           dist = 0.
           do isd=1,nsd
              dist = dist + (cogeo(jl,isd)-cogeo(kl,isd))**2
           end do
           if (dist.lt.hmin) hmin = dist
         end do
      end do
      hmin = sqrt(hmin)
c
      rmul = parmat(2)
      rhol = parmat(1)
      rnul = rmul/rhol
c level set modification
c we first determine if the element is
c kmix = 1: liquid, kmix=0: interface, kmix = -1...
c in the elements that are fully air we put a very small
c diagonal contribution so as to make k=0 in those nodes
c that are fully surrounded by air without affecting the rest.
c verify that k is indeed field kvar and same for phi
      rlevel = parnum(12)
      kphi   = 4
      kvar   = 8
      kmix = 1
      if (rlevel.gt.0.and.rlevel.lt.100) then
        kmix = 99
        do kl=1,nodgeo
         phi = v(kl,kphi,1)
         if (phi.gt.rlevel.and.kmix.eq.99) then
          kmix = 1
         else if (phi.gt.rlevel.and.kmix.eq.-1) then
          kmix = 0
         end if
         if (phi.lt.rlevel.and.kmix.eq.99) then
          kmix = -1
         else if (phi.lt.rlevel.and.kmix.eq.1) then
          kmix = 0
         end if
        end do
      end if
      amat = 0.01*rnul
      if (nsd.eq.3) amat = amat*hmin
c elements fully air
      if (kmix.eq.-1) then
         do jl=1,nod(1)
           ik = jl
           do jk=1,dimrhse
            if (jk.ne.ik) then
             ae(ik,jk) = 0.d0
            else
             ae(ik,jk) = amat
            end if
           end do
           rhse(ik) =  -ae(ik,ik)*v(jl,kvar,1)
          end do
          goto 987
      end if
c make zero the ae and rhse
      if(Aflag.eq. MAT_A_VARIABLE) then
         do i=1,dimrhse
            do j=1,dimrhse
               ae(i,j) = 0.d0
            enddo
         enddo
      endif

      do i=1,dimrhse
         rhse(i) = 0.d0
      enddo
c make zero rinte
      do i=1,nintgr
         rinte(i)=0.0
      enddo
C----------------------------------------------------------------------
C INSERT HERE ANY CALCULUS BEFORE THE LOOP OVER THE GAUSS POINTS
C----------------------------------------------------------------------
C----------------------------------------------------------------------
C loop over gaussian points
      do ipgau = 1,ngau
      call isopar (nvar,kint,ipgau,nsd,nodgeo,itypegeo,iel,detj,
     *             dpl,dp,cogeo,tm,tmi,nod,maxnpe,maxgau)
      dj = detj*wgp(ipgau)
c if axisymmetric, calculate radius of gaussian point and modify dj
      if (isim.eq.1.or.isim.eq.2) then
	r = 0.
	do i = 1,nodgeo
	  r = r + p(i,ipgau,itypegeo)*cogeo(i,isim)
	end do
	IF (R.EQ.0) WRITE(6,*) 'R=0  IEL = ',IEL,' IPGAU = ',IPGAU
	djr = dj
	dj = dj*r
      end if
c calculate variables at gauss points.
c verify that you are not using vara or dvara, if you use them, uncomment
c the corresponding lines
      do ivar=1,nvar0
	itype = kint(ivar)
	do k=1,icomp(ivar)
	  var(k,ivar) = 0.0
c          vara(k,ivar) = 0.0
	end do
        do  k=1,icomp(ivar)
           do  jl=1,nod(ivar)
              var(k,ivar) = var(k,ivar) + v(jl,ivar,k)*
     *                                              p(jl,ipgau,itype)
c              vara(k,ivar) = vara(k,ivar) + va(jl,ivar,k)*
c     *                                              p(jl,ipgau,itype)
           enddo
        enddo 
      enddo
c calculate derivatives at gp 
      do ivar=1,nvar0
	    itype = kint(ivar)
	    do i=1,nsd 
	       do k=1,icomp(ivar)
		  dvar(k,i,ivar) = 0.0
c		  dvara(k,i,ivar) = 0.0
	       end do
	    end do
            do i=1,nsd
               do k=1,icomp(ivar)
                  do jl=1,nod(ivar)
		     dvar(k,i,ivar) = dvar(k,i,ivar) + 
     *                                v(jl,ivar,k)*dp(jl,i,itype)
c		     dvara(k,i,ivar) = dvara(k,i,ivar) + 
c     *                                va(jl,ivar,k)*dp(jl,i,itype)
                  end do
               end do
            end do
      end do
c calculate coordinates at gauss points.
      do icoo=1,nsd
	    coopg(icoo) = 0
	    do i = 1,nodgeo
	       coopg(icoo) = coopg(icoo) + 
     *                       p(i,ipgau,itypegeo)*cogeo(i,icoo)
	    end do
      end do
c----------------------------------------------------------------------
c INSERT HERE THE LINES TO CALCULATE ELEMENTARY MATRIX AND R.H.S.
c-----------------------temperature equations--------------------------
C k equation
c verify all parameters and definitions
      rho    = parmat(1)
      rmul   = parmat(2)
      rnul   = rmul/rho
      prandtl = parmat(8)
      theta  = parnum(10)
      upwf   = parnum(11)
      rnut   = 0.d0
      rkkk   = var(1,5)
      reps   = var(1,6)
!      if (reps.gt.0.and.rkkk.gt.0) then
!       rnut   = cmu*rkkk*rkkk/reps
!       if (rnut.lt.0.001*rnul) rnut = 0.001*rnul
!       if (rnut.gt.1.d05*rnul) rnut=1.d05*rnul
!      end if
!      difk   = rnul+rnut/sigk
      dift = rnul / prandtl
c The field u is variable number iu
      iu = 8
      kiu= kint(iu)
      sss= 0.0
      rk = dift
      if (coopg(1) .lt. -0.05 .or. coopg(1) .gt. 0.05) then
         ff = 0
      else if (coopg(2) .lt. -0.05 .or. coopg(2) .gt. 0.05) then
         ff = 0
      else
         ff = parmat(12)
      end if
      cdt = 1.0d0
c
c Transport velocity
      do idim=1,nsd
         veloc(idim)=var(idim,1)
      enddo
c level set modification
c interface elements have no source or velocity, and their time-derivative
c coefficient is set to zero (only diffusion allowed, combined to the previous
c treatment of fully-air elements the result is zero normal derivative.
      if (kmix.eq.0) then
       ff = 0.d0
       sss = 0.d0
       cdt = 1.d-8
       do isd=1,nsd
          veloc(idim) = 0.d0
       end do
      end if
c Evaluation of hache      
      vel = 0.
      do i=1,nsd
         vel = vel + veloc(i)**2
      end do
      vel = sqrt(vel)
      hache = 0.
      if (nsd.eq.2) then
         ar = detj
         if (npe(1).eq.3)ar = 0.5 * detj
         if (npe(1).eq.4)ar = 4. * detj
         hk2 = 4*ar/3.1415926
         hache = sqrt(hk2)
      else if (nsd.eq.3) then
         volu = 0.1666666666*detj
         hache = (6.*volu/3.1415926)**.333333333
      end if
c--------------------------------------------------------
c--------------------------------------------------------
      pecl = 0.
      alfa = 0.
      hacheu = 0.
      if (vel.ne.0) then
         do inod=1,nod(1)
            proy(inod) = 0.d0
            do idim=1,nsd
               proy(inod)=proy(inod)+cogeo(inod,idim)*veloc(idim)
            enddo
         enddo
         pmax = proy(1)
         pmin = proy(1)
         do inod=2,nod(1)
            if(proy(inod).gt.pmax) then
               pmax=proy(inod)
            else if(proy(inod).lt.pmin) then
               pmin=proy(inod)
            endif
         enddo
         hacheu = (pmax - pmin)/vel
         pecl = hacheu*vel / (2*rk)
         if (pecl .gt. 1) then
            alfa = 1.
         else if (pecl .le. 1.) then
            alfa = pecl
         end if
      end if            
      hache = alfa*hacheu + (1.-alfa)*hache
c Evaluation of tau
      tau = 0.
      if (upwf.gt.0.00001) then
         tau = 1./((4.*rk/hache+2.*vel)/hache+sss)
      end if
c veloc.grad u
      vgradua= 0.
      do idim=1,nsd
         vgradua= vgradua+ veloc(idim)*dvar(1,idim,iu)
      end do
c Residual
      residual = vgradua+sss*var(1,iu)-ff
c
      sgsf = 0.0
      do in=1,nod(iu)
         pin = p(in,ipgau,kiu)
c        Stabilization
         if (upwf.gt.0.0001) then
            ugp = 0.
            do idim=1,nsd
               ugp = ugp + veloc(idim)*dp(in,idim,kiu)
            end do
            per = upwf*tau*(ugp-sgsf*sss*pin)
         else
            per = 0.0
         end if 
C        END stabilization
         ik = in
         rhse(ik) = rhse(ik) - residual*(pin+per)*dj
c
         do idim=1,nsd
            rhse(ik) = rhse(ik) - rk*dp(in,idim,kiu)*dj
     *         *dvar(1,idim,iu)
         end do
         if (Aflag.eq.MAT_A_VARIABLE) then
            do jn=1,nod(iu)
               jk = jn
               aeresj=(theta*sss+cdt/delcon)*p(jn,ipgau,kiu)
               veldpj=0.
               do idim=1,nsd
                veldpj=veldpj+veloc(idim)*dp(jn,idim,kiu)
               end do
               aeresj = aeresj + theta*veldpj
               ae(ik,jk)=ae(ik,jk)+(pin+per)*dj*aeresj
               do idim=1,nsd
                  ae(ik,jk)=ae(ik,jk)+rk*dj*theta
     *              *dp(in,idim,kiu)*dp(jn,idim,kiu)
               end do
            end do
         end if
      end do
c----------------------------------------------------------------------
C     Integrals:
c      rinte(4) = rinte(4) + dj*var(1,8)
c----------------------------------------------------------------------
      end do   ! END LOOP OVER GAUSS POINTS

C----------------------------------------------------------------------
C INSERT HERE ANY INSTRUCTION YOU NEED JUST BEFORE LEAVING ROUTINE
C----------------------------------------------------------------------


       goto 987
c cilindro
       kmix = 99
       radio = parnum(7)
       do kl=1,nodgeo
        rr = (cogeo(kl,1)-1.)**2 + (cogeo(kl,2)-0.25)**2
        rr = sqrt(rr)
        if (rr.gt.radio.and.kmix.eq.99) then
         kmix = 1
        else if (rr.gt.radio.and.kmix.eq.-1) then
         kmix = 0
        end if
        if (rr.lt.radio.and.kmix.eq.99) then
         kmix = -1
        else if (rr.lt.radio.and.kmix.eq.1) then
         kmix = 0
        end if
       end do
c kmix = 1: outside cylinder, kmix=0: interface, kmix = -1...
c cilindro
      if (kmix.ne.1) then
         do i=1,dimrhse
            do j=1,dimrhse
               ae(i,j) = 0.d0
            enddo
         enddo
         do i=1,dimrhse
            rhse(i) = 0.d0
         enddo
      end if
      if (kmix.eq.-1) then
         do i=1,dimrhse
               ae(i,i) = 1.d0
         enddo
      end if

c
 987  continue

      return
      end

#!/bin/bash
#      -pgpfep_prtpartfile part8.sol \
#nohup mpirun --oversubscribe -np 8 --mca btl ^sm,tcp ../bin/gpmain_opt \
nohup mpirun --use-hwthread-cpus -np 4 --mca btl ^sm,tcp ../bin/gpmain_opt \
-ksp_max_it 900 -ksp_gmres_restart 150 \
-ksp_rtol 0.001 -ksp_atol 0.00000000001 \
-log_summary -log_info \
-ksp_monitor < /dev/null >& go-debian10-corei73770-4p-RAM32g-3.4GHz.log &

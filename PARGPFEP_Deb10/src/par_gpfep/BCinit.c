#include "BCinit.h"
#include "error.h"
#include <stdlib.h>

/*-------------------------------------------------------------------------
    Function BCinit - BC distribution
    Programmed by Adrian Lew - Started: 17/11/97 - Finished: 17/11/97

    This function must be called from FORTRAN. Its function is to distribute 
    the BC and copy them into the systemdata structure.
  
    Variables names:
     v: parallel vector
     bcccond: sequential vector (I/O buffer, before scattering) (r)
     bccvalue: sequential vector (I/O buffer, before scattering) (r)
     Sy: systemdata asociated
     Par2Seq: parallel to sequential scattering
     option: COND_ONLY, VAL_ONLY, BOTH depending what is necesary to update

Return values:
     NONE

Note: those variables that have sense only in the main processor, are marked
      with an (r) in the comments beside their declaration 
-----------------------------------------------------------------------*/
#undef __SFUNC__
#define __SFUNC__ "BCinit "

void BCinit(Vec *fv, Vec *fbcccond, Vec *fbccvalue, systemdata **fSy, 
	    VecScatter *fPar2Seq, int *fmode, int *foption)
{
  Vec            v;
  Vec            bcccond;
  Vec            bccvalue;
  VecScatter     Par2Seq;
  systemdata     *Sy;
  int            option;

  PetscScalar    *aux_vec1;           /* Auxiliar vector      */
  Vec            *aux_vec2;           /* Auxiliar array of parallel vectors */
  int            ierr;                /* error checking       */
  int            i;
  int            tmpint1;
  partition      *Pa;
  int            length;
  int            loclength;
  int            nfields;
  int            nnodes;
  int            mode;
  int            rank, size;
  MPI_Comm       Comm;

  //  Seq2Par  = *fSeq2Par;
  Par2Seq = *fPar2Seq;
  v        = *fv;
  bcccond  = *fbcccond;
  bccvalue = *fbccvalue;

  Sy       = *fSy;
  Pa       = Sy->Pa;
  nnodes   = Pa->nodtot;
  nfields  = Sy->nfields;
  length   = nnodes * nfields;
  loclength= nfields * (Pa->last - Pa->first);
  option   = *foption;
  mode     = *fmode;
  Comm     = (mode == 1 ? PETSC_COMM_WORLD : Pa->Comm);

  MPI_Comm_rank(Comm, &rank);
  MPI_Comm_size(Comm, &size);

  /* Beginning */
  if (rank == 0) {
    ierr = VecGetSize(bcccond, &tmpint1);   CHKERRABORT(PETSC_COMM_WORLD,ierr);
    if (tmpint1 != length)
      error(EXIT,"It seems that not all the boundary conditions are set");
    
    ierr = VecGetSize(bccvalue, &tmpint1);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
    if(tmpint1!=length)
      error(EXIT,"It seems that not all the boundary conditions are set");
  }

  ierr = MPI_Bcast (&option, 1, MPI_INT, 0, Comm ); 

  ierr = VecDuplicateVecs (v, 2, &aux_vec2);

  if (option == COND_ONLY || option == BOTH) {
    if (mode == 0 || rank == 0) {
      ierr = VecScatterBegin(Par2Seq, bcccond, aux_vec2[0], INSERT_VALUES, 
		   SCATTER_REVERSE);        CHKERRABORT(PETSC_COMM_WORLD,ierr);
      ierr = VecScatterEnd(Par2Seq, bcccond, aux_vec2[0], INSERT_VALUES,
    		   SCATTER_REVERSE);        CHKERRABORT(PETSC_COMM_WORLD,ierr);
    }
  }


  if (option == VAL_ONLY || option == BOTH) {
    if (mode == 0 || rank == 0) {
      ierr = VecScatterBegin(Par2Seq, bccvalue, aux_vec2[1], INSERT_VALUES,
		   SCATTER_REVERSE);        CHKERRABORT(PETSC_COMM_WORLD,ierr);
      ierr = VecScatterEnd(Par2Seq, bccvalue, aux_vec2[1], INSERT_VALUES,
    		   SCATTER_REVERSE);        CHKERRABORT(PETSC_COMM_WORLD,ierr);
    }
  }

  /* Now, if option=COND_ONLY  bccval is not actualized, if option=VAL_ONLY
     bcccond is not actualized, and if bcccond and bccvalue are NULL it means 
     this is the first time bcinit is called to initialize them */

  if (option == COND_ONLY || option == BOTH) {
    ierr = VecGetArray (aux_vec2[0],
			&aux_vec1);       CHKERRABORT(PETSC_COMM_WORLD,ierr);
    if (mode == 1)
        ierr = MPI_Bcast (aux_vec1, loclength, MPI_DOUBLE, 0, PETSC_COMM_WORLD);

    if (Sy->bcccond == (int *)NULL) {    
      Sy->bcccond = (int *) malloc(loclength*sizeof(int));
      if (Sy->bcccond == (int *)NULL)
	error(EXIT, "Not enough memory for Sy->bcccond in bcinit");
    }
    for (i = 0; i < loclength; i++)
      Sy->bcccond[i] = (int) aux_vec1[i];
    ierr = VecRestoreArray (aux_vec2[0],
			    &aux_vec1);   CHKERRABORT(PETSC_COMM_WORLD,ierr);
  }  
  
  if (option == VAL_ONLY || option == BOTH) {
    ierr = VecGetArray (aux_vec2[1],
			&aux_vec1);        CHKERRABORT(PETSC_COMM_WORLD,ierr);
    if (mode == 1)
        ierr = MPI_Bcast (aux_vec1, loclength, MPI_DOUBLE, 0, PETSC_COMM_WORLD);
    if (Sy->bccvalue == (PetscScalar *)NULL) {
      Sy->bccvalue = (PetscScalar *) malloc(loclength*sizeof(PetscScalar));
      if (Sy->bccvalue == (PetscScalar *)NULL)
	error(EXIT, "Not enough memory for Sy->bccvalue in bcinit");
    }
    for (i = 0; i < loclength; i++)
      Sy->bccvalue[i] = aux_vec1[i];
    ierr = VecRestoreArray(aux_vec2[1],
			   &aux_vec1);     CHKERRABORT(PETSC_COMM_WORLD,ierr);
  }

  ierr = VecDestroyVecs(2, &aux_vec2);     CHKERRABORT(PETSC_COMM_WORLD,ierr);
}

/*-------------------------------------------------------------------------
    Function BC2v - Inserts the BccValues on the fields vector
    Programmed by Adrian Lew - Started: 17/11/97 - Finished: 17/11/97

    This function must be called from FORTRAN. Its function is to insert on 
v, the parallel unknowns vector, the values of the BC. Each processor insert 
their owns.
  
Variables names:
     v: parallel vector
     Sy: systemdata asociated

Return values:
     NONE

Note: those variables that have sense only in the main processor, are marked
      with an (r) in the comments beside their declaration 
-----------------------------------------------------------------------*/
#undef __SFUNC__
#define __SFUNC__ "BC2v "

#if defined(PRE_2_0_24)
void BC2v(int *fv, systemdata **fSy)
{
#else
void BC2v(Vec *fv, systemdata **fSy)
{
#endif
  Vec            v;
  int            ierr;       /* error checking */
  int            i;          /* loop counter   */
  partition      *Pa;
  PetscScalar         *vecscan;   /* scanning vector */
  int            length;

  systemdata     *Sy;
  
  v = *fv;

  Sy      = *fSy;
  Pa      = Sy->Pa;
  length  = Sy->nfields * (Pa->last - Pa->first);

  ierr = VecGetArray(v, &vecscan);          CHKERRABORT(PETSC_COMM_WORLD,ierr);
 
  for(i=0; i<length; i++)
    if(Sy->bcccond[i]!=0)
      vecscan[i] = Sy->bccvalue[i];
  
  ierr = VecRestoreArray(v, &vecscan);      CHKERRABORT(PETSC_COMM_WORLD,ierr);
}

/*-------------------------------------------------------------------------
    Function nobinit - oblique nodes reading and distribution
    Programmed by Enzo Dari - Started: 99/03/30 - Finished: 

    This function must be called from FORTRAN. Its function is to read
    the oblique nodes information (switches and basis) and rearrange
    it according to each processor needs
  
Variables names:
     Sy: systemdata

Return values:
     NONE

Note: those variables that have sense only in the main processor, are marked
      with an (r) in the comments beside their declaration 
-----------------------------------------------------------------------*/
#undef __SFUNC__
#define __SFUNC__ "nobinit "

void nobinit(systemdata **fSy, int *fnumnob, int *fmpn, int **fperm)
{
  systemdata     *Sy;
  partition      *Pa;
  MPI_Comm       Comm;
  FILE *fp;
  int i, nnob, numnob, mpn, rank, size, nsd, lengbase, sizlnob, ierr, pgb;
  int *perm;
  PetscScalar *lnobp;
  Vec lnobseq;
  double daux;
  PetscScalar *gbase;
  IS ISseq, ISperm;
  VecScatter s2p_lnob;

  Sy     = *fSy;
  Pa     = Sy->Pa;
  Comm   = Pa->Comm;
  nsd    = Pa->nsd;
  numnob = *fnumnob;
  mpn    = *fmpn;
  perm   = *fperm;

  ierr = MPI_Bcast(&numnob, 1, MPI_INT, mpn, Comm);  
  *fnumnob = numnob;
  lengbase = numnob * nsd * nsd;

  MPI_Comm_rank(Comm, &rank);
  MPI_Comm_size(Comm, &size);

/* Create sequencial vector lnobseq (only in mpn), for reading lnob */
  sizlnob = (rank == mpn) ? Pa->nodtot : 0;
  ierr = VecCreateSeq(MPI_COMM_SELF,
		      sizlnob, &lnobseq); CHKERRABORT(PETSC_COMM_WORLD,ierr);

/* Create sequencial vector gbase (all processors) */
  VecCreateSeq(MPI_COMM_SELF,
	       lengbase, &Sy->gbase); CHKERRABORT(PETSC_COMM_WORLD,ierr);

/* Read file with oblique nodes information */
  if (mpn == rank) {
    fp = fopen ("oblique1.cfg","r");
    if (!fp)
      error(EXIT,"Error opening oblique1.cfg");
    fscanf(fp, "%d", &nnob);
    if (nnob != numnob)
      error(EXIT,"Error reading number of oblique nodes");

/* Reads lnob */
    ierr = VecGetArray (lnobseq, &lnobp); CHKERRABORT(PETSC_COMM_WORLD,ierr);
    for (i = 0; i < Pa->nodtot; i++) {
      ierr = fscanf (fp, "%d", &pgb);
      if (ierr != 1)
	error(EXIT,"Error reading lnob vector (oblique nodes)");
      lnobp[i] = (PetscScalar) pgb;
    }
    ierr = VecRestoreArray (lnobseq,
			    &lnobp);        CHKERRABORT(PETSC_COMM_WORLD,ierr);

/* Reads gbase */
    ierr = VecGetArray (Sy->gbase, &gbase); CHKERRABORT(PETSC_COMM_WORLD,ierr);
    for (i = 0; i < lengbase; i++) {
      fscanf (fp, "%lf", &daux);
      gbase[i] = (PetscScalar) daux;
    }
    ierr=VecRestoreArray(Sy->gbase,&gbase); CHKERRABORT(PETSC_COMM_WORLD,ierr);

    fclose(fp);
  }

/* Broadcast gbase */
  ierr = VecGetArray (Sy->gbase, &gbase); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = MPI_Bcast(gbase, lengbase, MPI_DOUBLE, mpn, Comm);
  ierr =VecRestoreArray(Sy->gbase, &gbase); CHKERRABORT(PETSC_COMM_WORLD,ierr);

/* Creates lnob vector (distributed) */
  /* ierr = VecCreateMPI (Comm, Pa->nodloc, Pa->nodtot, &(Sy->lnob)); */
  /* CHKERRABORT(PETSC_COMM_WORLD,ierr); */
  ierr = VecCreateGhost (Comm, Pa->nodloc, Pa->nodtot, Pa->nodghost, Pa->Glob,
			 &(Sy->lnob));
  CHKERRABORT(PETSC_COMM_WORLD,ierr);

/* Creates Index sets for scattering lnobseq -> lnob */
  ierr = ISCreateStride  (MPI_COMM_SELF, sizlnob, 0, 1, &ISseq);
  CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = ISCreateGeneral (MPI_COMM_SELF, sizlnob, perm,
			  PETSC_USE_POINTER, &ISperm);
  CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecScatterCreate (lnobseq, ISseq, Sy->lnob, ISperm, &s2p_lnob);
  CHKERRABORT(PETSC_COMM_WORLD,ierr);

/* Scatter lnobseq -> lnob */
  ierr = VecScatterBegin (s2p_lnob, lnobseq, Sy->lnob, INSERT_VALUES,
      SCATTER_FORWARD); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecScatterEnd (s2p_lnob, lnobseq, Sy->lnob, INSERT_VALUES,
      SCATTER_FORWARD); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  VecScatterDestroy (&s2p_lnob);
  ISDestroy (&ISseq);
  ISDestroy (&ISperm);
  VecDestroy (&lnobseq);

/* /\* Create lnobext vectors *\/ */
/*   ierr = VecCreateSeq (MPI_COMM_SELF, Pa->nodext, &(Sy->lnobext)); */
/*   CHKERRABORT(PETSC_COMM_WORLD,ierr); */

/* /\* Create Index sets for scattering lnob -> lnobext *\/ */
/*   ierr = ISCreateGeneral(MPI_COMM_SELF, Pa->nodext, Pa->Glob, */
/* 			 PETSC_USE_POINTER, &ISperm); */
/*   CHKERRABORT(PETSC_COMM_WORLD,ierr); */
/*   ierr = ISCreateStride (MPI_COMM_SELF, Pa->nodext, 0, 1, &ISseq); */
/*   CHKERRABORT(PETSC_COMM_WORLD,ierr); */
/*   ierr = VecScatterCreate (Sy->lnob, ISperm, Sy->lnobext, ISseq, &s2p_lnob); */
/*   CHKERRABORT(PETSC_COMM_WORLD,ierr); */

/* /\* Scatter lnob -> lnobext *\/ */
/*   ierr = VecScatterBegin (s2p_lnob, Sy->lnob, Sy->lnobext, INSERT_VALUES, */
/* 			  SCATTER_FORWARD); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
/*   ierr = VecScatterEnd (s2p_lnob, Sy->lnob, Sy->lnobext, INSERT_VALUES, */
/* 			SCATTER_FORWARD); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
/*   VecScatterDestroy (&s2p_lnob); */
/*   ISDestroy (&ISseq); */
/*   ISDestroy (&ISperm); */

  VecGhostUpdateBegin (Sy->lnob, INSERT_VALUES, SCATTER_FORWARD);
  VecGhostUpdateEnd (Sy->lnob, INSERT_VALUES, SCATTER_FORWARD);
}

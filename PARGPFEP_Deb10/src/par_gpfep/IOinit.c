#include "IOinit.h"
#include "error.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "f2c_string.h"
#include <zlib.h>

/*-------------------------------------------------------------------------
    Function IOinit - I/O contexts generation
    Programmed by Adrian Lew - Started: 17/11/97 - Finished: 17/11/97

    This function must be called from FORTRAN. Its function is to create
    the scatter contexts for loading and saving  parallel vectors similar to
    the unknowns vector.
  
    Variables names:
     v: parallel vector
     s: sequential vector (I/O buffer, before scattering) (r)
     Sy: systemdata asociated
     nfs: number of fields to save or load (r)
     infs: array of lenght nfs with the number of the fields to save or 
           load, beggining from  0. (r)
     perm: old to new numeration mapping array (r)
     Par2Seq: parallel to sequential gathering
     mode: 0: normal behaviour
           1: all processes hold all vectors

    Return values:
     NONE

     Note: those variables that have sense only in the main processor,
      are marked with an (r) in the comments beside their declaration 
-----------------------------------------------------------------------*/
#undef __SFUNC__   
#define __SFUNC__ "IOinit" 

void IOinit (Vec *fv, Vec *fs, systemdata **fSy, int *fnfs, int *infs, 
	     int **fperm, VecScatter *fSeq2Par, VecScatter *fPar2Seq,
	     int *fmode)
{
  Vec            v;
  Vec            s;
  VecScatter     Par2Seq;
  int            nnodes;
  int            nfields;
  int            nfs;

  IS             ISseq;              /* Sequential Index Set */
  IS             ISperm;             /* Parallel Index Set   */
  int            *aux_vec;           /* Auxiliar vector      */
  int            ncio;               /* Number of components to output */
  int            ierr;               /* error checking       */
  int            i, k;
  int            tmpint1;
  int            tmpint2;
  int            rank, size;
  systemdata     *Sy;
  partition      *Pa;
  int            mode;
  int            *perm;
  MPI_Comm       Comm;

  v = *fv;
  Sy       = *fSy;
  Pa       = Sy->Pa;
  nnodes   = Pa->nodtot;
  nfields  = Sy->nfields;
  nfs      = *fnfs;
  ncio     = nnodes * nfs;
  mode     = *fmode;
  perm     = *fperm;
  Comm     = (mode == 1 ? PETSC_COMM_WORLD : Pa->Comm);

  aux_vec  = (int *)NULL; /* to avoid the warning */

  MPI_Comm_rank(Comm, &rank);
  MPI_Comm_size(Comm, &size);

  /* Indices creation for input */
  /*   if (mode == 1 || rank == 0) */
  if (rank == 0)
    {
      aux_vec = (int *) malloc(ncio*sizeof(int));
      if  (aux_vec == (int *)NULL)
	error (EXIT, "Not enough memory for aux_vec");

      for (i = 0; i < nnodes; i++)
	{
	  tmpint1 = i*nfs;
	  tmpint2 = perm[i]*nfields;
	  for (k = 0; k < nfs; k++)
	    aux_vec[tmpint1+k] = tmpint2 + infs[k];
	}
    }
  else
    ncio=0;

  if (mode == 0 || rank == 0) /* in mode 1 only master needs to create s */
    { ierr = VecCreateSeq (MPI_COMM_SELF, ncio,
			   &s);          CHKERRABORT(PETSC_COMM_WORLD,ierr);}
  ierr = ISCreateStride (MPI_COMM_SELF, ncio,
			 0, 1, &ISseq);    CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = ISCreateGeneral (MPI_COMM_SELF, ncio, aux_vec, PETSC_USE_POINTER,
			  &ISperm);        CHKERRABORT(PETSC_COMM_WORLD,ierr); 

  if (mode == 0 || rank == 0) /* in mode 1 only master needs to create scatter */
    { ierr = VecScatterCreate (v, ISperm, s, ISseq,
			       &Par2Seq); CHKERRABORT(PETSC_COMM_WORLD,ierr);}
  *fPar2Seq = Par2Seq;
  *fs = s;

  ISDestroy(&ISperm);
  ISDestroy(&ISseq);

  /*  if (mode == 1 || rank == 0) */
  if (rank == 0)
    free(aux_vec);
}

/*-------------------------------------------------------------------------
    Function IOfunc - Input/Output handling
    Programmed by Adrian Lew - Started: 27/11/97 - Finished: 27/11/97

    This function must be called from FORTRAN. Its function is to read/write
the values of a sequential PETSc vector from a file. Only one processor has to
call her.
  
Variables names:
     filename: filename of the input file
     op: type of operation to do: "r", "w", "a"
     IOvec: vector to save or load
     num: auxiliar array.
          num[0]: the character lenght declaration in fortran of filename.
	  num[1]: the position since the beggining of the file to begin
	          reading or writing. If num[1]<0, it means writing at the
		  end of the file.

Return values:
	  num[2]: final position of reading or writing

-----------------------------------------------------------------------*/
#undef __SFUNC__   
#define __SFUNC__ "IOfunc" 

#if defined(PRE_2_0_24)
void IOfunc(char *ffilename, char *fop, int *fIOvec, double *num)
{
#else
void IOfunc(char *ffilename, char *fop, Vec *fIOvec, double *num)
{
#endif
  Vec       IOvec;

  char      *filename;
  char      *op;
  double    daux[41];
  
  int       ierr;
  int       i;
  PetscScalar    *IOvecScan;
  int       lenght;
/*  FILE      *data; */
  char      buffer[300]/* , *pbuf */;
  gzFile    zdata;
  int       j, n_in_line;

  if(num[0]<0 || num[0]>300)
    error(EXIT, "Bad filename lenght");

  IOvec = *fIOvec;
  ierr = VecGetSize(IOvec, &lenght);      CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGetArray(IOvec, &IOvecScan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);

  filename = (char *) malloc(num[0]*sizeof(char));
  if (filename == (char *)NULL)
    error(EXIT,"Not enough memory for filename");
  
  op = (char *) malloc(num[0]*sizeof(char));
  if (op       == (char *)NULL)
    error(EXIT,"Not enough memory for op");
  
  f2c_string(ffilename, filename, num[0]);
  f2c_string(fop, op, num[0]);

  /* Reading */
  if(!strncmp(op, "r",1)){
    zdata = gzopen(filename, "rb");
    if(zdata == (gzFile)NULL)
      error(EXIT, "Cannot open file for reading");

    if(gzseek(zdata, (int) num[1], SEEK_SET) == -1)
      error(EXIT, "There is a problem  with the file position indicator");

    i = 0;
    while (i < lenght)
      {
	gzgets (zdata, buffer, 300);
	/* pbuf = buffer; */
	n_in_line = sscanf(buffer,
			   "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf"
			   "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf"
			   "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf"
			   "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf",
			   daux, daux+1, daux+2, daux+3, daux+4,
			   daux+5, daux+6, daux+7, daux+8, daux+9,
			   daux+10, daux+11, daux+12, daux+13, daux+14,
			   daux+15, daux+16, daux+17, daux+18, daux+19,
			   daux+20, daux+21, daux+22, daux+23, daux+24,
			   daux+25, daux+26, daux+27, daux+28, daux+29,
			   daux+30, daux+31, daux+32, daux+33, daux+34,
			   daux+35, daux+36, daux+37, daux+38, daux+39,
			   daux+40);
	if (n_in_line > 40)
	  error(EXIT, "More than 40 doubles in a line");
	if (n_in_line == 0)
	  error(EXIT,"EOF reading vector");
	for (j = 0; j < n_in_line; j++)
	  {
	    IOvecScan[i] = daux[j];
	    i++;
	    if (i == lenght && j < n_in_line - 1)
	      error(EXIT, "Extra characters at end of vector");
	  }
      }
/*
    for(i=0; i<lenght; i++){
      if(fscanf(data,"%lf", &IOvecScan[i])==EOF){
	error(EXIT,"File ended before the complete filling of the input vector");
	break;
      }
    }
*/
    num[2] = (double) gztell(zdata);
    gzclose(zdata);
  }
  
  /* writing */
  else if(!strncmp(op, "w",1) || !strncmp(op,"a",1)){
    zdata = gzopen(filename, "w9");
    if(zdata == (gzFile)NULL)
      error(EXIT, "Cannot open file for writing");

    if(!strncmp(op,"a",1)){
      if(num[1]>=0)
	if(gzseek(zdata, (int) num[1], SEEK_SET)==-1)
	  error(EXIT, "There is a problem  with the file position indicator");
    }

    for(i=0; i<lenght; i++)
      gzprintf(zdata,"%g \n", IOvecScan[i]);

    num[2] = (double) gztell(zdata);
    gzclose(zdata);
  }

  else
    error(EXIT, "Unknown operation indicator");

  /* Finishing */
  ierr = VecRestoreArray(IOvec, &IOvecScan); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  free(filename);
  free(op);
}

/*-------------------------------------------------------------------------
    Function WriteRee - write restart file, gzip compressed format

    This function must be called from FORTRAN. Its function is to write
    the values of a sequential PETSc vector to a file.
    Only one processor has to call it.
    Also writes "headings and footnotes".

    Parameters:
     filename: name of the output file
     fnl: file name length as in Fortran declaration
     IOvec: vector to save
     nodtot: Total # of nodes in the mesh (Heading, checking value)
     parcon: Continuation parameter.      (Heading, info)

-----------------------------------------------------------------------*/
#undef __SFUNC__   
#define __SFUNC__ "WriteRee" 

void WriteRee(char *ffilename, int *fnl, Vec *fIOvec,
	      int *nodtot, double *parcon)
{
  Vec       IOvec;
  char *filename;
  gzFile zdata;
  PetscScalar    *IOvecScan;
  int       length, ierr, i;

  if(fnl[0]<0 || fnl[0]>300)
    error(EXIT, "Bad filename length");

  IOvec = *fIOvec;
  ierr = VecGetSize(IOvec, &length);       CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGetArray(IOvec, &IOvecScan);   CHKERRABORT(PETSC_COMM_WORLD,ierr);

  filename = (char *) malloc(fnl[0]*sizeof(char));
  if (filename == (char *)NULL)
    error (EXIT, "Not enough memory for filename");
  f2c_string(ffilename, filename, fnl[0]);

  zdata = gzopen(filename, "w9");
  if(zdata == (gzFile)NULL)
    error (EXIT, "Cannot open file for writing");
  gzprintf (zdata, "\n\n*NOD\n %d\n*SCALAR_FIELD\n", nodtot[0]);
  gzprintf (zdata, "Continuation parameter: %g\n<NONE>\n", parcon[0]);

  for (i = 0; i < length; i++)
    gzprintf (zdata, "%.12g\n", IOvecScan[i]);
/*    gzprintf (zdata, "%g\n", IOvecScan[i]);   */

  gzprintf (zdata, "*END\n");
  gzclose(zdata);
  ierr = VecRestoreArray(IOvec, &IOvecScan); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  free(filename);
}

#undef __SFUNC__
#define __SFUNC__ "WriteVisualCFD"
/*-------------------------------------------------------------------------
    Function WriteVisualCFD - write file for visualization, no compressed format

    This function must be called from FORTRAN. Its function is to write
    the values of a sequential PETSc vector to a file.
    Only one processor has to call it.
    Also writes "headings and footnotes".

    Parameters:
     filename: name of the output file
     fnl: file name length as in Fortran declaration
     IOvec: vector to save
     nodtot: Total # of nodes in the mesh (Heading, checking value)
     parcon: Continuation parameter.      (Heading, info)

-----------------------------------------------------------------------*/
void WriteVisualCFD(Vec *fIOvec, int *nodtot , double *parcon)
{
    Vec       IOvec;
  FILE *zdata;
  PetscScalar    *IOvecScan;
  int       length, ierr, i;
  char filename[] = "VisualCFD.txt";

  IOvec = *fIOvec;
  ierr = VecGetSize(IOvec, &length);       CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGetArray(IOvec, &IOvecScan);   CHKERRABORT(PETSC_COMM_WORLD,ierr);

  zdata = fopen(filename, "w");
  if(zdata == 0)
    error (EXIT, "Cannot open file for writing");
  fprintf (zdata, "\n\n*NOD\n %d\n*SCALAR_FIELD\n", nodtot[0]);
  fprintf (zdata, "Continuation parameter: %g\n<NONE>\n", parcon[0]);

  for (i = 0; i < length; i++)
    fprintf (zdata, "%.12g\n", IOvecScan[i]);
/*    gzprintf (zdata, "%g\n", IOvecScan[i]);   */

  fprintf (zdata, "*END\n");
  fclose(zdata);
  ierr = VecRestoreArray(IOvec, &IOvecScan); CHKERRABORT(PETSC_COMM_WORLD,ierr);
}

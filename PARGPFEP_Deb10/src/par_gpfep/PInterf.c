#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <zlib.h>
#include "mpi.h"
#include "petsc.h"
#include "petscmat.h"

#include "partition.h"
#include "error.h"


/* User function to change element groups */
void u_set_groups (int *ngroups, int **nnpe, int **nelempgrp, int *incids);

/* This is a simple list structure for avoiding the C++ routine */
struct SList_{
  struct SList_    *next;
  int               data;
};

typedef struct SList_ SList;

/* int METIS_WPartGraphRecursive(int *, int *, int *, int *, int *, int *, int *,  */
/* 			      int *, float *,  int *,int *, int *); */
/* int METIS_WPartGraphKway(int *, int *, int *, int *, int *, int *, int *,  */
/* 			 int *, float *, int *,int *, int *); */
int METIS_PartGraphRecursive(int *nvtxs, int *ncon, int *xadj, int *adjncy,
			     int *vwgt, int *vsize, int *adjwgt, int *nparts, 
			     double *tpwgts, double *ubvec,
			     int *options, int *objval, int *part){};
int METIS_PartGraphKway(int *nvtxs, int *ncon, int *xadj, int *adjncy,
			int *vwgt, int *vsize, int *adjwgt, int *nparts, 
			double *tpwgts, double *ubvec,
			int *options, int *objval, int *part){};

void BldAdjncy (int my_nnodes, int all_nnodes, int **xadj, int **adjncy,
		int nnpe, int nelems, int *elements,
		int my_startnode);

/*---------------------------------------------------------------------------
  Function MeshPartition
  Programmed by Adrian Lew - Started: 12/11/97 Ended: 12/11/97
	     
  Modified by EAD: 00-07

  Names of the variables:
      flagprt: 1: read partition from file, 2: Compute (use weights)
      weights: array with the relative size of each zone in which the mesh 
              has to be divided.
      size: length of weights, number of zones to create.
      nsd: number of spatial dimensions.
      nnodes: total number of nodes
      nnpe: number of nodes per element.(equal in all groups in this version)
      ngroups: number of groups
      nelempgrp: total number of elements per group
      coordinates: array with the coordinates of the nodes
      elements: array with the elements of the  mesh

  Returned values:
      edgecut: edgecut of the partition
      nnodes_part: array with the number of nodes in each zone
      partition_vec: array with the number of zone to which each node belongs.
      d_nz: array with the number of neighboring nodes of the same zone for
            each node. (including itself) (sparsity structure)
      nd_nz: array with the number of neighboring nodes of the other zones for
             each node.  (sparsity structure)

----------------------------------------------------------------------------*/
#undef __SFUNC__
#define __SFUNC__ "MeshPartition"

void MeshPartition(int prtflag, double *weights, int size,
		   int nsd, int nnodes, int nnpe, int ngroups,
		   int *nelempgrp, double *coordinates, int *elements,
		   int *edgecut, int **nnodes_part, int **partition_vec,
		   int **d_nz, int **nd_nz)
{
  int         *xadj;       /* Starting point of each node's  neighbors in 
                              adjcny*/
  int         *adjncy;     /* Neighbor nodes for each node  */
  int         *nn;         /* numbers of the current element  nodes */
  int         i, j, k, l;  /* Loop counters */
  int         iel;         /* element number */
  int         tmpint1;     /* tmp variable */
  int         tmpint2;     /* tmp variable */
  int         tmpint3;     /* tmp variable */
  int         *particion;  /* partition vector */
  int ncon = 1; /* Metis: number of constrains (vertex weights) */

  int         *vwgt;       /* vetices weights , NULL in this case */
  int         *adjwgt;     /* aristas weights , NULL in this case */
  /* int         numflag;     Numbering flag */
  /* int         wgtflag;     Weight flag    */
  int         options[5];  /* options array */
  double      *fweights;   /* array for copying the double weights array */

  SList       **ListArray; /* Array of lists, each one containing the neighbors
			      of one node */
  SList       *HandNext;   /* Pointer to the next "list element" for each node
			      of the current element */
  SList       *Hand;       /* Pointer to the actual element of the list */
  SList       **HandLast = (SList **) NULL;  /* Pointer to the field "next" */
                                  /* of the last element visited in the list*/
  SList       *HandNew;    /* new list element */

  /* Compute adjacency */
  xadj = (int *) malloc ((nnodes + 1) * sizeof(int));
  if (xadj == (int *) NULL)
    error (EXIT, "Not enough memory for xadj");

  ListArray = (SList **) malloc (nnodes * sizeof (SList*));
  if (ListArray == (SList **) NULL)
    error (EXIT, "Not enough memory for ListArray");

  nn = (int *) malloc (nnpe * sizeof(int));
  if (nn == (int *) NULL)
    error (EXIT, "Not enough memory for nn");
  
  for (i = 0; i < nnodes; i++)
    {
      xadj[i] = 0;
      ListArray[i] = (SList *) NULL;
    }
  xadj[nnodes] = 0;
    
  /* Finding the connectivities */

  iel     = 0;
  tmpint1 = 0;
  for (i = 0; i < ngroups; i++)
    {
      for (j = 0; j < nelempgrp[i]; j++, iel++)
	{
	  for (k = 0; k < nnpe; k++)
	    nn[k] = elements[tmpint1+k];
      
	  /* Order the node numbers (bubble algorithm)*/ 
	  for (k = 0; k < nnpe - 1; k++)
	    for (l = k + 1; l < nnpe; l++)
	      if (nn[k] > nn[l])
		{
		  tmpint2 = nn[k];
		  nn[k]   = nn[l];
		  nn[l]   = tmpint2;
		}

	  /* List  building */
	  for (k = 0; k < nnpe; k++)
	    {
	      HandNext = ListArray [nn[k] - 1];  /* Node numbers begins at 1 */
	      Hand     = (SList *) (&(ListArray[nn[k] - 1]));
	      for (l = 0; l < nnpe; l++)
		{
		  if (k != l)
		    {
		      tmpint3 = 1;
		      tmpint2 = 1;
		      while (HandNext != (SList *)NULL && tmpint2)
			{
			  HandLast = &(Hand->next);
			  Hand = HandNext;
			  if (Hand->data >= nn[l])
			    tmpint2=0;
			  HandNext = Hand->next;
			}
	    
		      /* Case 1: There is an element >= nnl */
		      if (tmpint2 == 0)
			{
			  /* See if nnl is not already one of nnk neighbors */
			  if (Hand->data != nn[l])
			    HandNext = Hand;
			  else 
			    tmpint3 = 0; /* There's nothing to insert */
			}

		      /* Case 2: All the elements are smaller than nnl */
		      /* This is the case of a NULL list               */
		      else
			HandLast = &(Hand->next);

		      /* Adding l  to k list */
		      if (tmpint3 == 1)
			{
			  HandNew = (SList *) malloc (sizeof (SList));
			  if (HandNew == (SList *) NULL)
			    error (EXIT, "Not enough memory for HandNew");
	      
			  (*(HandLast)) = HandNew;
			  HandNew->next = HandNext;
			  HandNew->data = nn[l];
			  xadj[nn[k]]++; /* Save the list lenght */

			  Hand = HandNew;
			}
		    }
		}
	    }
	  tmpint1 += nnpe;
	}
    }

  /* Building xadj and adjncy */
  for (i = 1; i <= nnodes; i++)
    xadj[i] += xadj[i-1];
  
  adjncy = (int *) malloc (xadj[nnodes] * sizeof(int));
  if (adjncy == (int *) NULL)
    error (EXIT, "Not enough memory for adjncy");

  tmpint1 = 0;
  for (i = 0; i < nnodes; i++)
    {
      Hand = ListArray[i];
      for (j = 0; j < (xadj[i + 1] - xadj[i]); j++, tmpint1++)
	{
	  adjncy[tmpint1] = Hand->data -  1; /* node numbering begins with 0 */
	  HandNew = Hand;   /* Here HandNew is used as an auxiliary variable */
	  Hand = Hand->next;
	  free(HandNew);
	}
    }

  free(ListArray);
  free(nn);

  particion = (int *) malloc (nnodes * sizeof(int));
  if (particion == (int *) NULL)
    error (EXIT, "Not enough memory for particion");

  if (prtflag == 2) {             
    /* Calling METIS */
    fweights = (double *) malloc(size * sizeof(double));
    if(fweights == (double *) NULL)
      error(EXIT,"Not enought memory to allocate");
  
    if (weights)
      for (i = 0; i < size; i++)
	fweights[i] = (double) weights[i];
    else
      for (i = 0; i < size; i++)
	fweights[i] = 1.0 / size;


    vwgt = (int *)NULL;
    adjwgt = (int *)NULL;
    /* wgtflag = 0; */
    /* numflag = 0; */
    options[0]=0;

    if (size == 1)
      {
	for (i = 0; i < nnodes; i++)
	  particion[i] = 0;
      }
    else if (size < 8)
/*      error (EXIT, "METIS_WPartGraphRecursive not in titan"); */ 
      METIS_PartGraphRecursive(&nnodes, &ncon, xadj, adjncy,
			       vwgt, NULL, adjwgt, &size, fweights,
			       NULL, options, edgecut, particion);
    else
/*      error (EXIT, "METIS_WPartGraphKway not in titan"); */ 
      METIS_PartGraphKway(&nnodes, &ncon, xadj, adjncy,
			   vwgt, NULL, adjwgt, &size, fweights,
			   NULL, options, edgecut, particion);
    free(fweights);
  }

  /* Reading the partition from a file  */
  if (prtflag == 1) {
    FILE *input_partition;
    char *buffer;
    int  ibuffer;
    PetscBool pt_flg;

    buffer = (char *) malloc (sizeof(char) * 300);
    if (buffer == (char *)NULL)
      error (EXIT, "Not enough memory for buffer");

    PetscOptionsGetString (NULL, "pgpfep_", "-partfromfile",
			   buffer, 299, &pt_flg);
    if ((input_partition = fopen (buffer, "r")) == NULL)
      error (EXIT, "Cannot open the partition input file");

    while (fscanf (input_partition, "%s", buffer)!= EOF && 
	   strncmp (buffer, "Nnodos:", 7));

    if (strncmp (buffer, "Nnodos:", 7) == 0)
      {
	fscanf (input_partition, "%d", &ibuffer);
	if (ibuffer != nnodes)
	  error (EXIT, "Bad number of nodes in the partition file");
      }
    else
      error (EXIT, "Maybe the partition file is a wrong one");

    fscanf (input_partition, "%s%d%s%d", buffer, &size, buffer, edgecut);

    for (i = 0; i < nnodes; i++) 
      if (fscanf (input_partition, "%d", &(particion[i])) == EOF)
	error (EXIT, "Not enough numbers in the partition file");    

    fclose (input_partition);
    free (buffer);
  }

  /* generation of arrays d_nz ,nd_nz, nnodes_part y particion_vec */
  (*d_nz) = (int *) malloc (sizeof(int) * nnodes);
  if ((*d_nz) == (int *)NULL)
    error (EXIT, "Not enough memory for d_nz");

  (*nd_nz) = (int *) malloc (sizeof(int) * nnodes);
  if ((*nd_nz) == (int *) NULL)
    error (EXIT, "Not enough memory for nd_nz");

  (*nnodes_part) = (int *) malloc (sizeof(int) * size);
  if ((*nnodes_part) == (int *) NULL)
    error (EXIT, "Not enough memory for nnodes_part");

  (*partition_vec) = (int *) malloc (sizeof(int) * nnodes);
  if ((*partition_vec) == (int *) NULL)
    error (EXIT, "Not enough memory for partition_vec");

  for (i = 0; i < size; i++)
    (*nnodes_part)[i] = 0;

  for (i = 0; i < nnodes; i++)
    {
      (*nd_nz)[i] = 0;
      (*d_nz)[i] = 1; /* Recordar que cada nodo esta conectado con si mismo */
      (*nnodes_part)[particion[i]]++;
      (*partition_vec)[i] = particion[i];
      for (j = xadj[i]; j < xadj[i + 1]; j++)
	{
	  if (particion[i] != particion[adjncy[j]])
	    (*nd_nz)[i]++;
	  else
	    (*d_nz)[i]++;
	}
    }

  free(xadj);
  free(adjncy);
  free(particion);
}

/*---------------------------------------------------------------------------
  Function ParMeshPartition
  Parallel mesh partitioning routine using Par-Metis
  Programmed by Enzo Dari - Started: 22/06/99

Variable names:
      size, rank: lenght of weights, number of zones to create. 
      coordinates: array with the coordinates of the nodes
      nnodes: total number of nodes
      elements: array with the elements of the  mesh
      ngroups: number of groups
      nelempgrp: total number of elements per group
      nsd: number of spatial dimensions.
      nnpe: number of nodes per element.(equal in all groups in this version)

Returned values:
      edgecut: edgecut of the partition
      partition_vec: array with the number of zone to which each node belongs.
      nnodes_part: array with the number of nodes in each zone
      d_nz: array with the number of neighboring nodes of the same zone for
            each node. (including itself) (sparsity structure)
      nd_nz: array with the number of neighboring nodes of the other zones for
             each node.  (sparsity structure)

----------------------------------------------------------------------------*/
#undef __SFUNC__
#define __SFUNC__ "ParMeshPartition"

void ParMeshPartition (MPI_Comm Comm, int size, int rank, int mpn,
                       int nsd, int nnodes, int nelems, int nnpe, int ngroups,
                       int *nelempgrp, double *coordinates, int *elements,
                       int *edgecut, int **nnodes_part, int **partition_vec,
                       int **d_nz, int **nd_nz)
{
/*  int         *fnnpe;       array with the number of nodes per element in 
                              each group */
/*  int         flag = 0;     flag used reading mesh */
/*  char        *oname;       Partition file */

  int         *xadj;       /* Starting point of each node's  neighbors in 
                              adjcny*/
  int         *adjncy;     /* Neighbor nodes for each node  */
  /*int         *nn;          numbers of the current element  nodes */
  int         i, j/*, k, l*/;  /* Loop counters */
  /*int         iel;          element number */
  int         tmpint1;     /* tmp variable */
  /*int         tmpint2;      tmp variable */
  /*int         tmpint3;      tmp variable */
  int         *particion;  /* partition vector (== partition_vec) */

  /* SList       **ListArray;  Array of lists, each one containing the neighbors
			      of one node */

  int     *local_nnodes, *local_startnode;
  int     /*nelems, found, */ierr;
  int     my_startnode, my_nnodes/*, row*/;
  int     *my_nd_nz, *my_d_nz;
  const PetscInt *my_part;
  /*  SList   *Lrow, *LrowPrev, *Laux; */

  Mat Adj;
  MatPartitioning MPart;
  IS Indsetpart;

  /* Initial partitioning of the adjacency matrix: */
  if (rank == mpn)
    {
      local_nnodes    = (int *) malloc(size * sizeof(int));
      local_startnode = (int *) malloc(size * sizeof(int));
      if (!local_nnodes || !local_startnode)
	error(EXIT,"Not enough memory for local_nnodes or local_startnode");
      tmpint1 = 0;
      for (i = 0; i < size; i++)
	{
	  local_startnode[i] = tmpint1;
	  local_nnodes[i] = nnodes / (double) size;
	  tmpint1 += local_nnodes[i];
	}
      local_nnodes[size-1] -= tmpint1 - nnodes;
    }
  else
    local_nnodes = local_startnode = (int *) NULL;

  ierr = MPI_Scatter (local_startnode, 1, MPI_INT, 
		      &my_startnode, 1, MPI_INT, mpn, Comm);
  ierr = MPI_Scatter (local_nnodes, 1, MPI_INT,
		      &my_nnodes, 1, MPI_INT, mpn, Comm);

  /*  Build adjacencies,
      only local files, preliminar sequential partition
  */
  BldAdjncy (my_nnodes, nnodes, &xadj, &adjncy, nnpe, nelems, elements,
	     my_startnode);

  particion = (int *) malloc (nnodes * sizeof(int));
  if (!particion)
    error (EXIT, "Not enough memory for particion");
  *partition_vec = particion;

  ierr = MatCreateMPIAdj (Comm, my_nnodes, nnodes, xadj, adjncy,
			  PETSC_NULL, &Adj);  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = MatPartitioningCreate (Comm, &MPart);  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = MatPartitioningSetAdjacency (MPart, Adj);  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = MatPartitioningSetFromOptions (MPart);  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = MatPartitioningApply (MPart, &Indsetpart);  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = MatPartitioningDestroy (&MPart);  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  /*    ierr = MatDestroy (Adj);  CHKERRABORT(PETSC_COMM_WORLD, ierr);   */
  /* Por ahora nos quedamos con la particion
     ierr = ISPartitioningToNumbering (Indsetpart, IS *isperm);
     CHKERRABORT(PETSC_COMM_WORLD, ierr);
  */

  ierr = ISGetIndices(Indsetpart, &my_part); CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = MPI_Gatherv((void *)my_part, my_nnodes, MPI_INT, particion, 
		     local_nnodes, local_startnode, MPI_INT, mpn, Comm);
  ierr = ISRestoreIndices(Indsetpart, &my_part); CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = ISDestroy (&Indsetpart);

  /* generacion de los arrays d_nz ,nd_nz, nnodes_part y partition_vec */
  if (rank == mpn)
    {
      (*d_nz) = (int *) malloc (sizeof(int) * nnodes);
      if (!(*d_nz))
	error (EXIT, "Not enough memory for d_nz");

      (*nd_nz) = (int *) malloc (sizeof(int) * nnodes);
      if (!(*nd_nz))
	error (EXIT, "Not enough memory for nd_nz");

      (*nnodes_part) = (int *) malloc (sizeof(int) * size);
      if (!(*nnodes_part))
	error (EXIT, "Not enough memory for nnodes_part");
      for (i = 0; i < size; i++)
	(*nnodes_part)[i] = 0;

      for (i = 0; i < nnodes; i++)
	(*nnodes_part)[particion[i]]++;
    }

  my_nd_nz = (int *) malloc (my_nnodes * sizeof(int));
  my_d_nz  = (int *) malloc (my_nnodes * sizeof(int));
  if (!my_nd_nz || !my_d_nz)
    error (EXIT, "Not enough memory for my_nd_nz or my_d_nz");
  ierr = MPI_Bcast (particion, nnodes, MPI_INT, mpn, Comm);

  *edgecut = 0;
  for (i = 0; i < my_nnodes; i++)
    {
      my_d_nz[i]  = 1;
      my_nd_nz[i] = 0;
      for (j = xadj[i]; j < xadj[i+1]; j++) {
	if (particion[i + my_startnode] == particion[adjncy[j]])
	  my_d_nz[i]++;
	else
	  {
	    my_nd_nz[i]++;
	    *edgecut += 1;
	  }
      }
    }
  MPI_Reduce(edgecut, &i, 1, MPI_INT, MPI_SUM, mpn, Comm);
  *edgecut = i/2;
/* 
  if (flagprt != 1)
    { xadj y adjncy usados por Adj */
      ierr = MatDestroy (&Adj);  CHKERRABORT(PETSC_COMM_WORLD, ierr);
/*    }
  else 
    {               PETSc no se encarga
      PetscFree(xadj);
      PetscFree(adjncy);
    }
  if (rank == mpn)
    free(fnnpe);
*/

      ierr = MPI_Gatherv (my_d_nz, my_nnodes, MPI_INT, *d_nz,
			  local_nnodes, local_startnode, MPI_INT, mpn, Comm);
  ierr = MPI_Gatherv (my_nd_nz, my_nnodes, MPI_INT, *nd_nz,
		      local_nnodes, local_startnode, MPI_INT, mpn, Comm);
  free(my_d_nz);
  free(my_nd_nz);
  /*
  if(flagprt != 1 && rank == mpn)
    SavePartition (output_name, *partition_vec, *nnodes, size, *edgecut);
  */

  if (rank == mpn)
    {
      free (local_nnodes);
      free (local_startnode);
    }
}

int esup2psup(int nnod, int gnnod, int startnode, int nnpe, int *inpoel,
	      int *esup1, int *esup2,
	      int *psup2, int **psup1,
	      int wown, int sort);

void BldAdjncy (int my_nnodes, int all_nnodes, int **xadj, int **adjncy,
		int nnpe, int nelems, int *elements,
		int my_startnode)
{
  int i, iel, k, iaux;
  int *iet, *jet;

  /* Compute transpose of the conectivity matrix */
  iet = (int *) malloc ((my_nnodes+2) * sizeof(int));
  if (iet == (int *) NULL)
    error (EXIT, "Not enough memory for iet");
  for (i = 0; i < my_nnodes+2; i++)
    iet[i] = 0;
  jet = (int *) malloc (nnpe*nelems * sizeof(int));
  if (jet == (int *) NULL)
    error (EXIT, "Not enough memory for jet");

  for (iel = 0; iel < nelems; iel++)
    {                 /* Loop over elements */
      for (k = 0; k < nnpe; k++)         /* Load elemental incidence in nn */
	{
	  iaux = elements[nnpe * iel + k] - 1;/* Node numbers start with 1 */
	  iaux -= my_startnode;
	  if (iaux < 0 || iaux >= my_nnodes)
	    continue;
	  /* contemos en iet, corrido en 2 */
	  iet[iaux+2]++;
	}
    }
  /* convertimos a punteros */
  for (i = 2; i <= my_nnodes; i++)
    iet[i] += iet[i-1];
  /* recorremos nuevamente, guardando */
  for (iel = 0; iel < nelems; iel++)
    {                 /* Loop over elements */
      for (k = 0; k < nnpe; k++)         /* Load elemental incidence in nn */
	{
	  iaux = elements[nnpe * iel + k]-1; /* Node numbers start with 1 */
	  iaux -= my_startnode;
	  if (iaux < 0 || iaux >= my_nnodes)
	    continue;
	  /* contemos en iet, corrido en 2 */
	  jet[iet[iaux+1]++] = iel;
	}
    }

  /* Compute Adjacency matrix, include nodes in other processes: */
  if (PetscMalloc ((my_nnodes + 2) * sizeof(int), xadj) != 0)
    error (EXIT, "Not enough memory for xadj");

  for (i = 0; i < my_nnodes+2; i++)
    (*xadj)[i] = 0;

  esup2psup (my_nnodes, all_nnodes, my_startnode, nnpe, elements, jet, iet,
	     *xadj, adjncy, 0, 1);

  free(iet);
  free(jet);
  /*
    if (PetscMalloc ((*xadj)[my_nnodes] * sizeof(int), adjncy) != 0)
      error (EXIT, "Not enough memory for adjncy");
  */
}

int esup2psup(int nnod, int g_nnod, int s_node, int nnpe, int *inpoel,
	      int *esup1, int *esup2,
	      int *psup2, int **psup1,
	      int wown, int sort)
{
  int k;
  long li, lj, ljnnpe, nod, tmp;
  int *psup2r1, *psup2r2;
  int *auxa=NULL;

  /* memory allocation
  psup2 = (long *) calloc ((nnod+2), sizeof(long)); */
  auxa = (int *) calloc (g_nnod, sizeof(int));
  if (auxa == (int *)NULL)
    error (EXIT, "Not enough memory for auxa");

  psup2r1 = psup2+1; psup2r2 = psup2+2;

  /* first loop over nodes: counting nods per node */
  for (li = 0; li < nnod; li++) {
    /* loop over elements surrounding node 'li' */
    for (lj = esup2[li]; lj < esup2[li+1]; lj++) {
      ljnnpe=esup1[lj]*nnpe;
      /* loop over nods of element esup1[lj] */
      for (k = 0; k < nnpe; k++) {
        nod = inpoel[ljnnpe+k] - 1;
        if ((nod != li+s_node)&&(auxa[nod]<=li)) { /* match */
          psup2r2[li]++;
          auxa[nod] = li+1;
        }
      }
    }
    if (wown) psup2r2[li]++;  /* counting own */
  }

  /* loop over psup2: overadding */
  for (li = 1; li < nnod+2; li++)
    psup2[li] += psup2[li-1];

  /* memory allocation */
  if (PetscMalloc ((psup2[nnod+1]) * sizeof(int), psup1) != 0)
    error (EXIT, "Not enough memory for psup");

  /* cleaning auxiliar vector */
  for (li = 0; li < g_nnod; li++)
    auxa[li]=0;

  /* second loop over nodes: psup1 creation */
  for (li = 0; li < nnod; li++) {
    /* loop over elements surrounding node 'li' */
    for (lj = esup2[li]; lj < esup2[li+1]; lj++) {
      ljnnpe=esup1[lj]*nnpe;
      /* loop over nods of element esup1[lj] */
      for (k = 0; k < nnpe; k++) {
        nod = inpoel[ljnnpe+k] - 1;
        if ((nod != li+s_node)&&(auxa[nod]<=li)) {/* match */
          (*psup1)[psup2r1[li]] = nod;
          psup2r1[li]++;
          auxa[nod] = li+1;
        }
      }
    }
    if (wown) { /* counting own */
      (*psup1)[psup2r1[li]] = li+s_node;
      psup2r1[li]++;
    }
  }

  if (sort) { /* sorting */
    /* loop over nodes */
    for (li = 0; li < nnod; li++) {
      /* loop over nodes surrounding node 'li': sorting step */
      for (lj = psup2[li]; lj < psup2[li+1]-1; lj++)
        if ((*psup1)[lj]>(*psup1)[lj+1]) { /* SWAP it */
          tmp = (*psup1)[lj];
          (*psup1)[lj] = (*psup1)[lj+1];
          (*psup1)[lj+1] = tmp;
          lj = psup2[li]-1;
        }
    }
  }

  free(auxa);

  return 0;
}


/*----------------------------------------------------------------------------
         Function SavePartition - Save the partition to file
	 Programmed by A.Lew a long, long time ago ....

	 This function saves to a file the partition of the mesh.

Variable Names:
      name: output filename
      partition: array with the zone each node belongs to.
      Nnodes: Number of nodes in the mesh
      Nparts: Number of parts in wich the partition is divided.
      edgecut: edgecut of the partition.

-----------------------------------------------------------------------------*/
#undef __SFUNC__
#define __SFUNC__ "SavePartition"

void SavePartition(char *name,int *particion,int Nnodes,int Nparts,
		   int  edgecut)
{
  FILE   *output;
  int    k;
  
  output=fopen(name,"w");
  fprintf(output,"\n\n*NOD\n%d\n*SCALAR_FIELD\nNnodos: %d Npartes: %d  Edge_cut: %d\n",Nnodes,Nnodes,Nparts,edgecut);
  for(k=0;k<Nnodes;k++)
    fprintf(output," %d\n",particion[k]);
  
  fprintf(output,"*END");
  fclose(output);
  return;
}  

/*-------------------------------------------------------------------------
          Function ReadMesh - Reads a mesh from a file 
	  Modified by Adrian Lew - Started: 12/11/97  Ended: 12/11/97
	  Modified again in 09/03/97

Variables names:
     input_name: mesh filename
     flag : flag for knowing waht to read. COORDINATES, CONNECTIVITIES, BOTH.

Returned values:
     nsd: number of spatial dimensions. Only returned when reading the 
          coordinates
     coordinates: array with the nodes coordinates
     nnodes: number of nodes in the mesh
     nnpe: number of nodes per element in each group
     elements: array with the mesh elements
     ngroups: number of groups of the  mesh
     nelempgrp: number of elements in each group 

Note:
     Remember the nodes numbers begin with 1 in the file and here it is used 
the numeration starting at 0.

---------------------------------------------------------------------------*/
#undef __SFUNC__
#define __SFUNC__ "ReadMesh"

#define MAXREAD 300
#ifdef PLAIN_TEXT_MESH_FILE
void  ReadMesh(char *input_name, int *nsd, double **coordinates, int *nnodes, 
	       int **nnpe, int **elements, int *ngroups, int **nelempgrp, 
	       int *flag)
{
  int     i, j, k;            /* counter variables */
  int     iel;                /* number of the element */
  int     aux, dummy;         /* temporary variable */
  int     skip;               /* Multiusage indicator */
  char    *car;               /* reading buffer */
  FILE    *input;    
  double  tmp;
  int found, ftarget, foundcoor, foundelem, foundincid, rewinded;

  car = (char *) malloc((MAXREAD+1)*sizeof(char));
  if(car==(char *)NULL)
    error(EXIT,"Not enough memory to allocate car");

  car[MAXREAD]='\0';
  if((input=fopen(input_name,"r"))==NULL) 
    error(EXIT, "Cannot open the mesh input file");

  rewinded = found = 0;
  ftarget = foundcoor = foundelem = foundincid = 0;
  if (*flag == COORDINATES)
    {
      ftarget = 1;
      foundelem = foundincid = 1;
      foundcoor = 0;
    }
  else if (*flag == CONNECTIVITIES)
    {
      ftarget = 2;
      foundelem = foundincid = 0;
      foundcoor = 1;
    }
  else if (*flag == BOTH)
    {
      ftarget = 3;
      foundelem = foundincid = foundcoor = 0;
    }

  while (fgets(car, MAXREAD, input) && found < ftarget)
    {
      if (!foundcoor && !strncmp(car,"*COORDINATES",12))
	{
	  found++;
	  foundcoor = 1;
	  fscanf(input,"%d",nnodes);

	  /* Finding nsd value .... */
	  i=ftell(input);
	  fscanf(input,"%c",car);
	  while(fscanf(input,"%c",car)!=EOF && car[0]!='\n');

	  j=ftell(input);
	  fseek(input, i-j, SEEK_CUR);

	  *nsd=-1;
	  while(fscanf(input,"%lf",&tmp)!=EOF && ftell(input)<j)
	    (*nsd)++;

	  fseek(input, i, SEEK_SET);

	  /* Reading nodes coordinates */
	  (*coordinates) = (double *) 
	                   malloc((*nnodes) * (*nsd) * sizeof(double));
	  if((*coordinates) == (double *)NULL)
	    error(EXIT,"Not enough memory to allocate coords");
    
	  for(i=0; i<(*nnodes); i++)
	    {
	      fscanf(input,"%d",&aux);      /* Throw the node number */
	      aux = i*(*nsd);
	      for(j=0; j<(*nsd); j++)
		fscanf(input,"%lf",&(*coordinates)[aux+j]);
	    }
	}
      if (!foundelem && !strncmp(car,"*ELEMENT_GROUP",14))
	{
	  found++;
	  foundelem = 1;
	  fscanf(input,"%d",ngroups);

	  *nelempgrp = (int *) malloc(*ngroups * sizeof(int));
	  if(*nelempgrp == (int *) NULL)
	    error(EXIT,"Not enough memory to allocate nelemgrp");

	  *nnpe = (int *) malloc(*ngroups * sizeof(int));
	  if(*nnpe == (int *) NULL)
	    error(EXIT,"Not enough memory to allocate nnpe");

	  for (i = 0; i < *ngroups; i++)
	    {
	      fscanf(input,"%d %d %s",&j, &((*nelempgrp)[i]), car);
      
	      /* Find correct value of nnpe for each group */
	      if(!strcmp(car,"Tri3"))
		(*nnpe)[i]=3;
	      else if(!strcmp(car,"Cuad4"))
		(*nnpe)[i]=4;
	      else if(!strcmp(car,"Quad4"))
		(*nnpe)[i]=4;
	      else if(!strcmp(car,"LINEAR_TETRAHEDRUM"))
		(*nnpe)[i]=4;
	      else if(!strcmp(car,"Tetra4"))
		(*nnpe)[i]=4;    
	      else if(!strcmp(car,"Tetra10"))
		(*nnpe)[i]=10;    
	      else if(!strcmp(car,"Tri6"))
		(*nnpe)[i]=6;    
	      else if(!strcmp(car,"Hexah8"))
		(*nnpe)[i]=8;    
	      else if(!strcmp(car,"Seg2"))
		(*nnpe)[i]=2;
	    }
	}
      if (!foundincid && foundelem && !strncmp(car,"*INCIDENCE",10))
	{
	  found++;
	  foundincid = 1;

	  /* Reading the incidences  */
	  fscanf(input,"%s",car); 
	  if (strncmp ("<NONE>", car, 6) == 0)
	    skip=0;
	  else if (!strncmp ("<IGNORE>", car, 8) ||
		   !strncmp ("<LOCAL>",  car, 7) || 
		   !strncmp ("<GLOBAL>", car, 8))
	    skip=1;
	  else
	    {
	      skip=0;
	      fseek(input,-strlen(car)-1,SEEK_CUR);
	    }

	  aux = 0;
	  for (i = 0; i < *ngroups; i++)
	    aux += (*nnpe)[i] * (*nelempgrp)[i];

	  if (PetscMalloc(aux * sizeof(int), elements) !=0)
	    error(EXIT,"Not enough memory to allocate elements");

	  iel = 0;
	  aux = 0;
	  for(i=0; i<*ngroups; i++)
	    {
	      for(j=0; j<(*nelempgrp)[i]; j++, iel++)
		{
		  if(skip)
		    fscanf(input,"%d",&dummy);   /* Throw the element number */
      	
		  for(k=0; k<(*nnpe)[i]; k++)
		    fscanf(input,"%d",&(*elements)[aux+k]);
		  aux += (*nnpe)[i];
		}
	    }
	}
      if (!strncmp(car,"*END",4))
	{
	  if (!foundcoor || !foundelem)
	    error (EXIT, "Insufficient data in mesh file");
	  if (!rewinded)
	    rewind(input);
	  else
	    error (EXIT, "Insufficient data in mesh file");
	  rewinded = 1;
	}
    }

  fclose(input);
  free(car);
}
#else
/* gzscanfd:
   auxiliar function, reads doubles from a compressed file.
   uses gzgets and sscanf.
   buf: buffer for line
   sizbuf: size of buf
   zfp: pointer to compressed file
   retval: return value
   flag: 1: init: reads line, do the sscanf and returns
         2: init: line loaded, do the sscanf and returns
       other: returns next double value, increment internal counter,
             reads new line and perform sscanf if needed
*/
int gzscanfd (char *buf, int sizbuf, gzFile zfp, double *retval, int flag)
{
  char *cp;
  static int indx, n;
  static double daux[41];
  if (flag == 1) /* lee l�nea */
    {
      cp = gzgets (zfp, buf, sizbuf);
      if (!cp) return 1;
      flag = 2;
    }
  if (flag == 2) /* lee doubles desde l�nea */
    {
      indx = 0;
      n = sscanf (buf,
		  "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf"
		  "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf"
		  "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf"
		  "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf",
		  daux, daux+1, daux+2, daux+3, daux+4,
		  daux+5, daux+6, daux+7, daux+8, daux+9,
		  daux+10, daux+11, daux+12, daux+13, daux+14,
		  daux+15, daux+16, daux+17, daux+18, daux+19,
		  daux+20, daux+21, daux+22, daux+23, daux+24,
		  daux+25, daux+26, daux+27, daux+28, daux+29,
		  daux+30, daux+31, daux+32, daux+33, daux+34,
		  daux+35, daux+36, daux+37, daux+38, daux+39,
		  daux+40);
      if (n > 40)
	  error (EXIT, "More than 40 numbers in a line");
      return 0;
    }
  if (indx >= n)  /* read new line */
    {
      indx = n = 0;
      while (n <= 0)
	{
	  cp = gzgets (zfp, buf, sizbuf);
	  if (!cp) return 1;
	  n = sscanf (buf,
		      "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf"
		      "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf"
		      "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf"
		      "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf",
		      daux, daux+1, daux+2, daux+3, daux+4,
		      daux+5, daux+6, daux+7, daux+8, daux+9,
		      daux+10, daux+11, daux+12, daux+13, daux+14,
		      daux+15, daux+16, daux+17, daux+18, daux+19,
		      daux+20, daux+21, daux+22, daux+23, daux+24,
		      daux+25, daux+26, daux+27, daux+28, daux+29,
		      daux+30, daux+31, daux+32, daux+33, daux+34,
		      daux+35, daux+36, daux+37, daux+38, daux+39,
		      daux+40);
	  if (n > 40)
	    error (EXIT, "More than 40 numbers in a line");
	}
    }
  *retval = daux[indx++];
  return 0;
}
void  ReadMesh(char *input_name, int *nsd, double **coordinates, int *nnodes, 
	       int **nnpe, int **elements, int *ngroups, int **nelempgrp, 
	       int *flag)
{
  int     i, j, k;            /* counter variables */
  int     iel;                /* number of the element */
  int     aux;       /*, dummy;  temporary variable */
  int     skip;               /* Multiusage indicator */
  char    *car;               /* reading buffer */
  char eltype[50];
  gzFile  input;    
  double  tmp, xx, yy, zz;
  int found, ftarget, foundcoor, foundelem, foundincid, rewinded;
  /*  int n_in_line, i_in_line, iaux[41]; */

  car = (char *) malloc((MAXREAD+1)*sizeof(char));
  if(car==(char *)NULL)
    error(EXIT,"Not enough memory to allocate car");

  car[MAXREAD]='\0';
  if((input=gzopen(input_name,"r"))==NULL) 
    error(EXIT, "Cannot open the mesh input file");

  rewinded = found = 0;
  ftarget = foundcoor = foundelem = foundincid = 0;
  if (*flag == COORDINATES)
    {
      ftarget = 1;
      foundelem = foundincid = 1;
      foundcoor = 0;
    }
  else if (*flag == CONNECTIVITIES)
    {
      ftarget = 2;
      foundelem = foundincid = 0;
      foundcoor = 1;
    }
  else if (*flag == BOTH)
    {
      ftarget = 3;
      foundelem = foundincid = foundcoor = 0;
    }

  while (gzgets(input, car, MAXREAD) && found < ftarget)
    {
      if (!foundcoor && !strncmp(car,"*COORDINATES",12))
	{
	  found++;
	  foundcoor = 1;
	  gzgets(input, car, MAXREAD);
	  sscanf(car,"%d",nnodes);

	  /* Finding nsd value: read line */
	  gzgets (input, car, MAXREAD);
	  *nsd = sscanf (car, "%d %lf %lf %lf", &aux, &xx, &yy, &zz) - 1;

	  /* Reading nodes coordinates */
	  (*coordinates) = (double *) 
	                   malloc ((*nnodes) * (*nsd) * sizeof(double));
	  if ((*coordinates) == (double *)NULL)
	    error (EXIT, "Not enough memory to allocate coords");

	  (*coordinates)[0] = xx;
	  if (*nsd > 1) (*coordinates)[1] = yy;
	  if (*nsd > 2) (*coordinates)[2] = zz;

	  for (i = 1, aux = *nsd; i < (*nnodes); i++)
	    {
	      gzgets (input, car, MAXREAD);
	      if (*nsd == 1)
		sscanf (car, "%d %lf", &j, &(*coordinates)[aux]);
	      else if (*nsd == 2)
		sscanf (car, "%d %lf %lf", &j,
			&(*coordinates)[aux], &(*coordinates)[aux+1]);
	      else if (*nsd == 3)
		sscanf (car, "%d %lf %lf %lf", &j, &(*coordinates)[aux],
			&(*coordinates)[aux+1], &(*coordinates)[aux+2]);
	      aux += *nsd;
	    }
	}
      if (!foundelem && !strncmp (car, "*ELEMENT_GROUP", 14))
	{
	  found++;
	  foundelem = 1;
	  gzgets (input, car, MAXREAD);
	  sscanf (car, "%d", ngroups);

	  *nelempgrp = (int *) malloc (*ngroups * sizeof(int));
	  if (*nelempgrp == (int *) NULL)
	    error (EXIT, "Not enough memory to allocate nelemgrp");

	  *nnpe = (int *) malloc (*ngroups * sizeof(int));
	  if (*nnpe == (int *) NULL)
	    error (EXIT, "Not enough memory to allocate nnpe");

	  for (i = 0; i < *ngroups; i++)
	    {
	      gzgets (input, car, MAXREAD);
	      sscanf (car, "%d %d %s", &j, &((*nelempgrp)[i]), eltype);

	      /* Find correct value of nnpe for each group */
	      if      (!strcmp (eltype, "Tri3"))
		(*nnpe)[i] = 3;
	      else if (!strcmp (eltype, "Cuad4"))
		(*nnpe)[i] = 4;
	      else if (!strcmp (eltype, "Quad4"))
		(*nnpe)[i] = 4;
	      else if (!strcmp (eltype, "LINEAR_TETRAHEDRUM"))
		(*nnpe)[i] = 4;
	      else if (!strcmp (eltype, "Tetra4"))
		(*nnpe)[i] = 4;
	      else if (!strcmp (eltype, "Tetra10"))
		(*nnpe)[i] = 10;
	      else if (!strcmp (eltype, "Tri6"))
		(*nnpe)[i] = 6;
	      else if (!strcmp (eltype, "Hexah8"))
		(*nnpe)[i] = 8;
	      else if (!strcmp (eltype, "Seg2"))
		(*nnpe)[i] = 2;
	    }
	}
      if (!foundincid && foundelem && !strncmp (car,"*INCIDENCE",10))
	{
	  found++;
	  foundincid = 1;

	  /* Reading the incidences  */
	  gzgets (input, car, MAXREAD);
	  if (strncmp ("<NONE>", car, 6) == 0)
	    {
	      skip=0;
	      gzgets (input, car, MAXREAD);
	    }
	  else if (!strncmp ("<IGNORE>", car, 8) ||
		   !strncmp ("<LOCAL>",  car, 7) ||
		   !strncmp ("<GLOBAL>", car, 8))
	    {
	      skip=1;
	      gzgets (input, car, MAXREAD);
	    }
	  else
	    skip=0;
/*
printf("\Reading mesh, ngroups: %d\n nnpe, nelempgrp:", *ngroups);
for (i = 0; i < *ngroups; i++)
 printf("\n  %d, %d", (*nnpe)[i], (*nelempgrp)[i]);
printf("\n");
*/

	  aux = 0;
	  for (i = 0; i < *ngroups; i++)
	    aux += (*nnpe)[i] * (*nelempgrp)[i];

	  if (PetscMalloc (aux * sizeof(int), elements) != 0)
	    error (EXIT, "Not enough memory to allocate elements");

	  gzscanfd (car, MAXREAD, input, &tmp, 2);
	  iel = 0;
	  aux = 0;
	  for (i = 0; i < *ngroups; i++)
	    {
	      for (j = 0; j < (*nelempgrp)[i]; j++, iel++)
		{
		  if(skip)
		    {
		      gzscanfd (car, MAXREAD, input, &tmp, 0);
		      /* Throw the element number */
		    }
		  for (k = 0; k < (*nnpe)[i]; k++)
		    {
		      gzscanfd (car, MAXREAD, input, &tmp, 0);
		      (*elements)[aux+k] = (int) tmp;
		    }
		  aux += (*nnpe)[i];
		}
	    }
	  //	  u_set_groups (ngroups, nnpe, nelempgrp, *elements);
	}
      if (!strncmp(car,"*END",4))
	{
	  if (!foundcoor || !foundelem)
	    error (EXIT, "Insufficient data in mesh file");
	  if (!rewinded)
	    gzrewind(input);
	  else
	    error (EXIT, "Insufficient data in mesh file");
	  rewinded = 1;
	}
    }

  gzclose(input);
  free(car);
}
#endif

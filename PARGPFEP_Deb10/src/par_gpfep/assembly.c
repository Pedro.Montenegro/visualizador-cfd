#include <stdlib.h>
#include <float.h>
#include "assembly.h"
#include "prof_utils.h"
#include "petsclog.h"

#define MAT_A_VARIABLE   0
#define MAT_A_INVARIABLE 1
#define INTEGRATE        2
#define NOT_INTEGRATE    3

/* This function is internal */
void InternalAssembly
  (F1 onelem, partition *Pa, systemquad *Sq, int *bcccond, 
   PetscScalar *bccvalue, int nfields, int nnfa, int *nfa,
   int *ipos, int *gpos, int *lpos, int *cpos, 
   PetscScalar *varaux, PetscScalar *varaaux, PetscScalar *varscan, 
   PetscScalar *varascan, PetscScalar *coordaux, PetscScalar *ae, 
   PetscScalar *rhse, PetscScalar *ae_ins, double *parcon, 
   double *delcon, double *parnum, double *parmat, 
   double *pargen, int *isim, int *Aflag, int *Lflag,
   int *GrpInt, Mat A, Vec b, PetscScalar *dp,
   PetscScalar *integrals, PetscScalar *inte, int nintgr,
   PetscScalar *lnobscan,
   PetscScalar *gbasescan, PetscScalar *gbaseaux);
/* ^^^^ just the prototype ^^^^ */


/*------------------------------------------------------------------------

  Function assembly - Assemble a FEM Matrix - Version n+1
  Programmed  by  Adrian Lew -  3/11/97 - Modified 5/2/98
  Modified again 6/3/98
  Modified again by EAD & GCB on 31 March 1999 (oblique nodes)
  Thanks to the Flaco for his patience !!!
  Updated ~ 2016: Simplified treatment of ghost elements and ghost nodes
  2017 / 11: clean up and fixed igr sign problem

  This function must be called from FORTRAN. It receives the Partition of
  the domain, the Partition of the boundary divided by groups, two vectors of 
  switches for knowing whether to calculate each group, the last step solution 
  vector, two pointers to the functions that calculate the elemental matrix and 
  right hand side term, the matrix to fill and  the right hand side vector to
  fill. It returns the matrix filled and the RHS calculated.
  If there's no surface mesh Pa->SurPa must be set to NULL, that's the
  way to not integrate the Surface, in other case, the SurInt vector could be 
  used for the same purpose.

  Arguments:
    Sy: system structure  to assembly
    v: last step full unknowns vector
    onelem: pointer to the elemental operations function over the volume
    surelem: pointer to the elemental operations function over the boundary
    A: matrix to assemble
    b: right hand side vector to assemble
    nfa: array with the id of each field to assemble 
    nnfa: number of fields to assemble (1<= nfa <=nfields)

    parcon: value of the continuation parameter      (to elemenental functions)
    delcon: value of the continuation parameter step (to elemenental functions)
    parnum: array with numerical parameters          (to elemenental functions)
    parmat: array with material parameters           (to elemenental functions)
    pargen: array with general parameters            (to elemenental functions)
    rintegrals: array for computing integrals
    nintgr: lenght of rintegrals array
    isim: symmetry indicator                         (to elemenental functions)

    Aflag: flag for recalculating the matrix A
    Lflag: lumping flag
    Sflag: switch for surface integration (INTEGRATE or NOT_INTEGRATE)
    VolInt: array of length ngroups with switches for specific group integration
    SurInt: array of length ngroups with switches for specific group integration

Remember:
    The nodes numbers begin with 1, not with 0. So, in the first nfields
    positions of v are the values for the node number 1.
    In nfa, field is the number of the unknown.
-----------------------------------------------------------------------------*/

#undef __SFUNC__
#define __SFUNC__ "assembly"

void assembly (systemdata **fSy, Vec *fv, F1 onelem, F1 surelem, Mat *fA, 
	       Vec *fb, int *fnfa, int *fnnfa, double *parcon, double *delcon, 
	       double *parnum, double *parmat, double *pargen, 
	       double *rintegrals, int *fnintgr, int *isim, int *Aflag, 
	       int *Lflag, int *Sflag, int *VolInt, int *SurInt)
{
  Mat      A;
  Vec      b, v, lv, lva, llnob;

  MPI_Comm Comm;        /* Communicator of Partition */
  int      ierr;        /* error checking variable */
  int      i;           /* loop counters */
  int      low, high;   /* span of local indices */

  int      nnpemax;     /* maximum between Pa->nnpe and SurPa->nnpe */
  int      *ipos;       /* vector mapping element numbering to Mat A 
			   numbering */
  int      *cpos;       /* vector mapping element numbering to the position of 
			   varaux at wich  the data of that node starts*/
  int      *gpos;       /* vector mapping element numbering to global 
			   numbering */
  int      *lpos;       /* vector mapping element numbering to the local of 
			   nodes order in the data vactors (cogeo..,var...) */
  PetscScalar   *inte;  /* vector for adding the elemental contributions to
			   the integrals */
  PetscScalar   *integrals;  /* total local contribution to the integral */
  PetscScalar   *varaux;     /* vector for passing the element values of v */
  PetscScalar   *varaaux;    /* vector for passing the element values of va */
  PetscScalar   *coordaux;   /* vector for passing the element nodes
				coordinates */
  PetscScalar   *gbaseaux;   /* vector for passing the oblique nodes basis */
  PetscScalar   *varscan;    /* pointer for scanning the v values */
  PetscScalar   *varascan;   /* pointer for scanning the va values */
  PetscScalar   *lnobscan;   /* pointer for scanning the oblique
				switches vector */
  PetscScalar   *gbasescan;  /* pointer for scanning the oblique bases */
  PetscScalar   *ae;         /* pointer to the elemental matrix */
  PetscScalar   *rhse;       /* pointer to the elemental right hand side */
  PetscScalar   *ae_ins;     /* Matrix to use in MatSetValues            */
  PetscScalar   *dp;         /* array containing then FEM basis derivatives */

  int      aedim;       /* dimension of ae */
  int      rhsedim;     /* dimension of rhse */
  int Lcond, bcond;
  PetscScalar tmp;      /* temporary variable */
  PetscBool pflag;
  int Avariable;

  int nnfa, nvar, maxcomp, *nfa, nsd, nintgr;
  systemdata   *Sy;
  /*
  static int firsttime=1, counter;
  if (firsttime)
    {
      firsttime=0;
      PetscLogEventRegister(&counter,"Assembly without comm",PLER_RED);
    } 
  */

  A = *fA;
  b = *fb;
  v = *fv;
  Sy = *fSy;
  Lcond = *Lflag;
  Avariable = ((*Aflag & 1) == MAT_A_VARIABLE);

  nnpemax = Sy->Pa->nnpe; /* Sy->Pa->nnpe > Sy->SurPa->nnpe surely */
  /* Take care of this in the case that Pa->nnpe were 
     different from Sq->maxnpe */
  /* Note: 
     The nsd, nvar and maxcomp values of Pa and SurPa should be the same,
     so, there is no verification of consistency at all. */
  Comm    = Sy->Pa->Comm;
  nsd     = Sy->Pa->nsd;
  maxcomp = Sy->Sq->maxcomp;
  nvar    = Sy->Sq->nvar;

  nnfa    = *fnnfa; 
  nintgr  = *fnintgr;

  /* Filling nfa due to the fact that fnfa is in fortran numbering */
  nfa = (int *) malloc(nnfa * sizeof(int));
  if (nfa == (int *)NULL)
    error(EXIT, "Not enough memory for nfa");

  for (i = 0; i < nnfa; i++)
    nfa[i] = fnfa[i] - 1;
 
  /* Update values of ghost nodes in global unknowns vector */
  VecGhostUpdateBegin(v, INSERT_VALUES, SCATTER_FORWARD);
  VecGhostUpdateEnd(v, INSERT_VALUES, SCATTER_FORWARD);

  /* Also for va (unknowns in previous step) */
  VecGhostUpdateBegin(Sy->va, INSERT_VALUES, SCATTER_FORWARD);
  VecGhostUpdateEnd(Sy->va, INSERT_VALUES, SCATTER_FORWARD);

  /* Initializing some useful values */
  ierr = VecGetOwnershipRange
    (v, &low, &high);                CHKERRABORT(PETSC_COMM_WORLD,ierr);

  ierr = VecGhostGetLocalForm(v, &lv);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGetArray
    (lv, &varscan);                   CHKERRABORT(PETSC_COMM_WORLD,ierr);

  ierr = VecGhostGetLocalForm(Sy->va, &lva);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGetArray
    (lva, &varascan);             CHKERRABORT(PETSC_COMM_WORLD,ierr);

  if (Sy->lnob)
    {   /* There are oblique nodes in mesh */
      ierr = VecGhostGetLocalForm(Sy->lnob, &llnob);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
      ierr = VecGetArray
	(llnob, &lnobscan);       CHKERRABORT(PETSC_COMM_WORLD,ierr);
      ierr = VecGetArray
	(Sy->gbase, &gbasescan);     CHKERRABORT(PETSC_COMM_WORLD,ierr);
    }
  else             /* No oblique nodes in mesh, use lnobscan as switch */
    lnobscan = (PetscScalar *) NULL;

  /* Check if matrix A is assembled, if true and A is variable, set zero values */
  MatAssembled (A, &pflag);
  if (Avariable && pflag == PETSC_TRUE)
    {
      ierr = MatZeroEntries(A); CHKERRABORT(PETSC_COMM_WORLD,ierr);
    }

  /* Also zero vector b if needed */
  bcond=!(Lcond & RHS_IGNORE_FLAG);
  if (bcond && !(Lcond & RHS_DONT_ZERO_FLAG))
    {
      tmp = 0;
      ierr = VecSet(b, tmp);   CHKERRABORT(PETSC_COMM_WORLD,ierr);
    }

  /* Alloc memory for vector of unknowns in current element */
  varaux = (PetscScalar *) malloc (nnpemax*nvar*maxcomp*sizeof(PetscScalar));
  if (varaux == (PetscScalar *)NULL)
    error (EXIT, "Not enough memory for varaux");

  /* Also for previous step values */
  varaaux = (PetscScalar *) malloc (nnpemax*nvar*maxcomp*sizeof(PetscScalar));
  if (varaaux == (PetscScalar *)NULL)
    error (EXIT, "Not enough memory for varaaux");

  /* Alloc memory for coordinates of element nodes */
  coordaux = (PetscScalar *) malloc (nnpemax*nsd*sizeof(PetscScalar));
  if (coordaux == (PetscScalar *)NULL)
    error(EXIT, "Not enough memory for coordaux");

  /* In case of oblique nodes, alloc memory for base change matrix */
  if (Sy->lnob)
    {
      gbaseaux = (PetscScalar *)malloc(nnpemax*nsd*nsd*sizeof(PetscScalar));
      if(!gbaseaux)
	error(EXIT, "Not enough memory for gbaseaux");
    }
  else
    gbaseaux = (PetscScalar *) NULL;

  /* Elemental matrix, full, and compressed (deleted Dirichlet rows) forms */
  ae     = (PetscScalar *)NULL; /* for avoiding the warning */
  ae_ins = (PetscScalar *)NULL; /* for avoiding the warning */
  if (Avariable)
    {
      aedim = nnpemax*nnpemax*nnfa*nnfa;
      ae = (PetscScalar *) malloc (aedim*sizeof(PetscScalar)); 
      if(ae == (PetscScalar *)NULL)
	error(EXIT, "Not enough memory for ae");

      ae_ins = (PetscScalar *) malloc(aedim*sizeof(PetscScalar)); 
      if(ae_ins == (PetscScalar *)NULL)
	error(EXIT, "Not enough memory for ae_ins");
    }

  /* dp: array for nodal form function's derivatives */
  dp = (PetscScalar *) malloc(nnpemax*3*(nvar+1)*sizeof(PetscScalar));
  if (dp == (PetscScalar *)NULL)
    error(EXIT, "Not enough memory for dp");

  /* Elemental RHS */
  rhsedim = nnpemax*nnfa;
  rhse = (PetscScalar *) malloc(rhsedim*sizeof(PetscScalar));
  if (rhse == (PetscScalar *)NULL)
    error(EXIT, "Not enough memory for rhse");

  /* ipos: global position of unknowns to assemble */
  ipos = (int *) malloc(rhsedim*sizeof(int));
  if (ipos == (int *)NULL)
    error(EXIT, "Not enough memory for ipos");

  /* gpos: global node numbers (not used) */
  gpos = (int *) malloc(nnpemax*sizeof(int));
  if (gpos == (int *)NULL)
    error(EXIT, "Not enough memory for gpos");

  /* cpos: position of first unknown of each node (local to processor) */
  cpos = (int *) malloc(nnpemax*sizeof(int));
  if (cpos == (int *)NULL)
    error(EXIT, "Not enough memory for cpos");

  /* lpos: node numbers (local to processor) */
  lpos = (int *) malloc(nnpemax*sizeof(int));
  if (lpos==(int *)NULL)
    error(EXIT, "Not enough memory for lpos");

  /* inte: elemental integrals */
  inte = (PetscScalar *) malloc (nintgr * sizeof(PetscScalar));
  if (nintgr > 0 && inte == (PetscScalar *)NULL)
    error (EXIT, "Not enough memory for inte");

  /* integrals: processor local vector */
  integrals = (PetscScalar *) malloc (nintgr * sizeof(PetscScalar));
  if (nintgr > 0 && integrals == (PetscScalar *)NULL)
    error (EXIT, "Not enough memory for integrals");

  for (i = 0; i < nintgr; i++)
    if (i < 30)
      integrals[i] = 0;
    else if (i < 35)
      integrals[i] = -DBL_MAX;
    else if (i < 40)
      integrals[i] = DBL_MAX;
    else if (i < 45)
      integrals[i] = 1.0;
    else
      integrals[i] = 0.0;

  /* Local (and ghost) elements integration loop over volume mesh */
  InternalAssembly
    (onelem, Sy->Pa, Sy->Sq, Sy->bcccond, Sy->bccvalue, 
     Sy->nfields, nnfa, nfa, ipos, gpos, lpos, cpos, varaux,
     varaaux, varscan, varascan, coordaux, ae, rhse, ae_ins, 
     parcon, delcon, parnum, parmat, pargen, isim, Aflag, Lflag, 
     VolInt, A, b, dp, integrals, inte, nintgr,
     lnobscan, gbasescan, gbaseaux); 

  /* Integrating over the Surface Mesh */
  if (Sy->SurPa != (partition *)NULL && *Sflag==INTEGRATE)
    InternalAssembly
      (surelem, Sy->SurPa, Sy->SurSq, Sy->bcccond, Sy->bccvalue, 
       Sy->nfields, nnfa, nfa, ipos, gpos, lpos, cpos, varaux, 
       varaaux, varscan, varascan, coordaux, ae, rhse, ae_ins, 
       parcon, delcon, parnum, parmat, pargen, isim, Aflag, 
       Lflag, SurInt, A, b, dp, integrals, inte, nintgr, 
       lnobscan, gbasescan, gbaseaux);
  else if (Sy->SurPa == (partition *)NULL && *Sflag==INTEGRATE)
    error (STAY,"Attempting to integrate on a surface that doesn't exists");

  /* Petsc Matrix assebly, if needed */
  if (Avariable)
    {
      ierr = MatAssemblyBegin
	(A, MAT_FINAL_ASSEMBLY);           CHKERRABORT(PETSC_COMM_WORLD,ierr);
      ierr = MatAssemblyEnd
	(A, MAT_FINAL_ASSEMBLY);           CHKERRABORT(PETSC_COMM_WORLD,ierr);
      ierr = MatSetOption (A, MAT_NEW_NONZERO_LOCATIONS,
			   PETSC_FALSE);   CHKERRABORT(PETSC_COMM_WORLD,ierr);
    }

  /* Petsc vector assembly, if needed */
  if (bcond)
    {
      ierr = VecAssemblyBegin(b);          CHKERRABORT(PETSC_COMM_WORLD,ierr);
      ierr = VecAssemblyEnd(b);            CHKERRABORT(PETSC_COMM_WORLD,ierr);
    }

  /* Reduce over the integral values */
  if (nintgr != 0)
    {
      int nsum, nmax, nmin, nprod, nor;
      nsum = nintgr;
      if (nsum > 30) nsum = 30;
      ierr = MPI_Allreduce (integrals, rintegrals, nsum, MPI_DOUBLE, MPI_SUM, Comm);
      nmax = nintgr - 30;
      if (nmax < 0) nmax = 0;
      else if (nmax > 5) nmax = 5;
      if (nmax)
	ierr = MPI_Allreduce (integrals+30, rintegrals+30, nmax,
			      MPI_DOUBLE, MPI_MAX, Comm);
      nmin = nintgr - 35;
      if (nmin < 0) nmin = 0;
      else if (nmin > 5) nmin = 5;
      if (nmin)
	ierr = MPI_Allreduce (integrals+35, rintegrals+35, nmin,
			      MPI_DOUBLE, MPI_MIN, Comm);
      nprod = nintgr - 40;
      if (nprod < 0) nprod = 0;
      else if (nprod > 5) nprod = 5;
      if (nprod)
	ierr = MPI_Allreduce (integrals+40, rintegrals+40, nprod,
			      MPI_DOUBLE, MPI_PROD, Comm);
      nor = nintgr - 45;
      if (nor < 0) nor = 0;
      else if (nor > 5) nor = 5;
      if (nprod)
	ierr = MPI_Allreduce (integrals+45, rintegrals+45, nor,
			      MPI_DOUBLE, MPI_LOR, Comm);
    }

  ierr = VecRestoreArray (lv, &varscan);    CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGhostRestoreLocalForm(v, &lv);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecRestoreArray(lva, &varascan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGhostRestoreLocalForm (Sy->va,
				   &lva);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
  if (Sy->lnob)
    {
      ierr = VecRestoreArray
	(Sy->lnob, &lnobscan);         CHKERRABORT(PETSC_COMM_WORLD,ierr);
      ierr = VecRestoreArray
	(Sy->gbase, &gbasescan);       CHKERRABORT(PETSC_COMM_WORLD,ierr);
      free(gbaseaux);
    }

  free(varaux);
  free(varaaux);
  free(coordaux);
  if (Avariable)
    {
      free(ae);
      free(ae_ins);
    }
  free(rhse);
  free(dp);
  free(ipos);
  free(gpos);
  free(lpos);
  free(cpos);
  free(nfa);
  free(inte);
  free(integrals);

  /*
    if(nnfa==3)
    {
    Viewer   Va;
    ViewerFileOpenASCII(MPI_COMM_WORLD,"vector",&Va);
    VecView(b,Va);
    ViewerDestroy(Va);
    error(EXIT, "Chau");
    }
  */
}

/*------------------------------------------------------------------------
  Function InternalAssembly - Assembly the local elements of a FEM Matrix 
  Version 1 
  Programmed  by  Adrian Lew -  7/03/98
  Modified ~ 2016 to handle local and ghost elements
       
  This function is thought for being used only from assembly.
  Its task is to make the loop over the local elements of each group
  to assembly their contributions to the FEM matrix. 
  The memory is allocated outside this function because the same memory
  is used in several different calls.

Variable names:
    onelem: pointer to the elemental operations function
    Pa: mesh data
    Sq: qudrature data
    bcccond: boundary conditions vector
    bccvalue: boundary condition values vector
    nfields: total number of unknowns per node
    nnfa: number of fields to assembly (1<= nfa <=nfields)
    nfa: array with the numbers of the fields to assembly 
    ipos: array mapping elemental to global unknowns
    gpos: array mapping elemental to global nodes (not used) 
    lpos: array mapping elemental to local nodes
    cpos: array mapping elemental nodes to first local unknown
    varaux: current variables in the element
    varaaux: previous step variables in the element
    varscan: current variables at local nodes
    varascan: previous step variables at local nodes
    coordaux: nodal coordinates of the nodes of the element
    ae: elemental matrix
    rhse: elemental RHS
    ae_ins: compressed elemental matrix (whithout dirichlet rows)
    parcon: value of the continuation parameter
    delcon: value of the continuation parameter step
    parnum: array with numerical parameters
    parmat: array with material parameters
    pargen: array with general parameters
    isim: symmetry indicator

    Aflag: flag for recalculating the matrix A
    Lflag: lumping flag
    GrpInt: array with switches to integrate specific groups.
    A: matrix to assemble
    b: right hand side vector to assemble
    dp: array for contruct the FEM basis function derivatives
    integrals: array for acumulate the integral values in the loop  over 
              the elements
    inte: elementary integral contribution
    nintgr: number of integrals to calculate
    lnobscan: switch of oblique nodes 
             (pointer over local part of distributed Vec)
    gbasescan: oblique nodes basis
    gbaseaux: array for passing values of gbase to elemental routines
-----------------------------------------------------------------------------*/

#ifdef DEBUG_ASSEMBLY
  FILE* dbg_fp;
#endif

#undef __SFUNC__
#define __SFUNC__ "InternalAssembly"

void InternalAssembly
  (F1 onelem, partition *Pa, systemquad *Sq, int *bcccond, 
   PetscScalar *bccvalue, int nfields, int nnfa, int *nfa,
   int *ipos,  int *gpos, int *lpos, int *cpos, 
   PetscScalar *varaux, PetscScalar *varaaux, PetscScalar *varscan, 
   PetscScalar *varascan, PetscScalar *coordaux, PetscScalar *ae, 
   PetscScalar *rhse, PetscScalar *ae_ins, double *parcon, 
   double *delcon, double *parnum, double *parmat, 
   double *pargen, int *isim, int *Aflag, int *fLflag,
   int *GrpInt, Mat A, Vec b, PetscScalar *dp, PetscScalar *integrals,
   PetscScalar *inte, int nintgr,
   PetscScalar *lnobscan,
   PetscScalar *gbasescan, PetscScalar *gbaseaux)
{
  int nnpe, nsd, nvar;
  int j, i2, k, l, m, kkdir, inode;
  int tmpint1, tmpint2;
  int ierr;
  int inbe;        /* index at the origin of the current element nodes */
  int rhsedim;     /* dimension of rhse */
  int igr;         /* group index */
  int iel;         /* element index */
  int bcond;       /* logical: build rhs */
  int localElement, localNode; /* logical: internal element, local Rows */
  int Avariable, Asymm, Aflg;       /* result of a conditional */
  int Lcond;       /* lumping flag */
  int iswnob;      /* Switch: element contains O.N. */
  int posgbase;    /* Switch: node is an O.N. if posgbase != 0 */
  int anfa;        /* valor absoluto de nfa[l] */
  int size, ipass=0, npass; /* Numero de procesadores, partial assemblies */
  int ident, dpass=0.0;   /* Numero de elementos entre cada ensamblaje parcial */
  int lob=0;              /* position of oblique field */
  double vecaux[Pa->nsd]; /* auxiliar un-rotated oblique field */

  PetscScalar   tmp, daux;
  //  static int firsttime2 = 1, cmatseti;

#ifdef DEBUG_ASSEMBLY
  static int dbg_time=1;
  int prstart=3, prend=4;
  char dbg_file[20];
#endif

  /*  if (firsttime2)
    {
      firsttime2=0;
      PetscLogEventRegister(&cmatseti,"matset internal",PLER_RED);
      }*/

#ifdef DEBUG_ASSEMBLY
  if (dbg_time == 1) {
     MPI_Comm_rank (Pa->Comm, &ident);
     sprintf(dbg_file, "dbg_matelem%d", ident);
     dbg_fp = fopen(dbg_file, "w");
     fprintf(dbg_fp, "Mat. elem., elements with oblique nodes:\n");
  }
#endif

  /* calcula cada cuanto hacer los ensamblajes parciales */
  MPI_Comm_size (Pa->Comm, &size);
  MPI_Comm_rank (Pa->Comm, &ident);
  npass = Pa->nelemtot / size / 1000;
  if (npass > 1)
    {
      dpass = (Pa->nelemloc + Pa->nelemext) / npass + 1;   /* Approx 250 */
/*      printf(" Proc: %d, npass: %d, dpass: %d\n", ident, npass, dpass); */
      ipass = 0;
    }

  nnpe    = Pa->nnpe;
  nsd     = Pa->nsd;
  nvar    = Sq->nvar;

  rhsedim = nnpe*nnfa;
  Aflg = (*Aflag & 1);
  Avariable = (Aflg == MAT_A_VARIABLE);
  Asymm = ((*Aflag & 2) == 2);

  /* Also zero vector b if needed */
  Lcond = *fLflag;
  bcond=!(Lcond & RHS_IGNORE_FLAG);
  Lcond=Lcond & 15;
  /* Loop over elements */
  kkdir = 0;
  for (iel = 0; iel < Pa->nelemloc + Pa->nelemext; iel++) {
    igr = Pa->GOS[iel];
    if (GrpInt[(igr < 0 ? -igr-1 : igr-1)] == INTEGRATE) {   /* This group is integrated */
      inbe = iel * nnpe;
      for (k = 0; k < nnpe; k++)   /* Loop over nodes of this element */
	for (j = 0; j < nnfa; j++)   /* Loop over fields of this node */
	  {
	    inode = Pa->Mesh[inbe+k];
	    if (inode >= Pa->nodloc)
	      {
		inode = Pa->Glob[inode-Pa->nodloc];
	      }
	    else
	      inode = inode + Pa->first - 1;
	    ipos[k*nnfa+j] = inode * nnfa + j;
	  }
      /* ipos: global matrix indices, "C" style */

      /* Oblique nodes processing */
      iswnob = 0;
      if (lnobscan) {             /* Oblique nodes in mesh */
	for (k = 0; k < nnpe; k++)
	  if (lnobscan[Pa->Mesh[inbe+k]])
	    iswnob = 1;
	if (iswnob) {             /* Oblique nodes in this element */
	  for (k = 0; k < nnpe; k++){
	    posgbase = lnobscan[Pa->Mesh[inbe+k]];
	    if (posgbase) {        /* This node is oblique */
	      for (j = 0; j < nsd; j++){
		for (i2 = 0; i2 < nsd; i2++){
		  gbaseaux[k + nnpe*j + i2*nnpe*nsd] = 
		    gbasescan[(posgbase-1) + j*nsd + i2];
		}
	      }
	    }
	    else {               /* This one is normal */
	      for (j = 0; j < nsd; j++){
		for (i2 = 0; i2 < nsd; i2++){
		  gbaseaux[k + nnpe*j + i2*nnpe*nsd] = 
		    (i2 == j) ? 1.0 : 0.0; 
		}
	      }
	    }   /* end if ( This node is oblique ) */
	  }   /* end loop over nodes in this element */
	}   /* end if ( Oblique nodes in this element ) */
      }   /* end if ( Oblique nodes in mesh ) */

      for (k = 0; k < nnpe; k++) {
	lpos[k] = Pa->Mesh[inbe+k];
	cpos[k] = nfields * lpos[k];
	tmpint2 = 0;
	for (j = 0; j < nvar; j++) {
	  tmpint1 = Sq->icomp[j];
	  for (l = 0; l < tmpint1; l++) {
	    varaux[k+j*nnpe+l*nnpe*nvar] = varscan[cpos[k]+tmpint2];
	    varaaux[k+j*nnpe+l*nnpe*nvar] = varascan[cpos[k]+tmpint2];
	    tmpint2++;
	  }
	}
	for (j = 0; j < nsd; j++)
	  coordaux[k+j*nnpe] = (Pa->cogeo)[nsd*lpos[k]+j];
      }

      /* igr can be negative, to account for element ownership.
       * onelem should receive always positive values
       */
      tmpint1 = igr < 0 ? -igr : igr;

      /* Elemental operations */
      (*onelem)(varaux, varaaux, coordaux, &nvar, Sq->icomp, &nnpe, &nsd, 
		ae, rhse, parcon, delcon, parnum, parmat, pargen, 
		&(Sq->ngau), Sq->kint, &(Sq->itypegeo), &iel, Sq->nod, 
		&(Sq->maxnpe), &(Sq->maxgau), Sq->wgp, isim, Sq->p, 
		Sq->dpl, dp, &rhsedim, &Aflg, &tmpint1, inte, &nintgr,
		&iswnob, gbaseaux);

      /* Global assembly */
      if (iswnob) {
#ifdef DEBUG_ASSEMBLY
	/* Elemental matrices
	   if (dbg_time >= prstart && dbg_time <= prend) {
	   fprintf(dbg_fp, "\nElement: %d\n", iel);
	   for (k = 0; k < rhsedim; k++) {
	   for (j = 0; j < rhsedim; j++)
	   fprintf (dbg_fp, " %g", ae[k*rhsedim+j]);
	   fprintf (dbg_fp, "\n");
	   }
	   }
	*/
	if (dbg_time >= prstart && dbg_time <= prend) {
	  fprintf(dbg_fp, "\nElement: %d, gbaseaux:\n", iel);
	  for (k = 0; k < nnpe; k++) {
	    for (j = 0; j < nsd; j++)
	      for (l = 0; l < nsd; l++)
		fprintf (dbg_fp, " %g", gbaseaux[k+nnpe*j+nnpe*nsd*l]);
	    fprintf (dbg_fp, "\n");
	  }
	}
#endif
 	lob = -1;
	for (l = 0; l < nnfa && lob == -1; l++) {   /* Find oblique field */
	  if (nfa[l] < 0) {
	    lob = l;
	    if (nfa[l+1] > 0) lob = -1;
	    if (nsd >= 3 && nfa[l+2] > 0) lob = -1;
	  }
	}
      } /* end if (iswnob) */

      localElement = 1;
      for (k = 0; k < nnpe; k++) {   /* Nodes of this element */
	if (iswnob && lob != -1) {
	  /* vecaux is always needed (even for non-oblique nodes)
	     && lnobscan[Pa->LocMesh[inbe+k]-(Pa->first)]) {      */
	  /* There are oblique nodes in this element, and there is an oblique
	     field in this fractional step(, and k is an oblique node) */
	  /* ... un-rotate previous solution at node k */
	  anfa = nfa[lob] < 0 ? -(nfa[lob]+1)-1 : nfa[lob];
	  for (j = 0; j < nsd; j++)
	    {
	      vecaux[j] = 0;
	      for (i2 = 0; i2 < nsd; i2++)
		vecaux[j] += varscan[cpos[k]+anfa+i2] *
		  gbaseaux[k + nnpe*j + i2*nnpe*nsd];
	    }
	}

	/* Row locality depends only on node */
	localNode = lpos[k] < Pa->nodloc;
	if (!localNode)
	  localElement = 0;
	
	for (l = 0; l < nnfa; l++) {   /* Fields of this node (matrix rows) */
	  m = k*nnfa+l;
	  anfa = nfa[l] < 0 ? -(nfa[l]+1)-1 : nfa[l];
/*
 * Debug: print info if local node is 1:
	    {
	      if (lpos[k] >= 0 && lpos[k] <= 1 && nnfa == 4 && iswnob && bccvalue[cpos[k]+anfa] > 0) {
		printf("gpos, nfa: %d %d, bcccond, bccvalue: %d %g \n",
		       gpos[k], nfa[l], bcccond[cpos[k]+anfa], bccvalue[cpos[k]+anfa]);
		printf("gpos, lob: %d %d, vecaux: %g %g %g \n",
		       gpos[k], lob, vecaux[0],vecaux[1],vecaux[2]);
		for (j = 0; j < nsd; j++)
		  {
		    for (i2 = 0; i2 < nsd; i2++)
			printf("gbaseaux %d %d : %g \n",
                        j, i2,gbaseaux[k + nnpe*j + i2*nnpe*nsd]);
		  }
	      }
	    }
 * Debug end.
 */
	  if (localNode &&           /* local row */
	      bcccond[cpos[k]+anfa] == 0) {     /* No Dirichlet condition */

	    if (Avariable) {                         /* True if MAT_A_VARIABLE */

	      if (Lcond==LUMPED) {               /* Lumped: only diagonal */
		ierr = MatSetValues(A, 1, &ipos[m], 1, &ipos[m], 
				    &ae[m*rhsedim+m], ADD_VALUES);  
		CHKERRABORT(PETSC_COMM_WORLD,ierr);
	      }
	      else if (Lcond == STANDARD_UNCOUPLED) { /* Uncoupled fields:  */
		if (Asymm) {
		  /* Force symmetric matrix: if column is BC, pass term to the RHS */
		  for (j = 0; j < nnpe; j++) {
		    int anfacol, lcol, kcol;
		    kcol = j;
		    lcol = l;
		    anfacol = nfa[lcol] < 0 ? -(nfa[lcol]+1)-1 : nfa[lcol];
		    i2 = j*nnfa + l;
		    tmp = ae[i2 * rhsedim + m];
		    if (bcccond[cpos[kcol]+anfacol] == 0) /* Column is normal */
		      ierr = MatSetValues(A, 1, &ipos[m], 1, &ipos[i2], &tmp, 
					  ADD_VALUES);
		    else {                                /* Column is BC */
#ifdef MATRIX_FORM
		      daux = bccvalue[cpos[kcol]+anfacol];
#else
		      if (iswnob && lob != -1 && lcol >=lob && lcol < lob + nsd)
			daux = bccvalue[cpos[kcol]+anfacol] - vecaux[lcol-lob];
		      else
			daux = bccvalue[cpos[kcol]+anfacol] - varscan[cpos[kcol]+anfacol];
#endif
		      rhse[m] -= daux * tmp;
		    }
		  }
		}
		else { /* Not symmetric format */
		  for (j = 0; j < nnpe; j++) {
		    i2 = j*nnfa + l;
		    tmp = ae[i2 * rhsedim + m];
		    ierr = MatSetValues(A, 1, &ipos[m], 1, &ipos[i2], &tmp, 
					ADD_VALUES);
		    CHKERRABORT(PETSC_COMM_WORLD,ierr);
		  }
		}
	      }
	      else if (Lcond == STANDARD || Lcond == NSBSLUMP){  /* Not Lumped: */
		if (Asymm) {
		  /* Force symmetric matrix: pass BC to the RHS */
		  for (j = 0; j < rhsedim; j++) {
		    int anfacol, lcol, kcol;
		    kcol = j / nnfa;
		    lcol = j % nnfa;
		    anfacol = nfa[lcol] < 0 ? -(nfa[lcol]+1)-1 : nfa[lcol];
		    if (bcccond[cpos[kcol]+anfacol] == 0) /* Column is normal */
		      ae_ins[j + m * rhsedim] = ae[j * rhsedim + m];
		    else {                                /* Column is BC */
		      tmp = ae[j * rhsedim + m];
		      ae_ins[j + m * rhsedim] = 0.0;
#ifdef MATRIX_FORM
		      daux = bccvalue[cpos[kcol]+anfacol];
#else
		      if (iswnob && lob != -1 && lcol >=lob && lcol < lob + nsd)
			daux = bccvalue[cpos[kcol]+anfacol] - vecaux[lcol-lob];
		      else
			daux = bccvalue[cpos[kcol]+anfacol] - varscan[cpos[kcol]+anfacol];
#endif
		      rhse[m] -= daux * tmp;
		    }
		  }
		}
		else /* Assemble full row */
		  for (j = 0; j < rhsedim; j++)                 /* full row */
		    ae_ins[j + m * rhsedim] = ae[j * rhsedim + m];
	      }
	      else {   /* safety */
		error(EXIT, "assembly.c: Unrecognized lumping flag");
	      }
	    }
	  }
	  else if (localNode) {   /* Dirichlet Condition */
	    kkdir++;
	    if (Avariable) {
	      if (Lcond == STANDARD || Lcond == NSBSLUMP) {
		/* Not Lumped */
		for(j=0; j<rhsedim; j++)
		  ae_ins[j+m*rhsedim]=0.;
		ae_ins[m+m*rhsedim]=1.;
	      }
	      else if (Lcond == STANDARD_UNCOUPLED || Lcond == LUMPED) {
		/* Lumped */
		tmp = 1.;
		ierr = MatSetValues(A, 1, &ipos[m], 1, &ipos[m], &tmp, 
				    ADD_VALUES);    CHKERRABORT(PETSC_COMM_WORLD,ierr);
	      }
	    }

#ifdef MATRIX_FORM
	    rhse[m] = bccvalue[cpos[k]+anfa];
#else
	    if (iswnob && lob != -1 && l >=lob && l < lob + nsd)
	      rhse[m] = bccvalue[cpos[k]+anfa] - vecaux[l-lob];
	    else
	      rhse[m] = bccvalue[cpos[k]+anfa] - varscan[cpos[k]+anfa];
	    /* Parche porque varscan est\E1 rotado
	       if (lnobscan && nfa[l] < 0)
	       {
	       if (lnobscan[Pa->LocMesh[inbe+k]-(Pa->first)])
	       rhse[m] = 0.;
	       }
	    */
#endif
	  }
	  /* non-local rows should not be assembled ! (to avoid communications) */
	  /* else {                          /\* non-local row *\/ */
	  /*   for (j = 0; j < rhsedim; j++) */
	  /*     ae_ins[j + m * rhsedim] = 0.0; */
	  /*   rhse[m] = 0.0; */
	  /* } */
	} /* 0 <= l < nnfa */
      } /* 0 <= k < nnpe */
      
      if (Avariable)                                /* True if MAT_A_VARIABLE */
	{
	  if (Lcond==STANDARD)
	    {
	      if (localElement)       /* all local rows, set all ae values */
		{
		  ierr = MatSetValues(A, rhsedim, ipos, rhsedim, ipos, ae_ins,
				      ADD_VALUES
				      ); CHKERRABORT(PETSC_COMM_WORLD,ierr);
		}
	      else                            /* compact (only local rows) */
		{
		  int nlrow = 0;       /* nlrow counts number of local rows */
		  int *iposr;
		  iposr = (int *) malloc(rhsedim * sizeof(int));
		  for (k = 0; k < nnpe; k++)
		    {
		      localNode = lpos[k] < Pa->nodloc;
		      if (localNode)               /* only copy local rows */
			for (l = 0; l < nnfa; l++)
			  {
			    m=k*nnfa+l;
			    for (j = 0; j < rhsedim; j++)
			      ae_ins[nlrow*rhsedim+j] = ae_ins[m*rhsedim+j];
			    iposr[nlrow++] = ipos[m];
			  }
		    }
		  ierr = MatSetValues(A, nlrow, iposr, rhsedim, ipos,
				      ae_ins, ADD_VALUES
				      ); CHKERRABORT(PETSC_COMM_WORLD,ierr);
 		  free(iposr);
		}
	    }
	  else if (Lcond == NSBSLUMP)
	    {
	      int *iposparc;
	      //		if (nnfa>0)PetscLogEventBegin(cmatseti,0,0,0,0);
	      iposparc = (int *) malloc(rhsedim * sizeof(int));
	      for (k = 0; k < nnpe; k++)
		for (l = 0; l < nnfa; l++)
		  {
		    if (l < nnfa-1)  /* velocity component */
		      {
			int ii;
			for (ii=0; ii<nnpe; ii++) /* select columns */
			  {
			    iposparc[ii] = ipos[l+ii*nnfa];
			    ae_ins[(k*nnfa+l)*rhsedim + ii] = ae_ins[(k*nnfa+l)*rhsedim + l+ii*nnfa];
			  }
			ierr = MatSetValues(A, 1, ipos+k*nnfa+l, 
					    nnpe, iposparc, ae_ins+(k*nnfa+l)*rhsedim,
					    ADD_VALUES);
			CHKERRABORT(PETSC_COMM_WORLD,ierr);
		      }
		    else  /* pressure */
		      {
			ierr = MatSetValues(A, 1, ipos+k*nnfa+l, 
					    rhsedim, ipos, ae_ins+(k*nnfa+l)*rhsedim,
					    ADD_VALUES);
			CHKERRABORT(PETSC_COMM_WORLD,ierr);
		      }
		  }
	      free(iposparc);
//		if (nnfa>0)PetscLogEventEnd(cmatseti,0,0,0,0);
	    }
	}
      if (bcond) {
	if (localElement)                              /* set all rhse values */
	  {
	    ierr = VecSetValues (b, rhsedim, ipos,
				 rhse, ADD_VALUES
				 ); CHKERRABORT(PETSC_COMM_WORLD,ierr);
	  }
	else
	  {
	    int nlrow = 0;       /* nlrow counts number of local rows */
	    for (k = 0; k < nnpe; k++)
	      {
		localNode = lpos[k] < Pa->nodloc;
		if (localNode)               /* only copy local rows */
		  for (l = 0; l < nnfa; l++)
		    {
		      m=k*nnfa+l;
		      rhse[nlrow] = rhse[m];
		      ipos[nlrow++] = ipos[m];
		    }
	      }
	    ierr = VecSetValues(b, nlrow, ipos,
				rhse, ADD_VALUES
				); CHKERRABORT(PETSC_COMM_WORLD,ierr);
	    /* CAUTION: *do not* use ipos below this point in this function !!!! */
	  }
      }

      if (igr > 0)
	for (k = 0; k < nintgr; k++)
	  {
	    if (k < 30)      /* SUM */
	      integrals[k] += inte[k];
	    else if (k < 35) /* MAX */
	      {
		if (inte[k] > integrals[k])
		  integrals[k] = inte[k];
	      }
	    else if (k < 40) /* MIN */
	      {
		if (inte[k] < integrals[k])
		  integrals[k] = inte[k];
	      }
	    else if (k < 45) /* PROD */
	      integrals[k] *= inte[k];
	    else             /* OR */
	      if (inte[k] != 0.0)
		integrals[k] = 1.0;
	  }

      /* Ensamblaje parcial */
      if (Avariable && npass > 1 && !((iel+1) % dpass)) {
	ierr = MatAssemblyBegin(A, MAT_FLUSH_ASSEMBLY);      CHKERRABORT(PETSC_COMM_WORLD,ierr);
	ierr = MatAssemblyEnd  (A, MAT_FLUSH_ASSEMBLY);      CHKERRABORT(PETSC_COMM_WORLD,ierr);
	ipass++;
	/*	  printf("Parc. assembly %d/%d, proc: %d\n", ipass, npass, ident); */
      }
    } /* End if (integrate this group) */
  }   /* End loop over all elements */
  while (Avariable && npass > 1 && ipass < npass)
    {
      ierr = MatAssemblyBegin(A, MAT_FLUSH_ASSEMBLY);      CHKERRABORT(PETSC_COMM_WORLD,ierr);
      ierr = MatAssemblyEnd(A, MAT_FLUSH_ASSEMBLY);        CHKERRABORT(PETSC_COMM_WORLD,ierr);
      ipass++;
/*      printf("Parc. assembly(c) %d/%d, proc: %d\n", ipass, npass, ident); */
    }
/*    printf ("\n %d cond. de Dirichlet \n",kkdir) ;*/

#ifdef DEBUG_ASSEMBLY
  if (dbg_time == prend)
    fclose(dbg_fp);
  dbg_time++;
#endif

}   /* End function internal assembly */


/*
 * setbcond: put boundary conditions
 */
void setbcond (systemdata **fSy, Vec *fv, Mat *fA, 
	       Vec *fb, int *fnfa, int *fnnfa, IS *fisbc)
{
  /* Set boundary conditions on:
   *  -matrix (one in diagonal, zero rest of the row)
   *  -right hand side ("compressed" vector, nnfa unknowns per node)
   *  -unknowns vector ("full" vector, nfields unknowns per node)
   * Sy: general info about the system: partition, fields, etc...
   * v: unknowns vector (can be PETSC_NULL_OBJECT)
   * A: matrix (can be PETSC_NULL_OBJECT)
   * b: RHS (can be PETSC_NULL_OBJECT)
   * fnfa, fnnfa: components (# of components) to set
   * isbc: Index set of matrix rows to set
   */
  Mat      A;
  Vec      b, v;
  IS       isbc;

  MPI_Comm Comm;        /* Communicator of Partition */
  int      ierr;        /* error checking variable */
  int      i;           /* loop counters */
  int      ipos;
  PetscScalar *varscan; /* pointer for scanning the v values */
  PetscScalar *bscan;   /* pointer for scanning the b values */

  PetscScalar tmp;      /* temporary variable */

  int nnfa/*, nvar, maxcomp*/, *nfa/*, nsd*/, nfields;
  int inod, nodloc, anfa;
  int nbc, *ibcc;
  systemdata   *Sy;

  Sy = *fSy;
  b = *fb;
  v = *fv;
  A = *fA;
  isbc = *fisbc;

  if (!A && !b && !v)
    return;

  /* Take care of this in the case that Pa->nnpe were 
     different from Sq->maxnpe */
  /* Note: 
     The nsd, nvar and maxcomp values of Pa and SurPa should be the same,
     so, there is no verification of consistency at all. */
  Comm    = Sy->Pa->Comm;
  /* nsd     = Sy->Pa->nsd; */
  /* maxcomp = Sy->Sq->maxcomp; */
  /* nvar    = Sy->Sq->nvar; */
  nfields = Sy->nfields;
  nodloc  = Sy->Pa->nodloc;

  nnfa    = *fnnfa; 

  /* Filling nfa due to the fact that fnfa is in fortran numbering */
  nfa = (int *) malloc(nnfa * sizeof(int));
  if (nfa == (int *)NULL)
    error (EXIT, "Not enough memory for nfa");

  for (i = 0; i < nnfa; i++)
    nfa[i] = fnfa[i] - 1;
  /*
  if (A)
    printf("Setting B.C. on matrix");
  if (b)
    printf("Setting B.C. on RHS");
  if (v)
    printf("Setting B.C. on solution");
  */
  /*
   * Put boundary conditions on vectors b and/or v
   */

  if (v)
    {ierr = VecGetArray (v, &varscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);}
  if (b)
    {ierr = VecGetArray (b, &bscan);    CHKERRABORT(PETSC_COMM_WORLD,ierr);}

  if (b || v)
    {
      /*      printf("starting loop over nodloc...\n");  */
    for (inod = 0; inod < nodloc; inod++)
      {
	for (i = 0; i < nnfa; i++)
	  {
	    anfa = nfa[i] < 0 ? -(nfa[i]+1)-1 : nfa[i];
	    ipos = inod * nfields + anfa;
	    if (Sy->bcccond[ipos])
	      {
		tmp = Sy->bccvalue[ipos];
		if (b)
		  bscan[inod*nnfa+i] = tmp;
		if (v)
		  varscan[ipos] = tmp;
		/* bscan[] = tmp - varscan[ipos]; */
	      }
	  }
      }
    }
  if (v)
    {ierr = VecRestoreArray (v, &varscan); CHKERRABORT(PETSC_COMM_WORLD,ierr);}
  if (b)
    {ierr = VecRestoreArray (b, &bscan);   CHKERRABORT(PETSC_COMM_WORLD,ierr);}

  /*
   * Put boundary conditions on matrix A
   */
  if (A)
    {
      if (isbc == PETSC_NULL)
	{
	  /* count number of boundary conditions */
	  nbc = 0;
	  for (inod = 0; inod < nodloc; inod++)
	    {
	      for (i = 0; i < nnfa; i++)
		{
		  anfa = nfa[i] < 0 ? -(nfa[i]+1)-1 : nfa[i];
		  ipos = inod * nfields + anfa;
		  if (Sy->bcccond[ipos])
		    nbc++;
		}
	    }
	  ibcc = (int *) malloc (nbc * sizeof(int));
	  /* load vector of boundary conditions */
	  nbc = 0;
	  for (inod = 0; inod < nodloc; inod++)
	    {
	      for (i = 0; i < nnfa; i++)
		{
		  anfa = nfa[i] < 0 ? -(nfa[i]+1)-1 : nfa[i];
		  ipos = inod * nfields + anfa;
		  if (Sy->bcccond[ipos])
		    ibcc[nbc++] = inod*nnfa + i + nnfa*(Sy->Pa->first-1);
		}
	    }
	  ISCreateGeneral(Comm, nbc, ibcc, PETSC_COPY_VALUES, &isbc);
	  free (ibcc);
	  *fisbc = isbc;
	}

      tmp = 1.0;
      MatZeroRowsIS (A, isbc, tmp, 0, 0);
    }
  free (nfa);
}

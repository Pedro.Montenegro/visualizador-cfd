#include <stdlib.h>
#include "assembly.h"
#include "prof_utils.h"
#include "petsclog.h"

/*
 * Coordinate correction for periodic meshes
 * coordaux[n + i * nnpe]: coordinate "i" of the node "n"
 * nnpe: number of nodes per element
 * nsd: number of space dimensions
 * rl[i]: if > 0: size of the periodic mesh in the "i" direction
 *        if < 0: no periodicity in the "i" direction
 */
int periodic_corr(PetscScalar *coordaux, int nnpe, int nsd, double *rl)
{
  int i, j;
  double aux, width;

  for (i = 0; i < nsd; i++)
    if (rl[i] > 0)
      {
	width = fabs(coordaux[0+i*nnpe] - coordaux[1+i*nnpe]);
	for (j = 2; j<nnpe; j++)
	  {
	    aux = fabs(coordaux[0+i*nnpe] - coordaux[j+i*nnpe]);
	    if (aux > width)
	      width = aux;
	  }
	if (width > rl[i]/2.0)
	  for (j = 0; j <nnpe; j++)
	    if (coordaux[j+i*nnpe] < rl[i]/2.0)
	      coordaux[j+i*nnpe] = coordaux[j+i*nnpe] + rl[i];
      }
  return 0;
}

/* Interdist: distance for the nodes in the interface element */
/* computes distance point - plane                            */
/* coords[i*nsd + j]: Coordinate "j" of the "i"th node        */
/* nsd: Number of space dimensions                            */
/* v: Characteristic function in each node                    */
/* lv: Level Value                                            */
/* dists: Result                                              */
void interdist_p (double *coords, int nsd, double *v, double lv, double *dists)
{
  static int ftime = 1;
  static double *edgematrix, *gradmatrix;

  int i, ip1, ip2;
  int j, jp1, jp2;
  int nnpe;
  double vol, invvol;
  double gradcomp, gradmod;
  double *em, *gm;

  /* Simplex: number of nodes per element: number of space dimensions + 1 */
  nnpe = nsd + 1;

  if (ftime) /* Alloc memory only the first time it's invoked */
    {
      edgematrix = (double *) malloc (nsd*nsd*sizeof (double));
      gradmatrix = (double *) malloc ((nsd+1)*nsd*sizeof (double));
      ftime = 0;
    }
  em = edgematrix;
  gm = gradmatrix;

  /* Edge matrix:
   * edgematrix[j+i*nsd] : edge (0,i+1), component j
   */
  for (i = 0; i < nsd; i++) /* loop over nodes (except first) */
    for (j = 0; j < nsd; j++) /* loop over dimensions */
      edgematrix[j+i*nsd] = coords[(i+1)+j*nnpe] - coords[j*nnpe];
  
  /* Gradient matrix:
   * gradmatrix[j+i*nsd]: component 'j' of the grad(phi(node 'i'))
   *                     (times the volume)
   */

  if (nsd == 1)
    {
      gradmatrix[1] = 1.0;
      gradmatrix[0] = 0.0 - gm[1];
    }
  else if (nsd == 2)
    {
      gradmatrix[2] = em[3];
      gradmatrix[3] = -em[2];
      gradmatrix[4] = -em[1];
      gradmatrix[5] = em[0];

      gradmatrix[0] = 0.0 - gm[2] - gm[4];
      gradmatrix[1] = 0.0 - gm[3] - gm[5];
    }
  else if (nsd == 3)
    {
      for (j = 0; j < 3; j++)
	{
	  gradmatrix[j] = 0.0;
	  for (i = 0; i < 3; i++)
	    {
	      ip1 = (i+1)%3;   ip2 = (i+2)%3;
	      jp1 = (j+1)%3;   jp2 = (j+2)%3;
	      
	      gradmatrix[(i+1)*3+j] = (em[ip1*3+jp1]*em[ip2*3+jp2]-
				       em[ip1*3+jp2]*em[ip2*3+jp1]);
	      gradmatrix[j] -= gradmatrix[(i+1)*3+j];
	    }
	}
	  
      /*
	gradmatrix[1*3+0] = em[1*3+1]*em[2*3+2]-em[1*3+2]*em[2*3+1];
	gradmatrix[1*3+1] = em[1*3+2]*em[2*3+0]-em[1*3+0]*em[2*3+2];
	gradmatrix[1*3+2] = em[1*3+0]*em[2*3+1]-em[1*3+1]*em[2*3+0];
	gradmatrix[2*3+0] = em[2*3+1]*em[0*3+2]-em[2*3+2]*em[0*3+1];
	gradmatrix[2*3+1] = em[2*3+2]*em[0*3+0]-em[2*3+0]*em[0*3+2];
	gradmatrix[2*3+2] = em[2*3+0]*em[0*3+1]-em[2*3+1]*em[0*3+0];
	gradmatrix[3*3+0] = em[0*3+1]*em[1*3+2]-em[0*3+2]*em[1*3+1];
	gradmatrix[3*3+1] = em[0*3+2]*em[1*3+0]-em[0*3+0]*em[1*3+2];
	gradmatrix[3*3+2] = em[0*3+0]*em[1*3+1]-em[0*3+1]*em[1*3+0];
      */
    }
  
  /* Compute volume (determinant by 1st row) */
  vol = 0;
  for (i = 0; i < nsd; i++)
    vol += em[i]*gm[i];
  invvol = 1.0 / vol;

  /* Compute the gradient of "v" */
  gradmod = 0.0;
  for (j = 0; j < nsd; j++)
    {
      gradcomp = 0.0;
      for (i = 0; i < nnpe; i++)
	gradcomp += v[i]*gm[j+i*nsd]*invvol;
      gradmod += gradcomp*gradcomp;
    }
  gradmod = sqrt (gradmod);

  /* dists[i]: idem v, but with gradient module = 1.0 */
  for (i = 0; i < nnpe; i++)
    dists[i] = (v[i] - lv) / gradmod;
}

void nods2poldist2d (double ax, double ay,
		     double bx, double by,
		     double cx, double cy,
		     double dx, double dy,  int nv,
		     double n0x, double n0y,
		     double n1x, double n1y,
		     double n2x, double n2y,
		     double n3x, double n3y,
		     double *d0,  double *d1,  double *d2,  double *d3)
{
  double area, diam, dmin, dmax, dab, dac, dbc, dcd, dda=0.0;
  double ablen2, pe, pv, sproj;
  double nnx=0.0, nny=0.0, *dd=NULL, s0x=0.0, s0y=0.0, s1x=0.0, s1y=0.0, sl=0.0;
  double ddd;
  int node, side, inside, sign;
  /* Compute area */
  area = (bx-ax)*(cy-ay)-(by-ay)*(cx-ax);
  if (nv == 4)
    area += (cx-ax)*(dy-ay)-(cy-ay)*(dx-ax);
  sign = (area < 0.0)? -1: +1;
	  
  diam = fabs(n0x-n1x) + fabs(n0x-n2x) + fabs(n0x-n3x)
    + fabs(n1x-n2x) + fabs(n1x-n3x) + fabs(n2x-n3x);

  if (sign*area < 1.0e-15 * diam*diam)
    {
      dab = sqrt((ax-bx)*(ax-bx)+(ay-by)*(ay-by));
      dac = sqrt((ax-cx)*(ax-cx)+(ay-cy)*(ay-cy));
      dmax = dab > dac ? dab : dac;
      if (dmax < 1.0e-08 * diam) /* Triangle => point */
	{
	  *d0 = sqrt((n0x-ax)*(n0x-ax) + (n0y-ay)*(n0y-ay));
	  *d1 = sqrt((n1x-ax)*(n1x-ax) + (n1y-ay)*(n1y-ay));
	  *d2 = sqrt((n2x-ax)*(n2x-ax) + (n2y-ay)*(n2y-ay));
	  *d3 = sqrt((n3x-ax)*(n3x-ax) + (n3y-ay)*(n3y-ay));
	  return;
	}
      /* Quadrilateral => segment */
      if(dab < dac)
	{
	  bx = cx; by = cy;
	}
      ablen2 = (bx-ax)*(bx-ax) + (by-ay)*(by-ay);
      /* Point "0" projection */
      pe = (n0x-ax) * (bx-ax) + (n0y-ay) * (by-ay);
      pv = (n0x-ax) * (by-ay) - (n0y-ay) * (bx-ax);
      sproj = pe / ablen2;
      if (sproj < 0)
	*d0 = sqrt((n0x-ax)*(n0x-ax) + (n0y-ay)*(n0y-ay));
      else if (sproj > 1)
	*d0 = sqrt((n0x-bx)*(n0x-bx) + (n0y-by)*(n0y-by));
      else
	*d0 = sqrt(pv*pv/ablen2);
      /* Point "1" projection */
      pe = (n1x-ax) * (bx-ax) + (n1y-ay) * (by-ay);
      pv = (n1x-ax) * (by-ay) - (n1y-ay) * (bx-ax);
      sproj = pe / ablen2;
      if (sproj < 0)
	*d1 = sqrt((n1x-ax)*(n1x-ax) + (n1y-ay)*(n1y-ay));
      else if (sproj > 1)
	*d1 = sqrt((n1x-bx)*(n1x-bx) + (n1y-by)*(n1y-by));
      else
	*d1 = sqrt(pv*pv/ablen2);
      /* Point "2" projection */
      pe = (n2x-ax) * (bx-ax) + (n2y-ay) * (by-ay);
      pv = (n2x-ax) * (by-ay) - (n2y-ay) * (bx-ax);
      sproj = pe / ablen2;
      if (sproj < 0)
	*d2 = sqrt((n2x-ax)*(n2x-ax) + (n2y-ay)*(n2y-ay));
      else if (sproj > 1)
	*d2 = sqrt((n2x-bx)*(n2x-bx) + (n2y-by)*(n2y-by));
      else
	*d2 = sqrt(pv*pv/ablen2);
      /* Point "3" projection */
      pe = (n3x-ax) * (bx-ax) + (n3y-ay) * (by-ay);
      pv = (n3x-ax) * (by-ay) - (n3y-ay) * (bx-ax);
      sproj = pe / ablen2;
      if (sproj < 0)
	*d3 = sqrt((n3x-ax)*(n3x-ax) + (n3y-ay)*(n3y-ay));
      else if (sproj > 1)
	*d3 = sqrt((n3x-bx)*(n3x-bx) + (n3y-by)*(n3y-by));
      else
	*d3 = sqrt(pv*pv/ablen2);
      return;
    } /* End area ~ 0 */

  if (nv == 3) { dx = ax; dy = ay; }

  /* Check for quadrilateral with two coincident nodes */
  dab = sqrt((ax-bx)*(ax-bx)+(ay-by)*(ay-by));
  dbc = sqrt((bx-cx)*(bx-cx)+(by-cy)*(by-cy));
  dcd = sqrt((dx-cx)*(dx-cx)+(dy-cy)*(dy-cy));
  if (nv == 4)
    {
      dda = sqrt((dx-ax)*(dx-ax)+(dy-ay)*(dy-ay));
      if (dab < 1.0e-08 * diam)
	{
	  bx = cx; by = cy;
	  cx = dx; cy = dy;
	  nv = 3;
	}
      if (dbc < 1.0e-08 * diam)
	{
	  cx = dx; cy = dy;
	  nv = 3;
	}
      if (dcd < 1.0e-08 * diam)
	nv = 3;
    }

  /* Distance from polygon to points */
  for (node = 0; node < 4; node++)
    {
      if (node == 0) { nnx = n0x; nny = n0y; dd = d0; }
      if (node == 1) { nnx = n1x; nny = n1y; dd = d1; }
      if (node == 2) { nnx = n2x; nny = n2y; dd = d2; }
      if (node == 3) { nnx = n3x; nny = n3y; dd = d3; }
      inside = 1;
      dmin = diam;
      for (side = 0; side < nv; side++)
	{
	  if (side == 0) {s0x = ax; s0y = ay; s1x = bx; s1y = by; sl = dab;}
	  if (side == 1) {s0x = bx; s0y = by; s1x = cx; s1y = cy; sl = dbc;}
	  if (side == 2) {s0x = cx; s0y = cy; s1x = dx; s1y = dy; sl = dcd;}
	  if (side == 3) {s0x = dx; s0y = dy; s1x = ax; s1y = ay; sl = dda;}

	  /* Check on which side of side is node */
	  pv = (s1x-s0x)*(nny-s0y)-(s1y-s0y)*(nnx-s0x);
	  if (sign*pv < 0)
	    {
	      inside = 0;
	      pe = (nnx-s0x) * (s1x-s0x) + (nny-s0y) * (s1y-s0y);
	      sproj = pe / (sl*sl);
	      if (sproj < 0)
		ddd = sqrt((nnx-s0x)*(nnx-s0x)+(nny-s0y)*(nny-s0y));
	      else if (sproj > 1)
		ddd = sqrt((nnx-s1x)*(nnx-s1x)+(nny-s1y)*(nny-s1y));
	      else
		ddd = -sign*pv / sl;
	      if (ddd < dmin)
		dmin = ddd;
	    }
	}
      if (inside)
	dmin = 0.0;
      *dd = dmin;
    }
  return;
} /* End function nods2poldist2d */

/* Interdist: distance for the nodes in the interface element */
/* computes distance point - triangle                         */
/* coords[i*nsd + j]: Coordinate "j" of the "i"th node        */
/* nsd: Number of space dimensions                            */
/* v: Characteristic function in each node                    */
/* lv: Level Value                                            */
/* dists: Result                                              */
void interdist (double *coords, int nsd, double *v, double lv, double *dists,
		double *parvol)
{
  static int ftime = 1;
  static double *edgematrix, *gradmatrix;

  int i, ip1, ip2;
  int j, jp1, jp2;
  int nnpe;
  double vol, invvol;
  double gradcomp[3], gradmod;
  double *em, *gm;

  /* Simplex: number of nodes per element: number of space dimensions + 1 */
  nnpe = nsd + 1;

  if (ftime) /* Alloc memory only the first time it's invoked */
    {
      edgematrix = (double *) malloc (nsd*nsd*sizeof (double));
      gradmatrix = (double *) malloc ((nsd+1)*nsd*sizeof (double));
      ftime = 0;
    }
  em = edgematrix;
  gm = gradmatrix;

  /* Edge matrix:
   * edgematrix[j+i*nsd] : edge (0,i+1), component j
   */
  for (i = 0; i < nsd; i++) /* loop over nodes (except first) */
    for (j = 0; j < nsd; j++) /* loop over dimensions */
      edgematrix[j+i*nsd] = coords[(i+1)+j*nnpe] - coords[j*nnpe];
  
  /* Gradient matrix:
   * gradmatrix[j+i*nsd]: component 'j' of the grad(phi(node 'i'))
   *                     (times the volume)
   */

  if (nsd == 1)
    {
      gradmatrix[1] = 1.0;
      gradmatrix[0] = 0.0 - gm[1];
    }
  else if (nsd == 2)
    {
      gradmatrix[2] = em[3];
      gradmatrix[3] = -em[2];
      gradmatrix[4] = -em[1];
      gradmatrix[5] = em[0];

      gradmatrix[0] = 0.0 - gm[2] - gm[4];
      gradmatrix[1] = 0.0 - gm[3] - gm[5];
    }
  else if (nsd == 3)
    {
      for (j = 0; j < 3; j++)
	{
	  gradmatrix[j] = 0.0;
	  for (i = 0; i < 3; i++)
	    {
	      ip1 = (i+1)%3;   ip2 = (i+2)%3;
	      jp1 = (j+1)%3;   jp2 = (j+2)%3;
	      
	      gradmatrix[(i+1)*3+j] = (em[ip1*3+jp1]*em[ip2*3+jp2]-
				       em[ip1*3+jp2]*em[ip2*3+jp1]);
	      gradmatrix[j] -= gradmatrix[(i+1)*3+j];
	    }
	}
	  
      /*
	gradmatrix[1*3+0] = em[1*3+1]*em[2*3+2]-em[1*3+2]*em[2*3+1];
	gradmatrix[1*3+1] = em[1*3+2]*em[2*3+0]-em[1*3+0]*em[2*3+2];
	gradmatrix[1*3+2] = em[1*3+0]*em[2*3+1]-em[1*3+1]*em[2*3+0];
	gradmatrix[2*3+0] = em[2*3+1]*em[0*3+2]-em[2*3+2]*em[0*3+1];
	gradmatrix[2*3+1] = em[2*3+2]*em[0*3+0]-em[2*3+0]*em[0*3+2];
	gradmatrix[2*3+2] = em[2*3+0]*em[0*3+1]-em[2*3+1]*em[0*3+0];
	gradmatrix[3*3+0] = em[0*3+1]*em[1*3+2]-em[0*3+2]*em[1*3+1];
	gradmatrix[3*3+1] = em[0*3+2]*em[1*3+0]-em[0*3+0]*em[1*3+2];
	gradmatrix[3*3+2] = em[0*3+0]*em[1*3+1]-em[0*3+1]*em[1*3+0];
      */
    }
  
  /* Compute volume (determinant by 1st row) */
  vol = 0;
  for (i = 0; i < nsd; i++)
    vol += em[i]*gm[nsd+i];
  invvol = 1.0 / vol;

  /* Compute the gradient of "v" */
  gradmod = 0.0;
  for (j = 0; j < nsd; j++)
    {
      gradcomp[j] = 0.0;
      for (i = 0; i < nnpe; i++)
	gradcomp[j] += v[i]*gm[j+i*nsd]*invvol;
      gradmod += gradcomp[j]*gradcomp[j];
    }
  gradmod = sqrt (gradmod);

  /* dists[i]: idem v, but with gradient module = 1.0 */
  for (i = 0; i < nnpe; i++)
    dists[i] = (v[i] - lv) / gradmod;

  /* Check if distances to plane are real distances to the simplex */
  if (nsd == 1)
    {
      double s10;
      s10 = dists[1] / (dists[1] - dists[0]);
      if (v[0] > lv)
	*parvol = s10 * vol;
      else
	*parvol = (1.0 - s10) * vol;
      return;
    }
  else if (nsd == 2)
    {
      double ax=0.0,ay=0.0, bx=0.0,by=0.0, intmod2; /* Interface points */
      double pe, sproj, s10=0.0, s21=0.0, s02=0.0;
      int id[3];    /* nodal indicator: 1 or -1 if up or down the level */
      for (i = 0; i < 3; i++)
	{
	  if (v[i] > lv)
	    id[i] = 1;
	  else
	    id[i] = -1;
	}
      i = 0;  /* Bit-wise indicator: 1: side 0-1, 2: side 1-2, 4: side 2-0 */
      if (id[0] * id[1] < 0)      /* Level cross side 0-1 */
	{
	  s10 = dists[1] / (dists[1] - dists[0]);
	  ax = coords[0*nnpe+0] * s10 + coords[0*nnpe+1] * (1.0 - s10);
	  ay = coords[1*nnpe+0] * s10 + coords[1*nnpe+1] * (1.0 - s10);
	  i++;
	}
      if (id[1] * id[2] < 0)      /* Level cross side 1-2 */
	{
	  s21 = dists[2] / (dists[2] - dists[1]);
	  bx = coords[0*nnpe+1] * s21 + coords[0*nnpe+2] * (1.0 - s21);
	  by = coords[1*nnpe+1] * s21 + coords[1*nnpe+2] * (1.0 - s21);
	  if (i == 0)
	    { ax = bx; ay = by; }
	  i += 2;
	}
      if (id[2] * id[0] < 0)      /* Level cross side 2-0 */
	{
	  s02 = dists[0] / (dists[0] - dists[2]);
	  bx = coords[0*nnpe+2] * s02 + coords[0*nnpe+0] * (1.0 - s02);
	  by = coords[1*nnpe+2] * s02 + coords[1*nnpe+0] * (1.0 - s02);
	  i += 4;
	}
      if (i == 5)       /* node 0 / node 1, node 2 */
	{
	  *parvol = (1.0 - s10) * s02;
	  if (id[0] > 0)
	    *parvol *= vol * 0.5;
	  else
	    *parvol = (1 - *parvol) * vol * 0.5;
	}
      else if (i == 3)  /* node 1 / node 2, node 0 */
	{
	  *parvol = (1.0 - s21) * s10;
	  if (id[1] > 0)
	    *parvol *= vol * 0.5;
	  else
	    *parvol = (1 - *parvol) * vol * 0.5;
	}
      else if (i == 6)  /* node 2 / node 0, node 1 */
	{
	  *parvol = (1.0 - s02) * s21;
	  if (id[2] > 0)
	    *parvol *= vol * 0.5;
	  else
	    *parvol = (1 - *parvol) * vol * 0.5;
	}

      /* interface size squared */
      intmod2 = (bx-ax)*(bx-ax) + (by-ay)*(by-ay);
      if (intmod2 < 1.0e-15 * vol) /* Very small, very near to a node */
	{
	  if (i & 1) /* edge 0-1 crossed */
	    dists[2] = id[2] *
	      ((i & 4) ? /* also 0-2: size of 0-2 */
	       (sqrt(em[2]*em[2]+em[3]*em[3])) : /* else: size of 1-2 */
	       (sqrt((em[2]-em[0])*(em[2]-em[0])+(em[3]-em[1])*(em[3]-em[1]))));
	  if (i & 2) /* edge 1-2 crossed */
	    dists[0] = id[0] *
	      ((i & 4) ? /* also 0-2: size of 0-2 */
	       (sqrt(em[2]*em[2]+em[3]*em[3])) :
	       (sqrt(em[0]*em[0]+em[1]*em[1]))); /* else: size of 0-1 */
	  if (i & 4) /* edge 2-0 crossed */
	    dists[1] = id[1] *
	      ((i & 1) ? /* also 0-1: size of 0-1 */
	       (sqrt(em[0]*em[0]+em[1]*em[1])): /* else: size of 1-2 */
	       (sqrt((em[2]-em[0])*(em[2]-em[0])+(em[3]-em[1])*(em[3]-em[1]))));
	}
      /* Point "0" projection */
      pe = (coords[0*nnpe+0]-ax) * (bx-ax) + (coords[1*nnpe+0]-ay) * (by-ay);
      sproj = pe / intmod2;
      if (sproj < 0)
	dists[0] = id[0] * sqrt((coords[0*nnpe+0]-ax)*(coords[0*nnpe+0]-ax) +
				(coords[1*nnpe+0]-ay)*(coords[1*nnpe+0]-ay));
      else if (sproj > 1)
	dists[0] = id[0] * sqrt((coords[0*nnpe+0]-bx)*(coords[0*nnpe+0]-bx) +
				(coords[1*nnpe+0]-by)*(coords[1*nnpe+0]-by));
      /* Point "1" projection */
      pe = (coords[0*nnpe+1]-ax) * (bx-ax) + (coords[1*nnpe+1]-ay) * (by-ay);
      sproj = pe / intmod2;
      if (sproj < 0)
	dists[1] = id[1] * sqrt((coords[0*nnpe+1]-ax)*(coords[0*nnpe+1]-ax) +
				(coords[1*nnpe+1]-ay)*(coords[1*nnpe+1]-ay));
      else if (sproj > 1)
	dists[1] = id[1] * sqrt((coords[0*nnpe+1]-bx)*(coords[0*nnpe+1]-bx) +
				(coords[1*nnpe+1]-by)*(coords[1*nnpe+1]-by));
      /* Point "2" projection */
      pe = (coords[0*nnpe+2]-ax) * (bx-ax) + (coords[1*nnpe+2]-ay) * (by-ay);
      sproj = pe / intmod2;
      if (sproj < 0)
	dists[2] = id[2] * sqrt((coords[0*nnpe+2]-ax)*(coords[0*nnpe+2]-ax) +
				(coords[1*nnpe+2]-ay)*(coords[1*nnpe+2]-ay));
      else if (sproj > 1)
	dists[2] = id[2] * sqrt((coords[0*nnpe+2]-bx)*(coords[0*nnpe+2]-bx) +
				(coords[1*nnpe+2]-by)*(coords[1*nnpe+2]-by));
    }
  else /* nsd == 3 */
    {
      double ax=0.0,ay=0.0,az=0.0, bx=0.0,by=0.0,bz=0.0,
	cx=0.0,cy=0.0,cz=0.0, dx=0.0,dy=0.0,dz=0.0; /* Interface points */
      static double elength[4][4]; /* Edge lengths */
      double daux;
      double ix, iy, iz, jx, jy, jz, kx, ky, kz;
      double aax,aay, bbx,bby, ccx,ccy, ddx,ddy, zzz;
      double n0x,n0y,n0z, n1x,n1y,n1z, n2x,n2y,n2z, n3x,n3y,n3z;
      double d0, d1, d2, d3;
      double s30=0.0, s31=0.0, s32=0.0, s02=0.0, s12=0.0, s10=0.0,
	ese1, ese2, ese3, ese4;
      int id[4];

      for (i = 0; i < 4; i++)
	{
	  if (v[i] > lv)
	    id[i] = 1;
	  else
	    id[i] = -1;
	  elength[i][i] = 0;
	  for (j = i+1; j < 4; j++)
	    {
	      dx = coords[0*nnpe+i]-coords[0*nnpe+j];
	      dy = coords[1*nnpe+i]-coords[1*nnpe+j];
	      dz = coords[2*nnpe+i]-coords[2*nnpe+j];
	      elength[i][j] = sqrt(dx*dx+dy*dy+dz*dz);
	      elength[j][i] = elength[i][j];
	    }
	}
      i = j = 0;
      if (id[0] * id[3] < 0)
	{
	  s30 = dists[3] / (dists[3] - dists[0]);
	  ax = coords[0*nnpe+0] * s30 + coords[0*nnpe+3] * (1.0 - s30);
	  ay = coords[1*nnpe+0] * s30 + coords[1*nnpe+3] * (1.0 - s30);
	  az = coords[2*nnpe+0] * s30 + coords[2*nnpe+3] * (1.0 - s30);
	  j++; i += 1;
	}
      if (id[1] * id[3] < 0)
	{
	  s31 = dists[3] / (dists[3] - dists[1]);
	  bx = coords[0*nnpe+1] * s31 + coords[0*nnpe+3] * (1.0 - s31);
	  by = coords[1*nnpe+1] * s31 + coords[1*nnpe+3] * (1.0 - s31);
	  bz = coords[2*nnpe+1] * s31 + coords[2*nnpe+3] * (1.0 - s31);
	  if (j == 0) { ax = bx; ay = by; az = bz; }
	  j++; i += 2;
	}
      if (id[2] * id[3] < 0)
	{
	  s32 = dists[3] / (dists[3] - dists[2]);
	  cx = coords[0*nnpe+2] * s32 + coords[0*nnpe+3] * (1.0 - s32);
	  cy = coords[1*nnpe+2] * s32 + coords[1*nnpe+3] * (1.0 - s32);
	  cz = coords[2*nnpe+2] * s32 + coords[2*nnpe+3] * (1.0 - s32);
	  if (j == 0)
	    { ax = cx; ay = cy; az = cz; }
	  else if (j == 1)
	    { bx = cx; by = cy; bz = cz; }
	  j++; i += 4;
	}
      if (id[2] * id[0] < 0)
	{
	  s02 = dists[0] / (dists[0] - dists[2]);
	  cx = coords[0*nnpe+2] * s02 + coords[0*nnpe+0] * (1.0 - s02);
	  cy = coords[1*nnpe+2] * s02 + coords[1*nnpe+0] * (1.0 - s02);
	  cz = coords[2*nnpe+2] * s02 + coords[2*nnpe+0] * (1.0 - s02);
	  if (j == 1)
	    { bx = cx; by = cy; bz = cz; }
	  j++; i += 8;
	}
      if (id[2] * id[1] < 0)
	{
	  s12 = dists[1] / (dists[1] - dists[2]);
	  dx = coords[0*nnpe+2] * s12 + coords[0*nnpe+1] * (1.0 - s12);
	  dy = coords[1*nnpe+2] * s12 + coords[1*nnpe+1] * (1.0 - s12);
	  dz = coords[2*nnpe+2] * s12 + coords[2*nnpe+1] * (1.0 - s12);
	  if (j == 1)
	    { bx = dx; by = dy; bz = dz; }
	  else if (j == 2)
	    { cx = dx; cy = dy; cz = dz; }
	  j++; i += 16;
	}
      if (id[0] * id[1] < 0)
	{
	  s10 = dists[1] / (dists[1] - dists[0]);
	  dx = coords[0*nnpe+0] * s10 + coords[0*nnpe+1] * (1.0 - s10);
	  dy = coords[1*nnpe+0] * s10 + coords[1*nnpe+1] * (1.0 - s10);
	  dz = coords[2*nnpe+0] * s10 + coords[2*nnpe+1] * (1.0 - s10);
	  if (j == 2)
	    { cx = dx; cy = dy; cz = dz; }
	  j++; i += 32;
	}

      /* Interface points computed, fix orientation in quadrilateral case */
      if (j == 4)
	{
	  if (i == 1+2+8+16) /* 0 1  /  2 3 */
	    { /* a: 03 b: 13 c: 02 d: 12 */
	      daux = cx; cx = dx; dx = daux;
	      daux = cy; cy = dy; dy = daux;
	      daux = cz; cz = dz; dz = daux;

	      ese1 = 1.0 - s30;
	      ese2 = s02;
	      ese3 = 1.0 - s31;
	      ese4 = s12;
	    }
	  else if (i == 1+4+16+32 ) /* 0 2  /  1 3 */
	    { /* a: 03 b: 23 c: 12 d: 01 */
	      ese1 = 1.0 - s10;
	      ese2 = 1.0 - s30;
	      ese3 = 1.0 - s12;
	      ese4 = 1.0 - s32;
	    }
	  else /* (i == 2+4+8+32)  ->  0 3  /  1 2 */
	    { /* a: 13 b: 23 c: 02 d: 01 */
	      ese1 = 1.0 - s10;
	      ese2 = s02;
	      ese3 = s31;
	      ese4 = s32;
	    }
	  *parvol = ese1*ese2 + ese3*ese4 + ese1*ese4
	    - ese1*ese3*ese4 - ese1*ese2*ese4;
	  if (id[0] > 0)
	    *parvol *= vol / 6.0;
	  else
	    *parvol = (1 - *parvol) * vol / 6.0;
	}
      else /* one node in one side, three on the other side */
	{
	  if (i == 1+8+32) /* Node 0 */
	    {
	      *parvol = (1.0 - s10) * s02 * (1.0 - s30);
	      if (id[0] > 0)
		*parvol *= vol / 6.0;
	      else
		*parvol = (1 - *parvol) * vol / 6.0;
	    }
	  else if (i == 2+16+32) /* Node 1 */
	    {
	      *parvol = s10 * s12 * (1.0 - s31);
	      if (id[1] > 0)
		*parvol *= vol / 6.0;
	      else
		*parvol = (1 - *parvol) * vol / 6.0;
	    }
	  else if (i == 4+8+16) /* Node 2 */
	    {
	      *parvol = (1.0 - s02) * (1.0 - s12) * (1.0 - s32);
	      if (id[2] > 0)
		*parvol *= vol / 6.0;
	      else
		*parvol = (1 - *parvol) * vol / 6.0;
	    }
	  else /* if (i == 1+2+4)  Node 3 */
	    {
	      *parvol = s30 * s31 * s32;
	      if (id[3] > 0)
		*parvol *= vol / 6.0;
	      else
		*parvol = (1 - *parvol) * vol / 6.0;
	    }
	}
      /* Rotate coordinates, compute new base */
      kx = gradcomp[0]/gradmod;
      ky = gradcomp[1]/gradmod;
      kz = gradcomp[2]/gradmod;
      if (fabs(kx) > fabs(ky))
	{ /* (0,1,0) x gradient */
	  ix = kz/sqrt(kz*kz+kx*kx);
	  iy = 0;
	  iz = -kx/sqrt(kz*kz+kx*kx);
	}
      else
	{ /* (1,0,0) x gradient */
	  ix = 0;
	  iy = -kz/sqrt(kz*kz+ky*ky);
	  iz = ky/sqrt(kz*kz+ky*ky);
	}
      jx = ky*iz - kz*iy;
      jy = kz*ix - kx*iz;
      jz = kx*iy - ky*ix;

      /* Rotate interface, nodes */
      aax =ax*ix+ay*iy+az*iz; aay =ax*jx+ay*jy+az*jz;
      bbx =bx*ix+by*iy+bz*iz; bby =bx*jx+by*jy+bz*jz;
      ccx =cx*ix+cy*iy+cz*iz; ccy =cx*jx+cy*jy+cz*jz;
      ddx =dx*ix+dy*iy+dz*iz; ddy =dx*jx+dy*jy+dz*jz;
      zzz =ax*kx+ay*ky+az*kz;
      n0x =coords[0*nnpe+0]*ix+coords[1*nnpe+0]*iy+coords[2*nnpe+0]*iz;
      n0y =coords[0*nnpe+0]*jx+coords[1*nnpe+0]*jy+coords[2*nnpe+0]*jz;
      n0z =coords[0*nnpe+0]*kx+coords[1*nnpe+0]*ky+coords[2*nnpe+0]*kz-zzz;
      n1x =coords[0*nnpe+1]*ix+coords[1*nnpe+1]*iy+coords[2*nnpe+1]*iz;
      n1y =coords[0*nnpe+1]*jx+coords[1*nnpe+1]*jy+coords[2*nnpe+1]*jz;
      n1z =coords[0*nnpe+1]*kx+coords[1*nnpe+1]*ky+coords[2*nnpe+1]*kz-zzz;
      n2x =coords[0*nnpe+2]*ix+coords[1*nnpe+2]*iy+coords[2*nnpe+2]*iz;
      n2y =coords[0*nnpe+2]*jx+coords[1*nnpe+2]*jy+coords[2*nnpe+2]*jz;
      n2z =coords[0*nnpe+2]*kx+coords[1*nnpe+2]*ky+coords[2*nnpe+2]*kz-zzz;
      n3x =coords[0*nnpe+3]*ix+coords[1*nnpe+3]*iy+coords[2*nnpe+3]*iz;
      n3y =coords[0*nnpe+3]*jx+coords[1*nnpe+3]*jy+coords[2*nnpe+3]*jz;
      n3z =coords[0*nnpe+3]*kx+coords[1*nnpe+3]*ky+coords[2*nnpe+3]*kz-zzz;

      nods2poldist2d(aax,aay, bbx,bby, ccx,ccy, ddx,ddy, j,
		     n0x,n0y, n1x,n1y, n2x,n2y, n3x,n3y,
		     &d0, &d1, &d2, &d3);
      dists[0] = id[0] * sqrt(n0z*n0z + d0*d0);
      dists[1] = id[1] * sqrt(n1z*n1z + d1*d1);
      dists[2] = id[2] * sqrt(n2z*n2z + d2*d2);
      dists[3] = id[3] * sqrt(n3z*n3z + d3*d3);
    } /* end if (nsd == 3) */
} /* end interdist function */


/* Parvol: comput partial volume in the interface element     */
/* coords[i*nsd + j]: Coordinate "j" of the "i"th node        */
/* nsd: Number of space dimensions                            */
/* v: Characteristic function in each node (~distance)        */
double parvol (double *coords, int nsd, double *v, double *vref,
	       int *nintp, double *intpoints, double *sizeint)
{
  static int ftime = 1;
  double ParVol=0.0;
  static double *edgematrix, *gradmatrix;

  int i, ip1, ip2;
  int j, jp1, jp2;
  int nnpe;
  double vol/*, invvol*/;
  double *em, *gm;

  /* Simplex: number of nodes per element: number of space dimensions + 1 */
  nnpe = nsd + 1;

  if (ftime) /* Alloc memory only the first time it's invoked */
    {
      edgematrix = (double *) malloc (nsd*nsd*sizeof (double));
      gradmatrix = (double *) malloc ((nsd+1)*nsd*sizeof (double));
      ftime = 0;
    }
  em = edgematrix;
  gm = gradmatrix;

  /* Edge matrix:
   * edgematrix[j+i*nsd] : edge (0,i+1), component j
  */
  for (i = 0; i < nsd; i++) /* loop over nodes (except first) */
    for (j = 0; j < nsd; j++) /* loop over dimensions */
      edgematrix[j+i*nsd] = coords[(i+1)+j*nnpe] - coords[j*nnpe];
  
  /* Gradient matrix:
   * gradmatrix[j+i*nsd]: component 'j' of the grad(phi(node 'i'))
   *                     (times the volume)
   */

  if (nsd == 1)
    {
      gradmatrix[1] = 1.0;
      gradmatrix[0] = 0.0 - gm[1];
    }
  else if (nsd == 2)
    {
      gradmatrix[2] = em[3];
      gradmatrix[3] = -em[2];
      gradmatrix[4] = -em[1];
      gradmatrix[5] = em[0];

      gradmatrix[0] = 0.0 - gm[2] - gm[4];
      gradmatrix[1] = 0.0 - gm[3] - gm[5];
    }
  else if (nsd == 3)
    {
      for (j = 0; j < 3; j++)
	{
	  gradmatrix[j] = 0.0;
	  for (i = 0; i < 3; i++)
	    {
	      ip1 = (i+1)%3;   ip2 = (i+2)%3;
	      jp1 = (j+1)%3;   jp2 = (j+2)%3;
	      
	      gradmatrix[(i+1)*3+j] = (em[ip1*3+jp1]*em[ip2*3+jp2]-
				       em[ip1*3+jp2]*em[ip2*3+jp1]);
	      gradmatrix[j] -= gradmatrix[(i+1)*3+j];
	    }
	}
	  
      /*
	gradmatrix[1*3+0] = em[1*3+1]*em[2*3+2]-em[1*3+2]*em[2*3+1];
	gradmatrix[1*3+1] = em[1*3+2]*em[2*3+0]-em[1*3+0]*em[2*3+2];
	gradmatrix[1*3+2] = em[1*3+0]*em[2*3+1]-em[1*3+1]*em[2*3+0];
	gradmatrix[2*3+0] = em[2*3+1]*em[0*3+2]-em[2*3+2]*em[0*3+1];
	gradmatrix[2*3+1] = em[2*3+2]*em[0*3+0]-em[2*3+0]*em[0*3+2];
	gradmatrix[2*3+2] = em[2*3+0]*em[0*3+1]-em[2*3+1]*em[0*3+0];
	gradmatrix[3*3+0] = em[0*3+1]*em[1*3+2]-em[0*3+2]*em[1*3+1];
	gradmatrix[3*3+1] = em[0*3+2]*em[1*3+0]-em[0*3+0]*em[1*3+2];
	gradmatrix[3*3+2] = em[0*3+0]*em[1*3+1]-em[0*3+1]*em[1*3+0];
      */
    }
  
  /* Compute volume (determinant by 1st row) */
  vol = 0;
  for (i = 0; i < nsd; i++)
    vol += em[i]*gm[nsd+i];
  /* invvol = 1.0 / vol; */

  /* Compute (Interface points and) volume fraction */
  if (nsd == 1)
    {
      double s10;
      s10 = v[1] / (v[1] - v[0]);
      if (v[0] > 0)
	ParVol = s10 * vol;
      else
	ParVol = (1.0 - s10) * vol;
      if (nintp)
	*nintp = 1;
      if (intpoints)
	intpoints[0] = coords[0] * s10 + coords[1] * (1.0 - s10);
      if (sizeint)
	*sizeint = 1.0;
    }
  else if (nsd == 2)
    {
      double ax=0.0,ay=0.0, bx=0.0,by=0.0; /* Interface points */
      double s10=0.0, s21=0.0, s02=0.0;
      int id[3];
      for (i = 0; i < 3; i++)
	{
	  if (v[i] > 0.0)
	    id[i] = 1;
	  else
	    id[i] = -1;
	}
      i = 0;
      if (id[0] * id[1] < 0)
	{
	  s10 = v[1] / (v[1] - v[0]);
	  if (intpoints || sizeint) {
	    ax = coords[0*nnpe+0] * s10 + coords[0*nnpe+1] * (1.0 - s10);
	    ay = coords[1*nnpe+0] * s10 + coords[1*nnpe+1] * (1.0 - s10);
	  }
	  i++;
	}
      if (id[1] * id[2] < 0)
	{
	  s21 = v[2] / (v[2] - v[1]);
	  if (intpoints || sizeint) {
	    bx = coords[0*nnpe+1] * s21 + coords[0*nnpe+2] * (1.0 - s21);
	    by = coords[1*nnpe+1] * s21 + coords[1*nnpe+2] * (1.0 - s21);
	    if (i == 0)
	      { ax = bx; ay = by; }
	  }
	  i += 2;
	}
      if (id[2] * id[0] < 0)
	{
	  s02 = v[0] / (v[0] - v[2]);
	  if (intpoints || sizeint) {
	    bx = coords[0*nnpe+2] * s02 + coords[0*nnpe+0] * (1.0 - s02);
	    by = coords[1*nnpe+2] * s02 + coords[1*nnpe+0] * (1.0 - s02);
	  }
	  i += 4;
	}
      if (i == 5)       /* node 0 / node 1, node 2 */
	{
	  ParVol = (1.0 - s10) * s02;
	  if (id[0] > 0)
	    ParVol *= vol * 0.5;
	  else
	    ParVol = (1 - ParVol) * vol * 0.5;
	}
      else if (i == 3)  /* node 1 / node 2, node 0 */
	{
	  ParVol = (1.0 - s21) * s10;
	  if (id[1] > 0)
	    ParVol *= vol * 0.5;
	  else
	    ParVol = (1 - ParVol) * vol * 0.5;
	}
      else if (i == 6)  /* node 2 / node 0, node 1 */
	{
	  ParVol = (1.0 - s02) * s21;
	  if (id[2] > 0)
	    ParVol *= vol * 0.5;
	  else
	    ParVol = (1 - ParVol) * vol * 0.5;
	}
      if (nintp)
	*nintp = 2;
      if (intpoints) {
	intpoints[0] = ax;
	intpoints[1] = bx;
	intpoints[2] = ay;
	intpoints[3] = by;
      }
      if (sizeint)
	*sizeint = sqrt((bx-ax)*(bx-ax) + (by-ay)*(by-ay));
    }
  else /* nsd == 3 */
    {
      double ax=0.0,ay=0.0,az=0.0, bx=0.0,by=0.0,bz=0.0,
	cx=0.0,cy=0.0,cz=0.0, dx=0.0,dy=0.0,dz=0.0; /* Interface points */
      static double elength[4][4]; /* Edge lengths */
      double daux, aax, aay, aaz;
      double s30=0.0, s31=0.0, s32=0.0, s02=0.0, s12=0.0, s10=0.0,
	ese1, ese2, ese3, ese4;
      int id[4];

      for (i = 0; i < 4; i++)
	{
	  if (v[i] > 0.0)
	    id[i] = 1;
	  else
	    id[i] = -1;
	  elength[i][i] = 0;
	  for (j = i+1; j < 4; j++)
	    {
	      dx = coords[0*nnpe+i]-coords[0*nnpe+j];
	      dy = coords[1*nnpe+i]-coords[1*nnpe+j];
	      dz = coords[2*nnpe+i]-coords[2*nnpe+j];
	      elength[i][j] = sqrt(dx*dx+dy*dy+dz*dz);
	      elength[j][i] = elength[i][j];
	    }
	}
      i = j = 0;
      if (id[0] * id[3] < 0)
	{
	  s30 = v[3] / (v[3] - v[0]);
	  if (intpoints || sizeint) {
	    ax = coords[0*nnpe+0] * s30 + coords[0*nnpe+3] * (1.0 - s30);
	    ay = coords[1*nnpe+0] * s30 + coords[1*nnpe+3] * (1.0 - s30);
	    az = coords[2*nnpe+0] * s30 + coords[2*nnpe+3] * (1.0 - s30);
	  }
	  j++; i += 1;
	}
      if (id[1] * id[3] < 0)
	{
	  s31 = v[3] / (v[3] - v[1]);
	  if (intpoints || sizeint) {
	    bx = coords[0*nnpe+1] * s31 + coords[0*nnpe+3] * (1.0 - s31);
	    by = coords[1*nnpe+1] * s31 + coords[1*nnpe+3] * (1.0 - s31);
	    bz = coords[2*nnpe+1] * s31 + coords[2*nnpe+3] * (1.0 - s31);
	    if (j == 0) { ax = bx; ay = by; az = bz; }
	  }
	  j++; i += 2;
	}
      if (id[2] * id[3] < 0)
	{
	  s32 = v[3] / (v[3] - v[2]);
	  if (intpoints || sizeint) {
	    cx = coords[0*nnpe+2] * s32 + coords[0*nnpe+3] * (1.0 - s32);
	    cy = coords[1*nnpe+2] * s32 + coords[1*nnpe+3] * (1.0 - s32);
	    cz = coords[2*nnpe+2] * s32 + coords[2*nnpe+3] * (1.0 - s32);
	    if (j == 0)
	      { ax = cx; ay = cy; az = cz; }
	    else if (j == 1)
	      { bx = cx; by = cy; bz = cz; }
	  }
	  j++; i += 4;
	}
      if (id[2] * id[0] < 0)
	{
	  s02 = v[0] / (v[0] - v[2]);
	  if (intpoints || sizeint) {
	    cx = coords[0*nnpe+2] * s02 + coords[0*nnpe+0] * (1.0 - s02);
	    cy = coords[1*nnpe+2] * s02 + coords[1*nnpe+0] * (1.0 - s02);
	    cz = coords[2*nnpe+2] * s02 + coords[2*nnpe+0] * (1.0 - s02);
	    if (j == 1)
	      { bx = cx; by = cy; bz = cz; }
	  }
	  j++; i += 8;
	}
      if (id[2] * id[1] < 0)
	{
	  s12 = v[1] / (v[1] - v[2]);
	  if (intpoints || sizeint) {
	    dx = coords[0*nnpe+2] * s12 + coords[0*nnpe+1] * (1.0 - s12);
	    dy = coords[1*nnpe+2] * s12 + coords[1*nnpe+1] * (1.0 - s12);
	    dz = coords[2*nnpe+2] * s12 + coords[2*nnpe+1] * (1.0 - s12);
	    if (j == 1)
	      { bx = dx; by = dy; bz = dz; }
	    else if (j == 2)
	      { cx = dx; cy = dy; cz = dz; }
	  }
	  j++; i += 16;
	}
      if (id[0] * id[1] < 0)
	{
	  s10 = v[1] / (v[1] - v[0]);
	  if (intpoints || sizeint) {
	    dx = coords[0*nnpe+0] * s10 + coords[0*nnpe+1] * (1.0 - s10);
	    dy = coords[1*nnpe+0] * s10 + coords[1*nnpe+1] * (1.0 - s10);
	    dz = coords[2*nnpe+0] * s10 + coords[2*nnpe+1] * (1.0 - s10);
	    if (j == 2)
	      { cx = dx; cy = dy; cz = dz; }
	  }
	  j++; i += 32;
	}

      /* Interface points computed, fix orientation in quadrilateral case */
      if (j == 4)
	{
	  if (i == 1+2+8+16) /* 0 1  /  2 3 */
	    { /* a: 03 b: 13 c: 02 d: 12 */
	      if (intpoints || sizeint) {
		daux = cx; cx = dx; dx = daux;
		daux = cy; cy = dy; dy = daux;
		daux = cz; cz = dz; dz = daux;
	      }
	      ese1 = 1.0 - s30;
	      ese2 = s02;
	      ese3 = 1.0 - s31;
	      ese4 = s12;
	    }
	  else if (i == 1+4+16+32 ) /* 0 2  /  1 3 */
	    { /* a: 03 b: 23 c: 12 d: 01 */
	      ese1 = 1.0 - s10;
	      ese2 = 1.0 - s30;
	      ese3 = 1.0 - s12;
	      ese4 = 1.0 - s32;
	    }
	  else /* (i == 2+4+8+32)  ->  0 3  /  1 2 */
	    { /* a: 13 b: 23 c: 02 d: 01 */
	      ese1 = 1.0 - s10;
	      ese2 = s02;
	      ese3 = s31;
	      ese4 = s32;
	    }
	  ParVol = ese1*ese2 + ese3*ese4 + ese1*ese4
	    - ese1*ese3*ese4 - ese1*ese2*ese4;
	  if (id[0] > 0)
	    ParVol *= vol / 6.0;
	  else
	    ParVol = (1 - ParVol) * vol / 6.0;
	}
      else /* one node in one side, three on the other side */
	{
	  if (i == 1+8+32) /* Node 0 */
	    {
	      ParVol = (1.0 - s10) * s02 * (1.0 - s30);
	      if (id[0] > 0)
		ParVol *= vol / 6.0;
	      else
		ParVol = (1 - ParVol) * vol / 6.0;
	    }
	  else if (i == 2+16+32) /* Node 1 */
	    {
	      ParVol = s10 * s12 * (1.0 - s31);
	      if (id[1] > 0)
		ParVol *= vol / 6.0;
	      else
		ParVol = (1 - ParVol) * vol / 6.0;
	    }
	  else if (i == 4+8+16) /* Node 2 */
	    {
	      ParVol = (1.0 - s02) * (1.0 - s12) * (1.0 - s32);
	      if (id[2] > 0)
		ParVol *= vol / 6.0;
	      else
		ParVol = (1 - ParVol) * vol / 6.0;
	    }
	  else /* if (i == 1+2+4)  Node 3 */
	    {
	      ParVol = s30 * s31 * s32;
	      if (id[3] > 0)
		ParVol *= vol / 6.0;
	      else
		ParVol = (1 - ParVol) * vol / 6.0;
	    }
	} /* end else (triangular) */
      if (nintp)
	*nintp = j;
      if (intpoints) {
	intpoints[0] = ax; intpoints[4] = ay; intpoints[8] = az;
	intpoints[1] = bx; intpoints[5] = by; intpoints[9] = bz;
	intpoints[2] = cx; intpoints[6] = cy; intpoints[10] = cz;
	if (j == 4)
	  { intpoints[3] = dx; intpoints[7] = dy; intpoints[11] = dz; }
      }
      if (sizeint) {
	aax = (by - ay) * (cz - az) - (bz - az) * (cy - ay);
	aay = (bz - az) * (cx - ax) - (bx - ax) * (cz - az);
	aaz = (bx - ax) * (cy - ay) - (by - ay) * (cx - ax);
	*sizeint = sqrt(aax*aax + aay*aay + aaz*aaz);
	if (j == 4) {
	  aax = (cy - ay) * (dz - az) - (cz - az) * (dy - ay);
	  aay = (cz - az) * (dx - ax) - (cx - ax) * (dz - az);
	  aaz = (cx - ax) * (dy - ay) - (cy - ay) * (dx - ax);
	  *sizeint += sqrt(aax*aax + aay*aay + aaz*aaz);
	}
      }
    } /* end if (nsd == 3) */
  if (vol < 0)
    {
      printf("volumen negativo!!!: %g\n", vol);
      exit(100);
    }
  if (vref)
    *vref = vol;
  return ParVol;
} /* end parvol function */

/* Edge distance: given distances on nodes, try to improve
 *    moving over edges
 * coords[i*nsd + j]: Coordinate "j" of the "i"th node
 * nsd: number of space dimensions
 * d[i]: distance computed at node "i"
 * sign: 1: distance is distance (positive)
 *      -1: distance is (-distance)
 * returns true if any value gets improved
 */
int edge_distance (double *coords, int nsd, double *d, double sign)
{
  int i, j, k;
  int nnpe, retval;
  double eij, tol;

  nnpe = nsd + 1;
  retval = 0;
  tol = 1.0000001;
  for (i = 0; i < nnpe; i++)
    for (j = i+1; j < nnpe; j++)
      {
	/* Edge distance nodes (i,j) */
	eij = 0;
	for (k = 0; k < nsd; k++)
	  eij += ((coords[i+k*nnpe]-coords[j+k*nnpe])*
		  (coords[i+k*nnpe]-coords[j+k*nnpe]));
	eij = sqrt (eij);

	if (sign*d[i] > sign*(d[j]) + tol*eij)
	  {
	    d[i] = d[j] + sign*eij;
	    retval = 1;
	  }
	if (sign*d[j] > sign*(d[i]) + tol*eij)
	  {
	    d[j] = d[i] + sign*eij;
	    retval = 1;
	  }
      }
  return retval;
}

/* Shadow distance: given distances on nodes, try to improve
 *    moving over the simplex
 * coords[i*nsd + j]: Coordinate "j" of the "i"th node
 * nsd: number of space dimensions
 * d[i]: distance computed at node "i"
 * sign: 1: distance is distance (positive)
 *      -1: distance is (-distance)
 * returns true if any value gets improved
 */
int shadow_distance (double *coords, int nsd, double *d, double sign)
{

  int i, imin, n1, n2, n3;
  int nnpe, retval;
  double cn3, ct1, ct2, ct3, f2, f3;
  double ax, ay, az, da, daa, invdaa;
  double g3x, g3y, g3z;
  double g1x, g1y, g1z;
  double g2x, g2y, g2z;
  double gtx, gty, gtz;
  double d12, d13x, d13y;
  double gx, gy, gz, gn;
  double px, py, pz, dp;
  double dmin, dx, dy, s;
  double vol6, volg, gt;
  double edgelength, height;
  double tgrad, ngrad, newval;
  double auxcoords[6], auxd[3];

  nnpe = nsd + 1; /* Simplex: nodes per element: space dimensions + 1 */

  if (nsd == 1) /* 1D case: shadow == edge */
      return edge_distance (coords, nsd, d, sign);

  /* Computes node with the minimum distance */
  imin = 0;
  dmin = d[0];
  for (i = 1; i < nnpe; i++)
    if (sign*dmin > sign*d[i])
      {
	imin = i;
	dmin = d[i];
      }

  /* Try to improve node "i" (all except node with minimum distance) */
  retval = 0;
  for (i = 0; i < nnpe; i++)
    {
      if (i == imin)
	continue;

      /* 2D case: */
      if (nsd == 2)
	{
	  /* n1, n2: opposite side */
	  n1 = (i+1)%3;
	  n2 = (i+2)%3;

	  edgelength = 
	    ((coords[n1]-coords[n2])*(coords[n1]-coords[n2])+
	     (coords[n1+nnpe]-coords[n2+nnpe])*(coords[n1+nnpe]-coords[n2+nnpe]));
	  edgelength = sqrt(edgelength);

	  /* Tangential gradient */
	  tgrad = (d[n2]-d[n1]) / edgelength;

	  if (tgrad*tgrad > 1.0)
	    {        /* edgedistance improves d[n1] or d[n2] */
	      if (n1 == imin)
		d[n2] = d[n1] + sign*edgelength;
	      else
		d[n1] = d[n2] + sign*edgelength;
	      retval = 1;
	      continue;
	    }
	  /* Normal gradient */
	  ngrad = -sign * sqrt (1.0 - tgrad*tgrad);

	  /* Projection of node 'i' over side 'n1,n2'
	   * s: parameter, 0->n1, 1->n2 */
	  s = ((coords[i] - coords[n1])*(coords[n2]-coords[n1])+
	       (coords[i+nnpe] - coords[n1+nnpe])*(coords[n2+nnpe]-coords[n1+nnpe]));
	  s /= edgelength*edgelength;

	  dx = coords[i]-(s*coords[n2]+(1-s)*coords[n1]);
	  dy = coords[i+nnpe]-(s*coords[n2+nnpe]+(1-s)*coords[n1+nnpe]);

	  /* Distance from node 'i' to edge 'n1,n2' */	  
	  height = sqrt (dx*dx + dy*dy);

	  /* Coordinates normal and tangential:
	   * origin in n1: ct1=cn1=0
	   * tangential axis through n2: cn2=0 */
	  ct2 = edgelength;
	  ct3 = s*ct2;
	  cn3 = -height;

	  /* Function with isolines parallel to gradient
	   * (used to find if node 'i' is in the shadow of side 'n1,n2')
	   */
	  f2 = -ngrad * ct2;
	  f3 = -ngrad * ct3 + tgrad * cn3;
	  
	  if (f3/f2 > 1.0 || f3/f2 < 0.0)
	    continue;

	  /* YES, it's in the shadow, set newval so that
	   * module of the gradient is 1.0
	   */
	  newval = ngrad * cn3 + (d[n1]*(1-s)+d[n2]*s);

	  /* See if the new value improves the previous one */
	  if (sign*newval < sign*d[i] - 1.0e-06*edgelength)
	    {
	      d[i] = newval;
	      retval = 1;
	    }
	}
      else if (nsd == 3) /* Three dimensional case */
	{
	  /* Opposite face of the tetrahedron, numbered in
	   * counter-clockwise direction seen from 'i' node
	   */
	  if (i%2)
	    {
	      n1 = (i+1)%4;
	      n2 = (i+2)%4;
	      n3 = (i+3)%4;
	    }
	  else
	    {
	      n1 = (i+1)%4;
	      n2 = (i+3)%4;
	      n3 = (i+2)%4;
	    }

	  /* Area vector of face (n1, n2, n3) */
	  ax = ((coords[n2+1*nnpe]-coords[n1+1*nnpe])*
		(coords[n3+2*nnpe]-coords[n1+2*nnpe]) -
		(coords[n2+2*nnpe]-coords[n1+2*nnpe])*
		(coords[n3+1*nnpe]-coords[n1+1*nnpe]));
	  ay = ((coords[n2+2*nnpe]-coords[n1+2*nnpe])*
		(coords[n3+0*nnpe]-coords[n1+0*nnpe]) -
		(coords[n2+0*nnpe]-coords[n1+0*nnpe])*
		(coords[n3+2*nnpe]-coords[n1+2*nnpe]));
	  az = ((coords[n2+0*nnpe]-coords[n1+0*nnpe])*
		(coords[n3+1*nnpe]-coords[n1+1*nnpe]) -
		(coords[n2+1*nnpe]-coords[n1+1*nnpe])*
		(coords[n3+0*nnpe]-coords[n1+0*nnpe]));
	  
	  daa = ax*ax + ay*ay + az*az;
	  da = sqrt (daa);
	  invdaa = 1.0/daa;
	  
	  /* Gradient restricted to face (n1,n2,n3)
	   * shape function gradients */
	  g3x = invdaa*(ay*(coords[n2+2*nnpe]-coords[n1+2*nnpe])-
			az*(coords[n2+1*nnpe]-coords[n1+1*nnpe]));
	  g3y = invdaa*(az*(coords[n2+0*nnpe]-coords[n1+0*nnpe])-
			ax*(coords[n2+2*nnpe]-coords[n1+2*nnpe]));
	  g3z = invdaa*(ax*(coords[n2+1*nnpe]-coords[n1+1*nnpe])-
			ay*(coords[n2+0*nnpe]-coords[n1+0*nnpe]));

	  g1x = invdaa*(ay*(coords[n3+2*nnpe]-coords[n2+2*nnpe])-
			az*(coords[n3+1*nnpe]-coords[n2+1*nnpe]));
	  g1y = invdaa*(az*(coords[n3+0*nnpe]-coords[n2+0*nnpe])-
			ax*(coords[n3+2*nnpe]-coords[n2+2*nnpe]));
	  g1z = invdaa*(ax*(coords[n3+1*nnpe]-coords[n2+1*nnpe])-
			ay*(coords[n3+0*nnpe]-coords[n2+0*nnpe]));

	  g2x = invdaa*(ay*(coords[n1+2*nnpe]-coords[n3+2*nnpe])-
			az*(coords[n1+1*nnpe]-coords[n3+1*nnpe]));
	  g2y = invdaa*(az*(coords[n1+0*nnpe]-coords[n3+0*nnpe])-
			ax*(coords[n1+2*nnpe]-coords[n3+2*nnpe]));
	  g2z = invdaa*(ax*(coords[n1+1*nnpe]-coords[n3+1*nnpe])-
			ay*(coords[n1+0*nnpe]-coords[n3+0*nnpe]));
	  /* Gradient of the distance (restricted to face n1,n2,n3) */
	  gtx = g1x*d[n1] + g2x*d[n2] + g3x*d[n3];
	  gty = g1y*d[n1] + g2y*d[n2] + g3y*d[n3];
	  gtz = g1z*d[n1] + g2z*d[n2] + g3z*d[n3];

	  gt = sqrt (gtx*gtx + gty*gty + gtz*gtz);
	  
	  if (gt > 1.0)
	    { 
	      /* It's possible to improve the distance in the face:
	       * project over the face and call shadow_distance
	       * for the 2-D case */
	      d12 = sqrt ((coords[n2+0*nnpe]-coords[n1+0*nnpe])*
			  (coords[n2+0*nnpe]-coords[n1+0*nnpe])+
			  (coords[n2+1*nnpe]-coords[n1+1*nnpe])*
			  (coords[n2+1*nnpe]-coords[n1+1*nnpe])+
			  (coords[n2+2*nnpe]-coords[n1+2*nnpe])*
			  (coords[n2+2*nnpe]-coords[n1+2*nnpe]));

	      d13x = ((coords[n3+0*nnpe]-coords[n1+0*nnpe])*
		      (coords[n2+0*nnpe]-coords[n1+0*nnpe])+
		      (coords[n3+1*nnpe]-coords[n1+1*nnpe])*
		      (coords[n2+1*nnpe]-coords[n1+1*nnpe])+
		      (coords[n3+2*nnpe]-coords[n1+2*nnpe])*
		      (coords[n2+2*nnpe]-coords[n1+2*nnpe])) / d12;

	      d13y = da / d12;

	      /* Projected coordinates */
	      auxcoords[0] = 0;
	      auxcoords[1] = d12;
	      auxcoords[2] = d13x;

	      auxcoords[3] = 0;
	      auxcoords[4] = 0;
	      auxcoords[5] = d13y;

	      /* Distances */
	      auxd[0] = d[n1];
	      auxd[1] = d[n2];
	      auxd[2] = d[n3];

	      if (shadow_distance (auxcoords, nsd-1, auxd, sign))
		{
		  d[n1] = auxd[0];
		  d[n2] = auxd[1];
		  d[n3] = auxd[2];
		  retval = 1;
		}
	      continue;
	    }
	  /* Normal gradient so that mod(grad) = 1.0 */
	  gn = sign*sqrt (1 - gt*gt);

	  /* 6 times the volume of the tetrahedron */
	  vol6 = (ax*(coords[i+0*nnpe]-coords[n1+0*nnpe]) +
		  ay*(coords[i+1*nnpe]-coords[n1+1*nnpe]) +
		  az*(coords[i+2*nnpe]-coords[n1+2*nnpe]));
	  height = vol6 / da;

	  gx = gn * ax / da + gtx;
	  gy = gn * ay / da + gty;
	  gz = gn * az / da + gtz;

	  /* Find intersection between:
	   * line passing trhough node 'i', with direction = new gradient
	   * opposite face */
	  volg = (ax*((coords[i+0*nnpe]+gx)-coords[n1+0*nnpe]) +
		  ay*((coords[i+1*nnpe]+gy)-coords[n1+1*nnpe]) +
		  az*((coords[i+2*nnpe]+gz)-coords[n1+2*nnpe]));
	  
	  s = -vol6 / (volg - vol6);

	  /* Intersection point */
	  px = s * gx + coords[i+0*nnpe];
	  py = s * gy + coords[i+1*nnpe];
	  pz = s * gz + coords[i+2*nnpe];

	  /* Triangular coordinates of point px,py,pz, over face n1,n2,n3 */
	  ct3= ((((coords[n2+1*nnpe]-coords[n1+1*nnpe])*(pz-coords[n1+2*nnpe])-
		 (coords[n2+2*nnpe]-coords[n1+2*nnpe])*(py-coords[n1+1*nnpe]))*
		(coords[i+0*nnpe]-coords[n1+0*nnpe]))+
	       (((coords[n2+2*nnpe]-coords[n1+2*nnpe])*(px-coords[n1+0*nnpe])-
		 (coords[n2+0*nnpe]-coords[n1+0*nnpe])*(pz-coords[n1+2*nnpe]))*
		(coords[i+1*nnpe]-coords[n1+1*nnpe]))+
	       (((coords[n2+0*nnpe]-coords[n1+0*nnpe])*(py-coords[n1+1*nnpe])-
		 (coords[n2+1*nnpe]-coords[n1+1*nnpe])*(px-coords[n1+0*nnpe]))*
		(coords[i+2*nnpe]-coords[n1+2*nnpe]))) / vol6;
	  ct1= ((((coords[n3+1*nnpe]-coords[n2+1*nnpe])*(pz-coords[n2+2*nnpe])-
		 (coords[n3+2*nnpe]-coords[n2+2*nnpe])*(py-coords[n2+1*nnpe]))*
		(coords[i+0*nnpe]-coords[n2+0*nnpe]))+
	       (((coords[n3+2*nnpe]-coords[n2+2*nnpe])*(px-coords[n2+0*nnpe])-
		 (coords[n3+0*nnpe]-coords[n2+0*nnpe])*(pz-coords[n2+2*nnpe]))*
		(coords[i+1*nnpe]-coords[n2+1*nnpe]))+
	       (((coords[n3+0*nnpe]-coords[n2+0*nnpe])*(py-coords[n2+1*nnpe])-
		 (coords[n3+1*nnpe]-coords[n2+1*nnpe])*(px-coords[n2+0*nnpe]))*
		(coords[i+2*nnpe]-coords[n2+2*nnpe]))) / vol6;
	  ct2 = 1 - ct1 - ct3;

	  /* Is node 'i' is in the shadow of face (n1,n2,n3) ???? */
	  if ((ct1 < 0.0)||(ct2 < 0.0)||(ct3 < 0.0))
	    continue;

	  dp = ct1*d[n1] + ct2*d[n2] + ct3*d[n3];
	  newval = dp + sign*sqrt((coords[i+0*nnpe]-px)*(coords[i+0*nnpe]-px)+
				  (coords[i+1*nnpe]-py)*(coords[i+1*nnpe]-py)+
				  (coords[i+2*nnpe]-pz)*(coords[i+2*nnpe]-pz));


	  /* YES, find node 'i' projection over face (n1, n2, n3):
	   * point ... */
	  /*
	    px = coords[i+0*nnpe] - height*ax/da;
	    py = coords[i+1*nnpe] - height*ay/da;
	    pz = coords[i+2*nnpe] - height*az/da;
	  */
	  /* ... and triangular coordinates */
	  /*
	    ct3= ((((coords[n2+1*nnpe]-coords[n1+1*nnpe])*(pz-coords[n1+2*nnpe])-
	    (coords[n2+2*nnpe]-coords[n1+2*nnpe])*(py-coords[n1+1*nnpe]))*
	    (coords[i+0*nnpe]-coords[n1+0*nnpe]))+
	    (((coords[n2+2*nnpe]-coords[n1+2*nnpe])*(px-coords[n1+0*nnpe])-
	    (coords[n2+0*nnpe]-coords[n1+0*nnpe])*(pz-coords[n1+2*nnpe]))*
	    (coords[i+1*nnpe]-coords[n1+1*nnpe]))+
	    (((coords[n2+0*nnpe]-coords[n1+0*nnpe])*(py-coords[n1+1*nnpe])-
	    (coords[n2+1*nnpe]-coords[n1+1*nnpe])*(px-coords[n1+0*nnpe]))*
	    (coords[i+2*nnpe]-coords[n1+2*nnpe]))) / vol6;
	    ct1= ((((coords[n3+1*nnpe]-coords[n2+1*nnpe])*(pz-coords[n2+2*nnpe])-
	    (coords[n3+2*nnpe]-coords[n2+2*nnpe])*(py-coords[n2+1*nnpe]))*
	    (coords[i+0*nnpe]-coords[n2+0*nnpe]))+
	    (((coords[n3+2*nnpe]-coords[n2+2*nnpe])*(px-coords[n2+0*nnpe])-
	    (coords[n3+0*nnpe]-coords[n2+0*nnpe])*(pz-coords[n2+2*nnpe]))*
	    (coords[i+1*nnpe]-coords[n2+1*nnpe]))+
	    (((coords[n3+0*nnpe]-coords[n2+0*nnpe])*(py-coords[n2+1*nnpe])-
	    (coords[n3+1*nnpe]-coords[n2+1*nnpe])*(px-coords[n2+0*nnpe]))*
	    (coords[i+2*nnpe]-coords[n2+2*nnpe]))) / vol6;
	    ct2 = 1 - ct1 - ct3;
	  */
	  /* Distance at the projected point */
	  /*
	    dp = ct1*d[n1] + ct2*d[n2] + ct3*d[n3];
	  */
	  /* new value so that gradient has module == 1.0 */
	  /*
	    newval = dp + gn*height;
	  */

	  /* See if the new value improves the previous one */
	  if (sign*newval < sign*d[i] - 1.0e-06*height)
	    {
	      d[i] = newval;
	      retval = 1;
	    }
	}
    }

  return retval;
}

/*------------------------------------------------------------------------
        Function discomp - Smooth an indicator function, by re-computation
	of the distance to a given contour line.
	Programmed by E.Dari, 2003-08-06

	This function is usually called from FORTRAN.
	Arguments:
	Sy: general data about mesh and partitioning.          (input)
	v:  last step solution (all fields)                    (input-output)
	nforig: field in v with the characteristic function.   (input)
	nfdist: field in v containing the computed distance.   (workspace)
	nfedist: field in v for saving computed edge distance. (input)
	newfc: field in v for saving computed distance.        (input)
	level: the level for tetermining phase.                (input)
	parnum, parmat, pargen: just in case.                  (input)
-----------------------------------------------------------------------------*/

#undef __SFUNC__
#define __SFUNC__ "discomp"

/*#define NOT_DEF 1.0e+30*/

void discomp_(systemdata **fSy, Vec *fv,
	      int *fnforig, int *fnfdist, int *fnfedist, double *flevel,
	      int *fnewfc,
	      double *parnum, double *parmat, double *pargen)
{
  Vec v;

  MPI_Comm Comm;        /* MPI Communicator */
  int      ierr;        /* error checking variable */
  int      i;           /* loop counters */

  PetscScalar *coordaux; /* vector for passing the element nodes coordinates */
  PetscScalar *varscan;  /* pointer for scanning the v values */

  PetscScalar oldval=0.0, newval;
  PetscScalar *funcval, *distval, *nextval;
  PetscScalar max_distance;
  double *dvprop, dmassi, dmass, dmassoo, dmasso;
  double alfa, alfao, alfaoo, volele, vref, sizeint, delvol;
  int *nvprop;
  double sign, level, *rl;
  double *volelei, totvolint, minval, maxval;
  double daux;

  int iel/*, ngr*/, inbe, k, inoloc, posfunc, j, nnpe;
  int nforig, nfdist/*, nfedist, nvar, maxcomp*/, nsd;
  int newfc;
  int totelem, intelem;
  int nlow, nhigh, changes;
  int nodloc, nfields;
  int ed_count, gd_count, sg_count;
  int changed_since_scatter, ch_glob, rank;
  int max_ed_count, max_gd_count;
  int *iele, last_changed, comp_dfc, italfa, swmass;
  PetscBool pt_flg;
  systemdata   *Sy;

  Vec distfield; /* Local vectors: temporary distance field */
  Vec dfcorr;
  PetscScalar *dfscan;
  PetscScalar *dfcscan;
  Vec lv, ldistf, ldfcorr;

  v  = *fv;
  Sy = *fSy;
  level = *flevel;

  nnpe    = Sy->Pa->nnpe;
  Comm    = Sy->Pa->Comm;
  MPI_Comm_rank(Comm, &rank);
  nsd     = Sy->Pa->nsd;
  /* maxcomp = Sy->Sq->maxcomp; */
  /* nvar    = Sy->Sq->nvar; */
  nfields = Sy->nfields;
  nodloc  = Sy->Pa->nodloc;

  nforig = *fnforig - 1;
  nfdist = *fnfdist - 1;
  /* nfedist = *fnfedist - 1; */
  newfc = *fnewfc - 1;

  rl = &parnum[15];

  ierr = VecGetArray(v, &varscan);        CHKERRABORT(PETSC_COMM_WORLD,ierr);

  /* Set mass correction switch */
  PetscOptionsGetInt (NULL, "pgpfep_", "-wmc", &swmass, &pt_flg);
  if (pt_flg != PETSC_TRUE)
    swmass = 1;

  PetscPrintf (Comm, "Mass correction option: %d\n", swmass);

  /* Set distance values to +/- max_dist */
  PetscOptionsGetReal (NULL, "pgpfep_", "-maxdist", &max_distance, &pt_flg);
  if (pt_flg != PETSC_TRUE)
    max_distance = 1.0e+30;

  for (i = 0; i < nodloc; i++) {
    oldval = varscan[i*nfields+nforig];
    varscan[i*nfields+nfdist] = oldval > level ? max_distance : -max_distance;
  }
  ierr = VecRestoreArray(v, &varscan);     CHKERRABORT(PETSC_COMM_WORLD, ierr);

  /* Scatter field values to varext */
  /* ierr = VecScatterBegin(Sy->G2Lscat, v, Sy->varext, INSERT_VALUES, */
  /* 	SCATTER_FORWARD);  CHKERRABORT(PETSC_COMM_WORLD,ierr); */
  /* ierr = VecScatterEnd(Sy->G2Lscat, v, Sy->varext, INSERT_VALUES, */
  /* 	SCATTER_FORWARD);    CHKERRABORT(PETSC_COMM_WORLD,ierr); */
  VecGhostUpdateBegin(v, INSERT_VALUES, SCATTER_FORWARD);
  VecGhostUpdateEnd(v, INSERT_VALUES, SCATTER_FORWARD);

  /* Create distfield and distfieldext vectors */
  /* /\* Create scatter-gather context for a scalar vector *\/ */
  /* ierr = VecCreateMPI(Comm, Sy->Pa->nodloc, Sy->Pa->nodtot, */
  /* 		      &distfield);          CHKERRABORT(PETSC_COMM_WORLD,ierr); */
  /* ierr = VecCreateSeq(MPI_COMM_SELF, Sy->Pa->nodext, */
  /* 		      &distfieldext);       CHKERRABORT(PETSC_COMM_WORLD,ierr); */
  /* ierr = ISCreateGeneral(MPI_COMM_SELF, Sy->Pa->nodext, Sy->Pa->Glob, */
  /* 			 PETSC_USE_POINTER, */
  /* 			 &ISvglob);         CHKERRABORT(PETSC_COMM_WORLD,ierr); */
  /* ierr = ISCreateStride (MPI_COMM_SELF, Sy->Pa->nodext, 0, 1, &ISvext); */
  /* CHKERRABORT(PETSC_COMM_WORLD,ierr); */
  /* ierr = VecScatterCreate (distfield, ISvglob, distfieldext, ISvext, &sgc_df); */
  /* CHKERRABORT(PETSC_COMM_WORLD,ierr); */
  VecCreateGhost(Comm, Sy->Pa->nodloc, Sy->Pa->nodtot, Sy->Pa->nodghost,
		 Sy->Pa->Glob, &distfield);
  
  /* Initializing some useful values
 ierr = VecGetOwnershipRange(v,&low,&high); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  */
  ierr=VecGhostGetLocalForm(v,&lv);
  ierr=VecGetArray(lv,&varscan);             CHKERRABORT(PETSC_COMM_WORLD,ierr);
  /* ierr=VecGetArray(Sy->varext,&varextscan); CHKERRABORT(PETSC_COMM_WORLD,ierr); */

  /* Small vectors (element scope) */
  funcval = (PetscScalar *) malloc (nnpe*sizeof(PetscScalar));
  if (funcval == (PetscScalar *)NULL)
    error(EXIT, "Not enough memory for funcval");
  nextval = (PetscScalar *) malloc (nnpe*sizeof(PetscScalar));
  if (nextval == (PetscScalar *)NULL)
    error(EXIT, "Not enough memory for nextval");

  distval = (PetscScalar *) malloc (nnpe*sizeof(PetscScalar));
  if (distval == (PetscScalar *)NULL)
    error(EXIT, "Not enough memory for distval");
  
  coordaux = (PetscScalar *) malloc (nnpe*nsd*sizeof(PetscScalar));
  if(coordaux == (PetscScalar *)NULL)
    error(EXIT, "Not enough memory for coordaux");
  /* ngr = Sy->Pa->ngroups; */

  /* alloc iele (sorted elements) */
  totelem = Sy->Pa->nelemloc+Sy->Pa->nelemext;
  iele = (int *) malloc (totelem*sizeof(int));
  if (iele == (int *)NULL)
    error(EXIT, "Not enough memory for iele");
  for (i = 0; i < totelem; i++)
    iele[i] = i;
  last_changed = 0;
  volelei = (double *) malloc (totelem*sizeof(double));
  if (volelei == (double *)NULL)
    error(EXIT, "Not enough memory for volelei");

  dvprop = (double *) malloc (Sy->Pa->nodloc * sizeof(double));
  if (dvprop == (double *)NULL)
    error(EXIT, "Not enough memory for dvprop");
  for (i = 0; i < Sy->Pa->nodloc; i++)
    dvprop[i] = 0.0;
  nvprop = (int *) malloc ((Sy->Pa->nodloc+Sy->Pa->nodghost) * sizeof(int));
  if (nvprop == (int *)NULL)
    error(EXIT, "Not enough memory for nvprop");
  for (i = 0; i < Sy->Pa->nodloc+Sy->Pa->nodghost; i++)
    nvprop[i] = 0;
  /* nveprop = (int *) malloc (Sy->Pa->nodext * sizeof(int)); */
  /* if (nveprop == (int *)NULL) */
  /*   error(EXIT, "Not enough memory for nveprop"); */
  /* for (i = 0; i < Sy->Pa->nodext; i++) */
  /*   nveprop[i] = 0; */
  VecDuplicate(distfield, &dfcorr);
  /* VecDuplicate(distfieldext, &dfecorr); */

  /*
   * First loop over elements
   * locate interface elements
   * (compute mass)
   * start building iele (sorted elements)
   */
  for (i = 0; i < totelem; i++) /* Loop over elements of this group */
    {
      iel = iele[i];
      /* if (iel < locelem) /\* Element interior to this processor *\/ */
	{
	  inbe = iel * nnpe;
	  for (k = 0; k < nnpe; k++){  /* Loop over nodes of this element */
	    inoloc = Sy->Pa->Mesh[inbe+k];
	    posfunc = inoloc * Sy->nfields + nforig;
	    funcval[k] = varscan[posfunc];
	  }
	}
      /* else                /\* Inter-processor element *\/ */
      /* 	{ */
      /* 	  inbe = (iel-locelem) * nnpe; */
      /* 	  for (k = 0; k < nnpe; k++){   /\* Loop over nodes of this element *\/ */
      /* 	    inoloc = Sy->Pa->ExtMesh[inbe+k] - 1; */
      /* 	    if (inoloc < 0) /\* External node *\/ */
      /* 	      { */
      /* 		posfunc = (-inoloc - 1) * Sy->nfields + nforig; */
      /* 		funcval[k] = varextscan[posfunc]; */
      /* 	      } */
      /* 	    else          /\* Local node *\/ */
      /* 	      { */
      /* 		inoloc = Sy->Pa->ExtMesh[inbe+k] - Sy->Pa->first; */
      /* 		posfunc = inoloc * Sy->nfields + nforig; */
      /* 		funcval[k] = varscan[posfunc]; */
      /* 	      } */
      /* 	  } */
      /* 	} */
      /* Identify interface elements */
      nlow = nhigh = 0;
      for (k = 0; k < nnpe; k++) {
	if (funcval[k] > level) nhigh++;
	else nlow++;
      }
      if (!(nhigh && nlow))
	continue;

      /* YES, set coordinates and compute good distances */
      if (i > last_changed)
	{
	  k = iele[i];
	  iele[i] = iele[last_changed];
	  iele[last_changed] = k;
	  last_changed++;
	}
      /* if (iel < locelem) /\* Element interior to this processor *\/ */
	{
	  for (k = 0; k < nnpe; k++) {
	    inoloc = Sy->Pa->Mesh[inbe+k];
	    for (j = 0; j < nsd; j++)
	      coordaux[k+j*nnpe] = (Sy->Pa->cogeo)[inoloc*nsd+j];
	  }
	}
      /* else                /\* Inter-processor element *\/ */
      /* 	{ */
      /* 	  for (k = 0; k < nnpe; k++){ /\* Loop over nodes of this element *\/ */
      /* 	    inoloc = Sy->Pa->ExtMesh[inbe+k] - 1; */
      /* 	    if (inoloc < 0) */
      /* 	      { */
      /* 		for (j = 0; j < nsd; j++) */
      /* 		  coordaux[k+j*nnpe] = */
      /* 		    (Sy->Pa->cogeoext)[(-inoloc-1)*nsd+j]; */
      /* 	      } */
      /* 	    else */
      /* 	      { */
      /* 		inoloc = Sy->Pa->ExtMesh[inbe+k] - Sy->Pa->first; */
      /* 		for (j = 0; j < nsd; j++) */
      /* 		  coordaux[k+j*nnpe] = (Sy->Pa->cogeoloc)[inoloc*nsd+j]; */
      /* 	      } */
      /* 	  } */
      /* 	} */

      /* Correction of geometric coordinates if periodic */
      periodic_corr(coordaux, nnpe, nsd, rl);
      interdist (coordaux, nsd, funcval, level, distval, &volelei[iel]);
      /* if (iel < locelem) /\* Element interior to this processor *\/ */
	{
	  for (k = 0; k < nnpe; k++) {
	    posfunc = (Sy->Pa->Mesh[inbe+k])
	      * Sy->nfields + nfdist;
	    oldval = varscan[posfunc];
	    if ((oldval >  0 && distval[k] < oldval) ||
		(oldval <= 0 && distval[k] > oldval))
	      varscan[posfunc] = distval[k];
	    if (!swmass)
	      nvprop[(Sy->Pa->Mesh[inbe+k])] = 1;
	  }
	}
      /* else                /\* Inter-processor element *\/ */
      /* 	{ */
      /* 	  for (k = 0; k < nnpe; k++) { */
      /* 	    inoloc = Sy->Pa->ExtMesh[inbe+k] - 1; */
      /* 	    /\* Store local AND external distances *\/ */
      /* 	    if (inoloc < 0) { */
      /* 	      posfunc = (-inoloc-1) * Sy->nfields + nfdist; */
      /* 	      oldval = varextscan[posfunc]; */
      /* 	      if ((oldval >  0 && distval[k] < oldval) || */
      /* 		  (oldval <= 0 && distval[k] > oldval)) */
      /* 		varextscan[posfunc] = distval[k]; */
      /* 	      /\* */
      /* 		if (!swmass) */
      /* 		nveprop[(-inoloc-1)] = 1; */
      /* 	      *\/ */
      /* 	    } */
      /* 	    else { */
      /* 	      posfunc = (Sy->Pa->ExtMesh[inbe+k] - Sy->Pa->first) * */
      /* 		Sy->nfields + nfdist; */
      /* 	      oldval = varscan[posfunc]; */
      /* 	      if ((oldval > 0 && distval[k] < oldval) || */
      /* 		  (oldval <= 0 && distval[k] > oldval)) */
      /* 		varscan[posfunc] = distval[k]; */
      /* 	      if (!swmass) */
      /* 		nvprop[(Sy->Pa->ExtMesh[inbe+k] - Sy->Pa->first)] = 1; */
      /* 	    } */
      /* 	  } */
      /* 	} */
    } /* End loop elements of this mesh */
  intelem = last_changed;
  PetscSynchronizedPrintf (Comm, "%d elements in the interface\n", intelem);
  PetscSynchronizedFlush (PETSC_COMM_WORLD, stdout);

  /* Pass distance to distfield vector */
  ierr=VecGetArray(distfield, &dfscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
  for (i = 0; i < Sy->Pa->nodloc; i++)
      dfscan[i] = varscan[i*Sy->nfields + nfdist];
  ierr=VecRestoreArray(distfield, &dfscan); CHKERRABORT(PETSC_COMM_WORLD,ierr);

  /* /\* Scatter-gather updating external node's distances *\/ */
  /* /\* Scatter distfield -> distfieldext *\/ */
  /* ierr = VecScatterBegin (sgc_df, distfield, distfieldext, INSERT_VALUES, */
  /* 			  SCATTER_FORWARD); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
  /* ierr = VecScatterEnd (sgc_df, distfield, distfieldext, INSERT_VALUES, */
  /* 			SCATTER_FORWARD); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
  VecGhostUpdateBegin(distfield, INSERT_VALUES, SCATTER_FORWARD);
  VecGhostUpdateEnd(distfield, INSERT_VALUES, SCATTER_FORWARD);

  if (swmass) {
  /*
   * Mass correction: sweep elements over the interface, check
   * new mass vs original mass and compute suggested correction
   */
    ierr=VecGhostGetLocalForm(distfield,
			      &ldistf);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
    ierr=VecGetArray(ldistf, &dfscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
  /* ierr=VecGetArray(distfieldext, &dfescan);CHKERRABORT(PETSC_COMM_WORLD,ierr); */
    totvolint = 0.0;
    dmassi = 0.0;
    for (i = 0; i < intelem; i++) {  /* Loop over interface elements */
      iel = iele[i];
      maxval = -1.0e+30;
      minval = +1.0e+30;
      /* if (iel < locelem) /\* Element interior to this processor *\/ */
      /* { */
      inbe = iel * nnpe;
      for (k = 0; k < nnpe; k++) {  /* Loop over nodes of this element */
	inoloc = Sy->Pa->Mesh[inbe+k];
	nextval[k] = funcval[k] = dfscan[inoloc];
	if (nextval[k] < minval) minval = nextval[k];
	if (nextval[k] > maxval) maxval = nextval[k];
	for (j = 0; j < nsd; j++)
	  coordaux[k+j*nnpe] = (Sy->Pa->cogeo)[inoloc*nsd+j];
      }
    /*   } */
    /* else                /\* Inter-processor element *\/ */
    /*   { */
    /* 	inbe = (iel-locelem) * nnpe; */
    /* 	for (k = 0; k < nnpe; k++){   /\* Loop over nodes of this element *\/ */
    /* 	  inoloc = Sy->Pa->ExtMesh[inbe+k] - 1; */
    /* 	  if (inoloc < 0) /\* External node *\/ */
    /* 	    { */
    /* 	      nextval[k] = funcval[k] = dfescan[-inoloc - 1]; */
    /* 	      if (nextval[k] < minval) minval = nextval[k]; */
    /* 	      if (nextval[k] > maxval) maxval = nextval[k]; */
    /* 	      for (j = 0; j < nsd; j++) */
    /* 		coordaux[k+j*nnpe] = (Sy->Pa->cogeoext)[(-inoloc-1)*nsd+j]; */
    /* 	    } */
    /* 	  else          /\* Local node *\/ */
    /* 	    { */
    /* 	      inoloc = Sy->Pa->ExtMesh[inbe+k] - Sy->Pa->first; */
    /* 	      nextval[k] = funcval[k] = dfscan[inoloc]; */
    /* 	      if (nextval[k] < minval) minval = nextval[k]; */
    /* 	      if (nextval[k] > maxval) maxval = nextval[k]; */
    /* 	      for (j = 0; j < nsd; j++) */
    /* 		coordaux[k+j*nnpe] = (Sy->Pa->cogeoloc)[inoloc*nsd+j]; */
    /* 	    } */
    /* 	} */
    /*   } */

      /* Correction of geometric coordinates if periodic */
      periodic_corr(coordaux, nnpe, nsd, rl);

      /* Compute new mass in this element */
      for (j = 0; j < 10; j++) {
	volele = parvol (coordaux, nsd, nextval, &vref,
			 (int *)0, (double *)0, &sizeint);
	delvol = volele - volelei[iel];
	if (j == 0) /* First iteration, compute initial mass defect */
	  /* if (iel < locelem || Sy->Pa->ExtOwnership[iel-locelem] == 1) */
	  if (Sy->Pa->GOS[iel] > 0)
	    {
	      dmassi += delvol; /* Accumulate if element is internal or
				 * assigned to this processor */
	      totvolint += vref;
	    }
	if (fabs(delvol) < 1.0e-8 * vref)
	  break;
	if (sizeint < 1.0e-50)
	  break;
	for (k = 0; k < nnpe; k++) {
	  newval = nextval[k] - delvol / sizeint;
	  /*
	  if (newval > maxval)
	    newval = maxval - 0.01*(maxval-minval);
	  if (newval < minval)
	    newval = minval + 0.01*(maxval-minval);
	  */
	  if (nextval[k] > 0.0) {
	    if (newval > 0.0)
	      nextval[k] = newval;
	    else
	      nextval[k] = 1.0e-50;
	  }
	  else {
	    if (!(newval > 0.0))
	      nextval[k] = newval;
	    else
	      nextval[k] = -1.0e-50;
	  }
	}
      }

      /* if (iel < locelem) /\* Element interior to this processor *\/ */
      /* 	{ */
      for (k = 0; k < nnpe; k++) {
	inoloc = Sy->Pa->Mesh[inbe+k];
	dvprop[inoloc] += nextval[k] - funcval[k];
	nvprop[inoloc] ++;
      }
      /* 	} */
      /* else                /\* Inter-processor element *\/ */
      /* 	{ */
      /* 	  for (k = 0; k < nnpe; k++) { */
      /* 	    inoloc = Sy->Pa->ExtMesh[inbe+k] - 1; */
      /* 	    /\* Store local contribution *\/ */
      /* 	    if (inoloc >= 0) { */
      /* 	      inoloc = Sy->Pa->ExtMesh[inbe+k] - Sy->Pa->first; */
      /* 	      dvprop[inoloc] += nextval[k] - funcval[k]; */
      /* 	      nvprop[inoloc] ++; */
      /* 	    } */
      /* 	    /\* else { Use nveprop as interface node marker */
      /* 	       nveprop[-inoloc-1] ++; */
      /* 	       } */
      /* 	    *\/ */
      /* 	  } */
      /* 	} */
    } /* End loop elements of this mesh */
  /*
    Pass info from nvprop to nveprop
    (use petsc vectors dfcorr and dfecorr, as auxiliars)
  */
  ierr = VecGetArray (dfcorr, &dfcscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
  for (i = 0; i < Sy->Pa->nodloc; i++)
    dfcscan[i] = nvprop[i];
  ierr = VecRestoreArray (dfcorr,
			  &dfcscan);    CHKERRABORT(PETSC_COMM_WORLD,ierr);
  /* ierr = VecScatterBegin (sgc_df, dfcorr, dfecorr, INSERT_VALUES, */
  /* 			  SCATTER_FORWARD); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
  /* ierr = VecScatterEnd (sgc_df, dfcorr, dfecorr, INSERT_VALUES, */
  /* 			SCATTER_FORWARD); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
  /* ierr = VecGetArray (dfecorr, &dfecscan); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
  /* for (i = 0; i < Sy->Pa->nodext; i++) */
  /*   nveprop[i] = dfecscan[i]; */
  /* ierr = VecRestoreArray (dfecorr, */
  /* 			  &dfecscan);    CHKERRABORT(PETSC_COMM_WORLD,ierr); */
  VecGhostUpdateBegin(dfcorr, INSERT_VALUES, SCATTER_FORWARD);
  VecGhostUpdateEnd(dfcorr, INSERT_VALUES, SCATTER_FORWARD);
  VecGhostGetLocalForm(dfcorr,&ldfcorr);
  ierr = VecGetArray (ldfcorr, &dfcscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
  for (i = 0; i < Sy->Pa->nodghost; i++)
     nvprop[Sy->Pa->nodloc+i] = dfcscan[Sy->Pa->nodloc+i];
  ierr = VecRestoreArray(ldfcorr, &dfcscan); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  VecGhostRestoreLocalForm(dfcorr,&ldfcorr);

  PetscSynchronizedPrintf (Comm, "(%d): Initial mass defect: %g\n",
			   rank, dmassi);
  PetscSynchronizedFlush (PETSC_COMM_WORLD, stdout);
  ierr = MPI_Allreduce(&dmassi, &daux, 1, MPI_DOUBLE, MPI_SUM, Comm);
  dmassi = daux;
  PetscPrintf (Comm, "Total initial mass defect: %g\n", dmassi);
  PetscSynchronizedPrintf (Comm, "(%d): Volume of interface elements %g\n", 
			   rank, totvolint);
  PetscSynchronizedFlush (PETSC_COMM_WORLD, stdout);
  ierr = MPI_Allreduce (&totvolint, &daux, 1, MPI_DOUBLE, MPI_SUM, Comm);
  totvolint = daux;
  PetscPrintf (Comm, "Total volume of interface elements %g\n", totvolint);

  /* Suggested modifications computed */
  /* Find optimal values using "line search" */
  alfao = alfaoo = 0.0;
  dmasso = dmassoo = dmassi;
  ierr = VecGetArray(dfcorr, &dfcscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
  /* ierr = VecGetArray(dfecorr, &dfecscan); CHKERRABORT(PETSC_COMM_WORLD,ierr); */

  alfa = 1.0; 
  for (italfa = 0; italfa < 8; italfa++) {
    /* Descent: new distfield using alfa */
    for (i = 0; i < Sy->Pa->nodloc; i++) {
      if (nvprop[i] > 0) {
	newval = dfscan[i] + dvprop[i]/nvprop[i] * alfa;
	if (dfscan[i] > 0.0) {
	  if (newval > 0.0)
	    dfcscan[i] = newval;
	  else
	    dfcscan[i] = 1.0e-50;
	}
	else {
	  if (!(newval > 0.0))
	    dfcscan[i] = newval;
	  else
	    dfcscan[i] = -1.0e-50;
	}
      }
      else {
	dfcscan[i] = dfscan[i];
      }
    }
    ierr = VecRestoreArray(dfcorr,
			   &dfcscan);    CHKERRABORT(PETSC_COMM_WORLD,ierr);
    /* ierr = VecRestoreArray(dfecorr, */
    /* 			   &dfecscan);    CHKERRABORT(PETSC_COMM_WORLD,ierr); */
    /* ierr = VecScatterBegin (sgc_df, dfcorr, dfecorr, INSERT_VALUES, */
    /* 			    SCATTER_FORWARD);CHKERRABORT(PETSC_COMM_WORLD,ierr); */
    /* ierr = VecScatterEnd (sgc_df, dfcorr, dfecorr, INSERT_VALUES, */
    /* 			  SCATTER_FORWARD); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
    VecGhostUpdateBegin(dfcorr, INSERT_VALUES,SCATTER_FORWARD);
    VecGhostUpdateEnd(dfcorr, INSERT_VALUES,SCATTER_FORWARD);
    VecGhostGetLocalForm(dfcorr, &ldfcorr);
    ierr = VecGetArray(ldfcorr, &dfcscan);   CHKERRABORT(PETSC_COMM_WORLD,ierr);
    /* ierr = VecGetArray(dfecorr, &dfecscan); CHKERRABORT(PETSC_COMM_WORLD,ierr); */

    /* Compute mass with the new distance field */
    dmass = 0.0;
    for (i = 0; i < intelem; i++) {  /* Loop over interface elements */
      iel = iele[i];
      /* if (iel < locelem) /\* Element interior to this processor *\/ */
      /* 	{ */
      inbe = iel * nnpe;
      for (k = 0; k < nnpe; k++){  /* Loop over nodes of this element */
	inoloc = Sy->Pa->Mesh[inbe+k];
	funcval[k] = dfcscan[inoloc];
	for (j = 0; j < nsd; j++)
	  coordaux[k+j*nnpe] = (Sy->Pa->cogeo)[inoloc*nsd+j];
      }
      /* 	} */
      /* else                /\* Inter-processor element *\/ */
      /* 	{ */
      /* 	  inbe = (iel-locelem) * nnpe; */
      /* 	  for (k = 0; k < nnpe; k++){   /\* Loop over nodes of this element *\/ */
      /* 	    inoloc = Sy->Pa->ExtMesh[inbe+k] - 1; */
      /* 	    if (inoloc < 0) /\* External node *\/ */
      /* 	      { */
      /* 		funcval[k] = dfecscan[-inoloc - 1]; */
      /* 		for (j = 0; j < nsd; j++) */
      /* 		  coordaux[k+j*nnpe] = (Sy->Pa->cogeoext)[(-inoloc-1)*nsd+j]; */
      /* 	      } */
      /* 	    else          /\* Local node *\/ */
      /* 	      { */
      /* 		inoloc = Sy->Pa->ExtMesh[inbe+k] - Sy->Pa->first; */
      /* 		funcval[k] = dfcscan[inoloc]; */
      /* 		for (j = 0; j < nsd; j++) */
      /* 		  coordaux[k+j*nnpe] = (Sy->Pa->cogeoloc)[inoloc*nsd+j]; */
      /* 	      } */
      /* 	  } */
      /* 	} */
      
      /* Correction of geometric coordinates if periodic */
      periodic_corr(coordaux, nnpe, nsd, rl);

      /* Compute new mass in this element */
      volele = parvol (coordaux, nsd, funcval, &vref,
		       (int *)0, (double *)0, (double *)0);
      delvol = volele - volelei[iel];                /* Mass defect */
      /* if (iel < locelem || Sy->Pa->ExtOwnership[iel-locelem] == 1) */
      if (Sy->Pa->GOS[iel] > 0)
	{
	  dmass += delvol;       /* Accumulate if element is internal or
				* assigned to this processor
	printf("rank, iel, delvol:%d %d %g\n", rank, iel, delvol); */
	}

    } /* End loop elements of this mesh */
    PetscSynchronizedFlush (PETSC_COMM_WORLD, stdout);
    ierr = MPI_Allreduce(&dmass, &daux, 1, MPI_DOUBLE, MPI_SUM, Comm);
    dmass = daux;
    PetscPrintf (Comm, "Iter: %d, alpha: %g, mass defect: %g\n",
		 italfa, alfa, dmass);

    if (fabs(dmass) < totvolint * 1.0e-08) {
      comp_dfc = 0;
      break;
    }

    if (dmass * dmasso < 0) { /* Interpolate new value of alfa */
      dmassoo = dmasso; dmasso = dmass;
      alfaoo = alfao; alfao = alfa;
      alfa = alfaoo + (alfao - alfaoo) * (-dmassoo) / (dmasso-dmassoo);
      comp_dfc = 1;
    }
    else if (dmass * dmassoo < 0) { /* Interpolate new value of alfa */
      dmasso = dmass;
      alfao = alfa;
      alfa = alfaoo + (alfao - alfaoo) * (-dmassoo) / (dmasso-dmassoo);
      comp_dfc = 1;
    }
    else {               /* sign[dmassoo] = sign[dmasso] = sign[dmass] */
      if (fabs(dmass) < fabs(dmasso)) {
	alfaoo = alfao; alfao = alfa;
	dmassoo = dmasso; dmasso = dmass;
	alfa = 2*alfao - alfaoo;
	comp_dfc = 0;
      }
      else {	
	alfa = alfao;
	comp_dfc = 1;
	break;
      }
    }

  } /* Next alfa iteration */

  /* End alfa iteration, re-compute with last alfa ? */
  if (comp_dfc == 1)
    for (i = 0; i < Sy->Pa->nodloc; i++) {
      if (nvprop[i] > 0) {
	newval = dfscan[i] + dvprop[i]/nvprop[i] * alfa;
	if (dfscan[i] > 0.0) {
	  if (newval > 0.0)
	    dfcscan[i] = newval;
	  else
	    dfcscan[i] = 1.0e-50;
	}
	else {
	  if (!(newval > 0.0))
	    dfcscan[i] = newval;
	  else
	    dfcscan[i] = -1.0e-50;
	}
      }
      else {
	dfcscan[i] = dfscan[i];
      }
    }
  ierr =VecRestoreArray(ldfcorr, &dfcscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
  VecGhostRestoreLocalForm(dfcorr, &ldfcorr);
  /* ierr =VecRestoreArray(dfecorr,&dfecscan); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
  ierr =VecRestoreArray(distfield, &dfscan);CHKERRABORT(PETSC_COMM_WORLD,ierr);
  /* ierr =VecRestoreArray(distfieldext, */
  /* 			&dfescan); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
  VecCopy(dfcorr, distfield);
  /* ierr = VecScatterBegin (sgc_df, distfield, distfieldext, INSERT_VALUES, */
  /* 			  SCATTER_FORWARD); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
  /* ierr = VecScatterEnd (sgc_df, distfield, distfieldext, INSERT_VALUES, */
  /* 			SCATTER_FORWARD); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
  VecGhostUpdateBegin(distfield, INSERT_VALUES, SCATTER_FORWARD);
  VecGhostUpdateEnd(distfield, INSERT_VALUES, SCATTER_FORWARD);
  /*
    VecDestroy(dfcorr);
    VecDestroy(dfecorr);
    free (dvprop);
  */
  /* End mass correction */
  }
  VecDestroy(&dfcorr);
  /* VecDestroy(&dfecorr); */
  free (dvprop);
  /* Newfunc OK for all interface elements, propagate using edge distance */

  VecGhostGetLocalForm(distfield, &ldistf);
  ierr=VecGetArray(ldistf, &dfscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
  /* ierr=VecGetArray(distfieldext, &dfescan);CHKERRABORT(PETSC_COMM_WORLD,ierr); */

  /* Pass fixed distances to var */
  for (i = 0; i < Sy->Pa->nodloc + Sy->Pa->nodghost; i++)
    varscan[i*Sy->nfields + nfdist] = dfscan[i];

  ed_count = sg_count = 0;
  changed_since_scatter = 1;
  changes = 1;
  while (changed_since_scatter || changes /* && ed_count < 500 */) {
    changes = 0;
    ed_count++;
    /* Loop over elements, process elements not over the interface */
    for (i = intelem; i < totelem; i++) {  /* Loop over elements of the mesh
					    * skip interface elements */
      iel = iele[i];
      nlow = nhigh = 0;
      /* if (iel < locelem) /\* Element interior to this processor *\/ */
      /* 	{ */
      inbe = iel * nnpe;
      for (k = 0; k < nnpe; k++) {   /* Loop over nodes of this element */
	inoloc = Sy->Pa->Mesh[inbe+k];
	funcval[k] = dfscan[inoloc]; /* Use two-fields for sorting */
	for (j = 0; j < nsd; j++)
	  coordaux[k+j*nnpe] = (Sy->Pa->cogeo)[inoloc*nsd+j];
	posfunc = inoloc * Sy->nfields + nforig;
	oldval = varscan[posfunc];
	    /*	    if (oldval > level) nhigh++;
	     *	    else nlow++; */
      }
      /* 	} */
      /* else                /\* Inter-processor element *\/ */
      /* 	{ */
      /* 	  inbe = (iel-locelem) * nnpe; */
      /* 	  for (k = 0; k < nnpe; k++){   /\* Loop over nodes of this element *\/ */
      /* 	    inoloc = Sy->Pa->ExtMesh[inbe+k] - 1; */
      /* 	    if (inoloc < 0) /\* External node *\/ */
      /* 	      { */
      /* 		posfunc = (-inoloc - 1) * Sy->nfields + nforig; */
      /* 		funcval[k] = dfescan[(-inoloc - 1)]; */
      /* 		oldval = varextscan[posfunc]; */
      /* 		for (j = 0; j < nsd; j++) */
      /* 		  coordaux[k+j*nnpe] = (Sy->Pa->cogeoext)[(-inoloc-1)*nsd+j]; */
      /* 	      } */
      /* 	    else          /\* Local node *\/ */
      /* 	      { */
      /* 		inoloc = Sy->Pa->ExtMesh[inbe+k] - Sy->Pa->first; */
      /* 		posfunc = inoloc * Sy->nfields + nforig; */
      /* 		funcval[k] = dfscan[inoloc]; */
      /* 		oldval = varscan[posfunc]; */
      /* 		for (j = 0; j < nsd; j++) */
      /* 		  coordaux[k+j*nnpe] = (Sy->Pa->cogeoloc)[inoloc*nsd+j]; */
      /* 	      } */
      /* 	    /\*	    if (oldval > level) nhigh++; */
      /* 	     *	    else nlow++; *\/ */
      /* 	  } */
      /* 	} */
      /*      if (nlow && nhigh) continue; // Over the interfase */
      /*      sign = nhigh > 0 ? 1.0 : -1.0; */
      sign = (oldval > level) ? 1.0 : -1.0;

      /* Correction of geometric coordinates if periodic */
      periodic_corr(coordaux, nnpe, nsd, rl);

      if (edge_distance (coordaux, nsd, funcval, sign)) {
	if (i > last_changed)
	  {
	    k = iele[i];
	    iele[i] = iele[last_changed];
	    iele[last_changed] = k;
	    last_changed++;
	  }
	/* if (iel < locelem) /\* Element interior to this processor *\/ */
	/*   { */
	for (k = 0; k < nnpe; k++) {  /* Loop over nodes of this element */
	                                  /* Use two-fields for sorting */
	  inoloc = Sy->Pa->Mesh[inbe+k];
	  if (nvprop[inoloc] == 0 &&             /* skip interface nodes */
	      sign * funcval[k] <
	      sign * varscan[inoloc*Sy->nfields + nfdist]) {
	    varscan[inoloc*Sy->nfields + nfdist] = funcval[k];
	    changes = 1;
	  }
	}
	/*   } */
	/* else                /\* Inter-processor element *\/ */
	/*   { */
	/*     for (k = 0; k < nnpe; k++) {  /\* Loop over nodes of this element *\/ */
	/*       /\* Store local AND external distances *\/ */
	/*       inoloc = Sy->Pa->ExtMesh[inbe+k] - 1; */
	/*       if (inoloc < 0) {	              /\* Use two-fields for sorting *\/ */
	/* 	if (nveprop[-inoloc-1] == 0 &&      /\* skip interface nodes *\/ */
	/* 	    sign * funcval[k] < */
	/* 	    sign * varextscan[(-inoloc-1) * Sy->nfields + nfdist]) { */
	/* 	  varextscan[(-inoloc-1) * Sy->nfields + nfdist] = funcval[k]; */
	/* 	  changes = 1; */
	/* 	} */
	/*       } */
	/*       else {	                      /\* Use two-fields for sorting *\/ */
	/* 	inoloc = Sy->Pa->ExtMesh[inbe+k] - Sy->Pa->first; */
	/* 	if (nvprop[inoloc] == 0 &&           /\* skip interface nodes *\/ */
	/* 	    sign * funcval[k] < */
	/* 	    sign * varscan[inoloc*Sy->nfields + nfdist]) { */
	/* 	  varscan[inoloc * Sy->nfields + nfdist] = funcval[k]; */
	/* 	  changes = 1; */
	/* 	} */
	/*       } */
	/*     } */
	/*   } */
      }
    } /* End loop over elements of the mesh */

    /* End sweep over elements (internal and external) */
    if (changes) changed_since_scatter = 1;

    /* Pass distance to distfield vector */
    for (i = 0; i < Sy->Pa->nodloc + Sy->Pa->nodghost; i++)
      dfscan[i] = varscan[i*Sy->nfields + nfdist];
    /* Also external values */
    /* for (i = 0; i < Sy->Pa->nodext; i++) */
    /*   dfescan[i] = varextscan[i*Sy->nfields + nfdist]; */

    PetscOptionsGetInt (NULL, "pgpfep_", "-maxedcount", &max_ed_count, &pt_flg);
    if (pt_flg != PETSC_TRUE)
      max_ed_count = 10000000;

    if (!changes || (ed_count%max_ed_count) == 0)
      {
	ierr = MPI_Allreduce(&changed_since_scatter, &ch_glob, 1, MPI_INT,
		      MPI_SUM, Comm);
	if (!ch_glob) break; /* All without changes */
	ierr = VecRestoreArray(ldistf,
			       &dfscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
	VecGhostRestoreLocalForm(distfield, &ldistf);
	/* ierr = VecRestoreArray(distfieldext, */
	/* 		       &dfescan); CHKERRABORT(PETSC_COMM_WORLD,ierr); */

	/* Scatter-gather updating external node's distances */
	/* Scatter distfield -> distfieldext */
	/* ierr = VecScatterBegin (sgc_df, distfield, distfieldext, INSERT_VALUES, */
	/* 		SCATTER_FORWARD); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
	/* ierr = VecScatterEnd (sgc_df, distfield, distfieldext, INSERT_VALUES, */
	/* 	        SCATTER_FORWARD); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
	VecGhostUpdateBegin(distfield, INSERT_VALUES, SCATTER_FORWARD);
	VecGhostUpdateEnd(distfield, INSERT_VALUES, SCATTER_FORWARD);
	
	VecGhostGetLocalForm(distfield, &ldistf);
	ierr = VecGetArray (ldistf,
			    &dfscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
	/* ierr = VecGetArray (distfieldext, */
	/* 		    &dfescan);  CHKERRABORT(PETSC_COMM_WORLD,ierr); */
	changes = 1;
	changed_since_scatter = 0;
	sg_count++;
      }
  } /* End while (changes) */
  PetscSynchronizedPrintf (Comm, "Edge distance: %d sweeps over mesh\n",
			   ed_count);
  PetscSynchronizedFlush (PETSC_COMM_WORLD, stdout);
  PetscPrintf (Comm, "Edge distance: %d scatter operations\n", sg_count);

  /* calculo de masa (temporario) BEGIN */
  dmass = 0.0;
  for (i = 0; i < intelem; i++) {
    iel = iele[i];
    /* if (iel < locelem) */
    /*   { */
    inbe = iel * nnpe;
    for (k = 0; k < nnpe; k++) {
      inoloc = Sy->Pa->Mesh[inbe+k];
      funcval[k] = dfscan[inoloc];
      for (j = 0; j < nsd; j++)
	coordaux[k+j*nnpe] = (Sy->Pa->cogeo)[inoloc*nsd+j];
    }
    /*   } */
    /* else */
    /*   { */
    /* 	inbe = (iel-locelem) * nnpe; */
    /* 	for (k = 0; k < nnpe; k++){ */
    /* 	  inoloc = Sy->Pa->ExtMesh[inbe+k] - 1; */
    /* 	  if (inoloc < 0) */
    /* 	    { */
    /* 	      funcval[k] = dfescan[-inoloc - 1]; */
    /* 	      for (j = 0; j < nsd; j++) */
    /* 		coordaux[k+j*nnpe] = (Sy->Pa->cogeoext)[(-inoloc-1)*nsd+j]; */
    /* 	    } */
    /* 	  else */
    /* 	    { */
    /* 	      inoloc = Sy->Pa->ExtMesh[inbe+k] - Sy->Pa->first; */
    /* 	      funcval[k] = dfscan[inoloc]; */
    /* 	      for (j = 0; j < nsd; j++) */
    /* 		coordaux[k+j*nnpe] = (Sy->Pa->cogeoloc)[inoloc*nsd+j]; */
    /* 	    } */
    /* 	} */
    /*   } */
      
    periodic_corr(coordaux, nnpe, nsd, rl);

    volele = parvol (coordaux, nsd, funcval, &vref,
		     (int *)0, (double *)0, (double *)0);
    delvol = volele - volelei[iel];
    /* if (iel < locelem || Sy->Pa->ExtOwnership[iel-locelem] == 1) */
    if (Sy->Pa->GOS[iel] > 0)
      dmass += delvol;
  }
  ierr = MPI_Allreduce(&dmass, &daux, 1, MPI_DOUBLE, MPI_SUM, Comm);
  dmass = daux;
  PetscPrintf (Comm, "After edge_distance, mass defect: %g\n", dmass);
  /*   calculo de masa (temporario) END */

  /* Edge distance OK, try to improve, use distfield vectors */
  changes = 1;
  changed_since_scatter = 1;
  gd_count = sg_count = 0;
  while (changes || changed_since_scatter /*&& gd_count < 100*/) {
    changes = 0;
    gd_count++;
    /* Loop over elements, process elements not over the interface */
    for (i = intelem; i < totelem; i++) { /* Loop over elements of the mesh */
      iel = iele[i];
      nlow = nhigh = 0;
      /* if (iel < locelem) /\* Element interior to this processor *\/ */
      /* 	{ */
      inbe = iel * nnpe;
      for (k = 0; k < nnpe; k++) {   /* Loop over nodes of this element */
	inoloc = Sy->Pa->Mesh[inbe+k];
	funcval[k] = dfscan[inoloc];
	for (j = 0; j < nsd; j++)
	  coordaux[k+j*nnpe] = (Sy->Pa->cogeo)[inoloc*nsd+j];
	posfunc = inoloc * Sy->nfields + nforig;
	oldval = varscan[posfunc];
	    /*	    if (oldval > level) nhigh++;
	     *	    else nlow++; */
      }
      /* 	} */
      /* else                /\* Inter-processor element *\/ */
      /* 	{ */
      /* 	  inbe = (iel-locelem) * nnpe; */
      /* 	  for (k = 0; k < nnpe; k++) {   /\* Loop over nodes of this element *\/ */
      /* 	    inoloc = Sy->Pa->ExtMesh[inbe+k] - 1; */
      /* 	    if (inoloc < 0) /\* External node *\/ */
      /* 	      { */
      /* 		funcval[k] = dfescan[-inoloc-1]; */
      /* 		for (j = 0; j < nsd; j++) */
      /* 		  coordaux[k+j*nnpe] = (Sy->Pa->cogeoext)[(-inoloc-1)*nsd+j]; */
      /* 		posfunc = (-inoloc-1) * Sy->nfields + nforig; */
      /* 		oldval = varextscan[posfunc]; */
      /* 	      } */
      /* 	    else /\* Local node *\/ */
      /* 	      { */
      /* 		inoloc = Sy->Pa->ExtMesh[inbe+k] - Sy->Pa->first; */
      /* 		funcval[k] = dfscan[inoloc]; */
      /* 		posfunc = inoloc * Sy->nfields + nforig; */
      /* 		oldval = varscan[posfunc]; */
      /* 		for (j = 0; j < nsd; j++) */
      /* 		  coordaux[k+j*nnpe] = (Sy->Pa->cogeoloc)[inoloc*nsd+j]; */
      /* 	      } */
      /* 	    /\*	    if (oldval > level) nhigh++; */
      /* 	     *	    else nlow++; *\/ */
      /* 	  } */
      /* 	} */
      /*      if (nlow && nhigh) continue; // Over the interfase */
      /*      sign = nhigh > 0 ? 1.0 : -1.0; */
      sign = (oldval > level) ? 1.0 : -1.0;

      /* Correction of geometric coordinates if periodic */
      periodic_corr(coordaux, nnpe, nsd, rl);

      /* OK, we found an element to be processed */
      if (shadow_distance (coordaux, nsd, funcval, sign)) {
	/* shadow distance improves previous values */
	/* if (iel < locelem) /\* Element interior to this processor *\/ */
	/*   { */
	for (k = 0; k < nnpe; k++) {
	  inoloc = Sy->Pa->Mesh[inbe+k];
	  newval = funcval[k];
	  if (nvprop[inoloc] == 0 &&           /* skip interface nodes */
	      sign * newval < sign * dfscan[inoloc]) {
	    dfscan[inoloc] = newval;
	    changes = 1;
	  }
	}
	/*   } */
	/* else                /\* Inter-processor element *\/ */
	/*   { */
	/*     for (k = 0; k < nnpe; k++) { */
	/*       inoloc = Sy->Pa->ExtMesh[inbe+k] - 1; */
	/*       newval = funcval[k]; */
	/*       if (inoloc < 0) { /\* External node *\/ */
	/* 	if (nveprop[-inoloc-1] == 0 &&   /\*  skip interface nodes *\/ */
	/* 	    sign*newval < sign*dfescan[-inoloc-1]) { */
	/* 	  dfescan[-inoloc-1] = newval; */
	/* 	  changes = 1; */
	/* 	  /\* */
	/* 	    int posicion = -inoloc-1; */
	/* 	    if (nveprop[posicion] == 0 && */
	/* 	    ((sign*newval) < (sign*dfescan[posicion]*0.999999999))){ */
	/* 	    dfescan[posicion] = newval; */
	/* 	    changes = 1; */
	/* 	  *\/ */
	/* 	} */
	/*       } */
	/*       else { /\* Local node *\/ */
	/* 	inoloc = Sy->Pa->ExtMesh[inbe+k] - Sy->Pa->first; */
	/* 	if (nvprop[inoloc] == 0 &&         /\* skip interface nodes *\/ */
	/* 	    sign * newval < sign * dfscan[inoloc]) { */
	/* 	  dfscan[inoloc] = newval; */
	/* 	  changes = 1; */
	/* 	} */
	/*       } */
	/*     } */
	/*   } */
      }
    } /* End loop elements of this mesh */

    /* End sweep over elements (internal and external) */
    if (changes) changed_since_scatter = 1;

    /* printf("Element sweep concluded: %d changes\n", changes); */

    PetscOptionsGetInt (NULL, "pgpfep_", "-maxgdcount", &max_gd_count, &pt_flg);
    if (pt_flg != PETSC_TRUE)
      max_gd_count = 10000000;

    if (!changes || (gd_count%max_gd_count) == 0)
      {
	ierr = MPI_Allreduce(&changed_since_scatter, &ch_glob, 1,
		      MPI_INT, MPI_SUM, Comm);
	if (!ch_glob) break; /* All without changes */
	ierr = VecRestoreArray(ldistf,
			       &dfscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
	VecGhostRestoreLocalForm(distfield, &ldistf);
	/* ierr = VecRestoreArray(distfieldext, */
	/* 		       &dfescan); CHKERRABORT(PETSC_COMM_WORLD,ierr); */

	/* Scatter-gather updating external node's distances */
	/* Scatter distfield -> distfieldext */
	/* ierr = VecScatterBegin (sgc_df, distfield, distfieldext, INSERT_VALUES, */
	/* 		SCATTER_FORWARD); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
	/* ierr = VecScatterEnd (sgc_df, distfield, distfieldext, INSERT_VALUES, */
	/* 	        SCATTER_FORWARD); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
	VecGhostUpdateBegin(distfield, INSERT_VALUES, SCATTER_FORWARD);
	VecGhostUpdateEnd(distfield, INSERT_VALUES, SCATTER_FORWARD);

	VecGhostGetLocalForm(distfield, &ldistf);
	ierr = VecGetArray(ldistf,
			   &dfscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
	/* ierr = VecGetArray(distfieldext, */
	/* 		   &dfescan);  CHKERRABORT(PETSC_COMM_WORLD,ierr); */
	changes = 1;
	changed_since_scatter = 0;
	sg_count++;
      }
  } /* End while (changes) */

  /* calculo de masa (temporario) BEGIN */
  /* Compute mass with the new distance field */
  dmass = 0.0;
  for (i = 0; i < intelem; i++) {  /* Loop over interface elements */
    iel = iele[i];
    /* if (iel < locelem) /\* Element interior to this processor *\/ */
    /*   { */
    inbe = iel * nnpe;
    for (k = 0; k < nnpe; k++){  /* Loop over nodes of this element */
      inoloc = Sy->Pa->Mesh[inbe+k];
      funcval[k] = dfscan[inoloc];
      for (j = 0; j < nsd; j++)
	coordaux[k+j*nnpe] = (Sy->Pa->cogeo)[inoloc*nsd+j];
    }
    /*   } */
    /* else                /\* Inter-processor element *\/ */
    /*   { */
    /* 	inbe = (iel-locelem) * nnpe; */
    /* 	for (k = 0; k < nnpe; k++){   /\* Loop over nodes of this element *\/ */
    /* 	  inoloc = Sy->Pa->ExtMesh[inbe+k] - 1; */
    /* 	  if (inoloc < 0) /\* External node *\/ */
    /* 	    { */
    /* 	      funcval[k] = dfescan[-inoloc - 1]; */
    /* 	      for (j = 0; j < nsd; j++) */
    /* 		coordaux[k+j*nnpe] = (Sy->Pa->cogeoext)[(-inoloc-1)*nsd+j]; */
    /* 	    } */
    /* 	  else          /\* Local node *\/ */
    /* 	    { */
    /* 	      inoloc = Sy->Pa->ExtMesh[inbe+k] - Sy->Pa->first; */
    /* 	      funcval[k] = dfscan[inoloc]; */
    /* 	      for (j = 0; j < nsd; j++) */
    /* 		coordaux[k+j*nnpe] = (Sy->Pa->cogeoloc)[inoloc*nsd+j]; */
    /* 	    } */
    /* 	} */
    /*   } */
    
    /* Correction of geometric coordinates if periodic */
    periodic_corr(coordaux, nnpe, nsd, rl);

    /* Compute new mass in this element */
    volele = parvol (coordaux, nsd, funcval, &vref,
		     (int *)0, (double *)0, (double *)0);
    delvol = volele - volelei[iel];                /* Mass defect */
    /* if (iel < locelem || Sy->Pa->ExtOwnership[iel-locelem] == 1) */
    if (Sy->Pa->GOS[iel] > 0)
      dmass += delvol;       /* Accumulate if element is internal or
			      * assigned to this processor */
  } /* End loop elements of this mesh */
  ierr = MPI_Allreduce(&dmass, &daux, 1, MPI_DOUBLE, MPI_SUM, Comm);
  dmass = daux;
  PetscPrintf (Comm, "After shadow_distance: mass defect: %g\n", dmass);
  /* calculo de masa (temporario) END */

  free(nvprop);
  /* free(nveprop); */
  free(volelei);

  /* Pass distfield to distance field in unknowns vector */
  for (i = 0; i < nodloc + Sy->Pa->nodghost; i++)
    varscan[i*nfields+nfdist] = dfscan[i];
  /* /\* Also pass ext values of distfield to distance field in unknowns vector *\/ */
  /* for (i = 0; i < Sy->Pa->nodext; i++) */
  /*   varextscan[i*nfields+nfdist] = dfescan[i]; */

  PetscSynchronizedPrintf (Comm, "Final distance: %d sweeps over mesh\n",
			   gd_count);
  PetscSynchronizedFlush (PETSC_COMM_WORLD, stdout);
  PetscPrintf (Comm, "Final distance: %d scatter operations\n", sg_count);

  /* Pass distance to characteristic fuction */
  if (newfc > -1 && newfc != nforig)
    for (i = 0; i < nodloc; i++)
      {
	newval = varscan[i*nfields+newfc];
	/* newval = max_distance * tanh(newval/max_distance*2.5); */
	varscan[i*nfields+nforig] = newval+level;
      }
  ierr = VecRestoreArray(lv, &varscan);    CHKERRABORT(PETSC_COMM_WORLD,ierr);
  VecGhostRestoreLocalForm(v,&lv);
  /* ierr = VecRestoreArray(Sy->varext, */
  /* 			 &varextscan);    CHKERRABORT(PETSC_COMM_WORLD,ierr); */

  /* Destroy index sets and scatter-gather context */
  /* ISDestroy (&ISvglob); */
  /* ISDestroy (&ISvext); */
  /* VecScatterDestroy (&sgc_df); */
  ierr = VecRestoreArray(ldistf,
			 &dfscan);  CHKERRABORT(PETSC_COMM_WORLD,ierr);
  VecGhostRestoreLocalForm(distfield, &ldistf);
  /* ierr = VecRestoreArray(distfieldext, */
  /* 			 &dfescan); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
  VecDestroy(&distfield);
  /* VecDestroy(&distfieldext); */

  free(funcval);
  free(distval);
  free(coordaux);
}

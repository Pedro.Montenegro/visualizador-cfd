#include <stdio.h>
#include "error.h"
#include <string.h>
#include "f2c_string.h"
#include <stdlib.h>

#ifdef mpi
#include "petsc.h"
#endif

#undef __SFUNC__
#define __SFUNC__ "errorhandler"

void errorhandler(int code, const char *message,
                  int line, const char *file, const char *dir, const char *func)
{
  PetscPrintf(PETSC_COMM_WORLD, message);
  PetscError (PETSC_COMM_WORLD, line, func, file,
	      0, PETSC_ERROR_INITIAL, message);

  /* if(code == STAY) {
  }
  else */
  if(code == EXIT){
    MPI_Abort(MPI_COMM_WORLD, 1);
  }
}

#undef __SFUNC__
#define __SFUNC__ "ferr "

void ferr(int *code, char *message, int *line, char *file, char *dir, char *func)
{
  char       *errmessage;
  char       *ffile;
  char       *fdir;
  char       *ffunc;

  errmessage = (char *) malloc(100 * sizeof(char));
  ffile = (char *) malloc(100 * sizeof(char));
  fdir = (char *) malloc(100 * sizeof(char));
  ffunc = (char *) malloc(100 * sizeof(char));
  f2c_string(message, errmessage, 100) ;
  f2c_string(file, ffile, 100);
  f2c_string(dir, fdir, 100);
  f2c_string(func, ffunc, 100);

  errorhandler(*code, errmessage, *line, ffile, fdir, ffunc);

  free(errmessage);
  free(ffile);
  free(fdir);
  free(ffunc);

}

void mem_counter(int *num)
  {
    static int bytes=0;
    
    bytes+=*num;
  }

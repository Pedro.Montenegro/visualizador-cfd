#include <stdio.h>
#include "f2c_string.h"

/*----------------------------------------------------------------------
       Function f2c_string - Put a '\0' at the end of a fortran string

This function search for 2 consecutive blank characters and stop scanning 
there, taking that point as the end of the string. 

Variables names:
      source: source fortran string
      dest: destination C string. Has to have enought memory for copying 
            the source string in it. Could be dest=source.
      fort_lenght: maximum possible lenght of the source string

-------------------------------------------------------------------------*/
#undef __SFUNC__
#define __SFUNC__ "f2c_string "

void f2c_string(char *source, char *dest, int fort_lenght)
{
  int   i;
  char  a;

  a=dest[0]=source[0];
  i=0;

  while(i<fort_lenght && (source[++i]!=' ' || a!=' ')){
    a=source[i];   
    dest[i]=source[i];
  }

  dest[i-1]='\0';
}






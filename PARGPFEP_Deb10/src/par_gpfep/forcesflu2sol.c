#include <stdlib.h>
#include <float.h>
#include "fldsnode2msh.h"
#include "prof_utils.h"
#include "petsclog.h"

/*-------------------------------------------------------------------------------------------------
This function goes through the nodes of the fluid mesh.
It finds out which element, amongst the elements of the solid mesh, it is inside of
Then it takes the value of the Lagrange multiplier in the fluid node and it distributes that value amongst the nodes of the element in the solid mesh using different weights that are proportional 
to the proximity of each node in the solid element to the fluid node.
---------------------------------------------------------------------------------------------------*/


int fldsnode2msh(*Pa,)
{
	int nsd,nnpe,nnpe2,nfields,nfields2,nn2a,rval;
	nsd=Pa->nsd;
	nnpe=Pa->nnpe;
	nnpe2=Pa2->nnpe;
	nn2a=nsd*nnpe2;//number of spatial components of the force times the number of nodes per solid element
	double fnode_coords[nsd],*weights,WGHTS[nsd+1],*fncoor,*forces,FORCES[nn2a],NForce[nsd];
	int *elem,*indexes,INDEXES[nn2a],idisp,idisp2,iforce;
	//si el nodo del fl esta en un vertice puede caer en varios elementos de la malla del solido (pero search_local_disp_ffields devuelve uno solo, en el primero que aparece)
	//initializations--------------
	indexes=&INDEXES[0];
	forces=&FORCES[0];
	fncoor=&fnode_coords[0];
	idisp=nsd+1; 	//index of the mesh displacements in the fluid unknowns list
	idisp2=nsd;	//index of the mesh displacements in the solid unknowns list
	iforce=3*nsd+2;	//index of the forces in the fluid unknowns list
	//-----------------------------
/*	for (i=0;i<nsd+1;i++)*/
/*	{*/
/*		WGHTS[i]=0;*/
/*	}*/
	weights=&WGHTS[0];
	//-----------------------------
	nfields=0;
	for (i=0;i<Sq->nvar;i++)
	{
		nfields=nfields+*(Sq->icomp+i);
	};
	//-----------------------------
	nfields2=0;
	for (i=0;i<Sq2->nvar;i++)
	{
		nfields2=nfields2+*(Sq2->icomp+i);
	};
	//-----------------------------
	nfields2=0;
	for (i=0;i<Sq2->nvar;i++)
	{
		nfields2=nfields2+*(Sq2->icomp+i);
	};
	//-----------------------------
	//end of initializations-------
	for (i=0;i<Pa->nodloc;i++)		//beggining of the loop over local nodes in the fluid mesh
	{
		for (j=0;j<nsd;j++)		//getting the geometric coordinates of the fluid mesh node
		{
			fnode_coords[j]=*(Pa->cogeo+i*nsd+j)+*(v+i*nfields+idisp+j);
		};
		for (j=0;j<nsd;j++)
		{
			NForce[j]=*(v+i*nfields+iforce+j);
		};
//		printf("\n%lf\t%lf\n%lf\t%lf\n",fnode_coords[0],fnode_coords[1],NForce[0],NForce[1]);	
//		ierr=search_local_disp_ffields(Pa2,v2,nfields2,idisp2,fncoor,0,elem,weights);//THIS IS FOR THE ACTUAL CODE
		//----------------------------------------------------------------------------------------------//
		// THE FOLLOWING IS THE ALTERNATIVE FOR THE TEST CODE (avoiding manipulation of the geometric tree)
		int iel,ELEM,retval,count,k;
		double *secoor,selem_coords[nn2a];
		rval=-1;
		ELEM=-1;
		elem=&ELEM;
		secoor=&selem_coords[0]; //solid element displaced coordinates
		for (iel=0;iel<Pa2->nelemloc;iel++)	//for each element in the solid mesh
		{		
			retval=0;			
//			printf("iel=%d retval=%d\n",iel,retval);		
			for (k=0;k<Pa2->nnpe;k++) 	//for each node in the solid mesh element
			{		
				for (j=0;j<Pa->nsd;j++)	//for each component of the coordinates
				{
					selem_coords[k*Pa2->nsd+j]=*(Pa2->cogeo+*(Pa2->Mesh+Pa2->nnpe*iel+k)*Pa2->nsd+j)+*(v2+*(Pa2->Mesh+Pa2->nnpe*iel+k)*nfields2+idisp2+j); 
					// getting the displaced solid nodal coordinates
//					printf("selemcoord%d=%lf\n",k*Pa2->nsd+j,selem_coords[k*Pa2->nsd+j]);
				};
			};
//			printf("fncoor=[%lf,%lf]\n",fncoor[0],fncoor[1]);				
			retval=point_in_element(Pa2->nsd,Pa2->nnpe,secoor,fncoor,weights);
//			printf("retval=%d\n",retval);
			if (retval)
			{
//				printf("I was here %d\n",iel);			
				ELEM=iel;				
				rval=0;
			}
		};
		//adapting the retval value to what the function search_..._ffields would return
//		retval=-(!retval);
		//----------------------------------------------------------------------------------------------//
		printf("node number=%d   rval=%d   weights= %lf  %lf  %lf   elem=%d\n",i,rval,*weights,*(weights+1),*(weights+2),*elem);
		if (!(-rval))
		{
			for (k=0;k<Pa2->nnpe;k++)
			{
				for (j=0;j<nsd;j++)
				{
					INDEXES[k*nsd+j]=*(Pa2->Mesh+k+nnpe2 * *elem)*nsd+j;
					FORCES[k*nsd+j]=*(delcon) * *(weights+k) * NForce[j];
					printf("%d Index = %d \t Force = %lf\n",k*Pa2->nsd+j,INDEXES[k*nsd+j],FORCES[k*nsd+j]);
				};
			};
			//VecSetValues(b2(step),nn2a,indexes,forces,ADD_VALUES);
			/*	b = 		rhs
				nn2a = 		number of nodes to add
				indexes = 	indexes where to add the values
				forces =	values to insert                    */
		};		

	/*---------------------------------------------------------------------*/		
	};//end of the loop over local nodes of the fluid mesh
	return 0;
}

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "error.h"
#include "f2c_string.h"
#include "IOinit.h"
#include <zlib.h>

int findkeyword (const char *string, FILE *fp);

/*
char helpstring[]="\
 Par_Gpfep options database: \n\
 \t -mpn     Main Processor Number \n\
 \t -if      File with general data  \n\
 \t -w2f     1= Input from file of the processors weights \n\
 \t          2= Output to file of the processors weights   \n\
 \t -wfile   filename of the processors weights   \n\
 \t -prtfile read the partition from a .prt file \n\
 \t -prtlocren write local node (&element) renumbering\n\n\n";
*/
char helpstring[]="\
 Par_Gpfep options database: \n\
 \t -pgpfep_partfromfile file <FILE>: reads partition from file <FILE>\n\
 \t -pgpfep_partnonuniform: partition in non equal regions\n\
 \t -pgpfep_weightsfromfile <FILE>: reads weights from <FILE>\n\
 \t -pgpfep_prtweightsfile <FILE>: writes computed weights in <FILE>\n\
 \t -pgpfep_prtpartfile <FILE>: writes computed partition in <FILE> \n\
 \t -pgpfep_prtlocrenfile <FILE>: write local node (&element) renumbering\n\
 \t -pgpfep_wmc: use mass correction in redistancing\n\
 \t -pgpfep_maxdist: maximum distance in redistancing\n\
 \t -pgpfep_maxedcount: in redistancing: max iterations edge distance\n\
 \t -pgpfep_maxgdcount: in redistancing: max iterations g distance\n\
 \t -pgpfep_prtgt <FILE>: prints geometric tree used in searches\n\n\n";
/*-------------------------------------------------------------------
           Function GetOptions - Get the command line options
	   Programmed by Adrian Lew - Started: 22/11/97 Ended: 22/11/97

    This function must be called from FORTRAN. I had to do this because i 
couldn't find what was happenning that the PETSc could not get the options
from FORTRAN.

-------------------------------------------------------------------------*/ 
#undef __SFUNC__
#define __SFUNC__ "GetOptions "

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#define GetOptions getoptions_
#elif defined (HAVE_FORTRAN_PRE_DOT)
#define GetOptions .getoptions
#else
#define GetOptions getoptions
#endif

void GetOptions(int *fierr, char *filegd)
{
  PetscBool pt_flg;
  int ierr;

  ierr = PetscOptionsHasName
    (NULL, NULL,"-help", &pt_flg);   CHKERRABORT(PETSC_COMM_WORLD,ierr);
  if (pt_flg == PETSC_TRUE)
      printf("\n%s", helpstring);
  *fierr = ierr;
}

/*---------------------------------------------------------------------
           Function bcastdata - Broadcast some data
	   Programmed by Adrian Lew - Started: 22/11/97 Ended: 22/11/97

    This function must be called from FORTRAN. I had to do this because i 
couldn't find what was happenning that the MPI had't recognized MPI_INTEGER
and others.

-------------------------------------------------------------------------*/ 
#undef __SFUNC__
#define __SFUNC__ "bcastdata "

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#define BcastData bcastdata_
#else
#define BcastData bcastdata
#endif

void BcastData(int *JSIM, double *PARCI, double *PARCF, int *NCONT, 
	       int *NPARN, int *NPARM, int *NPARG,
	       double *PARNUM, double *PARMAT, double *PARGEN,
	       int *KOUTI, int *KOUTD, int *mpn, int *SURFACE_FLAG,
	       int *ISWNOB, int *KKNFRACS, int *ISWFRAC)
{
  int nerrors = 0;
  /* I don't verify if the mpn is equal in all places because i'm not so fool*/
  nerrors += MPI_Bcast( JSIM  , 1, MPI_INT, *mpn, MPI_COMM_WORLD);
  nerrors += MPI_Bcast( PARCI , 1, MPI_DOUBLE, *mpn, MPI_COMM_WORLD);
  nerrors += MPI_Bcast( PARCF , 1, MPI_DOUBLE, *mpn, MPI_COMM_WORLD);
  nerrors += MPI_Bcast( NCONT , 1, MPI_INT, *mpn, MPI_COMM_WORLD);
  nerrors += MPI_Bcast( NPARM , 1, MPI_INT, *mpn, MPI_COMM_WORLD);
  nerrors += MPI_Bcast( NPARN , 1, MPI_INT, *mpn, MPI_COMM_WORLD);
  nerrors += MPI_Bcast( NPARG , 1, MPI_INT, *mpn, MPI_COMM_WORLD);
  nerrors += MPI_Bcast( PARNUM  , *NPARN, MPI_DOUBLE, *mpn, MPI_COMM_WORLD);
  nerrors += MPI_Bcast( PARMAT  , *NPARM, MPI_DOUBLE, *mpn, MPI_COMM_WORLD);
  nerrors += MPI_Bcast( PARGEN  , *NPARG, MPI_DOUBLE, *mpn, MPI_COMM_WORLD);
  nerrors += MPI_Bcast( KOUTI , 1, MPI_INT, *mpn, MPI_COMM_WORLD);
  nerrors += MPI_Bcast( KOUTD , 1, MPI_INT, *mpn, MPI_COMM_WORLD);
  nerrors += MPI_Bcast( SURFACE_FLAG, 1 , MPI_INT, *mpn, MPI_COMM_WORLD);
  nerrors += MPI_Bcast( ISWNOB, 1 , MPI_INT, *mpn, MPI_COMM_WORLD);
  nerrors += MPI_Bcast( KKNFRACS, 1 , MPI_INT, *mpn, MPI_COMM_WORLD);
  nerrors += MPI_Bcast( ISWFRAC, *KKNFRACS, MPI_INT, *mpn, MPI_COMM_WORLD);
}

/*----------------------------------------------------------------------
  Function bcastpetscvec: broadcast a sequential PETSc vector

  This function is normally called from FORTRAN.
  --------------------------------------------------------------------*/ 
#undef __SFUNC__
#define __SFUNC__ "bcastpetscvec "

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#define BcastPetscVec bcastpetscvec_
#else
#define BcastPetscVec bcastpetscvec
#endif

void BcastPetscVec(Vec *fv)
{
  int ierr, n;
  Vec v;
  PetscScalar *v_scan;

  v = *fv;
  ierr = VecGetArray (v, &v_scan);     CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGetSize(v, &n);            CHKERRABORT(PETSC_COMM_WORLD, ierr);
  /* Future improvements:
   * -Check sizes (local / global ??)
   * -Check type (if complex values -> ??)
   */

  ierr = MPI_Bcast (v_scan, n, MPI_DOUBLE, 0, PETSC_COMM_WORLD);
  ierr = VecRestoreArray (v, &v_scan); CHKERRABORT(PETSC_COMM_WORLD, ierr);
}

/*---------------------------------------------------------------------
           Function ReadInitialGuess - read the initial guess
	   Programmed by Adrian Lew - Started: 27/11/97 Ended: 27/11/97

    This function must be called from FORTRAN. This function is also because
there is a problem with the alignement of the pointers between Fortran and C.
In num[0] is the character lenght in fortran.

-------------------------------------------------------------------------*/ 
#undef __SFUNC__
#define __SFUNC__ "readinitialguess "

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#define ReadInitialGuess readinitialguess_
#elif defined (HAVE_FORTRAN_PRE_DOT)
#define ReadInitialGuess .readinitialguess
#else
#define ReadInitialGuess readinitialguess
#endif

#if defined(PRE_2_0_24)
void ReadInitialGuess(char *ffilename, int *IOvec, double *num, 
		       double *parcig)
#else
void ReadInitialGuess(char *ffilename, Vec *IOvec, double *num, 
		       double *parcig)
#endif
{
 char   *filename;
 char   op[]="r   ";
 char   *auxchar;

 gzFile zdata;

 if(num[0]<0 || num[0]>300)
   error(EXIT, "Bad filename lenght");
 
 filename = (char *) malloc(num[0]*sizeof(char));
 if(filename == (char *)NULL)
   error(EXIT,"Not enought memory to allocate");

 auxchar = (char *) malloc(300*sizeof(char));
 if(auxchar == (char *)NULL)
   error(EXIT,"Not enought memory to allocate");

 f2c_string(ffilename, filename, num[0]);

 zdata = gzopen(filename,"rb");
 if(zdata == (gzFile)NULL)
   error(EXIT, "Cannot open initial guess file for reading");

 while(gzgets(zdata, auxchar, 300)==auxchar && 
       strncmp(auxchar,"*SCALAR_FIELD",13)!=0);

 if(strncmp(auxchar,"*SCALAR_FIELD",13)!=0)
   error(EXIT,"Cannot find *SCALAR_FIELD in the Initial Guess file");

/* fscanf(data,"%s %s %lf %s",auxchar, auxchar, parcig, auxchar); */
 while(gzgets(zdata, auxchar, 300)==auxchar && 
       strncmp(auxchar,"<NONE>",6)!=0);

 num[1] = gztell(zdata);
 gzclose(zdata);

 IOfunc(ffilename, op, IOvec, num);

 free(filename);
 free(auxchar);
}


/*---------------------------------------------------------------------
  Function MonNodVals - Monitor Nodal values
  Programmed by Enzo Dari - Started: 2005/12/15 Ended:

  This function must be called from FORTRAN.
-------------------------------------------------------------------------*/ 
#undef __SFUNC__
#define __SFUNC__ "monnodvals "

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#define MonNodVals monnodvals_
#elif defined (HAVE_FORTRAN_PRE_DOT)
#define MonNodVals .monnodvals
#else
#define MonNodVals monnodvals
#endif

void MonNodVals (systemdata **fSy, int **fperm, int *Rank, Vec *fv,
		 double *time)
{
  static int first_time = 1, NPoints;
  int *perm, i, j, ierr, locnod;
  static int *glonodes, *locnodes;
  systemdata *Sy;
  static PetscScalar *locfields, *glofields;
  Vec v;
  FILE *fpin, *fpout;

  Sy = *fSy;
  if (first_time)
    {
      first_time = 0;
      perm = *fperm;

      locfields = (PetscScalar *) malloc (Sy->nfields * sizeof(PetscScalar));
      glofields = (PetscScalar *) malloc (Sy->nfields * sizeof(PetscScalar));
      if (*Rank == 0)
	{
	  fpin = fopen ("gpfep.cfg", "r");
	  /*	  fpin = fopen ("gpfepmon.cfg", "r"); */
	  if (!fpin || !findkeyword("MONITORED_NODES", fpin))
	    {
	      NPoints = 0;
	      if (fpin) fclose(fpin);
	    }
	  else
	    {
	      fscanf(fpin, "%d", &NPoints);
	      glonodes = (int *) malloc (NPoints * sizeof(int));
	      locnodes = (int *) malloc (NPoints * sizeof(int));
	      for (i = 0; i < NPoints; i++)
		fscanf (fpin, "%d", &glonodes[i]);
	      fclose (fpin);
	      for (i = 0; i < NPoints; i++)
		{
		  glonodes[i]--;
		  locnodes[i] = perm[glonodes[i]];
		}
	    }
	}
      ierr = MPI_Bcast(&NPoints, 1, MPI_INT, 0, MPI_COMM_WORLD);
      if (NPoints == 0)
	return;
      if (*Rank != 0)
	{
	  glonodes = (int *) malloc (NPoints * sizeof(int));
	  locnodes = (int *) malloc (NPoints * sizeof(int));
	}
      ierr = MPI_Bcast(glonodes, NPoints, MPI_INT, 0, MPI_COMM_WORLD);
      ierr = MPI_Bcast(locnodes, NPoints, MPI_INT, 0, MPI_COMM_WORLD);
      for (i = 0; i < NPoints; i++)
	{
	  locnod = locnodes[i] + 1;
	  if (locnod >= Sy->Pa->first &&
	      locnod < Sy->Pa->last)
	    {
	      locnodes[i] = locnod - Sy->Pa->first;
	    }
	  else
	    locnodes[i] = -1;
	}
      if (*Rank == 0)	{	  fpout = fopen ("gpmon.dat", "w");
	  fprintf (fpout, "# node time fields...\n");
	  fclose (fpout);	}
    }
  else /* Not first time */
    {
      PetscScalar *v_scan;
      if (NPoints == 0)
	return;
      if (*Rank == 0)
	fpout = fopen ("gpmon.dat", "a");
      else
	fpout = NULL;
      v = *fv;
      ierr = VecGetArray (v, &v_scan);     CHKERRABORT(PETSC_COMM_WORLD, ierr);
      for (i = 0; i < NPoints; i++)
	{
	  if (locnodes[i] == -1)
	    for (j = 0; j < Sy->nfields; j++)
	      locfields[j] = 0.0;
	  else
	    {
	      for (j = 0; j < Sy->nfields; j++)
		locfields[j] = v_scan[locnodes[i] * Sy->nfields + j];
	    }
	  ierr = MPI_Reduce(locfields, glofields, Sy->nfields, MPI_DOUBLE,
			    MPI_SUM, 0, Sy->Pa->Comm);
	  if (*Rank == 0)
	    {
	      fprintf(fpout, " %d %lf", glonodes[i]+1, *time);
	      for (j = 0; j < Sy->nfields; j++)
		fprintf(fpout, " %lg", glofields[j]);
	      fprintf(fpout, "\n");
	    }
	}
      ierr = VecRestoreArray (v, &v_scan); CHKERRABORT(PETSC_COMM_WORLD, ierr);
      if (*Rank == 0)
	fclose (fpout);
    }
}

int findkeyword (const char *string, FILE *fp)
{
  char buffer[101], *pt;
  int vartudo, achou, cont;
  int tam;
  
  vartudo = achou = 0;
  cont = 1;
  tam = strlen(string);
  
  while (cont)
    {
      pt = fgets(buffer, 101, fp);   /* le uma linha */
      
      /*
      ** Verifica se chegou ao final do arquivo.
      ** Se for a primeira vez, reposiciona o arquivo no inicio.
      ** Senao, erro: Nao achou a string.
      */
      if (pt == NULL)
	if (vartudo)
	  {
	    rewind(fp);
	    return(0);
	  }
	else
	  {
	    vartudo = 1;
	    rewind(fp);
	  }
      
      /*
      ** Pesquisa para ver se e' candidato.
      */
      else if (*pt++ == '*')    /* e' candidato */
	{
	  if (strncmp(pt, "END", 3) == 0)
	    if (vartudo)
	      {
		rewind(fp);
		return(0);
	      }
	    else
	      {
		vartudo = 1;
		rewind(fp);
	      }
	  
	  else if (strncmp(pt, string, tam) == 0)
	    {
	      cont = 0;
	      achou = 1;
	    }
	}
    }
  
  return(achou);
}
/*
#undef __SFUNC__
#define __SFUNC__ "pvasciigstdout "

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#define PVASCIIGstdout pvasciigstdout_
#elif defined (HAVE_FORTRAN_PRE_DOT)
#define PVASCIIGstdout .pvasciigstdout
#else
#define PVASCIIGstdout pvasciigstdout
#endif
void PVASCIIGstdout (MPI_Comm Comm, PetscViewer *PV)
{
  PetscViewerASCIIGetStdout (PETSC_COMM_WORLD, PV);
}
*/

/*---------------------------------------------------------------------
           Function GPFEPcopypointer
-------------------------------------------------------------------------*/ 
#undef __SFUNC__
#define __SFUNC__ "gpfepcopypointer "

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#define GPFEPcopypointer gpfepcopypointer_
#elif defined (HAVE_FORTRAN_PRE_DOT)
#define GPFEPcopypointer .gpfepcopypointer
#else
#define GPFEPcopypointer gpfepcopypointer
#endif

void GPFEPcopypointer(void **src, void **dst)
{
  *dst = *src;
}

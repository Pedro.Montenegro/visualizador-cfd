/*------------------------------------------------------------------------
          fortranfree - frees from C a memory location allocated from C
	  Programmed by Adrian Lew - Started: 17/11/97 Ended: 17/11/97

--------------------------------------------------------------------------*/
#include <stdlib.h>
#include "data_structures.h"

#undef __SFUNC__
#define __SFUNC__ "fortranfree "

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#define FortranFree fortranfree_
#elif defined (HAVE_FORTRAN_PRE_DOT)
#define FortranFree .fortranfree
#else
#define FortranFree fortranfree
#endif

void FortranFree(void **location)
{
  free(*location);
}

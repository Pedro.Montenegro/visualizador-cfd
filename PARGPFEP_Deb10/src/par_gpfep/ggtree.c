/*
** generic geometric tree implementation
*/
#include "stdio.h"
#include "stdlib.h"
#include "malloc.h"
#include "string.h"
#include "ggtree.h"

int gg_tree_init (gg_tree *gt, int nsd, int sized, double *bbox)
{
  int isd;
  double center[3],dx[3],dm;
  gt->nsd = nsd;
  gt->sizedata = sized;
  dm = 0;

  for (isd = 0; isd < nsd; isd++)
    {
      dx[isd] = bbox[2*isd+1] - bbox[2*isd];
      if (dx[isd] > dm)
	dm = dx[isd];
      center[isd] = 0.5 * (bbox[2*isd+1] + bbox[2*isd]);
    }
  gt->root = gg_tree_createnode (gt, center, dm);
  if (gt->root)
    return 0;
  else
    return -1;
}

node_gg_tree *gg_tree_createnode (gg_tree *gt, double *center, double delta)
{
  int i;
  node_gg_tree *temp;
  temp = (node_gg_tree *) malloc (sizeof (node_gg_tree));
  if (!temp)
    return NULL;
  temp->center = (double *) malloc (gt->nsd * sizeof(double));
  if (!temp->center)
    return NULL;
  for (i = 0; i < gt->nsd; i++)
    temp->center[i] = center[i];
  temp->delta = delta;
  temp->branches = (node_gg_tree **)
    malloc ((1 << gt->nsd) * sizeof(node_gg_tree *));
  if (!temp->branches)
    return NULL;
  for (i = 0; i < (1 << gt->nsd); i++)
    temp->branches[i] = NULL;
  temp->list = (g_list *) malloc (sizeof(g_list));
  if (!temp->list)
    return NULL;
  i = g_list_init(temp->list, gt->sizedata, (fcmp) NULL);
  if (i != 0)
    return NULL;
  return (temp);
}

int gg_tree_clear (gg_tree *gt)
{
  int rval;
  rval = gg_tree_freenode (gt, gt->root);
  gt->nsd = gt->sizedata = 0;
  gt->root = NULL;
  return (rval);
}

int gg_tree_freenode (gg_tree *gt, node_gg_tree *ngt)
{
  int i;
  if (ngt->branches[0])
    for (i = 0; i < (1 << gt->nsd); i++)
      gg_tree_freenode (gt, ngt->branches[i]);
  else
    g_list_clear (ngt->list);
  free (ngt->list);
  free (ngt->branches);
  free (ngt->center);
  free (ngt);
  return 0;
}

int gg_tree_adjustsize (gg_tree *gt, double *point, double size)
{
  int i;
  double cpoint[3];
  node_gg_tree *ntree;

  ntree = gg_tree_getleaf (gt, point);
  while (ntree->delta > size)
    {
      for (i = 0; i < (1 << gt->nsd); i++)
	{
	  gg_tree_centerleaf (gt, ntree, i, cpoint);
	  ntree->branches[i] = gg_tree_createnode (gt, cpoint, ntree->delta/2);
	}
      ntree = gg_tree_getleaf (gt, point);
    }
  return 0;
}

node_gg_tree *gg_tree_getleaf (gg_tree *gt, double *point)
{
  int brnch, i;
  node_gg_tree *nt;

  nt = gt->root;
  while (nt->branches[0])
    {
      brnch = 0;
      for (i = 0; i < gt->nsd; i++)
	if (point[i] > nt->center[i])
	  brnch += (1 << i);
      nt = nt->branches[brnch];
    }
  return nt;
} 

int gg_tree_centerleaf (gg_tree *gt, node_gg_tree *nt, int brnch, double *cp)
{
  if (gt->nsd > 0)
    cp[0] = (brnch % 2) ?
      nt->center[0] + nt->delta / 4 : nt->center[0] - nt->delta / 4;
  if (gt->nsd > 1)
    cp[1] = ((brnch/2) % 2) ?
      nt->center[1] + nt->delta / 4 : nt->center[1] - nt->delta / 4;
  if (gt->nsd > 2)
    cp[2] = ((brnch/4) % 2) ?
      nt->center[2] + nt->delta / 4 : nt->center[2] - nt->delta / 4;
  return 0;
}

int gg_tree_insertdata (gg_tree *gt, void *data, double *limits)
{
  return gg_tree_buildlistbranch(gt, gt->root, limits, data);
}

int gg_tree_buildlistbranch (gg_tree *gt, node_gg_tree *nt,
			     double *limits, void *data)
{
  int i, intersecta = 1;
  double ntmin, ntmax, limin, limax;
  for (i = 0; i < gt->nsd; i++)
    {
      ntmin = nt->center[i] - 0.5 * nt->delta;
      ntmax = nt->center[i] + 0.5 * nt->delta;
      limin = limits[i*2];
      limax = limits[i*2+1];
      if (ntmax < limin || ntmin > limax)
	{
	  intersecta = 0;
	  break;
	}
    }
  if (intersecta == 0)
    return 0;

  if (!nt->branches[0])
    g_list_insertfirst (nt->list, data);
  else
    for (i = 0; i < (1 << gt->nsd); i++)
      gg_tree_buildlistbranch (gt, nt->branches[i], limits, data);
  return 0;
}

node_g_list *gg_tree_getlisthead (gg_tree *gt, double *point)
{
  node_gg_tree *leaf;
  leaf = gg_tree_getleaf (gt, point);
  return leaf->list->head;
}

int gg_tree_loadlist (gg_tree *gt, g_list *gl, double *limits)
{
  g_list_clear(gl);
  g_list_init (gl, gt->sizedata, (fcmp)NULL);
  return gg_tree_loadlistbranch (gt, gt->root, limits, gl);
}

int gg_tree_loadlistbranch (gg_tree *gt, node_gg_tree *nt,
			    double *limits, g_list *gl)
{
  int i, intersecta = 1;
  double ntmin, ntmax, limin, limax;
  node_g_list *nl;
  for (i = 0; i < gt->nsd; i++)
    {
      ntmin = nt->center[i] - 0.5 * nt->delta;
      ntmax = nt->center[i] + 0.5 * nt->delta;
      limin = limits[i*2];
      limax = limits[i*2+1];
      if (ntmax < limin || ntmin > limax)
	{
	  intersecta = 0;
	  break;
	}
    }
  if (intersecta == 0)
    return 0;

  if (!nt->branches[0])
    {
      nl = nt->list->head;
      while (nl)
	{
	  g_list_insertfirst (gl, nl->data);
	  nl = nl->next;
	}
    }
  else
    for (i = 0; i < (1 << gt->nsd); i++)
      gg_tree_loadlistbranch (gt, nt->branches[i], limits, gl);
  return 0;
}

int gg_tree_loadlistsort (gg_tree *gt, g_list *gl, double *limits)
{
  return gg_tree_loadlistsortbranch (gt, gt->root, limits, gl);
}

int gg_tree_loadlistsortbranch (gg_tree *gt, node_gg_tree *nt,
			    double *limits, g_list *gl)
{
  int i, intersecta = 1;
  double ntmin, ntmax, limin, limax;
  node_g_list *nl;
  for (i = 0; i < gt->nsd; i++)
    {
      ntmin = nt->center[i] - 0.5 * nt->delta;
      ntmax = nt->center[i] + 0.5 * nt->delta;
      limin = limits[i*2];
      limax = limits[i*2+1];
      if (ntmax < limin || ntmin > limax)
	{
	  intersecta = 0;
	  break;
	}
    }
  if (intersecta == 0)
    return 0;

  if (!nt->branches[0])
    {
      nl = nt->list->head;
      while (nl)
	{
	  g_list_insertsort (gl, nl->data);
	  nl = nl->next;
	}
    }
  else
    for (i = 0; i < (1 << gt->nsd); i++)
      gg_tree_loadlistsortbranch (gt, nt->branches[i], limits, gl);
  return 0;
}

int gg_tree_stats (gg_tree *gt, int *levels, int *leaves, int *ndata,
		   double *data_per_leaf)
{
  int retval, curlevel;
  *levels = *leaves = *ndata = 0;

  curlevel = 0;
  retval = gg_tree_load_stats (gt, gt->root, curlevel, levels, leaves, ndata);

  *data_per_leaf = ((double) *ndata) / *leaves;

  return retval;
}

int gg_tree_load_stats (gg_tree *gt, node_gg_tree *nt, int curlevel,
			int *levels, int *leaves, int *ndata)
{
  int i;
  node_g_list *nl;
  curlevel++;
  if (curlevel > *levels)
    (*levels)++;

  if (!nt->branches[0])   /* Is a leaf */
    {
      (*leaves)++;
      nl = nt->list->head;
      while (nl)
	{
	  (*ndata)++;
	  nl = nl->next;
	}

    }
  else
    for (i = 0; i < (1 << gt->nsd); i++)
      gg_tree_load_stats (gt, nt->branches[i], curlevel,
			  levels, leaves, ndata);

  return 0;
}
int gg_tree_plot (gg_tree *gt, int mode, FILE *fp)
{
  int retval;

  retval = gg_tree_plot_branch (gt, gt->root, mode, fp);

  return retval;
}

int gg_tree_plot_branch (gg_tree *gt, node_gg_tree *nt, int mode, FILE *fp)
{
  int i, j;
  double cmin, cmax;

  if (nt->branches[0])   /* Is not a leaf */
    {
      for (i = 0; i < gt->nsd; i++)
	{
	  cmin = nt->center[i] - 0.5 * nt->delta;
	  cmax = nt->center[i] + 0.5 * nt->delta;
	  for (j = 0; j < gt->nsd; j++)
	    if (j == i)
	      fprintf(fp, " %g", cmin);
	    else
	      fprintf(fp, " %g", nt->center[j]);
	  fprintf(fp, "\n");
	  for (j = 0; j < gt->nsd; j++)
	    if (j == i)
	      fprintf(fp, " %g", cmax);
	    else
	      fprintf(fp, " %g", nt->center[j]);
	  fprintf(fp, "\n\n");
	}
 
      for (i = 0; i < (1 << gt->nsd); i++)
	gg_tree_plot_branch (gt, nt->branches[i], mode, fp);
    }
  return 0;
}

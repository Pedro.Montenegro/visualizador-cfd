#include "petsc.h"

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#define BCinit  bcinit_
#define BC2v    bc2v_
#define nobinit nobinit_
#elif defined (HAVE_FORTRAN_PRE_DOT)
#define BCinit  .bcinit
#define BC2v    .bc2v
#define nobinit .nobinit
#else
#define BCinit bcinit
#define BC2v   bc2v
#endif

#define BOTH       0
#define VAL_ONLY   1
#define COND_ONLY  2

#include "data_structures.h"

#if defined(PRE_2_0_24)
void BCinit(int *fv, int *fbcccond, int *fbccvalue, systemdata **fSy, 
	    int *fSeq2Par, int *fmpn, int *option);

void BC2v(int *fv, systemdata **fSy);
#else
void BCinit(Vec *fv, Vec *fbcccond, Vec *fbccvalue, systemdata **fSy, 
	    VecScatter *fSeq2Par, int *fmpn, int *foption);

void BC2v(Vec *v, systemdata **fSy);
#endif

void nobinit(systemdata **fSy, int *numnob, int *mpn, int **fperm);

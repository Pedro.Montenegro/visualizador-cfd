C#include "include/finclude/petsc.h"
#define BOTH       0
#define VAL_ONLY   1
#define COND_ONLY  2
      
#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#define BCinit bcinit_
#define BC2v   bc2v_
#endif

#if defined (HAVE_FORTRAN_PRE_DOT)
#define BCinit .bcinit
#define BC2v   .bc2v
#endif

      external bcinit
      external bc2v

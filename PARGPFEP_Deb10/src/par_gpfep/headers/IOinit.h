#include "data_structures.h"

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#define IOinit ioinit_
#define IOfunc iofunc_
#define WriteRee writeree_
#define WriteVisualCFD writevisualcfd_
#elif defined (HAVE_FORTRAN_PRE_DOT)
#define IOinit .ioinit
#define IOfunc .iofunc
#define WriteRee .writeree
#define WriteVisualCFD .writevisualcfd
#else
#define IOinit ioinit
#define IOfunc iofunc
#define WriteRee writeree
#define WriteVisualCFD writevisualcfd
#endif

#if defined(PRE_2_0_24)
void IOinit(int *fv, int *fs, systemdata **fSy, int *fnfs, int *infs, 
	    int **fperm, int *fSeq2Par, int *fPar2Seq, int *fmpn);

void IOfunc(char *ffilename, char *fop, int *fIOvec, double *num);
#else
void IOinit(Vec *fv, Vec *fs, systemdata **fSy, int *fnfs, int *infs, 
	    int **fperm, VecScatter *fSeq2Par, VecScatter *fPar2Seq, int *fmpn);

void IOfunc(char *ffilename, char *fop, Vec *fIOvec, double *num);
void WriteRee(char *ffilename, int *fnl, Vec *fIOvec, int *nod, double *parcon);
void WriteVisualCFD(Vec *fIOvec, int *nodtot , double *parcon);
#endif

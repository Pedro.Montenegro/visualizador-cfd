#include "petsc.h"

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)	 
#define assembly assembly_
#define setbcond setbcond_
#define nodalcomp nodalcomp_
#define nassembly nassembly_
#endif

#include "data_structures.h"
#include "assembly_flags.h" 

typedef void(*F1)(PetscScalar *, PetscScalar *, PetscScalar *,
		  int *, int *, int *, int *, 
		  PetscScalar *, PetscScalar *,
		  double *, double *, double *, double *, double *,
		  int *, int *, int *, int *, int *,
		  int *, int *, double *, int *, double *,
		  double *, double *, int *, int *,
		  int*, PetscScalar *, int *, int *, double *);

typedef void(*nF1)(PetscScalar *vaux, PetscScalar *vaaux, PetscScalar *cooaux,
		   int *nvar, int *icomp, int *nnpe, int *nsd, 
		   PetscScalar *ae, PetscScalar *rhse,
		   double *parcon, double *delcon,
		   double *parnum, double *parmat, double *pargen,
		   int *ngau, int *kint, int *typgeo, int *iel, int *nod,
		   int *maxnpe, int *maxgau, double *wgp, int *isim,
		   double *p, double *dpl, double *dp, int *dimrhse,
		   int *AFlag, int *group,
		   int *nRV, PetscScalar *redvals, int *typered,
		   int *iswnob, double *gbaseaux);

typedef void(*FNC)(int *nodloc, PetscScalar *cogeoloc,
		   PetscScalar *varscan, PetscScalar *varascan,
		   int *nnfa, int *nfa, int *nfields, int *nsd, 
		   double *parcon, double *delcon,
		   double *parnum, double *parmat, double *pargen, int *isim);

void assembly(systemdata **fSy, Vec *fv, F1 onelem, F1 surelem, Mat *fA, 
	      Vec *fb, int *fnfa, int *fnnfa, double *parcon, double *delcon, 
	      double *parnum, double *parmat, double *pargen, 
	      double *integrals, int *fnintgr, int *isim, int *Aflag, 
	      int *Lflag, int *Sflag, int *VolInt, int *SurInt);

void setbcond (systemdata **fSy, Vec *fv, Mat *fA, 
	       Vec *fb, int *fnfa, int *fnnfa, IS *fisbc);

void nassembly (systemdata **fSy, Vec *fv, nF1 onelem, nF1 surelem,
		Mat *fA, Vec *fb, int *nfsteps, int *nnfa, int *fifa,
		double *parcon, double *delcon, 
		double *parnum, double *parmat, double *pargen, 
		double *redvals, int *typered, int *nredvals,
		int *isim, int *AssFlag, int *LumpFlag, 
		int *VolInt, int *SurInt);

#ifndef PETSC_HEAD
#include "petscksp.h"
#define PESTC_HEAD
#endif

#ifndef ERROR_HANDLING
#include "error.h"
#define ERROR_HANDLING
#endif

#include "glist.h"
#include "ggtree.h"

#ifndef DATA_STRUCT
#define DATA_STRUCT

/* Note: the word boundary is used here not for indicating the "ending" parts 
   of the local mesh but for indicating those elements with nodes both in
   the local and other processes */

struct partition_{
  MPI_Comm Comm;          /* Communicator to whom the mesh belongs */
  int     nodtot;         /* Total number of nodes in the whole mesh */
  int     nodloc;         /* Total number of nodes in the local mesh */
  int     nodghost;       /* Total number of ghost nodes */
  int     nelemtot;       /* Total number of elements in the whole mesh */
  int     nelemloc;       /* Number of local elements */
  int     nelemext;       /* Number of boundary elements
			     (with local and ghost nodes) */
  int     nnpe;           /* number of nodes per element */
  int     ngroups;        /* Numbers of groups in the whole mesh */
  int     *Mesh;       /* Array containing the local mesh */
  /*
  int     *LocGrIndex;    * Array with the beginning of each group
	  		     elements in the local mesh*/
  /*
  int     *ExtMesh;       * Array containing the boundary elements *
  int     *ExtGrIndex;    * Array with the beginning of each group *
                          * elements in the external mesh */
  int     *GOS;  /* Array containing the group index for each element, it 
                            is positive if current processor is the owner
                            of the element (i.e., computes integrals), 
                            negative otherwise  */
  int     nsd;            /* number of spatial dimensions */
  PetscScalar *cogeo;     /* Array containing the geometric coordinates
			     of the (ghosted) nodes */
  /*
  PetscScalar *cogeoloc;  * Array containing the geometric coordinates
			     of the local nodes *
  PetscScalar *cogeoext;  * Array containing the geometric coordinates 
			     of the external nodes */
  int     *d_nz;          /* local diagonal part sparsity structure */
  int     *nd_nz;         /* extra-diagonal part sparsity structure */
  int     first;          /* first local node */
  int     last;           /* last local node number + 1*/
  int     *Glob;          /* Ghost nodes global index set */  
  int     MeshKind;       /* Coordinates allocated? Periodic? */
  gg_tree *gt;            /* Geometric tree for efficient interpolation */
  double  per[3];         /* Periodicity in each direction */
};

typedef struct partition_ partition; 
/* Masks for MeshKind */
#define ALLOC_COORD_MASK      1
#define PERIODIC_X_MASK       2
#define PERIODIC_Y_MASK       4
#define PERIODIC_Z_MASK       8
#define PERIODIC_MASK        14

/* Note: 
   a) The next structure is constructed thinking that the program has the 
 ability to handle different meshes for each field, despite it is not 
 implemented yet.
   b) In this structure context field means a scalar or vectorial field, i.e.
   a group of scalar components 
*/ 

struct systemquad_{
  int         nvar;            /* number of different fields for each node */
  int         *icomp;          /* number of components of each field      
				  sum(i=0; i<nvar) icomp[i] = nfields      */
  int         maxcomp;         /* max of icomp                             */

  int         ngau;            /* number of gauss points                   */
  int         *kint;           /* interpolation code for each field        */
  int         itypegeo;        /* interpolation code for geometry          */
  int         *nod;            /* number of nodes in the mesh for each field */
  int         maxgau;          /* maximum number of gauss points available */
  int         maxnpe;          /* maximum number of nodes per element      */
  double      *wgp;            /* gauss points weights                     */

  double      *p;              /* interpolation functions values           */
  double      *dpl;            /* interpolation functions derivatives values */
  double      *dpl2;           /* interpolation functions second derivatives
				  values                                   */
};

typedef struct systemquad_ systemquad;

/* Note:
   In this structure context fields means the number of different scalar to 
solve, without distinguishing between components of the same vectorial field.
*/
  

struct systemdata_{
  partition  *Pa;              /* Mesh asociated to this system */
  partition  *SurPa;           /* Boundary mesh asociated to this system */
  /* VecScatter G2Lscat;          /\* Scatter enviroment between local and global  */
  /* 				  mapping of external fields(scalars) *\/ */
  /*
  Vec        varext;           * Array with external nodes values of the 
				  fields (scalars)*
  Vec        varaext;          * Array with the external nodes values of the
				  fields (scalars) in the last continuation 
				  step */
  Vec        va;               /* Vector with the (ghosted) nodal values of the 
				  fields (scalars) in the last continuation 
				  step */
  int        nfields;          /* number of fields (scalars) in each node */
  PetscScalar *bccvalue;       /* Array containing the value of the Dirichlet 
				  boundary conditions for each field (scalar)
				  in the local nodes */
  int        *bcccond;         /* Array containing the logic indicator of the 
				  kind of boundary conditions for each field 
				  (scalars) in the local nodes */
  Vec        lnob;             /* Array containing the logic indicator of the
                                  presence of an oblique boundary condition
                                  (distributed) */
  Vec        lnobext;          /* Array containing the logic indicator of the
                                  presence of an oblique boundary condition
                                  (external nodes) (local)*/
  Vec        gbase;            /* Array containing the local basis for
                                  each oblique node (local)*/
  systemquad *Sq;              /* quadrature data for the volume mesh */
  systemquad *SurSq;           /* quadrature data for the boundary mesh */ 
};

typedef struct systemdata_ systemdata; 

#endif

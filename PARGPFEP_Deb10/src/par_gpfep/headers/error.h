#include "petsc.h"
#define  EXIT 1
#define  STAY 0

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#if defined(PETSC_HAVE_FORTRAN_UNDERSCORE_UNDERSCORE)
#define mem_counter mem_counter__
#else
#define mem_counter mem_counter_
#endif
#endif

#if defined (HAVE_FORTRAN_PRE_DOT)
#define mem_counter .mem_counter
#endif

#define ferr ferr_

#ifndef errordef
#define error(n,s) errorhandler(n,s,__LINE__,__FILE__,__DIR__,__SFUNC__)
#define errordef
#endif

#ifndef __DIR__
#define __DIR__   " "
#endif

#ifndef __SFUNC__
#define __SFUNC__ " "
#endif

#ifndef __LINE__
#define __LINE__  0 
#endif

#ifndef __FILE__
#define __FILE__  "UNKNOWN"
#endif

#ifdef __cplusplus
extern "C"{
#endif
void errorhandler(int, const char *, int, const char *, const char *, const char *);
void mem_counter(int *num);
#ifdef __cplusplus
}
#endif

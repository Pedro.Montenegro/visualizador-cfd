#include "petsc.h"
#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#if defined(PETSC_HAVE_FORTRAN_UNDERSCORE_UNDERSCORE)
#define f2c_string f2c_string__
#else
#define f2c_string f2c_string_
#endif
#endif

#if defined (HAVE_FORTRAN_PRE_DOT)
#define f2c_string .f2c_string
#endif

void f2c_string(char *source, char *dest, int fort_lenght);

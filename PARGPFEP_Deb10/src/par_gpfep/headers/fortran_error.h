#define  EXIT 1
#define  STAY 0

#ifndef errordef
#define error(n,s) ferr(n,s,__LINE__,__GPFILE__,__GPDIR__,__SFUNC__)
#define errordef
#endif

#ifndef __LINE__
#define __LINE__  0 
#endif

#ifndef __GPFILE__
#define __GPFILE__  "UNKNOWN"
#endif

#ifndef __SFUNC__
#define __SFUNC__  "UNKNOWN"
#endif

#ifndef __GPDIR__
#define __GPDIR__  "UNKNOWN"
#endif

#ifndef GGTREE_H
#define GGTREE_H

/*
** generic geometric tree
*/
#include "glist.h"

struct _node_gg_tree
{
  double *center;
  double delta;

  struct _node_gg_tree **branches;
  g_list *list;
};
typedef struct _node_gg_tree node_gg_tree;

struct _gg_tree
{
  node_gg_tree *root;

  int sizedata;
  int nsd;
};
typedef struct _gg_tree gg_tree;

int gg_tree_init (gg_tree *gt, int nsd, int sized, double *bbox);
int gg_tree_clear (gg_tree *gt);
int gg_tree_freenode (gg_tree *gt, node_gg_tree *ngt);
int gg_tree_adjustsize (gg_tree *gt, double *point, double size);
int gg_tree_insertdata (gg_tree *gt, void *data, double *limits);

node_gg_tree *gg_tree_getleaf (gg_tree *gt, double *point); 
node_g_list *gg_tree_getlisthead (gg_tree *gt, double *point);
int gg_tree_centerleaf (gg_tree *gt, node_gg_tree *nt, int brnch, double *cp);

int gg_tree_loadlist (gg_tree *gt, g_list *gl, double *limits);
int gg_tree_loadlistsort (gg_tree *gt, g_list *gl, double *limits);

node_gg_tree *gg_tree_createnode (gg_tree *gt, double *point, double size);
int gg_tree_buildlistbranch (gg_tree *gt, node_gg_tree *nt,
			  double *limits, void *data);
int gg_tree_loadlistbranch (gg_tree *gt, node_gg_tree *nt,
			    double *limits, g_list *gl);
int gg_tree_loadlistsortbranch (gg_tree *gt, node_gg_tree *nt,
				double *limits, g_list *gl);

int gg_tree_stats (gg_tree *gt, int *levels, int *leaves, int *ndata,
		   double *data_per_leaf);
int gg_tree_load_stats (gg_tree *gt, node_gg_tree *nt, int curlevel,
			int *levels, int *leaves, int *ndata);

int gg_tree_plot (gg_tree *gt, int mode, FILE *fp);
int gg_tree_plot_branch (gg_tree *gt, node_gg_tree *nt, int mode, FILE *fp);

#endif
/* GGTREE_H */

#include "data_structures.h"

/*
 * pgpfep_plot_gtree:
 * Plot auxiliar geometric tree for efficient searchs
 * Pa(input): Data structure
 * mode(input): even number: only root plots the geometric tree
 */
int pgpfep_plot_gtree(partition *pa, int mode);
void pgpfepplotgtree_(partition **fPa, int *mode, int *retv);

/*
 * pgpfep_stats_gtree:
 * Get statistics about the geometric tree for efficient searchs
 * Pa(input): Data structure
 * levels: # of levels of the geometric tree
 * leaves: # of leaves of the geometric tree
 * ndata: Total number of data (elements) stored at the leaves
 * dpl: Average number of data elements in the tree leaves
 */
int pgpfep_stats_gtree (partition *Pa, int *levels, int *leaves, int *ndata,
			double *dpl);
/* Fortran interface */
void pgpfepstatsgtree_ (partition **fPa, int *levels, int *leaves, int *ndata,
			double *dpl, int *retv);

/*
 * pgpfep_init_interp:
 * Creates data structures for an efficient geometric search of elements
 * of the mesh, optionally displaced
 * (include local elements, as well as intra-proccesses elements)
 * arguments:
 * Pa(input,output): Data structure with distributed mesh
 * tree_size: adjust refinement of the auxiliary tree
 *     recommended: ~ 4.0
 *     > 4.0: less refinement, less memory, more time
 *     < 4.0: more refinement, more memory, less time
 * swperiodic: not zero if the mesh is periodic on some direction
 * periods(input)(1...nsd): if > 0 periodicity in each spatial dimension
 * disp(input): displacement of the nodes, distributed vector
 *     containing the displacement: disp[inode*sclpn+idisp+isd]
 *     (with or without ghosted nodes)
 * sclpn: scalars per node in vector disp
 * idisp: position of first component of displacement in vector disp
 * return value: 0: everthing OK, !0: error
 */
int pgpfep_init_interp (partition *Pa, double tree_size,
			int swperiodic, double *periods,
			Vec *disp, int sclpn, int idisp);
/* Fortran interface */
void pgpfepinitinterp_ (partition **fPa, double *tree_size,
			int *swperiodic, double *periods,
			Vec *fv, int *sclpn, int *idisp, int *retv);

/*
 * pgpfep_stop_interp:
 * Clears data structures for efficient search
 * Arguments:
 * Pa(input,output): Data structure with distributed mesh
 */
int pgpfep_stop_interp (partition *Pa);
/* Fortran interface */
void pgpfepstopinterp_ (partition **fPa, int *retv);

/*
 * pgpfep_point_in_element:
 * Given an element by its "nnpe" coordinates "coords", in "nsd" spatial
 * dimensions, find if the point "point" belongs to it.
 * If result is true, set the "weights" vector to the interpolation
 * weights of each node.
 * Arguments:
 * nsd(input): number of spatial dimensions
 * nnpe(input): number of points in the element (simplex: nsd+1)
 * coords(input): nodal coordinates (1...nnpe)(1...nsd)[inode * nsd + isd]
 * point(input): point coordinates (1...nsd)
 * weights(output): interpolation weights of each node (1...nnpe)
 *
 * return value: 1: point in element
 *               0: no
 */
int pgpfep_point_in_element (int nsd, int nnpe, double *coords, double *point,
		      double *weights);
/* Fortran Interface */
void pgpfeppointinelement_ (int *nsd, int *nnpe, double *coords, double *point,
		      double *weights, int *retv);

int pgpfep_point_in_elem_tol (int nsd, int nnpe, double *coords, double *point,
			      int mode, double tol, double *weights);
/* Fortran Interface */
void pgpfeppointinelemtol_ (int *nsd, int *nnpe, double *coords, double *point,
			    int *mode, double *tol, double *weights, int *retv);

/*
 * pgpfep_search_local
 * Given a point search the element containing it
 * (Optionally use displacement of the nodes)
 *
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * disp(input, optional): displacement of the nodes, distributed vector
 *     containing the displacement: disp[inode*sclpn+idisp+isd]
 *     (with or without ghosted nodes)
 *     Use NULL if mesh is not displaced
 * sclpn: scalars per node
 * idisp: index of displacement in the scalars of a node (starts at 0)
 * point(input): point
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * elem(output): element containing the point
 * weights(output): weights for interpolation
 *         (one for each node of the element, sum(weigths) = 1)
 *
 * return value: -1: point outside local mesh
 *                0: point found in totally local elements
 *                1: point found in an intra-proccesss element
 */
int pgpfep_search_local (partition *Pa,
			 double *disp, int sclpn, int idisp,
			 double *point, int mode,
			 int *elem, double *weights);
/* Fortran interface */
void pgpfepsearchlocal_ (partition **fPa,
			 double *disp, int *fsclpn, int *fidisp,
			 double *point, int *mode,
			 int *elem, double *weights, int *retv);

/*
 * pgpfep_find_nearest_local
 * Given a point search the element containing it
 * or the nearest element in the local mesh
 * (Optionally use displacement of the nodes)
 *
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * disp(input, optional): displacement of the nodes, distributed vector
 *     containing the displacement: disp[inode*sclpn+idisp+isd]
 *     (with or without ghosted nodes)
 *     Use NULL if mesh is not displaced
 * sclpn: scalars per node
 * idisp: index of displacement in the scalars of a node (starts at 0)
 * point(input): point
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * elem(output): element containing the point
 * weights(output): weights for interpolation
 *         (one for each node of the element, sum(weigths) = 1)
 *
 * return value: -1: point outside local mesh
 *                0: point found in totally local elements
 *                1: point found in an intra-proccesss element
 */
int pgpfep_find_nearest_local (partition *Pa,
			       double *disp, int sclpn, int idisp,
			       double *point, int mode,
			       int *elem, double *weights);
/* Fortran interface */
void pgpfepfindnearestlocal_ (partition **fPa,
			      double *disp, int *fsclpn, int *fidisp,
			      double *point, int *mode,
			      int *elem, double *weights, int *retv);

/*
 * pgpfep_interp_local:
 * Given a point and a field over the nodes of the mesh this function
 * search the element containing the point and interpolates the field
 * (Optionally use displacement of the nodes)
 *
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * disp(input, optional): displacement of the nodes, distributed vector
 *     containing the displacement: disp[inode*sclpn+idisp+isd]
 *     (with or without ghosted nodes)
 *     Use NULL if mesh is not displaced 
 * sclpn: scalars per node
 * idisp: index of displacement in the scalars of a node (starts at 0)
 * point(input): point where the function is needed
 * vsc(input): Vector with fields to interpolate
 * stride(input): number of values in each node.
 * start(input): position of the field in the list of values of the node
 * ncomps(input): components of the field to interpolate
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * result(output): pointer to memory where the results should be placed
 *        (ncomps components of the interpolated field)
 *
 * return value: 0: OK, interpolation done
 *              -1: point outside local mesh
 */
int pgpfep_interp_local (partition *Pa,
			 double *disp, int sclpn, int idisp,
			 double *point, double *vsc,
			 int stride, int start, int ncomps, int mode,
			 double *result);
/*
 * Fortran interface:
 * use fsclpn = 0 if mesh is not displaced
 */
void pgpfepinterplocal_ (partition **fPa,
			 double *disp, int *fsclpn, int *fidisp,
			 double *point, double *fields,
			 int *stride, int *start, int *ncomps, int *mode,
			 double *result, int *retv);

/*
 * pgpfep_interp_global:
 * Interpolate a field in a set of points, (all processes ask
 *  for a (different) set of points), mesh optionally displaced
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * disp(input, optional): displacement of the nodes, vector
 *     containing the displacement: disp[inode*sclpn+idisp+isd]
 *     (with or without ghosted nodes)
 *     Use NULL if mesh is not displaced
 * sclpn: scalars per node
 * idisp: index of displacement in the scalars of a node (starts at 0)
 * points(input): vector of points (x1, y1, ... x2, y2, .......)
 * npoints(input): number of points
 * vloc(input): Vector of values of the fields on the nodes
 * stride(input): number of values in each node
 * start(input): position of the field to be interpolated
 * ncomps(input): components of the field to be interpolated
 * results(output): vector to hold the results (npoints*ncomps)
 * sw(input/output): vector to hold the results of the search (size npoints)
 * return value: number of points not found in mesh
 */
int pgpfep_interp_global (partition *Pa,
			  double *disp, int sclpn, int idisp,
			  double *points, int npoints,
			  double *vloc, int stride,
			  int start, int ncomps, double *results,
			  int *sw);
/*
 * Fortran interface:
 * use fsclpn = 0 if mesh is not displaced
 */
void pgpfepinterpglobal_ (partition **fPa,
			  double *disp, int *sclpn, int *idisp,
			  double *points, int *npoints,
			  double *vfields, int *stride,
			  int *start, int *ncomps, double *results,
			  int *sw, int *retv);



/* -------------------------------------------------------------------------- */

/*
 * plot_interpol:
 */
int plot_interpol (partition *Pa, int mode);
/* Fortran interface */
void plotinterpol_ (partition **fPa, int *mode, int *retv);

/*
 * stats_interpol:
 * Get statistics about the data structures for an efficient geometric search
 * Pa(input,output): Data structure
 * levels: # of levels of the geometric tree
 * leaves: # of leaves of the geometric tree
 * ndata: Total number of data (elements) stored at the leaves
 * dpl: Average number of data elements in the tree leaves
 */
int stats_interpol (partition *Pa, int *levels, int *leaves, int *ndata,
		    double *dpl);
/* Fortran interface */
void statsinterpol_ (partition **fPa, int *levels, int *leaves, int *ndata,
		     double *dpl, int *retv);

/*
 * init_interpol:
 * Creates data structures for an efficient geometric search of elements
 * of the mesh (include local elements, as well as intra-proccesses elements
 * arguments:
 * Pa(input,output): Data structure with distributed mesh
 * return value: 0: everthing OK, < 0: error
 */
int init_interpol (partition *Pa);
/* Fortran interface */
void initinterpol_ (partition **fPa, int *retv);

/*
 * in_interp:
 * Creates data structures for an efficient geometric search of elements
 * of the mesh (include local elements, as well as intra-proccesses elements
 * arguments:
 * Pa(input,output): Data structure with distributed mesh
 * tree_size(input): adjust refinement of the auxiliary tree,
 *     1.0: same as init_interpol
 *     > 1.0: less refinement, less memory, more time
 *     < 1.0: more refinement, more memory, less time
 *     recommended: ~ 4.0
 * periodic(input): not zero if the mesh is periodic
 * periodicity(input)(1...nsd): if > 0 periodicity in each spatial dimension
 *
 * return value: 0: everthing OK, < 0: error
 */
int in_interp (partition *Pa, double tree_size,
	       int periodic, double *periodicity);
/* Fortran interface */
void ininterp_ (partition **fPa, double *tree_size,
		int *swperiodic, double *per_sizes, int *retv);

/*
 * in_interpdisp:
 * Creates data structures for an efficient geometric search of elements
 * of the mesh (include local elements, as well as intra-proccesses elements
 * arguments:
 * Pa(input,output): Data structure with distributed mesh
 * tree_size(input): adjust refinement of the auxiliary tree,
 *     1.0: same as init_interpol
 *     > 1.0: less refinement, less memory, more time
 *     < 1.0: more refinement, more memory, less time
 *     recommended: ~ 4.0
 * disp(input): displacement of the nodes, distributed vector
 *   with same structure as Pa->cooloc: disp[inode*nsd+isd]
 * dispe(input): displacement of the "external" nodes, same structure as
 *   Pa->cogeoext: dispe[inode*nsd+isd]
 *
 * return value: 0: everthing OK, < 0: error
 *
 */
int in_interpdisp (partition *Pa, double tree_size, Vec disp);
/* Fortran interface */
void ininterpdisp_ (partition **fPa, double *tree_size, Vec *disp,
		    int *retv);

/*
 * in_interpdisp_ffields:
 * Creates data structures for an efficient geometric search of elements
 * of the mesh (include local elements, as well as intra-proccesses elements
 * arguments:
 * Pa(input,output): Data structure with distributed mesh
 * tree_size(input): adjust refinement of the auxiliary tree,
 *     1.0: same as init_interpol
 *     > 1.0: less refinement, less memory, more time
 *     < 1.0: more refinement, more memory, less time
 *     recommended: ~ 4.0
 * v(input): vector of fields over nodes (including displacements)
 *          (v[inod*sclpn+iscl+isd])
 * ve(input): fields of "external" nodes
 *
 * return value: 0: everthing OK, < 0: error
 *
 */
int in_interpdisp_ffields (partition *Pa, double tree_size, Vec v,
			   int sclpn, int idisp);
/* Fortran interface */
void ininterpdispffields_ (partition **fPa, double *tree_size, Vec *v,
			   int *fsclpn, int *fidisp,
			   int *retv);

/*
 * stop_interp:
 * Clears data structures for efficient search
 * Arguments:
 * Pa(input,output): Data structure with distributed mesh
 */
int stop_interp (partition *Pa);
/* Fortran interface */
void stopinterp_ (partition **fPa, int *retv);

/*
 * interp_local:
 * Given a point and a field over the nodes of the mesh this function
 * search the element containing the point and interpolates the field.
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * point(input): point where the function is needed
 * vsc(input): Vector of values of the fields on the local nodes
 * vesc(input): vector of values of the fields on the "external" nodes.
 * stride(input): number of values in each node.
 * start(input): position of the field in the list of values of the node
 * ncomps(input): components of the field to interpolate
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * result(output): pointer to memory where the results should be placed
 *        (ncomps components of the interpolated field)
 *
 * return value: 0: OK, interpolation done
 *              -1: point outside local mesh
 */
int interp_local (partition *Pa, double *point, double *vsc,
		  int stride, int start, int ncomps, int mode,
		  double *result);
/* Fortran interface:
   Extra arguments:
     fSy: (system data): used for accessing the local components of the
         "external" vector. Also for the "stride" argument
     fv: Petsc vector with unknowns
 */
void interplocal_ (partition **fPa, systemdata **fSy, Vec *fv, double *point,
		   int *start, int *ncomps,
		   double *result, int *mode, int *retv);
/*
 * interp_local_disp:
 * Given a point and a field over the nodes of the mesh this function
 * search the element containing the point and interpolates the field,
 * (use a displacement of the nodes)
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * disp: Displacement of the nodes
 * dispe: Displacement of "external" nodes
 * point(input): point where the function is needed
 * vsc(input): Vector of values of the fields on the local nodes
 * vesc(input): vector of values of the fields on the "external" nodes.
 * stride(input): number of values in each node.
 * start(input): position of the field in the list of values of the node
 * ncomps(input): components of the field to interpolate
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * result(output): pointer to memory where the results should be placed
 *        (ncomps components of the interpolated field)
 *
 * return value: 0: OK, interpolation done
 *              -1: point outside local mesh
 */
int interp_local_disp (partition *Pa, double *disp,
		       double *point, double *vsc,
		       int stride, int start, int ncomps, int mode,
		       double *result);
/* Fortran interface:
   Extra arguments:
     fSy: (system data): used for accessing the local components of the
         "external" vector. Also for the "stride" argument
     fv: Petsc vector with unknowns
 */
void interplocaldisp_ (partition **fPa, Vec *disp,
		       double *point, Vec *fv, systemdata **fSy,
		       int *start, int *ncomps, int *mode,
		       double *result, int *retv);

/*
 * interp_local_disp_ffields:
 * Given a point and a field over the nodes of the mesh this function
 * search the element containing the point and interpolates the field,
 * (use displacement of the nodes contained in an unknowns vector)
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * v(input): vector of fields over nodes (including displacements)
 *          (v[inod*sclpn+iscl+isd])
 * ve: fields of "external" nodes
 * sclpn: scalars per node
 * idisp: index of displacement in the scalars of a node (starts at 0)
 * point(input): point where the function is needed
 * vsc(input): Vector of values of the fields on the local nodes
 * vesc(input): vector of values of the fields on the "external" nodes.
 * stride(input): number of values in each node.
 * start(input): position of the field in the list of values of the node
 * ncomps(input): components of the field to interpolate
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * result(output): pointer to memory where the results should be placed
 *        (ncomps components of the interpolated field)
 *
 * return value: 0: OK, interpolation done
 *              -1: point outside local mesh
 */
int interp_local_disp_ffields (partition *Pa,
			       double *v, int sclpn, int idisp,
			       double *point, double *vsc,
			       int stride, int start, int ncomps, int mode,
			       double *result);
/* Fortran interface:
   Extra arguments:
     fSy: (system data): used for accessing the local components of the
         "external" vector. Also for the "stride" argument
     fv: Petsc vector with unknowns
 */
void interplocaldispffields_ (partition **fPa, systemdata **fSy,
			      Vec *v, int *fsclpn, int *fidisp,
			      double *point, Vec *fv,
			      int *start, int *ncomps, int *mode,
			      double *result, int *retv);

/*
 * search_local:
 * Given a point search the element containing it
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * point(input): point
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * elem(output): element containing the point
 * weights(output): weights for interpolation
 *         (one for each node of the element, sum(weigths) = 1)
 *
 * return value: -1: point outside local mesh
 *                0: point found in totally local elements
 *                1: point found in an intra-proccesss element
 */
int search_local (partition *Pa, double *point, int mode,
		  int *elem, double *weights);
/* Fortran interface */
void searchlocal_ (partition **fPa, double *point, int *mode,
		   int *elem, double *weights, int *retv);

/*
 * search_local_disp:
 * Given a point search the element containing it, using a displaced mesh
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * disp(input): vector of displacements (disp[inod*nsd+isd])
 * dispe: Displacement of "external" nodes
 * point(input): point
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * elem(output): element containing the point
 * weights(output): weights for interpolation
 *         (one for each node of the element, sum(weigths) = 1)
 *
 * return value: -1: point outside local mesh
 *                0: point found in totally local elements
 *                1: point found in an intra-proccesss element
 */
int search_local_disp (partition *Pa, double *disp,
		       double *point, int mode, int *elem, double *weights);
/* Fortran interface */
void searchlocaldisp_ (partition **fPa, Vec *disp,
		       double *point, int *mode, int *elem, double *weights,
		       int *retv);

/*
 * search_local_disp_ffields:
 * Given a point search the element containing it, using a displaced mesh
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * v(input): vector of fields over nodes (including displacements)
 *          (v[inod*sclpn+iscl])
 * ve: fields of "external" nodes
 * sclpn: scalars per node
 * idisp: index of displacement in the scalars of a node (starts at 0)
 * point(input): point
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * elem(output): element containing the point
 * weights(output): weights for interpolation
 *         (one for each node of the element, sum(weigths) = 1)
 *
 * return value: -1: point outside local mesh
 *                0: point found in totally local elements
 *                1: point found in an intra-proccesss element
 */
int search_local_disp_ffields (partition *Pa,
			       double *v, int sclpn, int idisp,
			       double *point, int mode,
			       int *elem, double *weights);
/* Fortran interface */
void searchlocaldispffields_ (partition **fPa, Vec *v,
			      int *fsclpn, int *fidisp,
			      double *point, int *mode,
			      int *elem, double *weights, int *retv);

/*
 * interp_global:
 * Interpolate a field in a set of points,
 * (all processes ask for a (different) set of points)
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * points(input): vector of points (x1, y1, ... x2, y2, .......)
 * npoints(input): number of points
 * vloc(input): Vector of values of the fields on the local nodes
 * vext(input): Vector of values of the fields on the "external" nodes
 * stride(input): number of values in each node
 * start(input): position of the field to be interpolated
 * ncomps(input): components of the field to be interpolated
 * results(output): vector to hold the results (npoints*ncomps)
 *
 * return value: number of points not found in mesh
 */
int interp_global (partition *Pa, double *points, int npoints,
		   double *vloc,
		   int stride, int start, int ncomps, double *results, int*sw);
/* Fortran interface
 *  Extra arguments:
 *    fSy: (system data): used for accessing the local components of the
 *        "external" vector. Also for the "stride" argument
 *    fv: Petsc vector with unknowns
 */
void interpglobal_ (systemdata **fSy, double *points, int *npoints,
		    Vec *fv, int *start, int *ncomps, double *result,
		    int *sw, int *retv);

/*
 * interp_global_disp:
 * Interpolate a field in a set of points, (all processes ask
 *  for a (different) set of points), mesh is displaced
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * delta: displacement of local nodes
 * deltae: displacement of "external" nodes
 * points(input): vector of points (x1, y1, ... x2, y2, .......)
 * npoints(input): number of points
 * vloc(input): Vector of values of the fields on the local nodes
 * vext(input): Vector of values of the fields on the "external" nodes
 * stride(input): number of values in each node
 * start(input): position of the field to be interpolated
 * ncomps(input): components of the field to be interpolated
 * results(output): vector to hold the results (npoints*ncomps)
 *
 * return value: number of points not found in mesh
 */
int interp_global_disp (partition *Pa, double *delta,
			double *points, int npoints,
			double *vloc,
			int stride, int start, int ncomps, double *results,
			int*sw);
/* Fortran interface
 *  Extra arguments:
 *    fSy: (system data): used for accessing the local components of the
 *        "external" vector. Also for the "stride" argument
 *    fv: Petsc vector with unknowns
 */
void interpglobaldisp_ (systemdata **fSy, Vec *delt,
			double *points, int *npoints,
			Vec *fv, int *start, int *ncomps, double *result,
			int *sw, int *retv);

int interp_global_disp_ffields
(partition *Pa, double *vdelta,
 int sclpn, int idisp,
 double *points, int npoints,
 double *vloc, int stride, int start, int ncomps,
 double *results, int *sw);

/*
 * point_in_element:
 * Given an element by its "nnpe" coordinates "coords", in "nsd" spatial
 * dimensions, find if the point "point" belongs to it.
 * If result is true, set the "weights" vector to the interpolation
 * weights of each node.
 * Arguments:
 * nsd(input): number of spatial dimensions
 * nnpe(input): number of points in the element (simplex: nsd+1)
 * coords(input): nodal coordinates (1...nnpe)(1...nsd)[inode * nsd + isd]
 * point(input): point coordinates (1...nsd)
 * weights(output): interpolation weights of each node (1...nnpe)
 *
 * return value: 1: point in element
 *               0: no
 */
int point_in_element (int nsd, int nnpe, double *coords, double *point,
		      double *weights);
/* Fortran Interface */
void pointinelement_ (int *nsd, int *nnpe, double *coords, double *point,
		      double *weights, int *retv);

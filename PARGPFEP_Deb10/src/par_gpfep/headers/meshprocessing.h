#include "petsc.h"

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#define meshprocessing meshprocessing_
#define meshdata meshdata_
#define partitiondestroy partitiondestroy_ 
#define surfaceprocessing surfaceprocessing_
#endif

#include "data_structures.h"
#define   HAS_SURFACE_MESH              1
#define   DOES_NOT_HAVE_SURFACE_MESH    0

void meshprocessing(MPI_Comm *fComm, char *input_mesh,
		    int *fmpn, int *fnsd, int *fnnpe, int **fperm, 
		    partition **fPa);

void meshdata(partition **Pa, int *nelem, int *nsd, int *nodtot, int *nnpe,
	      int *ngroups);

void surfaceprocessing(char *input_mesh, int *fmpn, partition **fPa, 
		       int **fperm, partition **fSurPa, int *fflag);

void partitiondestroy(partition **fPa);

#include "petsc.h"

#if defined (HAVE_FORTRAN_UNDERSCORE)
#define meshprocessing meshprocessing_
#define meshdata meshdata_
#define partitiondestroy partitiondestroy_ 
#define surfaceprocessing surfaceprocessing_
#endif

#include "data_structures.h"
#define   HAS_SURFACE_MESH              1
#define   DOES_NOT_HAVE_SURFACE_MESH    0

#if defined(PRE_2_0_24)
void meshprocessing(int *fComm, double **fweights, char *input_mesh, int *fmpn,
		    int *fnsd, int *fnnpe, int **fperm, 
		    partition **fPa, char *output_file);
#else
void meshprocessing(MPI_Comm Comm, double **fweights, char *input_mesh,
		    int *fmpn,
		    int *fnsd, int *fnnpe, int **fperm, 
		    partition **fPa, char *output_file);
#endif

void meshdata(partition **Pa, int *nelem, int *nsd, int *nodtot, int *nnpe,
	      int *ngroups);

void surfaceprocessing(char *input_mesh, int *fmpn, partition **fPa, 
		       int **fperm, partition **fSurPa, int *fflag);

void partitiondestroy(partition **fPa);

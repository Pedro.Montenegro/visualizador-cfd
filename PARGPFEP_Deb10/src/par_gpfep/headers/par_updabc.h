#include "math.h"
#include "data_structures.h"

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)	 
#if defined(PETSC_HAVE_FORTRAN_UNDERSCORE_UNDERSCORE)
#define par_updabc par_updabc__
#else
#define par_updabc par_updabc_
#endif
#endif

typedef void(*F2)(int *, int *, int *, double *, int *, int *, 
		  double *, double *, double *, double *, PetscScalar *, double *, 
		  int *);


#if defined(PRE_2_0_24)
void par_updabc(F2 updatebc, systemdata **fSy, int *fv, double *fparcon, 
		  double *parmat, double *parnum, double *pargen, int *flag);
#else
void par_updabc(F2 updatebc, systemdata **fSy, Vec *fv, double *fparcon, 
		  double *parmat, double *parnum, double *pargen, int *flag);
#endif


#define BC_CONSTANT   1
#define BC_VARIABLE   0

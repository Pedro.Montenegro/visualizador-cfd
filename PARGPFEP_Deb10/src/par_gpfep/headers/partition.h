
void ParMeshPartition(MPI_Comm Comm, int size, int rank, int mpn,
		      int nsd, int nnodes, int nelems, int nnpe, int ngroups,
		      int *nelempgrp, double *coordinates, int *elements,
		      int *edgecut, int **nnodes_part, int **partition_vec,
		      int **d_nz, int **nd_nz);

void MeshPartition(int flagprt, double *weights, int size,
		   int nsd, int nnodes, int nnpe, int ngroups,
		   int *nelempgrp, double *coordinates, int *elements,
		   int *edgecut, int **nnodes_part, int **partition_vec,
		   int **d_nz, int **nd_nz);

#define COORDINATES       0
#define CONNECTIVITIES    1
#define BOTH              2 

void  ReadMesh(char *input_name, int *nsd, double **coordinates, int *nnodes, 
	       int **nnpe, int **elements, int *ngroups, int **nelempgrp, 
	       int *flag);

void SavePartition(char *name,int *particion,int Nnodes,int Nparts,
		   int  edgecut);

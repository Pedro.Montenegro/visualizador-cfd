#include "petsc.h"

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#define StageOn stageon_
#endif

#if defined (HAVE_FORTRAN_PRE_DOT)
#define StageOn .stageon
#endif

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#if defined(PETSC_HAVE_FORTRAN_UNDERSCORE_UNDERSCORE)
#define Fortran_StageOn fortran_stageon__
#else
#define Fortran_StageOn fortran_stageon_
#endif
#endif

#if defined (HAVE_FORTRAN_PRE_DOT)
#define Fortran_StageOn .fortran_stageon
#endif

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#define StageOff stageoff_
#endif

#if defined (HAVE_FORTRAN_PRE_DOT)
#define StageOff .stageoff
#endif

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#if defined(PETSC_HAVE_FORTRAN_UNDERSCORE_UNDERSCORE)
#define Fortran_StageOn fortran_stageon__
#else
#define Fortran_StageOn fortran_stageon_
#endif
#endif

#if defined (HAVE_FORTRAN_PRE_DOT)
#define Fortran_StageOn .fortran_stageon
#endif

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#if defined(PETSC_HAVE_FORTRAN_UNDERSCORE_UNDERSCORE)
#define Fortran_GetMemUsage fortran_getmemusage__
#else
#define Fortran_GetMemUsage fortran_getmemusage_
#endif
#endif

#if defined (HAVE_FORTRAN_PRE_DOT)
#define Fortran_GetMemUsage .fortran_getmemusage
#endif

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#define ResumeTimming resumetimming_
#endif

#if defined (HAVE_FORTRAN_PRE_DOT)
#define ResumeTimming .resumetimming
#endif

#define APP    1
#define WRI    0

void StageOn(char *filename);
void StageOff();
void ResumeTimming(char *ffilename, int *lenght, int *fmpn);
void GetMemUsage(char *filename, int mpn, MPI_Comm Comm, int flag);

#include "math.h"
#include "data_structures.h"

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)	 
#define setfields setfields_
#define setfldsnprev setfldsnprev_
#endif

typedef void(*F2)(int *, int *, int *, int *, 
		  double *, double *, double *, double *,
		  PetscScalar *, double *, int *);

void setfields(F2 seqsetf, systemdata **fSy, Vec *fv, double *fparcon, 
		  double *parmat, double *parnum, double *pargen);

typedef void(*F319)(int *, int *, int *, int *, 
		    double *, double *, double *, double *,
		    PetscScalar *, PetscScalar *, double *, int *);

void setfldsnprev(F319 seqsetf, systemdata **fSy, Vec *fv, double *fparcon, 
		  double *parmat, double *parnum, double *pargen);

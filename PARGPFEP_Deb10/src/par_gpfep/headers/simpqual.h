#include "data_structures.h"

double simplex_qual (int nsd, double *coords, int type);

void pgpfep_mesh_quality(partition *Pa, int *vqual);

void pgpfep_mesh_quality_disp(partition *Pa, int *vqual, Vec disp);

void pgpfep_mesh_quality_disp_ffields(partition *Pa, int *vqual, Vec v,
				      int sclpn, int idisp);

void meshquality_ (partition **fPa, int *vqual);

void meshqualitydisp_ (partition **fPa, int *vqual, Vec *disp);

void meshqualitydispffields_ (partition **fPa, int *vqual, Vec *v, 
			      systemdata **fSy, int *fsclpn, int *fidisp);

#include "petsc.h"

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#define pgpfepgetva pgpfepgetva_
#define systeminit systeminit_
#define systemmembers systemmembers_
#define createextvecnsgc createextvecnsgc_
#define createghostedfield createghostedfield_
#define systemquadrat systemquadrat_
#define systemdestroy systemdestroy_
#define systemquaddestroy systemquaddestroy_
#define tradtyp       tradtyp_
#define tradcod       tradcod_
#endif

#include "data_structures.h"
#include "assembly_flags.h"

void pgpfepgetva(systemdata **fSy, Vec *fvara);
void systeminit(systemdata **fSy, Vec *fvara, partition **fPa, 
		partition **SurPa, int *fnfields, int *fmpn);

void systemmembers(systemdata **fSy, Mat *fA, Vec *fx, Vec *fb, int *fnfa, 
		   int *fmpn, int *flag);

/* void createextvecnsgc(systemdata **fSy, int *fnfa, Vec *distvec, */
/*                       Vec *extvec, VecScatter *fsgc); */

void systemquadrat(systemdata **fSy, int *fnvar, int *icomp, int *npe,
		   int *fmaxgau, int *fieltype, int *fitgdeg, int *itpcod, 
		   int *fitpgeo, int *fitgtyp, int *kmesh,  int *fmpn);

void systemdestroy(systemdata **fSy);

void systemquaddestroy(systemquad **fSq);

void tradtyp(int *, int *);

void tradcod(int *, int *);

#include "petsc.h"

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#define speedsearch speedsearch_
#endif

#if defined (HAVE_FORTRAN_PRE_DOT)
#define speedsearch .speedsearch
#endif

void speedsearch(int *fSize, double *fDensidad, int *fseed_1, 
		 int *fseed_2, double **fweights, int *fmpn);

#include "petsc.h"

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#define v2x v2x_
#define va2x va2x_
#define v2xc v2xc_
#define x2v x2v_
#define x2vnorm x2vnorm_
#define pgpfepx2v pgpfepx2v_
#define x2v2va x2v2va_
#define x2v2vanorm x2v2vanorm_
#define v2va v2va_
#define va2vshift va2vshift_
#endif

#include "data_structures.h"

void v2x(systemdata **fSy, Vec *fv, Vec *fx, int *nfa, int *flnfa,int *option);
void va2x(systemdata **fSy, Vec *fv, Vec *fx, int *nfa, int *flnfa,int *option);
void v2xc (Vec *fv, Vec *fx, int *fnfields, int *fnfa, int *flnfa,int *option);
void x2v(systemdata **fSy, Vec *fv, Vec *fx, int *fmpn, int *nfa, int *flnfa,
	 int *option);
void x2vnorm (systemdata **fSy, Vec *fv, Vec *fx,
	      int *nfa, int *flnfa, int *option,
	      int *norm, PetscScalar *nvec, PetscScalar *nupd);

void x2v2va (systemdata **fSy, Vec *fv, Vec *fx, int *fmpn,
	     int *nfa, int *flnfa, int *option);
void x2v2vanorm (systemdata **fSy, Vec *fv, Vec *fx,
		 int *nfa, int *flnfa, int *option,
		 int *norm, PetscScalar *nvec, PetscScalar *nupd);

void v2va(systemdata **fSy, Vec *fv);
void va2vshift(systemdata **fSy, Vec *fv, int *fnfao, int *fnfad, int *flnfa);

#define ADD    0
#define INSERT 1

/*
#include <math.h>
#include "ggtree.h"
#include "data_structures.h"
*/
#include <malloc.h>
#include "interp.h"

/*
 * stats_interpol:
 * Get statistics about the data structures for an efficient geometric search
 * Pa(input,output): Data structure
 * levels: # of levels of the geometric tree
 * leaves: # of leaves of the geometric tree
 * ndata: Total number of data (elements) stored at the leaves
 * dpl: Average number of data elements in the tree leaves
 */

/*
 * plot_interpol:
 * Get statistics about the data structures for an efficient geometric search
 * Pa(input,output): Data structure
 * levels: # of levels of the geometric tree
 * leaves: # of leaves of the geometric tree
 * ndata: Total number of data (elements) stored at the leaves
 * dpl: Average number of data elements in the tree leaves
 */
/* Fortran interface */
void plotinterpol_ (partition **fPa, int *mode, int *retv)
{
  *retv = plot_interpol (*fPa, *mode);
}
int plot_interpol (partition *Pa, int mode)
{
  int retval, rank, ierr;
  FILE *fp;
  char file_name[150], name[170];
  PetscBool pt_flg;

  ierr = PetscOptionsGetString
    (NULL, "pgpfep_","-prtgt", file_name, 149,
     &pt_flg);                    CHKERRABORT(PETSC_COMM_WORLD,ierr);
  if (pt_flg  == PETSC_TRUE)
    {
      MPI_Comm_rank (Pa->Comm, &rank);
      if (mode % 2)
	sprintf (name, "%s-solid-%d.dat", file_name, rank);
      else
	sprintf (name, "%s-fluid-%d.dat", file_name, rank);
      fp = fopen(name,"w");
      if (!fp)
	return -1;

      retval = gg_tree_plot (Pa->gt, mode, fp);
      fclose(fp);
    }
  else
    retval = 0;
  return retval;
}

/* Fortran interface */
void statsinterpol_ (partition **fPa, int *levels, int *leaves, int *ndata,
		     double *dpl, int *retv)
{
  *retv = stats_interpol (*fPa, levels, leaves, ndata, dpl);
}

int stats_interpol (partition *Pa, int *levels, int *leaves, int *ndata,
		    double *dpl)
{
  int retval, ierr, mlevels, tleaves, tndata;
  retval = gg_tree_stats (Pa->gt, levels, leaves, ndata, dpl);
  ierr  = MPI_Allreduce (levels, &mlevels, 1, MPI_INT, MPI_MAX, Pa->Comm);
  ierr += MPI_Allreduce (leaves, &tleaves, 1, MPI_INT, MPI_SUM, Pa->Comm);
  ierr += MPI_Allreduce (ndata,  &tndata,  1, MPI_INT, MPI_SUM, Pa->Comm);
  *dpl = ((double)tndata) / tleaves;
  *levels = mlevels;
  *leaves = tleaves;
  *ndata = tndata;

  return retval + ierr;
}

/*
 * init_interpol:
 * Creates data structures for an efficient geometric search of elements
 * of the mesh (include local elements, as well as intra-proccesses elements
 * arguments:
 * Pa(input,output): Data structure with distributed mesh
 * return value: 0: everthing OK, < 0: error
 */

/* Fortran interface */
void initinterpol_ (partition **fPa, int *retv)
{
  *retv = init_interpol (*fPa);
}
/* Implementation */
int init_interpol (partition *Pa)
{
  int iel, in, isd;
  int ije, no;
  double xx, xmi, xma;
  double bbox[6];

  for (isd = 0; isd < Pa->nsd; isd++)
    bbox[2*isd] = bbox[2*isd+1] = Pa->cogeo[isd];

  /* Compute bounding box:
   *  local + ghost nodes:
   */
  for (in = 1; in < Pa->nodloc + Pa->nodghost; in++)
    {
      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  if (Pa->cogeo[in*Pa->nsd+isd] < bbox[2*isd])
	    bbox[2*isd] = Pa->cogeo[in*Pa->nsd+isd];
	  else if (Pa->cogeo[in*Pa->nsd+isd] > bbox[2*isd+1])
	    bbox[2*isd+1] = Pa->cogeo[in*Pa->nsd+isd];
	}
    }

  /* Create tree */
  Pa->gt = (gg_tree *) malloc (sizeof (gg_tree));
  if (!Pa->gt)
    return -1;
  gg_tree_init (Pa->gt, Pa->nsd, sizeof(int), bbox);

  /* Adjust size using element size (diameter 1st dimension)
   *  - only x-size
   *  - at last node of the element
   *  - only use local elements
   */
  for (iel = 0; iel < Pa->nelemloc + Pa->nelemext; iel++)
    {
      ije = Pa->nnpe * iel;
      no = Pa->Mesh[ije];
      xmi = xma = Pa->cogeo[no*Pa->nsd];
      for (in = 1; in < Pa->nnpe; in++)
	{
	  no = Pa->Mesh[++ije];
	  xx = Pa->cogeo[no*Pa->nsd];
	  if (xx > xma) xma = xx;
	  else if (xx < xmi) xmi = xx;
	}
      gg_tree_adjustsize (Pa->gt, Pa->cogeo+no*Pa->nsd, xma-xmi);
    }

  /* Insert data: element numbers (for local elements) */
  for (iel = 0; iel < Pa->nelemloc + Pa->nelemext; iel++)
    {
      /* Compute bounding box: */
      ije = Pa->nnpe * iel;  /* start of element in conectivity */
      no = Pa->Mesh[ije]; /* first node of this element */

      for (isd = 0; isd < Pa->nsd; isd++)
	bbox[2*isd] = bbox[2*isd+1] = Pa->cogeo[no*Pa->nsd+isd];

      for (in = 1; in < Pa->nnpe; in++) /* rest of nodes */
	{
	  no = Pa->Mesh[++ije];

	  for (isd = 0; isd < Pa->nsd; isd++)
	    {
	      if (Pa->cogeo[no*Pa->nsd+isd] < bbox[2*isd])
		bbox[2*isd] = Pa->cogeo[no*Pa->nsd+isd];
	      else if (Pa->cogeo[no*Pa->nsd+isd] > bbox[2*isd+1])
		bbox[2*isd+1] = Pa->cogeo[no*Pa->nsd+isd];
	    }
	}
      gg_tree_insertdata (Pa->gt, (void *)(&iel), bbox);
    }

  return 0;
}

/*
 * in_interp:
 * Creates data structures for an efficient geometric search of elements
 * of the mesh (include local elements, as well as intra-proccesses elements
 * arguments:
 * Pa(input,output): Data structure with distributed mesh
 * tree_size(input): adjust refinement of the auxiliary tree,
 *     1.0: same as init_interpol
 *     > 1.0: less refinement, less memory, more time
 *     < 1.0: more refinement, more memory, less time
 *     recommended: ~ 4.0
 * periodic(input): not zero if the mesh is periodic
 * periodicity(input)(1...nsd): if > 0 periodicity in each spatial dimension
 *
 * return value: 0: everthing OK, < 0: error
 */

/* Fortran interface */
void ininterp_ (partition **fPa, double *tree_size,
		int *swperiodic, double *per_sizes, int *retv)
{
  *retv = in_interp (*fPa, *tree_size, *swperiodic,
		     per_sizes);
}
/* Implementation */
int in_interp (partition *Pa, double tree_size,
	       int periodic, double *periodicity)
{
  int iel, in, isd;
  int ije, no;
  int perswitch;
  double xx, xmi, xma;
  double bbox[6];

  perswitch = 0;
  if (periodic)
    {
      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  Pa->per[isd] = periodicity[isd];
	  if (periodicity[isd] > 0.0)
	    perswitch += 1 << isd;
	}
    }
  Pa->MeshKind |= perswitch << 1;

  for (isd = 0; isd < Pa->nsd; isd++)
    bbox[2*isd] = bbox[2*isd+1] = Pa->cogeo[isd];

  /* Compute bounding box:
   *  local nodes:
   */
  for (in = 1; in < Pa->nodloc + Pa->nodghost; in++)
    {
      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  if (Pa->cogeo[in*Pa->nsd+isd] < bbox[2*isd])
	    bbox[2*isd] = Pa->cogeo[in*Pa->nsd+isd];
	  else if (Pa->cogeo[in*Pa->nsd+isd] > bbox[2*isd+1])
	    bbox[2*isd+1] = Pa->cogeo[in*Pa->nsd+isd];
	}
    }

  /*
   * If mesh is periodic periodicity is the max value:
   */
  if (periodic)
    {
      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  if (periodicity[isd] > 0.0 &&
	      periodicity[isd] > bbox[2*isd+1])
	    bbox[2*isd+1] = periodicity[isd];
	}
    }

  /* Create tree */
  Pa->gt = (gg_tree *) malloc (sizeof (gg_tree));
  if (!Pa->gt)
    return -1;
  gg_tree_init (Pa->gt, Pa->nsd, sizeof(int), bbox);

  /* Adjust size using element size (diameter 1st dimension)
   *  - only x-size
   *  - at last node of the element
   *  - only use local elements
   */

  for (iel = 0; iel < Pa->nelemloc; iel++)
    {
      ije = Pa->nnpe * iel;
      no = Pa->Mesh[ije];
      xmi = xma = Pa->cogeo[no*Pa->nsd];
      for (in = 1; in < Pa->nnpe; in++)
	{
	  no = Pa->Mesh[++ije];
	  xx = Pa->cogeo[no*Pa->nsd];
	  if (xx > xma) xma = xx;
	  else if (xx < xmi) xmi = xx;
	}
      gg_tree_adjustsize (Pa->gt, Pa->cogeo+no*Pa->nsd,
			  tree_size * (xma-xmi));
    }

  /* Insert data: element numbers (for local elements) */
  for (iel = 0; iel < Pa->nelemloc + Pa->nelemext; iel++)
    {
      /* Compute bounding box: */
      ije = Pa->nnpe * iel;  /* start of element in conectivity */
      no = Pa->Mesh[ije]; /* first node of this element */

      for (isd = 0; isd < Pa->nsd; isd++)
	bbox[2*isd] = bbox[2*isd+1] = Pa->cogeo[no*Pa->nsd+isd];

      for (in = 1; in < Pa->nnpe; in++) /* rest of nodes */
	{
	  no = Pa->Mesh[++ije];

	  for (isd = 0; isd < Pa->nsd; isd++)
	    {
	      if (Pa->cogeo[no*Pa->nsd+isd] < bbox[2*isd])
		bbox[2*isd] = Pa->cogeo[no*Pa->nsd+isd];
	      else if (Pa->cogeo[no*Pa->nsd+isd] > bbox[2*isd+1])
		bbox[2*isd+1] = Pa->cogeo[no*Pa->nsd+isd];
	    }
	}
      /*
       * Check for periodicity
       */
      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  if (perswitch & (1 << isd))
	    if (bbox[2*isd+1] - bbox[2*isd] > periodicity[isd]/2.0)
	      { /* This element is too big, re-compute !!! */
		for (in = 0; in < Pa->nnpe; in++) /* sweep nodes */
		  {
		    no = Pa->Mesh[Pa->nnpe * iel + in];
		    xx = Pa->cogeo[no*Pa->nsd+isd];
		    if (xx < periodicity[isd]/2)
		      xx += periodicity[isd];
		    if (in == 0)
		      bbox[2*isd] = bbox[2*isd+1] = xx;
		    else if (xx < bbox[2*isd])
		      bbox[2*isd] = xx;
		    else if (xx > bbox[2*isd+1])
		      bbox[2*isd+1] = xx;
		  }
	      }
	}
      gg_tree_insertdata (Pa->gt, (void *)(&iel), bbox);
    }

  return 0;
}

/*
 * in_interpdisp:
 * Creates data structures for an efficient geometric search of elements
 * of the mesh (include local elements, as well as intra-proccesses elements
 * arguments:
 * Pa(input,output): Data structure with distributed mesh
 * tree_size(input): adjust refinement of the auxiliary tree,
 *     1.0: same as init_interpol
 *     > 1.0: less refinement, less memory, more time
 *     < 1.0: more refinement, more memory, less time
 *     recommended: ~ 4.0
 * disp(input): displacement of the nodes, distributed vector
 *   with same structure as Pa->cogeo: disp[inode*nsd+isd]
 *   and with ghosted nodes
 *
 * return value: 0: everthing OK, < 0: error
 *
 */
/* Fortran interface */
void ininterpdisp_ (partition **fPa, double *tree_size,
		    Vec *disp, int *retv)
{
  *retv = in_interpdisp(*fPa, *tree_size, *disp);
}
/* Implementation */
int in_interpdisp (partition *Pa, double tree_size, Vec disp)
{
  int iel, in, isd;
  int ije, no;
  int nsd, ierr;

  double xx, xmi, xma;
  double bbox[6];
  Vec ldisp;
  double *dscan;

  nsd = Pa->nsd;

  ierr = VecGhostGetLocalForm(disp, &ldisp); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGetArray(ldisp, &dscan);         CHKERRABORT(PETSC_COMM_WORLD,ierr);

  for (isd = 0; isd < Pa->nsd; isd++)
    bbox[2*isd] = bbox[2*isd+1] = Pa->cogeo[isd]+dscan[isd];

  /* Compute bounding box:
   *  local nodes:
   */
  for (in = 1; in < Pa->nodloc + Pa->nodghost; in++)
    {
      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  double xx = Pa->cogeo[in*nsd+isd] + dscan[in*nsd+isd];
	  if (xx < bbox[2*isd])
	    bbox[2*isd] = xx;
	  else if (xx > bbox[2*isd+1])
	    bbox[2*isd+1] = xx;
	}
    }

  /* Create tree */
  Pa->gt = (gg_tree *) malloc (sizeof (gg_tree));
  if (!Pa->gt)
    return -1;
  gg_tree_init (Pa->gt, Pa->nsd, sizeof(int), bbox);

  /* Adjust size using element size (diameter 1st dimension)
   *  - only x-size
   *  - at last node of the element
   *  - only use local elements
   */

  for (iel = 0; iel < Pa->nelemloc + Pa->nelemext; iel++)
    {
      double point[nsd];
      ije = Pa->nnpe * iel;
      no = Pa->Mesh[ije];
      xmi = xma = Pa->cogeo[no*nsd] + dscan[no*nsd];
      for (in = 1; in < Pa->nnpe; in++)
	{
	  no = Pa->Mesh[++ije];
	  xx = Pa->cogeo[no*nsd] + dscan[no*nsd];
	  if (xx > xma) xma = xx;
	  else if (xx < xmi) xmi = xx;
	}
      for (isd = 0; isd < nsd; isd++)
	point[isd] = Pa->cogeo[no*nsd+isd] + dscan[no*nsd+isd];
      gg_tree_adjustsize (Pa->gt, point,
			  tree_size * (xma-xmi));
    }

  /* Insert data: element numbers (for local elements) */
  for (iel = 0; iel < Pa->nelemloc + Pa->nelemext; iel++)
    {
      /* Compute bounding box: */
      ije = Pa->nnpe * iel;  /* start of element in conectivity */
      no = Pa->Mesh[ije]; /* first node of this element */

      for (isd = 0; isd < Pa->nsd; isd++)
	bbox[2*isd] = bbox[2*isd+1] =
	  Pa->cogeo[no*nsd+isd]+dscan[no*nsd+isd];

      for (in = 1; in < Pa->nnpe; in++) /* rest of nodes */
	{
	  no = Pa->Mesh[++ije];

	  for (isd = 0; isd < Pa->nsd; isd++)
	    {
	      double xx = Pa->cogeo[no*nsd+isd] + dscan[no*nsd+isd];
	      if ( xx < bbox[2*isd])
		bbox[2*isd] = xx;
	      else if (xx > bbox[2*isd+1])
		bbox[2*isd+1] = xx;
	    }
	}
      gg_tree_insertdata (Pa->gt, (void *)(&iel), bbox);
    }

  ierr = VecRestoreArray(ldisp, &dscan);   CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGhostRestoreLocalForm(disp,
				  &ldisp); CHKERRABORT(PETSC_COMM_WORLD, ierr);
  return 0;
}

/*
 * in_interpdisp_ffields:
 * Creates data structures for an efficient geometric search of elements
 * of the mesh (include local elements, as well as intra-proccesses elements
 * arguments:
 * Pa(input,output): Data structure with distributed mesh
 * tree_size(input): adjust refinement of the auxiliary tree,
 *     1.0: same as init_interpol
 *     > 1.0: less refinement, less memory, more time
 *     < 1.0: more refinement, more memory, less time
 *     recommended: ~ 4.0
 * v(input): vector of fields over nodes (including displacements)
 *          (v[inod*sclpn+iscl+isd]) (with ghost nodes)
 *
 * return value: 0: everthing OK, < 0: error
 *
 */
/* Fortran interface */
void ininterpdispffields_ (partition **fPa, double *tree_size, Vec *v,
			   int *fsclpn, int *fidisp, int *retv)
{
  *retv = in_interpdisp_ffields(*fPa, *tree_size, *v,
				*fsclpn, *fidisp);
}
/* Implementation */
int in_interpdisp_ffields (partition *Pa, double tree_size, Vec v,
			   int sclpn, int idisp)
{
  int iel, in, isd;
  int ije, no;
  int nsd, ierr;

  double xx, xmi, xma;
  double bbox[6];
  double *dscan;
  Vec lv;

  nsd = Pa->nsd;
  ierr = VecGhostGetLocalForm(v, &lv); CHKERRABORT(PETSC_COMM_WORLD, ierr);
  if (lv) /* v was indeed a ghosted vector */
    { ierr = VecGetArray(lv, &dscan);  CHKERRABORT(PETSC_COMM_WORLD, ierr); }
  else    /* v wasn't a ghosted vector (as in immersed solid meshes) */
    { ierr = VecGetArray(v, &dscan);   CHKERRABORT(PETSC_COMM_WORLD, ierr); }

  for (isd = 0; isd < Pa->nsd; isd++)
    bbox[2*isd] = bbox[2*isd+1] = Pa->cogeo[isd]+dscan[idisp+isd];

  /* Compute bounding box:
   *  local nodes:
   */
  for (in = 1; in < Pa->nodloc + Pa->nodghost; in++)
    {
      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  double xx = Pa->cogeo[in*nsd+isd] + dscan[in*sclpn+idisp+isd];
	  if (xx < bbox[2*isd])
	    bbox[2*isd] = xx;
	  else if (xx > bbox[2*isd+1])
	    bbox[2*isd+1] = xx;
	}
    }

  /* Create tree */
  Pa->gt = (gg_tree *) malloc (sizeof (gg_tree));
  if (!Pa->gt)
    return -1;
  gg_tree_init (Pa->gt, Pa->nsd, sizeof(int), bbox);

  /* Adjust size using element size (diameter 1st dimension)
   *  - only x-size
   *  - at last node of the element
   *  - only use local elements
   */

  for (iel = 0; iel < Pa->nelemloc + Pa->nelemext; iel++)
    {
      double point[nsd];
      ije = Pa->nnpe * iel;
      no = Pa->Mesh[ije];
      xmi = xma = Pa->cogeo[no*nsd] + dscan[no*sclpn+idisp];
      for (in = 1; in < Pa->nnpe; in++)
	{
	  no = Pa->Mesh[++ije];
	  xx = Pa->cogeo[no*nsd] + dscan[no*sclpn+idisp];
	  if (xx > xma) xma = xx;
	  else if (xx < xmi) xmi = xx;
	}
      for (isd = 0; isd < nsd; isd++)
	point[isd] = Pa->cogeo[no*nsd+isd] + dscan[no*sclpn+idisp+isd];
      gg_tree_adjustsize (Pa->gt, point,
			  tree_size * (xma-xmi));
    }

  /* Insert data: element numbers (for local and ext elements) */
  for (iel = 0; iel < Pa->nelemloc + Pa->nelemext; iel++)
    {
      /* Compute bounding box: */
      ije = Pa->nnpe * iel;  /* start of element in conectivity */
      no = Pa->Mesh[ije]; /* first node of this element */

      for (isd = 0; isd < Pa->nsd; isd++)
	bbox[2*isd] = bbox[2*isd+1] =
	  Pa->cogeo[no*nsd+isd]+dscan[no*sclpn+idisp+isd];

      for (in = 1; in < Pa->nnpe; in++) /* rest of nodes */
	{
	  no = Pa->Mesh[++ije];

	  for (isd = 0; isd < Pa->nsd; isd++)
	    {
	      double xx = Pa->cogeo[no*nsd+isd] + dscan[no*sclpn+idisp+isd];
	      if ( xx < bbox[2*isd])
		bbox[2*isd] = xx;
	      else if (xx > bbox[2*isd+1])
		bbox[2*isd+1] = xx;
	    }
	}
      gg_tree_insertdata (Pa->gt, (void *)(&iel), bbox);
    }

  if (lv) /* v was indeed a ghosted vector */
    { ierr = VecRestoreArray(lv, &dscan); CHKERRABORT(PETSC_COMM_WORLD, ierr);}
  else
    { ierr = VecRestoreArray(v, &dscan);  CHKERRABORT(PETSC_COMM_WORLD, ierr);}
  
  ierr = VecGhostRestoreLocalForm(v, &lv); CHKERRABORT(PETSC_COMM_WORLD, ierr);
  return 0;
}

/*
 * stop_interp:
 * Clears data structures for efficient search
 * Arguments:
 * Pa(input,output): Data structure with distributed mesh
 */
/* Fortran interface */
void stopinterp_ (partition **fPa, int *retv)
{
  *retv = stop_interp(*fPa);
}
/* Implementation */
int stop_interp (partition *Pa)
{
  gg_tree_clear (Pa->gt);
  Pa->gt = NULL;
  return 0;
}

/*
 * interp_local:
 * Given a point and a field over the nodes of the mesh this function
 * search the element containing the point and interpolates the field.
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * point(input): point where the function is needed
 * vsc(input): Vector of values of the fields on the local (and ghost) nodes
 * stride(input): number of values in each node.
 * start(input): position of the field in the list of values of the node
 * ncomps(input): components of the field to interpolate
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * result(output): pointer to memory where the results should be placed
 *        (ncomps components of the interpolated field)
 *
 * return value: 0: OK, interpolation done
 *              -1: point outside local mesh
 */
/* Fortran interface:
 * Extra arguments:
 *    fSy: (system data): used for accessing the local components of the
 *        "external" vector. Also for the "stride" argument
 *    fv: Petsc vector with unknowns
 */
void interplocal_ (partition **fPa, systemdata **fSy, Vec *fv, double *point,
		   int *start, int *ncomps,
		   double *result, int *mode, int *retv)
{
  double *vsc;
  Vec lv;
  VecGhostGetLocalForm (*fv, &lv);
  VecGetArray(lv, &vsc);
  *retv = interp_local(*fPa, point, vsc,
		       (*fSy)->nfields, *start, *ncomps, *mode, result);
  VecRestoreArray(lv, &vsc);
  VecGhostRestoreLocalForm (*fv, &lv);
}
/* Implementation */
int interp_local (partition *Pa, double *point, double *vsc,
		  int stride, int start, int ncomps, int mode,
		  double *result)
{
  int iel, ic, in, no, rval;
  double weights[Pa->nnpe];

  rval = search_local (Pa, point, mode, &iel, weights);

  if (rval >= 0) /* -1: not found, 0: found in local mesh, 1: in extmesh */
    {
      for (ic = 0; ic < ncomps; ic++)
	{
	  result[ic] = 0;
	  for (in = 0; in < Pa->nnpe; in++)
	    {
	      no = Pa->Mesh[Pa->nnpe * iel + in];
	      result[ic] += weights[in] * vsc[start + no * stride + ic];
	    }
	}
      return 0;
    }
  return -1; /* Not found */
}

/*
 * interp_local_disp:
 * Given a point and a field over the nodes of the mesh this function
 * search the element containing the point and interpolates the field,
 * (use a displacement of the nodes)
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * disp: Displacement of the nodes (whith ghosted values)
 * point(input): point where the function is needed
 * vsc(input): Vector of values of the fields on the local nodes (+ ghost)
 * stride(input): number of values in each node.
 * start(input): position of the field in the list of values of the node
 * ncomps(input): components of the field to interpolate
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * result(output): pointer to memory where the results should be placed
 *        (ncomps components of the interpolated field)
 *
 * return value: 0: OK, interpolation done
 *              -1: point outside local mesh
 */
/* Fortran interface:
 * Extra arguments:
 *    fSy: (system data): used for accessing the local components of the
 *        "external" vector. Also for the "stride" argument
 *    fv: Petsc vector with unknowns
 */
void interplocaldisp_ (partition **fPa, Vec *disp,
		       double *point, Vec *fv, systemdata **fSy,
		       int *start, int *ncomps, int *mode,
		       double *result, int *retv)
{
  double *vsc, *scandisp;
  Vec lv, ldisp;
  
  VecGhostGetLocalForm (*disp, &ldisp);
  VecGetArray(ldisp, &scandisp);
  VecGhostGetLocalForm (*fv, &lv);
  VecGetArray(lv, &vsc);
  *retv = interp_local_disp (*fPa, scandisp,
			     point, vsc,
			     (*fSy)->nfields, *start, *ncomps, *mode,
			     result);
  VecRestoreArray(lv, &vsc);
  VecGhostRestoreLocalForm (*fv, &lv);
  VecRestoreArray(ldisp, &scandisp);
  VecGhostRestoreLocalForm (*disp, &ldisp);
}
/* Implementation */
int interp_local_disp (partition *Pa, double *disp,
		       double *point, double *vsc,
		       int stride, int start, int ncomps, int mode,
		       double *result)
{
  int iel, ic, in, no, rval;
  double weights[Pa->nnpe];

  rval = search_local_disp (Pa, disp, point, mode, &iel, weights);

  if (rval >= 0) /* -1: not found, 0: found in local mesh, 1: in extmesh */
    {
      for (ic = 0; ic < ncomps; ic++)
	{
	  result[ic] = 0;
	  for (in = 0; in < Pa->nnpe; in++)
	    {
	      no = Pa->Mesh[Pa->nnpe * iel + in];
	      result[ic] += weights[in] * vsc[start + no * stride + ic];
	    }
	}
      return 0;
    }
  return -1; /* Not found */
}

/*
 * interp_local_disp_ffields:
 * Given a point and a field over the nodes of the mesh this function
 * search the element containing the point and interpolates the field,
 * (use displacement of the nodes contained in an unknowns vector)
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * v(input): vector of fields over nodes (including displacements)
 *          (v[inod*sclpn+iscl+isd])
 * sclpn: scalars per node
 * idisp: index of displacement in the scalars of a node (starts at 0)
 * point(input): point where the function is needed
 * vsc(input): Vector of values of the fields on the local nodes
 * stride(input): number of values in each node.
 * start(input): position of the field in the list of values of the node
 * ncomps(input): components of the field to interpolate
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * result(output): pointer to memory where the results should be placed
 *        (ncomps components of the interpolated field)
 *
 * return value: 0: OK, interpolation done
 *              -1: point outside local mesh
 */
/* Fortran interface:
 * Extra arguments:
 *    fSy: (system data): used for accessing the local components of the
 *        "external" vector. Also for the "stride" argument
 *    fv: Petsc vector with unknowns
 */
void interplocaldispffields_ (partition **fPa, systemdata **fSy,
			      Vec *v, int *fsclpn, int *fidisp,
			      double *point, Vec *fv,
			      int *start, int *ncomps, int *mode,
			      double *result, int *retv)
{
  double *vsc, *scanv, *scanva;
  Vec lv, lva, lfv;

  VecGhostGetLocalForm (*v, &lv);
  VecGetArray(lv, &scanv);
  VecGhostGetLocalForm ((*fSy)->va, &lva);
  VecGetArray(lva, &scanva);

  VecGhostGetLocalForm (*fv, &lfv);
  VecGetArray(lfv, &vsc);

  *retv = interp_local_disp_ffields (*fPa,
				     scanv, *fsclpn, *fidisp,
				     point, vsc,
				     (*fSy)->nfields, *start, *ncomps, *mode,
				     result);

  VecRestoreArray(lfv, &vsc);
  VecGhostRestoreLocalForm (*fv, &lfv);

  VecRestoreArray(lva, &scanva);
  VecGhostRestoreLocalForm ((*fSy)->va, &lva);
  VecRestoreArray(lv, &scanv);
  VecGhostRestoreLocalForm (*v, &lv);
}

/* Implementation */
int interp_local_disp_ffields (partition *Pa,
			       double *v, int sclpn, int idisp,
			       double *point, double *vsc,
			       int stride, int start, int ncomps, int mode,
			       double *result)
{
  int iel, ic, in, no, rval;
  double weights[Pa->nnpe];

  rval = search_local_disp_ffields (Pa, v, sclpn, idisp,
				    point, mode, &iel, weights);

  if (rval >= 0) /* -1: not found, 0: found in local mesh, 1: in extmesh */
    {
      for (ic = 0; ic < ncomps; ic++)
	{
	  result[ic] = 0;
	  for (in = 0; in < Pa->nnpe; in++)
	    {
	      no = Pa->Mesh[Pa->nnpe * iel + in];
	      result[ic] += weights[in] * vsc[start + no * stride + ic];
	    }
	}
      return 0;
    }
  return -1; /* Not found */
}

/*
 * interp_local_disp_ffieldss:
 * Given a point and a field over the nodes of the mesh this function
 * search the element containing the point and interpolates the field,
 * (use displacement of the nodes contained in an unknowns vector)
 * This version takes the stride of the vsc vector as argument.
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * v(input): vector of fields over nodes (including displacements)
 *          (v[inod*sclpn+iscl+isd])
 * sclpn: scalars per node
 * idisp: index of displacement in the scalars of a node (starts at 0)
 * point(input): point where the function is needed
 * vsc(input): Vector of values of the fields on the local nodes
 * stride(input): number of values in each node.
 * start(input): position of the field in the list of values of the node
 * ncomps(input): components of the field to interpolate
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * result(output): pointer to memory where the results should be placed
 *        (ncomps components of the interpolated field)
 *
 * return value: 0: OK, interpolation done
 *              -1: point outside local mesh
 */
/* Fortran interface:
 * Extra arguments:
 *    fSy: (system data): used for accessing the local components of the
 *        "external" vector. Also for the "stride" argument
 *    fv: Petsc vector with unknowns
 */
void interplocaldispffieldss_ (partition **fPa, systemdata **fSy,
			      Vec *v, int *fsclpn, int *fidisp,
			      double *point, Vec *fv, int *fstride,
			      int *start, int *ncomps, int *mode,
			      double *result, int *retv)
{
  double *vsc, *scanv, *scanva;
  Vec lv, lva/*, lfv*/;

  VecGhostGetLocalForm (*v, &lv);
  VecGetArray(lv, &scanv);
  VecGhostGetLocalForm ((*fSy)->va, &lva);
  VecGetArray(lva, &scanva);

//  VecGhostGetLocalForm (*fv, &lfv);
  VecGetArray(*fv, &vsc);

  *retv = interp_local_disp_ffields (*fPa,
				     scanv, *fsclpn, *fidisp,
				     point, vsc,
				     *fstride, *start, *ncomps, *mode,
				     result);

  VecRestoreArray(*fv, &vsc);
//  VecGhostRestoreLocalForm (*fv, &lfv);

  VecRestoreArray(lva, &scanva);
  VecGhostRestoreLocalForm ((*fSy)->va, &lva);
  VecRestoreArray(lv, &scanv);
  VecGhostRestoreLocalForm (*v, &lv);
}

/*
 * search_local:
 * Given a point search the element containing it
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * point(input): point
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * elem(output): element containing the point
 * weights(output): weights for interpolation
 *         (one for each node of the element, sum(weigths) = 1)
 *
 * return value: -1: point outside local mesh
 *                0: point found in totally local elements
 *                1: point found in an intra-proccesss element
 */
/* Fortran interface */
void searchlocal_ (partition **fPa, double *point, int *mode,
		  int *elem, double *weights, int *retv)
{
  *retv = search_local(*fPa, point, *mode, elem, weights);
}
/* Implementation */
int search_local (partition *Pa, double *point, int mode,
		  int *elem, double *weights)
{
  int iel, ije, in, no, isd, skip, retval = -1;
  int perswitch;
  double coords[Pa->nnpe*Pa->nsd];
  node_g_list *nl;
  nl = gg_tree_getlisthead (Pa->gt, point);
  perswitch = Pa->MeshKind >> 1;

  while (nl)
    {
      iel = *((int *) nl->data);
      skip = 1;
      if (iel >= Pa->nelemloc && mode > 0) /* Element is in extmesh, but mode > 0 */
	{
	  /* iel = -iel-1; */
	  if (mode == 2 || Pa->GOS[iel] > 0) /* All elements in
					      * extmesh or element
					      * ownership */
	    {
	      skip = 0;
	      retval = 1;
	      ije = Pa->nnpe * iel;
	      for (in = 0; in < Pa->nnpe; in++)
		{
		  no = Pa->Mesh[ije++];
		  for (isd = 0; isd < Pa->nsd; isd++)
		    {
			coords[in * Pa->nsd + isd] =
			  Pa->cogeo[no*Pa->nsd+isd];
		    }
		}
	    }
	}
      else if (iel >= 0)
	{
	  skip = 0;
	  retval = 0;
	  ije = Pa->nnpe * iel;
	  for (in = 0; in < Pa->nnpe; in++)
	    {
	      no = Pa->Mesh[ije++];
	      for (isd = 0; isd < Pa->nsd; isd++)
		coords[in*Pa->nsd+isd] = Pa->cogeo[no*Pa->nsd+isd];
	    }
	}
      if (!skip)
	{   /* Correction for periodic coordinates */
	  if (perswitch)   /* general switch */
	    {
	      double cmin, cmax;
	      for (isd = 0; isd < Pa->nsd; isd++)
		{
		  if (perswitch & (1 << isd))  /* this coordinate switch */
		    {
		      cmin = cmax = coords[isd];  /* compute extension */
		      for (in = 1; in < Pa->nnpe; in++)
			{
			  if (coords[in*Pa->nsd+isd] < cmin)
			    cmin = coords[in*Pa->nsd+isd];
			  else if (coords[in*Pa->nsd+isd] > cmax)
			    cmax = coords[in*Pa->nsd+isd];
			}
		      if (cmax - cmin > Pa->per[isd] / 2.0)
			{   /* element too big */
			  for (in = 0; in < Pa->nnpe; in++)
			    {
			      if (coords[in*Pa->nsd+isd] < Pa->per[isd] / 2.0)
				coords[in*Pa->nsd+isd] += Pa->per[isd];
			    }
			}
		    }
		}
	    }
	  /* End correction for periodic coordinates */
	  if (point_in_element(Pa->nsd, Pa->nnpe, coords, point, weights))
	    {
	      *elem = iel;
	      return retval;
	    }
	}
      nl = nl->next;
    }
  return -1;
}

/*
 * search_local_disp:
 * Given a point search the element containing it, using a displaced mesh
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * disp(input): vector of displacements (disp[inod*nsd+isd])
 * point(input): point
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * elem(output): element containing the point
 * weights(output): weights for interpolation
 *         (one for each node of the element, sum(weigths) = 1)
 *
 * return value: -1: point outside local mesh
 *                0: point found in totally local elements
 *                1: point found in an intra-proccesss element
 */
/* Fortran interface */
void searchlocaldisp_ (partition **fPa, Vec *disp,
		       double *point, int *mode, int *elem, double *weights,
		       int *retv)
{
  double *scand;
  Vec ldisp;
  VecGhostGetLocalForm(*disp, &ldisp);
  VecGetArray(ldisp, &scand);
  *retv = search_local_disp (*fPa, scand, point, *mode, elem, weights);
  VecRestoreArray(ldisp, &scand);
  VecGhostRestoreLocalForm(*disp, &ldisp);
}
/* Implementation */
int search_local_disp (partition *Pa, double *disp,
		       double *point, int mode, int *elem, double *weights)
{
  int iel, ije, in, no, node, isd, skip, poscoo, retval = -1;
  /* int perswitch; */
  double coords[Pa->nnpe*Pa->nsd];
  node_g_list *nl;
  nl = gg_tree_getlisthead (Pa->gt, point);
  /* perswitch = Pa->MeshKind >> 1; */

  while (nl)
    {
      iel = *((int *) nl->data);
      skip = 1;
      if (iel >= Pa->nelemloc && mode > 0) /* Element is in extmesh, but mode > 0 */
	{
	  /* iel = -iel-1; */
	  if (mode == 2 || Pa->GOS[iel] > 0) /* All elements in
					      * extmesh or element
					      * ownership */
	    {
	      skip = 0;
	      retval = 1;
	      ije = Pa->nnpe * iel;
	      for (in = 0; in < Pa->nnpe; in++)
		{
		  node = Pa->Mesh[ije++];
		  for (isd = 0; isd < Pa->nsd; isd++)
		    {
		      poscoo = node * Pa->nsd + isd;
		      coords[in * Pa->nsd + isd] =
			Pa->cogeo[poscoo] + disp[poscoo];
		    }
		}
	    }
	}
      else if (iel >= 0)
	{
	  skip = 0;
	  retval = 0;
	  ije = Pa->nnpe * iel;
	  for (in = 0; in < Pa->nnpe; in++)
	    {
	      no = Pa->Mesh[ije++];
	      for (isd = 0; isd < Pa->nsd; isd++)
		{
		  poscoo = no * Pa->nsd + isd;
		  coords[in*Pa->nsd+isd] = Pa->cogeo[poscoo] + disp[poscoo];
		}
	    }
	}
      if (!skip &&
	  point_in_element(Pa->nsd, Pa->nnpe, coords, point, weights))
	{
	  *elem = iel;
	  return retval;
	}
      nl = nl->next;
    }
  return -1;
}

/*
 * search_local_disp_ffields:
 * Given a point search the element containing it, using a displaced mesh
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * v(input): vector of fields over nodes (including displacements)
 *          (v[inod*sclpn+iscl])
 * sclpn: scalars per node
 * idisp: index of displacement in the scalars of a node (starts at 0)
 * point(input): point
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * elem(output): element containing the point
 * weights(output): weights for interpolation
 *         (one for each node of the element, sum(weigths) = 1)
 *
 * return value: -1: point outside local mesh
 *                0: point found in totally local elements
 *                1: point found in an intra-proccesss element
 */
/* Fortran interface */
void searchlocaldispffields_ (partition **fPa, Vec *v,
			      int *fsclpn, int *fidisp,
			      double *point, int *mode,
			      int *elem, double *weights, int *retv)
{
  double *scanv;
  Vec lv;
  VecGhostGetLocalForm(*v, &lv);
  VecGetArray(lv, &scanv);
  *retv = search_local_disp_ffields (*fPa, scanv, *fsclpn, *fidisp,
				     point, *mode, elem, weights);
  VecRestoreArray(lv, &scanv);
  VecGhostRestoreLocalForm(*v, &lv);
}
/* Implementation */
int search_local_disp_ffields (partition *Pa,
			       double *v, int sclpn, int idisp,
			       double *point, int mode,
			       int *elem, double *weights)
{
  int iel, ije, in, no, isd, skip, poscoo, retval = -1;
  /* int perswitch; */
  double coords[Pa->nnpe*Pa->nsd];
  node_g_list *nl;
  nl = gg_tree_getlisthead (Pa->gt, point);
  /* perswitch = Pa->MeshKind >> 1; */

  while (nl)
    {
      iel = *((int *) nl->data);
      skip = 1;
      if (iel >= Pa->nelemloc && mode > 0) /* Element is in extmesh, but mode > 0 */
	{
	  /* iel = -iel-1; */
	  if (mode == 2 || Pa->GOS[iel] > 0) /* All elements in
					      * extmesh or element
					      * ownership */
	    {
	      skip = 0;
	      retval = 1;
	      ije = Pa->nnpe * iel;
	      for (in = 0; in < Pa->nnpe; in++)
		{
		  no = Pa->Mesh[ije++];
		  for (isd = 0; isd < Pa->nsd; isd++)
		    {
		      poscoo = no * Pa->nsd + isd;
		      coords[in * Pa->nsd + isd] =
			Pa->cogeo[poscoo] + v[no*sclpn+idisp+isd];
		    }
		}
	    }
	}
      else if (iel >= 0) /* Element is in locmesh (puede ser < 0?) */
	{
	  skip = 0;
	  retval = 0;
	  ije = Pa->nnpe * iel;
	  for (in = 0; in < Pa->nnpe; in++)
	    {
	      no = Pa->Mesh[ije++];
	      for (isd = 0; isd < Pa->nsd; isd++)
		{
		  poscoo = no * Pa->nsd + isd;
		  coords[in*Pa->nsd+isd] = Pa->cogeo[poscoo] +
		    v[no*sclpn+idisp+isd];
		}
	    }
	}
      if (!skip &&  /* Coordinates loaded, check point inside */
	  point_in_element(Pa->nsd, Pa->nnpe, coords, point, weights))
	{
	  *elem = iel;
	  return retval;
	}
      nl = nl->next;
    }
  return -1;
}

/*
 * interp_global:
 * Interpolate a field in a set of points, (all processes ask
 *  for a (different) set of points)
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * points(input): vector of points (x1, y1, ... x2, y2, .......)
 * npoints(input): number of points
 * vloc(input): Vector of values of the fields on the local nodes
 * vext(input): Vector of values of the fields on the "external" nodes
 * stride(input): number of values in each node
 * start(input): position of the field to be interpolated
 * ncomps(input): components of the field to be interpolated
 * results(output): vector to hold the results (npoints*ncomps)
 *
 * return value: number of points not found in mesh
 */
/* Fortran interface
 *  Extra arguments:
 *    fSy: (system data): used for accessing the local components of the
 *        "external" vector. Also for the "stride" argument
 *    fv: Petsc vector with unknowns
 */
void interpglobal_ (systemdata **fSy, double *points, int *npoints,
		    Vec *fv, int *start, int *ncomps, double *result,
		    int *sw, int *retv)
{
  double *Vloc;
  Vec lv;
  VecGhostGetLocalForm(*fv, &lv);
  VecGetArray(lv, &Vloc);

  *retv = interp_global ((*fSy)->Pa, points, *npoints, Vloc,
			 (*fSy)->nfields, *start, *ncomps,
			 result, sw);

  VecRestoreArray(lv, &Vloc);
  VecGhostRestoreLocalForm(*fv, &lv);
}
/* Implementation */
int interp_global (partition *Pa, double *points, int npoints,
		   double *vloc,
		   int stride, int start, int ncomps, double *results, int *sw)
{
  int i, ri, ip, isd;
  int nprocs;
  int snfp, totsnf, *vsnfp, *vcounts, *disps;
  int *snfsw, *allsnfsw;
  double *snfpts, *snfres;
  double *allsnfpts, *allsnfres;
  int rank , ierr;
  /* Profiling variables */
  int flocal, fexternal;
  double time0, time1, time2, time3, time4;

  time0 = MPI_Wtime();
  /* First step: interp_local */
  snfp = 0;
  for (ip = 0; ip < npoints; ip++)
    {
      sw[ip] = interp_local (Pa, points + ip * Pa->nsd, vloc,
			     stride, start, ncomps, 2,
			     results + ip * ncomps);
      if (sw[ip] == -1)
	snfp++;
    }

  MPI_Comm_size (Pa->Comm, &nprocs);
  if (nprocs == 1)
    {
      time1 = MPI_Wtime();
      PetscPrintf (Pa->Comm, "interp_global: found %d / %d points, %g Secs.\n",
		   npoints - snfp, npoints, time1-time0);
      return snfp;
    }
  flocal = npoints - snfp;
  time1 = MPI_Wtime();

  /* Second step: compact vector of still-not-found points */
  if (snfp > 0)
    {
      snfpts = (double *) malloc (snfp * Pa->nsd * sizeof(double));
      snfsw  = (int *) malloc (snfp * sizeof(int));
      snfres = (double *) malloc (snfp * ncomps * sizeof(double));
      if (!snfpts || !snfsw || !snfres)
	return snfp;

      for (ip = 0, snfp = 0; ip < npoints; ip++)
	if (sw[ip] == -1)
	  {
	    snfsw[snfp] = 0;
	    for (isd = 0; isd < Pa->nsd; isd++)
	      snfpts[Pa->nsd * snfp + isd] = points[ip * Pa->nsd + isd];
	    snfp++;
	  }
    }
  else
    {
      snfsw = NULL;
      snfpts = snfres = NULL;
    }

  /* Gather all still-not-found points: compute total number: */
  i = MPI_Allreduce (&snfp, &totsnf, 1, MPI_INT, MPI_SUM, Pa->Comm);

  if (totsnf == 0)
    {
      PetscSynchronizedPrintf
	(Pa->Comm, "interp_global: found locally all %d points, %g Secs.\n",
	 npoints, time1-time0);
      PetscSynchronizedFlush(Pa->Comm, stdout);
      return 0;
    }
  PetscPrintf
    (Pa->Comm, "interp_global (first search): %g Secs.\n", time1-time0);

  /* Alloc memory: */
  allsnfpts = (double *) malloc (totsnf * Pa->nsd * sizeof(double));
  allsnfsw  = (int *) malloc (totsnf * sizeof(int));
  allsnfres = (double *) malloc (totsnf * ncomps * sizeof(double));
  vsnfp = (int *) malloc (nprocs * sizeof(int));
  vcounts = (int *) malloc (nprocs * sizeof(int));
  disps = (int *) malloc (nprocs * sizeof(int));
  if (!allsnfpts || !allsnfsw || !allsnfres || !vsnfp || !disps)
    return -1;
  /* Gather all: */
  ierr = MPI_Allgather (&snfp, 1, MPI_INT, vsnfp, 1, MPI_INT, Pa->Comm);

  for (i = 0; i < nprocs; i++)
    {
      vcounts[i] = vsnfp[i] * Pa->nsd;
      if (i == 0)
	disps[i] = 0;
      else
	disps[i] = disps[i-1] + vcounts[i-1];
    }
    
  ierr += MPI_Allgatherv (snfpts, snfp*Pa->nsd, MPI_DOUBLE,
			  allsnfpts, vcounts, disps, MPI_DOUBLE, Pa->Comm);
  if (ierr)
    PetscPrintf (Pa->Comm, "WARNING: MPI_xxxGather error in interp_global\n");

  /* Free unneeded memory */
  if (snfpts)
    free(snfpts);

  time2 = MPI_Wtime();
  PetscPrintf
    (Pa->Comm, "interp_global (1st Communication stage): %g Secs.\n",
     time2-time1);

  /* All proccesses search all points:
   * (Optimization: skip local points)
   */
  MPI_Comm_rank (Pa->Comm, &rank);
  /* Re-build disps vector (to identify local points) */
  for (i = 0; i < nprocs; i++)
    {
      if (i == 0)
	disps[i] = 0;
      else
	disps[i] = disps[i-1] + vsnfp[i-1];
    }

  for (ip = 0; ip < totsnf; ip++)
    {
      if (ip - disps[rank] >= 0 &&
	  ip - disps[rank] < snfp) /* already not found in this rank, skip */
	ri = -1;
      else
	ri = interp_local (Pa, allsnfpts + ip * Pa->nsd, vloc,
			   stride, start, ncomps, 2,
			   allsnfres + ip * ncomps);

      if (ri == -1) /* Not found */
	{
	  allsnfsw[ip] = 0;
	  for (i = 0; i < ncomps; i++)
	    allsnfres[ip * ncomps + i] = 0.0;
	}
      else
	{
	  allsnfsw[ip] = 1;
	}
    }

  /* Free unneeded memory */
  free(allsnfpts);
  time3 = MPI_Wtime();
  PetscPrintf
    (Pa->Comm, "interp_global (Second search): %g Secs.\n", time3-time2);

  /* Reduce results: */
  for (i = 0; i < nprocs-1; i++)
    disps[i+1] = disps[i] + vsnfp[i];
  for (i = 0; i < nprocs; i++)              /* Proccessor "i" queries */
      MPI_Reduce (&allsnfsw[disps[i]], snfsw, vsnfp[i], MPI_INT, MPI_SUM,
		  i, Pa->Comm);
  for (i = 0; i < nprocs; i++)
    {
      vcounts[i] = vsnfp[i] * ncomps;
      if (i > 0)
	disps[i] = disps[i-1] + vcounts[i-1];
    }
  for (i = 0; i < nprocs; i++)              /* Proccessor "i" queries */
      MPI_Reduce (&allsnfres[disps[i]], snfres, vcounts[i], MPI_DOUBLE,
		  MPI_SUM, i, Pa->Comm);

  /* Free unneeded memory */
  free(allsnfsw);
  free(allsnfres);
  free(disps);
  free(vcounts);
  free(vsnfp);

  /* Compute average if found in multiple elements */
  for (ip = 0; ip < snfp; ip++)
    if (snfsw[ip] > 1)
      {
	for (i = 0; i < ncomps; i++)
	  snfres[ip*ncomps+i] /= snfsw[ip];
	snfsw[ip] = 1;
      }

  /* Distribute points found in another proccesses in local result vector */
  for (ip = 0, snfp = 0; ip < npoints; ip++)
    if (sw[ip] == -1)
      {
	if (snfsw[snfp] == 1)  /* found in another proccess, copy result */
	  {
	    for (i = 0; i < ncomps; i++)
	      results[ip * ncomps + i] = snfres[snfp * ncomps + i];
	    sw[ip] = 0;
	  }
	snfp++;
      }

  /* Free unneeded memory */
  free(snfsw);
  free(snfres);

  /* Re-count still-not-found points */
  for (ip = 0, snfp = 0; ip < npoints; ip++)
    if (sw[ip] == -1)
      snfp++;

  i = MPI_Allreduce (&snfp, &totsnf, 1, MPI_INT, MPI_SUM, Pa->Comm);

  fexternal = npoints - flocal - snfp;
  time4 = MPI_Wtime();
  PetscPrintf
    (Pa->Comm, "interp_global (2nd Communication stage): %g Secs.\n",
     time4-time3);
  PetscSynchronizedPrintf
    (Pa->Comm, "interp_global: looked for %d points, %d found locally, "
     "%d external, %d not found\n", npoints, flocal, fexternal, snfp);
  PetscSynchronizedFlush (Pa->Comm, stdout);

  return totsnf;
}

/*
 * interp_global_disp:
 * Interpolate a field in a set of points, (all processes ask
 *  for a (different) set of points), mesh is displaced
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * delta: displacement of local nodes
 * points(input): vector of points (x1, y1, ... x2, y2, .......)
 * npoints(input): number of points
 * vloc(input): Vector of values of the fields on the local nodes
 * stride(input): number of values in each node
 * start(input): position of the field to be interpolated
 * ncomps(input): components of the field to be interpolated
 * results(output): vector to hold the results (npoints*ncomps)
 *
 * return value: number of points not found in mesh
 */
/* Fortran interface
 *  Extra arguments:
 *    fSy: (system data): used for accessing the local components of the
 *        "external" vector. Also for the "stride" argument
 *    fv: Petsc vector with unknowns
 */
void interpglobaldisp_ (systemdata **fSy, Vec *delt,
			double *points, int *npoints,
			Vec *fv, int *start, int *ncomps, double *result,
			int *sw, int *retv)
{
  double *Vloc, *delta;
  Vec ldelt, lv;
  VecGhostGetLocalForm(*delt, &ldelt);
  VecGetArray(ldelt, &delta);
  VecGhostGetLocalForm(*fv, &lv);
  VecGetArray(lv, &Vloc);

  *retv = interp_global_disp ((*fSy)->Pa, delta,
			      points, *npoints, Vloc,
			      (*fSy)->nfields, *start, *ncomps,
			      result, sw);

  VecRestoreArray(lv, &Vloc);
  VecGhostRestoreLocalForm(*fv, &lv);
  VecRestoreArray(ldelt, &delta);
  VecGhostRestoreLocalForm(*delt, &ldelt);
}
/* Implementation */
int interp_global_disp (partition *Pa, double *delta,
			double *points, int npoints,
			double *vloc,
			int stride, int start, int ncomps, double *results,
			int *sw)
{
  int i, ri, ip, isd;
  int nprocs;
  int snfp, totsnf, *vsnfp, *vcounts, *disps;
  int *snfsw, *allsnfsw;
  double *snfpts, *snfres;
  double *allsnfpts, *allsnfres;
  int rank;
  /* Profiling variables */
  int flocal, fexternal;
  double time0, time1, time2, time3, time4;

  time0 = MPI_Wtime();
  /* First step: interp_local */
  snfp = 0;
  for (ip = 0; ip < npoints; ip++)
    {
      sw[ip] = interp_local_disp (Pa, delta,
				  points + ip * Pa->nsd, vloc,
				  stride, start, ncomps, 2,
				  results + ip * ncomps);
      if (sw[ip] == -1)
	snfp++;
    }

  MPI_Comm_size (Pa->Comm, &nprocs);
  if (nprocs == 1)
    {
      time1 = MPI_Wtime();
      PetscPrintf (Pa->Comm, "interp_global: found %d / %d points, %g Secs.\n",
		   npoints - snfp, npoints, time1-time0);
      return snfp;
    }
  flocal = npoints - snfp;
  time1 = MPI_Wtime();

  /* Second step: compact vector of still-not-found points */
  if (snfp > 0)
    {
      snfpts = (double *) malloc (snfp * Pa->nsd * sizeof(double));
      snfsw  = (int *) malloc (snfp * sizeof(int));
      snfres = (double *) malloc (snfp * ncomps * sizeof(double));
      if (!snfpts || !snfsw || !snfres)
	return snfp;

      for (ip = 0, snfp = 0; ip < npoints; ip++)
	if (sw[ip] == -1)
	  {
	    snfsw[snfp] = 0;
	    for (isd = 0; isd < Pa->nsd; isd++)
	      snfpts[Pa->nsd * snfp + isd] = points[ip * Pa->nsd + isd];
	    snfp++;
	  }
    }
  else
    {
      snfsw = NULL;
      snfpts = snfres = NULL;
    }

  /* Gather all still-not-found points: compute total number: */
  i = MPI_Allreduce (&snfp, &totsnf, 1, MPI_INT, MPI_SUM, Pa->Comm);

  if (totsnf == 0)
    {
      PetscSynchronizedPrintf
	(Pa->Comm, "interp_global: found locally all %d points, %g Secs.\n",
	 npoints, time1-time0);
      PetscSynchronizedFlush(Pa->Comm, stdout);
      return 0;
    }
  PetscPrintf
    (Pa->Comm, "interp_global (first search): %g Secs.\n", time1-time0);

  /* Alloc memory: */
  allsnfpts = (double *) malloc (totsnf * Pa->nsd * sizeof(double));
  allsnfsw  = (int *) malloc (totsnf * sizeof(int));
  allsnfres = (double *) malloc (totsnf * ncomps * sizeof(double));
  vsnfp = (int *) malloc (nprocs * sizeof(int));
  vcounts = (int *) malloc (nprocs * sizeof(int));
  disps = (int *) malloc (nprocs * sizeof(int));
  if (!allsnfpts || !allsnfsw || !allsnfres || !vsnfp || !disps)
    return -1;
  /* Gather all: */
  i = MPI_Allgather (&snfp, 1, MPI_INT, vsnfp, 1, MPI_INT, Pa->Comm);

  for (i = 0; i < nprocs; i++)
    {
      vcounts[i] = vsnfp[i] * Pa->nsd;
      if (i == 0)
	disps[i] = 0;
      else
	disps[i] = disps[i-1] + vcounts[i-1];
    }
    
  i = MPI_Allgatherv (snfpts, snfp*Pa->nsd, MPI_DOUBLE,
		      allsnfpts, vcounts, disps, MPI_DOUBLE, Pa->Comm);

  /* Free unneeded memory */
  if (snfpts)
    free(snfpts);

  time2 = MPI_Wtime();
  PetscPrintf
    (Pa->Comm, "interp_global (1st Communication stage): %g Secs.\n",
     time2-time1);

  /* All proccesses search all points:
   * (Optimization: skip local points)
   */
  MPI_Comm_rank (Pa->Comm, &rank);
  /* Re-build disps vector (to identify local points) */
  for (i = 0; i < nprocs; i++)
    {
      if (i == 0)
	disps[i] = 0;
      else
	disps[i] = disps[i-1] + vsnfp[i-1];
    }

  for (ip = 0; ip < totsnf; ip++)
    {
      if (ip - disps[rank] >= 0 &&
	  ip - disps[rank] < snfp) /* already not found in this rank, skip */
	ri = -1;
      else
	ri = interp_local_disp (Pa, delta,
				allsnfpts + ip * Pa->nsd, vloc,
				stride, start, ncomps, 2,
				allsnfres + ip * ncomps);

      if (ri == -1) /* Not found */
	{
	  allsnfsw[ip] = 0;
	  for (i = 0; i < ncomps; i++)
	    allsnfres[ip * ncomps + i] = 0.0;
	}
      else
	{
	  allsnfsw[ip] = 1;
	}
    }

  /* Free unneeded memory */
  free(allsnfpts);
  time3 = MPI_Wtime();
  PetscPrintf
    (Pa->Comm, "interp_global (Second search): %g Secs.\n", time3-time2);

  /* Reduce results: */
  for (i = 0; i < nprocs-1; i++)
    disps[i+1] = disps[i] + vsnfp[i];
  for (i = 0; i < nprocs; i++)              /* Proccessor "i" queries */
      MPI_Reduce (&allsnfsw[disps[i]], snfsw, vsnfp[i], MPI_INT, MPI_SUM,
		  i, Pa->Comm);
  for (i = 0; i < nprocs; i++)
    {
      vcounts[i] = vsnfp[i] * ncomps;
      if (i > 0)
	disps[i] = disps[i-1] + vcounts[i-1];
    }
  for (i = 0; i < nprocs; i++)              /* Proccessor "i" queries */
      MPI_Reduce (&allsnfres[disps[i]], snfres, vcounts[i], MPI_DOUBLE,
		  MPI_SUM, i, Pa->Comm);

  /* Free unneeded memory */
  free(allsnfsw);
  free(allsnfres);
  free(disps);
  free(vcounts);
  free(vsnfp);

  /* Compute average if found in multiple elements */
  for (ip = 0; ip < snfp; ip++)
    if (snfsw[ip] > 1)
      {
	for (i = 0; i < ncomps; i++)
	  snfres[ip*ncomps+i] /= snfsw[ip];
	snfsw[ip] = 1;
      }

  /* Distribute points found in another proccesses in local result vector */
  for (ip = 0, snfp = 0; ip < npoints; ip++)
    if (sw[ip] == -1)
      {
	if (snfsw[snfp] == 1)  /* found in another proccess, copy result */
	  {
	    for (i = 0; i < ncomps; i++)
	      results[ip * ncomps + i] = snfres[snfp * ncomps + i];
	    sw[ip] = 0;
	  }
	snfp++;
      }

  /* Free unneeded memory */
  free(snfsw);
  free(snfres);

  /* Re-count still-not-found points */
  for (ip = 0, snfp = 0; ip < npoints; ip++)
    if (sw[ip] == -1)
      snfp++;

  i = MPI_Allreduce (&snfp, &totsnf, 1, MPI_INT, MPI_SUM, Pa->Comm);

  fexternal = npoints - flocal - snfp;
  time4 = MPI_Wtime();
  PetscPrintf
    (Pa->Comm, "interp_global (2nd Communication stage): %g Secs.\n",
     time4-time3);
  PetscSynchronizedPrintf
    (Pa->Comm, "interp_global: looked for %d points, %d found locally, "
     "%d external, %d not found\n", npoints, flocal, fexternal, snfp);
  PetscSynchronizedFlush (Pa->Comm, stdout);

  return totsnf;
}

/* Implementation */
int interp_global_disp_ffields (partition *Pa, double *vdelta,
				int sclpn, int idisp,
				double *points, int npoints,
				double *vloc,
				int stride, int start, int ncomps,
				double *results,
				int *sw)
{
  int i, ri, ip, isd;
  int nprocs;
  int snfp, totsnf, *vsnfp, *vcounts, *disps;
  int *snfsw, *allsnfsw;
  double *snfpts, *snfres;
  double *allsnfpts, *allsnfres;
  int rank;
  /* Profiling variables */
  int flocal, fexternal;
  double time0, time1, time2, time3, time4;

  time0 = MPI_Wtime();
  /* First step: interp_local */
  snfp = 0;
  for (ip = 0; ip < npoints; ip++)
    {
      sw[ip] = interp_local_disp_ffields
	(Pa, vdelta, sclpn, idisp,
	 points + ip * Pa->nsd, vloc,
	 stride, start, ncomps, 2, results + ip * ncomps);
      if (sw[ip] == -1)
	snfp++;
    }

  MPI_Comm_size (Pa->Comm, &nprocs);
  if (nprocs == 1)
    {
      time1 = MPI_Wtime();
      PetscPrintf
	(Pa->Comm,
	 "interp_global_disp_ffields: found %d / %d points, %g Secs.\n",
	 npoints - snfp, npoints, time1-time0);
      return snfp;
    }
  flocal = npoints - snfp;
  time1 = MPI_Wtime();

  /* Second step: compact vector of still-not-found points */
  if (snfp > 0)
    {
      snfpts = (double *) malloc (snfp * Pa->nsd * sizeof(double));
      snfsw  = (int *) malloc (snfp * sizeof(int));
      snfres = (double *) malloc (snfp * ncomps * sizeof(double));
      if (!snfpts || !snfsw || !snfres)
	return snfp;

      for (ip = 0, snfp = 0; ip < npoints; ip++)
	if (sw[ip] == -1)
	  {
	    snfsw[snfp] = 0;
	    for (isd = 0; isd < Pa->nsd; isd++)
	      snfpts[Pa->nsd * snfp + isd] = points[ip * Pa->nsd + isd];
	    snfp++;
	  }
    }
  else
    {
      snfsw = NULL;
      snfpts = snfres = NULL;
    }

  /* Gather all still-not-found points: compute total number: */
  i = MPI_Allreduce (&snfp, &totsnf, 1, MPI_INT, MPI_SUM, Pa->Comm);

  if (totsnf == 0)
    {
      PetscSynchronizedPrintf
	(Pa->Comm,
	 "interp_global_disp_ffields: found locally all %d points, %g Secs.\n",
	 npoints, time1-time0);
      PetscSynchronizedFlush(Pa->Comm, stdout);
      return 0;
    }
  PetscPrintf
    (Pa->Comm,
     "interp_global_disp_ffields (first search): %g Secs.\n", time1-time0);

  /* Alloc memory: */
  allsnfpts = (double *) malloc (totsnf * Pa->nsd * sizeof(double));
  allsnfsw  = (int *) malloc (totsnf * sizeof(int));
  allsnfres = (double *) malloc (totsnf * ncomps * sizeof(double));
  vsnfp = (int *) malloc (nprocs * sizeof(int));
  vcounts = (int *) malloc (nprocs * sizeof(int));
  disps = (int *) malloc (nprocs * sizeof(int));
  if (!allsnfpts || !allsnfsw || !allsnfres || !vsnfp || !disps)
    return -1;
  /* Gather all: */
  i = MPI_Allgather (&snfp, 1, MPI_INT, vsnfp, 1, MPI_INT, Pa->Comm);

  for (i = 0; i < nprocs; i++)
    {
      vcounts[i] = vsnfp[i] * Pa->nsd;
      if (i == 0)
	disps[i] = 0;
      else
	disps[i] = disps[i-1] + vcounts[i-1];
    }
    
  i = MPI_Allgatherv (snfpts, snfp*Pa->nsd, MPI_DOUBLE,
		      allsnfpts, vcounts, disps, MPI_DOUBLE, Pa->Comm);

  /* Free unneeded memory */
  if (snfpts)
    free(snfpts);

  time2 = MPI_Wtime();
  PetscPrintf
    (Pa->Comm,
     "interp_global_disp_ffields (1st Communication stage): %g Secs.\n",
     time2-time1);

  /* All proccesses search all points:
   * (Optimization: skip local points)
   */
  MPI_Comm_rank (Pa->Comm, &rank);
  /* Re-build disps vector (to identify local points) */
  for (i = 0; i < nprocs; i++)
    {
      if (i == 0)
	disps[i] = 0;
      else
	disps[i] = disps[i-1] + vsnfp[i-1];
    }

  for (ip = 0; ip < totsnf; ip++)
    {
      if (ip - disps[rank] >= 0 &&
	  ip - disps[rank] < snfp) /* already not found in this rank, skip */
	ri = -1;
      else
	ri = interp_local_disp_ffields (Pa, vdelta, sclpn, idisp,
				allsnfpts + ip * Pa->nsd, vloc,
				stride, start, ncomps, 2,
				allsnfres + ip * ncomps);

      if (ri == -1) /* Not found */
	{
	  allsnfsw[ip] = 0;
	  for (i = 0; i < ncomps; i++)
	    allsnfres[ip * ncomps + i] = 0.0;
	}
      else
	{
	  allsnfsw[ip] = 1;
	}
    }

  /* Free unneeded memory */
  free(allsnfpts);
  time3 = MPI_Wtime();
  PetscPrintf
    (Pa->Comm,
     "interp_global_disp_ffields (Second search): %g Secs.\n", time3-time2);

  /* Reduce results: */
  for (i = 0; i < nprocs-1; i++)
    disps[i+1] = disps[i] + vsnfp[i];
  for (i = 0; i < nprocs; i++)              /* Proccessor "i" queries */
      MPI_Reduce (&allsnfsw[disps[i]], snfsw, vsnfp[i], MPI_INT, MPI_SUM,
		  i, Pa->Comm);
  for (i = 0; i < nprocs; i++)
    {
      vcounts[i] = vsnfp[i] * ncomps;
      if (i > 0)
	disps[i] = disps[i-1] + vcounts[i-1];
    }
  for (i = 0; i < nprocs; i++)              /* Proccessor "i" queries */
      MPI_Reduce (&allsnfres[disps[i]], snfres, vcounts[i], MPI_DOUBLE,
		  MPI_SUM, i, Pa->Comm);

  /* Free unneeded memory */
  free(allsnfsw);
  free(allsnfres);
  free(disps);
  free(vcounts);
  free(vsnfp);

  /* Compute average if found in multiple elements */
  for (ip = 0; ip < snfp; ip++)
    if (snfsw[ip] > 1)
      {
	for (i = 0; i < ncomps; i++)
	  snfres[ip*ncomps+i] /= snfsw[ip];
	snfsw[ip] = 1;
      }

  /* Distribute points found in another proccesses in local result vector */
  for (ip = 0, snfp = 0; ip < npoints; ip++)
    if (sw[ip] == -1)
      {
	if (snfsw[snfp] == 1)  /* found in another proccess, copy result */
	  {
	    for (i = 0; i < ncomps; i++)
	      results[ip * ncomps + i] = snfres[snfp * ncomps + i];
	    sw[ip] = 0;
	  }
	snfp++;
      }

  /* Free unneeded memory */
  free(snfsw);
  free(snfres);

  /* Re-count still-not-found points */
  for (ip = 0, snfp = 0; ip < npoints; ip++)
    if (sw[ip] == -1)
      snfp++;

  i = MPI_Allreduce (&snfp, &totsnf, 1, MPI_INT, MPI_SUM, Pa->Comm);

  fexternal = npoints - flocal - snfp;
  time4 = MPI_Wtime();
  PetscPrintf
    (Pa->Comm,
     "interp_global_disp_ffields (2nd Communication stage): %g Secs.\n",
     time4-time3);
  PetscSynchronizedPrintf
    (Pa->Comm,
     "interp_global_disp_ffields: looked for %d points, %d found locally, "
     "%d external, %d not found\n", npoints, flocal, fexternal, snfp);
  PetscSynchronizedFlush (Pa->Comm, stdout);

  return totsnf;
}

/*
 * point_in_element:
 * Given an element by its "nnpe" coordinates "coords", in "nsd" spatial
 * dimensions, find if the point "point" belongs to it.
 * If result is true, set the "weights" vector to the interpolation
 * weights of each node.
 * Arguments:
 * nsd(input): number of spatial dimensions
 * nnpe(input): number of points in the element (simplex: nsd+1)
 * coords(input): nodal coordinates (1...nnpe)(1...nsd)[inode * nsd + isd]
 * point(input): point coordinates (1...nsd)
 * weights(output): interpolation weights of each node (1...nnpe)
 *
 * return value: 1: point in element
 *               0: no
 */
/* Fortran Interface */
void pointinelement_ (int *nsd, int *nnpe, double *coords, double *point,
		      double *weights, int *retv)
{
  *retv = point_in_element(*nsd, *nnpe, coords, point, weights);
}
/* Implementation */
int point_in_element (int nsd, int nnpe, double *coords, double *point,
		      double *weights)
{
  if (nsd == 1)
    {
      if (nnpe == 2)     /* 1D -> Segment */
	{
	  double x1, x2;
	  x1 = -(coords[0] - point[0]);
	  x2 = coords[1] - point[0];
	  if (x1 < 0 || x2 < 0)
	    return 0;
	  weights[0] = x2 / (x1 + x2);
	  weights[1] = x1 / (x1 + x2);
	  return 1;
	}
      return 0;
    }
  if (nsd == 2)
    {
      if (nnpe == 3)     /* 2D -> Triangle */
	{
	  double x1,x2,x3, y1,y2,y3;
	  double ct1, ct2, ct3;
	  x1 = coords[0]-point[0];
	  y1 = coords[1]-point[1];
	  x2 = coords[2]-point[0];
	  y2 = coords[3]-point[1];
	  x3 = coords[4]-point[0];
	  y3 = coords[5]-point[1];
	  ct1 = x2 * y3 - x3 * y2;
	  ct2 = x3 * y1 - x1 * y3;
	  ct3 = x1 * y2 - y1 * x2;
	  weights[0] = ct1 / (ct1 + ct2 + ct3);
	  if (weights[0] < -0.01) return 0;
	  weights[1] = ct2 / (ct1 + ct2 + ct3);
	  if (weights[1] < -0.01) return 0;
	  weights[2] = ct3 / (ct1 + ct2 + ct3);
	  if (weights[2] < -0.01) return 0;
	  return 1;
	}
      if (nnpe == 4)     /* 2D -> Quadrilateral */
	{
	}
      return 0;
    }
  if (nsd == 3)
    {
      if (nnpe == 4)     /* 3D -> Tetrahedra */
	{
	  double x1,x2,x3,x4, y1,y2,y3,y4, z1,z2,z3,z4;
	  double ct1, ct2, ct3, ct4;
	  x1 = coords[0]-point[0];
	  y1 = coords[1]-point[1];
	  z1 = coords[2]-point[2];
	  x2 = coords[3]-point[0];
	  y2 = coords[4]-point[1];
	  z2 = coords[5]-point[2];
	  x3 = coords[6]-point[0];
	  y3 = coords[7]-point[1];
	  z3 = coords[8]-point[2];
	  x4 = coords[9]-point[0];
	  y4 = coords[10]-point[1];
	  z4 = coords[11]-point[2];
	  ct4 = x1*(y2*z3-z2*y3) + y1*(z2*x3-x2*z3) + z1*(x2*y3-y2*x3);
	  ct4 = -ct4;
	  if (ct4 < 0) return 0;
	  ct3 = x1*(y2*z4-z2*y4) + y1*(z2*x4-x2*z4) + z1*(x2*y4-y2*x4);
	  if (ct3 < 0) return 0;
	  ct2 = x1*(y3*z4-z3*y4) + y1*(z3*x4-x3*z4) + z1*(x3*y4-y3*x4);
	  ct2 = -ct2;
	  if (ct2 < 0) return 0;
	  ct1 = x2*(y3*z4-z3*y4) + y2*(z3*x4-x3*z4) + z2*(x3*y4-y3*x4);
	  if (ct1 < 0) return 0;
	  weights[0] = ct1/(ct1+ct2+ct3+ct4);
	  weights[1] = ct2/(ct1+ct2+ct3+ct4);
	  weights[2] = ct3/(ct1+ct2+ct3+ct4);
	  weights[3] = ct4/(ct1+ct2+ct3+ct4);
	  return 1;
	}
      return 0;
    }
  return 0;
}

/*
#include <math.h>
#include "ggtree.h"
#include "data_structures.h"
*/
#include <malloc.h>
#include "interp.h"

/*
 * stats_interpol:
 * Get statistics about the data structures for an efficient geometric search
 * Pa(input,output): Data structure
 * levels: # of levels of the geometric tree
 * leaves: # of leaves of the geometric tree
 * ndata: Total number of data (elements) stored at the leaves
 * dpl: Average number of data elements in the tree leaves
 */

/*
 * plot_interpol:
 * Get statistics about the data structures for an efficient geometric search
 * Pa(input,output): Data structure
 * levels: # of levels of the geometric tree
 * leaves: # of leaves of the geometric tree
 * ndata: Total number of data (elements) stored at the leaves
 * dpl: Average number of data elements in the tree leaves
 */
/* Fortran interface */
void plotinterpol_ (partition **fPa, int *mode, int *retv)
{
  *retv = plot_interpol (*fPa, *mode);
}
int plot_interpol (partition *Pa, int mode)
{
  int retval, rank, ierr;
  FILE *fp;
  char file_name[150], name[170];
  PetscBool pt_flg;

  ierr = PetscOptionsGetString
    ("pgpfep_","-prtgt", file_name, 149,
     &pt_flg);                    CHKERRABORT(PETSC_COMM_WORLD,ierr);
  if (pt_flg  == PETSC_TRUE)
    {
      MPI_Comm_rank (Pa->Comm, &rank);
      if (mode % 2)
	sprintf (name, "%s-solid-%d.dat", file_name, rank);
      else
	sprintf (name, "%s-fluid-%d.dat", file_name, rank);
      fp = fopen(name,"w");
      if (!fp)
	return -1;

      retval = gg_tree_plot (Pa->gt, mode, fp);
      fclose(fp);
    }
  else
    retval = 0;
  return retval;
}

/* Fortran interface */
void statsinterpol_ (partition **fPa, int *levels, int *leaves, int *ndata,
		     double *dpl, int *retv)
{
  *retv = stats_interpol (*fPa, levels, leaves, ndata, dpl);
}

int stats_interpol (partition *Pa, int *levels, int *leaves, int *ndata,
		    double *dpl)
{
  int retval, ierr, mlevels, tleaves, tndata;
  retval = gg_tree_stats (Pa->gt, levels, leaves, ndata, dpl);
  ierr  = MPI_Allreduce (levels, &mlevels, 1, MPI_INT, MPI_MAX, Pa->Comm);
  ierr += MPI_Allreduce (leaves, &tleaves, 1, MPI_INT, MPI_SUM, Pa->Comm);
  ierr += MPI_Allreduce (ndata,  &tndata,  1, MPI_INT, MPI_SUM, Pa->Comm);
  *dpl = ((double)tndata) / tleaves;
  *levels = mlevels;
  *leaves = tleaves;
  *ndata = tndata;

  return retval + ierr;
}

/*
 * init_interpol:
 * Creates data structures for an efficient geometric search of elements
 * of the mesh (include local elements, as well as intra-proccesses elements
 * arguments:
 * Pa(input,output): Data structure with distributed mesh
 * return value: 0: everthing OK, < 0: error
 */

/* Fortran interface */
void initinterpol_ (partition **fPa, int *retv)
{
  *retv = init_interpol (*fPa);
}
/* Implementation */
int init_interpol (partition *Pa)
{
  int iel, in, isd;
  int ije, no, idata;
  double xx, xmi, xma;
  double bbox[6];

  for (isd = 0; isd < Pa->nsd; isd++)
    bbox[2*isd] = bbox[2*isd+1] = Pa->cogeoloc[isd];

  /* Compute bounding box:
   *  local nodes:
   */
  for (in = 1; in < Pa->nodloc; in++)
    {
      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  if (Pa->cogeoloc[in*Pa->nsd+isd] < bbox[2*isd])
	    bbox[2*isd] = Pa->cogeoloc[in*Pa->nsd+isd];
	  else if (Pa->cogeoloc[in*Pa->nsd+isd] > bbox[2*isd+1])
	    bbox[2*isd+1] = Pa->cogeoloc[in*Pa->nsd+isd];
	}
    }
  /* "external" (non-local, neighbouring nodes) */
  for (in = 0; in < Pa->nodext; in++)
    {
      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  if (Pa->cogeoext[in*Pa->nsd+isd] < bbox[2*isd])
	    bbox[2*isd] = Pa->cogeoext[in*Pa->nsd+isd];
	  else if (Pa->cogeoext[in*Pa->nsd+isd] > bbox[2*isd+1])
	    bbox[2*isd+1] = Pa->cogeoext[in*Pa->nsd+isd];
	}
    }

  /* Create tree */
  Pa->gt = (gg_tree *) malloc (sizeof (gg_tree));
  if (!Pa->gt)
    return -1;
  gg_tree_init (Pa->gt, Pa->nsd, sizeof(int), bbox);

  /* Adjust size using element size (diameter 1st dimension)
   *  - only x-size
   *  - at last node of the element
   *  - only use local elements
   */
  for (iel = 0; iel < Pa->nelemloc; iel++)
    {
      ije = Pa->nnpe * iel;
      no = Pa->LocMesh[ije] - Pa->first;
      xmi = xma = Pa->cogeoloc[no*Pa->nsd];
      for (in = 1; in < Pa->nnpe; in++)
	{
	  no = Pa->LocMesh[++ije] - Pa->first;
	  xx = Pa->cogeoloc[no*Pa->nsd];
	  if (xx > xma) xma = xx;
	  else if (xx < xmi) xmi = xx;
	}
      gg_tree_adjustsize (Pa->gt, Pa->cogeoloc+no*Pa->nsd, xma-xmi);
    }

  /* Insert data: element numbers (for local elements) */
  for (iel = 0; iel < Pa->nelemloc; iel++)
    {
      /* Compute bounding box: */
      ije = Pa->nnpe * iel;  /* start of element in conectivity */
      no = Pa->LocMesh[ije] - Pa->first; /* first node of this element */

      for (isd = 0; isd < Pa->nsd; isd++)
	bbox[2*isd] = bbox[2*isd+1] = Pa->cogeoloc[no*Pa->nsd+isd];

      for (in = 1; in < Pa->nnpe; in++) /* rest of nodes */
	{
	  no = Pa->LocMesh[++ije] - Pa->first;

	  for (isd = 0; isd < Pa->nsd; isd++)
	    {
	      if (Pa->cogeoloc[no*Pa->nsd+isd] < bbox[2*isd])
		bbox[2*isd] = Pa->cogeoloc[no*Pa->nsd+isd];
	      else if (Pa->cogeoloc[no*Pa->nsd+isd] > bbox[2*isd+1])
		bbox[2*isd+1] = Pa->cogeoloc[no*Pa->nsd+isd];
	    }
	}
      gg_tree_insertdata (Pa->gt, (void *)(&iel), bbox);
    }

  /* Insert data: -(element_number+1) for "external"
   *                                      (inter-proccessor) elements
   */
  for (iel = 0; iel < Pa->nelemext; iel++)
    {
      /* Compute bounding box: */
      ije = Pa->nnpe * iel;  /* start of element in conectivity */
      no = Pa->ExtMesh[ije] - 1; /* first node of this element */

      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  if (no < 0)
	    xx = Pa->cogeoext[-(no+1)*Pa->nsd+isd];
	  else
	    xx = Pa->cogeoloc[(no+1-Pa->first)*Pa->nsd+isd];
	  bbox[2*isd] = bbox[2*isd+1] = xx;
	}

      for (in = 1; in < Pa->nnpe; in++) /* rest of nodes */
	{
	  no = Pa->ExtMesh[++ije] - 1;

	  for (isd = 0; isd < Pa->nsd; isd++)
	    {
	      if (no < 0)
		xx = Pa->cogeoext[-(no+1)*Pa->nsd+isd];
	      else
		xx = Pa->cogeoloc[(no+1-Pa->first)*Pa->nsd+isd];

	      if (xx < bbox[2*isd])
		bbox[2*isd] = xx;
	      else if (xx > bbox[2*isd+1])
		bbox[2*isd+1] = xx;
	    }
	}
      idata = -(iel+1);
      gg_tree_insertdata (Pa->gt, (void *)(&idata), bbox);
    }
  return 0;
}

/*
 * in_interp:
 * Creates data structures for an efficient geometric search of elements
 * of the mesh (include local elements, as well as intra-proccesses elements
 * arguments:
 * Pa(input,output): Data structure with distributed mesh
 * tree_size(input): adjust refinement of the auxiliary tree,
 *     1.0: same as init_interpol
 *     > 1.0: less refinement, less memory, more time
 *     < 1.0: more refinement, more memory, less time
 *     recommended: ~ 4.0
 * periodic(input): not zero if the mesh is periodic
 * periodicity(input)(1...nsd): if > 0 periodicity in each spatial dimension
 *
 * return value: 0: everthing OK, < 0: error
 */

/* Fortran interface */
void ininterp_ (partition **fPa, double *tree_size,
		int *swperiodic, double *per_sizes, int *retv)
{
  *retv = in_interp (*fPa, *tree_size, *swperiodic,
		     per_sizes);
}
/* Implementation */
int in_interp (partition *Pa, double tree_size,
	       int periodic, double *periodicity)
{
  int iel, in, isd;
  int ije, no, idata;
  int perswitch;
  double xx, xmi, xma;
  double bbox[6];

  perswitch = 0;
  if (periodic)
    {
      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  Pa->per[isd] = periodicity[isd];
	  if (periodicity[isd] > 0.0)
	    perswitch += 1 << isd;
	}
    }
  Pa->MeshKind |= perswitch << 1;

  for (isd = 0; isd < Pa->nsd; isd++)
    bbox[2*isd] = bbox[2*isd+1] = Pa->cogeoloc[isd];

  /* Compute bounding box:
   *  local nodes:
   */
  for (in = 1; in < Pa->nodloc; in++)
    {
      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  if (Pa->cogeoloc[in*Pa->nsd+isd] < bbox[2*isd])
	    bbox[2*isd] = Pa->cogeoloc[in*Pa->nsd+isd];
	  else if (Pa->cogeoloc[in*Pa->nsd+isd] > bbox[2*isd+1])
	    bbox[2*isd+1] = Pa->cogeoloc[in*Pa->nsd+isd];
	}
    }

  /* "external" (non-local, neighbouring nodes) */
  for (in = 0; in < Pa->nodext; in++)
    {
      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  if (Pa->cogeoext[in*Pa->nsd+isd] < bbox[2*isd])
	    bbox[2*isd] = Pa->cogeoext[in*Pa->nsd+isd];
	  else if (Pa->cogeoext[in*Pa->nsd+isd] > bbox[2*isd+1])
	    bbox[2*isd+1] = Pa->cogeoext[in*Pa->nsd+isd];
	}
    }

  /*
   * If mesh is periodic periodicity is the max value:
   */
  if (periodic)
    {
      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  if (periodicity[isd] > 0.0 &&
	      periodicity[isd] > bbox[2*isd+1])
	    bbox[2*isd+1] = periodicity[isd];
	}
    }

  /* Create tree */
  Pa->gt = (gg_tree *) malloc (sizeof (gg_tree));
  if (!Pa->gt)
    return -1;
  gg_tree_init (Pa->gt, Pa->nsd, sizeof(int), bbox);

  /* Adjust size using element size (diameter 1st dimension)
   *  - only x-size
   *  - at last node of the element
   *  - only use local elements
   */

  for (iel = 0; iel < Pa->nelemloc; iel++)
    {
      ije = Pa->nnpe * iel;
      no = Pa->LocMesh[ije] - Pa->first;
      xmi = xma = Pa->cogeoloc[no*Pa->nsd];
      for (in = 1; in < Pa->nnpe; in++)
	{
	  no = Pa->LocMesh[++ije] - Pa->first;
	  xx = Pa->cogeoloc[no*Pa->nsd];
	  if (xx > xma) xma = xx;
	  else if (xx < xmi) xmi = xx;
	}
      gg_tree_adjustsize (Pa->gt, Pa->cogeoloc+no*Pa->nsd,
			  tree_size * (xma-xmi));
    }

  /* Insert data: element numbers (for local elements) */
  for (iel = 0; iel < Pa->nelemloc; iel++)
    {
      /* Compute bounding box: */
      ije = Pa->nnpe * iel;  /* start of element in conectivity */
      no = Pa->LocMesh[ije] - Pa->first; /* first node of this element */

      for (isd = 0; isd < Pa->nsd; isd++)
	bbox[2*isd] = bbox[2*isd+1] = Pa->cogeoloc[no*Pa->nsd+isd];

      for (in = 1; in < Pa->nnpe; in++) /* rest of nodes */
	{
	  no = Pa->LocMesh[++ije] - Pa->first;

	  for (isd = 0; isd < Pa->nsd; isd++)
	    {
	      if (Pa->cogeoloc[no*Pa->nsd+isd] < bbox[2*isd])
		bbox[2*isd] = Pa->cogeoloc[no*Pa->nsd+isd];
	      else if (Pa->cogeoloc[no*Pa->nsd+isd] > bbox[2*isd+1])
		bbox[2*isd+1] = Pa->cogeoloc[no*Pa->nsd+isd];
	    }
	}
      /*
       * Check for periodicity
       */
      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  if (perswitch & (1 << isd))
	    if (bbox[2*isd+1] - bbox[2*isd] > periodicity[isd]/2.0)
	      { /* This element is too big, re-compute !!! */
		for (in = 0; in < Pa->nnpe; in++) /* sweep nodes */
		  {
		    no = Pa->LocMesh[Pa->nnpe * iel + in] - Pa->first;
		    xx = Pa->cogeoloc[no*Pa->nsd+isd];
		    if (xx < periodicity[isd]/2)
		      xx += periodicity[isd];
		    if (in == 0)
		      bbox[2*isd] = bbox[2*isd+1] = xx;
		    else if (xx < bbox[2*isd])
		      bbox[2*isd] = xx;
		    else if (xx > bbox[2*isd+1])
		      bbox[2*isd+1] = xx;
		  }
	      }
	}
      gg_tree_insertdata (Pa->gt, (void *)(&iel), bbox);
    }

  /* Insert data: -(element_number+1) for "external"
   *                                      (inter-proccessor) elements
   */
  for (iel = 0; iel < Pa->nelemext; iel++)
    {
      /* Compute bounding box: */
      ije = Pa->nnpe * iel;  /* start of element in conectivity */
      no = Pa->ExtMesh[ije] - 1; /* first node of this element */

      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  if (no < 0)
	    xx = Pa->cogeoext[-(no+1)*Pa->nsd+isd];
	  else
	    xx = Pa->cogeoloc[(no+1-Pa->first)*Pa->nsd+isd];
	  bbox[2*isd] = bbox[2*isd+1] = xx;
	}

      for (in = 1; in < Pa->nnpe; in++) /* rest of nodes */
	{
	  no = Pa->ExtMesh[++ije] - 1;

	  for (isd = 0; isd < Pa->nsd; isd++)
	    {
	      if (no < 0)
		xx = Pa->cogeoext[-(no+1)*Pa->nsd+isd];
	      else
		xx = Pa->cogeoloc[(no+1-Pa->first)*Pa->nsd+isd];

	      if (xx < bbox[2*isd])
		bbox[2*isd] = xx;
	      else if (xx > bbox[2*isd+1])
		bbox[2*isd+1] = xx;
	    }
	}
      /*
       * Check for periodicity
       */
      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  if (perswitch & 1 << isd)
	    if (bbox[2*isd+1] - bbox[2*isd] > periodicity[isd]/2)
	      { /* This element is too big, re-compute !!! */
		for (in = 0; in < Pa->nnpe; in++) /* sweep nodes */
		  {
		    no = Pa->ExtMesh[Pa->nnpe * iel + in] - 1;
		    if (no < 0)
		      xx = Pa->cogeoext[-(no+1)*Pa->nsd+isd];
		    else
		      xx = Pa->cogeoloc[(no+1-Pa->first)*Pa->nsd+isd];
		    if (xx < periodicity[isd]/2)
		      xx += periodicity[isd];
		    if (in == 0)
		      bbox[2*isd] = bbox[2*isd+1] = xx;
		    else if (xx < bbox[2*isd])
		      bbox[2*isd] = xx;
		    else if (xx > bbox[2*isd+1])
		      bbox[2*isd+1] = xx;
		  }
	      }
	}

      idata = -(iel+1);
      gg_tree_insertdata (Pa->gt, (void *)(&idata), bbox);
    }
  return 0;
}

/* Implementation for turbidfem */
#include "mesher.h"
// tfem_gt podría ir a la estructura params_t
gg_tree *tfem_gt;
int in_interp (struct params_t *params, double tree_size)
{
  int iel, in, isd;
  int ije, no, idata;
  int perswitch;
  double xx, xmi, xma;
  double bbox[6];

  // 1er nodo de la malla, debería ser nodo de la *malla local*
  for (isd = 0; isd < Pa->nsd; isd++)
    bbox[2*isd] = bbox[2*isd+1] = params.node[0].x[isd];

  /* Compute bounding box:
   *  local nodes:
   */
  // Loop sobre nodos, deberían ser nodos de la *malla local*
  for (in = 1; in < params.node_count; in++)
    {
      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  if (params.node[in].x[isd] < bbox[2*isd])
	    bbox[2*isd] = params.node[in].x[isd];
	  else if (params.node[in].x[isd] > bbox[2*isd+1])
	    bbox[2*isd+1] = params.node[in].x[isd];
	}
    }

  /* Create tree */
  tfem_gt = (gg_tree *) malloc (sizeof (gg_tree));
  if (!tfem_gt)
    return -1;
  gg_tree_init (tfem_gt, DIM, sizeof(int), bbox);

  /* Adjust size using element size (diameter 1st dimension)
   *  - only x-size
   *  - at last node of the element
   *  - only use local elements
   */

  for (iel = 0; iel < params.element_count; iel++)
    {
      // 1er nodo del elemento
      no = params.element[iel].node[0];
      xmi = xma = params.node[no].x[0];
      for (in = 1; in < params.element[e].node_count; in++)
	{
	  no = params.element[iel].node[in];
	  xx = params.node[no].x[0];
	  if (xx > xma) xma = xx;
	  else if (xx < xmi) xmi = xx;
	}
      gg_tree_adjustsize (tfem_gt, params.node[no].x,
			  tree_size * (xma-xmi));
    }

  /* Insert data: element numbers (for local elements) */
  for (iel = 0; iel < params.element_count; iel++)
    {
      /* Compute bounding box: */
      no = params.element[iel].node[0]; /* first node of this element */

      for (isd = 0; isd < DIM; isd++)
	bbox[2*isd] = bbox[2*isd+1] = params.node[no].x[isd];

      for (in = 1; in < params.element[e].node_count; in++) /* rest of nodes */
	{
	  no = params.element[iel].node[in];

	  for (isd = 0; isd < DIM; isd++)
	    {
	      if (params.node[no].x[isd] < bbox[2*isd])
		bbox[2*isd] = params.node[no].x[isd];
	      else if (params.node[no].x[isd] > bbox[2*isd+1])
		bbox[2*isd+1] = params.node[no].x[isd];
	    }
	}

      gg_tree_insertdata (tfem_gt, (void *)(&iel), bbox);
    }

  return 0;
}

/*
 * in_interpdisp:
 * Creates data structures for an efficient geometric search of elements
 * of the mesh (include local elements, as well as intra-proccesses elements
 * arguments:
 * Pa(input,output): Data structure with distributed mesh
 * tree_size(input): adjust refinement of the auxiliary tree,
 *     1.0: same as init_interpol
 *     > 1.0: less refinement, less memory, more time
 *     < 1.0: more refinement, more memory, less time
 *     recommended: ~ 4.0
 * disp(input): displacement of the nodes, distributed vector
 *   with same structure as Pa->cogeoloc: disp[inode*nsd+isd]
 * dispe(input): displacement of the "external" nodes, same structure as
 *   Pa->cogeoext: dispe[inode*nsd+isd]
 *
 * return value: 0: everthing OK, < 0: error
 *
 */
/* Fortran interface */
void ininterpdisp_ (partition **fPa, double *tree_size,
		    Vec *disp, Vec *dispe, int *retv)
{
  *retv = in_interpdisp(*fPa, *tree_size, *disp, *dispe);
}
/* Implementation */
int in_interpdisp (partition *Pa, double tree_size, Vec disp, Vec dispe)
{
  int iel, in, isd;
  int ije, no, idata;
  int nsd, ierr;

  double xx, xmi, xma;
  double bbox[6];
  double *dscan, *descan;

  nsd = Pa->nsd;

  ierr = VecGetArray(disp, &dscan);     CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGetArray(dispe, &descan);   CHKERRABORT(PETSC_COMM_WORLD, ierr);

  for (isd = 0; isd < Pa->nsd; isd++)
    bbox[2*isd] = bbox[2*isd+1] = Pa->cogeoloc[isd]+dscan[isd];

  /* Compute bounding box:
   *  local nodes:
   */
  for (in = 1; in < Pa->nodloc; in++)
    {
      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  double xx = Pa->cogeoloc[in*nsd+isd] + dscan[in*nsd+isd];
	  if (xx < bbox[2*isd])
	    bbox[2*isd] = xx;
	  else if (xx > bbox[2*isd+1])
	    bbox[2*isd+1] = xx;
	}
    }

  /* "external" (non-local, neighbouring nodes) */
  for (in = 0; in < Pa->nodext; in++)
    {
      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  double xx = Pa->cogeoext[in*nsd+isd] + descan[in*nsd+isd];
	  if (xx < bbox[2*isd])
	    bbox[2*isd] = xx;
	  else if (xx > bbox[2*isd+1])
	    bbox[2*isd+1] = xx;
	}
    }

  /* Create tree */
  Pa->gt = (gg_tree *) malloc (sizeof (gg_tree));
  if (!Pa->gt)
    return -1;
  gg_tree_init (Pa->gt, Pa->nsd, sizeof(int), bbox);

  /* Adjust size using element size (diameter 1st dimension)
   *  - only x-size
   *  - at last node of the element
   *  - only use local elements
   */

  for (iel = 0; iel < Pa->nelemloc; iel++)
    {
      double point[nsd];
      ije = Pa->nnpe * iel;
      no = Pa->LocMesh[ije] - Pa->first;
      xmi = xma = Pa->cogeoloc[no*nsd] + dscan[no*nsd];
      for (in = 1; in < Pa->nnpe; in++)
	{
	  no = Pa->LocMesh[++ije] - Pa->first;
	  xx = Pa->cogeoloc[no*nsd] + dscan[no*nsd];
	  if (xx > xma) xma = xx;
	  else if (xx < xmi) xmi = xx;
	}
      for (isd = 0; isd < nsd; isd++)
	point[isd] = Pa->cogeoloc[no*nsd+isd] + dscan[no*nsd+isd];
      gg_tree_adjustsize (Pa->gt, point,
			  tree_size * (xma-xmi));
    }

  /* Insert data: element numbers (for local elements) */
  for (iel = 0; iel < Pa->nelemloc; iel++)
    {
      /* Compute bounding box: */
      ije = Pa->nnpe * iel;  /* start of element in conectivity */
      no = Pa->LocMesh[ije] - Pa->first; /* first node of this element */

      for (isd = 0; isd < Pa->nsd; isd++)
	bbox[2*isd] = bbox[2*isd+1] =
	  Pa->cogeoloc[no*nsd+isd]+dscan[no*nsd+isd];

      for (in = 1; in < Pa->nnpe; in++) /* rest of nodes */
	{
	  no = Pa->LocMesh[++ije] - Pa->first;

	  for (isd = 0; isd < Pa->nsd; isd++)
	    {
	      double xx = Pa->cogeoloc[no*nsd+isd] + dscan[no*nsd+isd];
	      if ( xx < bbox[2*isd])
		bbox[2*isd] = xx;
	      else if (xx > bbox[2*isd+1])
		bbox[2*isd+1] = xx;
	    }
	}
      gg_tree_insertdata (Pa->gt, (void *)(&iel), bbox);
    }

  /* Insert data: -(element_number+1) for "external"
   *                                      (inter-proccessor) elements
   */
  for (iel = 0; iel < Pa->nelemext; iel++)
    {
      /* Compute bounding box: */
      ije = Pa->nnpe * iel;  /* start of element in conectivity */
      no = Pa->ExtMesh[ije] - 1; /* first node of this element */

      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  if (no < 0)
	    xx = Pa->cogeoext[-(no+1)*nsd+isd] +
	      descan[-(no+1)*nsd+isd];
	  else
	    xx = Pa->cogeoloc[(no+1-Pa->first)*nsd+isd] +
	      dscan[(no+1-Pa->first)*nsd+isd];
	  bbox[2*isd] = bbox[2*isd+1] = xx;
	}

      for (in = 1; in < Pa->nnpe; in++) /* rest of nodes */
	{
	  no = Pa->ExtMesh[++ije] - 1;

	  for (isd = 0; isd < Pa->nsd; isd++)
	    {
	      if (no < 0)
		xx = Pa->cogeoext[-(no+1)*nsd+isd] +
		  descan[-(no+1)*nsd+isd];
	      else
		xx = Pa->cogeoloc[(no+1-Pa->first)*nsd+isd] +
		  dscan[(no+1-Pa->first)*nsd+isd];

	      if (xx < bbox[2*isd])
		bbox[2*isd] = xx;
	      else if (xx > bbox[2*isd+1])
		bbox[2*isd+1] = xx;
	    }
	}

      idata = -(iel+1);
      gg_tree_insertdata (Pa->gt, (void *)(&idata), bbox);
    }
  ierr = VecRestoreArray(disp, &dscan);   CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecRestoreArray(dispe, &descan); CHKERRABORT(PETSC_COMM_WORLD, ierr);
  return 0;
}

/*
 * in_interpdisp_ffields:
 * Creates data structures for an efficient geometric search of elements
 * of the mesh (include local elements, as well as intra-proccesses elements
 * arguments:
 * Pa(input,output): Data structure with distributed mesh
 * tree_size(input): adjust refinement of the auxiliary tree,
 *     1.0: same as init_interpol
 *     > 1.0: less refinement, less memory, more time
 *     < 1.0: more refinement, more memory, less time
 *     recommended: ~ 4.0
 * v(input): vector of fields over nodes (including displacements)
 *          (v[inod*sclpn+iscl+isd])
 * ve(input): fields of "external" nodes
 *
 * return value: 0: everthing OK, < 0: error
 *
 */
/* Fortran interface */
void ininterpdispffields_ (partition **fPa, double *tree_size, Vec *v,
			   systemdata **fSy, int *fsclpn, int *fidisp,
			   int *retv)
{
  *retv = in_interpdisp_ffields(*fPa, *tree_size, *v, (*fSy)->varext,
				*fsclpn, *fidisp);
}
/* Implementation */
int in_interpdisp_ffields (partition *Pa, double tree_size, Vec v, Vec ve,
			   int sclpn, int idisp)
{
  int iel, in, isd;
  int ije, no, idata;
  int nsd, ierr;

  double xx, xmi, xma;
  double bbox[6];
  double *dscan, *descan;

  nsd = Pa->nsd;

  ierr = VecGetArray(v, &dscan);     CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGetArray(ve, &descan);   CHKERRABORT(PETSC_COMM_WORLD, ierr);

  for (isd = 0; isd < Pa->nsd; isd++)
    bbox[2*isd] = bbox[2*isd+1] = Pa->cogeoloc[isd]+dscan[idisp+isd];

  /* Compute bounding box:
   *  local nodes:
   */
  for (in = 1; in < Pa->nodloc; in++)
    {
      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  double xx = Pa->cogeoloc[in*nsd+isd] + dscan[in*sclpn+idisp+isd];
	  if (xx < bbox[2*isd])
	    bbox[2*isd] = xx;
	  else if (xx > bbox[2*isd+1])
	    bbox[2*isd+1] = xx;
	}
    }

  /* "external" (non-local, neighbouring nodes) */
  for (in = 0; in < Pa->nodext; in++)
    {
      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  double xx = Pa->cogeoext[in*nsd+isd] + descan[in*sclpn+idisp+isd];
	  if (xx < bbox[2*isd])
	    bbox[2*isd] = xx;
	  else if (xx > bbox[2*isd+1])
	    bbox[2*isd+1] = xx;
	}
    }

  /* Create tree */
  Pa->gt = (gg_tree *) malloc (sizeof (gg_tree));
  if (!Pa->gt)
    return -1;
  gg_tree_init (Pa->gt, Pa->nsd, sizeof(int), bbox);

  /* Adjust size using element size (diameter 1st dimension)
   *  - only x-size
   *  - at last node of the element
   *  - only use local elements
   */

  for (iel = 0; iel < Pa->nelemloc; iel++)
    {
      double point[nsd];
      ije = Pa->nnpe * iel;
      no = Pa->LocMesh[ije] - Pa->first;
      xmi = xma = Pa->cogeoloc[no*nsd] + dscan[no*sclpn+idisp];
      for (in = 1; in < Pa->nnpe; in++)
	{
	  no = Pa->LocMesh[++ije] - Pa->first;
	  xx = Pa->cogeoloc[no*nsd] + dscan[no*sclpn+idisp];
	  if (xx > xma) xma = xx;
	  else if (xx < xmi) xmi = xx;
	}
      for (isd = 0; isd < nsd; isd++)
	point[isd] = Pa->cogeoloc[no*nsd+isd] + dscan[no*sclpn+idisp+isd];
      gg_tree_adjustsize (Pa->gt, point,
			  tree_size * (xma-xmi));
    }

  /* Insert data: element numbers (for local elements) */
  for (iel = 0; iel < Pa->nelemloc; iel++)
    {
      /* Compute bounding box: */
      ije = Pa->nnpe * iel;  /* start of element in conectivity */
      no = Pa->LocMesh[ije] - Pa->first; /* first node of this element */

      for (isd = 0; isd < Pa->nsd; isd++)
	bbox[2*isd] = bbox[2*isd+1] =
	  Pa->cogeoloc[no*nsd+isd]+dscan[no*sclpn+idisp+isd];

      for (in = 1; in < Pa->nnpe; in++) /* rest of nodes */
	{
	  no = Pa->LocMesh[++ije] - Pa->first;

	  for (isd = 0; isd < Pa->nsd; isd++)
	    {
	      double xx = Pa->cogeoloc[no*nsd+isd] + dscan[no*sclpn+idisp+isd];
	      if ( xx < bbox[2*isd])
		bbox[2*isd] = xx;
	      else if (xx > bbox[2*isd+1])
		bbox[2*isd+1] = xx;
	    }
	}
      gg_tree_insertdata (Pa->gt, (void *)(&iel), bbox);
    }

  /* Insert data: -(element_number+1) for "external"
   *                                      (inter-proccessor) elements
   */
  for (iel = 0; iel < Pa->nelemext; iel++)
    {
      /* Compute bounding box: */
      ije = Pa->nnpe * iel;  /* start of element in conectivity */
      no = Pa->ExtMesh[ije] - 1; /* first node of this element */

      for (isd = 0; isd < Pa->nsd; isd++)
	{
	  if (no < 0)
	    xx = Pa->cogeoext[-(no+1)*nsd+isd] +
	      descan[-(no+1)*sclpn+idisp+isd];
	  else
	    xx = Pa->cogeoloc[(no+1-Pa->first)*nsd+isd] +
	      dscan[(no+1-Pa->first)*sclpn+idisp+isd];
	  bbox[2*isd] = bbox[2*isd+1] = xx;
	}

      for (in = 1; in < Pa->nnpe; in++) /* rest of nodes */
	{
	  no = Pa->ExtMesh[++ije] - 1;

	  for (isd = 0; isd < Pa->nsd; isd++)
	    {
	      if (no < 0)
		xx = Pa->cogeoext[-(no+1)*nsd+isd] +
		  descan[-(no+1)*sclpn+idisp+isd];
	      else
		xx = Pa->cogeoloc[(no+1-Pa->first)*nsd+isd] +
		  dscan[(no+1-Pa->first)*sclpn+idisp+isd];

	      if (xx < bbox[2*isd])
		bbox[2*isd] = xx;
	      else if (xx > bbox[2*isd+1])
		bbox[2*isd+1] = xx;
	    }
	}

      idata = -(iel+1);
      gg_tree_insertdata (Pa->gt, (void *)(&idata), bbox);
    }
  ierr = VecRestoreArray(v, &dscan);   CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecRestoreArray(ve, &descan); CHKERRABORT(PETSC_COMM_WORLD, ierr);
  return 0;
}

/*
 * stop_interp:
 * Clears data structures for efficient search
 * Arguments:
 * Pa(input,output): Data structure with distributed mesh
 */
/* Fortran interface */
void stopinterp_ (partition **fPa, int *retv)
{
  *retv = stop_interp(*fPa);
}
/* Implementation */
int stop_interp (partition *Pa)
{
  gg_tree_clear (Pa->gt);
  Pa->gt = NULL;
  return 0;
}

/*
 * interp_local:
 * Given a point and a field over the nodes of the mesh this function
 * search the element containing the point and interpolates the field.
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * point(input): point where the function is needed
 * vsc(input): Vector of values of the fields on the local nodes
 * vesc(input): vector of values of the fields on the "external" nodes.
 * stride(input): number of values in each node.
 * start(input): position of the field in the list of values of the node
 * ncomps(input): components of the field to interpolate
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * result(output): pointer to memory where the results should be placed
 *        (ncomps components of the interpolated field)
 *
 * return value: 0: OK, interpolation done
 *              -1: point outside local mesh
 */
/* Fortran interface:
 * Extra arguments:
 *    fSy: (system data): used for accessing the local components of the
 *        "external" vector. Also for the "stride" argument
 *    fv: Petsc vector with unknowns
 */
void interplocal_ (partition **fPa, systemdata **fSy, Vec *fv, double *point,
		   int *start, int *ncomps,
		   double *result, int *mode, int *retv)
{
  double *vsc, *vesc;
  VecGetArray(*fv, &vsc);
  VecGetArray((*fSy)->varext, &vesc);
  *retv = interp_local(*fPa, point, vsc, vesc,
		       (*fSy)->nfields, *start, *ncomps, *mode, result);
  VecRestoreArray((*fSy)->varext, &vesc);
  VecRestoreArray(*fv, &vsc);
}
/* Implementation */
int interp_local (partition *Pa, double *point, double *vsc, double *vesc,
		  int stride, int start, int ncomps, int mode,
		  double *result)
{
  int iel, ic, in, no, rval;
  double weights[Pa->nnpe];

  rval = search_local (Pa, point, mode, &iel, weights);

  if (rval >= 0) /* -1: not found, 0: found in local mesh, 1: in extmesh */
    {
      for (ic = 0; ic < ncomps; ic++)
	{
	  result[ic] = 0;
	  for (in = 0; in < Pa->nnpe; in++)
	    {
	      if (rval == 0)
		{
		  no = Pa->LocMesh[Pa->nnpe * iel + in] - Pa->first;
		  result[ic] += weights[in] * vsc[start + no * stride + ic];
		}
	      else
		{
		  no = Pa->ExtMesh[Pa->nnpe * iel + in] - 1;
		  if (no >= 0)
		    {
		      no = no + 1 - Pa->first;
		      result[ic] += weights[in] * vsc[start + no*stride + ic];
		    }
		  else
		    {
		      no = -(no + 1);
		      result[ic] += weights[in] * vesc[start + no*stride + ic];
		    }
		}
	    }
	}
      return 0;
    }
  return -1; /* Not found */
}

/*
 * interp_local_disp:
 * Given a point and a field over the nodes of the mesh this function
 * search the element containing the point and interpolates the field,
 * (use a displacement of the nodes)
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * disp: Displacement of the nodes
 * dispe: Displacement of "external" nodes
 * point(input): point where the function is needed
 * vsc(input): Vector of values of the fields on the local nodes
 * vesc(input): vector of values of the fields on the "external" nodes.
 * stride(input): number of values in each node.
 * start(input): position of the field in the list of values of the node
 * ncomps(input): components of the field to interpolate
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * result(output): pointer to memory where the results should be placed
 *        (ncomps components of the interpolated field)
 *
 * return value: 0: OK, interpolation done
 *              -1: point outside local mesh
 */
/* Fortran interface:
 * Extra arguments:
 *    fSy: (system data): used for accessing the local components of the
 *        "external" vector. Also for the "stride" argument
 *    fv: Petsc vector with unknowns
 */
void interplocaldisp_ (partition **fPa, Vec *disp, Vec *dispe,
		       double *point, Vec *fv, systemdata **fSy,
		       int *start, int *ncomps, int *mode,
		       double *result, int *retv)
{
  double *vsc, *vesc, *scandisp, *scandispe;
  VecGetArray(*disp, &scandisp);
  VecGetArray(*dispe, &scandispe);
  VecGetArray(*fv, &vsc);
  VecGetArray((*fSy)->varext, &vesc);
  *retv = interp_local_disp (*fPa, scandisp, scandispe,
			     point, vsc, vesc,
			     (*fSy)->nfields, *start, *ncomps, *mode,
			     result);
  VecRestoreArray((*fSy)->varext, &vesc);
  VecRestoreArray(*fv, &vsc);
  VecRestoreArray(*dispe, &scandispe);
  VecRestoreArray(*disp, &scandisp);
}
/* Implementation */
int interp_local_disp (partition *Pa, double *disp, double *dispe,
		       double *point, double *vsc, double *vesc,
		       int stride, int start, int ncomps, int mode,
		       double *result)
{
  int iel, ic, in, no, rval;
  double weights[Pa->nnpe];

  rval = search_local_disp (Pa, disp, dispe, point, mode, &iel, weights);

  if (rval >= 0) /* -1: not found, 0: found in local mesh, 1: in extmesh */
    {
      for (ic = 0; ic < ncomps; ic++)
	{
	  result[ic] = 0;
	  for (in = 0; in < Pa->nnpe; in++)
	    {
	      if (rval == 0)
		{
		  no = Pa->LocMesh[Pa->nnpe * iel + in] - Pa->first;
		  result[ic] += weights[in] * vsc[start + no * stride + ic];
		}
	      else
		{
		  no = Pa->ExtMesh[Pa->nnpe * iel + in] - 1;
		  if (no >= 0)
		    {
		      no = no + 1 - Pa->first;
		      result[ic] += weights[in] * vsc[start + no*stride + ic];
		    }
		  else
		    {
		      no = -(no + 1);
		      result[ic] += weights[in] * vesc[start + no*stride + ic];
		    }
		}
	    }
	}
      return 0;
    }
  return -1; /* Not found */
}

/*
 * interp_local_disp_ffields:
 * Given a point and a field over the nodes of the mesh this function
 * search the element containing the point and interpolates the field,
 * (use displacement of the nodes contained in an unknowns vector)
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * v(input): vector of fields over nodes (including displacements)
 *          (v[inod*sclpn+iscl+isd])
 * ve: fields of "external" nodes
 * sclpn: scalars per node
 * idisp: index of displacement in the scalars of a node (starts at 0)
 * point(input): point where the function is needed
 * vsc(input): Vector of values of the fields on the local nodes
 * vesc(input): vector of values of the fields on the "external" nodes.
 * stride(input): number of values in each node.
 * start(input): position of the field in the list of values of the node
 * ncomps(input): components of the field to interpolate
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * result(output): pointer to memory where the results should be placed
 *        (ncomps components of the interpolated field)
 *
 * return value: 0: OK, interpolation done
 *              -1: point outside local mesh
 */
/* Fortran interface:
 * Extra arguments:
 *    fSy: (system data): used for accessing the local components of the
 *        "external" vector. Also for the "stride" argument
 *    fv: Petsc vector with unknowns
 */
void interplocaldispffields_ (partition **fPa, systemdata **fSy,
			      Vec *v, int *fsclpn, int *fidisp,
			      double *point, Vec *fv,
			      int *start, int *ncomps, int *mode,
			      double *result, int *retv)
{
  double *vsc, *vesc, *scanv, *scanve, *scanva;
  int veqva, fveqva;

  VecGetArray(*v, &scanv);
  VecGetArray((*fSy)->va, &scanva);
  veqva = (scanv == scanva);

  /* This is not always correct:
   * we only check if the "v" input vector is the Sy->va,
   * and set the external fields vector to Sy->varaext in that case,
   * otherwise, we set the external fields vector to Sy->varext
   */
  if (veqva)
    VecGetArray((*fSy)->varaext, &scanve);
  else
    VecGetArray((*fSy)->varext, &scanve);

  VecGetArray(*fv, &vsc);

  /* This is not always correct:
   * we only check if the "fv" input vector is the Sy->va,
   * and set the external fields vector to Sy->varaext in that case,
   * otherwise, we set the external fields vector to Sy->varext
   */
  fveqva = (vsc == scanva);
  if (fveqva)
    VecGetArray((*fSy)->varaext, &vesc);
  else
    VecGetArray((*fSy)->varext, &vesc);

  *retv = interp_local_disp_ffields (*fPa,
				     scanv, scanve, *fsclpn, *fidisp,
				     point, vsc, vesc,
				     (*fSy)->nfields, *start, *ncomps, *mode,
				     result);
  if (fveqva)
    VecRestoreArray((*fSy)->varaext, &vesc);
  else
    VecRestoreArray((*fSy)->varext, &vesc);

  VecRestoreArray(*fv, &vsc);

  if (veqva)
    VecRestoreArray((*fSy)->varaext, &scanve);
  else
    VecRestoreArray((*fSy)->varext, &scanve);

  VecRestoreArray((*fSy)->va, &scanva);
  VecRestoreArray(*v, &scanv);
}

/* Implementation */
int interp_local_disp_ffields (partition *Pa,
			       double *v, double *ve, int sclpn, int idisp,
			       double *point, double *vsc, double *vesc,
			       int stride, int start, int ncomps, int mode,
			       double *result)
{
  int iel, ic, in, no, rval;
  double weights[Pa->nnpe];

  rval = search_local_disp_ffields (Pa, v, ve, sclpn, idisp,
				    point, mode, &iel, weights);

  if (rval >= 0) /* -1: not found, 0: found in local mesh, 1: in extmesh */
    {
      for (ic = 0; ic < ncomps; ic++)
	{
	  result[ic] = 0;
	  for (in = 0; in < Pa->nnpe; in++)
	    {
	      if (rval == 0)
		{
		  no = Pa->LocMesh[Pa->nnpe * iel + in] - Pa->first;
		  result[ic] += weights[in] * vsc[start + no * stride + ic];
		}
	      else
		{
		  no = Pa->ExtMesh[Pa->nnpe * iel + in] - 1;
		  if (no >= 0)
		    {
		      no = no + 1 - Pa->first;
		      result[ic] += weights[in] * vsc[start + no*stride + ic];
		    }
		  else
		    {
		      no = -(no + 1);
		      result[ic] += weights[in] * vesc[start + no*stride + ic];
		    }
		}
	    }
	}
      return 0;
    }
  return -1; /* Not found */
}


/*
 * search_local:
 * Given a point search the element containing it
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * point(input): point
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * elem(output): element containing the point
 * weights(output): weights for interpolation
 *         (one for each node of the element, sum(weigths) = 1)
 *
 * return value: -1: point outside local mesh
 *                0: point found in totally local elements
 *                1: point found in an intra-proccesss element
 */
/* Fortran interface */
void searchlocal_ (partition **fPa, double *point, int *mode,
		  int *elem, double *weights, int *retv)
{
  *retv = search_local(*fPa, point, *mode, elem, weights);
}
/* Implementation */
int search_local (partition *Pa, double *point, int mode,
		  int *elem, double *weights)
{
  int iel, ije, in, no, node, isd, skip, retval = -1;
  int perswitch;
  double coords[Pa->nnpe*Pa->nsd];
  node_g_list *nl;
  nl = gg_tree_getlisthead (Pa->gt, point);
  perswitch = Pa->MeshKind >> 1;

  while (nl)
    {
      iel = *((int *) nl->data);
      skip = 1;
      if (iel < 0 && mode > 0) /* Element is in extmesh, but mode > 0 */
	{
	  iel = -iel-1;
	  if (mode == 2 || Pa->ExtOwnership[iel] == 1) /* All elements in
							* extmesh or element
							* ownership */
	    {
	      skip = 0;
	      retval = 1;
	      ije = Pa->nnpe * iel;
	      for (in = 0; in < Pa->nnpe; in++)
		{
		  no = Pa->ExtMesh[ije++] - 1;
		  if (no < 0)
		    node = -(no+1);
		  else
		    node = no + 1 - Pa->first;
		  for (isd = 0; isd < Pa->nsd; isd++)
		    {
		      if (no < 0)
			coords[in * Pa->nsd + isd] =
			  Pa->cogeoext[node * Pa->nsd + isd];
		      else
			coords[in * Pa->nsd + isd] =
			  Pa->cogeoloc[node*Pa->nsd+isd];
		    }
		}
	    }
	}
      else if (iel >= 0)
	{
	  skip = 0;
	  retval = 0;
	  ije = Pa->nnpe * iel;
	  for (in = 0; in < Pa->nnpe; in++)
	    {
	      no = Pa->LocMesh[ije++] - Pa->first;
	      for (isd = 0; isd < Pa->nsd; isd++)
		coords[in*Pa->nsd+isd] = Pa->cogeoloc[no*Pa->nsd+isd];
	    }
	}
      if (!skip)
	{   /* Correction for periodic coordinates */
	  if (perswitch)   /* general switch */
	    {
	      double cmin, cmax;
	      for (isd = 0; isd < Pa->nsd; isd++)
		{
		  if (perswitch & (1 << isd))  /* this coordinate switch */
		    {
		      cmin = cmax = coords[isd];  /* compute extension */
		      for (in = 1; in < Pa->nnpe; in++)
			{
			  if (coords[in*Pa->nsd+isd] < cmin)
			    cmin = coords[in*Pa->nsd+isd];
			  else if (coords[in*Pa->nsd+isd] > cmax)
			    cmax = coords[in*Pa->nsd+isd];
			}
		      if (cmax - cmin > Pa->per[isd] / 2.0)
			{   /* element too big */
			  for (in = 0; in < Pa->nnpe; in++)
			    {
			      if (coords[in*Pa->nsd+isd] < Pa->per[isd] / 2.0)
				coords[in*Pa->nsd+isd] += Pa->per[isd];
			    }
			}
		    }
		}
	    }
	  /* End correction for periodic coordinates */
	  if (point_in_element(Pa->nsd, Pa->nnpe, coords, point, weights))
	    {
	      *elem = iel;
	      return retval;
	    }
	}
      nl = nl->next;
    }
  return -1;
}

/*
 * search_local_disp:
 * Given a point search the element containing it, using a displaced mesh
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * disp(input): vector of displacements (disp[inod*nsd+isd])
 * dispe: Displacement of "external" nodes
 * point(input): point
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * elem(output): element containing the point
 * weights(output): weights for interpolation
 *         (one for each node of the element, sum(weigths) = 1)
 *
 * return value: -1: point outside local mesh
 *                0: point found in totally local elements
 *                1: point found in an intra-proccesss element
 */
/* Fortran interface */
void searchlocaldisp_ (partition **fPa, Vec *disp, Vec *dispe,
		       double *point, int *mode, int *elem, double *weights,
		       int *retv)
{
  double *scand, *scande;
  VecGetArray(*disp, &scand);
  VecGetArray(*dispe, &scande);
  *retv = search_local_disp (*fPa, scand, scande, point, *mode, elem, weights);
  VecRestoreArray(*dispe, &scande);
  VecRestoreArray(*disp, &scand);
}
/* Implementation */
int search_local_disp (partition *Pa, double *disp, double *dispe,
		       double *point, int mode, int *elem, double *weights)
{
  int iel, ije, in, no, node, isd, skip, poscoo, retval = -1;
  /* int perswitch; */
  double coords[Pa->nnpe*Pa->nsd];
  node_g_list *nl;
  nl = gg_tree_getlisthead (Pa->gt, point);
  /* perswitch = Pa->MeshKind >> 1; */

  while (nl)
    {
      iel = *((int *) nl->data);
      skip = 1;
      if (iel < 0 && mode > 0) /* Element is in extmesh, but mode > 0 */
	{
	  iel = -iel-1;
	  if (mode == 2 || Pa->ExtOwnership[iel] == 1) /* All elements in
							* extmesh or element
							* ownership */
	    {
	      skip = 0;
	      retval = 1;
	      ije = Pa->nnpe * iel;
	      for (in = 0; in < Pa->nnpe; in++)
		{
		  no = Pa->ExtMesh[ije++] - 1;
		  if (no < 0)
		    node = -(no+1);
		  else
		    node = no + 1 - Pa->first;
		  for (isd = 0; isd < Pa->nsd; isd++)
		    {
		      if (no < 0)
			{
			  poscoo = node * Pa->nsd + isd;
			  coords[in * Pa->nsd + isd] =
			    Pa->cogeoext[poscoo] + dispe[poscoo];
			}
		      else
			{
			  poscoo = node * Pa->nsd + isd;
			  coords[in * Pa->nsd + isd] =
			    Pa->cogeoloc[poscoo] + disp[poscoo];
			}
		    }
		}
	    }
	}
      else if (iel >= 0)
	{
	  skip = 0;
	  retval = 0;
	  ije = Pa->nnpe * iel;
	  for (in = 0; in < Pa->nnpe; in++)
	    {
	      no = Pa->LocMesh[ije++] - Pa->first;
	      for (isd = 0; isd < Pa->nsd; isd++)
		{
		  poscoo = no * Pa->nsd + isd;
		  coords[in*Pa->nsd+isd] = Pa->cogeoloc[poscoo] + disp[poscoo];
		}
	    }
	}
      if (!skip &&
	  point_in_element(Pa->nsd, Pa->nnpe, coords, point, weights))
	{
	  *elem = iel;
	  return retval;
	}
      nl = nl->next;
    }
  return -1;
}

/*
 * search_local_disp_ffields:
 * Given a point search the element containing it, using a displaced mesh
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * v(input): vector of fields over nodes (including displacements)
 *          (v[inod*sclpn+iscl])
 * ve: fields of "external" nodes
 * sclpn: scalars per node
 * idisp: index of displacement in the scalars of a node (starts at 0)
 * point(input): point
 * mode(input): wether to include intra-proccesses elements in the search.
 *      0: only totally local elements
 *      1: add intra-proccess elements if belong to the proccessor
 *      2: add all intra-proccess elements.
 * elem(output): element containing the point
 * weights(output): weights for interpolation
 *         (one for each node of the element, sum(weigths) = 1)
 *
 * return value: -1: point outside local mesh
 *                0: point found in totally local elements
 *                1: point found in an intra-proccesss element
 */
/* Fortran interface */
void searchlocaldispffields_ (partition **fPa, Vec *v, Vec *ve,
			      int *fsclpn, int *fidisp,
			      double *point, int *mode,
			      int *elem, double *weights, int *retv)
{
  double *scanv, *scanve;
  VecGetArray(*v, &scanv);
  VecGetArray(*ve, &scanve);
  *retv = search_local_disp_ffields (*fPa, scanv, scanve, *fsclpn, *fidisp,
				     point, *mode, elem, weights);
  VecRestoreArray(*ve, &scanve);
  VecRestoreArray(*v, &scanv);
}
/* Implementation */
int search_local_disp_ffields (partition *Pa,
			       double *v, double *ve, int sclpn, int idisp,
			       double *point, int mode,
			       int *elem, double *weights)
{
  int iel, ije, in, no, node, isd, skip, poscoo, retval = -1;
  /* int perswitch; */
  double coords[Pa->nnpe*Pa->nsd];
  node_g_list *nl;
  nl = gg_tree_getlisthead (Pa->gt, point);
  /* perswitch = Pa->MeshKind >> 1; */

  while (nl)
    {
      iel = *((int *) nl->data);
      skip = 1;
      if (iel < 0 && mode > 0) /* Element is in extmesh, but mode > 0 */
	{
	  iel = -iel-1;
	  if (mode == 2 || Pa->ExtOwnership[iel] == 1) /* All elements in
							* extmesh or element
							* ownership */
	    {
	      skip = 0;
	      retval = 1;
	      ije = Pa->nnpe * iel;
	      for (in = 0; in < Pa->nnpe; in++)
		{
		  no = Pa->ExtMesh[ije++] - 1;
		  if (no < 0)
		    node = -(no+1);
		  else
		    node = no + 1 - Pa->first;
		  for (isd = 0; isd < Pa->nsd; isd++)
		    {
		      if (no < 0)
			{
			  poscoo = node * Pa->nsd + isd;
			  coords[in * Pa->nsd + isd] =
			    Pa->cogeoext[poscoo] + ve[node*sclpn+idisp+isd];
			}
		      else
			{
			  poscoo = node * Pa->nsd + isd;
			  coords[in * Pa->nsd + isd] =
			    Pa->cogeoloc[poscoo] + v[node*sclpn+idisp+isd];
			}
		    }
		}
	    }
	}
      else if (iel >= 0)
	{
	  skip = 0;
	  retval = 0;
	  ije = Pa->nnpe * iel;
	  for (in = 0; in < Pa->nnpe; in++)
	    {
	      no = Pa->LocMesh[ije++] - Pa->first;
	      for (isd = 0; isd < Pa->nsd; isd++)
		{
		  poscoo = no * Pa->nsd + isd;
		  coords[in*Pa->nsd+isd] = Pa->cogeoloc[poscoo] +
		    v[no*sclpn+idisp+isd];
		}
	    }
	}
      if (!skip &&
	  point_in_element(Pa->nsd, Pa->nnpe, coords, point, weights))
	{
	  *elem = iel;
	  return retval;
	}
      nl = nl->next;
    }
  return -1;
}

/*
 * interp_global:
 * Interpolate a field in a set of points, (all processes ask
 *  for a (different) set of points)
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * points(input): vector of points (x1, y1, ... x2, y2, .......)
 * npoints(input): number of points
 * vloc(input): Vector of values of the fields on the local nodes
 * vext(input): Vector of values of the fields on the "external" nodes
 * stride(input): number of values in each node
 * start(input): position of the field to be interpolated
 * ncomps(input): components of the field to be interpolated
 * results(output): vector to hold the results (npoints*ncomps)
 *
 * return value: number of points not found in mesh
 */
/* Fortran interface
 *  Extra arguments:
 *    fSy: (system data): used for accessing the local components of the
 *        "external" vector. Also for the "stride" argument
 *    fv: Petsc vector with unknowns
 */
void interpglobal_ (systemdata **fSy, double *points, int *npoints,
		    Vec *fv, int *start, int *ncomps, double *result,
		    int *sw, int *retv)
{
  double *Vloc, *Vext;
  VecGetArray(*fv, &Vloc);
  VecGetArray((*fSy)->varext, &Vext);

  *retv = interp_global ((*fSy)->Pa, points, *npoints, Vloc, Vext,
			 (*fSy)->nfields, *start, *ncomps,
			 result, sw);

  VecRestoreArray(*fv, &Vloc);
  VecRestoreArray((*fSy)->varext, &Vext);
}
/* Implementation */
int interp_global (partition *Pa, double *points, int npoints,
		   double *vloc, double *vext,
		   int stride, int start, int ncomps, double *results, int *sw)
{
  int i, ri, ip, isd;
  int nprocs;
  int snfp, totsnf, *vsnfp, *vcounts, *disps;
  int *snfsw, *allsnfsw;
  double *snfpts, *snfres;
  double *allsnfpts, *allsnfres;
  int rank , ierr;
  /* Profiling variables */
  int flocal, fexternal;
  double time0, time1, time2, time3, time4;

  time0 = MPI_Wtime();
  /* First step: interp_local */
  snfp = 0;
  for (ip = 0; ip < npoints; ip++)
    {
      sw[ip] = interp_local (Pa, points + ip * Pa->nsd, vloc, vext,
			     stride, start, ncomps, 2,
			     results + ip * ncomps);
      if (sw[ip] == -1)
	snfp++;
    }

  MPI_Comm_size (Pa->Comm, &nprocs);
  if (nprocs == 1)
    {
      time1 = MPI_Wtime();
      PetscPrintf (Pa->Comm, "interp_global: found %d / %d points, %g Secs.\n",
		   npoints - snfp, npoints, time1-time0);
      return snfp;
    }
  flocal = npoints - snfp;
  time1 = MPI_Wtime();

  /* Second step: compact vector of still-not-found points */
  if (snfp > 0)
    {
      snfpts = (double *) malloc (snfp * Pa->nsd * sizeof(double));
      snfsw  = (int *) malloc (snfp * sizeof(int));
      snfres = (double *) malloc (snfp * ncomps * sizeof(double));
      if (!snfpts || !snfsw || !snfres)
	return snfp;

      for (ip = 0, snfp = 0; ip < npoints; ip++)
	if (sw[ip] == -1)
	  {
	    snfsw[snfp] = 0;
	    for (isd = 0; isd < Pa->nsd; isd++)
	      snfpts[Pa->nsd * snfp + isd] = points[ip * Pa->nsd + isd];
	    snfp++;
	  }
    }
  else
    {
      snfsw = NULL;
      snfpts = snfres = NULL;
    }

  /* Gather all still-not-found points: compute total number: */
  i = MPI_Allreduce (&snfp, &totsnf, 1, MPI_INT, MPI_SUM, Pa->Comm);

  if (totsnf == 0)
    {
      PetscSynchronizedPrintf
	(Pa->Comm, "interp_global: found locally all %d points, %g Secs.\n",
	 npoints, time1-time0);
      PetscSynchronizedFlush(Pa->Comm, stdout);
      return 0;
    }
  PetscPrintf
    (Pa->Comm, "interp_global (first search): %g Secs.\n", time1-time0);

  /* Alloc memory: */
  allsnfpts = (double *) malloc (totsnf * Pa->nsd * sizeof(double));
  allsnfsw  = (int *) malloc (totsnf * sizeof(int));
  allsnfres = (double *) malloc (totsnf * ncomps * sizeof(double));
  vsnfp = (int *) malloc (nprocs * sizeof(int));
  vcounts = (int *) malloc (nprocs * sizeof(int));
  disps = (int *) malloc (nprocs * sizeof(int));
  if (!allsnfpts || !allsnfsw || !allsnfres || !vsnfp || !disps)
    return -1;
  /* Gather all: */
  ierr = MPI_Allgather (&snfp, 1, MPI_INT, vsnfp, 1, MPI_INT, Pa->Comm);

  for (i = 0; i < nprocs; i++)
    {
      vcounts[i] = vsnfp[i] * Pa->nsd;
      if (i == 0)
	disps[i] = 0;
      else
	disps[i] = disps[i-1] + vcounts[i-1];
    }
    
  ierr += MPI_Allgatherv (snfpts, snfp*Pa->nsd, MPI_DOUBLE,
			  allsnfpts, vcounts, disps, MPI_DOUBLE, Pa->Comm);
  if (ierr)
    PetscPrintf (Pa->Comm, "WARNING: MPI_xxxGather error in interp_global\n");

  /* Free unneeded memory */
  if (snfpts)
    free(snfpts);

  time2 = MPI_Wtime();
  PetscPrintf
    (Pa->Comm, "interp_global (1st Communication stage): %g Secs.\n",
     time2-time1);

  /* All proccesses search all points:
   * (Optimization: skip local points)
   */
  MPI_Comm_rank (Pa->Comm, &rank);
  /* Re-build disps vector (to identify local points) */
  for (i = 0; i < nprocs; i++)
    {
      if (i == 0)
	disps[i] = 0;
      else
	disps[i] = disps[i-1] + vsnfp[i-1];
    }

  for (ip = 0; ip < totsnf; ip++)
    {
      if (ip - disps[rank] >= 0 &&
	  ip - disps[rank] < snfp) /* already not found in this rank, skip */
	ri = -1;
      else
	ri = interp_local (Pa, allsnfpts + ip * Pa->nsd, vloc, vext,
			   stride, start, ncomps, 2,
			   allsnfres + ip * ncomps);

      if (ri == -1) /* Not found */
	{
	  allsnfsw[ip] = 0;
	  for (i = 0; i < ncomps; i++)
	    allsnfres[ip * ncomps + i] = 0.0;
	}
      else
	{
	  allsnfsw[ip] = 1;
	}
    }

  /* Free unneeded memory */
  free(allsnfpts);
  time3 = MPI_Wtime();
  PetscPrintf
    (Pa->Comm, "interp_global (Second search): %g Secs.\n", time3-time2);

  /* Reduce results: */
  for (i = 0; i < nprocs-1; i++)
    disps[i+1] = disps[i] + vsnfp[i];
  for (i = 0; i < nprocs; i++)              /* Proccessor "i" queries */
      MPI_Reduce (&allsnfsw[disps[i]], snfsw, vsnfp[i], MPI_INT, MPI_SUM,
		  i, Pa->Comm);
  for (i = 0; i < nprocs; i++)
    {
      vcounts[i] = vsnfp[i] * ncomps;
      if (i > 0)
	disps[i] = disps[i-1] + vcounts[i-1];
    }
  for (i = 0; i < nprocs; i++)              /* Proccessor "i" queries */
      MPI_Reduce (&allsnfres[disps[i]], snfres, vcounts[i], MPI_DOUBLE,
		  MPI_SUM, i, Pa->Comm);

  /* Free unneeded memory */
  free(allsnfsw);
  free(allsnfres);
  free(disps);
  free(vcounts);
  free(vsnfp);

  /* Compute average if found in multiple elements */
  for (ip = 0; ip < snfp; ip++)
    if (snfsw[ip] > 1)
      {
	for (i = 0; i < ncomps; i++)
	  snfres[ip*ncomps+i] /= snfsw[ip];
	snfsw[ip] = 1;
      }

  /* Distribute points found in another proccesses in local result vector */
  for (ip = 0, snfp = 0; ip < npoints; ip++)
    if (sw[ip] == -1)
      {
	if (snfsw[snfp] == 1)  /* found in another proccess, copy result */
	  {
	    for (i = 0; i < ncomps; i++)
	      results[ip * ncomps + i] = snfres[snfp * ncomps + i];
	    sw[ip] = 0;
	  }
	snfp++;
      }

  /* Free unneeded memory */
  free(snfsw);
  free(snfres);

  /* Re-count still-not-found points */
  for (ip = 0, snfp = 0; ip < npoints; ip++)
    if (sw[ip] == -1)
      snfp++;

  i = MPI_Allreduce (&snfp, &totsnf, 1, MPI_INT, MPI_SUM, Pa->Comm);

  fexternal = npoints - flocal - snfp;
  time4 = MPI_Wtime();
  PetscPrintf
    (Pa->Comm, "interp_global (2nd Communication stage): %g Secs.\n",
     time4-time3);
  PetscSynchronizedPrintf
    (Pa->Comm, "interp_global: looked for %d points, %d found locally, "
     "%d external, %d not found\n", npoints, flocal, fexternal, snfp);
  PetscSynchronizedFlush (Pa->Comm, stdout);

  return totsnf;
}

/*
 * interp_global_disp:
 * Interpolate a field in a set of points, (all processes ask
 *  for a (different) set of points), mesh is displaced
 * Arguments:
 * Pa(input): Data structure with distributed mesh
 * delta: displacement of local nodes
 * deltae: displacement of "external" nodes
 * points(input): vector of points (x1, y1, ... x2, y2, .......)
 * npoints(input): number of points
 * vloc(input): Vector of values of the fields on the local nodes
 * vext(input): Vector of values of the fields on the "external" nodes
 * stride(input): number of values in each node
 * start(input): position of the field to be interpolated
 * ncomps(input): components of the field to be interpolated
 * results(output): vector to hold the results (npoints*ncomps)
 *
 * return value: number of points not found in mesh
 */
/* Fortran interface
 *  Extra arguments:
 *    fSy: (system data): used for accessing the local components of the
 *        "external" vector. Also for the "stride" argument
 *    fv: Petsc vector with unknowns
 */
void interpglobaldisp_ (systemdata **fSy, Vec *delt, Vec *delte,
			double *points, int *npoints,
			Vec *fv, int *start, int *ncomps, double *result,
			int *sw, int *retv)
{
  double *Vloc, *Vext, *delta, *deltae;
  VecGetArray(*delt, &delta);
  VecGetArray(*delte, &deltae);
  VecGetArray(*fv, &Vloc);
  VecGetArray((*fSy)->varext, &Vext);

  *retv = interp_global_disp ((*fSy)->Pa, delta, deltae,
			      points, *npoints, Vloc, Vext,
			      (*fSy)->nfields, *start, *ncomps,
			      result, sw);

  VecRestoreArray(*fv, &Vloc);
  VecRestoreArray((*fSy)->varext, &Vext);
  VecRestoreArray(*delte, &deltae);
  VecRestoreArray(*delt, &delta);
}
/* Implementation */
int interp_global_disp (partition *Pa, double *delta, double *deltae,
			double *points, int npoints,
			double *vloc, double *vext,
			int stride, int start, int ncomps, double *results,
			int *sw)
{
  int i, ri, ip, isd;
  int nprocs;
  int snfp, totsnf, *vsnfp, *vcounts, *disps;
  int *snfsw, *allsnfsw;
  double *snfpts, *snfres;
  double *allsnfpts, *allsnfres;
  int rank;
  /* Profiling variables */
  int flocal, fexternal;
  double time0, time1, time2, time3, time4;

  time0 = MPI_Wtime();
  /* First step: interp_local */
  snfp = 0;
  for (ip = 0; ip < npoints; ip++)
    {
      sw[ip] = interp_local_disp (Pa, delta, deltae,
				  points + ip * Pa->nsd, vloc, vext,
				  stride, start, ncomps, 2,
				  results + ip * ncomps);
      if (sw[ip] == -1)
	snfp++;
    }

  MPI_Comm_size (Pa->Comm, &nprocs);
  if (nprocs == 1)
    {
      time1 = MPI_Wtime();
      PetscPrintf (Pa->Comm, "interp_global: found %d / %d points, %g Secs.\n",
		   npoints - snfp, npoints, time1-time0);
      return snfp;
    }
  flocal = npoints - snfp;
  time1 = MPI_Wtime();

  /* Second step: compact vector of still-not-found points */
  if (snfp > 0)
    {
      snfpts = (double *) malloc (snfp * Pa->nsd * sizeof(double));
      snfsw  = (int *) malloc (snfp * sizeof(int));
      snfres = (double *) malloc (snfp * ncomps * sizeof(double));
      if (!snfpts || !snfsw || !snfres)
	return snfp;

      for (ip = 0, snfp = 0; ip < npoints; ip++)
	if (sw[ip] == -1)
	  {
	    snfsw[snfp] = 0;
	    for (isd = 0; isd < Pa->nsd; isd++)
	      snfpts[Pa->nsd * snfp + isd] = points[ip * Pa->nsd + isd];
	    snfp++;
	  }
    }
  else
    {
      snfsw = NULL;
      snfpts = snfres = NULL;
    }

  /* Gather all still-not-found points: compute total number: */
  i = MPI_Allreduce (&snfp, &totsnf, 1, MPI_INT, MPI_SUM, Pa->Comm);

  if (totsnf == 0)
    {
      PetscSynchronizedPrintf
	(Pa->Comm, "interp_global: found locally all %d points, %g Secs.\n",
	 npoints, time1-time0);
      PetscSynchronizedFlush(Pa->Comm, stdout);
      return 0;
    }
  PetscPrintf
    (Pa->Comm, "interp_global (first search): %g Secs.\n", time1-time0);

  /* Alloc memory: */
  allsnfpts = (double *) malloc (totsnf * Pa->nsd * sizeof(double));
  allsnfsw  = (int *) malloc (totsnf * sizeof(int));
  allsnfres = (double *) malloc (totsnf * ncomps * sizeof(double));
  vsnfp = (int *) malloc (nprocs * sizeof(int));
  vcounts = (int *) malloc (nprocs * sizeof(int));
  disps = (int *) malloc (nprocs * sizeof(int));
  if (!allsnfpts || !allsnfsw || !allsnfres || !vsnfp || !disps)
    return -1;
  /* Gather all: */
  i = MPI_Allgather (&snfp, 1, MPI_INT, vsnfp, 1, MPI_INT, Pa->Comm);

  for (i = 0; i < nprocs; i++)
    {
      vcounts[i] = vsnfp[i] * Pa->nsd;
      if (i == 0)
	disps[i] = 0;
      else
	disps[i] = disps[i-1] + vcounts[i-1];
    }
    
  i = MPI_Allgatherv (snfpts, snfp*Pa->nsd, MPI_DOUBLE,
		      allsnfpts, vcounts, disps, MPI_DOUBLE, Pa->Comm);

  /* Free unneeded memory */
  if (snfpts)
    free(snfpts);

  time2 = MPI_Wtime();
  PetscPrintf
    (Pa->Comm, "interp_global (1st Communication stage): %g Secs.\n",
     time2-time1);

  /* All proccesses search all points:
   * (Optimization: skip local points)
   */
  MPI_Comm_rank (Pa->Comm, &rank);
  /* Re-build disps vector (to identify local points) */
  for (i = 0; i < nprocs; i++)
    {
      if (i == 0)
	disps[i] = 0;
      else
	disps[i] = disps[i-1] + vsnfp[i-1];
    }

  for (ip = 0; ip < totsnf; ip++)
    {
      if (ip - disps[rank] >= 0 &&
	  ip - disps[rank] < snfp) /* already not found in this rank, skip */
	ri = -1;
      else
	ri = interp_local_disp (Pa, delta, deltae,
				allsnfpts + ip * Pa->nsd, vloc, vext,
				stride, start, ncomps, 2,
				allsnfres + ip * ncomps);

      if (ri == -1) /* Not found */
	{
	  allsnfsw[ip] = 0;
	  for (i = 0; i < ncomps; i++)
	    allsnfres[ip * ncomps + i] = 0.0;
	}
      else
	{
	  allsnfsw[ip] = 1;
	}
    }

  /* Free unneeded memory */
  free(allsnfpts);
  time3 = MPI_Wtime();
  PetscPrintf
    (Pa->Comm, "interp_global (Second search): %g Secs.\n", time3-time2);

  /* Reduce results: */
  for (i = 0; i < nprocs-1; i++)
    disps[i+1] = disps[i] + vsnfp[i];
  for (i = 0; i < nprocs; i++)              /* Proccessor "i" queries */
      MPI_Reduce (&allsnfsw[disps[i]], snfsw, vsnfp[i], MPI_INT, MPI_SUM,
		  i, Pa->Comm);
  for (i = 0; i < nprocs; i++)
    {
      vcounts[i] = vsnfp[i] * ncomps;
      if (i > 0)
	disps[i] = disps[i-1] + vcounts[i-1];
    }
  for (i = 0; i < nprocs; i++)              /* Proccessor "i" queries */
      MPI_Reduce (&allsnfres[disps[i]], snfres, vcounts[i], MPI_DOUBLE,
		  MPI_SUM, i, Pa->Comm);

  /* Free unneeded memory */
  free(allsnfsw);
  free(allsnfres);
  free(disps);
  free(vcounts);
  free(vsnfp);

  /* Compute average if found in multiple elements */
  for (ip = 0; ip < snfp; ip++)
    if (snfsw[ip] > 1)
      {
	for (i = 0; i < ncomps; i++)
	  snfres[ip*ncomps+i] /= snfsw[ip];
	snfsw[ip] = 1;
      }

  /* Distribute points found in another proccesses in local result vector */
  for (ip = 0, snfp = 0; ip < npoints; ip++)
    if (sw[ip] == -1)
      {
	if (snfsw[snfp] == 1)  /* found in another proccess, copy result */
	  {
	    for (i = 0; i < ncomps; i++)
	      results[ip * ncomps + i] = snfres[snfp * ncomps + i];
	    sw[ip] = 0;
	  }
	snfp++;
      }

  /* Free unneeded memory */
  free(snfsw);
  free(snfres);

  /* Re-count still-not-found points */
  for (ip = 0, snfp = 0; ip < npoints; ip++)
    if (sw[ip] == -1)
      snfp++;

  i = MPI_Allreduce (&snfp, &totsnf, 1, MPI_INT, MPI_SUM, Pa->Comm);

  fexternal = npoints - flocal - snfp;
  time4 = MPI_Wtime();
  PetscPrintf
    (Pa->Comm, "interp_global (2nd Communication stage): %g Secs.\n",
     time4-time3);
  PetscSynchronizedPrintf
    (Pa->Comm, "interp_global: looked for %d points, %d found locally, "
     "%d external, %d not found\n", npoints, flocal, fexternal, snfp);
  PetscSynchronizedFlush (Pa->Comm, stdout);

  return totsnf;
}

/* Implementation */
int interp_global_disp_ffields (partition *Pa, double *vdelta, double *vdeltae,
				int sclpn, int idisp,
				double *points, int npoints,
				double *vloc, double *vext,
				int stride, int start, int ncomps,
				double *results,
				int *sw)
{
  int i, ri, ip, isd;
  int nprocs;
  int snfp, totsnf, *vsnfp, *vcounts, *disps;
  int *snfsw, *allsnfsw;
  double *snfpts, *snfres;
  double *allsnfpts, *allsnfres;
  int rank;
  /* Profiling variables */
  int flocal, fexternal;
  double time0, time1, time2, time3, time4;

  time0 = MPI_Wtime();
  /* First step: interp_local */
  snfp = 0;
  for (ip = 0; ip < npoints; ip++)
    {
      sw[ip] = interp_local_disp_ffields
	(Pa, vdelta, vdeltae, sclpn, idisp,
	 points + ip * Pa->nsd, vloc, vext,
	 stride, start, ncomps, 2, results + ip * ncomps);
      if (sw[ip] == -1)
	snfp++;
    }

  MPI_Comm_size (Pa->Comm, &nprocs);
  if (nprocs == 1)
    {
      time1 = MPI_Wtime();
      PetscPrintf
	(Pa->Comm,
	 "interp_global_disp_ffields: found %d / %d points, %g Secs.\n",
	 npoints - snfp, npoints, time1-time0);
      return snfp;
    }
  flocal = npoints - snfp;
  time1 = MPI_Wtime();

  /* Second step: compact vector of still-not-found points */
  if (snfp > 0)
    {
      snfpts = (double *) malloc (snfp * Pa->nsd * sizeof(double));
      snfsw  = (int *) malloc (snfp * sizeof(int));
      snfres = (double *) malloc (snfp * ncomps * sizeof(double));
      if (!snfpts || !snfsw || !snfres)
	return snfp;

      for (ip = 0, snfp = 0; ip < npoints; ip++)
	if (sw[ip] == -1)
	  {
	    snfsw[snfp] = 0;
	    for (isd = 0; isd < Pa->nsd; isd++)
	      snfpts[Pa->nsd * snfp + isd] = points[ip * Pa->nsd + isd];
	    snfp++;
	  }
    }
  else
    {
      snfsw = NULL;
      snfpts = snfres = NULL;
    }

  /* Gather all still-not-found points: compute total number: */
  i = MPI_Allreduce (&snfp, &totsnf, 1, MPI_INT, MPI_SUM, Pa->Comm);

  if (totsnf == 0)
    {
      PetscSynchronizedPrintf
	(Pa->Comm,
	 "interp_global_disp_ffields: found locally all %d points, %g Secs.\n",
	 npoints, time1-time0);
      PetscSynchronizedFlush(Pa->Comm, stdout);
      return 0;
    }
  PetscPrintf
    (Pa->Comm,
     "interp_global_disp_ffields (first search): %g Secs.\n", time1-time0);

  /* Alloc memory: */
  allsnfpts = (double *) malloc (totsnf * Pa->nsd * sizeof(double));
  allsnfsw  = (int *) malloc (totsnf * sizeof(int));
  allsnfres = (double *) malloc (totsnf * ncomps * sizeof(double));
  vsnfp = (int *) malloc (nprocs * sizeof(int));
  vcounts = (int *) malloc (nprocs * sizeof(int));
  disps = (int *) malloc (nprocs * sizeof(int));
  if (!allsnfpts || !allsnfsw || !allsnfres || !vsnfp || !disps)
    return -1;
  /* Gather all: */
  i = MPI_Allgather (&snfp, 1, MPI_INT, vsnfp, 1, MPI_INT, Pa->Comm);

  for (i = 0; i < nprocs; i++)
    {
      vcounts[i] = vsnfp[i] * Pa->nsd;
      if (i == 0)
	disps[i] = 0;
      else
	disps[i] = disps[i-1] + vcounts[i-1];
    }
    
  i = MPI_Allgatherv (snfpts, snfp*Pa->nsd, MPI_DOUBLE,
		      allsnfpts, vcounts, disps, MPI_DOUBLE, Pa->Comm);

  /* Free unneeded memory */
  if (snfpts)
    free(snfpts);

  time2 = MPI_Wtime();
  PetscPrintf
    (Pa->Comm,
     "interp_global_disp_ffields (1st Communication stage): %g Secs.\n",
     time2-time1);

  /* All proccesses search all points:
   * (Optimization: skip local points)
   */
  MPI_Comm_rank (Pa->Comm, &rank);
  /* Re-build disps vector (to identify local points) */
  for (i = 0; i < nprocs; i++)
    {
      if (i == 0)
	disps[i] = 0;
      else
	disps[i] = disps[i-1] + vsnfp[i-1];
    }

  for (ip = 0; ip < totsnf; ip++)
    {
      if (ip - disps[rank] >= 0 &&
	  ip - disps[rank] < snfp) /* already not found in this rank, skip */
	ri = -1;
      else
	ri = interp_local_disp_ffields (Pa, vdelta, vdeltae, sclpn, idisp,
				allsnfpts + ip * Pa->nsd, vloc, vext,
				stride, start, ncomps, 2,
				allsnfres + ip * ncomps);

      if (ri == -1) /* Not found */
	{
	  allsnfsw[ip] = 0;
	  for (i = 0; i < ncomps; i++)
	    allsnfres[ip * ncomps + i] = 0.0;
	}
      else
	{
	  allsnfsw[ip] = 1;
	}
    }

  /* Free unneeded memory */
  free(allsnfpts);
  time3 = MPI_Wtime();
  PetscPrintf
    (Pa->Comm,
     "interp_global_disp_ffields (Second search): %g Secs.\n", time3-time2);

  /* Reduce results: */
  for (i = 0; i < nprocs-1; i++)
    disps[i+1] = disps[i] + vsnfp[i];
  for (i = 0; i < nprocs; i++)              /* Proccessor "i" queries */
      MPI_Reduce (&allsnfsw[disps[i]], snfsw, vsnfp[i], MPI_INT, MPI_SUM,
		  i, Pa->Comm);
  for (i = 0; i < nprocs; i++)
    {
      vcounts[i] = vsnfp[i] * ncomps;
      if (i > 0)
	disps[i] = disps[i-1] + vcounts[i-1];
    }
  for (i = 0; i < nprocs; i++)              /* Proccessor "i" queries */
      MPI_Reduce (&allsnfres[disps[i]], snfres, vcounts[i], MPI_DOUBLE,
		  MPI_SUM, i, Pa->Comm);

  /* Free unneeded memory */
  free(allsnfsw);
  free(allsnfres);
  free(disps);
  free(vcounts);
  free(vsnfp);

  /* Compute average if found in multiple elements */
  for (ip = 0; ip < snfp; ip++)
    if (snfsw[ip] > 1)
      {
	for (i = 0; i < ncomps; i++)
	  snfres[ip*ncomps+i] /= snfsw[ip];
	snfsw[ip] = 1;
      }

  /* Distribute points found in another proccesses in local result vector */
  for (ip = 0, snfp = 0; ip < npoints; ip++)
    if (sw[ip] == -1)
      {
	if (snfsw[snfp] == 1)  /* found in another proccess, copy result */
	  {
	    for (i = 0; i < ncomps; i++)
	      results[ip * ncomps + i] = snfres[snfp * ncomps + i];
	    sw[ip] = 0;
	  }
	snfp++;
      }

  /* Free unneeded memory */
  free(snfsw);
  free(snfres);

  /* Re-count still-not-found points */
  for (ip = 0, snfp = 0; ip < npoints; ip++)
    if (sw[ip] == -1)
      snfp++;

  i = MPI_Allreduce (&snfp, &totsnf, 1, MPI_INT, MPI_SUM, Pa->Comm);

  fexternal = npoints - flocal - snfp;
  time4 = MPI_Wtime();
  PetscPrintf
    (Pa->Comm,
     "interp_global_disp_ffields (2nd Communication stage): %g Secs.\n",
     time4-time3);
  PetscSynchronizedPrintf
    (Pa->Comm,
     "interp_global_disp_ffields: looked for %d points, %d found locally, "
     "%d external, %d not found\n", npoints, flocal, fexternal, snfp);
  PetscSynchronizedFlush (Pa->Comm, stdout);

  return totsnf;
}

/*
 * point_in_element:
 * Given an element by its "nnpe" coordinates "coords", in "nsd" spatial
 * dimensions, find if the point "point" belongs to it.
 * If result is true, set the "weights" vector to the interpolation
 * weights of each node.
 * Arguments:
 * nsd(input): number of spatial dimensions
 * nnpe(input): number of points in the element (simplex: nsd+1)
 * coords(input): nodal coordinates (1...nnpe)(1...nsd)[inode * nsd + isd]
 * point(input): point coordinates (1...nsd)
 * weights(output): interpolation weights of each node (1...nnpe)
 *
 * return value: 1: point in element
 *               0: no
 */
/* Fortran Interface */
void pointinelement_ (int *nsd, int *nnpe, double *coords, double *point,
		      double *weights, int *retv)
{
  *retv = point_in_element(*nsd, *nnpe, coords, point, weights);
}
/* Implementation */
int point_in_element (int nsd, int nnpe, double *coords, double *point,
		      double *weights)
{
  if (nsd == 1)
    {
      if (nnpe == 2)     /* 1D -> Segment */
	{
	  double x1, x2;
	  x1 = -(coords[0] - point[0]);
	  x2 = coords[1] - point[0];
	  if (x1 < 0 || x2 < 0)
	    return 0;
	  weights[0] = x2 / (x1 + x2);
	  weights[1] = x1 / (x1 + x2);
	  return 1;
	}
      return 0;
    }
  if (nsd == 2)
    {
      if (nnpe == 3)     /* 2D -> Triangle */
	{
	  double x1,x2,x3, y1,y2,y3;
	  double ct1, ct2, ct3;
	  x1 = coords[0]-point[0];
	  y1 = coords[1]-point[1];
	  x2 = coords[2]-point[0];
	  y2 = coords[3]-point[1];
	  x3 = coords[4]-point[0];
	  y3 = coords[5]-point[1];
	  ct3 = x1 * y2 - y1 * x2;
	  if (ct3 < 0) return 0;
	  ct2 = x3 * y1 - x1 * y3;
	  if (ct2 < 0) return 0;
	  ct1 = x2 * y3 - x3 * y2;
	  if (ct1 < 0) return 0;
	  weights[0] = ct1 / (ct1 + ct2 + ct3);
	  weights[1] = ct2 / (ct1 + ct2 + ct3);
	  weights[2] = ct3 / (ct1 + ct2 + ct3);
	  return 1;
	}
      if (nnpe == 4)     /* 2D -> Quadrilateral */
	{
	}
      return 0;
    }
  if (nsd == 3)
    {
      if (nnpe == 4)     /* 3D -> Tetrahedra */
	{
	  double x1,x2,x3,x4, y1,y2,y3,y4, z1,z2,z3,z4;
	  double ct1, ct2, ct3, ct4;
	  x1 = coords[0]-point[0];
	  y1 = coords[1]-point[1];
	  z1 = coords[2]-point[2];
	  x2 = coords[3]-point[0];
	  y2 = coords[4]-point[1];
	  z2 = coords[5]-point[2];
	  x3 = coords[6]-point[0];
	  y3 = coords[7]-point[1];
	  z3 = coords[8]-point[2];
	  x4 = coords[9]-point[0];
	  y4 = coords[10]-point[1];
	  z4 = coords[11]-point[2];
	  ct4 = x1*(y2*z3-z2*y3) + y1*(z2*x3-x2*z3) + z1*(x2*y3-y2*x3);
	  ct4 = -ct4;
	  if (ct4 < 0) return 0;
	  ct3 = x1*(y2*z4-z2*y4) + y1*(z2*x4-x2*z4) + z1*(x2*y4-y2*x4);
	  if (ct3 < 0) return 0;
	  ct2 = x1*(y3*z4-z3*y4) + y1*(z3*x4-x3*z4) + z1*(x3*y4-y3*x4);
	  ct2 = -ct2;
	  if (ct2 < 0) return 0;
	  ct1 = x2*(y3*z4-z3*y4) + y2*(z3*x4-x3*z4) + z2*(x3*y4-y3*x4);
	  if (ct1 < 0) return 0;
	  weights[0] = ct1/(ct1+ct2+ct3+ct4);
	  weights[1] = ct2/(ct1+ct2+ct3+ct4);
	  weights[2] = ct3/(ct1+ct2+ct3+ct4);
	  weights[3] = ct4/(ct1+ct2+ct3+ct4);
	  return 1;
	}
      return 0;
    }
  return 0;
}

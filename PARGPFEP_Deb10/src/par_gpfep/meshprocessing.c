#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "meshprocessing.h"
#include "testspeed.h"
#include "partition.h"
#include "f2c_string.h"

/* User function to change element groups */
void u_set_groups (int *ngroups, int **nnpe, int **nelempgrp, int *incids);

/* This is a simple list structure to avoid using the C++ routine */
struct SList_{
  struct SList_    *next;
  int               data;
};

typedef struct SList_ SList;
void BldLocalAdjncy (int my_nnodes, int g_nnodes, int **xadj, int **adjncy,
		int *nnpe, int nelems, int *elements,
		int *particion);
void renban (int nod, int *ien, int *jen, int *nv, int *nn);
void mgasslev (int nod, int *ien, int *jen, int mglvls, int *iaux, int *mglev);

/*----------------------------------------------------------------------
    Function MeshProcessing - Partition and distribution of the mesh
    Programmed by Adrian Lew - Started: 5/11/97 - Finished: 10/11/97
    Thanks to the Flaco for his patience again, and for his good
    ideas.

    Modified 00-07: Parallel / Sequential partitioning.

    Modified 2015-06: Ghosted vectors

    This function must be called from FORTRAN. Its function is to distribute 
    the work between processors.
    This routine creates and fill the parallel partitioning structure 
    of the domain. 
    It returns the perm vector for a later renumbering of the boundary
    conditions.

    Arguments:
     Comm: (input) MPI communicator to use
     input_mesh: (input) file name containing the mesh
     mode: (input)
          *0: normal partitioning according to processes in the communicator
          *1: information multiplied (mesh in all the processors).
     nsd: (output) number of spatial dimensions
     nnpe: (output) number of nodes per element
     perm: (output) old to new numeration of the mesh.
          Only in the main processor of Comm.
     Pa: (output) partition structure pointer to return

Remember: 
     The nodes numbering begins with 1, not with 0. So for example, in 
     partition_vec[0] is the region  to which belongs node number 1.
-----------------------------------------------------------------------*/
#undef __SFUNC__
#define __SFUNC__ "meshprocessing"

void meshprocessing(MPI_Comm *fComm, char *input_mesh, 
		    int *fmode, int *fnsd, int *fnnpe, int **fperm, 
		    partition **fPa)
{
  MPI_Comm  Comm;

  int       ierr, izero = 0; /* error checking variable */
  int       i, j, k;         /* loop counters */
  int       iel;             /* element number */
  int       tmpint1, tmpint2 = 0;/* temporary int variables */
  int       tmpint3;
  int       rank, size, crank, csize; /* Communicator data */
  int       flagprt;
  int       edgecut;         /* edgecut of the partition (r)*/
  int       *d_nz;           /* array that contains number of nonzero in each
			 	row of the diagonal block */
  int       *nd_nz;          /* array that contains number of nonzero in each
				row of the extra-diagonal block */
  int       *partition_vec;  /* vector with the number indicating which
				subdomain each node belongs to (r)*/
  int       *nnodes_part;    /* array with the number of nodes in each zone 
				(r) */
  int       *nelem_part;     /* array with the number of elements in each
				zone (r) */
  double    *coordinates;    /* array with the  coordinates of the mesh 
				nodes (r)*/
  double    *coord_buf;      /* array buffer for the coordinates of the 
				mesh nodes */
  int       nnodes;          /* total number of nodes */
  int       buf_size;        /* dimension of the array elements_buf */ 
  int       *elements_buf;   /* array with the elements of the mesh, used as 
				buffer */
  int       nelem;           /* total number of elements in the mesh */
  int       ngroups;         /* Number of groups */
  int       *nelempgrp;      /* total number of elements per group */
  int       *ext_nelempgrp;  /* nelempgrp in the external mesh */
  int       *index_counter;  /* auxiliar vector for counting (r)*/
  int       *invperm = (int *) NULL; /* new to old numeration 
					permutation vector (r)*/ 
  int       *start_point;    /* starting index of each zone nodes, in the new 
			        numeration (r)*/
  int       *aux_vec;        /* All kind of usages vector (r)*/
  int       *aux_vec2;       /* All kind of usages vector 2 (r) */
  MPI_Status   status;       /* status checking variable */
  int       *Mesh_division;  /* Matrix for containing the elements of each
				region before sending them (r)*/
  int       *start_point_e;  /* starting index of each zone elements in
				the rearranged mesh (r)*/
  int       *Group_Index;
  Vec       vara;            /* auxiliary vector for getting the own range */
  int       *perm;           /* old to new numbering array */
  /* double    tmpdou;          double tmp variable */
  int       nz_entries;      /* total number of non_zero entries in a matrix 
				of 1 field per node */

  int       mode, nsd;
  int       nnpe;
  partition *Pa;
  double    *weights;
  int l, *loc_perm, *loc_aux;
  int *partition_save, *xadj, *adjncy, *iet, *jet;
  int *newconn, nelemtransf, *newGOS;
  int ighost, inode;
  int *Mesh;

  int mglevels, *loc_mglev=NULL, *glo_mglev=NULL;

  char file_name[150];
  double douaux;
  PetscBool pt_flg;

  /* Comm = *fComm; */
  /* Para openMPI hay que hacer: */
  Comm = MPI_Comm_f2c(*(MPI_Fint*)&*fComm);
  mode = *fmode;

  /* This is to avoid the warnings */
  nelem_part    = index_counter = perm         = (int *)NULL;
  Mesh_division = start_point   = start_point_e= (int *)NULL;
  weights = (double *)NULL;
  tmpint3=0;
  Group_Index = (int *)NULL;

  /* Initializing */
  MPI_Comm_rank (Comm, &crank);
  MPI_Comm_size (Comm, &csize);
  rank = crank;
  size = csize;

  if (mode == 1 && csize == 1) /* Don't do partitioning, but size = 1 anyway */
    mode = 0;                 /* ----> equivalent to normal behaviour       */

  *fPa = (partition *) malloc (sizeof (partition));
  if (*fPa == (partition *)NULL)
    error (EXIT, "Not enough memory for partition");
  Pa = *fPa;

  /*
   * Mesh reading: only one processor
   */
  if (rank == 0)
    {
      int flag = BOTH;
      int *fnnpe;
      f2c_string (input_mesh, input_mesh, 80);
      ReadMesh (input_mesh, &(Pa->nsd), &coordinates, &nnodes,
		&fnnpe, &elements_buf, &(Pa->ngroups),
		&nelempgrp, &flag);
      u_set_groups (&(Pa->ngroups), &fnnpe, &nelempgrp, elements_buf);
      if (Pa->nsd < 0)
	error(EXIT, "Unable to compute the nsd value. Please check meshfile");
      Pa->nnpe = fnnpe[0];    /* Only one nnpe value at this moment */
      free (fnnpe);
      nelem = 0;
      for (i = 0; i < Pa->ngroups; i++)
	nelem += nelempgrp[i];
    }
  else
    elements_buf = 0;
  ierr = MPI_Bcast (&nnodes, 1, MPI_INT, 0, Comm );
  ierr = MPI_Bcast (&nelem, 1, MPI_INT, 0, Comm );
  ierr = MPI_Bcast (&(Pa->nsd), 1, MPI_INT, 0, Comm );
  ierr = MPI_Bcast (&(Pa->nnpe), 1, MPI_INT, 0, Comm );
  nnpe = Pa->nnpe;
  ierr = MPI_Bcast (&(Pa->ngroups), 1, MPI_INT, 0, Comm);
  if (mode == 1)
    {
      if (rank != 0)
	{
	  coordinates = (double *) 
	    malloc(nnodes * Pa->nsd * sizeof(double));
	  if (coordinates == (double *)NULL)
	    error (EXIT,"Not enough memory to allocate coords");
	  if (PetscMalloc (nelem * nnpe * sizeof(int), &elements_buf) != 0)
	    error (EXIT, "Not enough memory for elements_buf");
	  nelempgrp = (int *) malloc (Pa->ngroups * sizeof(int));
	  if (nelempgrp == (int *)NULL)
	    error (EXIT, "Not enough memory for nelempgrp");
	}
      ierr = MPI_Bcast (coordinates, nnodes * Pa->nsd, MPI_DOUBLE, 0, Comm );
      ierr = MPI_Bcast (elements_buf, nelem * nnpe, MPI_INT, 0, Comm);
      ierr = MPI_Bcast (nelempgrp, Pa->ngroups, MPI_INT, 0, Comm);
      if (rank != 0)
	u_set_groups (&(Pa->ngroups), &fnnpe, &nelempgrp, elements_buf);
    }

  /*
   * Partitioning:
   *
   * flagprt == -1 --> do not part (broadcast mesh)
   * flagprt == 0 ---> uniform (parallel)
   * flagprt == 1 ---> read from file
   * flagprt == 2 ---> nonuniform (seq)
   */
  if (mode == 1)
    {
      flagprt = -1;
      size = 1;
      rank = 0;
    }
  if (rank == 0)
    {
      start_point = (int *) malloc (size * sizeof(int));
      if (!start_point)
	error (EXIT, "Not enough memory for start_point");

      /*    f2c_string (output_file, output_file, 80); */
      /* partition file option (flagprt == 0 ---> uniform (parallel) */
      /* partition file option (flagprt == 1 ---> read from file */
      /* partition file option (flagprt == 2 ---> nonuniform (seq) */
      if (mode != 1)
	{
	  flagprt = 0;
	  ierr = PetscOptionsGetString
	    (NULL, "pgpfep_","-partfromfile", file_name, 149,
	     &pt_flg);                    CHKERRABORT(PETSC_COMM_WORLD,ierr);
	  if (pt_flg == PETSC_TRUE)
	    flagprt = 1;
	  else
	    {
	      ierr = PetscOptionsHasName
		(NULL, "pgpfep_","-partnonuniform",
		 &pt_flg);                CHKERRABORT(PETSC_COMM_WORLD,ierr);
	      if (pt_flg == PETSC_TRUE)
		flagprt = 2;
	    }
	}
    }
  if (flagprt == -1)
    {
      size = csize;
      rank = crank;
    }
  
    ierr = MPI_Bcast (&flagprt, 1, MPI_INT, 0, Comm );

  if (flagprt == 0)
    {               /* Parallel partitioning */
      if (rank != 0)
	{
	  if (PetscMalloc (nelem * nnpe * sizeof(int), &elements_buf) != 0)
	    error (EXIT, "Not enough memory for elements_buf");
	}
      ierr = MPI_Bcast (elements_buf, nelem * nnpe, MPI_INT, 0, Comm);
      ParMeshPartition (Comm, size, rank, 0,
			Pa->nsd, nnodes, nelem, Pa->nnpe, Pa->ngroups,
			nelempgrp, coordinates, elements_buf,
			&edgecut, &nnodes_part, &partition_vec,
			&d_nz, &nd_nz);
    }
  else         /* Possibly partitions with different size */
    {          /* sequential partitioning or read from file */
      if (flagprt == -1 || rank != 0)
	{
	  partition_vec = (int *) malloc (sizeof(int) * nnodes);
	  if (partition_vec  == (int *) NULL)
	    error (EXIT, "Not enough memory for partition_vec");
	}
      if (flagprt == -1) // All processors call MeshPartition w/size = 1
	{
	  MeshPartition (2, weights, 1,
			 Pa->nsd, nnodes, Pa->nnpe, Pa->ngroups,
			 nelempgrp, coordinates, elements_buf,
			 &edgecut, &nnodes_part, &partition_vec,
			 &d_nz, &nd_nz);
	}
      if (flagprt == 2) // Non-uniform partitioning: compute weights
	{
	  i = 150; j = 1; k = 2; douaux = 0.1;
	  speedsearch (&i, &douaux, &j, &k, &weights, &izero);
	  if (rank == 0)
	    {
	      printf("\n Relative Speed computed in each processor:\n");
	      for (i = 0; i < size; i++)
		printf (" Processor %3d: \t%g\n", i, weights[i]);
	    }
	}
      if (flagprt == 1 || flagprt == 2) // Sequential partitioning or read
	{
	  if (rank == 0)
	    MeshPartition (flagprt, weights, size,
			   Pa->nsd, nnodes, Pa->nnpe, Pa->ngroups,
			   nelempgrp, coordinates, elements_buf,
			   &edgecut, &nnodes_part, &partition_vec,
			   &d_nz, &nd_nz);
	  ierr = MPI_Bcast (partition_vec, nnodes, MPI_INT, 0, Comm);
	}
    }

  if (rank == 0 && mode != 1)
    {
      PetscOptionsGetString (NULL, "pgpfep_", "-prtpartfile",
			     file_name, 149, &pt_flg);
      if (pt_flg == PETSC_TRUE)
	SavePartition (file_name, partition_vec, nnodes, size, edgecut);
    }
  PetscPrintf(Comm, "\n Mesh partition information:\n");
  PetscPrintf(Comm, "\tNumber of nodes: \t%d\n", nnodes);
  PetscPrintf(Comm, "\tNumber of elements: \t%d\n", nelem);
  PetscPrintf(Comm, "\tPartition edgecut: \t%d\n", edgecut);
  /*  for(i = 0; i < size; i++)
    printf("\tNumber of nodes in region %d: \t%d\n",
	   i, nnodes_part[i]);
  PetscPrintf(Comm, "\n"); */

  MPI_Barrier (Comm);  /* The idle processors wait the main one */

  if (flagprt == -1) // Don't do partitioning: local nodes = global nodes
    Pa->nodloc = nnodes;
  else               // "main" process inform about local nodes
    ierr = MPI_Scatter
      (nnodes_part, 1, MPI_INT, &(Pa->nodloc), 1, MPI_INT, 0, Comm);
  PetscSynchronizedPrintf(Comm, "\tNumber of nodes in region %d: \t%d\n",
			  rank, Pa->nodloc);
  PetscSynchronizedFlush (Comm, stdout);
  PetscPrintf(Comm, "\n");

  Pa->Comm = (flagprt == -1 ? PETSC_COMM_SELF : Comm);
  Pa->nodtot = nnodes;
  Pa->nelemtot = nelem;
  //
  // Create vector, on Pa->Comm
  // (distributed or "multiplied" if Pa->Comm == PETSC_COMM_SELF)
  //
  ierr = VecCreateMPI (Pa->Comm, Pa->nodloc, Pa->nodtot,
		       &vara);         CHKERRABORT(Pa->Comm, ierr);

  ierr = VecGetOwnershipRange
    (vara, &(Pa->first), &(Pa->last)); CHKERRABORT(Pa->Comm, ierr);

  if (flagprt == -1)   // Main processor gets all start points
    start_point[0] = 0;
  else
    ierr = MPI_Gather
      (&(Pa->first), 1, MPI_INT, start_point, 1, MPI_INT, 0, Comm);

  /* Store in Pa first and last nodes in "fortran" style numbering */
  Pa->first = (Pa->first) + 1;
  Pa->last = (Pa->last) + 1;
  nsd = *fnsd  = Pa->nsd;
  nnpe= *fnnpe = Pa->nnpe;
  ngroups      = Pa->ngroups;

  /* Alloc space for storing non-zeros in Pa structure */
  Pa->d_nz = (int *) malloc((Pa->nodloc)*sizeof(int));
  Pa->nd_nz = (int *) malloc((Pa->nodloc)*sizeof(int));
  if(!Pa->d_nz || !Pa->nd_nz)
    error(EXIT, "Not enough memory for Pa->d_nz || Pa->nd_nz");

  if (PetscMalloc(ngroups * sizeof(int), &ext_nelempgrp) != 0)
    error(EXIT, "Not enough memory for ext_nelemgrp");

  /* LOCAL renumbering and multigrid assignments */
  /* Build LOCAL adjacency */
  /* partition_vec will be used as auxiliary vector
   * after that only will be needed in main processor,
   * or in all processors if flagprt = -1
   */
  if (flagprt == -1)
    {
      size = 1;
      rank = 0;
    }
  if (rank == 0) {
    partition_save = (int *) malloc(nnodes*sizeof(int));
    if (partition_save == (int *)NULL)
      error (EXIT, "Not enough memory for partition_save");
    for (i = 0; i < nnodes; i++) {
      partition_save[i] = partition_vec[i];
    }
  }
  else
    partition_save = (int *) NULL;

  /* Use partition_vec as mapping global->local, -1 means non-local */
  j = 0;
  for (i = 0; i < nnodes; i++)
    {
      if (partition_vec[i] == rank)
	partition_vec[i] = j++;
      else
	partition_vec[i] = -1;
    }
  if (flagprt == -1)
    {
      size = csize;
      rank = crank;
    }

  if (flagprt >= 0)
  /* Broadcast connectivities !!! */
    {
      if (rank != 0)
	{
	  if (PetscMalloc (nelem * nnpe * sizeof(int), &elements_buf) != 0)
	    error (EXIT, "Not enough memory for elements_buf");
	}
      tmpint1 = nelem * nnpe;
      ierr = MPI_Bcast (elements_buf, tmpint1, MPI_INT, 0, Comm);
    }

  BldLocalAdjncy (Pa->nodloc, nnodes, &xadj, &adjncy,
		  &nnpe, nelem, elements_buf,
		  partition_vec);

  if (partition_save)
    {
      for(i = 0; i < nnodes; i++)
	partition_vec[i] = partition_save[i];
      free (partition_save);
    }
  if (flagprt != -1 && rank > 0)
    {
      free (partition_vec);
      PetscFree (elements_buf);
    }

  loc_perm = (int *) malloc(Pa->nodloc*sizeof(int));
  loc_aux  = (int *) malloc(Pa->nodloc*sizeof(int));
  if(!loc_perm || !loc_aux)
    error(EXIT, "Not enough memory for loc_perm || loc_aux");

  PetscOptionsHasName (NULL, "pgpfep_", "-locren", &pt_flg);
  if (pt_flg == PETSC_TRUE)
    renban (Pa->nodloc, xadj, adjncy, loc_aux, loc_perm);
  else
    {
      /* loc_perm: identity */
      for (i = 0; i < Pa->nodloc; i++)
	loc_perm[i] = i;
    }

  if (flagprt == -1)
    {
      size = 1;
      rank = 0;
    }
  /* Generate the permutation vector for renumbering */  
  if (rank == 0)
    {
      perm = (int *) malloc(nnodes*sizeof(int));
      if (perm == (int *)NULL)
	error(EXIT, "Not enough memory for perm");
    
      invperm = (int *) malloc(nnodes*sizeof(int));
      if (invperm == (int *)NULL)
	error(EXIT, "Not enough memory for invperm");
    }

  /* Join loc_perm in main processor */
  if (flagprt == -1)
    for (i = 0; i < Pa->nodloc; i++)
      invperm[i] = loc_perm[i];
  else
    ierr = MPI_Gatherv(loc_perm, Pa->nodloc, MPI_INT,
		       invperm, nnodes_part, start_point, MPI_INT, 0, Comm);
  free (loc_perm);

  if (rank == 0)
    {
      index_counter = (int *) malloc((1+size)*sizeof(int)); 
      if (index_counter == (int *)NULL) /*  defined 1+size for a later usage */
	error (EXIT, "Not enough memory for index_counter");

      for (i = 0; i < size; i++) 
	index_counter[i] = 0;

      /* renumbering */
      for (i = 0; i < nnodes; i++)
	{
	  k = partition_vec[i]; /* processor to whom i belongs */
	  l = index_counter[k] + start_point[k];
	  perm[i] = invperm[l] + start_point[k];
	  index_counter[k]++;
	}

      /* S�lo para visualizar la renumeraci�n: */
      PetscOptionsGetString (NULL, "pgpfep_", "-prtlocrenfile", file_name, 149,
			     &pt_flg);
      if (pt_flg == PETSC_TRUE && crank == 0)
	{
	  for(i=0; i<nnodes; i++)
	    {
	      k = partition_vec[i];
	      invperm[i] = perm[i] - start_point[k] + 1000000 * k;
	    }
	  SavePartition(file_name, invperm, nnodes, size, edgecut);
	}
      /* End S�lo para ... */

      for(i=0; i<nnodes; i++)
	invperm[perm[i]] = i;
    }

  /* Now compute nodes in each multigrid level */
  PetscOptionsGetInt (NULL, "mg_", "-levels", &mglevels, &pt_flg);
  if (pt_flg == PETSC_TRUE)
    {
      loc_mglev = (int *) malloc(Pa->nodloc*sizeof(int));
      if(!loc_mglev)
	error (EXIT, "Not enough memory for loc_mglev");
      mgasslev (Pa->nodloc, xadj, adjncy, mglevels, loc_aux, loc_mglev);

      /*
       *  Reorder loc_mglev
       */
      if (rank == 0)
	{
	  glo_mglev = (int *) malloc(nnodes * sizeof(int));
	  if(!glo_mglev)
	    error (EXIT, "Not enough memory for glo_mglev");
	}
      /* use invperm as auxiliary vector */
      if (flagprt == -1)
	for (i = 0; i < Pa->nodloc; i++)
	  glo_mglev[i] = loc_mglev[i];
      else
	{
	  ierr = MPI_Gatherv(loc_mglev, Pa->nodloc, MPI_INT,
			     invperm, nnodes_part, start_point, MPI_INT,
			     0, Comm);
	  if (rank == 0)
	    {
	      tmpint2 = 0;
	      for (i = 0; i < size; i++) 
		index_counter[i] = 0;
	      
	      for (i = 0; i < nnodes; i++)
		{
		  k = partition_vec[i]; /* processor to whom i belongs */
		  l = index_counter[k] + start_point[k];
		  glo_mglev[i] = invperm[l];
		  if (glo_mglev[i] > 0)
		    tmpint2++;
		  index_counter[k]++;
		}
	    }
	}

      /* Visualize level assignment: */
      PetscOptionsGetString (NULL, "mg_","-prtlevelsfile", file_name, 149,
			     &pt_flg);
      if (pt_flg == PETSC_TRUE && crank == 0)
	SavePartition (file_name, glo_mglev, nnodes, nnodes, tmpint2);

      if (rank == 0) /* Now reorder */
	{
	  int *aux_mglev = invperm;
	  for (i = 0; i < nnodes; i++)
	    aux_mglev[i] = glo_mglev[i];
	  for (i = 0; i < nnodes; i++)
	    glo_mglev[perm[i]] = aux_mglev[i];
	}
      if (flagprt != -1)
	ierr = MPI_Scatterv(glo_mglev, nnodes_part, start_point, MPI_INT,
			    loc_mglev, Pa->nodloc, MPI_INT, 0, Comm);

      if (rank == 0)
	{
	  /* Restore invperm to its original values */
	  for (i = 0; i < nnodes; i++)
	    invperm[perm[i]] = i;
	  free (glo_mglev);
	}
      if (flagprt == -1)
	{
	  size = csize;
	  rank = crank;
	}
    }
  else
    mglevels = 1;

  free (loc_aux);
  PetscFree (xadj);
  PetscFree (adjncy);

  /*
   * Computing the number of nonzero entries in the matrix for 1 field per node
   */
  if (flagprt == -1)
    {
      size = 1;
      rank = 0;
    }
  if (rank == 0)
    {
      nz_entries = 0;
      for (i = 0; i < nnodes; i++)
	nz_entries += d_nz[i] + nd_nz[i];
    }
  else nz_entries = 0;
  PetscPrintf(Comm, " Nonzero entries for a one field per node matrix: %d\n",
	      nz_entries);

  if (rank == 0)
    {
      /* Sending the Sparsity Structure to each processor */
      for (i = 0; i < size; i++){
	if (i != 0)
	  {
	    aux_vec = (int *) malloc(nnodes_part[i]*sizeof(int));
	    if(aux_vec==(int *)NULL)
	      error(EXIT, "Not enough memory for aux_vec");

	    for(j=0; j<nnodes_part[i]; j++)
	      aux_vec[j]=d_nz[invperm[start_point[i]+j]];

	    ierr = MPI_Send( aux_vec, nnodes_part[i], MPI_INT, i, 2*i, Comm);

	    for(j=0; j<nnodes_part[i]; j++)
	      aux_vec[j]=nd_nz[invperm[start_point[i]+j]];

	    ierr = MPI_Send(aux_vec, nnodes_part[i], MPI_INT, i,2*i+1, Comm);

	    free(aux_vec);
	  }
	else
	  {
	    for(j=0; j<nnodes_part[i]; j++)
	      Pa->d_nz[j] = d_nz[invperm[start_point[i]+j]];

	    for(j=0; j<nnodes_part[i]; j++)
	      Pa->nd_nz[j] = nd_nz[invperm[start_point[i]+j]];
	  }
      }
      free (d_nz);
      free (nd_nz);
      free (invperm);
    }
  else  /* rank != 0 */
    {   /* Receiving sparsity structure from main processor */
      ierr = MPI_Recv(Pa->d_nz, (Pa->nodloc), MPI_INT, 0, 2*rank,
		      Comm, &status);
      PetscPrintf(MPI_COMM_SELF, "     Diagonal Sparsity Structure"
		  " transfer from proc.  %d to proc. %d succeded \n",
		  0, rank);

      ierr = MPI_Recv(Pa->nd_nz, (Pa->nodloc), MPI_INT, 0, (2*rank+1),
		      Comm, &status);
      PetscPrintf(MPI_COMM_SELF," Non-Diagonal Sparsity Structure"
		  "transfer from proc.  %d to proc. %d succeded \n",
		  0, rank);
    }

  /* Begin element treatment */
  if (flagprt == -1)   /* no partition: simulate one processor */
    {
      rank = 0;
      size = 1;
    }
  if (rank == 0)
    {
      nelem_part = (int *) malloc ((1 + size) * sizeof(int));
      if (nelem_part == (int *)NULL)
	error(EXIT, "Not enough memory for nelem_part");

      for (i = 0; i <= size; i++) /* nelem_part[i]: elements in partition i */
	nelem_part[i] = 0;    /* nelem_part[size]: # of interface  elements */

      aux_vec2 = (int *) malloc (nelem * sizeof(int)); 
      if (!aux_vec2)
	error (EXIT, "Not enough memory for aux_vec2");

      Group_Index = (int *) malloc (ngroups * size * sizeof(int));
      if (Group_Index == (int *) NULL)
	error (EXIT, "Not enough memory for Group_Index");

      /* First loop: find number of elements per region */     
      iel = 0;
      for (i = 0; i < ngroups; i++)
	{
	  ext_nelempgrp[i] = 0;

	  for (j = 0; j < size; j++)
	    Group_Index[i + j * ngroups] = 0;

	  for (j = 0; j < nelempgrp[i]; j++, iel++)
	    {
	      tmpint1 = iel * nnpe;
	      tmpint2 = 1;
	      for (k = 1; k < nnpe && tmpint2; k++)
		if (partition_vec[elements_buf[tmpint1 + k - 1] - 1]!= 
		    partition_vec[elements_buf[tmpint1 + k] - 1])
		  tmpint2 = 0;
	      if (tmpint2) /* Element with all nodes in the same partition */
		{
		  nelem_part[partition_vec[elements_buf[tmpint1] - 1]]++;
		  aux_vec2[iel] = partition_vec[elements_buf[tmpint1] - 1];
		  Group_Index[aux_vec2[iel] * ngroups + i]++;
		}
	      else        /* Interface element */
		{
		  nelem_part[size]++;
		  aux_vec2[iel] = size;
		  ext_nelempgrp[i]++;
		}
	    }        /* end loop over elements of the group */
	}         /* end loop over groups */

      /* Mesh_division: reordered connectivities */
      Mesh_division = (int *) malloc (nnpe * nelem * sizeof(int));
      if (Mesh_division == (int *) NULL)
	error(EXIT, "Not enough memory for Mesh_division");
    
      start_point_e = (int *) malloc ((1 + size) * sizeof(int));
      if (start_point_e == (int *) NULL)
	error(EXIT, "Not enough memory for start_point_e");

      index_counter[0] = 0;
      start_point_e[0] = 0;
      for (i = 1; i <= size; i++)
	{
	  start_point_e[i] = start_point_e[i - 1] + nelem_part[i - 1]; 
	  index_counter[i] = 0;
	}

    /* Second loop for rearranging the elements and renumbering their nodes */
      for (i = 0; i < nelem; i++)
	{
	  tmpint1 = aux_vec2[i];
	  tmpint2 = nnpe * (start_point_e[tmpint1] + index_counter[tmpint1]);
	  tmpint3 = i * nnpe;

	  index_counter[tmpint1]++;
	  for(j=0; j<nnpe; j++)
	    Mesh_division[tmpint2+j] = perm[elements_buf[tmpint3+j] - 1] + 1;
	}
      free(aux_vec2);
      PetscFree(elements_buf);
      buf_size=nelem_part[size]; /* Number of "inter-process" elements */
      free(partition_vec);
    }

  if (flagprt == -1)
    {
      size = csize;
      rank = crank;
      Pa->nelemloc = nelem;
      buf_size = 0;
    }
  else
    {
      /* Distribute the data: # of local elements, # of interface elements */
      ierr = MPI_Scatter(nelem_part, 1, MPI_INT, &(Pa->nelemloc), 1, MPI_INT,
			 0, Comm);
      ierr = MPI_Bcast(&buf_size, 1, MPI_INT, 0, Comm);
    }
  Pa->Mesh = (int *) malloc (nnpe * (Pa->nelemloc) * sizeof(int));
  if (Pa->Mesh==(int *)NULL)
    error(EXIT, "Not enough memory for Pa->Mesh");

  /* Now elements_buf is used for the interfacial elements */
  if (PetscMalloc (nnpe * (buf_size==0 ? 1 : buf_size)*sizeof(int),
  	           &elements_buf) != 0)
    error (EXIT, "Not enough memory for interface elements");

  if (flagprt == -1)
    {
      size = 1;
      rank = 0;
    }
  if (rank == 0)
    {
      tmpint2 = nnpe * buf_size;
      tmpint3 = nnpe * start_point_e[size];
      for (i = 0; i < tmpint2; i++)
	elements_buf[i] = Mesh_division[tmpint3 + i];
    
      for (i = 0; i < size; i++)
	{
	  if (i != 0)
	    {
	      tmpint2 = nnpe * start_point_e[i];
	      ierr = MPI_Send (&Mesh_division[tmpint2], nnpe*nelem_part[i],
			       MPI_INT, i, i, Comm);
	    }
	}
      /* copying the local elements, global, fortran numeration */
      for (i = 0; i < nnpe * Pa->nelemloc; i++)
	Pa->Mesh[i] = Mesh_division[i];
      // start_point_e[0] vale 0 !
      // Pa->LocMesh[i] = Mesh_division[nnpe * start_point_e[0] + i];
    }
  else
    {
      nelempgrp = (int *) malloc (ngroups * sizeof(int));
      if (nelempgrp == (int *)NULL)
	error (EXIT, "Not enough memory for nelempgrp");

      /* receive the local elements, global, fortran numeration */
      ierr = MPI_Recv (Pa->Mesh, nnpe * (Pa->nelemloc), MPI_INT,
		       0, rank, Comm, &status);
      PetscPrintf (MPI_COMM_SELF,
		   "     Elements transfer from proc.  %d"
		   " to proc. %d succeded \n", 0, rank);
    }

  PetscPrintf (Comm, "\n");
  if (flagprt == -1)
    {
      size = csize;
      rank = crank;
    }
  else
    {
      /* All interface elements to all processors */
      ierr = MPI_Bcast (elements_buf, nnpe * buf_size, MPI_INT, 0, Comm); 
      ierr = MPI_Scatter (Group_Index, ngroups, MPI_INT,
			  nelempgrp, ngroups, MPI_INT, 0, Comm);
      ierr = MPI_Bcast (ext_nelempgrp, ngroups, MPI_INT, 0, Comm);
    }
  /* PetscPrintf (MPI_COMM_SELF, "%d: Interface elements broadcasted \n", rank); */

  if (flagprt == -1 || rank == 0)
    {
      free (start_point_e);
      free (nelem_part);
      free (Mesh_division);
    }

  coord_buf = (double *) malloc (nnodes * nsd * sizeof(double));
  if (coord_buf == (double *)NULL)
    error (EXIT, "Not enough memory for coord_buf");

  /* Reorder the coordinates of the nodes */
  if (flagprt == -1 || rank == 0)
    {
      for (i = 0; i < nnodes; i++)
	{
	  tmpint1 = perm[i] * nsd;
	  tmpint2 =      i  * nsd;
	  for (j = 0; j < nsd; j++)
	    coord_buf[tmpint1 + j] = coordinates[tmpint2 + j];	
	}
      free (coordinates);
      /* seting indexperm */
      *fperm = perm;    
    }
  /* PetscPrintf (MPI_COMM_SELF, "%d: Coordinates reordered \n",rank); */

  /*
   * Count interface elements and ghost nodes
   */
  aux_vec = (int *) malloc(nnodes*sizeof(int));
  if (aux_vec==(int *)NULL)
    error(EXIT, "Not enough memory for aux_vec");

  aux_vec2 = (int *) malloc(buf_size*sizeof(int));
  if (aux_vec2==(int *)NULL)
    error(EXIT, "Not enough memory for aux_vec2");

  for (i = 0; i < nnodes; i++) /* aux_vec is used now for saving if an */
    aux_vec[i]=0;          /* external node has been inserted yet, and */
                           /* the position where it went (< 0) */

  /* Loop looking for boundary elements with nodes in this process */
  Pa->nodghost = 0;
  Pa->nelemext = 0;
  for (i = 0; i < buf_size; i++){
    aux_vec2[i] = 0;       /* aux_vec2 is used like a logic vector, */
                           /* having a 1 if the element is a boundary one */
    tmpint1 = i * nnpe;
    for (j = 0; j < nnpe; j++) { /* Try to find a node in this process */
      if (elements_buf[tmpint1+j] >= Pa->first &&
	  elements_buf[tmpint1+j] <  Pa->last    ) { /* Found ! */
	j = nnpe;                         /* force exit of the outer loop */
	aux_vec2[i]=1;
	Pa->nelemext++;
	for(k=0; k<nnpe; k++) {
	  if(aux_vec[elements_buf[tmpint1+k]-1] == 0) {
	    if(elements_buf[tmpint1+k] <  Pa->first ||
	       elements_buf[tmpint1+k] >= Pa->last    ) {
	      aux_vec[elements_buf[tmpint1+k]-1]=-(Pa->nodghost)-1;
	      /* aux_vec[elements_buf[tmpint1+k]-1]=Pa->nodloc+Pa->nodghost; */
	      Pa->nodghost++;
	    }
	    else
	      aux_vec[elements_buf[tmpint1+k]-1] =
		elements_buf[tmpint1+k] - Pa->first + 1;
	  }
	}
      }
    }
  }
  /* PetscPrintf (MPI_COMM_SELF, "%d: Counted nodghost %d & nelemext %d\n", */
  /* 	       rank, Pa->nodghost, Pa->nelemext); */
  /* aux_vec: -1 -2... if ghost node
   *          local, Fortran indexing otherwise
   */

  /*
   * Broadcasts coordinates !
   * then select only local coords.
   * This cuold have been done with a scatter, but coord_buf is useful
   * for setting cogeoext.
   */
  if (flagprt != -1)
    ierr = MPI_Bcast (coord_buf, nsd * nnodes, MPI_DOUBLE, 0, Comm); 

  Pa->cogeo = (PetscScalar *) malloc ((Pa->nodloc + Pa->nodghost) * nsd *
					 sizeof(PetscScalar));
  if (Pa->cogeo == (PetscScalar *)NULL)
    error (EXIT, "Not enough memory for Pa->cogeo");

  /* copy local coordinates */
  for (i = 0; i < Pa->nodloc; i++)
    {
      tmpint1 = ((Pa->first - 1) + i) * nsd;
      tmpint2 =                    i  * nsd;
      for (j = 0; j < nsd; j++)
	Pa->cogeo[tmpint2 + j] = coord_buf[tmpint1 + j];
    }
  /* PetscPrintf (MPI_COMM_SELF, "%d: %d local coordinates copied\n", */
  /* 	       rank, Pa->nodloc); */
  /* copy ghost coordinates */
  for (i = 0; i < buf_size; i++){
    if (aux_vec2[i] == 1)
      for (j = 0; j < nnpe; j++) { /* Try to find a ghost node */
	ighost = aux_vec[elements_buf[i*nnpe+j]-1];
	if (ighost < 0) {
	  ighost = -ighost -1;
	  tmpint1 = (elements_buf[i*nnpe+j]-1) * nsd;
	  tmpint2 = (Pa->nodloc + ighost) * nsd;
	  for (k = 0; k < nsd; k++)
	    Pa->cogeo[tmpint2 + k] = coord_buf[tmpint1 + k];
	  //	  aux_vec[elements_buf[tmpint1+j]-1] = ????????;
	}
      }
  }
  /* PetscPrintf (MPI_COMM_SELF, "%d: %d ghost coordinates copied\n", */
  /* 	       rank, Pa->nodghost); */
  
  /* PetscPrintf (MPI_COMM_SELF, "%d: Built full Pa->cogeo\n", */
  /* 	       rank); */

  /* Alloc Group & Ownership vector */
  Pa->GOS = (int *) malloc((Pa->nelemloc + Pa->nelemext)*sizeof(int));
  if(Pa->GOS == (int *)NULL)
    error(EXIT, "Not enough memory for Pa->GOS");

  /* Setting local part of Group & Ownership vector */
  k = 0;
  for (i = 1; i <= ngroups; i++)
    for (j = 0; j < nelempgrp[i-1]; j++)
      Pa->GOS[k++] = i;

    /* Alloc for full connectivity (Local and External mesh) */
  Mesh = (int *) malloc ((Pa->nelemloc + Pa->nelemext) * nnpe * sizeof(int));
  if (Mesh==(int *)NULL)
    error(EXIT, "Not enough memory for Mesh");

  /* change index from global, FORTRAN indexing to local, "C" indexing */
  for (i = 0; i < Pa->nelemloc * nnpe; i++)
    Mesh[i] = Pa->Mesh[i] - Pa->first;
  free(Pa->Mesh);
  Pa->Mesh = Mesh;

  /* Set Groups and Ownership vector, */
  iel = 0;
  tmpint2 = Pa->nelemloc;
  for (i = 0; i < ngroups; i++)
    for (j = 0; j < ext_nelempgrp[i]; j++, iel++) {
      if (aux_vec2[iel] == 1) {
	tmpint3 = nnpe * iel;
	if (elements_buf[tmpint3] >= Pa->first &&
	    elements_buf[tmpint3] < Pa->last)
	  Pa->GOS[tmpint2] = (i+1);
	else
	  Pa->GOS[tmpint2] = -(i+1);

	tmpint1 = nnpe * tmpint2;
	for (k = 0; k < nnpe; k++)
	  {
	    inode = aux_vec[elements_buf[tmpint3+k]-1];
	    if (inode < 0) /* Ghost node */
	      inode = -inode -1 + Pa->nodloc;
	    else
	      inode = inode - 1;
	    Pa->Mesh[tmpint1+k] = inode;
	  }
	tmpint2++;
      }
    }
  
  PetscFree(ext_nelempgrp);
  PetscFree(elements_buf);
  free(aux_vec2);

  Pa->Glob = (int *) malloc((Pa->nodghost)*sizeof(int));
  if (Pa->Glob == (int *)NULL)
    error(EXIT, "Not enough memory for Pa->Glob");

  for (i = 0; i < nnodes; i++) {
    if (aux_vec[i] < 0) {
      tmpint1 = (-(aux_vec[i])-1);
      Pa->Glob[tmpint1] = i;
    }
  }

  /* Setting MeshKind */
  Pa->MeshKind = ALLOC_COORD_MASK;

  /* Local element renumbering */
  /* Local incidence (+ ghost mesh): */
  /* Pa->(nodloc+nodghost) x Pa->(nelemloc+nelemext)
   * (Pa->Mesh[nnpe*Pa->(nelemloc+nelemext)]) */
  /* First step: transpose conectivity */
  iet = (int *) malloc ((Pa->nodloc + Pa->nodghost + 1) * sizeof(int));
  jet = (int *) malloc (nnpe*(Pa->nelemloc+Pa->nelemext) * sizeof(int));
  if (!iet || !jet)
    error(EXIT, "Not enough memory for transpose connectivity");
  for (i = 0; i <= Pa->nodloc + Pa->nodghost; i++)
    iet[i] = 0;
  /* First loop: count elements containing node, store in node_position+2 */
  k = 0;
  for (i = 0; i < Pa->nelemloc + Pa->nelemext; i++)
    {
      for (j = 0; j < nnpe; j++)
	{
	  l = Pa->Mesh[k++];
	  if (l+2 <= Pa->nodloc + Pa->nodghost)
	    iet[l+2]++;
	}
    }
  /* Second loop: convert counters in pointers */
  for (i = 1; i <= Pa->nodloc + Pa->nodghost; i++)
    iet[i] += iet[i-1];
  /* Third loop: store elements, use pointers in node_position+1) */
  k = 0;
  for (i = 0; i < Pa->nelemloc + Pa->nelemext; i++)
    {
      for (j = 0; j < nnpe; j++)
	{
	  l = Pa->Mesh[k++];
	  jet[iet[l+1]] = i;
	  iet[l+1]++;
	}
    }

/* Gancho para ver renumeraci�n de elementos !!!!!!!!! */
  {
  char faux[150], *pch;
  double xx, yy;
  FILE *fpaux = NULL;
  int flgprtlocren;
  PetscOptionsGetString (NULL, "pgpfep_","-prtlocrenfile", faux, 149, &pt_flg);

  if (pt_flg == PETSC_TRUE)
    {
      pch = faux + strlen(faux);
      flgprtlocren = 1;
      sprintf (pch, "_%d", rank);
      fpaux = fopen (faux, "w");
    }
  else
    flgprtlocren = 0;

  /* Second step: rewrite connectivity ordering by lower node # */
  newconn = (int *) malloc (nnpe*(Pa->nelemloc+Pa->nelemext) * sizeof(int));
  newGOS = (int*) malloc ((Pa->nelemloc+Pa->nelemext) * sizeof(int));
  if (!newconn || !newGOS)
    error(EXIT, "Not enough memory for newconn || newGOS");
  nelemtransf = 0;
  for (i = 0; i < Pa->nodloc+Pa->nodghost &&
	 nelemtransf < Pa->nelemloc + Pa->nelemext; i++)
    {                                   /* Loop over nodes */
      for (j = iet[i]; j < iet[i+1]; j++)
	{                               /* Loop over elements sharing node */
	  l = jet[j];                   /* l is the element */
	  if (Pa->Mesh[l*nnpe] >= 0) /* not yet transferred */
	    {
	      xx = 0; yy = 0;
	      for (k = 0; k < nnpe; k++) /* transfer to new connectivity */
		{
		  newconn[nnpe*nelemtransf+k] =
		    Pa->Mesh[nnpe*l+k];
		  tmpint1 = Pa->Mesh[nnpe*l+k];
		  xx += Pa->cogeo[tmpint1*nsd];
		  yy += Pa->cogeo[tmpint1*nsd+1];
		}
	      if (flgprtlocren && fpaux)
		fprintf (fpaux, "%g %g\n", xx/nnpe, yy/nnpe);
	      newGOS[nelemtransf]=Pa->GOS[l];
	      nelemtransf++;
	      Pa->Mesh[l*nnpe] = -1; /* mark as transferred */
	    }
	}
    }
  if (flgprtlocren && fpaux)
    fclose(fpaux);
  }
  free(iet);
  free(jet);
  free(Pa->Mesh);
  Pa->Mesh = newconn;
  free(Pa->GOS);
  Pa->GOS = newGOS;

  /* Print elements partition information */
  PetscSynchronizedPrintf(Comm,
	      " Total number of elements in processor %d:  %d\n",
	      rank, Pa->nelemloc+Pa->nelemext);
  PetscSynchronizedFlush (Comm, stdout);
  PetscPrintf(Comm,"\n");

  free(aux_vec);
  free(coord_buf);
  free(nelempgrp);
  /* Free the allocated memory in the main processor */
  if (flagprt == -1 || rank == 0)
    {
      free (index_counter);
      free (nnodes_part);
      free (start_point);
      free (Group_Index);
    }

  PetscOptionsGetInt (NULL, "mg_", "-levels", &mglevels, &pt_flg);
  if (pt_flg == PETSC_TRUE)
    {
      /*
       *  Multigrid handling: Transfer assigned levels from neighboring
       *  processors
       */
      /* IS IS_aux1, IS_aux2; */

      /* /\* Create scatter-gather context *\/ */
      /* Vec vext; */
      /* VecScatter GLscat; */
      /* PetscScalar *vp; */
      /* ierr = ISCreateGeneral(MPI_COMM_SELF, Pa->nodghost, Pa->Glob, */
      /* 			     PETSC_COPY_VALUES, &IS_aux1); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
      /* ierr = ISCreateStride (MPI_COMM_SELF, Pa->nodghost, */
      /* 			     0, 1, &IS_aux2);     CHKERRABORT(PETSC_COMM_WORLD,ierr); */
      /* ierr = VecCreateSeq (MPI_COMM_SELF, Pa->nodghost, &vext); */
      /* ierr = VecScatterCreate (vara, IS_aux1, vext, IS_aux2, &GLscat); */
      /* CHKERRABORT(PETSC_COMM_WORLD,ierr); */

      /* /\* Store mg level in vara *\/ */
      /* ierr = VecGetArray (vara, &vp); CHKERRABORT(PETSC_COMM_WORLD,ierr); */
      /* for (i = 0; i < Pa->nodloc; i++) */
      /* 	vp[i] = loc_mglev[i]; */
      /* ierr = VecRestoreArray (vara, &vp); */
      /* /\* Get neighboring values *\/ */
      /* ierr = VecScatterBegin (GLscat, vara, vext, */
      /* 			      INSERT_VALUES, SCATTER_FORWARD); */
      /* ierr = VecScatterEnd   (GLscat, vara, vext, */
      /* 			      INSERT_VALUES, SCATTER_FORWARD); */
      /* /\* Aqui habr�a que armar el proyector, su traspuesto y la matriz coarse *\/ */

      /* /\* *\/ */
    }

  if (mglevels > 1)
    free (loc_mglev);
  VecDestroy(&vara);
}

int esup2psupm (int nnod, int g_nnod, int nnpe, int *inpoel,
		int *esup1, int *esup2,
		int *psup2, int **psup1,
		int *mask,
		int wown, int sort);

void BldLocalAdjncy (int my_nnodes, int g_nnodes, int **xadj, int **adjncy,
		int *pnnpe, int nelems, int *elements,
		int *locnumb)
{
  /* my_nnodes: nodes assigned to this process */
  /* xadj, adjncy: local adjacency (to be built) */
  /* pnnpe: address of number of nodes per element */
  /* nelems: number of elements */
  /* elements: conectivities */
  /* locnumb: array of size (total nodes) containing -1 if the node
     is not local to this processor, and the local number otherwise */
  int i, iel, k, iaux;
  int *iet, *jet, nnpe;

  nnpe = *pnnpe;
  /* Compute transpose of the conectivity matrix */
  iet = (int *) malloc ((my_nnodes+2) * sizeof(int));
  if (iet == (int *) NULL)
    error (EXIT, "Not enough memory for iet");
  for (i = 0; i < my_nnodes+2; i++)
    iet[i] = 0;

  for (iel = 0; iel < nelems; iel++)
    {                 /* Loop over elements */
      for (k = 0; k < nnpe; k++)         /* Load elemental incidence in nn */
	{
	  iaux = elements[nnpe * iel + k]-1; /* Node numbers start with 1 */
	  iaux = locnumb[iaux];
	  if (iaux < 0)
	    continue;
	  /* contemos en iet, corrido en 2 */
	  iet[iaux+2]++;
	}
    }
  /* convertimos a punteros */
  for (i = 2; i < my_nnodes+2; i++)
    iet[i] += iet[i-1];

  jet = (int *) malloc (iet[my_nnodes+1] * sizeof(int));
  if (jet == (int *) NULL)
    error (EXIT, "Not enough memory for jet");

  /* recorremos nuevamente, guardando */
  for (iel = 0; iel < nelems; iel++)
    {                 /* Loop over elements */
      for (k = 0; k < nnpe; k++)         /* Load elemental incidence in nn */
	{
	  iaux = elements[nnpe * iel + k]-1; /* Node numbers start with 1 */
	  iaux = locnumb[iaux];
	  if (iaux < 0)
	    continue;
	  /* guardamos en jet, incrementando iet */
	  jet[iet[iaux+1]++] = iel;
	}
    }

  /* Compute Adjacency matrix: */
  if (PetscMalloc ((my_nnodes + 2) * sizeof(int), xadj) != 0)
    error (EXIT, "Not enough memory for xadj");

  for (i = 0; i < my_nnodes+2; i++)
    (*xadj)[i] = 0;

  esup2psupm (my_nnodes, g_nnodes, nnpe, elements, jet, iet, *xadj, adjncy,
	      locnumb, 0, 0);

  free(iet);
  free(jet);
}

int esup2psupm (int nnod, int g_nnod, int nnpe, int *inpoel,
		int *esup1, int *esup2,
		int *psup2, int **psup1, int *locnum,
		int wown, int sort)
{
  int k;
  long i, li, lj, ljnnpe, nod, gnod, tmp;
  int *psup2r1, *psup2r2;
  int *auxa=NULL;

  auxa = (int *) calloc (g_nnod, sizeof(int));
  if (auxa == (int *)NULL)
    error (EXIT, "Not enough memory for auxa");

  psup2r1 = psup2+1; psup2r2 = psup2+2;

  /* first loop over nodes: counting nods per node */
  for (i = 0; i < nnod; i++) {
    /* loop over elements surrounding node 'li' */
    for (lj = esup2[i]; lj < esup2[i+1]; lj++) {
      ljnnpe=esup1[lj]*nnpe;
      /* loop over nods of element esup1[lj] */
      for (k = 0; k < nnpe; k++) {
        gnod = inpoel[ljnnpe+k]-1;
	nod = locnum[gnod];
        if ((nod >= 0) && (nod != i) && (auxa[gnod] <= i)) { /* match */
          psup2r2[i]++;
          auxa[gnod] = i+1;
        }
      }
    }
    if (wown) psup2r2[i]++;  /* counting own */
  }

  /* loop over psup2: overadding */
  for (i = 1; i < nnod+2; i++)
    psup2[i] += psup2[i-1];

  /* memory allocation */
  if (PetscMalloc ((psup2[nnod+1]) * sizeof(long), psup1) != 0)
    error (EXIT, "Not enough memory for psup");

  /* cleaning auxiliar vector */
  for (i = 0; i < g_nnod; i++)
    auxa[i]=0;

  /* second loop over nodes: psup1 creation */
  for (i = 0; i < nnod; i++) {
   /* loop over elements surrounding node 'li' */
    for (lj = esup2[i]; lj < esup2[i+1]; lj++) {
      ljnnpe=esup1[lj]*nnpe;
      /* loop over nods of element esup1[lj] */
      for (k = 0; k < nnpe; k++) {
        gnod = inpoel[ljnnpe+k]-1;
	nod = locnum[gnod];
        if ((nod >= 0) && (nod != i) && (auxa[gnod] <= i)) {/* match */
          (*psup1)[psup2r1[i]] = nod;
          psup2r1[i]++;
          auxa[gnod] = i+1;
        }
      }
    }
    if (wown) { /* counting own */
      (*psup1)[psup2r1[i]] = i;
      psup2r1[i]++;
    }
  }

  if (sort) { /* sorting */
    /* loop over nodes */
    for (li = 0; li < nnod; li++) {
      /* loop over nodes surrounding node 'li': sorting step */
      for (lj = psup2[li]; lj < psup2[li+1]-1; lj++)
        if ((*psup1)[lj]>(*psup1)[lj+1]) { /* SWAP it */
          tmp = (*psup1)[lj];
          (*psup1)[lj] = (*psup1)[lj+1];
          (*psup1)[lj+1] = tmp;
          lj = psup2[li]-1;
        }
    }
  }

  free(auxa);

  return 0;
}

/*-------------------------------------------------------------------------
  Function meshdata: returns the values of nelem, nodtot , nnpe and nsd 
--------------------------------------------------------------------------*/
void meshdata(partition **Pa, int *nelem, int *nsd, int *nodtot, int *nnpe, 
	      int *ngroups)
{
  *nelem  = (*Pa)->nelemtot;
  *nsd    = (*Pa)->nsd;
  *nodtot = (*Pa)->nodtot;
  *nnpe   = (*Pa)->nnpe;
  *ngroups= (*Pa)->ngroups;
}

/*------------------------------------------------------------------------
    Function surfaceprocessing - Partition of a surface mesh
    Programmed by Adrian Lew - Started: 08/03/98 Ended: ???
    
    This function receives another partition structure and its surface mesh
and generates the partition structure of the surface mesh, copying the major
part of the data from the first partition structure and identifying the 
elements corresponding to each processor according to the previous distribution
of the nodes.

    Modified 2015-06: Ghosted vectors

Variable names:
    input_mesh: surface mesh file (r)
    mode: (input)
      *0: normal partitioning according to processes in the communicator
      *1: information multiplied (mesh in all the processors).
    Pa: Volume mesh partition     
    perm: permutation index vector, mapping old to new node numbering. (r)
    flag: There's a surface mesh  (HAS_SURFACE_MESH) or 
          not (DOES_NOT_HAVE_SURFACE_MESH) 

Return values:
    SurPa; surface partition

Note:
    There are some parts of this routine that are similar to some parts of 
    meshprocessing, but the last one was already done and working at the
    moment of the creation of the present function, and they are not exactly
    the same, because here there is a lot of information that was already
    calculated before. 
---------------------------------------------------------------------------*/
#undef __SFUNC__
#define __SFUNC__ "surfaceprocesing"

void surfaceprocessing(char *input_mesh, int *fmode, partition **fPa, 
		       int **fperm, partition **fSurPa, int *fflag)
{
  MPI_Comm   Comm;             /* Commmunicator */
  double     *tmpdou;          /* Temporary double variable */ 
  int        rank, size;       /* obvious */
  int        tmpint1, tmpint2; /* temporary int variables */
  int        *elements_buf;    /* array with the mesh elements */
  int        *nelempgrp;       /* number of elements per group */
  int        i, j, k;          /* loop counters */
  int        iel;              /* element number */
  int        *aux_vec   ;      /* auxiliary vector */
  int        *fnnpe;           /* In this version nnpe is a scalar and not */
  int        nnpe;             /* over the groups                          */
  int        *Local;           /* Global to Local mapping of the external
				  nodes */
  int        ngroups;
  int        first;
  int        last;


  partition  *Pa;
  partition  *SurPa;
  int        *perm;
  int        flag;
  int ierr=0, mode;

  flag = *fflag;

  /*For avoiding the warning */
  SurPa = (partition *)NULL;

  /* Initialization */
  Pa    = *fPa;
  perm  = *fperm;
  mode   = *fmode;

  if (flag != HAS_SURFACE_MESH) {
    *fSurPa = (partition *) NULL;
    return;
  }

  Comm = Pa->Comm;
  if (mode == 1)
    Comm = PETSC_COMM_WORLD;

  MPI_Comm_rank(Comm, &rank);                           
  MPI_Comm_size(Comm, &size);

  *fSurPa = (partition *) malloc(sizeof(partition));
  if (fSurPa == (partition **)NULL)
    error (EXIT, "Not enough memory for SurPa");
    
  SurPa = *fSurPa;

  /* Filling SurPa */
  *SurPa    = *Pa;

  if (rank == 0) {
    f2c_string(input_mesh, input_mesh, 80);
      
    tmpint1 = CONNECTIVITIES;
    ReadMesh(input_mesh, &i, &tmpdou, &tmpint2, &fnnpe,
	     &elements_buf, &ngroups, &nelempgrp, &tmpint1);

    nnpe = fnnpe[0];  /* In this version only the first element group nnpe
			 is the valid one */
    free(fnnpe);
    /* The nsd value is not read because it has to be equal to Pa */

    /* renumbering and counting nelemtot */
    SurPa->nelemtot = 0;
    tmpint1 = 0;
    iel = 0;
    for (i = 0; i < ngroups; i++) {
      SurPa->nelemtot += nelempgrp[i];
      for (j = 0; j < nelempgrp[i]; j++, iel++) {
	for (k = 0; k < nnpe; k++)
	  elements_buf[tmpint1+k] = perm[elements_buf[tmpint1+k]-1]+1;
	tmpint1 += nnpe;
      }
    }
  }

  /* Transfering the data */
  ierr = MPI_Bcast (&ngroups , 1, MPI_INT, 0, Comm);
  ierr = MPI_Bcast (&(SurPa->nelemtot), 1, MPI_INT, 0, Comm);
  ierr = MPI_Bcast (&nnpe    , 1, MPI_INT, 0, Comm);

  SurPa->ngroups  = ngroups;
  SurPa->MeshKind = 0;

  SurPa->nnpe     = nnpe;

  if (rank != 0) {
    if (PetscMalloc (nnpe*(SurPa->nelemtot)*sizeof(int), &elements_buf) != 0)
      error(EXIT, "Not enough memory for elements_buf");
    
    nelempgrp = (int *) malloc (ngroups * sizeof(int));
    if (nelempgrp == (int *)NULL)
      error (EXIT, "Not enough memory for nelempgrp");
  }

  ierr = MPI_Bcast(elements_buf, SurPa->nelemtot * nnpe, MPI_INT, 0, Comm );
  ierr += MPI_Bcast(nelempgrp, SurPa->ngroups, MPI_INT, 0, Comm );

  /* /\* Local and boundary elements identification *\/ */
  /* SurPa->LocGrIndex = (int *) malloc((ngroups+1) * sizeof(int)); */
  /* if(SurPa->LocGrIndex == (int *)NULL) */
  /*   error(EXIT, "Not enough memory for SurPa->LocGrIndex"); */
    
  /* SurPa->ExtGrIndex = (int *) malloc((ngroups+1) * sizeof(int)); */
  /* if(SurPa->ExtGrIndex == (int *)NULL) */
  /*   error(EXIT, "Not enough memory for SurPa->ExtGrIndex"); */
    
  SurPa->nelemloc = 0;
  SurPa->nelemext = 0;
    
  /* aux_vector will be used as a flag vector that indicates wether
     an element is a local one, an exernal one o simply has nothing to 
     do here */ 
    
  aux_vec = (int *) malloc(SurPa->nelemtot * sizeof(int));
  if(aux_vec == (int *)NULL)
    error(EXIT, "Not enough memory (aux_vec@Surfaceprocessing)");
    

  /* Loop for knowing the number of local and boundary elements */
  /* first and last inherited from volume mesh */
  first = SurPa->first;
  last  = SurPa->last;

  iel     = 0;
  tmpint1 = 0;
  for (i = 0; i < ngroups; i++) {
    for (j = 0; j < nelempgrp[i]; j++, iel++) {
      aux_vec[iel]=0;
      tmpint2= 0;                        
      for (k = 0; k < nnpe; k++)
	if (elements_buf[tmpint1+k]>=first && elements_buf[tmpint1+k]<last)
	  tmpint2++;
      if (tmpint2 == nnpe) {
	aux_vec[iel] = 1; /* It's a local element */
	SurPa->nelemloc++;
      }
      else if (tmpint2 != 0) {
	aux_vec[iel] = 2; /* It's a boundary element */
	SurPa->nelemext++;
      }
      tmpint1 += nnpe; 
    }
  }

  /* Building the local (and ghost) mesh */
  SurPa->Mesh = (int *) malloc(nnpe*
			       (SurPa->nelemloc+SurPa->nelemext)*sizeof(int));
  if(SurPa->Mesh == (int *)NULL)
    error (EXIT, "Not enough memory for SurPa->Mesh");
    
  Local = (int *) malloc(Pa->nodtot * sizeof(int));
  if(Local == (int *)NULL)
    error(EXIT, "Not enough memory for Local@SurfaceProcessing");

  SurPa->GOS = (int *) malloc((SurPa->nelemloc + SurPa->nelemext) *
			      sizeof(int));
  if(SurPa->GOS == (int *)NULL)
    error(EXIT, "Not enough memory for SurPa->GOS");

  /* Building the Global to Local mapping of the external nodes */
  for (i = 0; i < SurPa->nodghost; i++)
    Local[Pa->Glob[i]] = i;

  /* Copy incidences:
   * local elements starting at 0,
   * interface elements starting at SurPa->nelemloc
   */
  iel   = 0;
  tmpint1 = 0;
  tmpint2 = SurPa->nelemloc;
  for (i = 0; i < ngroups; i++) {
    for (j = 0; j < nelempgrp[i]; j++, iel++) {
      if (aux_vec[iel] == 1) {                       /* Local element */
	for (k = 0; k < nnpe; k++)
	  SurPa->Mesh[tmpint1*nnpe+k] =
	    elements_buf[iel*nnpe+k] - SurPa->first;
	SurPa->GOS[tmpint1] = i+1;
	tmpint1++;
      }
      else if (aux_vec[iel] == 2) {              /* Interface element */
	/* Determining ownership (for integral computations) */
	if (elements_buf[iel*nnpe]>=first && elements_buf[iel*nnpe]<last)
	  SurPa->GOS[tmpint2] = i+1;
	else
	  SurPa->GOS[tmpint2] = -(i+1);

	for (k = 0; k < nnpe; k++) {
	  if (elements_buf[iel*nnpe+k] >= first && 
	      elements_buf[iel*nnpe+k] < last)          /* Local node */
	    SurPa->Mesh[tmpint2*nnpe+k] =
	      elements_buf[iel*nnpe+k] - SurPa->first;
	  else {                                        /* Ghost node */
	    SurPa->Mesh[tmpint2*nnpe+k] =
	      SurPa->nodloc + Local[elements_buf[iel*nnpe+k]-1];
	  }
	}
	tmpint2++;
      }
    }
  }

  PetscSynchronizedPrintf (Comm, " Number of Surface elements in processor"
	       "%d:  %d\t Percent: %g\n", rank,
	       SurPa->nelemloc+SurPa->nelemext, 100.*(double)
	       (SurPa->nelemloc+SurPa->nelemext)/(double)SurPa->nelemtot);
  PetscSynchronizedFlush(Comm, stdout);

  PetscPrintf(Comm, "\n");
  /* freeing auxiliary allocated memory */
  PetscFree(elements_buf);
  free(nelempgrp);
  free(aux_vec);
  free(Local);
}

/*----------------------------------------------------------------------
    Function partitiondestroy - frees partition
    Programmed by Adrian Lew - Started: 27/11/97 - Finished: 27/11/97

Variables names:
     Pa: partition structure

Return values:
     NONE

-----------------------------------------------------------------------*/
#undef __SFUNC__
#define __SFUNC__ "partitiondestroy"

void partitiondestroy(partition **fPa)
{
  partition    *Pa;


  /* freeing partition */
  Pa = *fPa;
  
  if(Pa!=(partition *)NULL){
    free(Pa->Mesh);
    free(Pa->GOS);
    if(Pa->MeshKind & ALLOC_COORD_MASK){
      free(Pa->cogeo);
      free(Pa->d_nz);
      free(Pa->nd_nz);
      free(Pa->Glob);
    }
    free(Pa);
  }
}

void renban (int nod, int *ien, int *jen, int *nv, int *nn)
{
  /*
    nod: n�mero de nodos a renumerar
    ien, jen: conectividad (puntero, vecinos)
    nv, nn: vectores de permutaci�n
    nv: numeraci�n inicial (se sobreescribe)
    nn: numeraci�n final (resultado)
  */
  int i,j,k,in,la, nvmi, nvk, lmax, n1, nrp, nr;
  int /*nextp, */neigh, iaux, i0, i1;
  int length, nr0, nov, minv, nve, found, mbanv, mbann;
  int nni, mbv, mbn;

  /* Primer loop: ordena vecinos segun grado creciente */
  for (i = 0; i < nod; i++)
    {
      in = ien[i];
      la = ien[i+1];
      for (j = in; j < la-1; j++)
	{
	  neigh = jen[j];
	  nvmi = ien[neigh+1] - ien[neigh];
	  for (k = j+1; k < la; k++)
	    {
	      nvk = ien[jen[k]+1] - ien[jen[k]];
	      if (nvk < nvmi)
		{
		  iaux = jen[k];
		  jen[k] = jen[j];
		  jen[j] = iaux;
		  nvmi = nvk;
		}
	    }
	}
    }

  /*
    Inicializaci�n:
    numeraci�n original: identidad
    nueva numeraci�n: -1 (nodos no numerados)
  */
  lmax = 0;         /* Longitud m�xima de las RLSs */
  n1 = 0;           /* Nodo inicial de esta CC */
  nrp = 0;          /* Nueva numeraci�n del nodo inicial
		       de esta CC */
  for (i = 0; i < nod; i++)
      nn[i] = -1;
  nr = 0;

  /* Loop sobre componentes conexas del grafo */
  /* nv: lista de nodos, seg�n se van numerando */
  /* nn: nueva numeraci�n de cada nodo */
  while (1)
    {
      if (nrp < nr) /* Se hab�a calculado una RLS */
	{           /* Re-inicializa vector nn
		       s�lo para esta CC */
	  for (i = nrp; i < nr; i++)
	    nn[nv[i]] = -1;
	}

      /*  Arma la "Rooted Level Structure" (RLS) de n1 */
      nr = nrp;
      /*      nr0 = nr + 1;  */
      nv[nr] = n1;
      nn[n1] = nr;
      i0 = nr;
      i1 = nr;
      length = 1;
      nr++;

      /*  i0 -> i1: Nivel length - 1 de la RLS */
      while(1) /* loop sobre niveles de la RLS */
	{
	  nr0 = nr; /* nr0: pos.del 1er nodo del nivel */
	  for (i = i0; i <= i1; i++)
	    {           /* Recorre nodos de este nivel */
	      in = ien[nv[i]];
	      la = ien[nv[i]+1]-1;
	      for (j = in; j <=la; j++)
		{
		  nov = jen[j];
		  if (nn[nov] > -1) continue;
		  nv[nr] = nov;  /* No estaba numerado */
		  nn[nov] = nr;
		  nr++;
		  if (nr == nod) break; /* Numer� todos */
		}
	      if (nr == nod) break; /* Idem, sale */
	    }
	  if (nr == nod) break; /* Idem, termin� RLS */

	  if (nr > nr0) /* agreg� al menos un nodo */
	    {           /* en este nivel */
	      i0 = nr0;
	      i1 = nr - 1;
	      length++;      /* Un nivel m�s */
	    }
	  else
	    break;
	}

      /* Termin� una CC, (tambi�n la malla ?) */
      if (length > lmax) /* Guarda m�ximo de las longitudes */
	{
	  lmax = length;
	  if (nr == nod) /* Termin� la malla */
	    {
	      i0 = i1+1; /* Salva �ltimo nivel */
	      i1 = nod-1;
	    }

	  /* Selecciona el nodo de m�nimo grado del �ltimo nivel */
	  n1 = nv[i0];
	  minv = ien[n1+1] - ien[n1];
	  for (i = i0+1; i <= i1; i++)
	    {
	      nve = ien[nv[i]+1] - ien[nv[i]];
	      if (nve < minv)
		{
		  n1 = nv[i];
		  minv = nve;
		}
	    }
	  continue;  /* y vuelve a calcular la RLS */
	}

      if (nr != nod) /* Otra CC ? */
	{
	  found = 0;
	  for (i = 0; i < nod && !found; i++)
	    {
	      if (nn[i] > -1)  /* i ya est� en una CC */
		continue;
	      nrp = nr;        /* Siguiente nodo en nv */
	      n1 = i;          /* Posic.: nrp, valor: i */
	      lmax = 0;        /* Lmax para next CC: 0 */
	      found = 1;
	    }
	  if (found)
	    continue;
	}
      else
	break;    /* Todos numerados, sale */
    }

  mbanv = 0;      /* Ancho de banda anterior */
  mbann = 0;      /* Nuevo ancho de banda */
  for (i = 0; i < nod; i++)
    {
      nni = nn[i];
      for (j = ien[i]; j < ien[i+1]; j++)
	{
	  mbv = abs(i-jen[j]);
	  mbn = abs(nni-nn[jen[j]]);
	  if (mbv > mbanv) mbanv = mbv;
	  if (mbn > mbann) mbann = mbn;
	}
    }
  if (mbann < mbanv)      /* Mejor� ? */
    {
    /*
      for (i = 0; i < nod; i++)
	nn[i] = nod-1 - nn[i];       Reverse - C/M Ordering */
    }
  else
    {
      for (i = 0; i < nod; i++)
	nn[i] = i;             /* Original numeration */
    }
}

void mgasslev (int nod, int *ien, int *jen, int mglevels,
	       int *iaux, int *mglev)
{
  /*
    nod: n�mero de nodos a clasificar
    ien, jen: conectividad (puntero, vecinos)
    iaux: vector auxiliar
    mglev: (resultado) clasificaci�n. 0: malla fina, 1,2,...mglevels-1: otras 
  */
  int i,j,k,kk,in,la, nvmi, nvk, lmax, n1, nrp, nr;
  int /*nextp, */neigh, i0, i1;
  int length, nr0, nov, minv, nve, found/*, mbanv, mbann*/;
  int nni/*, mbv, mbn*/;

  /* Fase inicial: numeraci�n Cuthill-McKee */

  /* Primer loop: ordena vecinos segun grado creciente */
  for (i = 0; i < nod; i++)
    {
      in = ien[i];
      la = ien[i+1];
      for (j = in; j < la-1; j++)
	{
	  neigh = jen[j];
	  nvmi = ien[neigh+1] - ien[neigh];
	  for (k = j+1; k < la; k++)
	    {
	      nvk = ien[jen[k]+1] - ien[jen[k]];
	      if (nvk < nvmi)
		{
		  kk = jen[k];
		  jen[k] = jen[j];
		  jen[j] = kk;
		  nvmi = nvk;
		}
	    }
	}
    }

  /*
    Inicializaci�n:
    numeraci�n original: identidad
    nueva numeraci�n: -1 (nodos no numerados)
  */
  lmax = 0;         /* Longitud m�xima de las RLSs */
  n1 = 0;           /* Nodo inicial de esta CC */
  nrp = 0;          /* Nueva numeraci�n del nodo inicial
		       de esta CC */
  for (i = 0; i < nod; i++)
      mglev[i] = -1;
  nr = 0;

  /* Loop sobre componentes conexas del grafo */
  /* iaux: lista de nodos, seg�n se van numerando */
  /* mglev: nueva numeraci�n de cada nodo */
  while (1)
    {
      if (nrp < nr) /* Se hab�a calculado una RLS */
	{           /* Re-inicializa vector mglev
		       s�lo para esta CC */
	  for (i = nrp; i < nr; i++)
	    mglev[iaux[i]] = -1;
	}

      /*  Arma la "Rooted Level Structure" (RLS) de n1 */
      nr = nrp;
      /*      nr0 = nr + 1;  */
      iaux[nr] = n1;
      mglev[n1] = nr;
      i0 = nr;
      i1 = nr;
      length = 1;
      nr++;

      /*  i0 -> i1: Nivel length - 1 de la RLS */
      while(1) /* loop sobre niveles de la RLS */
	{
	  nr0 = nr; /* nr0: pos.del 1er nodo del nivel */
	  for (i = i0; i <= i1; i++)
	    {           /* Recorre nodos de este nivel */
	      in = ien[iaux[i]];
	      la = ien[iaux[i]+1]-1;
	      for (j = in; j <=la; j++)
		{
		  nov = jen[j];
		  if (mglev[nov] > -1) continue;
		  iaux[nr] = nov;  /* No estaba numerado */
		  mglev[nov] = nr;
		  nr++;
		  if (nr == nod) break; /* Numer� todos */
		}
	      if (nr == nod) break; /* Idem, sale */
	    }
	  if (nr == nod) break; /* Idem, termin� RLS */

	  if (nr > nr0) /* agreg� al menos un nodo */
	    {           /* en este nivel */
	      i0 = nr0;
	      i1 = nr - 1;
	      length++;      /* Un nivel m�s */
	    }
	  else
	    break;
	}

      /* Termin� una CC, (tambi�n la malla ?) */
      if (length > lmax) /* Guarda m�ximo de las longitudes */
	{
	  lmax = length;
	  if (nr == nod) /* Termin� la malla */
	    {
	      i0 = i1+1; /* Salva �ltimo nivel */
	      i1 = nod-1;
	    }

	  /* Selecciona el nodo de m�nimo grado del �ltimo nivel */
	  n1 = iaux[i0];
	  minv = ien[n1+1] - ien[n1];
	  for (i = i0+1; i <= i1; i++)
	    {
	      nve = ien[iaux[i]+1] - ien[iaux[i]];
	      if (nve < minv)
		{
		  n1 = iaux[i];
		  minv = nve;
		}
	    }
	  continue;  /* y vuelve a calcular la RLS */
	}

      if (nr != nod) /* Otra CC ? */
	{
	  found = 0;
	  for (i = 0; i < nod && !found; i++)
	    {
	      if (mglev[i] > -1)  /* i ya est� en una CC */
		continue;
	      nrp = nr;        /* Siguiente nodo en iaux */
	      n1 = i;          /* Posic.: nrp, valor: i */
	      lmax = 0;        /* Lmax para next CC: 0 */
	      found = 1;
	    }
	  if (found)
	    continue;
	}
      else
	break;    /* Todos numerados, sale */
    }

  /* Fin renumeraci�n Cuthill-McKee */

  /* Fase final: clasifica en niveles */

  for (i = 0; i < nod; i++)
    mglev[i] = -1;

  for (i = 0; i < nod; i++)
    {
      nni = iaux[i];
      if (mglev[nni] == -1)
	{
	  mglev[nni] = 1;
	  for (j = ien[nni]; j < ien[nni+1]; j++)
	    {
	      nve = jen[j];
	      if (mglev[nve] == -1)
		mglev[nve] = 0;
	    }
	}
    }

}

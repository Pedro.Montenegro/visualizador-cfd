#include <stdlib.h>
#include <float.h>
#include "assembly.h"
#include "prof_utils.h"
#include "petsclog.h"

#define PLER_RED 737

/* This functions are internal to this file */
void InternalnAssembly
  (nF1 onelem, partition *Pa, systemquad *Sq, int *bcccond, 
   PetscScalar *bccvalue, int nfields, int nFS, int *nnfa, int *ifa,
   int *ipos,  int *gpos, int *lpos, int *cpos, 
   PetscScalar *varaux, PetscScalar *varaaux,
   PetscScalar *varscan, PetscScalar *varascan,
   PetscScalar *coordaux, PetscScalar *ae, PetscScalar *ae_ins, int maedim,
   PetscScalar *rhse, int mrhsedim,
   double *parcon, double *delcon,
   double *parnum, double *parmat, double *pargen, int *isim,
   int *AssFlag, int *LumpFlag,
   int *GrpInt, Mat *A, Vec *b, PetscScalar *dp,
   int nRV, PetscScalar *rvals, PetscScalar *evals, int *typered,
   PetscScalar *lnobscan, PetscScalar *gbasescan, PetscScalar *gbaseaux);

void ExternalnAssembly
  (nF1 onelem, partition *Pa, systemquad *Sq, int *bcccond, 
   PetscScalar *bccvalue, int nfields, int nFS, int *nnfa, int *ifa,
   int *ipos,  int *gpos, int *lpos, int *cpos, 
   PetscScalar *varaux, PetscScalar *varaaux,
   PetscScalar *varscan, PetscScalar *varextscan,
   PetscScalar *varascan, PetscScalar *varaextscan,
   PetscScalar *coordaux, PetscScalar *ae, PetscScalar *ae_ins, int maedim,
   PetscScalar *rhse, int mrhsedim,
   double *parcon, double *delcon,
   double *parnum, double *parmat, double *pargen, int *isim,
   int *AssFlag, int *LumpFlag,
   int *GrpInt, Mat *A, Vec *b, PetscScalar *dp,
   int nRV, PetscScalar *rvals, PetscScalar *evals, int *typered,
   PetscScalar *lnobscan, PetscScalar *lnobextscan,
   PetscScalar *gbasescan, PetscScalar *gbaseaux);

/*------------------------------------------------------------------------
  Function Assembly - Assemble a FEM Matrix - Version 4?
  Programmed  by  Adrian Lew -  3/11/97 - Modified 5/2/98
  Modified again 6/3/98
  Modified again by EAD & GCB on 31 March 1999 (oblique nodes)
  Modified by EAD 2004/12/16: many fractional steps in a single call

  This function must be called from FORTRAN.
  It receives general data about the mesh distribution
  (volume and surface meshes)
  the last step solution vector
  pointers to the functions that compute the elemental matrices and 
  right hand side terms
  the global matrices and RHSs to assemble

Variable names:
    A(1..nfsteps): matrices to assemble
    b(1..nfsteps): right hand side vectors to assemble
    onelem: pointer to the elemental operations function over the volume
    surelem: pointer to the elemental operations function over the boundary
    Sy: system structure  to assembly
    v: last step solution, or more generally, data to calculate the matrix 
      and rhs coefficients.
    nfsteps: number of matrices (and/or RHSs) to assemble
    nnfa(1..nfsteps): number of fields to assemble in each fractional step
    ifa(1..sum(nnfa)): array with the unknowns to assemble in each fr. step
    AssFlag(1..nfsteps): flag for assembling the matrix and/or the RHS
    LumpFlag(1..nfsteps): lumping flag for the matrix
    VolInt: switch array (1..ngroups) for integrate on that volume group.
    SurInt: switch array (1..ngroups) for integrate on that surface group.
    redvals(1..nredvals): array for computing reduced values over the elements
    typered(1..nredvals): type of reduction to perform (sum, max, min,...)
    nredvals: number of reduced values to compute

This arguments are for the onelem routine:
    parcon: value of the continuation parameter
    delcon: value of the continuation parameter step
    parnum: array with numerical parameters
    parmat: array with material parameters
    pargen: array with general parameters
    isim: symmetry indicator

Remember:
    The nodes numbers begin with 1, not with 0. So, in the first nfields
    positions of v are the values for the node number 1.
    In nfa, field is the number of the unknown.
-----------------------------------------------------------------------------*/

#undef __SFUNC__
#define __SFUNC__ "nassembly"

void nassembly (systemdata **fSy, Vec *fv, nF1 onelem, nF1 surelem,
		Mat *fA, Vec *fb, int *nfsteps, int *nnfa, int *fifa,
		double *parcon, double *delcon, 
		double *parnum, double *parmat, double *pargen, 
		double *redvals, int *typered, int *nredvals,
		int *isim, int *AssFlag, int *LumpFlag,
		int *VolInt, int *SurInt)
{
  Mat      A;
  Vec      b, v;

  MPI_Comm Comm;        /* Communicator of Partition */
  int      ierr;        /* error checking variable */
  int      i;           /* loop counters */
  /*int      low, high;    span of local indices */

  int      nnpemax;     /* maximum between Pa->nnpe and SurPa->nnpe */
  int      *ipos;       /* vector mapping element numbering to Mat A 
			   numbering */
  int      *cpos;       /* vector mapping element numbering to the position of 
			   varaux at wich  the data of that node starts*/
  int      *gpos;       /* vector mapping element numbering to global 
			   numbering */
  int      *lpos;       /* vector mapping element numbering to the local of 
			   nodes order in the data vactors (cogeo..,var...) */
  PetscScalar   *evals;  /* reduced elemental values */
  PetscScalar   *rvals;   /* reduced local values */
  PetscScalar   *varaux;     /* vector for passing the element values of v */
  PetscScalar   *varaaux;    /* vector for passing the element values of va */
  PetscScalar   *coordaux;   /* vector for passing the element nodes
				coordinates */
  PetscScalar   *gbaseaux;   /* vector for passing the oblique nodes basis */
  PetscScalar   *varscan;    /* pointer for scanning the v values */
  PetscScalar   *varextscan; /* pointer for scanning the varext values */
  PetscScalar   *varascan;   /* pointer for scanning the va values */
  PetscScalar   *varaextscan;/* pointer for scanning the varaext values */
  PetscScalar   *lnobscan;   /* pointer for scanning the oblique
				switches vector */
  PetscScalar   *lnobextscan;/* pointer for scanning the external oblique 
				switches vector */
  PetscScalar   *gbasescan;  /* pointer for scanning the oblique bases */
  PetscScalar   *ae;         /* pointer to the elemental matrix */
  PetscScalar   *rhse;       /* pointer to the elemental right hand side */
  PetscScalar   *ae_ins;     /* Matrix to use in MatSetValues            */
  PetscScalar   *dp;         /* array containing then FEM basis derivatives */

  int   maxaedim;       /* dimension of ae */
  int   maxrhsedim;     /* max dimension of rhse */
  int Sflag;
  int nFS;
  PetscScalar tmp;      /* temporary variable */
  PetscBool pflag;

  int nvar, ngroups, maxcomp, nsd, nRV, *ifa, totnfa;
  systemdata   *Sy;
  static int firsttime=1, counter;
  if (firsttime)
    {
      firsttime=0;
      PetscLogEventRegister(&counter,"Assembly without comm",PLER_RED);
    } 

  //  A = *fA;
  //  b = *fb;
  v = *fv;
  Sy = *fSy;
  ngroups = Sy->SurPa->ngroups;
  Sflag = 0;
  for (i = 0; i < ngroups; i++)
    if (SurInt[i])
      Sflag = 1;

  nnpemax = Sy->Pa->nnpe; /* Sy->Pa->nnpe > Sy->SurPa->nnpe surely */
  /* Take care of this in the case that Pa->nnpe were 
     different from Sq->maxnpe */
  /* Note: 
     The nsd, nvar and maxcomp values of Pa and SurPa should be the same,
     so, there is no verification of consistency at all. */
  Comm    = Sy->Pa->Comm;
  nsd     = Sy->Pa->nsd;
  maxcomp = Sy->Sq->maxcomp;
  nvar    = Sy->Sq->nvar;
  nFS = *nfsteps;

  nRV  = *nredvals;

  /* Translate fifa to ifa (fortran->C numbering) */
  totnfa = 0;
  for (i = 0; i < nFS; i++)
    totnfa += nnfa[i];

  ifa = (int *) malloc (totnfa * sizeof(int));
  if (ifa == (int *)NULL)
    error (EXIT, "Not enough memory for ifa");

  for (i = 0; i < totnfa; i++)
    ifa[i] = fifa[i] - 1;
 
  /* Request boundary nodes values */
  ierr = VecScatterBegin(v, Sy->varext, INSERT_VALUES, SCATTER_FORWARD, 
			 Sy->G2Lscat);   CHKERRABORT(PETSC_COMM_WORLD,ierr);
  /* Receive boundary nodes values */
  ierr = VecScatterEnd(v, Sy->varext, INSERT_VALUES, SCATTER_FORWARD, 
		       Sy->G2Lscat);   CHKERRABORT(PETSC_COMM_WORLD,ierr);

  /* Also for va (20030807) */
  ierr = VecScatterBegin(Sy->va, Sy->varaext, INSERT_VALUES, SCATTER_FORWARD, 
			 Sy->G2Lscat);   CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecScatterEnd(Sy->va, Sy->varaext, INSERT_VALUES, SCATTER_FORWARD, 
		       Sy->G2Lscat);   CHKERRABORT(PETSC_COMM_WORLD,ierr);

  /* Initializing some useful values */
  //  ierr = VecGetOwnershipRange
  //    (v, &low, &high);                CHKERRABORT(PETSC_COMM_WORLD,ierr);

  ierr = VecGetArray
    (v, &varscan);                   CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGetArray
    (Sy->varext, &varextscan);       CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGetArray
    (Sy->va, &varascan);             CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGetArray
    (Sy->varaext, &varaextscan);     CHKERRABORT(PETSC_COMM_WORLD,ierr);

  if (Sy->lnob)
    {   /* There are oblique nodes in mesh */
      ierr = VecGetArray
	(Sy->lnob, &lnobscan);       CHKERRABORT(PETSC_COMM_WORLD,ierr);
      ierr = VecGetArray
	(Sy->lnobext, &lnobextscan); CHKERRABORT(PETSC_COMM_WORLD,ierr);
      ierr = VecGetArray
	(Sy->gbase, &gbasescan);     CHKERRABORT(PETSC_COMM_WORLD,ierr);
    }
  else             /* No oblique nodes in mesh, use lnobscan as switch */
    lnobscan = (PetscScalar *) NULL;

  for (i = 0; i < nFS; i++) /* Zeroes global matrices and RHSs */
    {
      A = fA[i];
      b = fb[i];
      if (AssFlag[i] & MAT_ASSEMBLE_MASK) /* Assemble matrix */
	{
	  MatAssembled (A, &pflag);
	  if (pflag == PETSC_TRUE)
	    {
	      ierr = MatZeroEntries(A); CHKERRABORT(PETSC_COMM_WORLD,ierr);
	    }
	}
      if (AssFlag[i] & RHS_ASSEMBLE_MASK) /* Assemble RHS */
	{
	  tmp = 0;
	  ierr = VecSet(&tmp, b);   CHKERRABORT(PETSC_COMM_WORLD,ierr);
	}
    }

  varaux = (PetscScalar *) malloc (nnpemax*nvar*maxcomp*sizeof(PetscScalar));
  if (varaux == (PetscScalar *)NULL)
    error (EXIT, "Not enough memory for varaux");

  varaaux = (PetscScalar *) malloc (nnpemax*nvar*maxcomp*sizeof(PetscScalar));
  if (varaaux == (PetscScalar *)NULL)
    error (EXIT, "Not enough memory for varaaux");

  coordaux = (PetscScalar *) malloc (nnpemax*nsd*sizeof(PetscScalar));
  if (coordaux == (PetscScalar *)NULL)
    error(EXIT, "Not enough memory for coordaux");

  if (Sy->lnob)
    {
      gbaseaux = (PetscScalar *)malloc(nnpemax*nsd*nsd*sizeof(PetscScalar));
      if(!gbaseaux)
	error(EXIT, "Not enough memory for gbaseaux");
    }
  else
    gbaseaux = (PetscScalar *) NULL;

  ae     = (PetscScalar *)NULL; /* avoiding warnings */
  ae_ins = (PetscScalar *)NULL; /* avoiding warnings */
  maxaedim = 0;
  for (i = 0; i < nFS; i++)
    {
      if (AssFlag[i] & MAT_ASSEMBLE_MASK)
	if (nnpemax*nnpemax*nnfa[i]*nnfa[i] > maxaedim)
	  maxaedim = nnpemax*nnpemax*nnfa[i]*nnfa[i];
    }
  if (maxaedim > 0)
    {
      ae = (PetscScalar *) malloc (maxaedim*nFS*sizeof(PetscScalar)); 
      if (ae == (PetscScalar *)NULL)
	error (EXIT, "Not enough memory for ae");
      ae_ins = (PetscScalar *) malloc (maxaedim*nFS*sizeof(PetscScalar)); 
      if (ae_ins == (PetscScalar *)NULL)
	error (EXIT, "Not enough memory for ae_ins");
    }

  dp = (PetscScalar *) malloc(nnpemax*3*(nvar+1)*sizeof(PetscScalar));
  if (dp == (PetscScalar *)NULL)
    error(EXIT, "Not enough memory for dp");

  maxrhsedim = 0;
  for (i = 0; i < nFS; i++)
    {
      if (AssFlag[i] & RHS_ASSEMBLE_MASK)
	if (nnpemax*nnfa[i] > maxrhsedim)
	  maxrhsedim = nnpemax*nnfa[i];
    }
  if (maxrhsedim > 0)
    {
      rhse = (PetscScalar *) malloc(nFS*maxrhsedim*sizeof(PetscScalar));
      if (rhse == (PetscScalar *)NULL)
	error(EXIT, "Not enough memory for rhse");

      ipos = (int *) malloc(nFS*maxrhsedim*sizeof(int));
      if (ipos == (int *)NULL)
	error(EXIT, "Not enough memory for ipos");
    }
  gpos = (int *) malloc(nnpemax*sizeof(int));
  if (gpos == (int *)NULL)
    error(EXIT, "Not enough memory for gpos");

  cpos = (int *) malloc(nnpemax*sizeof(int));
  if (cpos == (int *)NULL)
    error(EXIT, "Not enough memory for cpos");

  lpos = (int *) malloc(nnpemax*sizeof(int));
  if (lpos==(int *)NULL)
    error(EXIT, "Not enough memory for lpos");

  if (nRV)
    {
      evals = (PetscScalar *) malloc (nRV * sizeof(PetscScalar));
      if (evals == (PetscScalar *)NULL)
	error (EXIT, "Not enough memory for evals");

      rvals = (PetscScalar *) malloc (nRV * sizeof(PetscScalar));
      if (rvals == (PetscScalar *)NULL)
	error (EXIT, "Not enough memory for rvals");
      for (i = 0; i < nRV; i++)
	if (typered[i] == (int)MPI_SUM)
	  rvals[i] = 0.0;
	else if (typered[i] == (int)MPI_MAX)
	  rvals[i] = -DBL_MAX;
	else if (typered[i] == (int)MPI_MIN)
	  rvals[i] = DBL_MAX;
	else if (typered[i] == (int)MPI_PROD)
	  rvals[i] = 1.0;
	else
	  rvals[i] = 0.0;
    }

  /* Local elements integration loop
     PetscLogEventBegin (counter,0,0,0,0);
     Integrating over the Volume mesh */
  InternalnAssembly
    (onelem, Sy->Pa, Sy->Sq, Sy->bcccond, Sy->bccvalue, 
     Sy->nfields, nFS, nnfa, ifa, ipos, gpos, lpos, cpos,
     varaux, varaaux, varscan, varascan, coordaux, ae, ae_ins, maxaedim,
     rhse, maxrhsedim, parcon, delcon, parnum, parmat, pargen, isim,
     AssFlag, LumpFlag, VolInt, fA, fb, dp,
     nRV, rvals, evals, typered, lnobscan, gbasescan, gbaseaux); 

  /* Integrating over the Surface Mesh */
  if (Sflag) {
    if (Sy->SurPa != (partition *)NULL)
      InternalnAssembly
	(surelem, Sy->SurPa, Sy->SurSq, Sy->bcccond, Sy->bccvalue, 
	 Sy->nfields, nFS, nnfa, ifa, ipos, gpos, lpos, cpos,
	 varaux, varaaux, varscan, varascan, coordaux, ae, ae_ins, maxaedim,
	 rhse, maxrhsedim, parcon, delcon, parnum, parmat, pargen, isim,
	 AssFlag, LumpFlag, SurInt, fA, fb, dp,
	 nRV, rvals, evals, typered, lnobscan, gbasescan, gbaseaux);
    else
      error (STAY, "Surface Flag active, but no surface mesh found");
  }
  /* End of loop over local elements    */

  /* External elements integration loop */   

  /* Integrating over the Volume mesh   */
  ExternalnAssembly
    (onelem, Sy->Pa, Sy->Sq, Sy->bcccond, Sy->bccvalue, 
     Sy->nfields, nFS, nnfa, ifa, ipos, gpos, lpos, cpos,
     varaux, varaaux, varscan, varextscan, varascan, varaextscan,
     coordaux, ae, ae_ins, maxaedim,
     rhse, maxrhsedim, parcon, delcon, parnum, parmat, pargen, isim,
     AssFlag, LumpFlag, VolInt, fA, fb, dp,
     nRV, rvals, evals, typered, lnobscan, lnobextscan, gbasescan, gbaseaux); 

  /* Integrating over the Surface Mesh */
  if (Sflag) {
    if(Sy->SurPa != (partition *)NULL)
      ExternalnAssembly
	(surelem, Sy->SurPa, Sy->SurSq, Sy->bcccond, Sy->bccvalue,
	 Sy->nfields, nFS, nnfa, ifa, ipos, gpos, lpos, cpos,
	 varaux, varaaux, varscan, varextscan, varascan, varaextscan,
	 coordaux, ae, ae_ins, maxaedim,
	 rhse, maxrhsedim, parcon, delcon, parnum, parmat, pargen, isim,
	 AssFlag, LumpFlag, SurInt, fA, fb, dp,
	 nRV, rvals, evals, typered,
	 lnobscan, lnobextscan, gbasescan, gbaseaux);
    else
      error (STAY,"Surface Flag active, but no surface mesh found");
  }
  /* End of loop over external elements
     PetscLogEventEnd (counter,0,0,0,0);
     Ending ... */

  for (i = 0; i < nFS; i++) /* Assemble matrices and RHSs */
    {
      if (AssFlag[i] & MAT_ASSEMBLE_MASK)
	{
	  A = fA[i];
	  ierr = MatAssemblyBegin
	    (A, MAT_FINAL_ASSEMBLY);       CHKERRABORT(PETSC_COMM_WORLD,ierr);
	  ierr = MatAssemblyEnd
	    (A, MAT_FINAL_ASSEMBLY);       CHKERRABORT(PETSC_COMM_WORLD,ierr);
	  ierr = MatSetOption
	    (A, MAT_NO_NEW_NONZERO_LOCATIONS);
	}
      if (AssFlag[i] & RHS_ASSEMBLE_MASK)
	{
	  b = fb[i];
	  ierr = VecAssemblyBegin(b);      CHKERRABORT(PETSC_COMM_WORLD,ierr);
	  ierr = VecAssemblyEnd(b);        CHKERRABORT(PETSC_COMM_WORLD,ierr);
	}
    }

  /* Reduce over the integral values */
  if (nRV != 0)
    {
      for (i = 0; i < nRV; i++)
	MPI_Allreduce (redvals+i, rvals+i, 1, MPI_DOUBLE,
		       (MPI_Op)typered[i], Comm);
    }

  ierr = VecRestoreArray
    (v, &varscan);                   CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecRestoreArray
    (Sy->varext, &varextscan);       CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecRestoreArray
    (Sy->va, &varascan);             CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecRestoreArray
    (Sy->varaext, &varaextscan);     CHKERRABORT(PETSC_COMM_WORLD,ierr);
  if (Sy->lnob)
    {
      ierr = VecRestoreArray
	(Sy->lnob, &lnobscan);         CHKERRABORT(PETSC_COMM_WORLD,ierr);
      ierr = VecRestoreArray
	(Sy->lnobext, &lnobextscan);   CHKERRABORT(PETSC_COMM_WORLD,ierr);
      ierr = VecRestoreArray
	(Sy->gbase, &gbasescan);       CHKERRABORT(PETSC_COMM_WORLD,ierr);
      free(gbaseaux);
    }

  free(varaux);
  free(varaaux);
  free(coordaux);
  if (maxaedim)
    {
      free(ae);
      free(ae_ins);
    }
  if (maxrhsedim)
    {
      free(rhse);
      free(ipos);
    }
  free(dp);
  free(gpos);
  free(lpos);
  free(cpos);
  free(ifa);
  if (nRV)
    {
      free(evals);
      free(rvals);
    }

  /*
    if(nnfa==3)
    {
    Viewer   Va;
    ViewerFileOpenASCII(MPI_COMM_WORLD,"vector",&Va);
    VecView(b,Va);
    ViewerDestroy(Va);
    error(EXIT, "Chau");
    }
  */
}

/*------------------------------------------------------------------------
  Function InternalnAssembly - Assembly the local elements of a FEM Matrix 
  Version 2
  Programmed  by  Adrian Lew -  7/03/98 
  Modified by EAD, 2004/12/16

  This function is thought for being used only from assembly.
  Its task is to make the loop over the local elements of each group
  to assembly their contributions to the FEM matrix. 
  The memory is allocated outside this function because the same memory
  is used in several different calls.

Variable names:
    onelem: pointer to the elemental operations function
    Pa: mesh data
    Sq: qudrature data
    bcccond: boundary conditions vector
    bccvalue: boundary condition values vector
    nfields: total number of different fields (scalars) 
    nnfa: number of fields to assembly (1<= nfa <=nfields)
    nfa: array with the numbers of the fields to assembly 
    ipos: array mapping element numbering to Mat A numbering 
    gpos: array mapping element numbering to global numbering 
    lpos: array mapping element numbering to the local of nodes order in 
         the data vactors (cogeo..,var...) 
    cpos: array mapping element numbering to the position of varaux at wich 
          the data of that node starts.
    varaux: array for passing the values of varscan to onelem.
    varaaux: ?
    varscan: last step solution, or more generally, data to calculate the 
            matrix and rhs coefficients.
    varascan: ?
    coordaux: array for passing the coordinates values to onelem.
    ae: elemental matrix
    rhse: elemental RHS
    ae_ins: ?

    Aflag: flag for recalculating the matrix A
    Lflag: lumping flag
    GrpInt: array with the switches to integrate or not the different groups.
    A: matrix to fill
    b: right hand side vector to fill
    dp: array for contruct the FEM basis function derivatives
    integrals: array for acumulate the integral values in the loop  over 
              the elements
    inte: elementary integral contribution
    nintgr: number of integrals to calculate
    lnobscan: switch of oblique nodes 
             (pointer over local part of distributed Vec)
    gbasescan: oblique nodes basis
    gbaseaux: array for passing values of gbase to elemental routines

This arguments are for the onelem routine:
    parcon: value of the continuation parameter
    delcon: value of the continuation parameter step
    parnum: array with numerical parameters
    parmat: array with material parameters
    pargen: array with general parameters
    isim: symmetry indicator

Remember:
    The nodes numbers begin with 1, not with 0. So, in the first nfields
    positions of v are the values for the node number 1.
    In nfa, field is the number of the unknown.
-----------------------------------------------------------------------------*/

#ifdef DEBUG_ASSEMBLY
  FILE* dbg_fp;
#endif

#undef __SFUNC__
#define __SFUNC__ "InternalnAssembly"

void InternalnAssembly
  (nF1 onelem, partition *Pa, systemquad *Sq, int *bcccond,
   PetscScalar *bccvalue,
   int nfields, int nFS, int *nnfa, int *ifa,
   int *ipos,  int *gpos, int *lpos, int *cpos,
   PetscScalar *varaux, PetscScalar *varaaux,
   PetscScalar *varscan, PetscScalar *varascan,
   PetscScalar *coordaux, PetscScalar *ae, PetscScalar *ae_ins, int maedim,
   PetscScalar *rhse, int mrhsedim,
   double *parcon, double *delcon,
   double *parnum, double *parmat, double *pargen, int *isim,
   int *AssFlag, int *LumpFlag,
   int *GrpInt, Mat *A, Vec *b, PetscScalar *dp,
   int nRV, PetscScalar *rvals, PetscScalar *evals, int *typered,
   PetscScalar *lnobscan, PetscScalar *gbasescan, PetscScalar *gbaseaux)
{
  int nnpe, nsd, nvar, ngr;
  int ifs, i, j, i2, k, l, m, kkdir, ind;
  int tmpint1, tmpint2;
  int ierr;
  int inbe;        /* index at the origin of the current element nodes */
  int igr;         /* group index */
  int iel;         /* element index */
  int AFlag;       /* lumping flag */
  int iswnob;      /* Switch: element contains O.N. */
  int posgbase;    /* Switch: node is an O.N. if posgbase != 0 */
  int anfa;        /* valor absoluto de nfa[l] */
  int size, ipass, npass; /* Numero de procesadores, partial assemblies */
  int ident, dpass;  /* Numero de elementos entre cada ensamblaje parcial */
  int lob;           /* position of oblique field */
  double vecaux[Pa->nsd]; /* auxiliar un-rotated oblique field */

  PetscScalar   tmp;
  static int firsttime2 = 1, cmatseti;

#ifdef DEBUG_ASSEMBLY
  static int dbg_time=1;
  int prstart=3, prend=4;
  char dbg_file[20];
#endif

  if (firsttime2)
    {
      firsttime2=0;
      PetscLogEventRegister(&cmatseti,"matset internal",PLER_RED);
    }

#ifdef DEBUG_ASSEMBLY
  if (dbg_time == 1) {
     MPI_Comm_rank (Pa->Comm, &ident);
     sprintf(dbg_file, "dbg_matelem%d", ident);
     dbg_fp = fopen(dbg_file, "w");
     fprintf(dbg_fp, "Mat. elem., elements with oblique nodes:\n");
  }
#endif

  /* calcula cada cuanto hacer los ensamblajes parciales */
  MPI_Comm_size (Pa->Comm, &size);
  MPI_Comm_rank (Pa->Comm, &ident);
  npass = Pa->nelemtot / size / 250;
  if (npass > 1)
    {
      dpass = Pa->nelemloc / npass + 1;   /* Approx 250 */
/*      printf(" Proc: %d, npass: %d, dpass: %d\n", ident, npass, dpass); */
      ipass = 0;
    }

  nnpe    = Pa->nnpe;
  nsd     = Pa->nsd;
  nvar    = Sq->nvar;
  ngr     = Pa->ngroups;
  /*
    rhsedim = 0;
    for (i = 0; i < nFs; i++)
    rhsedim += nnpe * nnfa[i];
  */

  /* Loop over mesh groups */
  iel = 0;
  kkdir = 0;

  pargen[8] = 1.0; /* This element belongs to this processor */
  for (igr = 0; igr < ngr; igr++) {   /* Loop over groups of elements */
    if (GrpInt[igr]) {                /* This group is integrated */
      for (i = Pa->LocGrIndex[igr]; i < Pa->LocGrIndex[igr+1]; i++, iel++) {
	                             /* Loop over elements of this group */
	inbe = iel * nnpe;
	for (ifs = 0; ifs < nFS; ifs++)
	  for (k = 0; k < nnpe; k++)   /* Loop over nodes of this element */
	    for (j = 0; j < nnfa[ifs]; j++) /* Loop over fields of this node */
	      ipos[ifs*mrhsedim+k*nnfa[ifs]+j] =
		(Pa->LocMesh[inbe+k]-1) * nnfa[ifs] + j;

	/* Oblique nodes processing */
        iswnob = 0;
        if (lnobscan) {             /* Oblique nodes in mesh */
	  for (k = 0; k < nnpe; k++)
	    if (lnobscan[Pa->LocMesh[inbe+k]-(Pa->first)])
	      iswnob = 1;
	  if (iswnob) {             /* Oblique nodes in this element */
            for (k = 0; k < nnpe; k++){
	      posgbase = lnobscan[Pa->LocMesh[inbe+k]-(Pa->first)];
	      if (posgbase) {        /* This node is oblique */
		/*
		for (j = 0; j < nsd*nsd; j++){
		  		  gbaseaux[k*nsd*nsd + j] = 
		    gbasescan[(posgbase-1)*nsd*nsd + j];
		  */
		for (j = 0; j < nsd; j++){
		  for (i2 = 0; i2 < nsd; i2++){
		    gbaseaux[k + nnpe*j + i2*nnpe*nsd] = 
		    //  gbasescan[(posgbase-1)*nsd*nsd + j*nsd + i2]; GCB, 3 dec 99
		      gbasescan[(posgbase-1) + j*nsd + i2];
		  }
		}
	      }
	      else {               /* This one is normal */
		/*
		for (j = 0; j < nsd*nsd; j++)
		  gbaseaux[k*nsd*nsd + j] = (j % (nsd+1)) ? 0.0 : 1.0;
		
		for (j = 0; j < nsd*nsd; j++)
		  gbaseaux[k+ (j/nsd)*nnpe + (j%nsd)*nnpe*nsd] =
		    (j % (nsd+1)) ? 0.0 : 1.0;
		*/
		for (j = 0; j < nsd; j++){
		  for (i2 = 0; i2 < nsd; i2++){
		    gbaseaux[k + nnpe*j + i2*nnpe*nsd] = 
		      (i2 == j) ? 1.0 : 0.0; 
	             /* if (i2==j){
		    	gbaseaux[k + nnpe*j + i2*nnpe*nsd] = 999999.;
		      }
		      else {
		    	gbaseaux[k + nnpe*j + i2*nnpe*nsd] = 0.;
		      }*/
		  }
		}
	      }   /* end if ( This node is oblique ) */
	    }   /* end loop over nodes in this element */
	  }   /* end if ( Oblique nodes in this element ) */
        }   /* end if ( Oblique nodes in mesh ) */

        for (k = 0; k < nnpe; k++){
	  gpos[k] = Pa->LocMesh[inbe+k] - 1;
	  lpos[k] = gpos[k] + 1 - (Pa->first);
	  cpos[k] = nfields * lpos[k];
	  tmpint2 = 0;
	  for (j = 0; j < nvar; j++){
	    tmpint1 = Sq->icomp[j];
	    for (l = 0; l < tmpint1; l++){
	      varaux[k+j*nnpe+l*nnpe*nvar] = varscan[cpos[k]+tmpint2];
	      varaaux[k+j*nnpe+l*nnpe*nvar] = varascan[cpos[k]+tmpint2];
	      tmpint2++;
	    }
	  }
	  for(j=0; j<nsd; j++)
	    coordaux[k+j*nnpe] = (Pa->cogeoloc)[nsd*lpos[k]+j];
	}

	tmpint1 = igr+1;

	/* Elemental operations */
	AFlag = (AssFlag[ifs] & MAT_ASSEMBLE_MASK) ? 1 : 0;
	(*onelem)(varaux, varaaux, coordaux, &nvar, Sq->icomp, &nnpe, &nsd, 
		  ae, rhse, parcon, delcon, parnum, parmat, pargen, 
		  &(Sq->ngau), Sq->kint, &(Sq->itypegeo), &iel, Sq->nod, 
		  &(Sq->maxnpe), &(Sq->maxgau), Sq->wgp, isim, Sq->p, 
		  Sq->dpl, dp, &mrhsedim, &AFlag, &tmpint1,
		  &nRV, evals, typered, &iswnob, gbaseaux);
	/* Global assembly */

	if (iswnob) {
#ifdef DEBUG_ASSEMBLY
	  /* Elemental matrices
	  if (dbg_time >= prstart && dbg_time <= prend) {
	    fprintf(dbg_fp, "\nElement: %d\n", iel);
	    for (k = 0; k < rhsedim; k++) {
	      for (j = 0; j < rhsedim; j++)
	        fprintf (dbg_fp, " %g", ae[k*rhsedim+j]);
	      fprintf (dbg_fp, "\n");
	    }
	  }
	  */
	  if (dbg_time >= prstart && dbg_time <= prend) {
	    fprintf(dbg_fp, "\nElement: %d, gbaseaux:\n", iel);
	    for (k = 0; k < nnpe; k++) {
	      for (j = 0; j < nsd; j++)
		for (l = 0; l < nsd; l++)
		  fprintf (dbg_fp, " %g", gbaseaux[k+nnpe*j+nnpe*nsd*l]);
	      fprintf (dbg_fp, "\n");
	    }
	  }
#endif
	  lob = -1;
	  for (l = 0; l < nnfa[0] && lob == -1; l++){   /* Find first                     oblique field */
	    if (ifa[l] < 0){
	      lob = l;
	      if (ifa[l+1] > 0) lob = -1;
	      if (nsd >= 3 && ifa[l+2] > 0) lob = -1;
	    }
	  }
	}

	for (ind = ifs = 0; ifs < nFS; ind +=nnfa[ifs], ifs++) {
	                                      /* Loop over fractional steps */
	  for (k = 0; k < nnpe; k++){              /* Nodes of this element */
	    if (iswnob && lob != -1) {
	      /* vecaux is always needed (even for non-oblique nodes)
		 && lnobscan[Pa->LocMesh[inbe+k]-(Pa->first)]) {      */
	    /* There are oblique nodes in this element, and there is an oblique
	       field in this fractional step(, and k is an oblique node) */
	    /* ... un-rotate previous solution at node k */
	      anfa = ifa[lob] < 0 ? -(ifa[lob]+1)-1 : ifa[lob];
	      for (j = 0; j < nsd; j++)
		{
		  vecaux[j] = 0;
		  for (i2 = 0; i2 < nsd; i2++)
		    vecaux[j] +=         varscan[cpos[k]+anfa+i2] *
		      gbaseaux[k + nnpe*j + i2*nnpe*nsd];
		}
	    }

	    for (l = 0; l < nnfa[ifs]; l++){/*Fields of the node (matrix row)*/
	      m = k*nnfa[ifs]+l;  /* m is the elemental matrix row */
	      anfa = ifa[ind+l] < 0 ? -(ifa[ind+l]+1)-1 : ifa[ind+l];

	      if (bcccond[cpos[k]+anfa]==0 ||
		  !(AssFlag[ifs] & BCC_ASSEMBLE_MASK)) {
		/* No Dirichlet condition or disable BC assembly */
		if (AssFlag[ifs] & MAT_ASSEMBLE_MASK) {
		  if (LumpFlag[ifs]==LUMPED) {      /* Lumped: only diagonal */
		    ierr = MatSetValues (A[ifs],
					 1, &ipos[ifs*mrhsedim+m],
					 1, &ipos[ifs*mrhsedim+m], 
					 &ae[ifs*mrhsedim*mrhsedim+
					     m*mrhsedim+
					     m], ADD_VALUES);  
		  }
		  else if (LumpFlag[ifs]==STANDARD_UNCOUPLED) { /* Uncoupled
								 fields:  */
		    for (j = 0; j < nnpe; j++) {
		      i2 = j*nnfa[ifs] + l;
		      tmp = ae[ifs*mrhsedim*mrhsedim + i2*mrhsedim + m];
		      ierr = MatSetValues (A[ifs],
					   1, &ipos[ifs*mrhsedim+m],
					   1, &ipos[ifs*mrhsedim+i2],
					   &tmp, ADD_VALUES);
		    }
		  }
		  else if (LumpFlag[ifs]==STANDARD ||
			   LumpFlag[ifs]==NSBSLUMP) {         /* Not Lumped: */
		    for (j = 0; j < mrhsedim; j++)            /* full row */
		      ae_ins[j + m * mrhsedim] =
			ae[ifs*mrhsedim*mrhsedim + j*mrhsedim + m];
		  }
		  else {   /* safety */
		    error (EXIT, "assembly.c: Unrecognized lumping flag");
		  }
		}
	      }
	      else {   /* Dirichlet Condition */
		kkdir++;
		if (AssFlag[ifs] & MAT_ASSEMBLE_MASK) {
		  if (LumpFlag[ifs]==STANDARD || LumpFlag[ifs]==NSBSLUMP) {
		    /* Not Lumped */
		    for (j = 0; j < mrhsedim; j++)
		      ae_ins [j + m*mrhsedim] = 0.;
		    ae_ins [m + m*mrhsedim]=1.;
		  }
		  else if (LumpFlag[ifs]==STANDARD_UNCOUPLED ||
			   LumpFlag[ifs]==LUMPED) {
		    /* Lumped */
		    tmp = 1.;
		    ierr = MatSetValues (A[ifs],
					 1, &ipos[ifs*mrhsedim+m],
					 1, &ipos[ifs*mrhsedim+m], &tmp, 
					 ADD_VALUES);
		  }
		}

		if (AssFlag[ifs] & RHS_ASSEMBLE_MASK) {
		  if (!(AssFlag[ifs] & BCC_DELTA_MASK)) /* BC: field */
		    rhse[ifs*mrhsedim + m] = bccvalue[cpos[k]+anfa];
		  else {                                /* BC: increment */
		    if (iswnob && lob != -1 && l >=lob && l < lob + nsd)
		      rhse[ifs*mrhsedim + m] =
			bccvalue[cpos[k]+anfa] - vecaux[l-lob];
		    else
		      rhse[ifs*mrhsedim + m] =
			bccvalue[cpos[k]+anfa] - varscan[cpos[k]+anfa];
		  }   /* end block "put boundary condition as increment" */
		}   /* end block assemble BC in RHS */
	      }   /* end block "put boundary condition */
	    }   /* next unknown (row of the matrix) */
	  }   /* next node of this element */

	  if (AssFlag[ifs] & MAT_ASSEMBLE_MASK)       /* Assembling matrix */
	    {
	      if (LumpFlag[ifs] == STANDARD)
		{
		  ierr = MatSetValues (A[ifs],
				       nnpe*nnfa[ifs], ipos+ifs*mrhsedim,
				       nnpe*nnfa[ifs], ipos+ifs*mrhsedim,
				       ae_ins, ADD_VALUES);
		}
	      else if (LumpFlag[ifs] == NSBSLUMP)
		{
		  int *iposparc;
//		if (nnfa>0)PetscLogEventBegin(cmatseti,0,0,0,0);
		  iposparc = (int *) malloc (nnpe*nnfa[ifs]*sizeof(int));
		  for (k=0; k<nnpe; k++)
		    for (l=0; l<nnfa[ifs]; l++)
		      {
			if (l < nnfa[ifs]-1)  /* velocity component */
			  {
			    int ii;
			    for (ii=0; ii<nnpe; ii++) /* select columns */
			      {
				iposparc[ii] = ipos[l+ii*nnfa[ifs]];
				ae_ins[(k*nnfa[ifs]+l)*nnpe*nnfa[ifs] + ii] =
				  ae_ins[(k*nnfa[ifs]+l)*nnpe*nnfa[ifs] +
					 l+ii*nnfa[ifs]];
			      } // Esto qued'o mal !!!!!!!!!!
			    ierr = MatSetValues(A[ifs],
						1, ipos+
						   ifs*mrhsedim+k*nnfa[ifs]+l,
						nnpe, iposparc,
						ae_ins+
						ifs*mrhsedim*mrhsedim+
						(k*nnfa[ifs]+l)*nnpe*nnfa[ifs],
						ADD_VALUES);
			    CHKERRABORT(PETSC_COMM_WORLD,ierr);
			  }
			else  /* pressure */
			  {
			    ierr = MatSetValues(A[ifs],
						1, ipos+
						   ifs*mrhsedim+k*nnfa[ifs]+l,
						nnpe*nnfa[ifs],
						ipos+ifs*mrhsedim,
						ae_ins+
						ifs*mrhsedim*mrhsedim+
						(k*nnfa[ifs]+l)*nnpe*nnfa[ifs],
						ADD_VALUES);
			    CHKERRABORT(PETSC_COMM_WORLD,ierr);
			  }
		      }
		  free (iposparc);
//		if (nnfa>0)PetscLogEventEnd(cmatseti,0,0,0,0);
		}
	    } /* end block assembling matrix */

	  if (AssFlag[ifs] & RHS_ASSEMBLE_MASK){       /* Assembling RHS */
	    ierr = VecSetValues
	      (b[ifs], nnpe*nnfa[ifs], ipos+ifs*mrhsedim, rhse+ifs*mrhsedim,
	       ADD_VALUES); CHKERRABORT(PETSC_COMM_WORLD,ierr);
	  }

	  /* Ensamblaje parcial */
	  if ((AssFlag[ifs] & MAT_ASSEMBLE_MASK) &&
	      npass > 1 && !((iel+1) % dpass)) {
	    ierr = MatAssemblyBegin(A[ifs], MAT_FLUSH_ASSEMBLY);
	    ierr = MatAssemblyEnd  (A[ifs], MAT_FLUSH_ASSEMBLY);
	    ipass++;
/*	  printf("Parc. assembly %d/%d, proc: %d\n", ipass, npass, ident); */
	  }
	}   /* End loop over fractional steps to assembly */

	for (k = 0; k < nRV; k++)
	  {
	    if (typered[k] == (int) MPI_SUM)
	      rvals[k] += evals[k];
	    else if (typered[k] == (int) MPI_MAX){
	      if (rvals[k] < evals[k])
		rvals[k] = evals[k];
	    }
	    else if (typered[k] == (int) MPI_MIN){
	      if (rvals[k] > evals[k])
		rvals[k] = evals[k];
	    }
	    else if (typered[k] == (int) MPI_PROD)
	      rvals[k] *= evals[k];
	    else             /* OR */
	      if (evals[k] != 0.0)
		rvals[k] = 1.0;
	  }
      }   /* End loop over elements of this group */
    }   /* End if (integrate this group) */
  }   /* End loop over groups of elements */

  for (ifs = 0; ifs < nFS; ifs++)
    while ((AssFlag[ifs] & MAT_ASSEMBLE_MASK) && npass > 1 && ipass < npass)
      {
	ierr = MatAssemblyBegin(A[ifs], MAT_FLUSH_ASSEMBLY);
	ierr = MatAssemblyEnd(A[ifs], MAT_FLUSH_ASSEMBLY);
	ipass++;
/*      printf("Parc. assembly(c) %d/%d, proc: %d\n", ipass, npass, ident); */
      }
/*    printf ("\n %d cond. de Dirichlet \n",kkdir) ;*/

}   /* End function internal assembly */

/*------------------------------------------------------------------------
  Function ExternalAssembly - Assembly the external elements of a FEM Matrix 
  Version 2
  Programmed  by  Adrian Lew -  7/03/98 
  Modified by EAD, 2004/12/16

  This function is thought for being used only from assembly. Its task is
to make the loop over the external elements of each group to assembly their 
contributions to the FEM matrix. 
       The memory is allocated outside this function because the same memory is
 used in several different calls.
 
Variable names:
    onelem: pointer to the elemental operations function
    Pa: mesh data
    Sq: qudrature data
    bcccond: boundary conditions vector
    bccvalue: boundary condition values vector
    nfields: total number of different fields (scalars) 
    nnfa: number of fields to assembly (1<= nfa <=nfields)
    nfa: array with the numbers of the fields to assembly 
    ipos: array mapping element numbering to Mat A numbering 
    gpos: array mapping element numbering to global numbering 
    lpos: array mapping element numbering to the local of nodes order in 
          the data vactors (cogeo..,var...) 
    cpos: array mapping element numbering to the position of varaux at wich 
          the data of that node starts.
    varaux: array for passing the values of varscan to onelem.
    varaaux: ?
    varscan: last step solution, or more generally, data to calculate the 
             matrix and rhs coefficients.
    varextscan: the same as varscan but for the external nodes values.
    varascan: ?
    varaextscan: ?
    coordaux: array for passing the coordinates values to onelem.
    ae: elemental matrix
    rhse: elemental RHS
    ae_ins: ?

    Aflag: flag for recalculating the matrix A
    Lflag: lumping flag
    GrpInt: array with the switches to integrate or not the different groups.
    A: matrix to fill
    b: right hand side vector to fill
    dp: array for contruct the FEM basis function derivatives
    integrals: array for acumulate the integral values in the loop  over 
               the elements
    inte: elementary integral contribution
    nintgr: number of integrals to calculate

    lnobscan: switch of oblique nodes 
             (pointer over local part of distributed Vec)
    lnobextscan: switch of oblique nodes (external nodes)
    gbasescan: oblique nodes basis
    gbaseaux: array for passing values of gbase to elemental routines
    
This arguments are for the onelem routine:
    parcon: value of the continuation parameter
    delcon: value of the continuation parameter step
    parnum: array with numerical parameters
    parmat: array with material parameters
    pargen: array with general parameters
    isim: symmetry indicator

    
Remember:
    The nodes numbers begin with 1, not with 0. So, in the first nfields
 positions of v are the values for the node number 1.
    In nfa, field is the number of the unknown.
-----------------------------------------------------------------------------*/

#undef __SFUNC__
#define __SFUNC__ "ExternalnAssembly"

void ExternalnAssembly
  (nF1 onelem, partition *Pa, systemquad *Sq, int *bcccond, 
   PetscScalar *bccvalue,
   int nfields, int nFS, int *nnfa, int *ifa,
   int *ipos,  int *gpos, int *lpos, int *cpos,
   PetscScalar *varaux, PetscScalar *varaaux,
   PetscScalar *varscan, PetscScalar *varextscan,
   PetscScalar *varascan, PetscScalar *varaextscan,
   PetscScalar *coordaux, PetscScalar *ae, PetscScalar *ae_ins, int maedim,
   PetscScalar *rhse, int mrhsedim,
   double *parcon, double *delcon,
   double *parnum, double *parmat, double *pargen, int *isim,
   int *AssFlag, int *LumpFlag,
   int *GrpInt, Mat *A, Vec *b, PetscScalar *dp,
   int nRV, PetscScalar *rvals, PetscScalar *evals, int *typered,
   PetscScalar *lnobscan, PetscScalar *lnobextscan,
   PetscScalar *gbasescan, PetscScalar *gbaseaux)
{
  int nnpe, nsd, nvar, ngr;
  int ifs, i, j, k, l, m, i2, ind;
  int tmpint1, tmpint2;
  int ierr;
  int inbe;        /* index at the origin of the current element nodes */
  int igr;         /* group index */
  int iel;         /* element index */
  int AFlag;       /* result of a conditional */
  int iswnob;      /* Switch: element contains O.N. */
  int posgbase;    /* Switch: node is an O.N. if posgbase != 0 */
  int anfa;        /* valor absoluto de nfa[l] */
  int size, ipass, npass; /* Numero de procesadores, partial assemblies */
  int ident, dpass;  /* Numero de elementos entre cada ensamblaje parcial */
  int lob;           /* position of oblique field */
  double vecaux[Pa->nsd];
  
  PetscScalar   tmp;
  static int firsttime3=1,cmatsete;

#ifdef DEBUG_ASSEMBLY
  static int dbg_time=1;
  int prstart=3, prend=4;
  char dbg_file[20];
/*  static FILE* dbg_fp; */
#endif

  if (firsttime3)
    {
      firsttime3=0;
      PetscLogEventRegister(&cmatsete,"matset external",PLER_RED);
    }

  nnpe    = Pa->nnpe;
  nsd     = Pa->nsd;
  nvar    = Sq->nvar;
  ngr     = Pa->ngroups;

#ifdef DEBUG_ASSEMBLY
  if (dbg_time == 1) {
     MPI_Comm_rank (Pa->Comm, &ident);
     sprintf(dbg_file, "dbg_matelem%d", ident);
/*     dbg_fp = fopen(dbg_file, "w"); */
     fprintf(dbg_fp, "External Assembly:\n");
     fprintf(dbg_fp, "Mat. elem., elements with oblique nodes:\n");
  }
#endif

  /* calcula cada cuanto hacer los ensamblajes parciales */
  MPI_Comm_size (Pa->Comm, &size);
  MPI_Comm_rank (Pa->Comm, &ident);
  ipass = Pa->nelemext / 250;
  MPI_Allreduce(&ipass, &npass, 1, MPI_INT, MPI_MAX, Pa->Comm);
  if (npass > 1)
    {
      dpass = Pa->nelemext / npass + 1;
/*      printf(" (ext) Proc: %d, npass: %d, dpass: %d\n", ident, npass, dpass); */
      ipass = 0;
    }

  /* Loop over mesh external elements groups  */
  iel = 0;
  for (igr = 0; igr < ngr; igr++) {   /* Loop over groups of elements */
    if (GrpInt[igr]) {   /* This group is integrated */
      for (i = Pa->ExtGrIndex[igr]; i < Pa->ExtGrIndex[igr+1]; i++, iel++){
	                             /* Loop over elements of this group */
        if (Pa->ExtOwnership[iel] == 1)
          pargen[8] = 1.0; /* This element belongs to this processor */
	else
	  pargen[8] = 0.0;

	inbe = iel * nnpe;
	for (ifs = 0; ifs < nFS; ifs++)
	  for (k = 0; k < nnpe; k++) {   /* Loop over nodes of this element */
	    for (j = 0; j < nnfa[ifs]; j++) {/*Loop over fields of this node*/
	      m = ifs * mrhsedim + k * nnfa[ifs] + j;
	      ipos[m] = Pa->ExtMesh[inbe+k]-1;  
	      if (ipos[m] < 0)
		ipos[m] = Pa->Glob[-ipos[m]-1];

	      ipos[m] = ipos[m] * nnfa[ifs] + j;
	    }
	  }

	/* Oblique nodes processing */
        iswnob = 0;
        if (lnobscan) {             /* Oblique nodes in mesh */
	  for (k = 0; k < nnpe; k++) {
	    j = Pa->ExtMesh[inbe+k] - 1;
	    if (j < 0)  /* External node */
	      posgbase = lnobextscan[-j-1];
	    else        /* Local node */
	      posgbase = lnobscan[j+1-(Pa->first)];

	    if (posgbase)
	      iswnob = 1;
	  }
	  if (iswnob) {             /* Oblique nodes in this element */
            for (k = 0; k < nnpe; k++){
	      j = Pa->ExtMesh[inbe+k]-1;
	      if (j < 0)  /* External node */
		posgbase = lnobextscan[-j-1];
	      else        /* Local node */
		posgbase = lnobscan[j+1-(Pa->first)];

	      if (posgbase) {        /* This node is oblique */
		for (j = 0; j < nsd; j++){
		  for (i2 = 0; i2 < nsd; i2++){
		    gbaseaux[k + nnpe*j + i2*nnpe*nsd] = 
	  //  gbasescan[(posgbase-1)*nsd*nsd + j*nsd + i2]; GCB, 3 dec 99
		      gbasescan[(posgbase-1) + j*nsd + i2];
		  }
	/*	for (j = 0; j < nsd*nsd; j++){
		  gbaseaux[k*nsd*nsd + j] = 
		  //  gbasescan[(posgbase-1)*nsd*nsd + j]; GCB, 3 dec 99
		    gbasescan[(posgbase-1) + j]; */ //GCB 3 dec 99
		}
	      }
	      else {               /* This one is normal */
		for (j = 0; j < nsd; j++){
		  for (i2 = 0; i2 < nsd; i2++){
		    gbaseaux[k + nnpe*j + i2*nnpe*nsd] = 
		      (i2 == j) ? 1.0 : 0.0;
		  }
		}
	/*	for (j = 0; j < nsd*nsd; j++)
		  gbaseaux[k*nsd*nsd + j] = (j % (nsd+1)) ? 0.0 : 1.0; GCB, 3 dec 99 */
	      }   /* end if ( This node is oblique ) */
	    }   /* end loop over nodes in this element */
	  }   /* end if ( Oblique nodes in this element ) */
        }   /* end if ( Oblique nodes in mesh ) */

	for (k = 0; k < nnpe; k++) {
	  gpos[k] = Pa->ExtMesh[inbe+k] - 1;
	  if (gpos[k] < 0) {
	    lpos[k] = -(gpos[k] + 1);
	    cpos[k] = lpos[k] * nfields;

	    tmpint2 = 0;
	    for (j = 0; j < nvar; j++) {
	      tmpint1 = Sq->icomp[j];
	      for (l = 0; l < tmpint1; l++) {
		varaux[k+j*nnpe+l*nnpe*nvar] = varextscan[cpos[k]+tmpint2];
		varaaux[k+j*nnpe+l*nnpe*nvar] = varaextscan[cpos[k]+tmpint2];
		tmpint2++;
	      }
	    }
	    for (j = 0; j < nsd; j++)
	      coordaux[k+j*nnpe] = (Pa->cogeoext)[nsd*lpos[k]+j];
	  }
	  else{
	    lpos[k] = gpos[k] + 1 - (Pa->first);
	    cpos[k] = lpos[k] * nfields;  
	                            /* nfields*gpos[k]=low+lpos[k]*nfields */

	    tmpint2 = 0;
	    for (j = 0; j < nvar; j++){
	      tmpint1 = Sq->icomp[j];
	      for (l = 0; l < tmpint1; l++){
		varaux[k+j*nnpe+l*nnpe*nvar] = varscan[cpos[k]+tmpint2];
		varaaux[k+j*nnpe+l*nnpe*nvar] = varascan[cpos[k]+tmpint2];
		tmpint2++;
	      }
	    }
	    for (j = 0; j < nsd; j++)
	      coordaux[k+j*nnpe] = (Pa->cogeoloc)[nsd*lpos[k]+j];
	  }
	}

	tmpint1 = igr + 1;
	
	/* Elemental operations */
	AFlag = (AssFlag[ifs] & MAT_ASSEMBLE_MASK) ? 1 : 0;
	(*onelem)(varaux, varaaux, coordaux, &nvar, Sq->icomp, &nnpe, &nsd, 
		  ae, rhse, parcon, delcon, parnum, parmat, pargen, 
		  &(Sq->ngau), Sq->kint, &(Sq->itypegeo), &iel, Sq->nod,
		  &(Sq->maxnpe), &(Sq->maxgau), Sq->wgp, isim, Sq->p,
		  Sq->dpl, dp, &mrhsedim, &AFlag, &tmpint1,
		  &nRV, evals, typered, &iswnob, gbaseaux);

	/* Global assembly, skip external nodes rows  assembly */
//        PetscLogEventBegin(cmatsete,0,0,0,0);

	if (iswnob) {
#ifdef DEBUG_ASSEMBLY
	  /* Elemental matrices
	  if (dbg_time >= prstart && dbg_time <= prend) {
	    fprintf(dbg_fp, "\nElement: %d\n", iel);
	    for (k = 0; k < rhsedim; k++) {
	      for (j = 0; j < rhsedim; j++)
	        fprintf (dbg_fp, " %g", ae[k*rhsedim+j]);
	      fprintf (dbg_fp, "\n");
	    }
	  }
	  */
	  if (dbg_time >= prstart && dbg_time <= prend) {
	    fprintf(dbg_fp, "\nElement: %d, gbaseaux:\n", iel);
	    for (k = 0; k < nnpe; k++) {
	      for (j = 0; j < nsd; j++)
		for (l = 0; l < nsd; l++)
		  fprintf (dbg_fp, " %g", gbaseaux[k+nnpe*j+nnpe*nsd*l]);
	      fprintf (dbg_fp, "\n");
	    }
	  }
#endif
	  lob = -1;
	  for (l = 0; l < nnfa[0] && lob == -1; l++){   /* Find first oblique field */
	    if (ifa[l] < 0){
	      lob = l;
	      if (ifa[l+1] > 0) lob = -1;
	      if (nsd >= 3 && ifa[l+2] > 0) lob = -1;
	    }
	  }
	}

	for (ind = ifs = 0; ifs < nFS; ind +=nnfa[ifs], ifs++) {
	                                      /* Loop over fractional steps */
	  for (k = 0; k < nnpe; k++) {   /* Nodes of this element */
	    if (iswnob && lob != -1) {
	      j = Pa->ExtMesh[inbe+k]-1;
	      if (j < 0)  /* External node */
		posgbase = lnobextscan[-j-1];
	      else        /* Local node */
		posgbase = lnobscan[j+1-(Pa->first)];
	      if (posgbase) {
		/* There are oblique nodes in this element,
		   and there is an oblique
		   field in this fractional step, and k is an oblique node */
		/* ... un-rotate previous solution at node k */
		anfa = ifa[lob] < 0 ? -(ifa[lob]+1)-1 : ifa[lob];
		for (j = 0; j < nsd; j++)
		  {
		    vecaux[j] = 0;
		    for (i2 = 0; i2 < nsd; i2++)
		      vecaux[j] +=         varscan[cpos[k]+anfa+i2] *
			gbaseaux[k + nnpe*j + i2*nnpe*nsd];
		  }
	      }
	    }

	    for (l = 0; l < nnfa[ifs]; l++){/*Fields of the node (matrix row)*/
	      m = k*nnfa[ifs]+l;  /* m is the elemental matrix row */
	      anfa = ifa[ind+l] < 0 ? -(ifa[ind+l]+1)-1 : ifa[ind+l];

	      if (gpos[k] >= 0) {   /* Local node => local row */
		if (bcccond[cpos[k]+anfa] == 0 ||
		    !(AssFlag[ifs] & BCC_ASSEMBLE_MASK)) {
		  /* No Dirichlet condition or disable BC assembly */
		  if (AssFlag[ifs] & MAT_ASSEMBLE_MASK) {
		    if (LumpFlag[ifs]==STANDARD) {            /* Not Lumped */
		      for (j = 0; j < nnpe*nnfa[ifs]; j++) {
			ierr = MatSetValues(A[ifs],
					    1, &ipos[ifs*mrhsedim+m],
					    1, &ipos[ifs*mrhsedim+j], 
					    &ae[ifs*mrhsedim*mrhsedim+
						j*mrhsedim+
						m], ADD_VALUES);
		      }
		    }
		    else if (LumpFlag[ifs]==STANDARD_UNCOUPLED) { /* Uncoupled
								     fields: */
		      for (j = 0; j < nnpe; j++) {
			i2 = j*nnfa[ifs] + l;
			tmp = ae[ifs*mrhsedim*mrhsedim + i2*mrhsedim + m];
			ierr = MatSetValues(A[ifs],
					    1, &ipos[ifs*mrhsedim+m],
					    1, &ipos[ifs*mrhsedim+i2], 
					    &tmp, ADD_VALUES);
		      }
		    }
		    else if (LumpFlag[ifs]==NSBSLUMP) { /* Special NSBS
							   "lumping" */
		      for (j = 0; j < nnpe*nnfa[ifs]; j++)
			if (l==nnfa[ifs]-1 || (j-l)%nnfa[ifs] == 0) {
			  ierr = MatSetValues(A[ifs],
					      1, &ipos[ifs*mrhsedim+m],
					      1, &ipos[ifs*mrhsedim+j],
					      &ae[ifs*mrhsedim*mrhsedim+
						  j*nnpe*nnfa[ifs]+
						  m], ADD_VALUES);
			}
		    }
		    else if (LumpFlag[ifs]==LUMPED){	  /* Lumped */
		      ierr = MatSetValues(A[ifs],
					  1, &ipos[ifs*mrhsedim+m],
					  1, &ipos[ifs*mrhsedim+m], 
					  &ae[ifs*mrhsedim*mrhsedim+
					      m*nnpe*nnfa[ifs]+
					      m], ADD_VALUES);   
		    }
		    else {   /* safety */
		      error(EXIT, "assembly.c: Unrecognized lumping flag");
		    }
		  } /* end if (must assemble matrix) */

		  if (AssFlag[ifs] & RHS_ASSEMBLE_MASK) {
		    ierr = VecSetValues (b[ifs], 1,
					 &ipos[ifs*mrhsedim+m],
					 &rhse[ifs*mrhsedim+m], ADD_VALUES); 
		  }
		} /* end block (this row has no BC) */
		else {  /* Dirichlet condition */
		  if (AssFlag[ifs] & MAT_ASSEMBLE_MASK) { /* Matrix diagonal */
		    tmp = 1.;
		    ierr = MatSetValues (A[ifs],
					 1, &ipos[ifs*mrhsedim+m],
					 1, &ipos[ifs*mrhsedim+m],
					 &tmp, ADD_VALUES);
		  }
		  if (AssFlag[ifs] & RHS_ASSEMBLE_MASK) { /* RHS */
		    if (!(AssFlag[ifs] & BCC_DELTA_MASK)) /* BC: field */
		      ierr = VecSetValues (b[ifs], 1,
					   &ipos[ifs*mrhsedim+m],
					   &bccvalue[cpos[k]+anfa],
					   ADD_VALUES);
		    else {                                /* BC: increment */
		      if (iswnob && lob != -1 && l >=lob && l < lob + nsd)
			tmp = bccvalue[cpos[k]+anfa] - vecaux[l-lob];
		      else
			tmp = bccvalue[cpos[k]+anfa] - varscan[cpos[k]+anfa];
		      ierr = VecSetValues (b[ifs], 1,
					   &ipos[ifs*mrhsedim+m],
					   &tmp, ADD_VALUES);
		    }
		  }
		}   /* end if Dirichlet condition */
	      }   /* End if (row is local) */
	    }   /* End loop over fields of this node (matrix rows) */
	  }   /* End loop over nodes of this element */

//        PetscLogEventEnd(cmatsete,0,0,0,0);

	  /* Ensamblaje parcial */
	  if ((AssFlag[ifs] & MAT_ASSEMBLE_MASK) &&
	      npass > 1 && !((iel+1) % dpass)) {
	    ierr = MatAssemblyBegin(A[ifs], MAT_FLUSH_ASSEMBLY);
	    ierr = MatAssemblyEnd  (A[ifs], MAT_FLUSH_ASSEMBLY);
/*	  printf("Parc. assembly %d/%d, proc: %d\n", ipass, npass, ident); */
	    ipass++;
	  }
	}   /* End loop over fractional steps to assembly */

	if (Pa->ExtOwnership[iel] == 1)
	  for (k = 0; k < nRV; k++)
	    {
	      if (typered[k] == (int) MPI_SUM)
		rvals[k] += evals[k];
	      else if (typered[k] == (int) MPI_MAX){
		if (rvals[k] < evals[k])
		  rvals[k] = evals[k];
	      }
	      else if (typered[k] == (int) MPI_MIN){
		if (rvals[k] > evals[k])
		  rvals[k] = evals[k];
	      }
	      else if (typered[k] == (int) MPI_PROD)
		rvals[k] *= evals[k];
	      else             /* OR */
		if (evals[k] != 0.0)
		  rvals[k] = 1.0;
	    }
      }   /* End loop over elements of this group */
    }   /* End if (integrate this group) */
  }   /* End loop over groups of elements */

  for (ifs = 0; ifs < nFS; ifs++)
    while ((AssFlag[ifs] & MAT_ASSEMBLE_MASK) && npass > 1 && ipass < npass)
      {
	ierr = MatAssemblyBegin(A[ifs], MAT_FLUSH_ASSEMBLY);
	ierr = MatAssemblyEnd(A[ifs], MAT_FLUSH_ASSEMBLY);
	ipass++;
/*      printf("Parc. assembly(c) %d/%d, proc: %d\n", ipass, npass, ident); */
      }

#ifdef DEBUG_ASSEMBLY
  if (dbg_time == prend)
    fclose(dbg_fp);
  dbg_time++;
#endif

}   /* End function external assembly */

/*
 * nsetbcond: put boundary conditions
 */
void nsetbcond (systemdata **fSy, Vec *fv,
		Mat *A, Vec *b, int *AssFlag,
		int nFS, int *nnfa, int *fifa, IS *isbc)
{
  Vec      v;

  MPI_Comm Comm;        /* Communicator of Partition */
  int      ierr;        /* error checking variable */
  int      i;           /* loop counters */
  int      ipos;
  PetscScalar   *varscan;  /* pointer for scanning the v values */
  PetscScalar   *bscan;    /* pointer for scanning the b values */

  PetscScalar tmp;      /* temporary variable */

  int *ifa, nvar, maxcomp, nsd, nfields;
  int inod, nodloc, anfa, totnfa, ind, ifs;
  int nbc, *ibcc;
  systemdata   *Sy;

  v = *fv;
  Sy = *fSy;

  /* Take care of this in the case that Pa->nnpe were 
     different from Sq->maxnpe */
  /* Note: 
     The nsd, nvar and maxcomp values of Pa and SurPa should be the same,
     so, there is no verification of consistency at all. */
  Comm    = Sy->Pa->Comm;
  nsd     = Sy->Pa->nsd;
  maxcomp = Sy->Sq->maxcomp;
  nvar    = Sy->Sq->nvar;
  nfields = Sy->nfields;
  nodloc  = Sy->Pa->nodloc;

  /* Translate fifa to ifa (fortran->C numbering) */
  totnfa = 0;
  for (i = 0; i < nFS; i++)
    totnfa += nnfa[i];
  ifa = (int *) malloc (totnfa * sizeof(int));
  if (ifa == (int *)NULL)
    error (EXIT, "Not enough memory for ifa");

  for (i = 0; i < totnfa; i++)
    ifa[i] = fifa[i] - 1;

  ierr = VecGetArray (v, &varscan);        CHKERRABORT(PETSC_COMM_WORLD,ierr);

  for (ind = ifs = 0; ifs < nFS; ind += nnfa[ifs], ifs++)
    {
      if (!(AssFlag[ifs] & MAT_ASSEMBLE_MASK) &&
	  !(AssFlag[ifs] & RHS_ASSEMBLE_MASK))
	continue;
      ierr = VecGetArray (b[ifs], &bscan); CHKERRABORT(PETSC_COMM_WORLD,ierr);

      for (inod = 0; inod < nodloc; inod++)
	{
	  for (i = 0; i < nnfa[ifs]; i++)
	    {
	      anfa = ifa[ind+i] < 0 ? -(ifa[ind+i]+1)-1 : ifa[ind+i];
	      ipos = inod * nfields + anfa;
	      if (Sy->bcccond[ipos])
		{
		  tmp = Sy->bccvalue[ipos];
		  if (AssFlag[ifs] & RHS_ASSEMBLE_MASK)
		    {
		      if (AssFlag[ifs] & BCC_DELTA_MASK)
			bscan[inod*nnfa[ifs]+i] = tmp - varscan[ipos];
		      else
			bscan[inod*nnfa[ifs]+i] = tmp;
		    }
		} /* end if (has boundary condition) */
	    } /* end loop over unknowns of this FS */
	} /* end loop over local nodes */

      ierr =VecRestoreArray(b[ifs],&bscan); CHKERRABORT(PETSC_COMM_WORLD,ierr);
      ierr = VecRestoreArray (v, &varscan); CHKERRABORT(PETSC_COMM_WORLD,ierr);

      if (AssFlag[ifs] & MAT_ASSEMBLE_MASK)
	{
	  if (isbc[ifs] == PETSC_NULL)
	    {
	      /* count number of boundary conditions */
	      nbc = 0;
	      for (inod = 0; inod < nodloc; inod++)
		{
		  for (i = 0; i < nnfa[ifs]; i++)
		    {
		      anfa = ifa[ind+i] < 0 ? -(ifa[ind+i]+1)-1 : ifa[ind+i];
		      ipos = inod * nfields + anfa;
		      if (Sy->bcccond[ipos])
			nbc++;
		    }
		}
	      ibcc = (int *) malloc (nbc * sizeof(int));
	      /* load vector of boundary conditions */
	      nbc = 0;
	      for (inod = 0; inod < nodloc; inod++)
		{
		  for (i = 0; i < nnfa[ifs]; i++)
		    {
		      anfa = ifa[ind+i] < 0 ? -(ifa[ind+i]+1)-1 : ifa[ind+i];
		      ipos = inod * nfields + anfa;
		      if (Sy->bcccond[ipos])
			ibcc[nbc++] = inod*nnfa[ifs] + i +
			              nnfa[ifs]*(Sy->Pa->first-1);
		    }
		}
	      ISCreateGeneral(Sy->Pa->Comm, nbc, ibcc, &isbc[ifs]);      
	      free (ibcc);
	    }
	  tmp = 1.0;
	  MatZeroRows (A[ifs], isbc[ifs], &tmp);
	} /* end if (set conditions on matrix) */
    } /* end loop over FS */
  free (ifa);
}

#include <stdlib.h>
#include "assembly.h"
//#include "prof_utils.h"

/*---------------------------------------------------------------------------
  Function NodalComp: Compute fields node by node

  Programmed  by Enzo Dari, 2004/12/02

  This function is usually called from FORTRAN (gpmain.F)

  Arguments:
    Sy: general info about system
    v: last step solution
    compfunc: pointer to the computation function
    nfa: array with the numbers of the fields to assembly
    nnfa: number of fields to use (1<= nfa <=nfields)
    parcon: value of the continuation parameter
    delcon: value of the continuation parameter step
    parnum: array with numerical parameters
    parmat: array with material parameters
    pargen: array with general parameters
    isim: symmetry indicator
  ---------------------------------------------------------------------------*/

#undef __SFUNC__
#define __SFUNC__ "nodalcomp"

void nodalcomp (systemdata **fSy, Vec *fv, FNC compfunc,
	       int *fnfa, int *fnnfa, double *parcon, double *delcon, 
	       double *parnum, double *parmat, double *pargen, 
	       int *isim)
{
  Vec      v;
  int      ierr;        /* error checking variable */

  PetscScalar   *varscan;    /* pointer for scanning the v values */
  PetscScalar   *varascan;   /* pointer for scanning the va values */

  int nodloc, nsd, nfields;
  systemdata *Sy;

  v = *fv;
  Sy = *fSy;

  nsd     = Sy->Pa->nsd;
  nodloc  = Sy->Pa->nodloc;
  nfields = Sy->nfields;

  ierr = VecGetArray
    (v, &varscan);                   CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGetArray
    (Sy->va, &varascan);             CHKERRABORT(PETSC_COMM_WORLD,ierr);

  (*compfunc) (&nodloc, Sy->Pa->cogeo,
	       varscan, varascan, fnnfa, fnfa, &nfields, &nsd,
	       parcon, delcon, parnum, parmat, pargen, isim);

  ierr = VecRestoreArray
    (v, &varscan);                   CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecRestoreArray
    (Sy->va, &varascan);             CHKERRABORT(PETSC_COMM_WORLD,ierr);
}

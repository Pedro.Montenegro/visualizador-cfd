#include <stdlib.h>
#include "par_updabc.h"

/*------------------------------------------------------------------------
        Function par_updabc - Modifies the boundary conditions
	Programmed  by  Adrian Lew -  8/04/98 

       This function must be called from FORTRAN. It has to call the FORTRAN 
routine that will modify the BC.

Variable names:
    updatebc: pointer to the function to call
    Sy: system structure
    v: vector of unknowns
    parcon: continuation parameter value
    parmat: array of material parameters
    parnum: array of numerical parameters
    pargen: array with general parameters
    flag: flag equal to BC_CONSTANT or BC_VARIABLE.
-----------------------------------------------------------------------------*/
#undef __SFUNC__
#define __SFUNC__ "par_updabc  "

#if defined(PRE_2_0_24)
void par_updabc(F2 updatebc, systemdata **fSy, int *fv, double *fparcon, 
		double *parmat, double *parnum, double *pargen, int *flag)
{
#else
void par_updabc(F2 updatebc, systemdata **fSy, Vec *fv, double *fparcon, 
		double *parmat, double *parnum, double *pargen, int *flag)
{
#endif
  Vec          v;

  systemdata   *Sy;        
  
  int          nodloc;     /* number of local nodes */
  int          nfields;    /* number of scalars per node */
  int          *bcccond;   /* boundary conditions flag */
  int          *icomp;     /* number of components of each var */
  int          nvar;       /* number of variables */
  int          nsd;        /* number of spatial dimensions */
  double       *bccvalue;  /* boundary condition value */
  double       parcon;     
  double       *cogeoloc;  /* coordinates of the local nodes */
  PetscScalar  *vscan;     /* array for scanning v */
  int          ierr;
  /*  int i, nbc, *ibcc, bc_changed;
  static int   firsttime = 1;
  */
  if(*flag==BC_CONSTANT/* && !firsttime*/)
    return;

  /* Initializing */
  Sy = *fSy;
  v = *fv;
  parcon   = *fparcon;

  bcccond  = Sy->bcccond;
  bccvalue = Sy->bccvalue;
  nodloc   = Sy->Pa->nodloc;
  cogeoloc = Sy->Pa->cogeo;
  nsd      = Sy->Pa->nsd;
  nfields  = Sy->nfields;
  nvar     = Sy->Sq->nvar;
  icomp    = Sy->Sq->icomp;

  ierr = VecGetArray(v, &vscan); CHKERRABORT(PETSC_COMM_WORLD,ierr);

  (*updatebc)(&nodloc, &nfields, bcccond, bccvalue, &nvar, icomp,  
	    &parcon, parmat, parnum, pargen, vscan, cogeoloc, &nsd);

  ierr = VecRestoreArray(v, &vscan);
}

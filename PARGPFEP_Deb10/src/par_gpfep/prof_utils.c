#include "mpi.h"
#include "petscsys.h"
#include "f2c_string.h"
#include "error.h"
#include <stdlib.h>
#include <stdio.h>
#include "prof_utils.h"
#include <unistd.h>
#include <string.h>
#include "tiempo.h"

static int stage = 0;
static double start_cpu_time[100];
static double start_usr_time[100];
static double final_cpu_time[100];
static double final_usr_time[100];
static char *Stage[100];

/*-------------------------------------------------------------------
           Function StageOn - Begin an stage trace
	   Programmed by Adrian Lew - Started: 12/7/97 Ended: 12/7/97

    This function must be called from FORTRAN. 

-------------------------------------------------------------------------*/ 
#undef __SFUNC__
#define __SFUNC__ "StageOn"

void StageOn(char *filename);

/* Fortran Interface */
void Fortran_StageOn(char *fStage, int *lenght)
{
  char  *filename;

  if(*lenght<0)
    error(EXIT,"Lenght less than zero");

  filename = (char *) malloc((*lenght)*sizeof(char));
  if(filename == (char *)NULL)
    error(EXIT,"Not enought memory to allocate");

  f2c_string(fStage, filename, *lenght);

  StageOn(filename);

  free(filename);
}

/* C Interface */
void StageOn(char *filename)
{/*
  int        ierr;

  stage++;
  if(stage >= 10)
    error(EXIT,"Attempt to profile more than ten stages, bad results");

  if(stage>= 100)
    error(EXIT,"Too many stages to timming");

  Stage[stage] = (char *) malloc((strlen(filename)+1)*sizeof(char));
  if(Stage[stage] == (char *)NULL)
    error(EXIT,"Not enought memory to allocate");

  strcpy(Stage[stage], filename);

  start_cpu_time[stage] = tiempo(USER_TIME);
  start_usr_time[stage] = tiempo(REAL_TIME);

  ierr = PetscLogStagePush(stage);        CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = PetscLogStageRegister
    (&stage,Stage[stage]);                CHKERRABORT(PETSC_COMM_WORLD, ierr);
 */
}
  

/*-------------------------------------------------------------------
           Function StageOff - Terminates an stage trace
	   Programmed by Adrian Lew - Started: 12/7/97 Ended: 12/7/97

    This function must be called from FORTRAN or C. 

-------------------------------------------------------------------------*/ 
#undef __SFUNC__
#define __SFUNC__ "StageOff"

void StageOff()
{
  PetscLogStagePop();
  final_cpu_time[stage] = tiempo(USER_TIME);
  final_usr_time[stage] = tiempo(REAL_TIME);
}

/*-------------------------------------------------------------------
           Function ResumeTimming - Send timming to file
	   Programmed by Adrian Lew - Started: 12/7/97 Ended: 12/7/97

    This function must be called from FORTRAN. 

-------------------------------------------------------------------------*/ 
#undef __SFUNC__
#define __SFUNC__ "ResumeTimming"

void ResumeTimming(char *ffilename, int *lenght, int *fmpn)
{
 FILE         *output;
 char         *filename;
 MPI_Comm     Comm;
 int          rank;
 int          size;
 int  ierr=0, mpn;
 int          i, j;
 int          namelenght;
 double       *cpu_time;
 double       *usr_time;
 double       *max_cpu_time = (double *)NULL;
 double       *min_cpu_time = (double *)NULL;
 double       *max_usr_time = (double *)NULL;
 double       *min_usr_time = (double *)NULL;
 double       *cpu_time_gather = (double *)NULL;
 char         **max_cpu_time_host = (char **)NULL;
 char         **min_cpu_time_host = (char **)NULL;
 char         *hostname_gather = (char *)NULL;
 char         hostname[300];
 

 mpn = *fmpn;
 Comm = MPI_COMM_WORLD;
 MPI_Comm_rank(Comm, &rank);                           
 MPI_Comm_size(Comm, &size);
 
 ierr += MPI_Bcast(&mpn, 1, MPI_INT, 0, Comm );
 if(mpn<0 || mpn >= size){
   error(EXIT, "Main processor number out of range ");
 }

 MPI_Get_processor_name(hostname, &namelenght);
 if(namelenght>49)
   hostname[49]='\0';

 cpu_time = (double *) malloc(stage * sizeof(double));
 if(cpu_time == (double *)NULL)
   error(EXIT,"Not enought memory to allocate");

 usr_time = (double *) malloc(stage * sizeof(double));
 if(usr_time == (double *)NULL)
   error(EXIT,"Not enought memory to allocate");


 for(i=1; i<=stage; i++){
   cpu_time[i-1] = final_cpu_time[i] - start_cpu_time[i];
   usr_time[i-1] = final_usr_time[i] - start_usr_time[i];
 }

 if(rank == mpn){
   max_cpu_time = (double *) malloc(stage * sizeof(double));
   if(max_cpu_time == (double *)NULL)
     error(EXIT,"Not enought memory to allocate");
   
   min_cpu_time = (double *) malloc(stage * sizeof(double));
   if(min_cpu_time == (double *)NULL)
     error(EXIT,"Not enought memory to allocate");
   
   max_usr_time = (double *) malloc(stage * sizeof(double));
   if(max_usr_time == (double *)NULL)
     error(EXIT,"Not enought memory to allocate");

   min_usr_time = (double *) malloc(stage * sizeof(double));
   if(min_usr_time == (double *)NULL)
     error(EXIT,"Not enought memory to allocate");

   min_cpu_time_host = (char **) malloc(stage * sizeof(char *));
   if(min_cpu_time_host == (char **)NULL)
     error(EXIT,"Not enought memory to allocate");

   max_cpu_time_host = (char **) malloc(stage * sizeof(char *));
   if(max_cpu_time_host == (char **)NULL)
     error(EXIT,"Not enought memory to allocate");

   for(i=1; i<=stage; i++){
     min_cpu_time_host[i-1] = (char *) malloc(50 * sizeof(char));
     if(min_cpu_time_host[i-1] == (char *)NULL)
       error(EXIT,"Not enought memory to allocate");

     max_cpu_time_host[i-1] = (char *) malloc(50 * sizeof(char));
     if(max_cpu_time_host[i-1] == (char *)NULL)
       error(EXIT,"Not enought memory to allocate");
   }

   cpu_time_gather = (double *) malloc(size * stage * sizeof(double));
   if(cpu_time_gather == (double *)NULL)
     error(EXIT,"Not enought memory to allocate");
   
   hostname_gather = (char *) malloc(50 * size * sizeof(char));
   if(hostname_gather == (char *)NULL)
     error(EXIT,"Not enought memory to allocate");
 }

 i = MPI_Gather(cpu_time, stage, MPI_DOUBLE, cpu_time_gather, stage, 
		MPI_DOUBLE, mpn, Comm);
 i = MPI_Gather(hostname, 50, MPI_CHAR, hostname_gather, 50, 
		MPI_CHAR, mpn, Comm);
 MPI_Reduce(usr_time, max_usr_time, stage,MPI_DOUBLE,MPI_MAX, mpn, Comm);
 MPI_Reduce(usr_time, min_usr_time, stage,MPI_DOUBLE,MPI_MIN, mpn, Comm);

 if(rank == mpn){
   for(i=0; i<stage; i++){
     min_cpu_time[i] = cpu_time_gather[i];
     max_cpu_time[i] = cpu_time_gather[i];
     strncpy(max_cpu_time_host[i], &(hostname_gather[0]), 50);
     strncpy(min_cpu_time_host[i], &(hostname_gather[0]), 50);
     for(j=1; j<size; j++){
       if(cpu_time_gather[i+j*stage]>max_cpu_time[i]){
	 max_cpu_time[i] = cpu_time_gather[i+j*stage];
	 strncpy(max_cpu_time_host[i], &(hostname_gather[j*50]), 50);
       }
       if(cpu_time_gather[i+j*stage]<min_cpu_time[i]){
	 min_cpu_time[i] = cpu_time_gather[i+j*stage];
	 strncpy(min_cpu_time_host[i], &(hostname_gather[j*50]), 50);
       }
     }
   }

   if(*lenght<0)
     error(EXIT,"Lenght less than zero");
   
   filename = (char *) malloc((*lenght)*sizeof(char));
   if(filename == (char *)NULL)
     error(EXIT,"Not enought memory to allocate");
   
   f2c_string(ffilename, filename, *lenght);
   
   output = (FILE *)fopen(filename,"w");
   if(output == (FILE *) NULL)
     error(EXIT,"Cannot open the output file");
   
   fprintf(output, "\n Stage\t\t\tMax CPU_Time\t Min CPU_Time \tMax USR_Time\t\
 Min USR_Time\t MaxCPUHostname\tMinCPUHostname\n");

   for(i=0;i<stage; i++)
     fprintf(output,"%s\t\t%g\t\t%g\t\t%g\t\t%g\t\t%s\t\t%s\n",Stage[i+1],
	     max_cpu_time[i]/1.e6, min_cpu_time[i]/1.e6, max_usr_time[i], 
	     min_usr_time[i], max_cpu_time_host[i], min_cpu_time_host[i]);

   fclose(output);

   free(max_cpu_time);
   free(min_cpu_time);
   free(max_usr_time);
   free(min_usr_time);
   for(i=0; i<stage; i++){
     free(max_cpu_time_host[i]);
     free(min_cpu_time_host[i]);
   }
   free(cpu_time_gather);
   free(hostname_gather);
   free(max_cpu_time_host);
   free(min_cpu_time_host);
   
   free(filename);
 }
 free(cpu_time);
 free(usr_time);
}

/*-------------------------------------------------------------------
           Function GetMemUsage - Get the memory usage
	   Programmed by Adrian Lew - Started: 12/7/97 Ended: 12/7/97

    This function must be called from C. 

    flag = APP, WRI, for appending to the file or writing on it
-------------------------------------------------------------------------*/ 

void GetMemUsage(char *filename, int mpn, MPI_Comm Comm, int flag);

#undef __SFUNC__
#define __SFUNC__ "Fortran_GetMemUsage"

void Fortran_GetMemUsage(char *ffilename, int *lenght, int *mpn, int *fflag)
{
  char  *filename;
  int   wflag; 

  if(*lenght<0)
    error(EXIT,"Lenght less than zero");

  filename = (char *) malloc((*lenght)*sizeof(char));
  if(filename == (char *)NULL)
    error(EXIT,"Not enought memory to allocate");

  f2c_string(ffilename, filename, *lenght);

  wflag = *fflag;
  GetMemUsage(filename, *mpn, MPI_COMM_WORLD, wflag);

  free(filename);
}

#undef __SFUNC__
#define __SFUNC__ "GetMemUsage"

/* C version */
void GetMemUsage(char *filename, int mpn, MPI_Comm Comm, int wflag)
{
  FILE         *output;
  FILE         *input;
  char         procfile[80];
  char         *buffer;
  int          rank;
  int          size;
  int          rss;                /* resident size */
  int          vms;                /*  virtual memory size */
  pid_t        pid;
  int          flag, i;
  int          *rss_array = (int *)NULL;
  int          *vms_array = (int *)NULL;
  int ierr=0;

  MPI_Comm_rank(Comm, &rank);                           
  MPI_Comm_size(Comm, &size);

  ierr += MPI_Bcast(&mpn, 1, MPI_INT, 0, Comm );
  if(mpn<0 || mpn >= size){
    error(EXIT, "Main processor number out of range ");
  }

  buffer = (char *) malloc(sizeof(char) * 300);
  if(buffer == (char *)NULL)
    error(EXIT,"Not enought memory to allocate");

  pid = getpid();
  sprintf(procfile,"/proc/%d/status",pid);
  input = (FILE *)fopen(procfile,"r");
  if(input == (FILE *) NULL)
    error(EXIT,"Cannot open the procces file");

  
  flag = 1;
  while(flag){
    fscanf(input,"%s",buffer);
    if(strncmp("VmSize",buffer,6)==0)
      flag=0;
  }

  fscanf(input,"%d",&vms);
  
  flag = 1;
  while(flag){
    fscanf(input,"%s",buffer);
    if(strncmp("VmRSS",buffer,5)==0)
      flag=0;
  }
    
  fscanf(input,"%d",&rss);

  fclose(input);

  if(rank == mpn){
    rss_array = (int *) malloc(size*sizeof(int));
    if(rss_array == (int *) NULL)
      error(EXIT,"Not enought memory to allocate");

    vms_array = (int *) malloc(size*sizeof(int));
    if(vms_array == (int *) NULL)
      error(EXIT,"Not enought memory to allocate");
  }
   
  i = MPI_Gather(&rss, 1, MPI_INT, rss_array, 1, MPI_INT, mpn, Comm);
  i = MPI_Gather(&vms, 1, MPI_INT, vms_array, 1, MPI_INT, mpn, Comm);

  
  if(rank == mpn){
    char   output_type[3];

    if(wflag==APP)
      strcpy(output_type ,"a");
    else
      strcpy(output_type , "w");

    output = (FILE *)fopen(filename, output_type);
    if(output == (FILE *) NULL)
      error(EXIT,"Cannot open the output file");

    rss = vms =0;
    fprintf(output,"\n\n Processor \tRSS \t\t\tVMS\n");
    for(i=0; i<size; i++){
      fprintf(output," %d \t\t%d kb \t\t%d kb\n", i, rss_array[i], 
	      vms_array[i]);

      rss += rss_array[i];
      vms += vms_array[i];
    }
    
    fprintf(output,"\nTotal \t\t%d kb \t\t%d kb\n", rss, vms);
    
    fclose(output);
    free(rss_array);
    free(vms_array);
  }
  free(buffer);
}

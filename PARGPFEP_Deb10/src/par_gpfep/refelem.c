#include "error.h"
#include <stdio.h>
#include <stdlib.h>

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#define quadrul quadrul_
#define shapef shapef_
#endif

#if defined (HAVE_FORTRAN_PRE_DOT)
#define quadrul .quadrul
#define shapef .shapef
#endif

void quadrul(int *, int *, int *, int *, int *, int *, double *, double *);

void shapef(double *, double *, double *, double *, int *, int *, int *, 
	     int *);

/*----------------------------------------------------------------------
    Function refelem - Set the important values of the reference element
    Programmed by Adrian Lew - Started: 19/11/97 - Finished: 19/11/97
    Modified from the routine elemat from gpfep.

    This function must be called from C. Its function is to call the fortran
routines to calculate several values. The returned arrays are fortran type 
arrays.
  
Variables names:
        nvar  : number of unknown fields
	maxnpe: maximum number of nodes per element
        ieltyp: geometric element type 
	itgdeg: maximum order of polynomial integrated exactly
	itgtyp: integration type
	itpcod: array containing the interpolation code used for each field
	itpgeo: interpolation code used for geometry
	nsd   : number of space dimensions
	kmesh : mesh corresponding to each field
	npe   : number of nodes per element in each mesh

Return values:
        ngau  : number of gauss points
	wgp   : array with the gauss points weights
	kint  : array with the interpolation type for each field
	itypegeo: type of geometric interpolation
	p     : array with the value of the interpolation functions
	dpl   : p's derivatives
	dpl2  : some other information
	nod   : array with the number of nodes in the mesh of each field
	maxgau: value of the constant utilized for dimensioning the arrays 

Note: 
   In this context nvar does not means the number of scalars per node but 
means the number of unknown fields. (Could be some scalars not used as unknown
fields, and some fields composed of one or more scalars)  

-----------------------------------------------------------------------*/
#undef __SFUNC__
#define __SFUNC__ "refelem "

void refelem(int nvar, int maxnpe, int ieltyp, int itgdeg, int itgtyp, 
	     int *itpcod, int itpgeo, int nsd, int *kmesh, int *npe, 
	     int *ngau, double **fwgp, int **fkint, int *itypegeo, 
	     double **fp, double **fdpl, double **fdpl2, int **fnod, 
	     int *fmaxgau)
{
  int       i;
  int       maxvar1;
  int       maxgau;

  double  *cog;      
  int     itype;
  int     icod;
  int     ivar;
  int     tmp;

  double  *wgp;
  double  *p;
  double  *dpl;
  double  *dpl2;
  int     *nod;
  int     *kint;

  maxvar1 = nvar + 1;
  maxgau  = *fmaxgau;

  *fwgp = (double *) malloc(maxgau * sizeof(double));
  if(*fwgp == (double *)NULL)
    error(EXIT,"Not enought memory to allocate");

  wgp = *fwgp;

  cog = (double *) malloc(maxgau*3* sizeof(double));
  if(cog == (double *)NULL)
    error(EXIT,"Not enought memory to allocate");

  /* making cog equal zero */
  /* This is done because when one has a triangle in a 3D space that is
     mapped from a reference element in the 2D plane it's nice to calculate
     the coordinates of the original element without taking care if it is a 2D
     or a 3D figure */

  for(i=0; i<maxgau*3; i++)
    cog[i]=0;

  quadrul(&ieltyp, &itgdeg, &itgtyp, &maxgau, ngau, &nsd, cog, wgp);
  icod      = 0;
  itype     = 0;
  *itypegeo = 0;
  *fmaxgau  = maxgau;

  /* PREGUNTAR ACA A GUSTAVO SOBRE EL TRES, ES NSD SUPONGO */

  *fp   = (double *) malloc(maxnpe * maxgau * maxvar1 * sizeof(double));
  if(*fp   == (double *)NULL)
    error(EXIT,"Not enought memory to allocate");

  *fdpl = (double *) malloc(maxnpe * maxgau * 3 * maxvar1 * sizeof(double));
  if(*fdpl == (double *)NULL) 
    error(EXIT,"Not enought memory to allocate");
			  
  *fdpl2= (double *) malloc(maxnpe * maxgau * 3 * 3 * maxvar1 *sizeof(double));
  if(*fdpl2== (double *)NULL)
    error(EXIT,"Not enought memory to allocate");

  *fkint = (int *) malloc(nvar * sizeof(int));
  if(*fkint == (int *)NULL)
    error(EXIT,"Not enought memory to allocate");

  p   = *fp;
  dpl = *fdpl;
  dpl2= *fdpl2;
  kint = *fkint;

  tmp = maxnpe * maxgau;

  for(ivar=0; ivar<nvar; ivar++){
    if(itpcod[ivar]!=icod){
      icod   = itpcod[ivar];
      itype += 1;
 
      shapef(cog, &p[tmp * (itype-1)], &dpl[tmp * 3 * (itype-1)], 
	      &dpl2[tmp * 3 * 3 * (itype-1)], ngau, &maxgau, 
	      &maxnpe, &icod);
      
      if(itpgeo == icod)
	*itypegeo = itype;
    }
    kint[ivar]=itype;
  }
  
  if(*itypegeo == 0){
    *itypegeo = ++itype;
    shapef(cog, &p[tmp * (*itypegeo-1)], &dpl[tmp * 3 * (*itypegeo-1)], 
	    &dpl2[tmp * 3 * 3 * (*itypegeo-1)], ngau, 
	    &maxgau, &maxnpe, &itpgeo);
  }

  free(cog);
  
  /* nod() initialization */
  *fnod = (int *) malloc(nvar * sizeof(int));
  if(*fnod == (int *)NULL)
    error(EXIT,"Not enough memory for fnod");

  nod  = *fnod;

  for(ivar=0; ivar<nvar; ivar++)
    nod[ivar] = npe[kmesh[ivar]-1]; /* Meshes are numbered from 1 on */
}

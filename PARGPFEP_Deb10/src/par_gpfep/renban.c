void renban (int nod, int *ien, int *jen, int *nv, int *nn)
{
  /*
    nod: n�mero de nodos a renumerar
    ien, jen: conectividad (puntero, vecinos)
    nv, nn: vectores de permutaci�n
    nv: numeraci�n inicial (se sobreescribe)
    nn: numeraci�n final (resultado)
  */
  int i,j,k,in,la, nvmi, nvk, lmax, n1, nrp, nr;
  int nextp, neigh, iaux, i0, i1;
  int length, nr0, nov, minv, nve, found, mbanv, mbann;
  int nni, mbv, mbn;

  /* Primer loop: ordena vecinos segun grado creciente */
  for (i = 0; i < nod; i++)
    {
      in = ien[i];
      la = ien[i+1];
      for (j = in; j < la-1; j++)
	{
	  neigh = jen[j];
	  nvmi = ien[neigh+1] - ien[neigh];
	  for (k = j+1; k < la; k++)
	    {
	      nvk = ien[jen[k]+1] - ien[jen[k]];
	      if (nvk < nvmi)
		{
		  iaux = jen[k];
		  jen[k] = jen[j];
		  jen[j] = iaux;
		  nvmi = nvk;
		}
	    }
	}
    }

  /*
    Inicializaci�n:
    numeraci�n original: identidad
    nueva numeraci�n: -1 (nodos no numerados)
  */
  lmax = 0;         /* Longitud m�xima de las RLSs */
  n1 = 0;           /* Nodo inicial de esta CC */
  nrp = 0;          /* Nueva numeraci�n del nodo inicial
		       de esta CC */
  for (i = 0; i < nod; i++)
      nn[i] = -1;
  nr = 0;

  /* Loop sobre componentes conexas del grafo */
  /* nv: lista de nodos, seg�n se van numerando */
  /* nn: nueva numeraci�n de cada nodo */
  while (1)
    {
      if (nrp < nr) /* Se hab�a calculado una RLS */
	{           /* Re-inicializa vector nn
		       s�lo para esta CC */
	  for (i = nrp; i < nr; i++)
	    nn[nv[i]] = -1;
	}

      /*  Arma la "Rooted Level Structure" (RLS) de n1 */
      nr = nrp;
      /*      nr0 = nr + 1;  */
      nv[nr] = n1;
      nn[n1] = nr;
      i0 = nr;
      i1 = nr;
      length = 1;
      nr++;

      /*  i0 -> i1: Nivel length - 1 de la RLS */
      while(1) /* loop sobre niveles de la RLS */
	{
	  nr0 = nr; /* nr0: pos.del 1er nodo del nivel */
	  for (i = i0; i <= i1; i++)
	    {           /* Recorre nodos de este nivel */
	      in = ien[nv[i]];
	      la = ien[nv[i]+1]-1;
	      for (j = in; j <=la; j++)
		{
		  nov = jen[j];
		  if (nn[nov] > -1) continue;
		  nv[nr] = nov;  /* No estaba numerado */
		  nn[nov] = nr;
		  nr++;
		  if (nr == nod) break; /* Numer� todos */
		}
	      if (nr == nod) break; /* Idem, sale */
	    }
	  if (nr == nod) break; /* Idem, termin� RLS */

	  if (nr > nr0) /* agreg� al menos un nodo */
	    {           /* en este nivel */
	      i0 = nr0;
	      i1 = nr - 1;
	      length++;      /* Un nivel m�s */
	    }
	  else
	    break;
	}

      /* Termin� una CC, (tambi�n la malla ?) */
      if (length > lmax) /* Guarda m�ximo de las longitudes */
	{
	  lmax = length;
	  if (nr == nod) /* Termin� la malla */
	    {
	      i0 = i1+1; /* Salva �ltimo nivel */
	      i1 = nod-1;
	    }

	  /* Selecciona el nodo de m�nimo grado del �ltimo nivel */
	  n1 = nv[i0];
	  minv = ien[n1+1] - ien[n1];
	  for (i = i0+1; i <= i1; i++)
	    {
	      nve = ien[nv[i]+1] - ien[nv[i]];
	      if (nve < minv)
		{
		  n1 = nv[i];
		  minv = nve;
		}
	    }
	  continue;  /* y vuelve a calcular la RLS */
	}

      if (nr != nod) /* Otra CC ? */
	{
	  found = 0;
	  for (i = 0; i < nod && !found; i++)
	    {
	      if (nn[i] > -1)  /* i ya est� en una CC */
		continue;
	      nrp = nr;        /* Siguiente nodo en nv */
	      n1 = i;          /* Posic.: nrp, valor: i */
	      lmax = 0;        /* Lmax para next CC: 0 */
	      found = 1;
	    }
	  if (found)
	    continue;
	}
      else
	break;    /* Todos numerados, sale */
    }

  mbanv = 0;      /* Ancho de banda anterior */
  mbann = 0;      /* Nuevo ancho de banda */
  for (i = 0; i < nod; i++)
    {
      nni = nn[i];
      for (j = ien[i]; j < ien[i+1]; j++)
	{
	  mbv = abs(i-jen[j]);
	  mbn = abs(nni-nn[jen[j]]);
	  if (mbv > mbanv) mbanv = mbv;
	  if (mbn > mbann) mbann = mbn;
	}
    }
  if (mbann < mbanv)      /* Mejor� ? */
    {
      for (i = 0; i < nod; i++)
	nn[i] = nod-1 - nn[i]; /* Reverse - C/M Ordering */
    }
  else
    {
      for (i = 0; i < nod; i++)
	nn[i] = i;             /* Original numeration */
    }
}

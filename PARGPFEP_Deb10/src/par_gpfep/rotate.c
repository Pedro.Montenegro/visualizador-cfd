/*-------------------------------------------------------------------------
    Function nobrotate - oblique nodes rotation
    Programmed by Enzo Dari - Started: 99/08/05 - Finished: 

    This function must be called from FORTRAN. Its function is to rotate
    unknowns before storing results in vector of variables
  
Variables names:
     Sy: systemdata

Return values:
     NONE
-----------------------------------------------------------------------*/
#include "data_structures.h"
#include "error.h"

#if defined (PETSC_HAVE_FORTRAN_UNDERSCORE)
#define nobrotate nobrotate_
#elif defined (HAVE_FORTRAN_PRE_DOT)
#define nobrotate .nobrotate
#else
#define nobrotate nobrotate
#endif

#undef __SFUNC__
#define __SFUNC__ "nobrotate"

void nobrotate(systemdata **fSy, Vec *funk, int *offblp)
{
  systemdata *Sy;
  Vec unk;
  PetscScalar *base, *lnobscan, *gbasescan, *unkscan;
  int ierr, i, j, k;
  int ndim, lsize, sizebl, pos;
  int offbl;
  PetscScalar vaux[3] = {0.0, 0.0, 0.0};

  Sy  = *fSy;
  unk = *funk;
  offbl = *offblp;

  if (!Sy->lnob)   /* There are no oblique nodes in mesh */
    return;

  ierr = VecGetArray (Sy->lnob, &lnobscan);         CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGetArray (Sy->gbase, &gbasescan);       CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGetArray (unk, &unkscan);               CHKERRABORT(PETSC_COMM_WORLD, ierr);

  ndim = Sy->Pa->nsd;
  ierr = VecGetLocalSize (unk, &lsize);
  sizebl = lsize / Sy->Pa->nodloc;

  if (offbl + ndim > sizebl+1) {
    error (STAY, "offbl+ndim > sizbl+1, Rotation NOT performed");
    ierr = VecRestoreArray (Sy->lnob, &lnobscan);         CHKERRABORT(PETSC_COMM_WORLD, ierr);
    ierr = VecRestoreArray (Sy->gbase, &gbasescan);       CHKERRABORT(PETSC_COMM_WORLD, ierr);
    ierr = VecRestoreArray (unk, &unkscan);               CHKERRABORT(PETSC_COMM_WORLD, ierr);
    return;
  }
  offbl--;

  for (i = 0; i < Sy->Pa->nodloc; i++)
    {
      if (lnobscan[i])
	{
	  pos = (lnobscan[i]-1) ;
	  base = &(gbasescan[pos]);
	  for (j=0; j<ndim; j++)
	    vaux[j] = 0.0;
	  for (j=0; j<ndim; j++)
	    for (k=0; k<ndim; k++)
	      vaux[k] += unkscan[i*sizebl+offbl+j] * base[j*ndim+k];
	  for (j=0; j<ndim; j++)
	    unkscan[i*sizebl+offbl+j] = vaux[j];
	}
    }

  ierr = VecRestoreArray (Sy->lnob, &lnobscan);         CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecRestoreArray (Sy->gbase, &gbasescan);       CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecRestoreArray (unk, &unkscan);               CHKERRABORT(PETSC_COMM_WORLD, ierr);
}
/*
      DIMENSION BASE(3,3),VAUX(3)
      DO I=1,NOD
         IX = (I-1)*NDIM
         if (lnob(i).ne.0) then
            DO ICO=1,NDIM
               DO ISD=1,NDIM
                BASE(ICO,ISD)=GBASE(LNOB(I)+(ICO-1)*NDIM+ISD-1)
               END DO
               VAUX(ICO) = 0.D0
            END DO
            DO ICO=1,NDIM
               DO K=1,NDIM
                VAUX(K) = VAUX(K)+X(IX+ICO)*BASE(ICO,K)
               END DO
            END DO
            DO ICO=1,NDIM
               X(IX+ICO)=VAUX(ICO)
            END DO
         end if
      END DO
      RETURN
      END
*/

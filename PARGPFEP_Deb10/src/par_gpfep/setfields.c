#include <stdlib.h>
#include "setfields.h"

/*------------------------------------------------------------------------
        Function setfields - Modifies the unknowns vector

       This function must be called from FORTRAN.
       It has to call the FORTRAN routine that will modify the
       local unknowns vector.

Variable names:
    seqsetf: pointer to the function to call
    Sy: system structure
    v: vector of unknowns
    parcon: continuation parameter value
    parmat: array of material parameters
    parnum: array of numerical parameters
    pargen: array with general parameters
-----------------------------------------------------------------------------*/
#undef __SFUNC__
#define __SFUNC__ "setfields   "

void setfields(F2 seqsetf, systemdata **fSy, Vec *fv, double *parcon, 
	       double *parmat, double *parnum, double *pargen)
{
  Vec          v;

  systemdata   *Sy;        
  
  int          nodloc;     /* number of local nodes */
  int          nfields;    /* number of scalars per node */
  int          *icomp;     /* number of components of each var */
  int          nvar;       /* number of variables */
  int          nsd;        /* number of spatial dimensions */
  double       *cogeoloc;  /* coordinates of the local nodes */
  PetscScalar  *vscan;     /* array for scanning v */
  int          ierr;

  /* Initializing */
  Sy = *fSy;
  v = *fv;

  nodloc   = Sy->Pa->nodloc;
  cogeoloc = Sy->Pa->cogeo;
  nsd      = Sy->Pa->nsd;
  nfields  = Sy->nfields;
  nvar     = Sy->Sq->nvar;
  icomp    = Sy->Sq->icomp;

  ierr = VecGetArray(v, &vscan); CHKERRABORT(PETSC_COMM_WORLD,ierr);

  (*seqsetf)(&nodloc, &nfields, &nvar, icomp,  
	     parcon, parmat, parnum, pargen, vscan, cogeoloc, &nsd);

  ierr = VecRestoreArray(v, &vscan);
  
  ierr = VecGhostUpdateBegin (v, INSERT_VALUES, SCATTER_FORWARD
			      ); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGhostUpdateEnd (v, INSERT_VALUES, SCATTER_FORWARD
			    );   CHKERRABORT(PETSC_COMM_WORLD,ierr);
}

/*------------------------------------------------------------------------
 * Function setfldsnprev
 * Modifies the unknowns vector and the previous unknows vector

 This function must be called from FORTRAN.
 It calls the FORTRAN routine that will modify the
 *local* unknowns vector.

 Variable names:
 seqsetf: pointer to the function to call
 Sy: system structure
 v: vector of unknowns
 parcon: continuation parameter value
 parmat: array of material parameters
 parnum: array of numerical parameters
 pargen: array with general parameters
-----------------------------------------------------------------------------*/
#undef __SFUNC__
#define __SFUNC__ "setfldsnprev"

void setfldsnprev(F319 seqsetf, systemdata **fSy, Vec *fv, double *parcon, 
	       double *parmat, double *parnum, double *pargen)
{
  Vec          v;

  systemdata   *Sy;        
  
  int          nodloc;     /* number of local nodes */
  int          nfields;    /* number of scalars per node */
  int          *icomp;     /* number of components of each var */
  int          nvar;       /* number of variables */
  int          nsd;        /* number of spatial dimensions */
  double       *cogeoloc;  /* coordinates of the local nodes */
  PetscScalar  *vscan;     /* array for scanning v */
  PetscScalar  *vscana;    /* array for scanning va */
  int          ierr;

  /* Initializing */
  Sy = *fSy;
  v = *fv;

  nodloc   = Sy->Pa->nodloc;
  cogeoloc = Sy->Pa->cogeo;
  nsd      = Sy->Pa->nsd;
  nfields  = Sy->nfields;
  nvar     = Sy->Sq->nvar;
  icomp    = Sy->Sq->icomp;

  ierr = VecGetArray(v, &vscan);       CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGetArray(Sy->va, &vscana); CHKERRABORT(PETSC_COMM_WORLD,ierr);

  (*seqsetf)(&nodloc, &nfields, &nvar, icomp,  
	     parcon, parmat, parnum, pargen, vscan, vscana, cogeoloc, &nsd);

  ierr = VecRestoreArray(v, &vscan); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecRestoreArray(Sy->va, &vscana); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  
  ierr = VecGhostUpdateBegin (v, INSERT_VALUES, SCATTER_FORWARD
			      ); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGhostUpdateEnd (v, INSERT_VALUES, SCATTER_FORWARD
			    );   CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGhostUpdateBegin (Sy->va, INSERT_VALUES, SCATTER_FORWARD
			      ); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGhostUpdateEnd (Sy->va, INSERT_VALUES, SCATTER_FORWARD
			    );   CHKERRABORT(PETSC_COMM_WORLD,ierr);
}

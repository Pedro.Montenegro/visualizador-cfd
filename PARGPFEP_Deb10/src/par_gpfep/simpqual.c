#include <math.h>
#include "simpqual.h"

/*
 * pgpfep_mesh_quality: returns an "histogram" of the element quality:
 * vqual[0]: elements with quality > 0.1
 * vqual[1]: elements with 0.01 < quality < 0.1
 * vqual[2]: elements with 0.001 < quality < 0.01
 * vqual[k]: elements with 10^{-k-1} < quality < 10^{-k}
 * vqual[8]: elements with 0 < quality < 10e-8
 * vqual[9]: elements with quality < 0.0
 *
 * Fortran interface:
*/
void meshquality_ (partition **fPa, int *vqual)
{
  pgpfep_mesh_quality(*fPa, vqual);
}

void meshqualitydisp_ (partition **fPa, int *vqual, Vec *disp)
{
  pgpfep_mesh_quality_disp(*fPa, vqual, *disp);
}

void meshqualitydispffields_ (partition **fPa, int *vqual, Vec *v,
			      systemdata **fSy, int *fsclpn, int *fidisp)
{
  int ierr;
  /* /\* Request boundary nodes values *\/ */
  /* ierr = VecScatterBegin((*fSy)->G2Lscat, (*v), (*fSy)->varext, INSERT_VALUES, */
  /* 			 SCATTER_FORWARD);   CHKERRABORT(PETSC_COMM_WORLD,ierr); */
  /* /\* Receive boundary nodes values *\/ */
  /* ierr = VecScatterEnd((*fSy)->G2Lscat, (*v), (*fSy)->varext, INSERT_VALUES, */
  /* 		       SCATTER_FORWARD);   CHKERRABORT(PETSC_COMM_WORLD,ierr); */
  ierr = VecGhostUpdateBegin(*v, INSERT_VALUES, SCATTER_FORWARD
			     );            CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGhostUpdateEnd  (*v, INSERT_VALUES, SCATTER_FORWARD
			     );            CHKERRABORT(PETSC_COMM_WORLD,ierr);
  pgpfep_mesh_quality_disp_ffields(*fPa, vqual, *v,
				   *fsclpn, *fidisp);
}

void pgpfep_mesh_quality_disp_ffields(partition *Pa, int *vqual, Vec v,
				      int sclpn, int idisp)
{
  int i, iel, ije, in, no, isd, ierr;
  int nsd = Pa->nsd;
  double coords[(nsd+1)*nsd];
  double qelem, qlim, fqual=0.5;
  double *dscan;
  Vec lv;

  int vqloc[10];

  for (i = 0; i < 10; i++)
    vqloc[i] = 0;
  ierr = VecGhostGetLocalForm (v, &lv); CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGetArray(lv, &dscan);       CHKERRABORT(PETSC_COMM_WORLD, ierr);

  for (iel = 0; iel <  Pa->nelemloc + Pa->nelemext; iel++)
    {
      if (Pa->GOS[iel] < 0)
	continue;
      ije = Pa->nnpe * iel;
      for (in = 0; in < Pa->nnpe; in++)
	{
	  no = Pa->Mesh[ije++];
	  for (isd = 0; isd < nsd; isd++)
	    coords[in*nsd+isd] =
	      Pa->cogeo[no*nsd+isd] + dscan[no*sclpn+idisp+isd];
	}
      qelem = simplex_qual (nsd, coords, 1);

      qlim = fqual;
      for (i = 0; i < 10; i++)
	{
	  if (qelem > qlim)
	    {
	      vqloc[i]++;
	      break;
	    }
	  qlim = qlim * fqual;
	}
      if (i == 10)
	vqloc[9]++;
    }

  /*
   * Reduce quality vector:
   */

  (void) MPI_Allreduce(vqloc, vqual, 10, MPI_INT, MPI_SUM, Pa->Comm);

  ierr = VecRestoreArray(lv, &dscan);       CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGhostRestoreLocalForm (v, &lv); CHKERRABORT(PETSC_COMM_WORLD, ierr);
}

void pgpfep_mesh_quality_disp(partition *Pa, int *vqual, Vec disp)
{
  int i, iel, ije, in, no, isd, ierr;
  int nsd = Pa->nsd;
  double coords[(nsd+1)*nsd];
  double qelem, qlim, fqual=0.5;
  double *dscan;
  Vec ldisp;

  double vqloc[10];

  for (i = 0; i < 10; i++)
    vqloc[i] = 0;

  ierr = VecGhostGetLocalForm(disp, &ldisp); CHKERRABORT(PETSC_COMM_WORLD,ierr);
  ierr = VecGetArray(ldisp, &dscan);         CHKERRABORT(PETSC_COMM_WORLD,ierr);

  for (iel = 0; iel <  Pa->nelemloc + Pa->nelemext; iel++)
    {
      if (Pa->GOS[iel] < 0)
	continue;
      ije = Pa->nnpe * iel;
      for (in = 0; in < Pa->nnpe; in++)
	{
	  no = Pa->Mesh[ije++];
	  for (isd = 0; isd < nsd; isd++)
	    coords[in*nsd+isd] = Pa->cogeo[no*nsd+isd] + dscan[no*nsd+isd];
	}
      qelem = simplex_qual (nsd, coords, 1);

      qlim = fqual;
      for (i = 0; i < 10; i++)
	{
	  if (qelem > qlim)
	    {
	      vqloc[i]++;
	      break;
	    }
	  qlim = qlim * fqual;
	}
      if (i == 10)
	vqloc[9]++;
    }

  /*
   * Reduce quality vector:
   */

  (void)MPI_Allreduce(vqloc, vqual, 10, MPI_INT, MPI_SUM, Pa->Comm);

  ierr = VecRestoreArray(ldisp, &dscan);  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGhostRestoreLocalForm (disp, &ldisp
				   );     CHKERRABORT(PETSC_COMM_WORLD, ierr);
}

void pgpfep_mesh_quality(partition *Pa, int *vqual)
{
  int i, iel, ije, in, no, isd;
  int nsd = Pa->nsd;
  double coords[(nsd+1)*nsd];
  double qelem, qlim, fqual=0.5;

  double vqloc[10];

  for (i = 0; i < 10; i++)
    vqloc[i] = 0;

  for (iel = 0; iel <  Pa->nelemloc + Pa->nelemext; iel++)
    {
      if (Pa->GOS[iel] < 0)
	continue;
      ije = Pa->nnpe * iel;
      for (in = 0; in < Pa->nnpe; in++)
	{
	  no = Pa->Mesh[ije++];
	  for (isd = 0; isd < nsd; isd++)
	    coords[in*nsd+isd] = Pa->cogeo[no*nsd+isd];
	}
      qelem = simplex_qual (nsd, coords, 1);

      qlim = fqual;
      for (i = 0; i < 10; i++)
	{
	  if (qelem > qlim)
	    {
	      vqloc[i]++;
	      break;
	    }
	  qlim = qlim * fqual;
	}
      if (i == 10)
	vqloc[9]++;
    }

  /*
   * Reduce quality vector:
   */

  (void) MPI_Allreduce(vqloc, vqual, 10, MPI_INT, MPI_SUM, Pa->Comm);
}

double simplex_qual (int nsd, double *coords, int type)
{
  double qual = 0.0;
  if (nsd == 2)
    {
      if (type == 1)
	{
	  double area, per = 0.0, dx, dy, ex, ey;
	  /* d: \vec{2-3} */
	  dx = coords[4] - coords[2];
	  dy = coords[5] - coords[3];
	  per += sqrt(dx*dx + dy*dy);
	  /* d: \vec{1-2} */
	  dx = coords[2] - coords[0];
	  dy = coords[3] - coords[1];
	  per += sqrt(dx*dx + dy*dy);
	  /* e: -\vec{1-3} */
	  ex = coords[0] - coords[4];
	  ey = coords[1] - coords[5];
	  per += sqrt(ex*ex + ey*ey);
	  /* area: d x -e */
	  area = 0.5*(-dx * ey + dy * ex);
	  qual = 36 / sqrt(3) * area / (per*per);
	}
    }
  else if (nsd == 3)
    {
      if (type == 1)
	{
	  double vol, per = 0.0, dx, dy, dz, ex, ey, ez, fx, fy, fz;
	  double ax, ay, az;
	  /* d: \vec{1-2} */
	  dx = coords[ 3] - coords[0];
	  dy = coords[ 4] - coords[1];
	  dz = coords[ 5] - coords[2];
	  per += sqrt(dx*dx + dy*dy + dz*dz);
	  /* e: \vec{1-3} */
	  ex = coords[ 6] - coords[0];
	  ey = coords[ 7] - coords[1];
	  ez = coords[ 8] - coords[2];
	  per += sqrt(ex*ex + ey*ey + ez*ez);
	  /* f: -\vec{1-4} */
	  fx = coords[ 9] - coords[0];
	  fy = coords[10] - coords[1];
	  fz = coords[11] - coords[2];
	  per += sqrt(ex*ex + ey*ey);
	  /* vol: d x e . f*/
	  ax = dy*ez - dz*ey;
	  ay = dz*ex - dx*ez;
	  az = dx*ey - dy*ex;
	  vol = ax*fx + ay*fy + az*fz;

	  /* d: \vec{2-3} */
	  dx = coords[ 6] - coords[3];
	  dy = coords[ 7] - coords[4];
	  dz = coords[ 8] - coords[5];
	  per += sqrt(dx*dx + dy*dy + dz*dz);
	  /* e: \vec{3-4} */
	  ex = coords[ 9] - coords[6];
	  ey = coords[10] - coords[7];
	  ez = coords[11] - coords[8];
	  per += sqrt(ex*ex + ey*ey + ez*ez);
	  /* f: -\vec{2-4} */
	  fx = coords[ 9] - coords[3];
	  fy = coords[10] - coords[4];
	  fz = coords[11] - coords[5];
	  per += sqrt(ex*ex + ey*ey);

	  qual = 2592 / sqrt(2) * vol / (per*per*per);
	}
    }
  else if (nsd == 1)
    {
      if (type == 1)
	{
	  qual = coords[1] - coords[0];
	  if (qual < 0.0)
	    qual = -1.0;
	  else if (qual > 0.0)
	    qual = 1.0;
	}
    }

  return qual;
}

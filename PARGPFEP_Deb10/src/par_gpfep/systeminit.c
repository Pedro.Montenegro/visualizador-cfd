#include <stdlib.h>
#include <stdio.h>
#include "systeminit.h"
#include "refelem.h"

void pgpfepgetva_(systemdata **fSy, Vec *fv)
{
  *fv = (*fSy)->va;
}

/*----------------------------------------------------------------------
    Function systeminit - Structures asociated to the linear system
    Programmed by Adrian Lew - Started: 10/11/97 - Finished: 11/11/97
    Thanks to the Flaco for his patience again and again.


    This function must be called from FORTRAN. Its function is to set the
structures for the scattering  of field values of the external nodes of each
region.
    If there's no surface mesh, SurPa should come equal to NULL. 
  
Variables names:
     Sy: system structure
     v: vector with the values of all the fields in each node 
     Pa: partition of the mesh in wich the system is based
     SurPa: partition of the surface mesh in wich the system is based
     nfields: number of total fields asociated with each node (r)
     fmode: 0: normal behaviour
            1: indicates full vectors in all processors

Return values:
     NONE

Remember: 
     The nodes numbering begins with 1, not with 0. So for example, in 
partition_vec[0] is the region  to which belongs node number 1.
-----------------------------------------------------------------------*/
#undef __SFUNC__   
#define __SFUNC__ "systeminit" 

void systeminit(systemdata **fSy, Vec *fv, partition **fPa, 
		partition **fSurPa, int *fnfields, int *fmode)
{
  Vec       v;

  int       ierr;            /* error checking variable */
  int       i, k;            /* loop counters */
  MPI_Comm  Comm;            /* Communicator */

  systemdata  *Sy;
  partition *Pa;
  partition *SurPa;
  int       nfields;
  int       mode;

  Pa   = *fPa;
  SurPa= *fSurPa;
  mode = *fmode;

  *fSy = (systemdata *) malloc(sizeof(systemdata));
  if (*fSy == (systemdata *)NULL)
      error (EXIT, "Not enough memory for fSy@systeminit");
  Sy   = *fSy;
  
  /* Initialization */
  Sy->Pa = Pa;
  Comm = (mode == 1 ? PETSC_COMM_WORLD : Pa->Comm);

  nfields = *fnfields;
  ierr = MPI_Bcast(&nfields, 1, MPI_INT, 0, Comm);

  Sy->nfields = nfields;

  /* Verifying consistency between SurPa and Pa */  
  if(SurPa!=(partition *)NULL){
    if(Pa->nodtot!=SurPa->nodtot || Pa->cogeo!=SurPa->cogeo ||
       Pa->d_nz!=SurPa->d_nz ||
       Pa->nd_nz!=SurPa->nd_nz || Pa->first!=SurPa->first ||
       Pa->last!=SurPa->last || Pa->Glob!=SurPa->Glob ||
       Pa->Comm!=SurPa->Comm)
      error(EXIT," The volume and surface meshes don't seem to correspond");
  }
  Sy->SurPa = SurPa;
  

  /* Creating the vectors
  if (mode == 1)
    {
      ierr = VecCreateSeq(MPI_COMM_SELF, nfields*(Pa->nodtot),
			  &v);           CHKERRABORT(PETSC_COMM_WORLD, ierr);
    }
  else
  */
  {
    PetscInt *aghost;
    aghost = (PetscInt *) malloc (nfields*Pa->nodghost*sizeof(PetscInt));
    if (aghost == (PetscInt *)NULL)
      error (EXIT, "Not enough memory for aghost@systeminit");
    for (i = 0; i < Pa->nodghost; i++)
      for (k = 0; k < nfields; k++)
	aghost[i*nfields + k] = nfields*Pa->Glob[i] + k;
      
    ierr = VecCreateGhost(Pa->Comm, nfields*Pa->nodloc, nfields*Pa->nodtot,
			  nfields*Pa->nodghost, aghost, &v);
    free(aghost);
  }
  ierr = VecDuplicate(v, &(Sy->va));      CHKERRABORT(PETSC_COMM_WORLD, ierr);

  Sy->bcccond = (int    *)NULL;
  Sy->bccvalue= (PetscScalar *)NULL;

  Sy->lnob    = (Vec) NULL;
  Sy->lnobext = (Vec) NULL;
  Sy->gbase   = (Vec) NULL;

  *fv = v;
}

/*----------------------------------------------------------------------
    Function systemmembers - Creates the matrix, the RHS, and the solution
                             vector of a linear system
    Programmed by Adrian Lew - Started: 15/11/97 - Finished: 15/11/97
    Thanks to the Flaco for his patience again and again.


    This function must be called from FORTRAN. Its function is to crete
the Matrix and the Vector asociated to a linear system.
  
Variables names:
     Sy: system structure
     A: Matrix of the system
     x: solution vector
     b: RHS vector asociated to the matrix A.
     nfa: number of fields to assembly in the matrix A and the vector of 
          unknowns b.
     mode: 0: normal behaviour
           1: all processes hold full vectors (and matrices)
     type_flag: STANDARD or LUMPED, due to the kind of matrix. LUMPED
                generates a diagonal matrix.

Return values:
     NONE

Remember: 
     The nodes numbering begins with 1, not with 0. So for example, in 
partition_vec[0] is the region  to which belongs node number 1.
-----------------------------------------------------------------------*/
#undef __SFUNC__   
#define __SFUNC__ "systemmembers" 

void systemmembers(systemdata **fSy, Mat *fA, Vec *fx, Vec *fb, int *fnfa, 
		   int *fmode, int *fflag)
{
  Mat       A;
  Vec       b;
  Vec       x;

  int       ierr;            /* error checking variable */
  int       i, k;            /* loop counters */
  int       tmpint1;         /* temporary int variable */
  MPI_Comm  Comm;            /* Communicator */
  int       *aux_vec1;       /* All kind of usages vector */
  int       *aux_vec2;       /* All kind of usages vector */
  int       size;            /* Communicator size */
  int       flag;

  systemdata  *Sy;
  partition *Pa;
  int       nfa;
  int       mode;

  mode  = *fmode;
  nfa  = *fnfa;
  Sy   = *fSy;
  flag = *fflag;

  /* PetscPrintf(PETSC_COMM_WORLD,
	      "Systemmembers, flag: %d, NVX: %d, NVB: %d, NM: %d\n",
	      *fflag, NO_VECTOR_X, NO_VECTOR_B, NO_MATRIX); */
  /* Initialization */
  Pa     = Sy->Pa;
  Comm   = Pa->Comm;
  MPI_Comm_size(Comm, &size);

  ierr = MPI_Bcast(&nfa, 1, MPI_INT, 0, Comm);

  if (!(flag & X_IGNORE_FLAG))    
    {
      if (mode == 1)
	{
	  ierr = VecCreateMPI(MPI_COMM_SELF,
			      nfa*(Pa->nodloc), nfa*(Pa->nodtot),
			      &x);  CHKERRABORT(PETSC_COMM_WORLD, ierr);
	}
      else
	{
	  ierr = VecCreateMPI(Comm, nfa*(Pa->nodloc), nfa*(Pa->nodtot),
			      &x);  CHKERRABORT(PETSC_COMM_WORLD, ierr);
	}
      *fx = x;
    }

  if (!(flag & RHS_IGNORE_FLAG))
    {
      if (mode == 1)
	{
	  ierr = VecCreateMPI(MPI_COMM_SELF,
			      nfa*(Pa->nodloc), nfa*(Pa->nodtot),
			      &b);  CHKERRABORT(PETSC_COMM_WORLD, ierr);
	}
      else
	{
	  ierr = VecCreateMPI(Comm, nfa*(Pa->nodloc), nfa*(Pa->nodtot),
			      &b);  CHKERRABORT(PETSC_COMM_WORLD, ierr);
	}
      *fb = b;
    }

  if (flag & MATRIX_IGNORE_FLAG)
    return;
  flag=flag & 15;
  /* Matrix Creation */
  aux_vec1 = (int *) malloc(nfa*(Pa->nodloc)*sizeof(int));
  if(aux_vec1==(int *)NULL)
    error(EXIT, "Not enough memory to allocate aux_vec1");
								 
  aux_vec2 = (int *) malloc (nfa * (Pa->nodloc) * sizeof(int));
  if (aux_vec2 == (int *)NULL)
    error (EXIT, "Not enough memory to allocate aux_vec2");

  if (flag == STANDARD)
    {
      for (i = 0; i < Pa->nodloc; i++)
	{
	  tmpint1 = i * nfa;
	  for (k = 0; k < nfa; k++)
	    {
	      aux_vec1[tmpint1 + k] = nfa * Pa->d_nz[i];
	      aux_vec2[tmpint1 + k] = nfa * Pa->nd_nz[i];
	    }
	}
    }
  else if (flag == STANDARD_UNCOUPLED)
    {
      for (i = 0; i < Pa->nodloc; i++)
	{
	  tmpint1 = i * nfa;
	  for (k = 0; k < nfa; k++)
	    {
	      aux_vec1[tmpint1 + k] = Pa->d_nz[i];
	      aux_vec2[tmpint1 + k] = Pa->nd_nz[i];
	    }
	}
    }
  else if (flag==LUMPED)
    {
      for(i=0; i<Pa->nodloc; i++)
	{
	  tmpint1=i*nfa;
	  for(k=0; k<nfa; k++)
	    {
	      aux_vec1[tmpint1+k]=1;
	      aux_vec2[tmpint1+k]=0;
	    }
	}
    }  
  else if (flag==NSBSLUMP)
    {
      for(i=0; i<Pa->nodloc; i++)
	{
	  tmpint1=i*nfa;
	  for(k=0; k<nfa; k++)
	    {
              if (k==nfa-1)
		{
		  aux_vec1[tmpint1+k]=nfa*Pa->d_nz[i];
		  aux_vec2[tmpint1+k]=nfa*Pa->nd_nz[i];
		}
              else
		{
		  aux_vec1[tmpint1+k]=Pa->d_nz[i];
		  aux_vec2[tmpint1+k]=Pa->nd_nz[i];
		}
	    }
	}
    }
  else
    {
      PetscPrintf(PETSC_COMM_WORLD, "Unrecognized lumping flag: %d\n",
		  flag);
      error(EXIT, "Unrecognized lumping flag");
    }

  if (mode == 1)
    {
      ierr = MatCreateAIJ
	(MPI_COMM_SELF, nfa*(Pa->nodloc), nfa*(Pa->nodloc), nfa*(Pa->nodtot),
	 nfa*(Pa->nodtot), 0, aux_vec1, 0, aux_vec2,
	 &A); CHKERRABORT(PETSC_COMM_WORLD, ierr);
    }
  else
    {
      ierr = MatCreateAIJ
	(Comm, nfa*(Pa->nodloc), nfa*(Pa->nodloc), nfa*(Pa->nodtot),
	 nfa*(Pa->nodtot), 0, aux_vec1, 0, aux_vec2,
	 &A); CHKERRABORT(PETSC_COMM_WORLD, ierr);
    }

  *fA = A;

  free(aux_vec1);
  free(aux_vec2);
}
 
/*
 * Function createghostedfield:
 * given a number of nodal unknowns,
 * create a ghosted distributed vector
 * 
 * Arguments:
 *  fSy: (input) the system info (par-gpfep specific)
 *  fnfa: (input) number of unknowns per node
 *  gdvec: (output) ghosted distributed vector
 */
void createghostedfield (systemdata **fSy, int *fnfa, Vec *gdvec)
{
  int i, k, nfa, ierr;
  partition *Pa;
  PetscInt *aghost;

  Pa = (*fSy)->Pa;
  nfa = *fnfa;

  aghost = (PetscInt *) malloc (nfa*Pa->nodghost*sizeof(PetscInt));
  if (aghost == (PetscInt *)NULL)
    error (EXIT, "Not enough memory for aghost@systeminit");
  for (i = 0; i < Pa->nodghost; i++)
    for (k = 0; k < nfa; k++)
      aghost[i*nfa + k] = nfa*Pa->Glob[i] + k;
  
  ierr = VecCreateGhost(Pa->Comm, nfa*Pa->nodloc, nfa*Pa->nodtot,
			nfa*Pa->nodghost, aghost, gdvec); CHKERRABORT(PETSC_COMM_WORLD, ierr);
  free(aghost);
}

/*----------------------------------------------------------------------
    Function systemquadrat - Initialize the quadrature parameters
    Programmed by Adrian Lew - Started: 20/11/97 - Finished: 20/11/97

    This function must be called from C or Fortran. This function must be 
called each time is desirable changing the cuadrature rules of one or more
of the system fields.
  
Variables names:
     Sy: system structure
     nvar: number of fields (r)
     icomp: number of components of each field (r) 
     npe: array with the number of nodes per elements in each mesh (r)
     maxgau: maximum number of gauss points (r)
     ieltype: geometric element type (r)
     itgdeg: maximum order of polynomial integrated exactly (r)
     itpcod: array containing the interpolation code used for each field (r)
     itpgeo: interpolation codes used for geometry (r)
     itgtyp: integration type (r)
     kmesh: mesh corresponding to each field (r)
     mode: 0: normal behaviour
           1: all processes have full vectors

Return values:
     NONE

Note:
   npe does not has much sense here, because there is only one mesh, but
the intention was to follow the gpfep style for future upgrades, despite
the fact that in the case of many meshes it is not clear yet which is the 
role of the partition structure.
-----------------------------------------------------------------------*/
#undef __SFUNC__   
#define __SFUNC__ "systemquadrat" 

void systemquadrat(systemdata **fSy, int *fnvar, int *icomp, int *npe,
		   int *fmaxgau, int *fieltype, int *fitgdeg, int *itpcod, 
		   int *fitpgeo, int *fitgtyp, int *kmesh,  int *fmode)
{
  MPI_Comm  Comm;            /* Communicator */
  int       size, rank;      /* Communicator size  and rank */
  int       i;

  systemdata  *Sy;
  partition   *Pa;
  partition   *SurPa;
  systemquad  *Sq;
  systemquad  *SurSq;
  int mode;
  int ierr;
  int nvar, nmeshes;
  int maxnpe;
  int ieltype;
  int itgdeg;
  int itpgeo;
  int itgtyp;
  int Surnsd;      /* This is the nsd value of the reference element */

  Sy   = *fSy;
  mode = *fmode;

  /* Initialization */
  Pa     = Sy->Pa;
  Comm   = (mode == 1 ? PETSC_COMM_WORLD : Pa->Comm);
  MPI_Comm_size(Comm, &size);
  MPI_Comm_rank(Comm, &rank);

  Sy->Sq = (systemquad *) malloc(sizeof(systemquad));
  if (Sy->Sq == (systemquad *)NULL)
    error (EXIT, "Not enough memory in systemquadrat for Sy->Sq");

  Sq       = Sy->Sq;
  Sq->nvar = *fnvar;
  ierr = MPI_Bcast(&(Sq->nvar), 1, MPI_INT, 0, Comm);
  
  Sq->icomp = (int *) malloc(Sq->nvar * sizeof(int));
  if (Sq->icomp == (int *)NULL)
    error(EXIT,"Not enough memory in systemquadrat for Sq->icomp");

  if (rank == 0)
    {
      for (i = 0; i < Sq->nvar; i++)
	Sq->icomp[i]  = icomp[i];
      Sq->maxcomp= 0;
      for (i = 0; i < Sq->nvar; i++)
	if (Sq->maxcomp < icomp[i])
	  Sq->maxcomp = icomp[i];
    }

  /* Broadcasting of the input data */
  ierr = MPI_Bcast(Sq->icomp  , Sq->nvar, MPI_INT, 0, Comm );
  ierr = MPI_Bcast(&(Sq->maxcomp), 1    , MPI_INT, 0, Comm ); 
  ierr = MPI_Bcast(fieltype   , 1       , MPI_INT, 0, Comm );
  ierr = MPI_Bcast(fitgdeg    , 1       , MPI_INT, 0, Comm );
  ierr = MPI_Bcast(fitpgeo    , 1       , MPI_INT, 0, Comm );
  ierr = MPI_Bcast(kmesh      , Sq->nvar, MPI_INT, 0, Comm );
  ierr = MPI_Bcast(itpcod     , Sq->nvar, MPI_INT, 0, Comm );
  ierr = MPI_Bcast(fitgtyp    , 1       , MPI_INT, 0, Comm );
  ierr += MPI_Bcast(fmaxgau    , 1       , MPI_INT, 0, Comm );
  
  /* Count meshes */
  nmeshes = 0;
  for (i = 0; i < Sq->nvar; i++)
    if (kmesh[i] > nmeshes)
      nmeshes = kmesh[i];
  ierr = MPI_Bcast(npe, nmeshes, MPI_INT, 0, Comm ); 

  /* filling Sq */
  nvar       = *fnvar    = Sq->nvar;
  Sq->maxgau = *fmaxgau;
  ieltype    = *fieltype;
  itgdeg     = *fitgdeg;
  itpgeo     = *fitpgeo;
  itgtyp     = *fitgtyp;
  
  maxnpe = Sq->maxnpe = Pa->nnpe; /* This is because there is only one mesh */
  refelem(nvar, maxnpe, ieltype, itgdeg, itgtyp, itpcod, itpgeo, Pa->nsd,
	  kmesh, npe, &(Sq->ngau), &(Sq->wgp), &(Sq->kint), &(Sq->itypegeo),
	  &(Sq->p), &(Sq->dpl), &(Sq->dpl2), &(Sq->nod), &(Sq->maxgau));

  /* filling the surface mesh quadrature rule */
  /* This is explicity done in an special part of code and not like another
     simple quadrature rule because this one is terribly related to the volume
     mesh cuadrature */

  if (Sy->SurPa != (partition *) NULL)
    {
      int *SurItpcod;

      SurPa = Sy->SurPa;

      Sy->SurSq = (systemquad *) malloc(sizeof(systemquad));
      if (Sy->SurSq == (systemquad *)NULL)
	error(EXIT,"Not enough memory in systemquadrat for Sy->SurSq");

      SurItpcod = (int *) malloc(nvar * sizeof(int));
      if (SurItpcod == (int *)NULL)
	error(EXIT,"Not enough memory in systemquadrat for SurItpcod");

      SurSq          = Sy->SurSq;
      SurSq->nvar    = Sq->nvar;

      SurSq->icomp  = (int *) malloc(SurSq->nvar * sizeof(int));
      if (SurSq->icomp == (int *)NULL)
	error(EXIT,"Not enough memory in systemquadrat for SurSq->icomp");

      for (i = 0; i < nvar; i++)
	SurSq->icomp[i] = Sq->icomp[i];

      SurSq->maxcomp = Sq->maxcomp;
      SurSq->maxnpe  = Sy->SurPa->nnpe;  
      SurSq->maxgau  = Sq->maxgau;

      tradtyp(fieltype, &ieltype);
      tradcod(fitpgeo , &itpgeo);
      for (i = 0; i < nvar; i++)
	tradcod(&itpcod[i], &SurItpcod[i]);

      maxnpe = npe[0] = SurSq->maxnpe; /*Only one type of mesh*/
      Surnsd = SurPa->nsd - 1;     /*This is because the integration over the
				     surface is not null in 1 dimension less 
				     than the volume dimension */

      refelem(nvar, maxnpe, ieltype, itgdeg, itgtyp, SurItpcod, itpgeo, 
	      Surnsd, kmesh, npe, &(SurSq->ngau), &(SurSq->wgp), 
	      &(SurSq->kint), &(SurSq->itypegeo), &(SurSq->p), &(SurSq->dpl), 
	      &(SurSq->dpl2), &(SurSq->nod), &(SurSq->maxgau));

      free(SurItpcod);
    }
  else
    Sy->SurSq = (systemquad *) NULL;
}

/*----------------------------------------------------------------------
    Function systemdestroy - frees system
    Programmed by Adrian Lew - Started: 27/11/97 - Finished: 27/11/97

  
Variables names:
     Sy: system structure

Return values:
     NONE

-----------------------------------------------------------------------*/
#undef __SFUNC__
#define __SFUNC__ "systemdestroy"

void systemdestroy(systemdata **fSy)
{
  systemdata   *Sy;
  systemquad   *Sq;
  systemquad   *SurSq;

  Sy    = *fSy;
  Sq    = Sy->Sq;
  SurSq = Sy->SurSq;

  /* freeing systemquad */
  systemquaddestroy(&Sq);
  if(Sy->SurSq!=NULL)
    systemquaddestroy(&SurSq);

  /* freeing systemdata */

  /* VecDestroy(&(Sy->varext)); */
  /* VecDestroy(&(Sy->varaext)); */
  VecDestroy(&(Sy->va));

  /* VecScatterDestroy(&(Sy->G2Lscat)); */
  free(Sy->bcccond);
  free(Sy->bccvalue);
  free(Sy);
}


/*----------------------------------------------------------------------
    Function systemquaddestroy - frees systemquad
    Programmed by Adrian Lew - Started: 08/03/98 - Finished: 08/03/98
  
Variables names:
     Sq: systemquad structure

Return values:
     NONE

-----------------------------------------------------------------------*/
#undef __SFUNC__
#define __SFUNC__ "systemquaddestroy"

void systemquaddestroy(systemquad **fSq)
{
  systemquad    *Sq;

  Sq = *fSq;

  free(Sq->icomp);
  free(Sq->kint);
  free(Sq->nod);
  free(Sq->wgp);
  free(Sq->p);
  free(Sq->dpl);
  free(Sq->dpl2);
  
  free(Sq);
}

/*----------------------------------------------------------------------
 
	HostSpeed - Codigo, para Petsc 2.0.17 
 	Hecho por Adrian Lew el 29/04/97 - Modificado el 17/11/97
 	Version 03 (La primera es del codigo unidimensional en PVM)
                   (La segunda es para Poisson2D)

---------------------------------------------------------------------*/

#include "petscksp.h"
#include <stdlib.h>
#include "tiempo.h"
#include "error.h"
#include "testspeed.h"

/*---------------------------------------------------------------------
Funcion HostSpeed: 
	    Mide el tiempo que demora un dado procesador en realizar una
determinada tarea.


Variables names:
       Size: tamanho de la matriz de test
       Densidad: numero entre 0 y 1. Densidad*Size es el numero
		 de elementos no nulos por fila de la matriz
       seed_1: semilla 1 de los numeros aleatorios que llenan la
	       matriz
       seed_2: semilla 2 de los numeros aleatorios que llenan la
	       matriz
Returned valued:
       Tiempo transcurrido para llevar a cabo la tarea en .......

Descripcion:
     Crea una matriz secuencial y un vector secuencial. Completa los 
mismos  con valores al azar tomados de las semillas que recibio, si 
todos los procesadores reciben las mismas semilla construiran el mismo 
problema, por lo que los test seran comparables. Para evitar la 
periodicidad de los valores, cada algun numero de valores de renueva la
 semilla. A continuacion se resuelve el sistema y se mide finalmente 
el tiempo que se demoro en realizar toda la tarea completa.

Requisitos:
     Debe haberse iniciado PETSC antes de llamar a la funcion
----------------------------------------------------------------------*/
#undef __SFUNC__
#define __SFUNC__ "HostSpeed"

double HostSpeed(int Size, double Densidad,double seed_1, double seed_2)
{
  int	      ierr, i, j, nelem /*, its*/;
  Mat         A;
  Vec         x, y;
  PetscScalar *values1;
  int         *values2;
  KSP         kspobj;
  PC          pcontext;
  double      t1, t2;

  printf("Computing speed ...\n"); 
  values1 = (PetscScalar *) malloc (sizeof(PetscScalar)*Size);
  if(values1==(PetscScalar *)NULL)
    error(EXIT,"Not enough memory to allocate values1");

  values2 = (int    *) malloc (sizeof(int)   *Size);
  if(values2==(int    *)NULL)
    error(EXIT,"Not enough memory to allocate values2");


  t1=tiempo(/*REAL_TIME*/USER_TIME);

  /* ierr = VecCreate 
     (MPI_COMM_SELF, Size, &x); CHKERRABORT(PETSC_COMM_WORLD, ierr); */
  ierr = VecCreateSeq 
    (MPI_COMM_SELF, Size, &x);  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecDuplicate (x, &y);  CHKERRABORT(PETSC_COMM_WORLD, ierr);

  srand((int)seed_1);
  values1[0]= (PetscScalar)(rand()/(RAND_MAX+1.)*100.0);

  ierr = VecSet(x, values1[0]); CHKERRABORT(PETSC_COMM_WORLD, ierr);

  values1[0]=1.;  /* Para asegurar que el vector de  terminos indep. */
  values2[0]=0;   /* no sea identicamente nulo                       */

  for(i=1; i<Size; i++)
    {
      values1[i]= (PetscScalar)(rand()/(RAND_MAX+1.)*100.0);
      values2[i]= i;
      if ( ((int)(i/232))*232 == i)
	srand ((int)(++seed_1));
    }

  ierr = VecSetValues (y, Size, &(values2[0]), &(values1[0]),
		      INSERT_VALUES);     CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecAssemblyBegin(y);             CHKERRABORT(PETSC_COMM_WORLD, ierr);

  nelem =(int)(0.1*Size);
  ierr = MatCreateSeqAIJ (MPI_COMM_SELF, Size, Size, nelem, PETSC_NULL, &A); 

  for (j = 0; j < Size; j++)
    {
      for (i = 0; i < nelem; i++,((int)(i/232))*232==i ? 
	     srand((int)(++seed_2)) : (void) 0) {
	values1[i]= (PetscScalar)(rand()/(RAND_MAX+1.0)*100.);
	values2[i]= (int)(((double)Size)*rand()/(RAND_MAX+1.0)); 
      }
      ierr = MatSetValues(A, 1, &j, nelem,&(values2[0]), &(values1[0]), 
			  INSERT_VALUES ); CHKERRABORT(PETSC_COMM_WORLD, ierr);
    }
  
  ierr = MatAssemblyBegin
    (A, MAT_FINAL_ASSEMBLY);      CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecAssemblyEnd(y);       CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = MatAssemblyEnd
    (A, MAT_FINAL_ASSEMBLY);      CHKERRABORT(PETSC_COMM_WORLD, ierr);

  values1[0]=101;
  ierr = MatShift(A, values1[0]); CHKERRABORT(PETSC_COMM_WORLD, ierr);

  /*  Listos los datos, vamos a resolver  */
  ierr = KSPCreate
    (MPI_COMM_SELF, &kspobj);     CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = KSPSetOperators
    (kspobj, A, A);               CHKERRABORT(PETSC_COMM_WORLD, ierr);
  /*  El precond. default da problemas !?  */
  ierr = KSPGetPC(kspobj, &pcontext);
  ierr = PCSetType(pcontext, PCNONE);

  /* ierr = SLESSolve
     (sles, y, x, &its);            CHKERRABORT(PETSC_COMM_WORLD, ierr); */
  /* ierr = KSPSetRhs(kspobj, y);   CHKERRABORT(PETSC_COMM_WORLD, ierr); */
  /* ierr=KSPSetSolution(kspobj,x); CHKERRABORT(PETSC_COMM_WORLD, ierr); */
  ierr = KSPSolve (kspobj, y, x);   CHKERRABORT(PETSC_COMM_WORLD, ierr);
/*  ierr = KSPGetIterationNumber(kspobj, &its);   CHKERRABORT(PETSC_COMM_WORLD, ierr); */

  t2 = tiempo(/*REAL_TIME*/USER_TIME);

  KSPDestroy(&kspobj);
  VecDestroy(&x);
  VecDestroy(&y);
  MatDestroy(&A);
  free(values1);
  free(values2);

  printf ("Speed: %g\n", 1.0/(t2-t1));
  return(t2-t1);
  }

/*----------------------------------------------------------------------
 Funcion speedsearch
	  Determinar la velocidad relativa entre procesadores. Esta 
vendra dada por un numero entre cero y uno, la suma de todas sera 1.

Variables names:
     Size: tamanho de la matriz de test
     Densidad: numero entre 0 y 1. Densidad*Size es el numero
	       de elementos no nulos por fila de la matriz
     seed_1: semilla 1 de los numeros aleatorios que llenan la matriz
     seed_2: semilla 2 de los numeros aleatorios que llenan la matriz
     mpn: main processor number of MPI_COMM_WORLD

Returned values:
     weights: array de velocidades relativas, de longitud igual
              al tamanho de MPI_COMM_WORLD.

Descripcion:
	Se llama a la funcion HostSpeed que soluciona un sistema de 
ecuaciones en cada procesador y devuelve el tiempo que demoro. A 
continuacion todos los procesos pasan sus valores al mpn quien los 
 normaliza.

----------------------------------------------------------------------*/
#undef __SFUNC__
#define __SFUNC__ "speedsearch"

void speedsearch(int *fSize, double *fDensidad, int *fseed_1, 
		 int *fseed_2, double **fweights, int *fmpn)
{
  int    mpn;
  int    seed_1;
  int    seed_2;
  int    Size;
  double Densidad;
  double *weights = NULL;

  double selfweight;
  double suma;
  int    rank, Comm_size;
  int    i;

  int    ierr = 0;
  int    weightflag;
  char   weightfile[150];
  FILE   *fp;

  PetscBool pt_flg;

  /* Inicializacion */
  mpn      = *fmpn;
  seed_1   = *fseed_1;
  seed_2   = *fseed_2;
  Size     = *fSize;
  Densidad = *fDensidad;

  /*
   */
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &Comm_size);

  if(rank==mpn){
    *fweights = (double *) malloc (Comm_size * sizeof(double));
    if(*fweights==(double *)NULL)
      error(EXIT,"Not enough memory for weights");
    weights = *fweights;
    for (i = 0; i < Comm_size; i++)
      weights[i] = 1.0 / Comm_size;
  }
  if (Comm_size == 1)
    return;

  PetscOptionsGetString(NULL, "pgpfep_","-partfromfile", weightfile, 149,
			&pt_flg);
  if (pt_flg == PETSC_TRUE)
    return;                    /* There is no need to measure the speeds */

  ierr = PetscOptionsHasName(NULL, "pgpfep_","-partnonuniform", &pt_flg);
  if (ierr == 0 && pt_flg == PETSC_TRUE)
    {
      PetscOptionsGetString (NULL, "pgpfep_", "-weightsfromfile", weightfile,
			     149, &pt_flg);
      if (pt_flg == PETSC_TRUE)
	weightflag = 1;
      else
	weightflag = 2;
    }
  else
    {
      return;
    }

  /* weightflag=1 -> read,
     weightfile=2 -> compute */

  if (weightflag == 2)
    {
      selfweight = 1. / HostSpeed(Size, Densidad, seed_1, seed_2);
      ierr = MPI_Gather (&selfweight, 1, MPI_DOUBLE,
			      weights, 1, MPI_DOUBLE, mpn, MPI_COMM_WORLD);
    }
  else
    {
      if (rank == mpn)
	{
	  if ((fp = fopen (weightfile, "r"))==(FILE *)NULL)
	    error (EXIT, "Cannot open weights input file");
	  for (i = 0; i < Comm_size; i++)
	    if ((fscanf (fp, "%lf", &weights[i])) == EOF)
	      error (EXIT, "Not enough weights in weights file");
	  fclose (fp);
	}
    }

  /* Normalizacion */    
  if (rank == mpn)
    {
      suma = 0;
      for (i = 0; i < Comm_size; i++)
	suma += weights[i];
      for (i = 0; i < Comm_size; i++)
	weights[i] = weights[i] / suma;

      ierr += PetscOptionsGetString(NULL, "pgpfep_","-prtweightsfile",
				    weightfile, 149, &pt_flg);
      if (pt_flg == PETSC_TRUE)
	{
	  if ((fp = fopen (weightfile, "w")) == (FILE *)NULL)
	    error (EXIT, "Cannot open weights output file");
	  for (i = 0; i < Comm_size; i++)
	    fprintf (fp, "%g\n", weights[i]);
	  fclose (fp);
	}
    }
}

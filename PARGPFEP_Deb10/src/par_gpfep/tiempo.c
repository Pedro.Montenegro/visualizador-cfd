#include "tiempo.h"
#include <stdio.h>
#include <stdlib.h>

/*
** somente para UNIX 
*/
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>


double tmpcpu_()
    {
    int r;
    struct rusage t;
    double dtempo;

    r = getrusage(RUSAGE_SELF, &t);
    if (r == -1)
        {
        printf("\nErro em tmpcpu\n");
        exit(1);
        }
    dtempo = t.ru_utime.tv_usec + t.ru_stime.tv_usec;
    dtempo += 1.0e+06*(t.ru_utime.tv_sec + t.ru_stime.tv_sec);

    return(dtempo);
    }


int pegdat_(int *dia, int *mes, int *ano)
    {
    long tclock;
    struct tm *t;

    tzset();
    tclock = time(0);
    t = localtime(&tclock);
    *dia = t->tm_mday;
    *mes = t->tm_mon;
    *ano = t->tm_year;
    return(1);
    }

int peghor_(int *hora, int *minuto, int *segundo)
    {
    long tclock;
    struct tm *t;

    tzset();
    tclock = time(0);
    t = localtime(&tclock);
    *hora = t->tm_hour;
    *minuto = t->tm_min;
    *segundo = t->tm_sec;
    return(1);
    }


/*
// Funcion tiempo: lee el tiempo en microsegundos, y esto es lo que devuelve
// REAL_TIME: lee el tiempo real
// USER_TIME: lee el tiempo de utilizacion de la CPU por el proceso
*/

double tiempo(int mode)
{
  if(mode==REAL_TIME){
    int   hora, minuto , segundo;
    double time1;
    peghor_(&hora, &minuto, &segundo);
    time1 =(double)( hora *3600 + minuto * 60 +segundo);
    return(time1);
  }
  else {
    double time1;
    time1=tmpcpu_();
    return(time1);
  }
  return(0);
}

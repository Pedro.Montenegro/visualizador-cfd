#include "v2x_x2v.h"

/*----------------------------------------------------------------------
  Function v2x - Set the initial guess equal to that of the last 
  continuation step
  Programmed by Adrian Lew - Started: 21/11/97 - Finished: 21/11/97

  This function must be called from Fortran. 

  Variables names:
  Sy: system structure
  v: vector of unknowns (created with ghosted values)
  x: solution vector (without ghosted values)
  nfa: array with the scalars to copy ( in Fortran numbering )
  lnfa: lenght of array nfa
  option: ADD or INSERT

  Returned values:
  NONE

  ----------------------------------------------------------------------------*/
#undef  __SFUNC__
#define __SFUNC__ "v2x  "

void v2x (systemdata **fSy, Vec *fv, Vec *fx, int *nfa, int *flnfa, int *option)
{
  Vec v;
  Vec x;

  int ierr;            /* error checking variable */
  int i, k;            /* loop counters */
  PetscScalar *v_scan;         /* scanning vector of v */
  PetscScalar *x_scan;         /* scanning vector of x */

  systemdata  *Sy;
  int nfields;
  int lnfa;
  int nodloc;
  int anfa; /* absolute value of nfa[k] */

  v = *fv;
  x = *fx;

  Sy   = *fSy;
  nfields = Sy->nfields;
  nodloc  = Sy->Pa->nodloc;
  lnfa    = *flnfa;

  ierr = VecGetArray(v, &v_scan);      CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGetArray(x, &x_scan);      CHKERRABORT(PETSC_COMM_WORLD, ierr);

  if (*option == INSERT)
    {
      for (i = 0; i < nodloc; i++)
	for (k = 0; k < lnfa; k++)
	  {
	    anfa = nfa[k] < 0 ? -nfa[k] : nfa[k] ;
	    x_scan[i*lnfa+k] = v_scan[i*nfields+anfa-1];
	  }
    }
  else
    {
      for (i = 0; i < nodloc; i++)
	for (k = 0; k < lnfa; k++)
	  {
	    anfa = nfa[k] < 0 ? -nfa[k] : nfa[k] ;
	    x_scan[i*lnfa+k] += v_scan[i*nfields+anfa-1];
	  }
    }

  ierr = VecRestoreArray(v, &v_scan);    CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecRestoreArray(x, &x_scan);    CHKERRABORT(PETSC_COMM_WORLD, ierr);
}


/*-----------------------------------------------------------------------------
    Function va2x - Set the initial guess equal to that of the last 
                   continuation step
    Programmed by Adrian Lew - Started: 21/11/97 - Finished: 21/11/97

    This function must be called from Fortran. 

Variables names:
     Sy: system structure
     v: vector of unknowns 
     x: solution vector
     nfa: array with the scalars to copy ( in Fortran numbering )
     lnfa: lenght of array nfa
     option: ADD or INSERT

Returned values:
     NONE

-----------------------------------------------------------------------------*/
#undef  __SFUNC__
#define __SFUNC__ "va2x  "

void va2x (systemdata **fSy, Vec *fv, Vec *fx, int *nfa, int *flnfa,
	   int *option)
{
  Vec va;
  Vec x;

  int ierr;            /* error checking variable */
  int i, k;            /* loop counters */
  PetscScalar *va_scan;         /* scanning vector of v */
  PetscScalar *x_scan;         /* scanning vector of x */

  systemdata  *Sy;
  int nfields;
  int lnfa;
  int nodloc;
  int anfa; /* absolute value of nfa[k] */

  x  = *fx;
  Sy   = *fSy;
  va = Sy->va;
  nfields = Sy->nfields;
  nodloc  = Sy->Pa->nodloc;
  lnfa    = *flnfa;

  ierr = VecGetArray(va, &va_scan);      CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGetArray(x, &x_scan);        CHKERRABORT(PETSC_COMM_WORLD, ierr);

  if (*option == INSERT)
    {
      for (i = 0; i < nodloc; i++)
	for (k = 0; k < lnfa; k++)
	  {
	    anfa = nfa[k] < 0 ? -nfa[k] : nfa[k] ;
	    x_scan[i*lnfa+k] = va_scan[i*nfields+anfa-1];
	  }
    }
  else
    {
      for (i = 0; i < nodloc; i++)
	for (k = 0; k < lnfa; k++)
	  {
	    anfa = nfa[k] < 0 ? -nfa[k] : nfa[k] ;
	    x_scan[i*lnfa+k] += va_scan[i*nfields+anfa-1];
	  }
    }

  ierr = VecRestoreArray(va, &va_scan);  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecRestoreArray(x, &x_scan);    CHKERRABORT(PETSC_COMM_WORLD, ierr);
}

/*----------------------------------------------------------------------
    Function v2x_c
    This function must be called from Fortran. 

Variables names:
     v: vector of unknowns 
     x: solution vector
     nfields: number of unknowns per node
     nfa: first unknown to copy
     lnfa: number of unknowns to copy
     option: ADD or INSERT

Returned values:
     NONE

----------------------------------------------------------------------------*/
#undef  __SFUNC__
#define __SFUNC__ "v2xc"

void v2xc (Vec *fv, Vec *fx, int *fnfields, int *fnfa, int *flnfa,
	   int *option)
{
  Vec v;
  Vec x;

  int ierr;            /* error checking variable */
  int i, k;
  PetscScalar *v_scan; /* scanning vector of v */
  PetscScalar *x_scan; /* scanning vector of x */

  int nfields;
  int nfa, lnfa;
  int nodloc, locsiz;

  v = *fv;
  x = *fx;

  nfields = *fnfields;
  VecGetLocalSize (v, &locsiz);
  nodloc  = locsiz / nfields;
  lnfa    = *flnfa;
  nfa     = *fnfa;

  ierr = VecGetArray(v, &v_scan);         CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGetArray(x, &x_scan);         CHKERRABORT(PETSC_COMM_WORLD, ierr);

  if (*option == INSERT)
    {
      for (i = 0; i < nodloc; i++)
	for (k = 0; k < lnfa; k++)
	  {
	    x_scan[i*lnfa+k] = v_scan[i*nfields+nfa+k-1];
	  }
    }
  else
    {
      for (i = 0; i < nodloc; i++)
	for (k = 0; k < lnfa; k++)
	  {
	    x_scan[i*lnfa+k] += v_scan[i*nfields+nfa+k-1];
	  }
    }

  ierr = VecRestoreArray(v, &v_scan);     CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecRestoreArray(x, &x_scan);     CHKERRABORT(PETSC_COMM_WORLD, ierr);
}


/*----------------------------------------------------------------------
    Function x2v - update the vector v with the new solution
                 
    Programmed by Adrian Lew - Started: 21/11/97 - Finished: 21/11/97

    This function must be called from Fortran. 

Variables names:
     Sy: system structure
     v: vector of unknowns (with ghosted values)
     x: solution vector (without ghosted values)
     mpn: main processor number
     nfa: array with the scalars to copy ( in Fortran numbering )
     lnfa: lenght of array nfa
     option: ADD or INSERT

Returned values:
     NONE
     
Note:
    (r) means that this variable has sense only at mpn.
----------------------------------------------------------------------------*/
#undef  __SFUNC__
#define __SFUNC__ "x2v  "

void x2v (systemdata **fSy, Vec *fv, Vec *fx, int *fmpn, int *nfa, int *flnfa, 
	 int *option)
{
  Vec v;
  Vec x;

  int ierr;            /* error checking variable */
  int i, k;            /* loop counters */
  PetscScalar    *v_scan;         /* scanning vector of v */
  PetscScalar    *x_scan;         /* scanning vector of x */
  int       rank, size;      /* rank and size in Comm */
  MPI_Comm  Comm;
  double    max, min;        /* Local maximum, local minimum */
  double    delta;           /* auxiliar variable */
  double    Max, Min;        /* Global maximum, Global Minimum (r) */

  systemdata  *Sy;
  int nfields;
  int lnfa;
  int nodloc;
  int mpn;
  int anfa; /* absolute value of nfa[k] */

  v = *fv;
  x = *fx;

  Sy   = *fSy;
  nfields = Sy->nfields;
  nodloc  = Sy->Pa->nodloc;
  lnfa    = *flnfa;
  mpn     = *fmpn;
  Comm    = Sy->Pa->Comm;

  MPI_Comm_rank (Comm, &rank);
  MPI_Comm_size (Comm, &size);

  ierr = MPI_Bcast (&mpn, 1, MPI_INT, 0, Comm );
  if (mpn < 0 || mpn >= size)
    error (EXIT, "Main processor number out of range ");

  ierr = VecGetArray (v, &v_scan);         CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGetArray (x, &x_scan);         CHKERRABORT(PETSC_COMM_WORLD, ierr);

  if (*option == INSERT)
    {
      for (i = 0; i < nodloc; i++)
	for (k = 0; k < lnfa; k++)
	  {
	    anfa = nfa[k] < 0 ? -nfa[k] : nfa[k] ;
	    v_scan[i*nfields+anfa-1] = x_scan[i*lnfa+k];
	  }
    }
  else
    {
      max = min = x_scan[0];

      for (i = 0; i < nodloc; i++)
	for (k = 0; k < lnfa; k++){
	  delta = x_scan[i*lnfa+k];
	  anfa = nfa[k] < 0 ? -nfa[k] : nfa[k] ;
	  v_scan[i*nfields+anfa-1] += delta;
	  if (min > delta)
	    min = delta;
	  if (max < delta)
	    max = delta;
      }

    MPI_Reduce (&max, &Max, 1, MPI_DOUBLE, MPI_MAX, mpn, Comm);
    MPI_Reduce (&min, &Min, 1, MPI_DOUBLE, MPI_MIN, mpn, Comm);
    
    PetscPrintf(Comm, " Max: %g     Min: %g\n", Max, Min); 
  }

  ierr = VecRestoreArray(v, &v_scan);      CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecRestoreArray(x, &x_scan);      CHKERRABORT(PETSC_COMM_WORLD, ierr);
}

/*----------------------------------------------------------------------
    Function x2v2va - update the vector v with the new solution
                    - saves previous value in va.

    Programmed by E.Dari - Started: 2003-08-06 - Finished: 

    function usually called from Fortran. 

Variables names:
     Sy: system structure
     v: vector of unknowns (with ghosted values)
     x: solution vector
     mpn: main processor number
     nfa: array with the scalars to copy ( in Fortran numbering )
     lnfa: lenght of array nfa
     option: ADD or INSERT

Returned values:
     NONE
     
Note:
    (r) means that this variable has sense only at mpn.
----------------------------------------------------------------------------*/
#undef  __SFUNC__
#define __SFUNC__ "x2v2va  "

void x2v2va (systemdata **fSy, Vec *fv, Vec *fx, int *fmpn,
	     int *nfa, int *flnfa, int *option)
{
  Vec         v;
  Vec         x;

  int       ierr;            /* error checking variable */
  int       i, k;            /* loop counters */
  PetscScalar *v_scan;       /* scanning vector of v */
  PetscScalar *va_scan;      /* scanning vector of va */
  PetscScalar *x_scan;       /* scanning vector of x */
  int       rank, size;      /* rank and size in Comm */
  MPI_Comm  Comm;
  double    max, min;        /* Local maximum, local minimum */
  double    delta;           /* auxiliar variable */
  double    Max, Min;        /* Global maximum, Global Minimum (r) */

  systemdata  *Sy;
  int         nfields;
  int         lnfa;
  int         nodloc;
  int         mpn;
  int         anfa; /* absolute value of nfa[k] */

  v = *fv;
  x = *fx;

  Sy   = *fSy;
  nfields = Sy->nfields;
  nodloc  = Sy->Pa->nodloc;
  lnfa    = *flnfa;
  mpn     = *fmpn;
  Comm    = Sy->Pa->Comm;

  MPI_Comm_rank(Comm, &rank);
  MPI_Comm_size(Comm, &size);

  ierr = MPI_Bcast(&mpn, 1, MPI_INT, 0, Comm );
  if (mpn < 0 || mpn >= size)
    error(EXIT, "Main processor number out of range ");

  ierr = VecGetArray(v, &v_scan);        CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGetArray(Sy->va, &va_scan);  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGetArray(x, &x_scan);        CHKERRABORT(PETSC_COMM_WORLD, ierr);

  if (*option == INSERT)
    {
      for(i = 0; i < nodloc; i++)
	for(k = 0; k < lnfa; k++)
	  {
	    anfa = nfa[k] < 0 ? -nfa[k] : nfa[k] ;
	    va_scan[i*nfields+anfa-1] = v_scan[i*nfields+anfa-1];
	    v_scan[i*nfields+anfa-1] = x_scan[i*lnfa+k];
	  }
    }
  else
    {
      max = min = x_scan[0];

      for (i = 0; i < nodloc; i++)
	for (k = 0; k < lnfa; k++)
	  {
	    delta = x_scan[i*lnfa+k];
	    anfa = nfa[k] < 0 ? -nfa[k] : nfa[k] ;
	    va_scan[i*nfields+anfa-1] = v_scan[i*nfields+anfa-1];
	    v_scan[i*nfields+anfa-1] += delta;
	    if (min > delta)
	      min = delta;
	    if (max < delta)
	      max = delta;
	  }

      MPI_Reduce(&max, &Max, 1, MPI_DOUBLE, MPI_MAX, mpn, Comm);
      MPI_Reduce(&min, &Min, 1, MPI_DOUBLE, MPI_MIN, mpn, Comm);
    
      PetscPrintf(Comm, " Max: %g     Min: %g\n", Max, Min); 
    }

  ierr = VecRestoreArray(v, &v_scan);      CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecRestoreArray(Sy->va, &va_scan);CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecRestoreArray(x, &x_scan);      CHKERRABORT(PETSC_COMM_WORLD, ierr);

  /* ierr = VecScatterBegin(Sy->G2Lscat, Sy->va, Sy->varaext, INSERT_VALUES, */
  /* 			 SCATTER_FORWARD); CHKERRABORT(PETSC_COMM_WORLD, ierr); */

  /* ierr = VecScatterEnd(Sy->G2Lscat, Sy->va, Sy->varaext, INSERT_VALUES, */
  /* 		       SCATTER_FORWARD);   CHKERRABORT(PETSC_COMM_WORLD, ierr); */
  ierr = VecGhostUpdateBegin(Sy->va, INSERT_VALUES, SCATTER_FORWARD
			     );           CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGhostUpdateEnd  (Sy->va, INSERT_VALUES, SCATTER_FORWARD
			     );           CHKERRABORT(PETSC_COMM_WORLD, ierr);
}

/*----------------------------------------------------------------------
    Function v2va - update the vector va with the vector v values
                 
    Programmed by Adrian Lew - Started: 1/4/98 - Finished: 1/4/98

    This function must be called from Fortran. 

Variables names:
     Sy: system structure
     v: vector of unknowns in the last continuation parameter

Returned values:
     NONE
     
Note:
    (r) means that this variable has sense only at mpn.
----------------------------------------------------------------------------*/
#undef  __SFUNC__
#define __SFUNC__ "v2va  "

void v2va(systemdata **fSy, Vec *fv)
{
  Vec         v;               /* vector of unknowns */

  int         ierr;            /* error checking variable */
  systemdata  *Sy;

  Sy   = *fSy;

  v = *fv;

  ierr = VecCopy(v, Sy->va);               CHKERRABORT(PETSC_COMM_WORLD, ierr);

  /* ierr = VecScatterBegin(Sy->G2Lscat, v, Sy->varaext, INSERT_VALUES, */
  /* 			 SCATTER_FORWARD); CHKERRABORT(PETSC_COMM_WORLD, ierr); */

  /* ierr = VecScatterEnd(Sy->G2Lscat, v, Sy->varaext, INSERT_VALUES, */
  /* 		       SCATTER_FORWARD);   CHKERRABORT(PETSC_COMM_WORLD, ierr); */
  ierr = VecGhostUpdateBegin(Sy->va, INSERT_VALUES, SCATTER_FORWARD
			     );           CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGhostUpdateEnd  (Sy->va, INSERT_VALUES, SCATTER_FORWARD
			     );           CHKERRABORT(PETSC_COMM_WORLD, ierr);
}

/*----------------------------------------------------------------------
    Function vshift - 
                 
    Programmed by 

    This function must be called from Fortran. 

Variables names:
     Sy: system structure
     v: vector of unknowns 
     nfao: origin index (between 1 and nfields)in va of scalars to be copied
     nfad: destination index (between 1 and nfields) in v of scalars to be copied
     lnfa: number of scalars to be copied and shifted
Returned values:
     NONE
     
Note:
    (r) means that this variable has sense only at mpn.
----------------------------------------------------------------------------*/
#undef  __SFUNC__
#define __SFUNC__ "va2vshift"

void va2vshift(systemdata **fSy, Vec *fv, int *fnfao, int *fnfad, int *flnfa)
{
  Vec         v;

  int       ierr;                    /* error checking variable */
  int       i, k;                    /* loop counters */
  PetscScalar    *v_scan, *va_scan;       /* scanning vector of v and va*/

  systemdata  *Sy;
  int         nfields;
  int         nodloc;
  int         nfao, nfad;            /* origin and destination fields */
  int         lnfa;

  v = *fv;

  Sy   = *fSy;
  nfields = Sy->nfields;
  nodloc  = Sy->Pa->nodloc;
  lnfa    = *flnfa;
  if (lnfa)
    {
      nfao    = *fnfao-1;
      nfad    = *fnfad-1;
      if (nfao+lnfa > nfields || nfad+lnfa > nfields ||
	  lnfa < 0 || nfao < 0 || nfad < 0)
	error(EXIT, "Parameters out of range");

      ierr = VecGetArray(Sy->va, &va_scan);CHKERRABORT(PETSC_COMM_WORLD, ierr);
      ierr = VecGetArray(v, &v_scan);      CHKERRABORT(PETSC_COMM_WORLD, ierr);

      for (i = 0; i < nodloc; i++)
	for (k = 0; k < lnfa; k++)
	  v_scan[i*nfields+nfad+k] = va_scan[i*nfields+nfao+k];

      ierr = VecRestoreArray(Sy->va,
			     &va_scan);    CHKERRABORT(PETSC_COMM_WORLD, ierr);
      ierr = VecRestoreArray(v, &v_scan);  CHKERRABORT(PETSC_COMM_WORLD, ierr);
    }
}

/*----------------------------------------------------------------------
    Function x2v2vanorm
    - update the vector v with the new solution
    - saves previous value in va.
    - reports norm of updated vector and norm of update

    Programmed by E.Dari - Started: 2007-07-12 - Finished: 2007-07-12

    function usually called from Fortran. 

Variables names:
     Sy: system structure
     v: vector of unknowns 
     x: solution vector

     nfa: array with the scalars to copy ( in Fortran numbering )
     lnfa: lenght of array nfa
     option: ADD or INSERT
     norm: type of norm (-1: Inf, 0:don't compute, 1,2: Norm 1, 2)

Returned values:
     nvec:(return) norm of received vector
     nupd:(return) nomr of the update
----------------------------------------------------------------------------*/
#undef  __SFUNC__
#define __SFUNC__ "x2v2vanorm"

void x2v2vanorm (systemdata **fSy, Vec *fv, Vec *fx,
		 int *nfa, int *flnfa, int *option,
		 int *norm, PetscScalar *nvec, PetscScalar *nupd)
{
  Vec         v;
  Vec         x;

  int       ierr;            /* error checking variable */
  int       i, k;            /* loop counters */
  PetscScalar *v_scan;       /* scanning vector of v */
  PetscScalar *va_scan;      /* scanning vector of va */
  PetscScalar *x_scan;       /* scanning vector of x */
  MPI_Comm  Comm;

  double    delta;           /* auxiliar variable */
  PetscScalar nor_vec, nor_upd, Nor_vec, Nor_upd;

  systemdata  *Sy;
  int         nfields;
  int         lnfa;
  int         nodloc;
  int         anfa; /* absolute value of nfa[k] */

  v = *fv;
  x = *fx;

  Sy   = *fSy;
  nfields = Sy->nfields;
  nodloc  = Sy->Pa->nodloc;
  lnfa    = *flnfa;

  Comm    = Sy->Pa->Comm;

  ierr = VecGetArray(v, &v_scan);        CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGetArray(Sy->va, &va_scan);  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGetArray(x, &x_scan);        CHKERRABORT(PETSC_COMM_WORLD, ierr);

  if (*norm != 0) /* Norm is computed */
      nor_vec = nor_upd = 0.0;

  if (*option == INSERT)     /* x contains new values for unknowns */
    {
      for(i = 0; i < nodloc; i++)
	for(k = 0; k < lnfa; k++)
	  {
	    anfa = nfa[k] < 0 ? -nfa[k] : nfa[k] ;
	    va_scan[i*nfields+anfa-1] = v_scan[i*nfields+anfa-1];
	    if (*norm != 0)
	      {
		delta = x_scan[i*lnfa+k] - v_scan[i*nfields+anfa-1];
		if (*norm == -1) /* Infinite norm */
		  {
		    if (fabs(x_scan[i*lnfa+k]) > nor_vec)
		      nor_vec = fabs(x_scan[i*lnfa+k]);
		    if (fabs(delta) > nor_upd)
		      nor_upd = fabs(delta);
		  }
		else if (*norm == 1) /* Norm 1 */
		  {
		    nor_vec += fabs(x_scan[i*lnfa+k]);
		    nor_upd += fabs(delta);
		  }
		else if (*norm == 2) /* Norm 2 */
		  {
		    nor_vec += x_scan[i*lnfa+k] * x_scan[i*lnfa+k];
		    nor_upd += delta*delta;
		  }
	      }
	    v_scan[i*nfields+anfa-1] = x_scan[i*lnfa+k];
	  }
    }
  else                       /* x contains updates for unknowns */
    {
      /*      max = min = x_scan[0]; */

      for (i = 0; i < nodloc; i++)
	for (k = 0; k < lnfa; k++)
	  {
	    delta = x_scan[i*lnfa+k];
	    anfa = nfa[k] < 0 ? -nfa[k] : nfa[k] ;
	    va_scan[i*nfields+anfa-1] = v_scan[i*nfields+anfa-1];
	    v_scan[i*nfields+anfa-1] += delta;

	    if (*norm == -1) /* Infinite norm */
	      {
		if (fabs(v_scan[i*nfields+anfa-1]) > nor_vec)
		  nor_vec = fabs(v_scan[i*nfields+anfa-1]);
		if (fabs(delta) > nor_upd)
		  nor_upd = fabs(delta);
	      }
	    else if (*norm == 1)
	      {
		nor_vec += fabs(v_scan[i*nfields+anfa-1]);
		nor_upd += fabs(delta);
	      }
	    else if (*norm == 2)
	      {
		nor_vec += v_scan[i*nfields+anfa-1] * v_scan[i*nfields+anfa-1];
		nor_upd += delta*delta;
	      }
	    /*	    if (min > delta)
		    min = delta;
		    if (max < delta)
		    max = delta; */
	  }
      /*
      MPI_Reduce(&max, &Max, 1, MPI_DOUBLE, MPI_MAX, mpn, Comm);
      MPI_Reduce(&min, &Min, 1, MPI_DOUBLE, MPI_MIN, mpn, Comm);
      PetscPrintf(Comm, " Max: %g     Min: %g\n", Max, Min); 
      */
    }

  ierr = VecRestoreArray(v, &v_scan);      CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecRestoreArray(Sy->va, &va_scan);CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecRestoreArray(x, &x_scan);      CHKERRABORT(PETSC_COMM_WORLD, ierr);

  /* ierr = VecScatterBegin(Sy->G2Lscat, Sy->va, Sy->varaext, INSERT_VALUES, */
  /* 			 SCATTER_FORWARD); CHKERRABORT(PETSC_COMM_WORLD, ierr); */

  /* ierr = VecScatterEnd(Sy->G2Lscat, Sy->va, Sy->varaext, INSERT_VALUES, */
  /* 		       SCATTER_FORWARD);   CHKERRABORT(PETSC_COMM_WORLD, ierr); */
  ierr = VecGhostUpdateBegin(Sy->va, INSERT_VALUES, SCATTER_FORWARD
			     );           CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGhostUpdateEnd  (Sy->va, INSERT_VALUES, SCATTER_FORWARD
			     );           CHKERRABORT(PETSC_COMM_WORLD, ierr);
  
  if (*norm == 0)
    return;

  if (*norm == -1) /* Infinite norm */
    {
      ierr = MPI_Allreduce(&nor_vec, &Nor_vec, 1, MPI_DOUBLE, MPI_MAX, Comm);
      ierr = MPI_Allreduce(&nor_upd, &Nor_upd, 1, MPI_DOUBLE, MPI_MAX, Comm);
    }
  else
    {
      ierr = MPI_Allreduce(&nor_vec, &Nor_vec, 1, MPI_DOUBLE, MPI_SUM, Comm);
      ierr = MPI_Allreduce(&nor_upd, &Nor_upd, 1, MPI_DOUBLE, MPI_SUM, Comm);
    }

  if (*norm == 2)
    {
      Nor_vec = sqrt(Nor_vec);
      Nor_upd = sqrt(Nor_upd);
    }
  *nvec = Nor_vec;
  *nupd = Nor_upd;
}

/*----------------------------------------------------------------------
    Function x2vnorm
    - update the vector v with the new solution
    - reports norm of updated vector and norm of update

    Programmed by E.Dari - Started: 2007-07-12 - Finished: 2007-07-12

    function usually called from Fortran. 

Variables names:
     Sy: system structure
     v: vector of unknowns 
     x: solution vector

     nfa: array with the scalars to copy
         ( in Fortran numbering, if zero, the field is not updated )
     lnfa: lenght of array nfa
     option: ADD or INSERT
     norm: type of norm (-1: Inf, 0:don't compute, 1,2: Norm 1, 2)

Returned values:
     nvec:(return) norm of received vector
     nupd:(return) nomr of the update
----------------------------------------------------------------------------*/
#undef  __SFUNC__
#define __SFUNC__ "x2vnorm"

void x2vnorm (systemdata **fSy, Vec *fv, Vec *fx,
	      int *nfa, int *flnfa, int *option,
	      int *norm, PetscScalar *nvec, PetscScalar *nupd)
{
  Vec         v;
  Vec         x;

  int       ierr;            /* error checking variable */
  int       i, k;            /* loop counters */
  PetscScalar *v_scan;       /* scanning vector of v */
  PetscScalar *x_scan;       /* scanning vector of x */
  MPI_Comm  Comm;

  double    delta;           /* auxiliar variable */
  PetscScalar nor_vec, nor_upd, Nor_vec, Nor_upd;

  systemdata  *Sy;
  int         nfields;
  int         lnfa;
  int         nodloc;
  int         anfa; /* absolute value of nfa[k] */

  v = *fv;
  x = *fx;

  Sy   = *fSy;
  nfields = Sy->nfields;
  nodloc  = Sy->Pa->nodloc;
  lnfa    = *flnfa;

  Comm    = Sy->Pa->Comm;

  ierr = VecGetArray(v, &v_scan);        CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGetArray(x, &x_scan);        CHKERRABORT(PETSC_COMM_WORLD, ierr);

  if (*norm != 0) /* Norm is computed */
      nor_vec = nor_upd = 0.0;

  if (*option == INSERT)     /* x contains new values for unknowns */
    {
      /*      printf("x2vnorm:INSERTing %d unk/node, norm:%d\n",lnfa,*norm);*/

      for (i = 0; i < nodloc; i++)
	for (k = 0; k < lnfa; k++)
	  {
	    if (nfa[k] == 0)
	      continue;
	    anfa = nfa[k] < 0 ? -nfa[k] : nfa[k] ;
	    if (*norm != 0)
	      {
		delta = x_scan[i*lnfa+k] - v_scan[i*nfields+anfa-1];
		if (*norm == -1) /* Infinite norm */
		  {
		    if (fabs(x_scan[i*lnfa+k]) > nor_vec)
		      nor_vec = fabs(x_scan[i*lnfa+k]);
		    if (fabs(delta) > nor_upd)
		      nor_upd = fabs(delta);
		  }
		else if (*norm == 1) /* Norm 1 */
		  {
		    nor_vec += fabs(x_scan[i*lnfa+k]);
		    nor_upd += fabs(delta);
		  }
		else if (*norm == 2) /* Norm 2 */
		  {
		    nor_vec += x_scan[i*lnfa+k] * x_scan[i*lnfa+k];
		    nor_upd += delta*delta;
		  }
	      }
	    v_scan[i*nfields+anfa-1] = x_scan[i*lnfa+k];
	  }
    }
  else                       /* x contains updates for unknowns */
    {
      /*      printf("x2vnorm:ADDing %d unk/node, norm:%d\n",lnfa,*norm);*/
      for (i = 0; i < nodloc; i++)
	for (k = 0; k < lnfa; k++)
	  {
	    if (nfa[k] == 0)
	      continue;
	    delta = x_scan[i*lnfa+k];
	    anfa = nfa[k] < 0 ? -nfa[k] : nfa[k] ;
	    v_scan[i*nfields+anfa-1] += delta;

	    if (*norm == -1) /* Infinite norm */
	      {
		if (fabs(v_scan[i*nfields+anfa-1]) > nor_vec)
		  nor_vec = fabs(v_scan[i*nfields+anfa-1]);
		if (fabs(delta) > nor_upd)
		  nor_upd = fabs(delta);
	      }
	    else if (*norm == 1)
	      {
		nor_vec += fabs(v_scan[i*nfields+anfa-1]);
		nor_upd += fabs(delta);
	      }
	    else if (*norm == 2)
	      {
		nor_vec += v_scan[i*nfields+anfa-1] * v_scan[i*nfields+anfa-1];
		nor_upd += delta*delta;
	      }
	  }
    }

  ierr = VecRestoreArray(v, &v_scan);      CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecRestoreArray(x, &x_scan);      CHKERRABORT(PETSC_COMM_WORLD, ierr);

  /* ierr = VecScatterBegin(Sy->G2Lscat, v, Sy->varext, INSERT_VALUES, */
  /* 			 SCATTER_FORWARD); CHKERRABORT(PETSC_COMM_WORLD, ierr); */

  /* ierr = VecScatterEnd(Sy->G2Lscat, v, Sy->varext, INSERT_VALUES, */
  /* 		       SCATTER_FORWARD);   CHKERRABORT(PETSC_COMM_WORLD, ierr); */
  ierr = VecGhostUpdateBegin(v, INSERT_VALUES, SCATTER_FORWARD
			     );           CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGhostUpdateEnd  (v, INSERT_VALUES, SCATTER_FORWARD
			     );           CHKERRABORT(PETSC_COMM_WORLD, ierr);
  if (*norm == 0)
    return;

  if (*norm == -1) /* Infinite norm */
    {
      ierr = MPI_Allreduce(&nor_vec, &Nor_vec, 1, MPI_DOUBLE, MPI_MAX, Comm);
      ierr = MPI_Allreduce(&nor_upd, &Nor_upd, 1, MPI_DOUBLE, MPI_MAX, Comm);
    }
  else
    {
      ierr = MPI_Allreduce(&nor_vec, &Nor_vec, 1, MPI_DOUBLE, MPI_SUM, Comm);
      ierr = MPI_Allreduce(&nor_upd, &Nor_upd, 1, MPI_DOUBLE, MPI_SUM, Comm);
    }

  if (*norm == 2)
    {
      Nor_vec = sqrt(Nor_vec);
      Nor_upd = sqrt(Nor_upd);
    }
  *nvec = Nor_vec;
  *nupd = Nor_upd;
}

/*----------------------------------------------------------------------
    Function pgpfep_x2v
    - update (partially) the vector of unknowns with a new solution
    - reports norm of updated vector and norm of update
    - Apply (optionally) a relaxation factor

    Programmed by E.Dari - Started: 2018-08-1 - Finished: 2018-08-1

    function usually called from Fortran. 

Variables names:
     Sy: system structure
     v: vector of unknowns 
     x: solution vector

     nfa: array with the scalars to copy
         ( in Fortran numbering, if zero, the field is not updated )
     lnfa: lenght of array nfa
     option: ADD or INSERT
     alpha: relaxation factor
     norm: type of norm (-1: Inf, 0:don't compute, 1,2: Norm 1, 2)

Returned values:
     nvec:(return) norm of received vector
     nupd:(return) nomr of the update
----------------------------------------------------------------------------*/
#undef  __SFUNC__
#define __SFUNC__ "pgpfepx2v"

void pgpfepx2v (systemdata **fSy, Vec *fv, Vec *fx,
	      int *nfa, int *flnfa, int *option, double *alpha,
	      int *norm, PetscScalar *nvec, PetscScalar *nupd)
{
  Vec         v;
  Vec         x;

  int       ierr;            /* error checking variable */
  int       i, k;            /* loop counters */
  PetscScalar *v_scan;       /* scanning vector of v */
  PetscScalar *x_scan;       /* scanning vector of x */
  MPI_Comm  Comm;

  double    delta, newvalue;           /* auxiliar variable */
  PetscScalar nor_vec, nor_upd, Nor_vec, Nor_upd;

  systemdata  *Sy;
  int nfields;
  int lnfa;
  int nodloc;
  int anfa; /* absolute value of nfa[k] */

  v = *fv;
  x = *fx;

  Sy   = *fSy;
  nfields = Sy->nfields;
  nodloc  = Sy->Pa->nodloc;
  lnfa    = *flnfa;

  Comm    = Sy->Pa->Comm;

  ierr = VecGetArray(v, &v_scan);        CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGetArray(x, &x_scan);        CHKERRABORT(PETSC_COMM_WORLD, ierr);

  if (*norm != 0) /* Norm is computed */
      nor_vec = nor_upd = 0.0;

  for (i = 0; i < nodloc; i++) /* loop over local nodes */
    for (k = 0; k < lnfa; k++) /* loop over fields to update */
      {
	if (nfa[k] == 0)       /* skip field */
	  continue;
	anfa = nfa[k] < 0 ? -nfa[k] : nfa[k] ; /* position in v */
	if (*option == INSERT)     /* x contains new values for unknowns */
	  delta = *alpha *
	    (x_scan[i * lnfa + k] - v_scan[i * nfields + anfa-1]);
	else                       /* x contains updates for unknowns */
	  delta = *alpha * x_scan[i*lnfa+k];
	newvalue = v_scan[i * nfields + anfa-1] + delta;
	v_scan[i * nfields + anfa-1] = newvalue;

	if (*norm != 0)
	  {
	    if (*norm == -1) /* Infinite norm */
	      {
		if (fabs(newvalue) > nor_vec)
		  nor_vec = fabs(newvalue);
		if (fabs(delta) > nor_upd)
		  nor_upd = fabs(delta);
	      }
	    else if (*norm == 1) /* Norm 1 */
	      {
		nor_vec += fabs(newvalue);
		nor_upd += fabs(delta);
	      }
	    else if (*norm == 2) /* Norm 2 */
	      {
		nor_vec += newvalue * newvalue;
		nor_upd += delta * delta;
	      }
	  } /* end if "compute norm" */
      } /* loop over local nodes and fields to update */

  ierr = VecRestoreArray(v, &v_scan);      CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecRestoreArray(x, &x_scan);      CHKERRABORT(PETSC_COMM_WORLD, ierr);

  ierr = VecGhostUpdateBegin(v, INSERT_VALUES, SCATTER_FORWARD
			     );           CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = VecGhostUpdateEnd  (v, INSERT_VALUES, SCATTER_FORWARD
			     );           CHKERRABORT(PETSC_COMM_WORLD, ierr);
  if (*norm == 0)
    return;

  if (*norm == -1) /* Infinite norm, reduce with MAX function */
    {
      ierr = MPI_Allreduce(&nor_vec, &Nor_vec, 1, MPI_DOUBLE, MPI_MAX, Comm);
      ierr = MPI_Allreduce(&nor_upd, &Nor_upd, 1, MPI_DOUBLE, MPI_MAX, Comm);
    }
  else /* 1 or 2 norm: reduce with SUM function */
    {
      ierr = MPI_Allreduce(&nor_vec, &Nor_vec, 1, MPI_DOUBLE, MPI_SUM, Comm);
      ierr = MPI_Allreduce(&nor_upd, &Nor_upd, 1, MPI_DOUBLE, MPI_SUM, Comm);
    }

  if (*norm == 2)
    {
      Nor_vec = sqrt(Nor_vec);
      Nor_upd = sqrt(Nor_upd);
    }
  *nvec = Nor_vec;
  *nupd = Nor_upd;
}

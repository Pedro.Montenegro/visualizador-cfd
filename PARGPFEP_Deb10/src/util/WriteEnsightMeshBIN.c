#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

void writeensightmeshbin_ (double *xyz, int *je, int *nod, int *nel, int *nsd)
{
  char name[300], auxname[300];
  char buffer[81]="                                        "
    "                                        ";
  int fdfile, i, j, nnoel;
  float *coords;

  printf("Output_Mesh_File?\n");
  scanf("%s", name);
  sprintf(auxname, "%s.geo", name);
  fdfile = open(auxname, O_WRONLY|O_CREAT,
		S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
  sprintf(buffer, "%-80s", "C Binary");
  write(fdfile, buffer, 80);
  sprintf(buffer, "%-80s", "Created by vwm2vtk");
  write(fdfile, buffer, 80);
  sprintf(buffer, "%-80s", "Preprocessor2D program");
  write(fdfile, buffer, 80);
  sprintf(buffer, "%-80s", "node id given");
  write(fdfile, buffer, 80);
  sprintf(buffer, "%-80s", "element id given");
  write(fdfile, buffer, 80);
  sprintf(buffer, "%-80s", "coordinates");
  write(fdfile, buffer, 80);

  write(fdfile, nod, sizeof(int));
  for (i = 1; i <= *nod; i++)
    write(fdfile, &i, sizeof(int));

  coords = (float *) malloc (*nod * 3 * sizeof(float));

  if (*nsd == 2)
    {
      for (i = 0; i < *nod; i++)
	{
	  coords[3*i] = xyz[2*i];
	  coords[3*i+1] = xyz[2*i+1];
	  coords[3*i+2] = 0.0;
	}
    }
  else
    {
      for (i = 0; i < *nod; i++)
	for (j = 0; j < 3; j++)
	  coords[3*i+j] = xyz[3*i+j];
    }

  write(fdfile, coords, 3* *nod * sizeof(float));

  sprintf(buffer, "%-80s", "part 1");
  write(fdfile, buffer, 80);
  sprintf(buffer, "%-80s", "Partition 1");
  write(fdfile, buffer, 80);

  if (*nsd == 2)
    {
      sprintf(buffer, "%-80s", "tria3");
      write(fdfile, buffer, 80);
      nnoel = 3;
    }
  else
    {
      sprintf(buffer, "%-80s", "tetra4");
      write(fdfile, buffer, 80);
      nnoel = 4;
    }

  write(fdfile, nel, sizeof(int));
  for (i = 1; i <= *nel; i++)
    write(fdfile, &i, sizeof(int));

  write(fdfile, je, nnoel* *nel * sizeof(int));

  close(fdfile);
}

C
      program gpboco
C
C     General Purpose Finite Element Program - Boundary Conditions
C
C     PREPROCESSOR
C
C
C     Sergio D. Felicelli and Gustavo C. Buscaglia - 1994
C     .gz support by EAD - 2001
C
      IMPLICIT REAL*8 (A-H,O-Z)
      PARAMETER (MAX_NUMB_FIELDS=25, MAX_NUMB_MESHES=10)
      PARAMETER (MAX_GAUSS_POINTS=27, MAX_GROUPS=30)
!      PARAMETER (MEM4=48000000)
!      PARAMETER (MEM8=24000000)
      CHARACTER*80 FILEGD,FILEBC,FILEMS(MAX_NUMB_MESHES),FILEGS,POSTPR,
     *     FILEST,FILREE,FILPER,FILOUT(MAX_NUMB_FIELDS),TITLE,SOLVER,
     *     FILBCC,FILPOS(MAX_NUMB_FIELDS),
     *     filmso(10),filemssur(MAX_NUMB_MESHES)
      CHARACTER *80 eltype
!      REAL*4 A4(MEM4)
!      REAL*8 A8(MEM8)
!      COMMON /STORE/ MEMMAX
      real*8, allocatable :: coor (:)
      integer*4, allocatable :: ieglo (:)
      integer*4, allocatable :: jeglo (:)
      integer*4, allocatable :: ietgl (:)
      integer*4, allocatable :: jetgl (:)
      integer*4, allocatable :: iper (:)
      integer*4, allocatable :: iinv (:)
      integer*4, allocatable :: ldir (:)
      integer*4, allocatable :: iaux (:)
      real*8, allocatable :: vdir (:)
      real*8, allocatable :: aux (:)
      integer*4, allocatable :: iobl (:)
      integer*4, allocatable :: iaux2 (:)
      real*8, allocatable :: rot (:)
      integer*4, allocatable :: lnob (:)
      real*8, allocatable :: gbase (:)
C
      DIMENSION KFTYP(300),KCOMP(300),KSHAP(300)
      DIMENSION NODTOT(MAX_NUMB_MESHES),MEDJE(MAX_NUMB_MESHES),
     *     NNODE(MAX_NUMB_MESHES)
      DIMENSION INDIE(MAX_NUMB_MESHES),INDJE(MAX_NUMB_MESHES),
     *     INDIET(MAX_NUMB_MESHES),INDJET(MAX_NUMB_MESHES),
     *     INDCOO(MAX_NUMB_MESHES),
     *     INDXXX(300),INDNOD(300),PARMAT(300),PARNUM(300),IELTYP(150),
     *     MESINT(300)
      dimension numnob(MAX_NUMB_MESHES)
C
      FILEGD = 'gpfep.cfg'
C READING OF GENERAL DATA
      include './cfgreading.for'
C
!      LAST8 = 1
!      LAST4 = 1
C READING OF MESH DATA WITH ARRAY DIMENSIONING
      DO IMESH=1,NMESH
         OPEN (IMESH,FILE=FILEMS(IMESH),STATUS='OLD')
         if (IFOUNDKY(IMESH, 'COORDINATES') .NE. 1) THEN
            STOP 'CAN NOT FIND KEYWORD COORDINATES IN MESH FILE'
         end if
         READ (IMESH,*) NODTOT(IMESH)
      END DO
C COORDINATES ARRAYS
      INDCOO(1) = 0
      DO IMESH=1,NMESH
         INDCOO (IMESH+1) = INDCOO(IMESH) + NDIM*NODTOT(IMESH)
      END DO
      LCOOR = INDCOO(NMESH+1)
      Allocate (coor(LCOOR))
!      IF (LAST8 .GT. MEM8+1) THEN
!      	 write (*,*) 'Last8: ', LAST8
!         stop ' Not enough memory (8)'
!      ENDIF
C     READING COORDINATES
      DO IMESH=1,NMESH
         CALL REACOO(IMESH,NDIM,NODTOT(IMESH),coor(INDCOO(IMESH)+1))
      END DO
C
      IF (NDIM .EQ. 2) THEN      ! Conectividades: s�lo 2 D
C
      DO IMESH=1,NMESH
         if (IFOUNDKY(IMESH, 'ELEMENT_GROUPS') .NE. 1) THEN
            if (IFOUNDKY(IMESH, 'ELEMENT GROUPS') .NE. 1)
     *           stop 'Can not find element groups keyword'
         end if
         read (IMESH,*) ngroups
         NEL1 = 0
         MEDJE(IMESH) = 0
         nne = 0
         do igroup = 1, ngroups
            call reaeltyp (IMESH, IGR, nelgr, eltype)
            if (eltype(1:15) .EQ. 'LINEAR_SEGMENT' .OR.
     *           eltype(1:4)  .EQ. 'Seg2') then
               nnegr = 2
               idim = 1
            else if (eltype(1:15) .EQ. 'LINEAR_TRIANGLE' .OR.
     *              eltype(1:4)  .EQ. 'Tri3') then
               nnegr = 3
               idim = 2
            else if (eltype(1:5) .eq. 'Quad4') then
               nnegr = 4
               idim = 2
            else if (eltype(1:18) .EQ. 'LINEAR_TETRAHEDRON' .OR.
     *              ELTYPE(1:6)  .EQ. 'Tetra4') then
               nnegr = 4
               idim = 3
            else
               WRITE (6,*) 'Element not recognized:'
               write (6,*) eltype
               stop 'Error'
            end if
c
            if (nne .eq. 0) nne = nnegr
            if (nne .ne. nnegr) STOP 'Different elements in mesh'
c
            if (idim .ne. ndim) then
               write (6,*) 'WARNING: incompatible dimensions:'
               write (6,*) 'cfg file:', NDIM
               write (6,*) 'mesh file:', idim
            end if
c
            NEL1 = NEL1 + NELGR
            MEDJE(IMESH) = MEDJE(IMESH) + NELGR * nne
         end do
C
C         READ (IMESH,*) NODTOT(IMESH),NEL1,MEDJE(IMESH)
         IF (NEL1.NE.NEL) THEN
            WRITE (6,*) 'THERE EXISTS AN INCOMPATIBILITY BETWEEN YOUR'
            WRITE (6,*) 'GENERAL DATA FILE AND YOUR MESH DATA FILE'
            WRITE (6,*) 'THE PROGRAM HAS ADOPTED THE NUMBER OF ELEMENTS'
            WRITE (6,*) 'GIVEN BY YOUR MESH FILE: ', NEL1   
            IF (NMESH.GT.1) THEN 
               WRITE (6,*) 'WARNING: IF YOU HAVE SEVERAL MESHES WITH'
               WRITE (6,*) 'DIFFERENT NUMBER OF ELEMENTS YOU ARE IN'
               WRITE (6,*) 'SERIOUS TROUBLE (STOP THE PROGRAM)'
            END IF
            NEL = NEL1
	 END IF
      END DO
      INDIE (1) = 0
      INDJE (1) = 0
      INDIET(1) = 0
      INDJET(1) = 0
      DO IMESH=1,NMESH
         INDIE (IMESH+1) = INDIE (IMESH) + NEL + 1
         INDJE (IMESH+1) = INDJE (IMESH) + MEDJE(IMESH)
         INDIET(IMESH+1) = INDIET(IMESH) + NODTOT(IMESH) + 1
         INDJET(IMESH+1) = INDJET(IMESH) + MEDJE(IMESH)
      END DO
      MEDIEG = INDIE (NMESH+1)
      MEDJEG = INDJE (NMESH+1)
      MEIETG = INDIET(NMESH+1)
!      LIEGLO = LAST4
!      LJEGLO = LIEGLO + MEDIEG
!      LIETGL = LJEGLO + MEDJEG
!      LJETGL = LIETGL + MEIETG
!      LAST4  = LJETGL + MEDJEG
      Allocate (ieglo(MEDIEG))
      Allocate (jeglo(MEDJEG))
      Allocate (ietgl(MEIETG))
      Allocate (jetgl(MEDJEG))
!      IF (LAST4 .GT. MEM4+1) THEN
!      	 write (*,*) 'Last4: ', LAST4
!         stop ' Not enough memory (4)'
!      ENDIF
C READING CONNECTIVITIES
C
      DO IMESH=1,NMESH
         if (IFOUNDKY(IMESH, 'INCIDENCE') .NE. 1)
     *        stop 'Can not find INCIDENCE keyword'
         CALL RCONEC(IMESH,NEL,MEDJE(IMESH),ieglo(1+INDIE(IMESH)),
     *               jeglo(1+INDJE(IMESH)),NNODE(IMESH),IMESH)
C         CALL REACON(IMESH,NEL,MEDJE(IMESH),A4(LIEGLO+INDIE(IMESH)),
C     *               A4(LJEGLO+INDJE(IMESH)),NNODE(IMESH),IMESH)
C
      END DO
C TRANSPOSING CONNECTIVITIES
      DO IMESH=1,NMESH
         CALL TRASIM (ieglo(1+INDIE(IMESH)),jeglo(1+INDJE(IMESH)),
     *             ietgl(1+INDIET(IMESH)),jetgl(1+INDJET(IMESH)),
     *             NODTOT(IMESH),NEL)
      END DO
C
      ENDIF       ! Conectividades: s�lo 2 D
C
!C COORDINATES ARRAYS
!      INDCOO(1) = 0
!      DO IMESH=1,NMESH
!         INDCOO (IMESH+1) = INDCOO(IMESH) + NDIM*NODTOT(IMESH)
!      END DO
!      LCOOR = 1
!      LAST8 = LCOOR + INDCOO(NMESH+1)
!C READING COORDINATES
!      DO IMESH=1,NMESH
!         CALL REAXYZ(IMESH,NDIM,NODTOT(IMESH),A8(LCOOR+INDCOO(IMESH)))
!         CLOSE(IMESH)
!      END DO
      DO IMESH=1,NMESH
         CLOSE(IMESH)
      END DO
      write (6,*) 'Mesh reading: Done'
C
C UNKNOWN VECTOR - RELATED ARRAYS
      NUNK = 0
      INDXXX(1) = 0
      DO IFIELD=1,NFIELD
         NUNK = NUNK + NODTOT(KFTYP(IFIELD))*KCOMP(IFIELD)
         INDXXX(IFIELD+1) = NUNK
      END DO
!      LIPER = LAST4
!      LIINV = LIPER + NUNK
!      LAST4 = LIINV + NUNK
      Allocate (iper(NUNK))
      Allocate (iinv(NUNK))
!      LXXXX = LAST8
!      LXORD = LXXXX + 0*NUNK
!      LAST8 = LXORD + 0*NUNK
!      IF (LAST4 .GT. MEM4+1) THEN
!      	 write (*,*) 'Last4: ', LAST4
!         stop ' Not enough memory (4)'
!      ENDIF
!      IF (LAST8 .GT. MEM8+1) THEN
!      	 write (*,*) 'Last8: ', LAST8
!         stop ' Not enough memory (8)'
!      ENDIF
C READING PERMUTATION VECTOR
      IF (FILPER.EQ.' NONE') THEN
         CALL INIPER (NUNK,iper,iinv)
      ELSE IF (FILPER.EQ.' EQUALORDER') THEN
         CALL EQUPER (NFIELD,NODTOT(1),KCOMP,iper,iinv)
      ELSE
         OPEN (1,FILE=FILPER,STATUS='OLD')
         READ (1,*) NUNK1
         IF (NUNK1.NE.NUNK) STOP ' INCOMPATIBLE PERMUTATION FILE'
         CALL READVI (1,NUNK,iper)
         CALL READVI (1,NUNK,iinv)
         CLOSE(1)
      END IF
      write (6,*) 'Permutation vector: Done'
C BOUNDARY CONDITIONS ARRAYS
      Allocate (ldir(NUNK))
      Allocate (iaux(4*NODTOT(1)))   !!!!5ene01
      Allocate (vdir(NUNK))
!?      Allocate (aux(0*NUNK))         !!!!5ene01
      Allocate (aux(1))         !!!!P/evitar error de compilacion
!      LLDIR = LAST4
!      LIAUX = LLDIR + NUNK
!      LAST4 = LIAUX + 4*NODTOT(1)   !!!!5ene01
!      LVDIR = LAST8
!      LAUX  = LVDIR + NUNK
!      LAST8 = LAUX + 0*NUNK         !!!!5ene01
C OBLIQUE NODES ARRAY
      Allocate (iobl(INDCOO(NMESH+1)))
      Allocate (iaux2(3*NODTOT(1)))
      Allocate (rot(INDCOO(NMESH+1) * NDIM * (NDIM - 1)))
      Allocate (lnob(NODTOT(1)))
      Allocate (gbase(NDIM*NDIM*NODTOT(1)))

!      LIOBL  = LAST4
!      LIAUX2 = LIOBL + INDCOO(NMESH+1)
!      LAST4  = LIAUX2 + NODTOT(1) * 3 ! ???????????????????
!      LROT  = LAST8
!      LAST8 = LROT + INDCOO(NMESH+1) * NDIM * (NDIM - 1)
!      LLNOB  = LAST4
!      LAST4 = LLNOB + NODTOT(1)
!      LGBASE = LAST8
!      LAST8  = LGBASE + NDIM*NDIM*NODTOT(1)
C CONSTRUCTING BOUNDARY CONDITIONS
!      IF (LAST4 .GT. MEM4+1) THEN
!      	 write (*,*) 'Last4: ', LAST4
!         stop ' Not enough memory (4)'
!      ENDIF
!      IF (LAST8 .GT. MEM8+1) THEN
!      	 write (*,*) 'Last8: ', LAST8
!         stop ' Not enough memory (8)'
!      ENDIF
      CALL GPCOBC (FILBCC,NDIM,NUNK,NFIELD,NMESH,KFTYP,KCOMP,NODTOT,
     *             INDCOO, coor, INDXXX, iper,
     *             iaux, ldir, vdir, lnob, gbase,
     *             ISWOBL, iobl, rot, iaux2)
!      CALL GPCOBC (FILBCC,NDIM,NUNK,NFIELD,NMESH,KFTYP,KCOMP,NODTOT,
!     *             INDCOO,A8(LCOOR),INDXXX,A4(LIPER),
!     *             A4(LIAUX),A4(LLDIR),A8(LVDIR),A4(LLNOB),A8(LGBASE),
!     *             ISWOBL,A4(LIOBL),A8(LROT),A4(LIAUX2))
C WRITING BOUNDARY CONDITIONS
      IF (FILEBC.NE.' NONE') THEN
         OPEN (1,FILE=FILEBC,STATUS='NEW')
         CALL WRIVI (1, NUNK, ldir)
         CALL WRIVD (1, NUNK, vdir)
!         CALL WRIVI (1,NUNK,A4(LLDIR))
!         CALL WRIVD (1,NUNK,A8(LVDIR))
         CLOSE(1)
      ELSE
         CALL VALVI (NUNK, ldir, 0)
!         CALL VALVI (NUNK,A4(LLDIR),0)
      END IF
C
      WRITE (6,*) ' ----------------------------------- '
      WRITE (6,*) ' GPBOCO: NORMAL COMPLETION '
      WRITE (6,*) ' MEM = ', LAST4,LAST8
      WRITE (6,*) ' ----------------------------------- '
      STOP
C
! 9001 FORMAT (/21X,38('-')
!     *        /20X,'|',38X,'|'
!     *        /20X,'|              G P B O C O             |'
!     *        /20X,'|',38X,'|'
!     *        /20X,'|        GENERAL PURPOSE FINITE        |'
!     *        /20X,'|           ELEMENT PROGRAM            |'
!     *        /20X,'|         BOUNDARY CONDITIONS          |'
!     *        /20X,'|',38X,'|'
!     *        /20X,'|                          S.F. & G.B. |'
!     *        /21X,38('-'))
! 9002 FORMAT(/2X,'File with general data:',$)
! 9003 FORMAT(A80)
      END
C---------------------------------------------------------------------
      SUBROUTINE GPCOBC (FILBCC,NDIM,NUNK,NFIELD,NMESH,
     *                   KFTYP,KCOMP,NODTOT,INDCOO,
     *                   COOR,INDXXX,IPER,IAUX,LDIR,VDIR,LNOB,GBASE,
     *                   ISWOBL, IOBL, ROT, INCID )
C---------------------------------------------------------------------
C     READS BOUNDARY CONDITIONS USING CIRCULAR LISTS OF BDRY NODES
      IMPLICIT REAL*8 (A-H,O-Z)
      CHARACTER *80 BCTYPE,FILBCC
      DIMENSION KFTYP(1),KCOMP(1),NODTOT(1),INDCOO(1),COOR(1),IAUX(1)
      DIMENSION LDIR(1),VDIR(1),INDXXX(1),IPER(1)
      DIMENSION VALUES(20),VAL2(20),VAL3(20),COOLOC(30,30),NLOC(30)
      DIMENSION NNODL(10),INDLIS(10),ISWBC(10)
      DIMENSION LNOB(*),GBASE(*)
C
C DEFINITIONS FOR GROUP MANAGING AND OBLIQUE NODES
C
      PARAMETER (MAXNGRPS=1000)
      CHARACTER*80 FILESUR(10)
      DIMENSION ISWGDEF(10), NELGRP(MAXNGRPS), IOBL(1), ROT(1), INCID(1)
C
      DO IMESH = 1, NMESH
         ISWGDEF(IMESH) = 0
      END DO
C
      ISWOBL = 0
      DO I = 1, INDCOO(NMESH+1)
         IOBL(I) = 0
      END DO
C
C END DEF'S FOR GROUP MANAGING AND OBLIQUE NODES
C
      ppii = 3.141592653589793238462643d+00
      DO I=1,NUNK
         LDIR(I) = 0
         VDIR(I) = 0
      END DO
      OPEN (1,FILE=FILBCC,STATUS='OLD')
      INDLIS(1)=0
      DO IMESH=1,NMESH
C READS NUMBER OF NODES IN THE LIST
         CALL REINKY (1,' NODES IN THE LIST:',NNODL(IMESH))
C READS THE LIST
         READ (1,*)(IAUX(I+INDLIS(IMESH)),I=1,NNODL(IMESH))
         INDLIS(IMESH+1) = INDLIS(IMESH) + NNODL(IMESH)
      END DO
C
C NOW SEVERAL OPTIONS EXIST CONCERNING THE INPUT FORMAT
C
      DO ILOOP=1,1000
         kk = 0
         READ (1,'(A80)',END=200) BCTYPE
         IF (BCTYPE(1:16).EQ.' CONDITION INDEX') THEN
            WRITE (6,*) ' READING CONDITION INDEX'
            WRITE (6,*) '                 '
            kk = 1
            CALL REINKY (1,' FIRST NODE:',N1)
            CALL REINKY (1,' LAST NODE:',N2)
            CALL REINKY (1,' FIELD NUMBER:',IFIELD)
            CALL REINKY (1,' COMPONENTS:',NCOMP)
            CALL REAKEY (1,' ISWBC(1:NCOMP), 1 IF B.C. IS SPECIFIED:')
            READ(1,*) (ISWBC(I),I=1,NCOMP)
            IF (NCOMP.NE.KCOMP(IFIELD)) THEN
               WRITE (6,*) ' INCOMPATIBLE KCOMP(',IFIELD,')= ',
     *                     NCOMP,KCOMP(IFIELD)
               STOP 'BOUNDARY CONDITIONS'
            END IF
            CALL REINKY (1,' INDEX:', INDEXX)
            IN1 = 0
            IN2 = 0
            IMESH = KFTYP(IFIELD)
            IND   = INDLIS(IMESH)
            DO I=1+IND,NNODL(IMESH)+IND
               IF (N1.EQ.IAUX(I)) IN1=I
               IF (N2.EQ.IAUX(I)) IN2=I
            END DO
            IF (IN1.EQ.0.OR.IN2.EQ.0) THEN
               WRITE (6,*) 'FIRST OR LAST NODE IN BC NOT IN THE LIST'
               STOP 'BOUNDARY CONDITIONS'
            END IF
            IF (IN1.LE.IN2) THEN
               DO I=IN1,IN2
                  IJ = INDXXX(IFIELD) + NCOMP*(IAUX(I)-1) 
                  DO ICOMP = 1,NCOMP
                     IF (ISWBC(ICOMP).EQ.1) THEN
                        JJ = IPER(IJ + ICOMP)
                        LDIR(JJ) = INDEXX
                     END IF
                  END DO
               END DO
            ELSE
               DO I=IND+1,IN2
                  IJ = INDXXX(IFIELD) + NCOMP*(IAUX(I)-1) 
                  DO ICOMP = 1,NCOMP
                     IF (ISWBC(ICOMP).EQ.1) THEN
                        JJ = IPER(IJ + ICOMP)
                        LDIR(JJ) = INDEXX
                     END IF
                  END DO
               END DO
               DO I=IN1,IND+NNODL(IMESH)
                  IJ = INDXXX(IFIELD) + NCOMP*(IAUX(I)-1) 
                  DO ICOMP = 1,NCOMP
                     IF (ISWBC(ICOMP).EQ.1) THEN
                        JJ = IPER(IJ + ICOMP)
                        LDIR(JJ) = INDEXX
                     END IF
                  END DO
               END DO
            END IF
         END IF
C
         IF (BCTYPE(1:9).EQ.' CONSTANT') THEN
            WRITE (6,*) ' READING CONSTANT'
            WRITE (6,*) '                 '
            kk = 1
            CALL REINKY (1,' FIRST NODE:',N1)
            CALL REINKY (1,' LAST NODE:',N2)
            CALL REINKY (1,' FIELD NUMBER:',IFIELD)
            CALL REINKY (1,' COMPONENTS:',NCOMP)
            CALL REAKEY (1,' ISWBC(1:NCOMP), 1 IF B.C. IS SPECIFIED:')
            READ(1,*) (ISWBC(I),I=1,NCOMP)
            IF (NCOMP.NE.KCOMP(IFIELD)) THEN
               WRITE (6,*) ' INCOMPATIBLE KCOMP(',IFIELD,')= ',
     *                     NCOMP,KCOMP(IFIELD)
               STOP 'BOUNDARY CONDITIONS'
            END IF
            CALL REAKEY (1,' VALUES (LIST BELOW):')
            DO I=1,NCOMP
               READ (1,*) VALUES(I)
            END DO
            IN1 = 0
            IN2 = 0
            IMESH = KFTYP(IFIELD)
            IND   = INDLIS(IMESH)
            DO I=1+IND,NNODL(IMESH)+IND
               IF (N1.EQ.IAUX(I) .AND. IN1.EQ.0) IN1=I
               IF (N2.EQ.IAUX(I) .AND. IN2.EQ.0) IN2=I
            END DO
            IF (IN1.EQ.0.OR.IN2.EQ.0) THEN
               WRITE (6,*) 'FIRST OR LAST NODE IN BC NOT IN THE LIST'
               STOP 'BOUNDARY CONDITIONS'
            END IF
            IF (IN1.LE.IN2) THEN
               DO I=IN1,IN2
                  IJ = INDXXX(IFIELD) + NCOMP*(IAUX(I)-1) 
                  DO ICOMP = 1,NCOMP
                     IF (ISWBC(ICOMP).EQ.1) THEN
                        JJ = IPER(IJ + ICOMP)
                        VDIR(JJ) = VALUES(ICOMP)
                        LDIR(JJ) = 1
                     END IF
                  END DO
               END DO
            ELSE
               DO I=IND+1,IN2
                  IJ = INDXXX(IFIELD) + NCOMP*(IAUX(I)-1) 
                  DO ICOMP = 1,NCOMP
                     IF (ISWBC(ICOMP).EQ.1) THEN
                        JJ = IPER(IJ + ICOMP)
                        VDIR(JJ) = VALUES(ICOMP)
                        LDIR(JJ) = 1
                     END IF
                  END DO
               END DO
               DO I=IN1,IND+NNODL(IMESH)
                  IJ = INDXXX(IFIELD) + NCOMP*(IAUX(I)-1) 
                  DO ICOMP = 1,NCOMP
                     IF (ISWBC(ICOMP).EQ.1) THEN
                        JJ = IPER(IJ + ICOMP)
                        VDIR(JJ) = VALUES(ICOMP)
                        LDIR(JJ) = 1
                     END IF
                  END DO
               END DO
            END IF
         END IF
C
         IF (BCTYPE(1:7).EQ.' LINEAR') THEN
            WRITE (6,*) ' READING LINEAR'
            WRITE (6,*) '                 '
            kk = 1
            CALL REINKY (1,' FIRST NODE:',NLOC(1))
            CALL REINKY (1,' LAST NODE:',NLOC(2))
            CALL REINKY (1,' FIELD NUMBER:',IFIELD)
            CALL REINKY (1,' COMPONENTS:',NCOMP)
            CALL REAKEY (1,' ISWBC(1:NCOMP), 1 IF B.C. IS SPECIFIED:')
            READ(1,*) (ISWBC(I),I=1,NCOMP)
            IF (NCOMP.NE.KCOMP(IFIELD)) THEN
               WRITE (6,*) ' INCOMPATIBLE KCOMP(',IFIELD,')= ',
     *                     NCOMP,KCOMP(IFIELD)
               STOP 'BOUNDARY CONDITIONS'
            END IF
            CALL REAKEY (1,' FIRST VALUES (LIST BELOW):')
            DO I=1,NCOMP
               READ (1,*) VALUES(I)
            END DO
            CALL REAKEY (1,' LAST VALUES (LIST BELOW):')
            DO I=1,NCOMP
               READ (1,*) VAL2(I)
            END DO
            IF (NLOC(1).EQ.NLOC(2)) THEN
               WRITE (6,*) ' LINEAR BC WITH ONLY ONE NODE'
               STOP 'BOUNDARY CONDITIONS'
            END IF
            DO I=1,2
               DO IDIM=1,NDIM
                  INDX = INDCOO(KFTYP(IFIELD))+(IDIM-1)*
     *                   NODTOT(KFTYP(IFIELD))
                  COOLOC(I,IDIM) = COOR(INDX+NLOC(I))
               END DO
            END DO
            DISTOT = 0
            DO IDIM=1,NDIM
               DISTOT = DISTOT + (COOLOC(2,IDIM)-COOLOC(1,IDIM))**2
            END DO
            DISTOT = SQRT(DISTOT)
            IN1 = 0
            IN2 = 0
            IMESH = KFTYP(IFIELD)
            IND   = INDLIS(IMESH)
            DO I=1+IND,NNODL(IMESH)+IND
               IF (NLOC(1).EQ.IAUX(I)) IN1=I
               IF (NLOC(2).EQ.IAUX(I)) IN2=I
            END DO
            IF (IN1.EQ.0.OR.IN2.EQ.0) THEN
               WRITE (6,*) 'FIRST OR LAST NODE IN BC NOT IN THE LIST'
               STOP 'BOUNDARY CONDITIONS'
            END IF
            IF (IN1.LE.IN2) THEN
               DO I=IN1,IN2
                  NO = IAUX(I)
                  DO IDIM=1,NDIM
                     INDX = INDCOO(KFTYP(IFIELD))+(IDIM-1)*
     *                   NODTOT(KFTYP(IFIELD))
                     COOLOC(3,IDIM) = COOR(INDX+NO)
                  END DO
                  DISTAN = 0
                  DO IDIM=1,NDIM
                     DISTAN = DISTAN + 
     *                        (COOLOC(3,IDIM)-COOLOC(1,IDIM))**2
                  END DO
                  DISTAN = SQRT(DISTAN)
                  IF (DISTOT.EQ.0) THEN
                     WRITE (6,*) 'DISTOT=0, LINEAR'
                     WRITE (6,*) 'IFIELD= ',IFIELD
                     WRITE (6,*) 'IMESH = ',IMESH
                     WRITE (6,*) 'IND   = ',IND
                     WRITE (6,*) 'NLOC =',NLOC(1),NLOC(2)
                     WRITE (6,*) 'COOLOC ='
                     WRITE (6,*) COOLOC(1,1),COOLOC(1,2)
                     WRITE (6,*) COOLOC(2,1),COOLOC(2,2)
                     WRITE (6,*) COOLOC(3,1),COOLOC(3,2)
                     STOP
                  END IF
                  SS = DISTAN/DISTOT
                  IJ = INDXXX(IFIELD) + NCOMP*(IAUX(I)-1) 
                  DO ICOMP = 1,NCOMP
                     IF (ISWBC(ICOMP).EQ.1) THEN
                        JJ = IPER(IJ + ICOMP)
                        VDIR(JJ) = (1-SS)*VALUES(ICOMP) + SS*VAL2(ICOMP)
                        LDIR(JJ) = 1
                     END IF
                  END DO
               END DO
            ELSE
               DO I=IND+1,IN2
                  NO = IAUX(I)
                  DO IDIM=1,NDIM
                     INDX = INDCOO(KFTYP(IFIELD))+(IDIM-1)*
     *                   NODTOT(KFTYP(IFIELD))
                     COOLOC(3,IDIM) = COOR(INDX+NO)
                  END DO
                  DISTAN = 0
                  DO IDIM=1,NDIM
                     DISTAN = DISTAN + 
     *                        (COOLOC(3,IDIM)-COOLOC(1,IDIM))**2
                  END DO
                  DISTAN = SQRT(DISTAN)
                  IF (DISTOT.EQ.0) THEN
                     WRITE (6,*) 'DISTOT=0, LINEAR'
                     WRITE (6,*) 'IFIELD= ',IFIELD
                     WRITE (6,*) 'IMESH = ',IMESH
                     WRITE (6,*) 'IND   = ',IND
                     WRITE (6,*) 'NLOC =',NLOC(1),NLOC(2)
                     WRITE (6,*) 'COOLOC ='
                     WRITE (6,*) COOLOC(1,1),COOLOC(1,2)
                     WRITE (6,*) COOLOC(2,1),COOLOC(2,2)
                     WRITE (6,*) COOLOC(3,1),COOLOC(3,2)
                     STOP
                  END IF

                  SS = DISTAN/DISTOT
                  IJ = INDXXX(IFIELD) + NCOMP*(IAUX(I)-1) 
                  DO ICOMP = 1,NCOMP
                     IF (ISWBC(ICOMP).EQ.1) THEN
                        JJ = IPER(IJ + ICOMP)
                        VDIR(JJ) = (1-SS)*VALUES(ICOMP) + SS*VAL2(ICOMP)
                        LDIR(JJ) = 1
                     END IF
                  END DO
               END DO
               DO I=IN1,IND+NNODL(IMESH)
                  NO = IAUX(I)
                  DO IDIM=1,NDIM
                     INDX = INDCOO(KFTYP(IFIELD))+(IDIM-1)*
     *                   NODTOT(KFTYP(IFIELD))
                     COOLOC(3,IDIM) = COOR(INDX+NO)
                  END DO
                  DISTAN = 0
                  DO IDIM=1,NDIM
                     DISTAN = DISTAN + 
     *                        (COOLOC(3,IDIM)-COOLOC(1,IDIM))**2
                  END DO
                  DISTAN = SQRT(DISTAN)
                  IF (DISTOT.EQ.0) THEN
                     WRITE (6,*) 'DISTOT=0, LINEAR'
                     WRITE (6,*) 'IFIELD= ',IFIELD
                     WRITE (6,*) 'IMESH = ',IMESH
                     WRITE (6,*) 'IND   = ',IND
                     WRITE (6,*) 'NLOC =',NLOC(1),NLOC(2)
                     WRITE (6,*) 'COOLOC ='
                     WRITE (6,*) COOLOC(1,1),COOLOC(1,2)
                     WRITE (6,*) COOLOC(2,1),COOLOC(2,2)
                     WRITE (6,*) COOLOC(3,1),COOLOC(3,2)
                     STOP
                  END IF

                  SS = DISTAN/DISTOT
                  IJ = INDXXX(IFIELD) + NCOMP*(IAUX(I)-1) 
                  DO ICOMP = 1,NCOMP
                     IF (ISWBC(ICOMP).EQ.1) THEN
                        JJ = IPER(IJ + ICOMP)
                        VDIR(JJ) = (1-SS)*VALUES(ICOMP) + SS*VAL2(ICOMP)
                        LDIR(JJ) = 1
                     END IF
                  END DO
               END DO
            END IF
         END IF
         IF (BCTYPE(1:10).EQ.' QUADRATIC') THEN
            WRITE (6,*) ' READING QUADRATIC'
            WRITE (6,*) '                 '
            kk = 1
            CALL REINKY (1,' FIRST NODE:',NLOC(1))
            CALL REINKY (1,' LAST NODE:',NLOC(2))
            CALL REINKY (1,' FIELD NUMBER:',IFIELD)
            CALL REINKY (1,' COMPONENTS:',NCOMP)
            CALL REAKEY (1,' ISWBC(1:NCOMP), 1 IF B.C. IS SPECIFIED:')
            READ(1,*) (ISWBC(I),I=1,NCOMP)
            IF (NCOMP.NE.KCOMP(IFIELD)) THEN
               WRITE (6,*) ' INCOMPATIBLE KCOMP(',IFIELD,')= ',
     *                     NCOMP,KCOMP(IFIELD)
               STOP 'BOUNDARY CONDITIONS'
            END IF
            CALL REAKEY (1,' FIRST VALUES (LIST BELOW):')
            DO I=1,NCOMP
               READ (1,*) VALUES(I)
            END DO
            CALL REAKEY (1,' MID-POINT VALUES (LIST BELOW):')
            DO I=1,NCOMP
               READ (1,*) VAL3(I)
            END DO
            CALL REAKEY (1,' LAST VALUES (LIST BELOW):')
            DO I=1,NCOMP
               READ (1,*) VAL2(I)
            END DO
            IF (NLOC(1).EQ.NLOC(2)) THEN
               WRITE (6,*) ' QUADRATIC BC WITH ONLY ONE NODE'
               STOP 'BOUNDARY CONDITIONS'
            END IF
            DO I=1,2
               DO IDIM=1,NDIM
                  INDX = INDCOO(KFTYP(IFIELD))+(IDIM-1)*
     *                   NODTOT(KFTYP(IFIELD))
                  COOLOC(I,IDIM) = COOR(INDX+NLOC(I))
               END DO
            END DO
            DISTOT = 0
            DO IDIM=1,NDIM
               DISTOT = DISTOT + (COOLOC(2,IDIM)-COOLOC(1,IDIM))**2
            END DO
            DISTOT = SQRT(DISTOT)
            IN1 = 0
            IN2 = 0
            IMESH = KFTYP(IFIELD)
            IND   = INDLIS(IMESH)
            DO I=1+IND,NNODL(IMESH)+IND
               IF (NLOC(1).EQ.IAUX(I)) IN1=I
               IF (NLOC(2).EQ.IAUX(I)) IN2=I
            END DO
            IF (IN1.EQ.0.OR.IN2.EQ.0) THEN
               WRITE (6,*) 'FIRST OR LAST NODE IN BC NOT IN THE LIST'
               STOP 'BOUNDARY CONDITIONS'
            END IF
            IF (IN1.LE.IN2) THEN
               DO I=IN1,IN2
                  NO = IAUX(I)
                  DO IDIM=1,NDIM
                     INDX = INDCOO(KFTYP(IFIELD))+(IDIM-1)*
     *                   NODTOT(KFTYP(IFIELD))
                     COOLOC(3,IDIM) = COOR(INDX+NO)
                  END DO
                  DISTAN = 0
                  DO IDIM=1,NDIM
                     DISTAN = DISTAN + 
     *                        (COOLOC(3,IDIM)-COOLOC(1,IDIM))**2
                  END DO
                  DISTAN = SQRT(DISTAN)
                  IF (DISTOT.EQ.0) THEN
                     WRITE (6,*) 'DISTOT=0, QUADRATIC'
                     WRITE (6,*) 'IFIELD= ',IFIELD
                     WRITE (6,*) 'IMESH = ',IMESH
                     WRITE (6,*) 'IND   = ',IND
                     WRITE (6,*) 'NLOC =',NLOC(1),NLOC(2)
                     WRITE (6,*) 'COOLOC ='
                     WRITE (6,*) COOLOC(1,1),COOLOC(1,2)
                     WRITE (6,*) COOLOC(2,1),COOLOC(2,2)
                     WRITE (6,*) COOLOC(3,1),COOLOC(3,2)
                     STOP
                  END IF

                  S = DISTAN/DISTOT
                  IJ = INDXXX(IFIELD) + NCOMP*(IAUX(I)-1) 
                  DO ICOMP = 1,NCOMP
                     IF (ISWBC(ICOMP).EQ.1) THEN
                        JJ = IPER(IJ + ICOMP)
                        VDIR(JJ) = VALUES(ICOMP)*(2*(S-.5)*(S-1))+
     *                          VAL3(ICOMP)*(-4*S*(S-1)) +
     *                          VAL2(ICOMP)*(2*S*(S-.5))
                        LDIR(JJ) = 1
                     END IF
                  END DO
               END DO
            ELSE
               DO I=IND+1,IN2
                  NO = IAUX(I)
                  DO IDIM=1,NDIM
                     INDX = INDCOO(KFTYP(IFIELD))+(IDIM-1)*
     *                   NODTOT(KFTYP(IFIELD))
                     COOLOC(3,IDIM) = COOR(INDX+NO)
                  END DO
                  DISTAN = 0
                  DO IDIM=1,NDIM
                     DISTAN = DISTAN + 
     *                        (COOLOC(3,IDIM)-COOLOC(1,IDIM))**2
                  END DO
                  DISTAN = SQRT(DISTAN)
                  IF (DISTOT.EQ.0) THEN
                     WRITE (6,*) 'DISTOT=0, QUADRATIC'
                     WRITE (6,*) 'IFIELD= ',IFIELD
                     WRITE (6,*) 'IMESH = ',IMESH
                     WRITE (6,*) 'IND   = ',IND
                     WRITE (6,*) 'NLOC =',NLOC(1),NLOC(2)
                     WRITE (6,*) 'COOLOC ='
                     WRITE (6,*) COOLOC(1,1),COOLOC(1,2)
                     WRITE (6,*) COOLOC(2,1),COOLOC(2,2)
                     WRITE (6,*) COOLOC(3,1),COOLOC(3,2)
                     STOP
                  END IF

                  S = DISTAN/DISTOT
                  IJ = INDXXX(IFIELD) + NCOMP*(IAUX(I)-1) 
                  DO ICOMP = 1,NCOMP
                     IF (ISWBC(ICOMP).EQ.1) THEN
                        JJ = IPER(IJ + ICOMP)
                        VDIR(JJ) = VALUES(ICOMP)*(2*(S-.5)*(S-1))+
     *                          VAL3(ICOMP)*(-4*S*(S-1)) +
     *                          VAL2(ICOMP)*(2*S*(S-.5))
                        LDIR(JJ) = 1
                     END IF
                  END DO
               END DO
               DO I=IN1,IND+NNODL(IMESH)
                  NO = IAUX(I)
                  DO IDIM=1,NDIM
                     INDX = INDCOO(KFTYP(IFIELD))+(IDIM-1)*
     *                   NODTOT(KFTYP(IFIELD))
                     COOLOC(3,IDIM) = COOR(INDX+NO)
                  END DO
                  DISTAN = 0
                  DO IDIM=1,NDIM
                     DISTAN = DISTAN + 
     *                        (COOLOC(3,IDIM)-COOLOC(1,IDIM))**2
                  END DO
                  DISTAN = SQRT(DISTAN)
                  IF (DISTOT.EQ.0) THEN
                     WRITE (6,*) 'DISTOT=0, QUADRATIC'
                     WRITE (6,*) 'IFIELD= ',IFIELD
                     WRITE (6,*) 'IMESH = ',IMESH
                     WRITE (6,*) 'IND   = ',IND
                     WRITE (6,*) 'NLOC =',NLOC(1),NLOC(2)
                     WRITE (6,*) 'COOLOC ='
                     WRITE (6,*) COOLOC(1,1),COOLOC(1,2)
                     WRITE (6,*) COOLOC(2,1),COOLOC(2,2)
                     WRITE (6,*) COOLOC(3,1),COOLOC(3,2)
                     STOP
                  END IF
                  S = DISTAN/DISTOT
                  IJ = INDXXX(IFIELD) + NCOMP*(IAUX(I)-1) 
                  DO ICOMP = 1,NCOMP
                     IF (ISWBC(ICOMP).EQ.1) THEN
                        JJ = IPER(IJ + ICOMP)
                        VDIR(JJ) = VALUES(ICOMP)*(2*(S-.5)*(S-1))+
     *                          VAL3(ICOMP)*(-4*S*(S-1)) +
     *                          VAL2(ICOMP)*(2*S*(S-.5))
                        LDIR(JJ) = 1
                     END IF
                  END DO
               END DO
            END IF
         END IF

         IF (BCTYPE(1:10).EQ.' WALL') THEN
            WRITE (6,*) ' READING WALL'
            WRITE (6,*) '                 '
            kk = 1
            CALL REINKY (1,' WALL NUMBER:',IWALL)
            IWALL = IWALL + 10
            CALL REINKY (1,' FIRST NODE:',NLOC(1))
            CALL REINKY (1,' LAST NODE:',NLOC(2))
            CALL REINKY (1,' FIELD NUMBER:',IFIELD)
            CALL REINKY (1,' COMPONENTS:',NCOMP)
            CALL REAKEY (1,' ISWBC(1:NCOMP), 1 IF B.C. IS SPECIFIED:')
            READ(1,*) (ISWBC(I),I=1,NCOMP)
            IF (NCOMP.NE.KCOMP(IFIELD)) THEN
               WRITE (6,*) ' INCOMPATIBLE KCOMP(',IFIELD,')= ',
     *                     NCOMP,KCOMP(IFIELD)
               STOP 'BOUNDARY CONDITIONS'
            END IF
            CALL REAKEY (1,' FIRST VALUES (LIST BELOW):')
            DO I=1,NCOMP
               READ (1,*) VALUES(I)
            END DO
            CALL REAKEY (1,' MID-POINT VALUES (LIST BELOW):')
            DO I=1,NCOMP
               READ (1,*) VAL3(I)
            END DO
            CALL REAKEY (1,' LAST VALUES (LIST BELOW):')
            DO I=1,NCOMP
               READ (1,*) VAL2(I)
            END DO
            IF (NLOC(1).EQ.NLOC(2)) THEN
               WRITE (6,*) ' WALL BC WITH ONLY ONE NODE'
               STOP 'BOUNDARY CONDITIONS'
            END IF
            DO I=1,2
               DO IDIM=1,NDIM
                  INDX = INDCOO(KFTYP(IFIELD))+(IDIM-1)*
     *                   NODTOT(KFTYP(IFIELD))
                  COOLOC(I,IDIM) = COOR(INDX+NLOC(I))
               END DO
            END DO
            DISTOT = 0
            DO IDIM=1,NDIM
               DISTOT = DISTOT + (COOLOC(2,IDIM)-COOLOC(1,IDIM))**2
            END DO
            DISTOT = SQRT(DISTOT)
            IN1 = 0
            IN2 = 0
            IMESH = KFTYP(IFIELD)
            IND   = INDLIS(IMESH)
            DO I=1+IND,NNODL(IMESH)+IND
               IF (NLOC(1).EQ.IAUX(I)) IN1=I
               IF (NLOC(2).EQ.IAUX(I)) IN2=I
            END DO
            IF (IN1.EQ.0.OR.IN2.EQ.0) THEN
               WRITE (6,*) 'FIRST OR LAST NODE IN BC NOT IN THE LIST'
               STOP 'BOUNDARY CONDITIONS'
            END IF
            IF (IN1.LE.IN2) THEN
               DO I=IN1,IN2
                  NO = IAUX(I)
                  DO IDIM=1,NDIM
                     INDX = INDCOO(KFTYP(IFIELD))+(IDIM-1)*
     *                   NODTOT(KFTYP(IFIELD))
                     COOLOC(3,IDIM) = COOR(INDX+NO)
                  END DO
                  DISTAN = 0
                  DO IDIM=1,NDIM
                     DISTAN = DISTAN + 
     *                        (COOLOC(3,IDIM)-COOLOC(1,IDIM))**2
                  END DO
                  DISTAN = SQRT(DISTAN)
                  IF (DISTOT.EQ.0) THEN
                     WRITE (6,*) 'DISTOT=0, QUADRATIC'
                     WRITE (6,*) 'IFIELD= ',IFIELD
                     WRITE (6,*) 'IMESH = ',IMESH
                     WRITE (6,*) 'IND   = ',IND
                     WRITE (6,*) 'NLOC =',NLOC(1),NLOC(2)
                     WRITE (6,*) 'COOLOC ='
                     WRITE (6,*) COOLOC(1,1),COOLOC(1,2)
                     WRITE (6,*) COOLOC(2,1),COOLOC(2,2)
                     WRITE (6,*) COOLOC(3,1),COOLOC(3,2)
                     STOP
                  END IF

                  S = DISTAN/DISTOT
                  IJ = INDXXX(IFIELD) + NCOMP*(IAUX(I)-1) 
                  DO ICOMP = 1,NCOMP
                     IF (ISWBC(ICOMP).EQ.1) THEN
                        JJ = IPER(IJ + ICOMP)
                        VDIR(JJ) = VALUES(ICOMP)*(2*(S-.5)*(S-1))+
     *                          VAL3(ICOMP)*(-4*S*(S-1)) +
     *                          VAL2(ICOMP)*(2*S*(S-.5))
                        LDIR(JJ) = IWALL
                     END IF
                  END DO
               END DO
            ELSE
               DO I=IND+1,IN2
                  NO = IAUX(I)
                  DO IDIM=1,NDIM
                     INDX = INDCOO(KFTYP(IFIELD))+(IDIM-1)*
     *                   NODTOT(KFTYP(IFIELD))
                     COOLOC(3,IDIM) = COOR(INDX+NO)
                  END DO
                  DISTAN = 0
                  DO IDIM=1,NDIM
                     DISTAN = DISTAN + 
     *                        (COOLOC(3,IDIM)-COOLOC(1,IDIM))**2
                  END DO
                  DISTAN = SQRT(DISTAN)
                  IF (DISTOT.EQ.0) THEN
                     WRITE (6,*) 'DISTOT=0, QUADRATIC'
                     WRITE (6,*) 'IFIELD= ',IFIELD
                     WRITE (6,*) 'IMESH = ',IMESH
                     WRITE (6,*) 'IND   = ',IND
                     WRITE (6,*) 'NLOC =',NLOC(1),NLOC(2)
                     WRITE (6,*) 'COOLOC ='
                     WRITE (6,*) COOLOC(1,1),COOLOC(1,2)
                     WRITE (6,*) COOLOC(2,1),COOLOC(2,2)
                     WRITE (6,*) COOLOC(3,1),COOLOC(3,2)
                     STOP
                  END IF

                  S = DISTAN/DISTOT
                  IJ = INDXXX(IFIELD) + NCOMP*(IAUX(I)-1) 
                  DO ICOMP = 1,NCOMP
                     IF (ISWBC(ICOMP).EQ.1) THEN
                        JJ = IPER(IJ + ICOMP)
                        VDIR(JJ) = VALUES(ICOMP)*(2*(S-.5)*(S-1))+
     *                          VAL3(ICOMP)*(-4*S*(S-1)) +
     *                          VAL2(ICOMP)*(2*S*(S-.5))
                        LDIR(JJ) = IWALL
                     END IF
                  END DO
               END DO
               DO I=IN1,IND+NNODL(IMESH)
                  NO = IAUX(I)
                  DO IDIM=1,NDIM
                     INDX = INDCOO(KFTYP(IFIELD))+(IDIM-1)*
     *                   NODTOT(KFTYP(IFIELD))
                     COOLOC(3,IDIM) = COOR(INDX+NO)
                  END DO
                  DISTAN = 0
                  DO IDIM=1,NDIM
                     DISTAN = DISTAN + 
     *                        (COOLOC(3,IDIM)-COOLOC(1,IDIM))**2
                  END DO
                  DISTAN = SQRT(DISTAN)
                  IF (DISTOT.EQ.0) THEN
                     WRITE (6,*) 'DISTOT=0, QUADRATIC'
                     WRITE (6,*) 'IFIELD= ',IFIELD
                     WRITE (6,*) 'IMESH = ',IMESH
                     WRITE (6,*) 'IND   = ',IND
                     WRITE (6,*) 'NLOC =',NLOC(1),NLOC(2)
                     WRITE (6,*) 'COOLOC ='
                     WRITE (6,*) COOLOC(1,1),COOLOC(1,2)
                     WRITE (6,*) COOLOC(2,1),COOLOC(2,2)
                     WRITE (6,*) COOLOC(3,1),COOLOC(3,2)
                     STOP
                  END IF
                  S = DISTAN/DISTOT
                  IJ = INDXXX(IFIELD) + NCOMP*(IAUX(I)-1) 
                  DO ICOMP = 1,NCOMP
                     IF (ISWBC(ICOMP).EQ.1) THEN
                        JJ = IPER(IJ + ICOMP)
                        VDIR(JJ) = VALUES(ICOMP)*(2*(S-.5)*(S-1))+
     *                          VAL3(ICOMP)*(-4*S*(S-1)) +
     *                          VAL2(ICOMP)*(2*S*(S-.5))
                        LDIR(JJ) = IWALL
                     END IF
                  END DO
               END DO
            END IF
         END IF
C
         IF (BCTYPE(1:11).EQ.' BURBUJAXEL') THEN
            WRITE (6,*) ' READING BURBUJAXEL'
            WRITE (6,*) '                 '
            kk = 1
            CALL REINKY (1,' FIRST NODE:',N1)
            CALL REINKY (1,' LAST NODE:',N2)
            CALL REINKY (1,' FIELD NUMBER:',IFIELD)
            CALL REINKY (1,' COMPONENTS:',NCOMP)
            CALL REAKEY (1,' ISWBC(1:NCOMP), 1 IF B.C. IS SPECIFIED:')
            READ(1,*) (ISWBC(I),I=1,NCOMP)
            IF (NCOMP.NE.KCOMP(IFIELD)) THEN
               WRITE (6,*) ' INCOMPATIBLE KCOMP(',IFIELD,')= ',
     *                     NCOMP,KCOMP(IFIELD)
               STOP 'BOUNDARY CONDITIONS'
            END IF
            CALL REAKEY (1,' VALUES (LIST BELOW):')
            DO I=1,NCOMP
               READ (1,*) VALUES(I)
            END DO
            CALL REAKEY (1,' NORMALS:')
            IN1 = 0
            IN2 = 0
            IMESH = KFTYP(IFIELD)
            IND   = INDLIS(IMESH)
            DO I=1+IND,NNODL(IMESH)+IND
               IF (N1.EQ.IAUX(I)) IN1=I
               IF (N2.EQ.IAUX(I)) IN2=I
            END DO
            IF (IN1.EQ.0.OR.IN2.EQ.0) THEN
               WRITE (6,*) 'FIRST OR LAST NODE IN BC NOT IN THE LIST'
               STOP 'BOUNDARY CONDITIONS'
            END IF
            XXVAL = VALUES(1)
            YYVAL = VALUES(2)
            IF (IN1.LE.IN2) THEN
               DO I=IN1,IN2
                  IJ = INDXXX(IFIELD) + NCOMP*(IAUX(I)-1) 
                  READ (1,*) XXNOR, YYNOR
                  VALUES(1) = XXVAL * XXNOR - YYVAL * YYNOR
                  VALUES(2) = XXVAL * YYNOR + YYVAL * XXNOR
                  DO ICOMP = 1,NCOMP
                     IF (ISWBC(ICOMP).EQ.1) THEN
                        JJ = IPER(IJ + ICOMP)
                        VDIR(JJ) = VALUES(ICOMP)
                        LDIR(JJ) = 1
                     END IF
                  END DO
               END DO
            ELSE
               DO I=IN1,IND+NNODL(IMESH)
                  IJ = INDXXX(IFIELD) + NCOMP*(IAUX(I)-1) 
                  READ (1,*) XXNOR, YYNOR
                  VALUES(1) = XXVAL * XXNOR - YYVAL * YYNOR
                  VALUES(2) = XXVAL * YYNOR + YYVAL * XXNOR
                  DO ICOMP = 1,NCOMP
                     IF (ISWBC(ICOMP).EQ.1) THEN
                        JJ = IPER(IJ + ICOMP)
                        VDIR(JJ) = VALUES(ICOMP)
                        LDIR(JJ) = 1
                     END IF
                  END DO
               END DO
               DO I=IND+1,IN2
                  IJ = INDXXX(IFIELD) + NCOMP*(IAUX(I)-1) 
                  READ (1,*) XXNOR, YYNOR
                  VALUES(1) = XXVAL * XXNOR - YYVAL * YYNOR
                  VALUES(2) = XXVAL * YYNOR + YYVAL * XXNOR
                  DO ICOMP = 1,NCOMP
                     IF (ISWBC(ICOMP).EQ.1) THEN
                        JJ = IPER(IJ + ICOMP)
                        VDIR(JJ) = VALUES(ICOMP)
                        LDIR(JJ) = 1
                     END IF
                  END DO
               END DO
            END IF
         END IF
C
         IF (BCTYPE(1:10).EQ.' PIPROBLEM') THEN
            WRITE (6,*) ' READING PIPROBLEM'
            WRITE (6,*) '                 '
            kk = 1
            CALL REINKY (1,' FIRST NODE:',NLOC(1))
            CALL REINKY (1,' LAST NODE:',NLOC(2))
            CALL REINKY (1,' FIELD NUMBER:',IFIELD)
            CALL REINKY (1,' COMPONENTS:',NCOMP)
            CALL REAKEY (1,' ISWBC(1:NCOMP), 1 IF B.C. IS SPECIFIED:')
            READ(1,*) (ISWBC(I),I=1,NCOMP)
            IF (NCOMP.NE.KCOMP(IFIELD)) THEN
               WRITE (6,*) ' INCOMPATIBLE KCOMP(',IFIELD,')= ',
     *                     NCOMP,KCOMP(IFIELD)
               STOP 'BOUNDARY CONDITIONS'
            END IF
            IF (NLOC(1).EQ.NLOC(2)) THEN
               WRITE (6,*) ' PIPROBLEM BC WITH ONLY ONE NODE'
               STOP 'BOUNDARY CONDITIONS'
            END IF
            IN1 = 0
            IN2 = 0
            IMESH = KFTYP(IFIELD)
            IND   = INDLIS(IMESH)
            DO I=1+IND,NNODL(IMESH)+IND
               IF (NLOC(1).EQ.IAUX(I)) IN1=I
               IF (NLOC(2).EQ.IAUX(I)) IN2=I
            END DO
            IF (IN1.EQ.0.OR.IN2.EQ.0) THEN
               WRITE (6,*) 'FIRST OR LAST NODE IN BC NOT IN THE LIST'
               STOP 'BOUNDARY CONDITIONS'
            END IF
            IF (IN1.LE.IN2) THEN
               DO I=IN1,IN2
                  NO = IAUX(I)
                  DO IDIM=1,NDIM
                     INDX = INDCOO(KFTYP(IFIELD))+(IDIM-1)*
     *                   NODTOT(KFTYP(IFIELD))
                     COOLOC(3,IDIM) = COOR(INDX+NO)
                  END DO
                  XX = COOLOC(3,1)
                  YY = COOLOC(3,2)
c
c      Lado curvo, calcula argumento
c
                  if (abs(YY) .lt. 0.00001) then
                     if (XX .gt. 0.0) then
                        ANG = 0
                     else
                        ANG = PPII
                     end if
                  else if (XX .eq. 0.0) then
                     if (YY .gt. 0.0) then
                        ANG = PPII / 2
                     else
                        ANG = 3 * PPII / 2
                     endif
                  else
                     ANG = atan(YY/XX)
                     if (XX .gt. 0.0) then
                        if (YY .ge. 0.0) then              ! 1er cuadrante
                           continue
                        else                               ! 4to cuadrante
                           ANG = ANG + PPII * 2
                        endif
                     else if (XX .lt. 0.0) then
                        if (YY .ge. 0.0) then              ! 2do cuadrante
                           ANG = ANG + PPII
                        else                               ! 3er cuadrante
                           ANG = ANG + PPII
                        end if
                     end if
                  end if
C
                  aka = 8.0   ! Dominio: 2Pi
                  CDIR = sin(2*ANG/aka)
                  IF (I.EQ.IN2) CDIR = 1.0D+00
                  IJ = INDXXX(IFIELD) + NCOMP*(IAUX(I)-1) 
                  DO ICOMP = 1,NCOMP
                     IF (ISWBC(ICOMP).EQ.1) THEN
                        JJ = IPER(IJ + ICOMP)
                        VDIR(JJ) = CDIR
                        LDIR(JJ) = 1
                     END IF
                  END DO
               END DO
            ELSE
               DO I=IND+1,IN2
                  NO = IAUX(I)
                  DO IDIM=1,NDIM
                     INDX = INDCOO(KFTYP(IFIELD))+(IDIM-1)*
     *                   NODTOT(KFTYP(IFIELD))
                     COOLOC(3,IDIM) = COOR(INDX+NO)
                  END DO
                  XX = COOLOC(3,1)
                  YY = COOLOC(3,2)
c
c      Lado curvo, calcula argumento
c
                  if (abs(YY) .lt. 0.00001) then
                     if (XX .gt. 0.0) then
                        ANG = 0
                     else
                        ANG = PPII
                     end if
                  else if (XX .eq. 0.0) then
                     if (YY .gt. 0.0) then
                        ANG = PPII / 2
                     else
                        ANG = 3 * PPII / 2
                     endif
                  else
                     ANG = atan(YY/XX)
                     if (XX .gt. 0.0) then
                        if (YY .ge. 0.0) then              ! 1er cuadrante
                           continue
                        else                               ! 4to cuadrante
                           ANG = ANG + PPII * 2
                        endif
                     else if (XX .lt. 0.0) then
                        if (YY .ge. 0.0) then              ! 2do cuadrante
                           ANG = ANG + PPII
                        else                               ! 3er cuadrante
                           ANG = ANG + PPII
                        end if
                     end if
                  end if
C
                  aka = 8.0   ! Dominio: 2Pi
                  CDIR = sin(2*ANG/aka)
                  IF (I.EQ.IN2) CDIR = 1.0D+00
                  IJ = INDXXX(IFIELD) + NCOMP*(IAUX(I)-1) 
                  DO ICOMP = 1,NCOMP
                     IF (ISWBC(ICOMP).EQ.1) THEN
                        JJ = IPER(IJ + ICOMP)
                        VDIR(JJ) = CDIR
                        LDIR(JJ) = 1
                     END IF
                  END DO
               END DO
               DO I=IN1,IND+NNODL(IMESH)
                  NO = IAUX(I)
                  DO IDIM=1,NDIM
                     INDX = INDCOO(KFTYP(IFIELD))+(IDIM-1)*
     *                   NODTOT(KFTYP(IFIELD))
                     COOLOC(3,IDIM) = COOR(INDX+NO)
                  END DO
                  XX = COOLOC(3,1)
                  YY = COOLOC(3,2)
c
c      Lado curvo, calcula argumento
c
                  if (abs(YY) .lt. 0.00001) then
                     if (XX .gt. 0.0) then
                        ANG = 0
                     else
                        ANG = PPII
                     end if
                  else if (XX .eq. 0.0) then
                     if (YY .gt. 0.0) then
                        ANG = PPII / 2
                     else
                        ANG = 3 * PPII / 2
                     endif
                  else
                     ANG = atan(YY/XX)
                     if (XX .gt. 0.0) then
                        if (YY .ge. 0.0) then              ! 1er cuadrante
                           continue
                        else                               ! 4to cuadrante
                           ANG = ANG + PPII * 2
                        endif
                     else if (XX .lt. 0.0) then
                        if (YY .ge. 0.0) then              ! 2do cuadrante
                           ANG = ANG + PPII
                        else                               ! 3er cuadrante
                           ANG = ANG + PPII
                        end if
                     end if
                  end if
C
                  aka = 8.0   ! Dominio: 2Pi
                  CDIR = sin(2*ANG/aka)
                  IF (I.EQ.IN2) CDIR = 1.0D+00
                  IJ = INDXXX(IFIELD) + NCOMP*(IAUX(I)-1) 
                  DO ICOMP = 1,NCOMP
                     IF (ISWBC(ICOMP).EQ.1) THEN
                        JJ = IPER(IJ + ICOMP)
                        VDIR(JJ) = CDIR
                        LDIR(JJ) = 1
                     END IF
                  END DO
               END DO
            END IF
         END IF
C
C     GROUP DEFINITIONS
C
         IF (BCTYPE(1:17) .EQ. ' GROUP DEFINITION') THEN
            WRITE (6,*) ' READING GROUP DEFINITION'
            kk = 1
            CALL REINKY (1,' MESH:', IMESH)
            WRITE (6,*) 'MESH: ', IMESH
            IF (IMESH .LT. 1 .OR. IMESH .GT. NMESH)
     *           STOP 'BOUNDARY CONDITIONS: INVALID MESH NUMBER'
            IF (ISWGDEF(IMESH) .NE. 0) THEN
               WRITE (6,*) ' GROUP DEFINITION FOR MESH:',
     *              IMESH, 'ALREADY SET'
               STOP 'BOUNDARY CONDITIONS'
            END IF
            ISWGDEF(IMESH) = 1
            CALL REINKY (1,' LOCATION (0:HERE, 1:FILE):', IDUM)
            CALL REAKEY(1,' FILE FOR/WITH SURFACE MESH:')
            READ (1,'(A80)') FILESUR(IMESH)
C
            IF (IDUM .EQ. 0) THEN
C     Group definitions here, write file
               IF (NDIM .NE. 2) STOP 'NDIM .NE. 2'
               OPEN(8, FILE=FILESUR(IMESH), STATUS='UNKNOWN')
C     Write coordinates
               WRITE (8, '(''*COORDINATES'')')
               WRITE (8, *) NODTOT(IMESH)
               DO I = 1, NODTOT(IMESH)
                  INDX = INDCOO(IMESH)
                  WRITE (8, *) I,
     *                 COOR(INDX+I), COOR(INDX+NODTOT(IMESH)+I)
               END DO
C     Write incidences while reading
               WRITE (8, '(''*INCIDENCES'',/,''<NONE>'')')
               CALL REINKY (1,' NUMBER OF GROUPS:', NGRPS)
               IF (NGRPS .GT. MAXNGRPS) STOP 'INCREASE MAXNGRPS'
               DO IGRPS = 1, NGRPS
                  CALL REINKY (1, ' FIRST NODE:', N1)
                  CALL REINKY (1, ' LAST NODE:',  N2)
		  NABS2 = N2
		  IF (NABS2 .LT. 0) NABS2 = -N2
                  IN1 = 0
                  IN2 = 0
                  IND   = INDLIS(IMESH)
                  DO I=1+IND,NNODL(IMESH)+IND
                     IF (N1.EQ.IAUX(I)) IN1=I
                     IF (NABS2.EQ.IAUX(I)) IN2=I
                  END DO
                  IF (IN1.EQ.0.OR.IN2.EQ.0) THEN
                     WRITE (6, *) 'FIRST OR LAST NODE NOT IN THE LIST'
                     STOP 'BOUNDARY CONDITIONS'
                  END IF
                  IF (IN1.LE.IN2) THEN
                     NELGRP(IGRPS) = IN2 - IN1
                     DO I = IN1, IN2 - 1
                        WRITE (8, *) IAUX(I), IAUX(I+1)
                     END DO
                  ELSE
                     IPREV = IN1
                     NELGRP(IGRPS) = NNODL(IMESH) + IN2 - IN1
                     DO I = IN1 + 1, IND + NNODL(IMESH)
                        WRITE (8, *) IAUX(IPREV), IAUX(I)
                        IPREV = I
                     END DO
                     DO I=IND+1,IN2
                        WRITE (8, *) IAUX(IPREV), IAUX(I)
                        IPREV = I
                     END DO
                  END IF
		  IF (NABS2 .NE. N2) THEN
		     NELGRP(IGRPS) = NELGRP(IGRPS)+1
		     WRITE (8, *) NABS2, N1
		  END IF
               END DO        ! END LOOP GROUPS
               WRITE (8, '(''*ELEMENT_GROUPS'')')
               WRITE (8, *) NGRPS
               DO IGRP = 1, NGRPS
                  WRITE (8, *) IGRP, NELGRP(IGRP), ' Seg2'
               END DO
               WRITE (8, '(''*END'')')
	       CLOSE (8)
            END IF
         END IF
C
C     OBLIQUE NODES
C
         IF (BCTYPE(1:14).EQ.' OBLIQUE NODES') THEN
            WRITE (6,*) ' READING OBLIQUE NODES'
            kk = 1
            CALL REINKY (1,' MESH:', IMESH)
            IF (ISWGDEF(IMESH) .EQ. 0) THEN
               WRITE (6,*) ' GROUP DEFINITION FOR MESH:',
     *              IMESH, 'NOT SET'
               STOP 'BOUNDARY CONDITIONS'
            END IF
            CALL REINKY (1,' NUMBER OF GROUPS:', NGRP)
            CALL REAKEY (1,' GROUPS:')
            READ (1, *) (NELGRP(I), I = 1, NGRP)
C     COMPUTE (OR READ) NORMALS AND LOAD ROT VECTOR
            LIA = INDLIS(NMESH+1) + 1
            LIB = LIA + NODTOT(IMESH)
            CALL COMPNORMALS (NDIM, FILESUR(IMESH), COOR, INCID,
     *                        NGRP, NELGRP, NELGRP(NGRP+1), 
     *                        IOBL, ROT, IAUX(LIA), IAUX(LIB))
         END IF
C
         IF (BCTYPE(1:18).EQ.' OBLIQUE CONDITION') THEN
            WRITE (6,*) ' READING OBLIQUE CONDITION'
            WRITE (6,*) '                 '
            kk = 1
            CALL REINKY (1,' FIRST NODE:',N1)
            CALL REINKY (1,' LAST NODE:',N2)
            CALL REINKY (1,' MESH NUMBER:',IMESH)
            IF (IMESH.NE.1) STOP ' OBLIQUE: ONLY ONE MESH'
            IN1 = 0
            IN2 = 0
            IND   = INDLIS(IMESH)
            DO I=1+IND,NNODL(IMESH)+IND
               IF (N1.EQ.IAUX(I)) IN1=I
               IF (N2.EQ.IAUX(I)) IN2=I
            END DO
            IF (IN1.EQ.0.OR.IN2.EQ.0) THEN
               WRITE (6,*) 'FIRST OR LAST NODE IN BC NOT IN THE LIST'
               STOP 'BOUNDARY CONDITIONS'
            END IF
            IJK = 1
            NNOB = 0
            DO I=1,NODTOT(IMESH)
               LNOB(I) = 0
            END DO
            IF (IN1.LE.IN2) THEN
               DO I=IN1,IN2
                  LNOB(IAUX(I))=IJK
                  IJK = IJK + NDIM*NDIM
                  NNOB = NNOB + 1
               END DO
            ELSE
               DO I=IND+1,IN2
                  LNOB(IAUX(I))=IJK
                  IJK = IJK + NDIM*NDIM
                  NNOB = NNOB + 1
               END DO
               DO I=IN1,IND+NNODL(IMESH)
                  LNOB(IAUX(I))=IJK
                  IJK = IJK + NDIM*NDIM
                  NNOB = NNOB + 1
               END DO
            END IF
            LIA = INDLIS(NMESH+1) + 1
            LIB = LIA + NODTOT(IMESH)
            CALL COMPBASE (NDIM, FILESUR(IMESH), COOR, INCID, 
     *                     NNOB,LNOB, IAUX(LIA), IAUX(LIB),
     *                     GBASE)
            OPEN (22,FILE='oblique1.cfg',STATUS='NEW')
            WRITE (22,*) NNOB
            WRITE (22,*) (LNOB(I),I=1,NODTOT(1))
            DO I=1,NNOB
               IJK = (I-1)*NDIM*NDIM +1
               WRITE (22,*) GBASE(IJK),GBASE(IJK+1),GBASE(IJK+2),
     *                      GBASE(IJK+3)
            END DO
            CLOSE (22)
         END IF
C
         IF (BCTYPE(1:4).EQ.' END') THEN
            CLOSE (1)
            RETURN
         END IF
         if (bctype(1:10).eq.'          ') kk=1
         if (kk.eq.0) then
            write (6,*) 'UNRECOGNIZED KEYWORD IN BOUNDARY CONDITIONS'
            write (6,'(a80)') bctype
         end if
C
      END DO
      CLOSE (1)
  200 RETURN
      END
C-----------------------------------------------------------------------
      SUBROUTINE COMPNORMALS (NDIM, FILSUR, COOR, INCID, NGROBL, IGROBL,
     *     NELGRP, IOBL, ROT, IA, IB)
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H, O-Z)
      CHARACTER*80 FILSUR, REC, ELTYPE
      DIMENSION COOR(1), IGROBL(NGROBL), NELGRP(1), IOBL(1), ROT(1)
      DIMENSION INCID(1)
      DIMENSION IA(1), IB(1)
      LUSUR = 8
C
C     OPEN SURFACE MESH
C
      OPEN (LUSUR, FILE=FILSUR, STATUS='OLD')
      CALL SEARSTR (LUSUR, 'COORDINATES')
      READ (LUSUR, *) NOD
      CALL SEARSTR (LUSUR, 'ELEMENT_GROUPS')
      READ (LUSUR,*) NGRPS
      DO IGRP = 1, NGROBL
         IF (IGROBL(IGRP) .LT. 1 .OR. IGROBL(IGRP) .GT. NGRPS)
     *        STOP 'INVALID OBLIQUE NODES GROUP'
      END DO
C
      NEL = 0
      DO I = 1, NGRPS
         CALL REAELTYP (LUSUR, I, NELGRP(I), ELTYPE)
         NEL = NEL + NELGRP(I)
      END DO
C
      IF (ELTYPE(1:4) .EQ. 'Seg2') THEN
         NNE = 2
      ELSE IF (ELTYPE(1:4) .EQ. 'Line') THEN
         NNE = 2
      ELSE IF (ELTYPE(1:4) .EQ. 'Linear_Triangle') THEN
         NNE = 3
      ELSE IF (ELTYPE(1:4) .EQ. 'Tri3') THEN
         NNE = 3
      ELSE
         STOP 'INVALID TYPE OF ELEMENT (Not yet programmed)'
      END IF
C
      CALL SEARSTR (LUSUR, 'INCIDENCE')
      READ (LUSUR, '(A80)') REC
      IF (REC(1:6) .NE. '<NONE>') STOP 'PROBLEMAS...'
      READ (LUSUR, *) (INCID(I), I = 1, NEL*NNE)
C
C     Sets IOBL: 0: no oblicuo
C                >0: 1 grupo
C                <0: mas de un grupo
C
      DO I = 1, NGROBL
         IGRP = IGROBL(I)
         INIEL = 1
         DO J = 1, IGRP - 1
            INIEL = INIEL + NELGRP(J)
         END DO
         DO IEL = INIEL, INIEL + NELGRP(IGRP)
            DO J = 1, NNE
               NODE = INCID((IEL-1)*NNE+J)
               IF (IOBL(NODE) .EQ. 0) THEN
                  IOBL(NODE) = IGRP
               ELSE IF (IOBL(NODE) .GT. 0) THEN
                  IOBL(NODE) = -2
               ELSE
                  IOBL(NODE) = IOBL(NODE) - 1
               END IF
            END DO
         END DO
      END DO
C SEARCH FOR NORMALS INFO
      IF (IFOUNDKY(LUSUR, 'NORMALS') .EQ. 1) THEN
         READ (LUSUR, *) (ROT(I), I = 1, NDIM*(NDIM-1)*NOD)
      ELSE
C NORMALS NOT FOUND, COMPUTE ( 2 D ONLY)
         IF (NDIM .NE. 2) STOP 'ERROR NORMAL COMP: ONLY 2-D)'
         CALL SEARSTR (LUSUR, 'COORDINATES')
         READ (LUSUR, *) NOD
         READ (LUSUR, *) (IDUM, (COOR(NDIM*(I-1)+K), K=1,NDIM),
     *                    I = 1, NOD)
C INITIALIZE ACCUMULATORS  
         do i = 1, ndim * nod
            rot(i) = 0.0
         end do
         do i = 1, nod
            IA(i) = 0
            IB(i) = 0
         end do
C Loop over elements
         do iel = 1, nel
            N1 = INCID(IEL*2-1) 
            N2 = INCID(IEL*2) 
            IA(N1) = N2
            IB(N2) = N1
         end do
C LOOP OVER NODES
         DO N = 1, NOD
            IF (IA(N) .ne. 0) THEN
               NPREV = IA(N)
               NNEXT = IB(N)
               XP = COOR(2*NPREV-1)
               YP = COOR(2*NPREV)
               XX = COOR(2*N-1)
               YY = COOR(2*N)
               XN = COOR(2*NNEXT-1)
               YN = COOR(2*NNEXT)
               PREV_NX = -(YY - YP)
               PREV_NY =   XX - XP
               PROX_NX = -(YN - YY)
               PROX_NY =   XN - XX
               PREV_MOD = SQRT(PREV_NX*PREV_NX+PREV_NY*PREV_NY)
               PROX_MOD = SQRT(PREV_NX*PREV_NX+PREV_NY*PREV_NY)
               X_NORMAL = PREV_MOD * PROX_NX + PROX_MOD * PREV_NX
               Y_NORMAL = PREV_MOD * PROX_NY + PROX_MOD * PREV_NY
               X_NORMAL = X_NORMAL / (PREV_MOD + PROX_MOD)
               Y_NORMAL = Y_NORMAL / (PREV_MOD + PROX_MOD)
               RMOD = SQRT(X_NORMAL*X_NORMAL+Y_NORMAL*Y_NORMAL)
               X_NORMAL = X_NORMAL / RMOD
               Y_NORMAL = Y_NORMAL / RMOD
               rot(2*n-1) = X_NORMAL
               rot(2*n)   = Y_NORMAL
            END IF
         END DO
      END IF
C
      CLOSE (LUSUR)
C
      END
C-----------------------------------------------------------------------
      SUBROUTINE COMPBASE (NDIM, FILSUR, COOR, INCID, NNOB,LNOB, IA, IB,
     *                     GBASE)
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H, O-Z)
      CHARACTER*80 FILSUR, REC, ELTYPE
      DIMENSION COOR(1), GBASE(*), nelgrp(1000)
      DIMENSION INCID(1), LNOB(*)
      DIMENSION IA(1), IB(1)
      LUSUR = 8
C
C     OPEN SURFACE MESH
C
      OPEN (LUSUR, FILE=FILSUR, STATUS='OLD')
      CALL SEARSTR (LUSUR, 'COORDINATES')
      READ (LUSUR, *) NOD
      CALL SEARSTR (LUSUR, 'ELEMENT_GROUPS')
      READ (LUSUR,*) NGRPS
C
      NEL = 0
      DO I = 1, NGRPS
         CALL REAELTYP (LUSUR, I, NELGRP(I), ELTYPE)
         NEL = NEL + NELGRP(I)
      END DO
C
      IF (ELTYPE(1:4) .EQ. 'Seg2') THEN
         NNE = 2
      ELSE IF (ELTYPE(1:4) .EQ. 'Line') THEN
         NNE = 2
      ELSE IF (ELTYPE(1:4) .EQ. 'Linear_Triangle') THEN
         NNE = 3
      ELSE IF (ELTYPE(1:4) .EQ. 'Tri3') THEN
         NNE = 3
      ELSE
         STOP 'INVALID TYPE OF ELEMENT (Not yet programmed)'
      END IF
C
      CALL SEARSTR (LUSUR, 'INCIDENCE')
      READ (LUSUR, '(A80)') REC
      IF (REC(1:6) .NE. '<NONE>') STOP 'PROBLEMAS...'
      READ (LUSUR, *) (INCID(I), I = 1, NEL*NNE)
      

         IF (NDIM .NE. 2) STOP 'ERROR NORMAL COMP: ONLY 2-D)'
         CALL SEARSTR (LUSUR, 'COORDINATES')
         READ (LUSUR, *) NOD
         READ (LUSUR, *) (IDUM, (COOR(NDIM*(I-1)+K), K=1,NDIM),
     *                    I = 1, NOD)
C INITIALIZE ACCUMULATORS  
         do i = 1, ndim*ndim * nnob
            gbase(i) = 0.0
         end do
         do i = 1, nod
            IA(i) = 0
            IB(i) = 0
         end do
C Loop over elements
         do iel = 1, nel
            N1 = INCID(IEL*2-1) 
            N2 = INCID(IEL*2) 
            IA(N1) = N2
            IB(N2) = N1
         end do
C LOOP OVER NODES
         DO N = 1, NOD
            IF (lnob(N) .ne. 0) THEN
               NPREV = IA(N)
               NNEXT = IB(N)
               XP = COOR(2*NPREV-1)
               YP = COOR(2*NPREV)
               XX = COOR(2*N-1)
               YY = COOR(2*N)
               XN = COOR(2*NNEXT-1)
               YN = COOR(2*NNEXT)
               PREV_NX = -(YY - YP)
               PREV_NY =   XX - XP
               PROX_NX = -(YN - YY)
               PROX_NY =   XN - XX
               PREV_MOD = SQRT(PREV_NX*PREV_NX+PREV_NY*PREV_NY)
               PROX_MOD = SQRT(PREV_NX*PREV_NX+PREV_NY*PREV_NY)
               X_NORMAL = PROX_MOD * PROX_NX + PREV_MOD * PREV_NX
               Y_NORMAL = PROX_MOD * PROX_NY + PREV_MOD * PREV_NY
               X_NORMAL = X_NORMAL / (PREV_MOD+PROX_MOD)
               Y_NORMAL = Y_NORMAL / (PREV_MOD+PROX_MOD)
               RMOD = SQRT(X_NORMAL*X_NORMAL+Y_NORMAL*Y_NORMAL)
               X_NORMAL = X_NORMAL / RMOD
               Y_NORMAL = Y_NORMAL / RMOD
               IJK = LNOB(N)
               GBASE(IJK)  = X_NORMAL
               GBASE(IJK+1)= Y_NORMAL
               GBASE(IJK+2)= -Y_NORMAL
               GBASE(IJK+3)= X_NORMAL
            END IF
         END DO
C
      CLOSE (LUSUR)
      RETURN
C
      END
C---------------------------------------------------------------------
      FUNCTION IFOUNDKY (LU, STR)
C---------------------------------------------------------------------
      LOGICAL REW
      CHARACTER *(*) STR
      CHARACTER *80 REC
      INTEGER STRLEN
      IFOUNDKY = 1
      REW = .FALSE.
      STRLEN = LEN(STR)
   10 READ (LU, '(A80)', END=20) REC
      IF (REC(1:1) .NE. '*') GOTO 10
      IF (REC(2:STRLEN+1) .EQ. STR) RETURN
      IF (REC(2:4) .EQ. 'END') THEN
         IF (REW) THEN
            GOTO 20
         ELSE
            REW =.TRUE.
            REWIND (LU)
            GOTO 10
         ENDIF
      END IF
      GOTO 10
   20 IFOUNDKY = 0
      RETURN
      END
C---------------------------------------------------------------------
c      SUBROUTINE SEARSTR(LU, STR)
C---------------------------------------------------------------------
c      LOGICAL REW
c      CHARACTER *(*) STR
c      CHARACTER *80 REC
c      INTEGER STRLEN
c      REW = .FALSE.
c      STRLEN = LEN(STR)
c   10 READ (LU, '(A80)', END=20) REC
c      IF (REC(1:1) .NE. '*') GOTO 10
c      IF (REC(2:STRLEN+1) .EQ. STR) RETURN
c      IF (REC(2:4) .EQ. 'END') THEN
c         IF (REW) THEN
c            GOTO 20
c         ELSE
c            REW =.TRUE.
c            REWIND (LU)
c            GOTO 10
c         ENDIF
c      END IF
c      GOTO 10
c   20 STOP 'Can''t find Keyword'
c      END
cC---------------------------------------------------------------------
c      SUBROUTINE REAELTYP (LU, NGR, NEL, ELTYPE)
cC---------------------------------------------------------------------
c      CHARACTER *80 ELTYPE, REC
c      READ (LU, '(A80)') REC
c      OPEN  (3, FILE='SMORED.AUX',STATUS='UNKNOWN')
c      WRITE (3,'(A80)') REC
c      CLOSE (3)
c      OPEN (3, FILE='SMORED.AUX',STATUS='OLD')
c      READ (3, *) IGR, NEL
c      CLOSE(3, STATUS='DELETE')
cC
c      IF (IGR.NE.NGR) THEN
c        STOP 'Unexpected Group number'
c      END IF
c      I = 80
c      DO WHILE (REC(I:I) .EQ. ' ')
c         I = I - 1
c      END DO
c      ILAST = I
c      DO WHILE (REC(I:I) .NE. ' ')
c         I = I - 1
c      END DO
c      IFIRST = I+1
c      LENELTYPE = ILAST - IFIRST + 1
c      ELTYPE(1:LENELTYPE) = REC(IFIRST:ILAST)
c      RETURN
c      END


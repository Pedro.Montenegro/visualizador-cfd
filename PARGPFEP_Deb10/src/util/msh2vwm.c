#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define RECSIZE 300

int main(int argc, char **argv)
{
  int i, j, nsd, nodes, elements, *numnode, nnodes, invert = 0;
  int *n, *type, *igr, *nelgrps, firstnode;
  int npoints, nsegments, ntriangles, ntetrahedra;
  int nseggrps, ntrigrps, ntetgrps, lastseggrp,  lasttrigrp, lasttetgrp;
  int index, dim, iaux, /* vmaj, */ vmin;
  char buf[RECSIZE];
  char inpfile[RECSIZE], outfile[RECSIZE+4], outsur[RECSIZE+4];
  double *coords, zmin, zmax;
  FILE *fpi, *fpo, *fps;

  if (argc < 2)
    {
      fprintf(stderr, "Usage: %s inpfile.msh [dimension]\n",
              argv[0]);
      exit(1);
    }
  strncpy(inpfile, argv[1], RECSIZE);
  sprintf(outfile, "%s.vwm", inpfile);
  sprintf(outsur, "%s.sur", inpfile);

  if (argc > 2)
    {
      nsd = atoi(argv[2]);
      fprintf(stdout, "Forced %d spatial dimensions.\n", nsd);
    }
  else
    nsd = 0;

  if (argc > 3)
    invert = 1;
  fpi = fopen(inpfile, "r");
  fpo = fopen(outfile, "w");
  fps = fopen(outsur, "w");

  if (!fpi || !fpo || !fps)
    {
      fprintf(stderr, "Error opening files\n");
      exit(2);
    }

  /*
   *  First pass: count elements of each type
   *  count also used nodes
   */
  // Should read "$MeshFormat
  fgets(buf, RECSIZE, fpi);
  // Read 4 integers (mesh format)
  fgets(buf, RECSIZE, fpi);
  if (!strncmp(buf, "2 0 8", 5) || 
      !strncmp(buf, "2.1 0 8", 7))
    {
      /* vmaj = 2; */
      vmin = 1;
      /* i = 0; */
      /* j = 8; */
    }
  else if (!strncmp(buf, "2.2 0 8", 7))
    {
      /* vmaj = 2; */
      vmin = 2;
      /* i = 0; */
      /* j = 8; */
    }
  else
    {
      fprintf(stderr, "Invalid file format\n"
	      "(only 2 0 8, 2.1 0 8 and 2.2 0 8 supported)\nRead:\n%s\n",
              buf);
      exit(1);
    }
  // Should read "$EndMeshFormat
  fgets(buf, RECSIZE, fpi);
  // Should read "$Nodes
  fgets(buf, RECSIZE, fpi);
  fscanf(fpi, "%d", &nodes);
  fprintf(stdout, "Number of nodes: %d\n", nodes);
  numnode = (int *) malloc(nodes * sizeof(int));
  coords = (double *) malloc(nodes * 3 * sizeof(double));
  if (!numnode || !coords)
    {
      fprintf(stderr, "Error malloc-ing numnode || coords\n");
      exit(3);
    }
  zmin = zmax = 0.0;
  for (i = 0; i < nodes; i++)
    {
      fscanf(fpi, "%d", &index);
      if (i == 0) // First node may be "0" or "1"
	{
	  if (index == 0 || index ==1)
	    firstnode = index;
	  else
	    firstnode = 1;
	}
      if (index != i+firstnode)
        {
          fprintf(stderr, "Error with node index: %d != %d\n",
                  index, i+1);
          exit(4);
        }
      numnode[i] = 0;
      for (j = 0; j < 3; j++)
        fscanf(fpi, "%lg", coords+3*i+j);
      if (coords[3*i+2] < zmin)
	zmin = coords[3*i+2];
      else if (coords[3*i+2] > zmax)
	zmax = coords[3*i+2];
    }
  if (nsd == 0)
    {
      if (zmax - zmin < 1e-9)
	nsd = 2;
      else
	nsd = 3;
      fprintf(stdout, "Auto-detected %d spatial dimensions.\n", nsd);
    }

  // Read end of line (last node)
  fgets(buf, RECSIZE, fpi);
  // Should read "$EndNodes
  fgets(buf, RECSIZE, fpi);

  npoints = nsegments = ntriangles = ntetrahedra = 0;
  lasttetgrp = lasttrigrp = lastseggrp = -1;
  ntetgrps = ntrigrps = nseggrps = 0;

  // Should read "$Elements
  fgets(buf, RECSIZE, fpi);
  fscanf(fpi, "%d", &elements);
  fprintf(stdout, "Number of elements: %d\n", elements);
  n = (int *) malloc(4*elements * sizeof(int));
  type = (int *) malloc(elements * sizeof(int));
  igr = (int *) malloc(elements * sizeof(int));
  if (!n || !type || !igr)
    {
      fprintf(stderr, "Error malloc-ing n || type || igr\n");
      exit(5);
    }
  // Read end of line
  fgets(buf, RECSIZE, fpi);
  for (i = 0; i < elements; i++)
    {
      // Read line with element description
      fgets(buf, RECSIZE, fpi);
      sscanf(buf, "%d %d %d", &index, &type[i], &dim);
      igr[i] = type[i]; // Group may be missing in file
      if (index != i+1)
        {
          fprintf(stderr, "Error with element index: %d != %d\n",
                  index, i+1);
	  // exit(6);
        }
      if (type[i] == 15)  // Point, just count
        {
          npoints++;
          continue;
        }
      if (type[i] == 1)   // Segment
        {
          nsegments++;
          // Read full line: index, type, dimension, 0, group, 0, nodes
          if (vmin == 1) // format: 2.1 0 8
	    sscanf(buf, "%d %d %d %d %d %d %d %d",
		   &index, &type[i], &dim, &iaux, &igr[i], &iaux,
		   n+4*i, n+4*i+1);
	  else           // format: 2.2 0 8
	    {
	      if (dim == 2)         // generated with gmsh, read physical
		sscanf(buf, "%d %d %d %d %d %d %d", // and geometric tags
		       &index, &type[i], &dim, &iaux, &igr[i],
		       n+4*i, n+4*i+1);
	      else if (dim == 0)    // no tags 
		sscanf(buf, "%d %d %d %d %d",
		       &index, &type[i], &dim, n+4*i, n+4*i+1);
	      else
		fprintf(stderr, "Not implemented # of tags: %d\n", dim);
	    }
	  n[4*i]   += (1-firstnode); // firsnode == 1: ok, == 0: +1
	  n[4*i+1] += (1-firstnode);

          // Check for new group
          if (igr[i] != lastseggrp)
            {
              /* fprintf(stdout, "Elements read: %d, new element group %d.\n",
                 i, igr[i]); */
              nseggrps++;
              lastseggrp = igr[i];
            }
          // Mark nodes as used
          numnode[n[4*i  ]-1] = 1;
          numnode[n[4*i+1]-1] = 1;
          continue;
        }
      if (type[i] == 2)   // Triangle
        {
          ntriangles++;
          // Read full line: index, type, dimension, 0, group, 0, nodes
          if (vmin == 1) // format: 2.1 0 8
	    sscanf(buf, "%d %d %d %d %d %d %d %d %d",
		   &index, &type[i], &dim, &iaux, &igr[i], &iaux,
		   n+4*i, n+4*i+1, n+4*i+2);
	  else           // format: 2.2 0 8
	    {
	      if (dim == 2)            // generated with gmsh, read physical
		sscanf(buf, "%d %d %d %d %d %d %d %d", // and geometric tags
		       &index, &type[i], &dim, &iaux, &igr[i],
		       n+4*i, n+4*i+1, n+4*i+2);
	      else if (dim == 0)
		sscanf(buf, "%d %d %d %d %d %d",
		       &index, &type[i], &dim,
		       n+4*i, n+4*i+1, n+4*i+2);
	      else
		fprintf(stderr, "Not implemented # of tags: %d\n", dim);
	    }
	  n[4*i]   += (1-firstnode); // firsnode == 1: ok, == 0: +1
	  n[4*i+1] += (1-firstnode);
	  n[4*i+2] += (1-firstnode);
          // Check for new group
          if (igr[i] != lasttrigrp)
            {
              /* fprintf(stdout, "Elements read: %d, new element group %d.\n",
                 i, igr[i]); */
              ntrigrps++;
              lasttrigrp = igr[i];
            }
          // Mark nodes as used
          numnode[n[4*i  ]-1] = 1;
          numnode[n[4*i+1]-1] = 1;
          numnode[n[4*i+2]-1] = 1;
          continue;
        }
      if (type[i] == 4)   // Tetrahedron
        {
          ntetrahedra++;
          // Read full line: index, type, dimension, 0, group, 0, nodes
	  if (vmin == 1) // format: 2.1 0 8
	    sscanf(buf, "%d %d %d %d %d %d %d %d %d %d",
		   &index, &type[i], &dim, &iaux, &igr[i], &iaux,
		   n+4*i, n+4*i+1, n+4*i+2, n+4*i+3);
	  else           // format: 2.2 0 8
	    {
	      if (dim == 2)            // generated with gmsh, read physical
		sscanf(buf, "%d %d %d %d %d %d %d %d %d", // and geometric tags
		       &index, &type[i], &dim, &iaux, &igr[i],
		       n+4*i, n+4*i+1, n+4*i+2, n+4*i+3);
	      else if (dim == 0)    // no tags 
		sscanf(buf, "%d %d %d %d %d %d %d", // and geometric tags
		       &index, &type[i], &dim,
		       n+4*i, n+4*i+1, n+4*i+2, n+4*i+3);
	      else
		fprintf(stderr, "Not implemented # of tags: %d\n", dim);
	    }
	  n[4*i]   += (1-firstnode); // firsnode == 1: ok, == 0: +1
	  n[4*i+1] += (1-firstnode);
	  n[4*i+2] += (1-firstnode);
	  n[4*i+3] += (1-firstnode);
          // Check for new group
          if (igr[i] != lasttetgrp)
            {
              /* fprintf(stdout, "Elements read: %d, new element group %d.\n",
                 i, igr[i]); */
              ntetgrps++;
              lasttetgrp = igr[i];
            }
          // Mark nodes as used
          numnode[n[4*i  ]-1] = 1;
          numnode[n[4*i+1]-1] = 1;
          numnode[n[4*i+2]-1] = 1;
          numnode[n[4*i+3]-1] = 1;
          continue;
        }
    }  // End loop elements
  fprintf(stdout, "Point elements read: %d.\n", npoints);
  fprintf(stdout, "Segment elements read: %d.\n", nsegments);
  fprintf(stdout, "Triangular elements read: %d.\n", ntriangles);
  fprintf(stdout, "Tetrahedrical elements read: %d.\n", ntetrahedra);

  // Read end of line (last element)
  fgets(buf, RECSIZE, fpi);
  // Should read "$EndElements
  fgets(buf, RECSIZE, fpi);

  fclose(fpi);
  //  rewind(fpi);

  /*
   * Check for unused nodes
   */
  nnodes = 0;
  for (i = 0; i < nodes; i++)
    {
      if (numnode[i] == 0)
        numnode[i] = -1;
      else
        numnode[i] = nnodes++;
    }
  // Now numnode has the renumbering
  fprintf(stdout, "Number of actually used nodes: %d\n", nnodes);

  fprintf(stdout, "Writing volume and surface meshes...\n");
  /*
   *  write output files
   */
  fprintf(fpo, "*COORDINATES\n %d\n", nnodes);
  for (i = 0; i < nodes; i++)
    {
      if (numnode[i] >= 0)
        {
          fprintf(fpo, " %d", numnode[i]+1);
          for (j = 0; j < nsd; j++)
            fprintf(fpo, " %lg", coords[3*i+j]);
          fprintf(fpo, "\n");
        }
    }

  fprintf(fps, "*COORDINATES\n %d\n", nnodes);
  for (i = 0; i < nodes; i++)
    {
      if (numnode[i] >= 0)
        {
          fprintf(fps, " %d", numnode[i]+1);
          for (j = 0; j < nsd; j++)
            fprintf(fps, " %lg", coords[3*i+j]);
          fprintf(fps, "\n");
        }
    }
  if (nsd == 2)
    {
      fprintf(fps, "\n*ELEMENT_GROUPS\n %d\n", nseggrps);
      fprintf(fpo, "\n*ELEMENT_GROUPS\n %d\n", ntrigrps);
    }
  else if (nsd == 3)
    {
      fprintf(fps, "\n*ELEMENT_GROUPS\n %d\n", ntrigrps);
      fprintf(fpo, "\n*ELEMENT_GROUPS\n %d\n", ntetgrps);
    }

  nelgrps = (int *) malloc((nseggrps+ntrigrps+ntetgrps)*sizeof(int));
  if (!nelgrps)
    {
      fprintf(stderr, "Error malloc-ing nelgrps\n");
      exit(6);
    }
  for (i = 0; i < nseggrps+ntrigrps+ntetgrps; i++)
    nelgrps[i] = 0;

  /*
   * Count segment elements in each group
   */
  lastseggrp = -1;
  nseggrps = 0;
  for (i = 0; i < elements; i++)
    {
      if (type[i] == 1)   // Segment
        {
          if (igr[i] != lastseggrp)
            {
              nseggrps++;
              lastseggrp = igr[i];
            }
          nelgrps[nseggrps-1]++;
        }
    } // End loop elements

  /*
   * Count triangular elements in each group
   */
  lasttrigrp = -1;
  ntrigrps = 0;
  for (i = 0; i < elements; i++)
    {
      if (type[i] == 2)   // Triangle
        {
          if (igr[i] != lasttrigrp)
            {
              ntrigrps++;
              lasttrigrp = igr[i];
            }
          nelgrps[nseggrps+ntrigrps-1]++;
        }
    } // End loop elements

  /*
   * Count tetrahedrical elements in each group
   */
  lasttetgrp = -1;
  ntetgrps = 0;
  for (i = 0; i < elements; i++)
    {
      if (type[i] == 4)   // Tetrahedron
        {
          if (igr[i] != lasttetgrp)
            {
              ntetgrps++;
              lasttetgrp = igr[i];
            }
          nelgrps[nseggrps+ntrigrps+ntetgrps-1]++;
        }
    } // End loop elements

  // Write number of elements in each group, segments, triangles, tetrahedra
  if (nsd == 2)
    {
      for (i = 0; i < nseggrps; i++)
        fprintf(fps, " %d %d Seg2\n", i+1, nelgrps[i]);
      for (i = 0; i < ntrigrps; i++)
        fprintf(fpo, " %d %d Tri3\n", i+1, nelgrps[nseggrps+i]);
    }
  else if (nsd == 3)
    {
      for (i = 0; i < ntrigrps; i++)
        fprintf(fps, " %d %d Tri3\n", i+1, nelgrps[nseggrps+i]);
      for (i = 0; i < ntetgrps; i++)
        fprintf(fpo, " %d %d Tetra4\n", i+1, nelgrps[nseggrps+ntrigrps+i]);
    }

  /*
   * Now write incidences
   */
  fprintf(fps, "\n*INCIDENCES\n<NONE>\n");
  fprintf(fpo, "\n*INCIDENCES\n<NONE>\n");

  if (nsd == 2)
    {
      int i0 = 0, i1 = 1, nn;
      double x1,y1,x2,y2,x3,y3,area;

      // Find first triangle
      for (i = 0; i < elements; i++)
	{
	  if (type[i] == 2)
	    {
	      // Check triangle orientation
	      nn = n[4*i] - 1;
	      x1 = coords[3*nn];
	      y1 = coords[3*nn+1];
	      nn = n[4*i+1] - 1;
	      x2 = coords[3*nn];
	      y2 = coords[3*nn+1];
	      nn = n[4*i+2] - 1;
	      x3 = coords[3*nn];
	      y3 = coords[3*nn+1];
	      area = (x2-x1)*(y3-y1)-(y2-y1)*(x3-x1);
	      
	      if (area < 0.0)
		{
		  i0 = 1;
		  i1 = 0;
		}
	      break;
	    }
	}
      if (invert)
	{
	  i0 = 1 - i0;
	  i1 = 1 - i1;
	}

      for (i = 0; i < elements; i++)
        {
          if(type[i] == 1) // Segment
            {
              fprintf(fps, " %d %d\n",
                      numnode[n[4*i  ]-1] + 1,
                      numnode[n[4*i+1]-1] + 1);
            }
        }

      for (i = 0; i < elements; i++)
        {
          if(type[i] == 2) // Triangle (check orientation)
            {
              fprintf(fpo, " %d %d %d\n",
                      numnode[n[4*i+i0]-1] + 1,
                      numnode[n[4*i+i1]-1] + 1,
                      numnode[n[4*i+ 2]-1] + 1);
            }
        }
    }
  else if (nsd == 3)
    {
      for (i = 0; i < elements; i++)
        {
          if(type[i] == 2) // Triangle (invert orientation)
            {
              fprintf(fps, " %d %d %d\n",
                      numnode[n[4*i+1]-1] + 1,
                      numnode[n[4*i  ]-1] + 1,
                      numnode[n[4*i+2]-1] + 1);
            }
        }
      for (i = 0; i < elements; i++)
        {
          if(type[i] == 4) // Tetrahedron
            {
              fprintf(fpo, " %d %d %d %d\n",
                      numnode[n[4*i  ]-1] + 1,
                      numnode[n[4*i+1]-1] + 1,
                      numnode[n[4*i+2]-1] + 1,
                      numnode[n[4*i+3]-1] + 1);
            }
        }
    }
  fprintf(fps, "\n*END\n");
  fclose(fps);
  fprintf(fpo, "\n*END\n");
  fclose(fpo);
  fprintf(stdout, "... ready.\n\n");
  exit(0);
}

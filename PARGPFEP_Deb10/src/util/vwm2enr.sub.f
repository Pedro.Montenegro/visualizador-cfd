C     ------------------------------------------------------------------
      SUBROUTINE SEARSTR(LU, STR)
C     ------------------------------------------------------------------
      LOGICAL REW
      CHARACTER *(*) STR
      CHARACTER *80 REC
      INTEGER STRLEN
      REW = .FALSE.
      STRLEN = LEN(STR)
   10 READ (LU, '(A80)', ERR=20, END=20) REC
      IF (REC(1:1) .NE. '*') GOTO 10
      IF (REC(2:STRLEN+1) .EQ. STR) RETURN
      IF (REC(2:4) .EQ. 'END') GOTO 20
      GOTO 10
   20 IF (REW) THEN
         GOTO 30
      ELSE
         REW =.TRUE.
         REWIND (LU)
         GOTO 10
      ENDIF
   30 STOP 'Can''t find Keyword'
      END
C     ------------------------------------------------------------------
      FUNCTION IFOUNDKY (LU, STR)
C     ------------------------------------------------------------------
      LOGICAL REW
      CHARACTER *(*) STR
      CHARACTER *80 REC
      INTEGER STRLEN
      IFOUNDKY = 1
      REW = .FALSE.
      STRLEN = LEN(STR)
   10 READ (LU, '(A80)', END=20, ERR=20) REC
      IF (REC(1:1) .NE. '*') GOTO 10
      IF (REC(2:STRLEN+1) .EQ. STR) RETURN
      IF (REC(2:4) .EQ. 'END') GOTO 20
      GOTO 10
   20 IF (REW) THEN
         GOTO 30
      ELSE
         REW = .TRUE.
         REWIND (LU)
         GOTO 10
      END IF
   30 IFOUNDKY = 0
      RETURN
      END
c
      SUBROUTINE READCOOR (LU, idim, NOD, X, Y, Z)
      IMPLICIT REAL*8 (A-H, O-Z)
      DIMENSION X(1), Y(1), Z(1)
      if (idim .EQ. 2) then
         DO I = 1, NOD
            READ (LU, *, ERR=10, END=10) IDUM, X(I), Y(I)
         END DO
      else if (idim .EQ. 3) then
         DO I = 1, NOD
            READ (LU, *, ERR=10, END=10) IDUM, X(I), Y(I), Z(I)
         END DO
      else
         goto 10
      end if
      RETURN
   10 STOP 'Error reading Coordinates'
      END
c     ------------------------------------------------------------------
      SUBROUTINE READXYZ (LU, idim, NOD, XYZ)
c     ------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H, O-Z)
      DIMENSION XYZ(*)
c
      DO I = 1, NOD
         READ (LU, *, ERR=10, END=10)
     #        IDUM, (XYZ(IDIM*(I-1)+J), J = 1, idim)
      END DO
c
      RETURN
   10 STOP 'Error reading Coordinates'
      END
c     ------------------------------------------------------------------
      SUBROUTINE REAELTYP (LU, NGR, NEL, ELTYPE)
c     ------------------------------------------------------------------
      CHARACTER *80 ELTYPE, REC
      READ (LU, '(A80)') REC
      OPEN  (3, FILE='SMORED.AUX',STATUS='UNKNOWN')
      WRITE (3,'(A80)') REC
      CLOSE (3)
      OPEN (3, FILE='SMORED.AUX',STATUS='OLD')
      READ (3, *) IGR, NEL
      CLOSE(3, STATUS='DELETE')
C
      IF (IGR.NE.NGR) THEN
         STOP 'Unexpected Group number'
      END IF
      I = 80
      DO WHILE (REC(I:I) .EQ. ' ')
         I = I - 1
      END DO
      ILAST = I
      DO WHILE (REC(I:I) .NE. ' ')
         I = I - 1
      END DO
      IFIRST = I+1
      LENELTYPE = ILAST - IFIRST + 1
      ELTYPE(1:LENELTYPE) = REC(IFIRST:ILAST)
      RETURN
      END
c     ------------------------------------------------------------------
      SUBROUTINE READJE (LU, NEL, LAS, IE, JE)
c     ------------------------------------------------------------------
      CHARACTER*80 MODE
      LOGICAL READINDEX, TESTINDEX
      DIMENSION IE(1), JE(1)
c
      READ (LU, '(A80)') MODE
      READINDEX = .FALSE.
      TESTINDEX = .FALSE.
      IF (MODE(1:8) .EQ. '<IGNORE>') READINDEX = .TRUE.
      IF (MODE(1:6) .EQ. '<TEST>') THEN
         READINDEX = .TRUE.
         TESTINDEX = .TRUE.
      END IF
C
      IJE = 1
cc
c      DO I = 1, NEL
c         IE(I) = IJE
c         IF (LAS/NEL .EQ. 3) THEN
c	    IF (MODE(1:8) .EQ. '<IGNORE>' .OR.
c     *		MODE(1:6) .EQ. '<TEST>') THEN
c		READ (LU, *) IDUM, (JE(IJE+K), K = 0, 2)
c	    ELSE
cC		 (deberia ser: <NONE>)
c		READ (LU, *) (JE(IJE+K), K = 0, 2)
c	    END IF
c            IJE = IJE + 3
c         ELSE IF (LAS/NEL .EQ. 4) THEN
c	    IF (MODE(1:8) .EQ. '<IGNORE>' .OR.
c     *		MODE(1:6) .EQ. '<TEST>') THEN
c		READ (LU, *) IDUM, (JE(IJE+K), K = 0, 3)
c	    ELSE
c		READ (LU, *) (JE(IJE+K), K = 0, 3)
c	    END IF
c            IJE = IJE + 4
c         END IF
c      END DO
c
      NNE = LAS/NEL
      DO I = 1, NEL
         IE(I) = IJE
         IF (READINDEX) THEN
            READ (LU, *) IDUM, (JE(IJE+K), K = 0, NNE - 1)
         ELSE
            READ (LU, *)       (JE(IJE+K), K = 0, NNE - 1)
         END IF
         IF (TESTINDEX .AND. IDUM .NE. I) STOP 'BAD INDEX'
         IJE = IJE + NNE
      END DO
      IE (NEL+1) = IJE
      RETURN
      END
c     ------------------------------------------------------------------
      SUBROUTINE READJE2 (LU, NEL, LAS, JE)
c     ------------------------------------------------------------------
      CHARACTER*80 MODE
      LOGICAL READINDEX, TESTINDEX
      DIMENSION JE(*)
c
      READ (LU, '(A80)') MODE
      READINDEX = .FALSE.
      TESTINDEX = .FALSE.
      IF (MODE(1:8) .EQ. '<IGNORE>') READINDEX = .TRUE.
      IF (MODE(1:6) .EQ. '<TEST>') THEN
         READINDEX = .TRUE.
         TESTINDEX = .TRUE.
      END IF
C
      IJE = 1
      NNE = LAS/NEL
      DO I = 1, NEL
c         IE(I) = IJE
         IF (READINDEX) THEN
            READ (LU, *) IDUM, (JE(IJE+K), K = 0, NNE - 1)
         ELSE
            READ (LU, *)       (JE(IJE+K), K = 0, NNE - 1)
         END IF
         IF (TESTINDEX .AND. IDUM .NE. I) STOP 'BAD INDEX'
         IJE = IJE + NNE
      END DO
c      IE (NEL+1) = IJE
      RETURN
      END
c
      SUBROUTINE WRRED (LU, idim, NOD, NEL, LAS, IE, JE, X, Y, Z)
      IMPLICIT REAL*8 (A-H, O-Z)
      DIMENSION IE(1), JE(1), X(1), Y(1), Z(1)
c
      WRITE (LU, *) NOD, NEL, LAS, 0
c
      WRITE (LU, *) (IE(I), I = 1, NEL+1)
      WRITE (LU, *) (JE(I), I = 1, LAS)
      WRITE (LU, *) (X(I), I = 1, NOD)
      WRITE (LU, *) (Y(I), I = 1, NOD)
      if (idim .EQ. 3) then
         WRITE (LU, *) (Z(I), I = 1, NOD)
      end if
      RETURN
      END

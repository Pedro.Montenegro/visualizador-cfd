C
C	Lee en formato VWM, escribe en formato binario ParaView (VTK)
C
      PROGRAM VWM2VTK
C
c      PARAMETER (MEMMAX = 3900000)
      IMPLICIT REAL*8 (A-H, O-Z)
      character*80 name, eltype
      Allocatable xyz (:)
      Allocatable je (:)
c      real*4 a(MEMMAX)
      common /nod/nod  /nel/nel,las
c
c
c     Apertura del archivo con la red
c
   10 write (6,1010)
 1010 format (' Input_Mesh_file [red.vwm] : ')
      read  (5,'(a80)') name
      if (name .eq. ' ') then
         name = 'red.vwm'
      end if
c
c     Lectura de la red y dimensionamiento
c
      open (1,file=name, status='old', err=10)
c
      if (IFOUNDKY(1, 'ELEMENT_GROUPS') .NE. 1)
     *     call searstr(1, 'ELEMENT GROUPS')
      read (1,*) ngroups
      if (ngroups.ne.1) then
         stop 'More than one element group not allowed'
      end if
c
      call reaeltyp (1, 1, nel, eltype)
      if (eltype(1:15) .EQ. 'LINEAR_SEGMENT' .OR.
     *    eltype(1:4)  .EQ. 'Seg2') then
         las = 2 * nel
         nsd = 2
      else if (eltype(1:15) .EQ. 'LINEAR_TRIANGLE' .OR.
     *    eltype(1:4)  .EQ. 'Tri3') then
         las = 3 * nel
         nsd = 2
      else if (eltype(1:18) .EQ. 'LINEAR_TETRAHEDRON' .OR.
     *    ELTYPE(1:6)  .EQ. 'Tetra4') then
         las = 4 * nel
         nsd = 3
      else
         stop 'Linear triangle o tetrahedra elements required'
      end if
c
      call searstr(1, 'COORDINATES')
      read (1,*) nod
      Allocate(xyz(nod*nsd))
c
      call readxyz(1, nsd, nod, xyz)
c
      Allocate(je(las))
c
      call searstr(1, 'INCIDENCE')
      call readje2 (1, nel, las, je)
      close (1)
c
!      write (*,*) 'Output_Mesh_File?'
!      read (*,'(a80)') name
!      if (name(1:1) .eq. ' ') stop
!      write (*,*) 'Writing mesh ...'
c      OPEN (1, FILE=name, STATUS='UNKNOWN')
      call WriteEnsightMeshBIN (xyz, je, nod, nel, nsd)
c      close (1)
      write (*,*) '... listo.'
c
   30 stop
      end

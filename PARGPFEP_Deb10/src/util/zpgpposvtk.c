/*
 *  Par-gppos with support for reading .gz files.
 *            writes binary VTK files.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MAXFIELDS 50
#define SIZBUF 300
int count_files(char *name, char *nameXX, int *alphaver);
int makename (int alphaver, int istep, char* name, char *nameXX);

int main (int argc, char **argv)
{
  int   nfields, nnodes, nfcfg, ancomp;
  int   ncomp[MAXFIELDS];
  int   i, j, k, alphaver;
  int   totcomps, savecomps, step, nfiles, istep;
  //  char  c1, c2;
  char  *name;
  char  *nameXX;
  char  *auxname;
  char *pc;
  double tmp;
  double parcon;
  float *fields[MAXFIELDS];
  float zero = 0.0;
  char  *FieldNames[MAXFIELDS];
  FILE  *cfgfile;
  FILE  *casefp;

  gzFile *zfile[MAXFIELDS + 1];
  int fdfile[MAXFIELDS];
  char buffer[SIZBUF];
  int found, ierr;

  nfields = 0;
  if ((cfgfile = fopen ("par_gppos.cfg", "r")) != NULL)
    {
      printf ("Found configuration file \n");
      fscanf (cfgfile, "%d", &nfields);
      if (nfields < 1 || nfields > MAXFIELDS)
	{
	  printf("Error: invalid number of fields\n");
	  fclose(cfgfile);
	  exit(1);
	}
      for (i = 0; i < nfields; i++)
	{
	  if ((FieldNames[i] = (char *) malloc(SIZBUF * sizeof(char))) == NULL)
	    {
	      printf("\n Not enough memory for FieldNames\n");
	      exit(2);
	    }
	  ierr = fscanf (cfgfile, "%d %s", &ncomp[i], FieldNames[i]);
	  if (ierr != 2)
	    {
	      printf("Error reading field %d's ncomp & field name\n",
		     i+1);
	      fclose(cfgfile);
	      exit(3);
	    }
	}
      /* Read initial step */
      ierr = fscanf(cfgfile, "%d", &istep);
      if (ierr != 1)
	istep = 0;
      fclose (cfgfile);
    }
  nfcfg = nfields;

  if (argc < 2)
    {
      printf("Usage: %s <File with output> [<nfields> <ncomps_1> "
	     "<ncomps_2> ... <ncomps_nfields>]", argv[0]);
      exit(4);
    }

  if (argc < 4 && nfcfg == 0)
    {
      printf("Usage: %s <File with output> <nfields> <ncomps_1> "
	     "<ncomps_2> ... <ncomps_nfields>", argv[0]);
      exit(8);
    }

  if (argc > 2)
    {
      nfields = atoi(argv[2]);
      if (nfcfg > 0 && nfields != nfcfg)
	{
	  printf("\n Conflicting number of fields "
		 "in cfg file (%d) and command line (%d)\n", nfcfg, nfields);
	  exit(5);
	}

      if (nfcfg == 0)
	for (i = 0; i < nfields; i++)
	  {
	    if ((FieldNames[i]=(char *)malloc(SIZBUF * sizeof(char))) == NULL)
	      {
		printf("\n Not enough memory for FieldNames\n");
		exit(6);
	      }    
	    strcpy (FieldNames[i], " ");
	  }
      for (i = 0; i < nfields; i++)
	ncomp[i] = atoi(argv[3 + i]);
      if (argc >= 4+nfields)
	istep = atoi(argv[3 + nfields]);
      else
	istep = 0;
    }

  totcomps = 0;
  savecomps = 0;
  for (i = 0; i < nfields; i++)
    {
      if (ncomp[i] > 0)
	{
	  totcomps += ncomp[i];
	  savecomps += ncomp[i];
	}
      else
	{
	  totcomps += -ncomp[i];
	}
    }

  name  = (char *) malloc (SIZBUF * sizeof(char));
  nameXX  = (char *) malloc (SIZBUF * sizeof(char));
  auxname = (char *) malloc (SIZBUF * sizeof(char));
  if (!name || !nameXX || !auxname)
    {
      printf ("\n Not enough memory (2)");
      exit (7);
    }

  strcpy (name, argv[1]);
  /*
  lname = 0;
  while (name[lname] != '.' && name[lname])
    {
      auxname[lname] = name[lname];
      lname ++;
    }
  auxname[lname] = '\0';            // auxname: name until first dot
  step = (auxname[lname-2] - 'A')*26 + auxname[lname-1] - 'A';
  */

  /*
  nfiles = 0;
  for (c1 = 'A'; c1 <= 'Z'; c1++)
    for (c2 = 'A'; c2 <= 'Z'; c2++)
      {
	sprintf(nameXX, "%s%c%c.dat", name, c1, c2);
	//	printf("Opening... %s, ", nameXX);
	if ((zfile[0] = gzopen (nameXX, "rb")))
	  {
	    nfiles++;
	    gzclose(zfile[0]);
	    printf("%s FOUND!\n", nameXX);
	  }
      }
  */
  nfiles = count_files(name, nameXX, &alphaver);

  if (nfiles == 0)
    {
      printf ("\n Error opening %s\n", name);
      exit(9);
    }

  /* write case file */
  i = 0;
  pc = name;
  while(*pc)
    {
      if (*pc == '.')
	break;
      if (*pc != '_')
	auxname[i++] = *pc++;
      else
	pc++;
    }
  auxname[i] = '\0';
  strcpy(nameXX, auxname);
  sprintf(auxname+i, ".case");
  casefp = fopen(auxname, "w");
  fprintf(casefp,
	  "FORMAT\ntype:   ensight\n\nGEOMETRY\nmodel:     %s.geo\n", nameXX);
  fprintf(casefp, "\nVARIABLE\n\n");
  for (i = 0; i < nfields; i++)
    {
      if (ncomp[i] > 0)
	{
	  if (ncomp[i] == 1)
	    fprintf(casefp, "scalar per node:     %s    PV-%d-****.scl\n",
		    FieldNames[i], i);
	  else
	    fprintf(casefp, "vector per node:     %s    PV-%d-****.vec\n",
		    FieldNames[i], i);
	}
    }
  fprintf(casefp, "\nTIME\ntime set: 1\nnumber of steps: %d\n", nfiles);
  fprintf(casefp,
	  "filename start number:  %d\nfilename increment:  1\n", istep);
  fprintf(casefp, "time values:\n");

  step = -1;
  istep = 0;
  while (istep < nfiles && makename(alphaver, istep++, name, nameXX))
    //      for (c1 = 'A'; c1 <= 'Z'; c1++)
    //	for (c2 = 'A'; c2 <= 'Z'; c2++)
    {
      //      sprintf(nameXX, "%s%c%c.dat", name, c1, c2);
      zfile[0] = gzopen (nameXX, "rb");
      printf("%s, ", nameXX);
      step++;

      found = 0;
      while (!found)
	{
	  gzgets (zfile[0], buffer, SIZBUF);
	  found = !strncmp (buffer, "*NOD", 4);
	}
      gzgets (zfile[0], buffer, SIZBUF);
      sscanf (buffer, "%d", &nnodes);
      //      printf("Nodes: %d, ", nnodes);

      if (step == 0)
	{
	  for (i = 0; i < nfields; i++)
	    {
	      if (ncomp[i] > 0)
		{
		  fields[i] = (float *)
		    malloc (nnodes*ncomp[i]*sizeof(float));
		  if (!fields[i])
		    {
		      printf ("\n Not enough memory (180)");
		      exit (10);
		    }
		}
	    }
	}

      found = 0;
      while (!found)
	{
	  gzgets (zfile[0], buffer, SIZBUF);
	  found = !strncmp (buffer, "*SCALAR_FIELD", 13);
	}
      gzgets (zfile[0], buffer, SIZBUF);
      sscanf (buffer, "%s %s %lf", auxname, auxname, &parcon);
      printf("time: %lg, ", parcon);

      fprintf(casefp, " %lg\n", parcon);

      found = 0;
      while (!found)
	{
	  gzgets (zfile[0], buffer, SIZBUF);
	  found = !strncmp (buffer, "<NONE>", 6);
	}

      for (i = 0; i < nfields; i++)
	{
	  if (ncomp[i] > 0)
	    {
	      if (ncomp[i] > 1)
		sprintf(auxname, "PV-%d-%.4d.vec", i, step);
	      else
		sprintf(auxname, "PV-%d-%.4d.scl", i, step);

	      //	      printf("Opening... %s, ", auxname);
	      fdfile[i] = open(auxname, O_WRONLY|O_CREAT,
			       S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
	      //      printf("Field: %d, name: %s, file descriptor: %d\n",
	      //	     i, auxname, fdfile[i]);
	      write(fdfile[i], FieldNames[i], 80);
	    }
	}

      for (i = 0; i < nnodes; i++)
	{
	  for (j = 0; j < nfields; j++)
	    {
	      ancomp = ncomp[j] > 0 ? ncomp[j] : -ncomp[j];
	      for (k = 0; k < ancomp; k++)
		{
		  gzgets (zfile[0], buffer, SIZBUF);
		  if (ncomp[j] > 0)
		    {
		      sscanf (buffer, "%lf", &tmp);
		      fields[j][i*ncomp[j]+k] = tmp;
		    }
		}
	    }
	}

      for (i = 0; i < nfields; i++)
	{
	  if (ncomp[i] > 0)
	    {
	      for (j = 0; j < nnodes; j++)
		{
		  write(fdfile[i], &fields[i][j*ncomp[i]],
			ncomp[i]*sizeof(float));
		  if (ncomp[i] == 2)
		    write(fdfile[i], &zero, sizeof(float));
		}
	      close(fdfile[i]);
	    }
	}
      gzclose(zfile[0]);
      printf("ready file %d/%d.\n", istep, nfiles);

    }
  fclose (casefp);
  return 0;
}

int count_files (char* name, char *nameXX, int *alphaver)
{
  int nfiles = 0, ndigits;
  gzFile *pzfile;
  char *pn = name;

  ndigits=0;
  while (*pn)
    {
      if (*pn++ == '_')
	ndigits++;
    }

  *alphaver = 0;
  if (ndigits == 0) // Old behaviour: XX before .dat
    {
      int c1, c2;
      *alphaver = 1;
      for (c1 = 'A'; c1 <= 'Z'; c1++)
	for (c2 = 'A'; c2 <= 'Z'; c2++)
	  {
	    sprintf(nameXX, "%s%c%c.dat", name, c1, c2);
	    //	printf("Opening... %s, ", nameXX);
	    if ((pzfile = gzopen (nameXX, "rb")))
	      {
		nfiles++;
		gzclose(pzfile);
		printf("%s FOUND!\n", nameXX);
	      }
	  }
      return nfiles;
    }
  else
    {
      // New behaviour: replace "___" by digits
      int i, j, k, ntot, indfile, idig, ldig;
      ntot = 1;
      i = ndigits;
      while(i--)
	ntot *=10;
      for (i = 0; i < ntot; i++)
	{
	  indfile = i;
	  sprintf(nameXX, "%s", name);
	  for (idig = 0; idig < ndigits; idig++)
	    {
	      ldig = indfile % 10; // last digit in indfile
	      indfile = indfile / 10;
	      j = k = 0;
	      while (nameXX[k])
		{
		  if (nameXX[k] == '_')
		    {
		      j++;
		      if (j == ndigits-idig)
			break;
		    }
		  k++;
		}
	      nameXX[k] = '0' + ldig;
	    }
	  if ((pzfile = gzopen (nameXX, "rb")))
	    {
	      nfiles++;
	      gzclose(pzfile);
	      printf("%s FOUND!\n", nameXX);
	    }
	  else
	    break;
	}
      return nfiles;
    }
}

int makename (int alphaver, int istep, char* name, char *nameXX)
{
  if (alphaver)
    {
      int c1, c2;
      c1 = 'A' + (istep/26)%26;
      c2 = 'A' + istep%26;
      sprintf(nameXX, "%s%c%c.dat", name, c1, c2);
    }
  else
    {
      int ndigits, j, k, indfile, idig, ldig;
      char *pn = name;

      ndigits=0;
      while (*pn)
	{
	  if (*pn++ == '_')
	    ndigits++;
	}
      sprintf(nameXX, "%s", name);

      indfile = istep;
      for (idig = 0; idig < ndigits; idig++)
	{
	  ldig = indfile % 10; // last digit in indfile
	  indfile = indfile / 10;
	  j = k = 0;
	  while (nameXX[k])
	    {
	      if (nameXX[k] == '_')
		{
		  j++;
		  if (j == ndigits-idig)
		    break;
		}
	      k++;
	    }
	  nameXX[k] = '0' + ldig;
	}
    }
  return 1;
}

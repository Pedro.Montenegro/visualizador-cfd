VisualCFD: Es una función de post-procesamiento de OpenFOAM para extraer datos y visualizar en tiempo real. Las instrucciones para instalarla se encuentran dentro de la carpeta.

PARGPFEP: Simulador de elementos finitos.

Visualizador CFD: Herramienta de visualización orientada a problemas de fluido dinámica. Posee dos modos de visualización:
    
    -Online: Permite conectarse al solver (GPFEP o OpenFOAM) y analizar la solución mientras este la calcula.
    -Offline: Se cargan las geometrías luego de que ya ha terminado la corrida de cálculo.
 
Filtros y parametros:
 StreamLines: Calcula las lineas de corriente.
    -Type of Integration: RungeKutta2, RungeKutta4, RungeKutta45
    -Direction of Integration: Forward, Backward, both.
    -Time step of integration
    -Min/Max time of integration (rango maximo y minimo de la integracion)
    -Ratio of sample. Indica cada cuantos puntos de la grilla toma una semilla. ej: 10 -> toma 1 punto cada 10.
    -Tube filter radio: radio de los tubos dibujados.
    -Number of sides: numero de lados (resoulucion) de los tubos dibujados.
    
 VortexFinder: Encuentra los vortices.Este filtro tiene 3 parametros, los cuales fijan condiciones sobre los autovalores del tensor gradiente de velocidad.
    -Min value: (Autovalor.parteReal / Autovalor.parteImaginaria)>Min Value. Este parametro filtra vortices segun que tan compactos son. Su valor por defecto es el del metodo Q (-1/Sqrt(3));
    -Max value: (Autovalor.parteReal / Autovalor.parteImaginaria)<Max Value. Este parametro filtra vortices segun que tan compactos son. Su valor por defecto es el del metodo Q (1/Sqrt(3));
    -Abs value: Autovalor.parteImaginaria >Abs Value. Este parametro filtra vortices segun que tan rapido gira el vortice.
    
 PathLines: Calcula las lineas de trayectoria.
    -Time step: Paso de tiempo con el que son calculadas las lineas de trayectoria.
    -Tiempo inicial: Tiempo inicial de la integracion.
    -Tiempo final: Tiempo final de la integracion.
    -Tipos de generadores de semilla:
        -Mask method: Dispersa las semillas por toda la grilla.
            -Ratio of sample. Indica cada cuantos puntos de la grilla toma una semilla. ej: 10 -> toma 1 punto cada 10.
            -Simple/Random sample: Distintos algoritmos para elegir los puntos de la grilla.
        -Sphere method: Produce una nube de puntos como semillas dentro de una esfera la cual puede moverse con el mouse.
            -Radio: Radio de la esfera.
            -Number of points: Numero de semillas dentro de la esfera.
        -Line method: Produce semillas a lo largo de una linea.
            - 1:x 1:y 1:z : Posiciones del punto #1 de la linea.
            - 2:x 2:y 2:z : Posiciones del punto #2 de la linea.
            - Seeds: Numero de semillas a lo largo de la linea.
            - is2D: Opcion para geometrias 2D. Coloca los puntos(#1 y #2) sobre la grilla. Si la geometria es 3D toma por defecto el plano Z de la grilla y lo coloca ahi.
             
 Puntos de estancamiento: Busca los puntos de estancamiento.
    -Point size: Tamaño con el que se visualizan los puntos:
    -Show Surface points: Por defecto el filtro extrae los puntos que se encuentran sobre la superficie, ya que estos suelen ser consecuencia de las condiciones de borde. Con esta opcion se pueden mostrar.
    -Point List: Muestra los puntos encontrados y sus coordenadas.
    
 Caudal en plano: Calcula el caudal a través de un plano seleccionado dinámicamente desde la interfaz.
    -Calculo caudal: Calcula el caudal y lo muestra a la derecha del botton.
    -Plane normal components: componentes de la normal del plano.
    -Plane origin components: coordenadas de un punto perteneciente al plano.
    -Clip geometry: Opcion para visualizar solo la geometria de un lado del plano.
    -is 2D: Para geometrias 2D proyecta la normal sobre el plano donde se encuentra la geometria.
 Gradiente:
    - Calcula el gradiente de un campo vectorial o escalar.
    - Posee la opcion de calcular la divergencia, vorticidad o QCriterion de un campo vectorial.
    
 Clip: Realiza un corte de la geometria utilizando un plano seleccionado dinámicamente.
    -Plane normal components: componentes de la normal del plano.
    -Plane origin components: coordenadas de un punto perteneciente al plano.
    
 Slice: Realiza la intersección de la geometria con un plano seleccionado dinámicamente
    -Plane normal components: componentes de la normal del plano.
    -Plane origin components: coordenadas de un punto perteneciente al plano.
    
 Fuerzas Viscosas: Calcula las fuerzas viscosas sobre la superficie de la geometria.
    - Viscosidad dinamica con la que se calculan las fuerzas.
    - Presión de referencia.
                                                                              

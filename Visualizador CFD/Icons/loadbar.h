#ifndef LOADBAR_H
#define LOADBAR_H

#include <QDialog>

namespace Ui {
class LoadBar;
}

class LoadBar : public QDialog
{
    Q_OBJECT

public:
    explicit LoadBar(QWidget *parent = nullptr);
    ~LoadBar();

private:
    Ui::LoadBar *ui;
};

#endif // LOADBAR_H

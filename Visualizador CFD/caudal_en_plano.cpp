#include "caudal_en_plano.h"

Caudal_en_plano::Caudal_en_plano()
{
    Clipper = vtkSmartPointer<vtkClipDataSet>::New();
    cutter = vtkSmartPointer<vtkCutter>::New();
    plane = new Plane_Widget;
    type = CaudalPlano;
    InputHandler = new Input_handler(1);
    callback = new GeoCallback;
    FilterPropConfig();
}

Caudal_en_plano::~Caudal_en_plano()
{
    mute.lock();
    unsigned long i;
    for( i= 0 ; i< Input->NextFilter.size();i++)
    {
        if(Input->NextFilter[i]->Name == this->Name)
        {
            break;
        }
    }
    Input->NextFilter.erase(Input->NextFilter.begin()+i);
    plane->plane_Widget->RemoveObserver(callback);
    delete Calculo_caudal_box;
    delete InputHandler;
    delete plane;
    callback->Delete();
    mute.unlock();

}

void Caudal_en_plano::SetInputData(Geometry *Source)
{
    mute.lock();
    //this->Grid->ShallowCopy(Source->Grid);
    this->Times->ShallowCopy(Source->Times);
    render_Win = Source->render_Win;
    Input = Source;
    this->Name = Source->Name + "/CaudalEnPlano";
    std::string prev_field_name = Source->Field_list[Source->SelectedField]->Field_Name;
    this->SelectedField = Source->SelectedField;
    this->CurrentTimeStep = Source->CurrentTimeStep;

    InputHandler->AddAllFields(Source->Field_list,Vector);
    InputHandler->SetInputName(0,"Velocity");

    InputHandler->AddItemsToComboBox(0,Vector);
    InputHandler->SetInputTimeStep(Source->CurrentTimeStep);
    InputHandler->SetSelectedInput(prev_field_name);

    Input->GetOutput(Grid);
    Grid->GetPointData()->SetActiveVectors(InputHandler->GetSelectedFieldName().c_str());
    Grid->GetPointData()->SetActiveScalars(InputHandler->GetSelectedFieldMagnitudName().c_str());
    Mapper->SetInputData(this->Grid);
    Mapper->ScalarVisibilityOn();
    Mapper->Update();
    Actor->SetMapper(Mapper);

    //Plane settings
    plane->WidgetConfig(render_Win,Input->Mapper->GetBounds(),Input->Mapper->GetCenter());
    //Cutter settings
    cutter->SetInputData(this->Grid);
    cutter->SetCutFunction(plane->plane_function);
    cutter->GenerateValues(1,0,0);
    cutter->Update();

    QCheckBox::connect(ClipOption,SIGNAL(pressed()),this,SLOT(UpdateClipOption()));

    Clipper->SetInputData(this->Grid);
    Clipper->SetClipFunction(plane->plane_function);
    Clipper->Update();

    Mapper->SetInputConnection(Clipper->GetOutputPort());
    Mapper->Update();

    QCheckBox::connect(is2D_CheckBox,SIGNAL(pressed()),this,SLOT(ChangeBetween2D3D()));
    QComboBox::connect(InputHandler->ComboBoxes[0],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputField(QString)));
    callback->Geo =this;
    plane->plane_Widget->AddObserver(vtkCommand::EndInteractionEvent,callback);

    Input->NextFilter.push_back(this);

    mute.unlock();
}


double Caudal_en_plano::Integral_over_cell(double *Normal, vtkSmartPointer<vtkDataSet> Corte, vtkIdType i)
{
    //Integral en celda trinagular
    //Exacto para funciones de interpolación lineales.
    double Area =0;
    if(Corte->GetCell(i)->GetNumberOfPoints()==3)
    {
    int puntos[3];
    double p1[3],p2[3],p3[3];

    puntos[0]=Corte->GetCell(i)->GetPointId(0);
    puntos[1]=Corte->GetCell(i)->GetPointId(1);
    puntos[2]=Corte->GetCell(i)->GetPointId(2);

    Corte->GetPoint(puntos[0], p1);
    Corte->GetPoint(puntos[1], p2);
    Corte->GetPoint(puntos[2], p3);


    double u[3]={p2[0]-p1[0],p2[1]-p1[1],p2[2]-p1[2]};
    double v[3]={p3[0]-p1[0],p3[1]-p1[1],p3[2]-p1[2]};
    double Cross_product[3]={
            u[1]*v[2]-u[2]*v[1],
            u[2]*v[0]-v[2]*u[0],
            u[0]*v[1]-v[0]*u[1]};
    Area= sqrt(
            Cross_product[0]*Cross_product[0]+
            Cross_product[1]*Cross_product[1]+
            Cross_product[2]*Cross_product[2])/2;
    }
    if(Corte->GetCell(i)->GetNumberOfPoints()==2)
    {
        int puntos[3];
        double p1[3],p2[3];

        puntos[0]=Corte->GetCell(i)->GetPointId(0);
        puntos[1]=Corte->GetCell(i)->GetPointId(1);

        Corte->GetPoint(puntos[0], p1);
        Corte->GetPoint(puntos[1], p2);

        Area = std::sqrt((p2[0]-p1[0])*(p2[0]-p1[0]) + (p2[1]-p1[1])*(p2[1]-p1[1])+ (p2[2]-p1[2])*(p2[2]-p1[2]));
    }
    double *auu = new double;
    Corte->GetCellData()->GetArray(InputHandler->GetSelectedFieldName().c_str())->GetTuple(i,auu);
    double result= 0;
    int j;
    for(j=0;j < Corte->GetCellData()->GetArray(InputHandler->GetSelectedFieldName().c_str())->GetNumberOfComponents();j++)
    {
        result += Normal[j]*auu[j];
    }
    result = result * Area;
    return result;


}

double Caudal_en_plano::caudal(double *Normal, vtkSmartPointer<vtkPolyData> cut)
{
    double Aux,Caudal;
    Caudal=0;

    int Number_of_Cells = cut->GetNumberOfCells();
    int i;


    vtkSmartPointer<vtkPointDataToCellData> asd = vtkSmartPointer<vtkPointDataToCellData>::New();
    asd->SetInputData(cut);
    asd->Update();

    for(i= 0;i<Number_of_Cells; i++)
    {
     Aux=Integral_over_cell(Normal, asd->GetOutput(), i);
     Caudal+= Aux;
    }
    return Caudal;
}

void Caudal_en_plano::FilterPropConfig()
{

    Calculo_caudal_box = new QGroupBox;
    Calculo_caudal_layout = new QGridLayout(Calculo_caudal_box);
    Calculo_caudal_label = new QLabel(Calculo_caudal_box);
    Calculo_caudal_button = new QPushButton(Calculo_caudal_box);
    Calculo_caudal_box->setTitle("Caudal");
    Calculo_caudal_box->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    Calculo_caudal_button->setText("Calculo caudal");

    Calculo_caudal_layout->addWidget(Calculo_caudal_button,0,0);
    Calculo_caudal_layout->addWidget(Calculo_caudal_label,0,1);
    Calculo_caudal_box->setLayout(Calculo_caudal_layout);
    Calculo_caudal_box->setMaximumHeight(100);

    QPushButton::connect(Calculo_caudal_button,SIGNAL(pressed()),this,SLOT(CaudalCalc()));


    ClipOption = new QCheckBox(plane->options);
    ClipOption->setText("Clip geometry");
    ClipOption->setCheckState(Qt::Checked);

    is2D_CheckBox = new QCheckBox(plane->options);
    is2D_CheckBox->setText("is 2D");
    is2D_CheckBox->setCheckState(Qt::Unchecked);

    plane->options_layout->addWidget(ClipOption);
    plane->options_layout->addWidget(is2D_CheckBox);

    QVBoxLayout *FilterProp = new QVBoxLayout;
    InputHandler->AddQBoxToLayout(FilterProp);
    FilterProp->addWidget(Calculo_caudal_box);
    plane->AddPropertiesBoxToLayout(FilterProp);
    AddFilterProperties(FilterProp);
    FilterProp->setAlignment(Qt::AlignmentFlag::AlignTop);

    Properties_names.push_back("Filter prop.");

}

void Caudal_en_plano::CaudalCalc()
{
    cutter->SetInputData(Grid);
    cutter->Update();
    double c = caudal(plane->plane_function->GetNormal(),cutter->GetOutput());
    Calculo_caudal_label->setText(QString::fromStdString(std::to_string(c) + " [m³/s]"));
}

void Caudal_en_plano::UpdateClipOption()
{
    if(ClipOption->isChecked())
    {
        Mapper->SetInputData(this->Grid);
        Mapper->Update();
    }
    else
    {
        Mapper->SetInputConnection(Clipper->GetOutputPort());
        Mapper->Update();
    }
}

void Caudal_en_plano::ChangeBetween2D3D()
{
    if(is2D_CheckBox->isChecked())
    {
        plane->plane_callback->is2D=0;
    }
    else
    {
        plane->plane_callback->is2D=1;
    }
    Update();
}

void Caudal_en_plano::HideOrShow(int i)
{
    if(i==0)
    {
        Actor->VisibilityOff();
    }
    else
    {
        Actor->VisibilityOn();
    }
    plane->HideOrShowPlane(i);
}

void Caudal_en_plano::Update()
{
    mute.lock();
    Grid->GetPointData()->SetActiveVectors(InputHandler->GetSelectedFieldName().c_str());
    Grid->GetPointData()->SetActiveScalars(InputHandler->GetSelectedFieldMagnitudName().c_str());
    if(plane->plane_callback->is2D==1)
    {
        double *bounds = Actor->GetBounds();
        if(bounds[0]==bounds[1])
        {
            plane->plane_callback->plane_normal[0]=0;
        }
        else if(bounds[2]==bounds[3])
        {
            plane->plane_callback->plane_normal[1]=0;
        }
        else if(bounds[4]==bounds[5])
        {
            plane->plane_callback->plane_normal[2]=0;
        }
        double module = std::sqrt(plane->plane_callback->plane_normal[0]*plane->plane_callback->plane_normal[0]+
                plane->plane_callback->plane_normal[1]*plane->plane_callback->plane_normal[1]+
                plane->plane_callback->plane_normal[2]*plane->plane_callback->plane_normal[2]);
        plane->plane_callback->plane_normal[0]=plane->plane_callback->plane_normal[0]/module;
        plane->plane_callback->plane_normal[1]=plane->plane_callback->plane_normal[1]/module;
        plane->plane_callback->plane_normal[2]=plane->plane_callback->plane_normal[2]/module;

        plane->plane_representation->SetNormal(plane->plane_callback->plane_normal);
        plane->plane_representation->GetPlane(plane->plane_function);
    }
    Clipper->Update();
    Mapper->Update();
    SendSignalsDownStream();
    mute.unlock();
}
void Caudal_en_plano::UpstreamChange()
{
    mute.lock();
    CurrentTimeStep=Input->CurrentTimeStep;

    QComboBox::disconnect(InputHandler->ComboBoxes[0],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputField(QString)));

    std::string prev_velocity_name = InputHandler->GetSelectedFieldName(0);

    InputHandler->RemoveItemsFromComboBox();
    InputHandler->UpdateInputHandler(Input->Field_list);
    InputHandler->AddItemsToComboBox(0,Vector);

    if(!InputHandler->SetSelectedInput(0,prev_velocity_name))
    {
        InputHandler->SetSelectedInput(0,InputHandler->GetVectorsFieldsNames()[0]);
        InputHandler->SetComboBoxIndex(0,InputHandler->GetVectorsFieldsNames()[0]);
    }
    else
    {
        InputHandler->SetComboBoxIndex(0,prev_velocity_name);
    }

    Input->GetOutput(Grid);

    Grid->GetPointData()->SetActiveVectors(InputHandler->GetSelectedFieldName().c_str());
    Grid->GetPointData()->SetActiveScalars(InputHandler->GetSelectedFieldMagnitudName().c_str());
    QComboBox::connect(InputHandler->ComboBoxes[0],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputField(QString)));

    mute.unlock();
    Update();

}

void Caudal_en_plano::HideWidgets()
{
    plane->plane_Widget->Off();
}
void Caudal_en_plano::ShowWidgets()
{
    if(Actor->GetVisibility() && plane->ShowOrHidePlane_CheckBox->isChecked())
        plane->plane_Widget->On();
}

void Caudal_en_plano::ChangeToThisTime(int i)
{
    Input->ChangeToThisTime(i);
}

void Caudal_en_plano::ChangeField(std::string field_name)
{
    InputHandler->SetSelectedInput(field_name);
    Update();
    ParallelDownStream->start();
}

void Caudal_en_plano::ChangeInputField(QString name)
{
    ChangeField(name.toStdString());
}

void Caudal_en_plano::ReScaleToCurrentData()
{
    if(Grid->GetPointData()->GetNumberOfArrays() ==0)
        return;
    Mapper->SetScalarRange(InputHandler->GetSelectedSolutionMagnitud()->GetRange());
    Mapper->Update();
}

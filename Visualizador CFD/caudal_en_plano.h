#ifndef CAUDAL_EN_PLANO_H
#define CAUDAL_EN_PLANO_H

#include "geometry.h"
#include "plane_widget.h"
#include "input_handler.h"

#include <vtkCommand.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkCutter.h>
#include <vtkImplicitPlaneWidget2.h>
#include <vtkImplicitPlaneRepresentation.h>
#include <vtkPlane.h>
#include <vtkCell.h>
#include <vtkCellData.h>
#include <vtkEventQtSlotConnect.h>


#include <vtkPointDataToCellData.h>
#include <vtkClipDataSet.h>

#include <QCheckBox>


class Caudal_en_plano : public Geometry
{
    Q_OBJECT
public:
    //Constructor
    Caudal_en_plano();
    //Destructor
    ~Caudal_en_plano();

    /**
     * @brief Sirve como input para la clase
     * @param Geometria input
     */
    void SetInputData(Geometry *Source);
    /**
     * @brief Esconde o muestra la geometria y los widgets
     * @param i = 1 muestra, i =0 oculta
     */
    void HideOrShow(int i) override;
    /**
     * @brief Esconde los widgets
     */
    void HideWidgets() override;
    /**
     * @brief Muestra los widgets
     */
    void ShowWidgets() override;
    /**
     * @brief Cambia el arbol de geometrias a este paso de tiempo
     * @param i = paso de tiempo
     */
    void ChangeToThisTime(int i) override;
    /**
     * @brief Re escalea la solución al campo actual.
     */
    void ReScaleToCurrentData() override;
    /**
     * @brief Cambia el campo sobre el cual se calcula el caudal
     * @param field_name
     */
    void ChangeField(std::string field_name) override;
    /**
     * @brief UpstreamChange. Se usa en caso ed que el input del filtro cambie. Usualmente se llama mediante "SendSignalsDownStream"
     */
    void UpstreamChange() override;
    /**
     * @brief Actualiza el filtro a nivel objeto. Por ejemplo, cuando el plano cambia de lugar.
     */
    void Update() override;

protected:
    //Variables para la pestaña de propiedades
    //Caudal
    QGridLayout *Calculo_caudal_layout;
    QGroupBox *Calculo_caudal_box;
    QPushButton *Calculo_caudal_button;
    QLabel *Calculo_caudal_label;

    // Clip option
    QCheckBox *ClipOption;
    //2D option
    QCheckBox *is2D_CheckBox;

    //Flag para proyectar la solución en un plano de ser necesario
    int is2D;

private:
    /**
     * @brief Crea la pestaña de propiedades del filtro
     */
    void FilterPropConfig();
    /**
     * @brief Calcula el caudal de una celda
     * @param Normal. Normal al plano
     * @param Resultado del filtro "cutter".
     * @param Indice de la celda sobre la que se hace la integral
     * @return devuelve el caudal de la celda.
     */
    double Integral_over_cell(double *Normal, vtkSmartPointer<vtkDataSet> Corte, vtkIdType i);
    /**
     * @brief Calcula el caudal por todo el plano
     * @param Normal al plano
     * @param Resultado del filtro "cutter"
     * @return Caudal
     */
    double caudal(double *Normal,vtkSmartPointer<vtkPolyData> cut);
    /**
     * @brief Clase que calcula la intersección de la geometria con el caudal.
     */
    vtkSmartPointer<vtkCutter> cutter;
    /**
     * @brief Clase que calcula el corte de la geometria con el plano. Se usa solo si se activa la opción clip.
     */
    vtkSmartPointer<vtkClipDataSet> Clipper;
    /**
     * @brief Clase para manejar los distintos inputs. Controla inputs vectoriales en este caso
     */
    Input_handler * InputHandler;
    /**
     * @brief Clase implementada para manejar el plano. Provee tanto una pestaña de propiedades como todas las clases necesarias.
     */
    Plane_Widget *plane;

public slots:
    /**
     * @brief Slot que calcula el caudal luego de apretar el boton.
     */
    void CaudalCalc();
    /**
     * @brief Activa/Desactiva la opción de clip
     */
    void UpdateClipOption();
    /**
     * @brief Cambia entre el modo 2D y 3D.
     */
    void ChangeBetween2D3D();
    /**
     * @brief Cambia el input del filtro.
     * @param Nombre del input.
     */
    void ChangeInputField(QString name);
};

#endif // CAUDAL_EN_PLANO_H

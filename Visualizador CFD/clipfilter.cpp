#include "clipfilter.h"
ClipFilter::ClipFilter()
{
    Clipper = vtkSmartPointer<vtkClipDataSet>::New();
    Plane = new Plane_Widget;
    callback = new GeoCallback;
    type = Filter_type::Slice;
    FilterPropConfig();
    grid_copy = vtkSmartPointer<vtkUnstructuredGrid>::New();

}

ClipFilter::~ClipFilter()
{
    mute.lock();
    unsigned long i;
    for( i= 0 ; i< Input->NextFilter.size();i++)
    {
        if(Input->NextFilter[i]->Name == this->Name)
        {
            break;
        }
    }
    Input->NextFilter.erase(Input->NextFilter.begin()+i);
    Plane->plane_Widget->RemoveObserver(callback);
    delete Plane;
    callback->Delete();
    mute.unlock();
}

void ClipFilter::SetInputData(Geometry *Source)
{
    mute.lock();
    //render_Win = Source->render_Win;
    this->Times->ShallowCopy(Source->Times);

    this->Name = Source->Name + "/Clipped";
    //this->type = Source->type;
    if(type == PuntosEstancamiento)
    {
        Actor->GetProperty()->SetPointSize(10);
    }
    this->SelectedField = 0;
    this->CurrentTimeStep = Source->CurrentTimeStep;

    Input = Source;

    callback->Geo = this;
    Input->GetOutput(grid_copy);

    Clipper->SetInputData(grid_copy);

    //Plane settings
    Plane->WidgetConfig(render_Win,Input->Mapper->GetBounds(),Input->Mapper->GetCenter());
    //Clipper->SetInputConnection(Source->Mapper->GetInputConnection(0,0));
    //render_Win->Render();

    Clipper->SetClipFunction(Plane->plane_function);

    Clipper->Update();

    Grid->ShallowCopy(Clipper->GetOutput());
    unsigned long j;
    for(int i=0;i<Grid->GetPointData()->GetNumberOfArrays();i++)
    {
        vtkSmartPointer<vtkDataArray> aux = Grid->GetPointData()->GetArray(i);
        const std::string name1 = aux->GetName();
        if(name1.find("comp") !=std::string::npos)
            continue;
        for(j=0; j< Input->Field_list.size();j++)
        {
            if(Input->Field_list[j]->Field_Name ==name1)
            {
                break;
            }
        }
        if(j==Input->Field_list.size())
            continue;

        this->Field_list.push_back(new Field(aux));
    }

    Mapper->SetInputConnection(Clipper->GetOutputPort());
    Mapper->Update();
    Actor->SetMapper(Mapper);

    Plane->plane_Widget->AddObserver(vtkCommand::EndInteractionEvent,callback);

    SetVectors();

    Input->NextFilter.push_back(this);

    mute.unlock();
}

void ClipFilter::FilterPropConfig()
{
    QVBoxLayout *FilterProp = new QVBoxLayout;
    Plane->AddPropertiesBoxToLayout(FilterProp);
    AddFilterProperties(FilterProp);
    FilterProp->setAlignment(Qt::AlignmentFlag::AlignTop);

    Properties_names.push_back("Filter prop.");
}

void ClipFilter::UpstreamChange()
{
    mute.lock();
    CurrentTimeStep = Input->CurrentTimeStep;
    Input->GetOutput(grid_copy);
    Clipper->SetInputData(grid_copy);
    mute.unlock();
    Update();
}
void ClipFilter::ChangeToThisTime(int i)
{
    Input->ChangeToThisTime(i);
}
void ClipFilter::GetOutput(vtkSmartPointer<vtkUnstructuredGrid> GridData)
{
    mute.lock();
    Clipper->Update();
    GridData->ShallowCopy(Clipper->GetOutput());
    mute.unlock();
}
void ClipFilter::Update()
{
    mute.lock();
    Clipper->Update();
    Grid->ShallowCopy(Clipper->GetOutput());
    Mapper->SetInputData(Grid);
    Mapper->Update();
    std::vector<std::string> names;
    std::vector<unsigned long> selectedComponent;
    for(unsigned long i=0; i<Field_list.size();i++)
    {
        names.push_back(Field_list[i]->Field_Name);
        selectedComponent.push_back(Field_list[i]->selectedComponent);
        delete Field_list[i];
    }
    Field_list.clear();
    for(int i=0;i<Grid->GetPointData()->GetNumberOfArrays();i++)
    {
        unsigned long j;
        vtkSmartPointer<vtkDataArray> aux = Grid->GetPointData()->GetArray(i);
        const std::string name1 = aux->GetName();
        if(name1.find("comp") !=std::string::npos)
            continue;
        for(j=0; j< Input->Field_list.size();j++)
        {
            if(Input->Field_list[j]->Field_Name ==name1)
            {
                break;
            }
        }
        if(j==Input->Field_list.size())
            continue;
        this->Field_list.push_back(new Field(aux));
    }
    for(int i =0; i<Field_list.size();i++)
    {
        for(unsigned long j=0;j<names.size();j++)
        {
            if(names[j] == Field_list[i]->Field_Name)
            {
                Field_list[i]->selectedComponent = selectedComponent[j];
                break;;
            }
        }
    }
    SendSignalsDownStream();
    mute.unlock();
    emit UpdateInterfaseSignal();

}

void ClipFilter::ShowWidgets()
{
    if(Actor->GetVisibility() && Plane->ShowOrHidePlane_CheckBox->isChecked())
        Plane->plane_Widget->On();
    if(UseScalarBar && Actor->GetVisibility())
    {
        ScalarBar->VisibilityOn();
        ScalarWidget->On();
    }
}
void ClipFilter::HideWidgets()
{
    Plane->plane_Widget->Off();
    if(UseScalarBar)
    {
        ScalarWidget->Off();
        ScalarBar->VisibilityOff();
    }
}

void ClipFilter::HideOrShow(int i)
{
    if(i==1)
    {
        Actor->VisibilityOn();
    }
    else
    {
        Actor->VisibilityOff();
    }
    Plane->HideOrShowPlane(i);
}

void ClipFilter::SetScalarBarWidget()
{
    mute.lock();
    if(Field_list.empty())
        return;
    ScalarBar = vtkSmartPointer<vtkScalarBarActor>::New();
    ScalarBar->SetNumberOfLabels(5);
    ScalarBar->SetHeight(0.4);
    ScalarBar->DragableOn();
    //ScalarBar->SetLookupTable(Mapper->GetLookupTable());
    ScalarWidget = vtkSmartPointer<vtkScalarBarWidget>::New();
    ScalarWidget->SetScalarBarActor(ScalarBar);
    ScalarWidget->SetInteractor(this->render_Win->GetInteractor());


    UseScalarBar = true;
    ScalarBar->VisibilityOff();
    ScalarWidget->Off();
    mute.unlock();
}

void ClipFilter::ReScaleToCurrentData()
{
    if(Grid->GetPointData()->GetNumberOfArrays() ==0)
        return;
    if(Field_list[SelectedField]->type ==Solid)
        return;
    if(Field_list[SelectedField]->selectedComponent==-1)
    {
        Mapper->SetScalarRange(Field_list[SelectedField]->Solution_Magnitud[0]->GetRange());
    }
    else
    {
        int selec = Field_list[SelectedField]->selectedComponent;
        Mapper->SetScalarRange(Field_list[SelectedField]->Solution_components[0][selec]->GetRange());
    }
    Mapper->Update();
}

void ClipFilter::DisconectSonsWidgets()
{
    mute.lock();
    Plane->plane_Widget->RemoveObserver(callback);
    Plane->DisconectAll();
    foreach(Geometry *geo, NextFilter)
    {
        geo->DisconectSonsWidgets();
    }
    mute.unlock();
}

void ClipFilter::ReconectSonsWidgets()
{
    mute.lock();
    Plane->plane_Widget->AddObserver(vtkCommand::EndInteractionEvent,callback);
    Plane->ReconectAll();
    foreach(Geometry *geo, NextFilter)
    {
        geo->ReconectSonsWidgets();
    }
    mute.unlock();

}

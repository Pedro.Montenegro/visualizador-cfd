#ifndef CLIPFILTER_H
#define CLIPFILTER_H

#include "geometry.h"
#include "plane_widget.h"
#include <vtkCommand.h>
#include <vtkCell.h>
#include <vtkCellData.h>

#include <vtkClipDataSet.h>
#include <vtkRenderWindow.h>
#include <QCheckBox>

class ClipFilter : public Geometry
{
    Q_OBJECT
public:
    //Constructor
    ClipFilter();
    //Destructor
    ~ClipFilter();

    /**
     * @brief Sirve como input para la clase
     * @param Source = Input
     */
    void SetInputData(Geometry *Source);
    /**
     * @brief Esconde o muestra la geometria y los widgets
     * @param i = 1 muestra, i =0 oculta
     */
    void HideOrShow(int i) override;
    /**
     * @brief Cambia el arbol de geometrias a este paso de tiempo
     * @param i = paso de tiempo
     */
    void ChangeToThisTime(int i) override;
    /**
     * @brief Crea una copia de la geometria dentro de GridData
     * @param GridData = Contenedor donde se va a copiar
     */
    void GetOutput(vtkSmartPointer<vtkUnstructuredGrid> GridData) override;
    /**
     * @brief UpstreamChange. Se usa en caso ed que el input del filtro cambie. Usualmente se llama mediante "SendSignalsDownStream"
     */
    void UpstreamChange() override;
    /**
     * @brief Actualiza el filtro a nivel objeto. Por ejemplo, cuando el plano cambia de lugar.
     */
    void Update() override;
    /**
     * @brief Esconde los widgets
     */
    void HideWidgets() override;
    /**
     * @brief Muestra los widgets
     */
    void ShowWidgets() override;
    /**
     * @brief Configura la barra de escala de colores
     */
    void SetScalarBarWidget() override;
    /**
     * @brief Re escalea la solución al campo actual.
     */
    void ReScaleToCurrentData() override;
    /**
     * @brief ReconectSonsWidgets. Reconect all the widgets. Implemeted to call after update is complete.
     * This function is meant to be override for classes with widgets
     */
    virtual void ReconectSonsWidgets() override;
    /**
     * @brief DisconectSonsWidgets. Implemented for avoid widget interaction during update process in online mode.
     * This function is meant to be override for classes with widgets
     */
    virtual void DisconectSonsWidgets() override;
protected:
    /**
     * @brief Clase que calcula el corte de la geometria con el plano. Se usa solo si se activa la opción clip.
     */
    vtkSmartPointer<vtkClipDataSet> Clipper;
    /**
     * @brief Clase implementada para manejar el plano. Provee tanto una pestaña de propiedades como todas las clases necesarias.
     */
    Plane_Widget *Plane;
private:
    /**
     * @brief Crea la pestaña de propiedades del filtro
     */
    void FilterPropConfig();
public slots:

};

#endif // CLIPFILTER_H

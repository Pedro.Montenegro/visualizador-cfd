#ifndef CONTOUR_FILTER_H
#define CONTOUR_FILTER_H

#include <QObject>
#include <vtkContourFilter.h>

#include "geometry.h"
#include "input_handler.h"

class Contour_Filter : public Geometry
{
public:
    //Constructor
    Contour_Filter();

    //Destructor
    ~Contour_Filter();

    //Flags

    //Functions
    void SetInputData(Geometry *Source);
    void FilterPropertiesConfig();
    void ChangeToThisTime(int i) override;

    vtkSmartPointer<vtkContourFilter> Contour_Calculator;
    Input_handler *InputHandler;

    void UpstreamChange() override;
    void Update() override;
};

#endif // CONTOUR_FILTER_H

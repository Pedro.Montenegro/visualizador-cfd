#include "customscale.h"
#include "ui_customscale.h"

#include <QDoubleValidator>
#include <QPushButton>
CustomScale::CustomScale(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CustomScale)
{
    ui->setupUi(this);
    this->ui->MaxValue->setValidator(new QDoubleValidator(this->ui->MaxValue));
    this->ui->MinValue->setValidator(new QDoubleValidator(this->ui->MinValue));
    this->ui->buttonBox->button( QDialogButtonBox::Ok )->setEnabled( false );
    this->setWindowTitle("Range to custom scale");
    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    QLineEdit::connect(this->ui->MaxValue,SIGNAL(textChanged(QString)),this,SLOT(EnableOk()));
    QLineEdit::connect(this->ui->MinValue,SIGNAL(textChanged(QString)),this,SLOT(EnableOk()));
    QLineEdit::connect(this->ui->MaxValue,SIGNAL(textChanged(QString)),this,SLOT(DisableOk()));
    QLineEdit::connect(this->ui->MinValue,SIGNAL(textChanged(QString)),this,SLOT(DisableOk()));

}

CustomScale::~CustomScale()
{
    delete ui;
}

void CustomScale::on_buttonBox_accepted()
{
    minvalue = std::stod(this->ui->MinValue->text().toStdString());
    maxvalue = std::stod(this->ui->MaxValue->text().toStdString());
    this->close();
}

void CustomScale::on_buttonBox_rejected()
{
    this->close();

}
void CustomScale::EnableOk()
{
    if(this->ui->MaxValue->text().isEmpty() || this->ui->MinValue->text().isEmpty())
        return;
    this->ui->buttonBox->button( QDialogButtonBox::Ok )->setEnabled( true );
}
void CustomScale::DisableOk()
{
    if(this->ui->MaxValue->text().isEmpty() || this->ui->MinValue->text().isEmpty())
        this->ui->buttonBox->button( QDialogButtonBox::Ok )->setEnabled( false );

}
void CustomScale::SetValues(double *a)
{
    this->ui->MinValue->setText(QString::fromStdString(std::to_string(a[0])));
    minvalue= a[0];
    this->ui->MaxValue->setText(QString::fromStdString(std::to_string(a[1])));
    maxvalue = a[1];

}

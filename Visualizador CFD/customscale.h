#ifndef CUSTOMSCALE_H
#define CUSTOMSCALE_H

#include <QDialog>

//Clase creada para ui de la ventana de autoscale.
namespace Ui {
class CustomScale;
}

class CustomScale : public QDialog
{
    Q_OBJECT

public:
    explicit CustomScale(QWidget *parent = nullptr);
    ~CustomScale();
    /**
     * @brief minvalue
     */
    double minvalue;
    /**
     * @brief maxvalue
     */
    double maxvalue;
    /**
     * @brief SetValues. Se usa para poner los valores actuales en la ventana.
     * @param a = puntero de dimension dos [min,max]
     */
    void SetValues(double *a);

private slots:
    //Slot para aceptar o cancelar.
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
    /**
     * @brief Activa los botones si los lineEdit estan llenos
     */
    void EnableOk();
    /**
     * @brief Desactiva los botones si los lineEdit no tiene un valor valido
     */
    void DisableOk();

private:
    Ui::CustomScale *ui;
};

#endif // CUSTOMSCALE_H

#include "field.h"

#include <vtkArrayData.h>
#include <vtkArray.h>
#include <vtkArrayDataReader.h>

//Constructor. NO es necesario inicializar nada.
Field::Field()
{

}
//Destructor. NO es necesario destruir nada.
Field::~Field()
{
    Solution.clear();
    Solution_Magnitud.clear();
    for(unsigned long i=0;i<Solution_components.size();i++)
    {
        Solution_components[i].clear();
    }
    Solution_components.clear();
}
Field::Field(Field *Source)
{
    this->type = Source->type;
    this->Field_Name = Source->Field_Name;
    for(unsigned long i=0;i<Source->Solution.size();i++)
    {
        vtkSmartPointer<vtkDoubleArray> copy = vtkSmartPointer<vtkDoubleArray>::New();
        copy->ShallowCopy(Source->Solution[i]);
        Solution.push_back(copy);
    }
    Magnitud();
    Components();
    this->selectedComponent = Source->selectedComponent;

}
//Reader Constructor
Field::Field(QString Field_Path)
{
    ReadAll(Field_Path);
}

Field::Field(vtkSmartPointer<vtkDoubleArray> array)
{
    Solution.push_back(array);
    Field_Name = array->GetName();
    Magnitud();
    Components();
    if(array->GetNumberOfComponents()>1)
        type = Vector;
    else
        type = Scalar;
    this->selectedComponent =-1;

}
Field::Field(vtkSmartPointer<vtkDataArray> array)
{
    Field_Name = array->GetName();
    vtkSmartPointer<vtkDoubleArray> field = vtkSmartPointer<vtkDoubleArray>::New();
    field->SetNumberOfComponents(array->GetNumberOfComponents());
    field->SetName(this->Field_Name.c_str());
    //field->SetNumberOfTuples(array->GetNumberOfTuples());
    int num_of_comp = array->GetNumberOfComponents();
    if(array->GetNumberOfComponents()>1)
    {
        type = Vector;
    }
    else
    {
        type = Scalar;
    }
    for(int i=0;i<array->GetNumberOfTuples();i++)
    {
        double aux[num_of_comp];
        array->GetTuple(i,aux);
        field->InsertNextTuple(aux);
    }
    Solution.push_back(field);
    Magnitud();
    Components();
    selectedComponent=-1; //Magnitud por defecto
}
//Igual al Reader Constructor.
void Field::ReadAll(QString Field_Path)
{
    if(Field_Path.isEmpty())
        return;
    this->Field_Name = Field_Path.mid(Field_Path.lastIndexOf("/") +1).toStdString();
    Lector(Field_Path.toStdString().c_str());
    Magnitud();
    Components();
}
//Lee el campo del ArrayData y lo guarda en la variable Solution.
void Field::Lector(const char* filename)
{
    std::string file = filename;
    //Checkeo que el archivo sea un vtkArrayData. Lo hago asi porque la funcion no esta incorpoarada en el reader
    std::string line = "";
    std::ifstream config_file(filename);
    if(!config_file.is_open())
    {
        return;
    }
    std::getline(config_file, line);
    if(line != "vtkArrayData 1")
    {
        config_file.close();
        return;
    }
    vtkSmartPointer<vtkArrayDataReader> reader = vtkSmartPointer<vtkArrayDataReader>::New();
    reader->SetFileName(file.c_str());
    reader->Update();

    std::vector<vtkSmartPointer<vtkDoubleArray>> output;
    vtkSmartPointer<vtkArrayData> ArrayData = vtkSmartPointer<vtkArrayData>(reader->GetOutput());
    if(ArrayData->GetNumberOfArrays()==0)
        return;
    vtkSmartPointer<vtkArray>  Array = vtkSmartPointer<vtkArray>(ArrayData->GetArray(0));
    int NumberOfTimeSteps =  Array->GetExtent(0).GetSize();
    int NumberOfPoints =  Array->GetExtent(1).GetSize();
    int NumberOfComponents = Array->GetExtent(2).GetSize();
    int i,j,k;
    this->type = Scalar;
    if(NumberOfComponents!=1)
    {
        this->type = Vector;
    }
    for(i=0;i<NumberOfTimeSteps;i++)
    {
      vtkSmartPointer<vtkDoubleArray> field = vtkSmartPointer<vtkDoubleArray>::New();
      field->SetNumberOfComponents(NumberOfComponents);
      field->SetName(this->Field_Name.c_str());
      for(j=0;j<NumberOfPoints;j++)
      {
        double auxiliar[NumberOfComponents];
        for(k=0;k<NumberOfComponents;k++)
        {
          auxiliar[k]=Array->GetVariantValue(i,j,k).ToDouble();
        }
        field->InsertNextTuple(auxiliar);
      }
      output.push_back(field);
    }
    this->Solution = output;
}
//Calcula la Magnitud del campo guardado en Solution y lo guarda en la variable Solution_Magnitud
void Field::Magnitud()
{
    selectedComponent = -1; //Magnitud by default.
    int i,j,k;
    if(Solution.empty())
        return;
    std::vector<vtkSmartPointer<vtkDoubleArray>> magnitud;
    if(Solution[0]->GetNumberOfComponents()==1)
    {
        Solution_Magnitud=Solution;
        return;
    }
    for(i=0;i<(int)this->Solution.size();i++)
    {
        vtkSmartPointer<vtkDoubleArray> auxiliar = vtkSmartPointer<vtkDoubleArray>::New();
        std::string aux_name = this->Field_Name + "Magnitud";
        auxiliar->SetName(aux_name.c_str());
        auxiliar->SetNumberOfComponents(1);
        for(j=0;j<this->Solution[i]->GetNumberOfTuples();j++)
        {
            double mag =0;
            double aux_tuple[3];
            this->Solution[i]->GetTuple(j,aux_tuple);
            for(k=0;k<this->Solution[i]->GetNumberOfComponents();k++)
            {
                mag += aux_tuple[k]*aux_tuple[k];
            }

            mag = std::sqrt(mag);
            auxiliar->InsertNextValue(mag);
        }
        magnitud.push_back(auxiliar);
    }
    this->Solution_Magnitud=magnitud;
}

//Extract the scalar field of each component
void Field::Components()
{
    int i,j,k;
    if(Solution.empty() || Solution[0]->GetNumberOfComponents()==1)
        return;
    std::vector<std::vector<vtkSmartPointer<vtkDoubleArray>>> components;
    for(i=0;i<(int)this->Solution.size();i++)
    {
        std::vector<vtkSmartPointer<vtkDoubleArray>> auxiliar;
        for(k=0;k<(int)Solution[i]->GetNumberOfComponents();k++)
        {
            char a= 'X'+k;
            std::string aux_name = this->Field_Name +"_comp_" +a;
            auxiliar.push_back(vtkSmartPointer<vtkDoubleArray>::New());
            auxiliar[k]->SetName(aux_name.c_str());
            auxiliar[k]->SetNumberOfComponents(1);
        }
        for(j=0;j<this->Solution[i]->GetNumberOfTuples();j++)
        {
            double aux_tuple[Solution[i]->GetNumberOfComponents()];
            this->Solution[i]->GetTuple(j,aux_tuple);
            for(k=0;k<(int)Solution[i]->GetNumberOfComponents();k++)
            {
                auxiliar[k]->InsertNextValue(aux_tuple[k]);
            }
        }
        components.push_back(auxiliar);
    }
    this->Solution_components=components;
}


std::string Field::GetCurrentComponentName()
{
    if(Solution_components.empty())
        return Solution_Magnitud[0]->GetName();
    if(selectedComponent==-1)
        return Solution_Magnitud[0]->GetName();
    else
    {
        return Solution_components[0][selectedComponent]->GetName();
    }
}


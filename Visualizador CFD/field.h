#ifndef FIELD_H
#define FIELD_H

#include <QString>
#include <QObject>
#include <vtkSmartPointer.h>
#include <vtkDoubleArray.h>


/**
    * Definition of type of fields.
    * Solid is implemented when we only want to see the geometry
*/
enum Type_of_Field
{
    Solid,
    Vector,
    Scalar
};
/**
* Base class to handle vector,scalar or just to see the geometry
*/
class Field
{
public:

    //-------------------------------------Atributes-------------------------------------------------
    /**
     * @brief Type of field: Solid,Scalar or Vector
     */
    Type_of_Field type;
    /**
     * @brief Field name. Use to identify the differents fields in a geometry
     */
    std::string Field_Name;
    /**
     * @brief Variables to save the field. Vector or Scalar is the same. Unused in Solids Fields
     */
    std::vector<vtkSmartPointer<vtkDoubleArray>> Solution;
    /**
     * @brief Variable to save the magnitud of the Field save in Solution. Unused in Scalars and Solid Fields
     */
    std::vector<vtkSmartPointer<vtkDoubleArray>> Solution_Magnitud;
    /**
     * @brief Variable to save the components of the Field save in Solution. Unused in Scalars and Solid Fields
     */
    std::vector<std::vector<vtkSmartPointer<vtkDoubleArray>>> Solution_components;
    /**
    * Specify the component to see. If we want to see magnitud the value should be -1.
    */
    int selectedComponent;

    //--------------------------------------Methods--------------------------------------------------
    /**
    * Base constructor
    */
    Field();
    /**
    * Desctructor
    */
    ~Field();
    /**
     * @brief Copy constructor
     * @param Source
     */
    Field(Field *Source);
    /**
     * @brief Reader Constructor
     * @param Field_Path
     */
    Field(QString Field_Path);
    /**
     * @brief Constructor from vtkDoubleArray
     * @param array
     */
    Field(vtkSmartPointer<vtkDoubleArray> array);
    /**
     * @brief Constructor from vtkDataArray
     * @param array
     */
    Field(vtkSmartPointer<vtkDataArray> array);
    /**
     * @brief Implement both Lector and Magnitud functions. It is used in the reader constructor
     * @param Field_Path
     */
    void ReadAll(QString Field_Path);
    /**
     * @brief GetCurrentComponentName
     * @return Return the name of the current component
     */
    std::string GetCurrentComponentName();
private:
    /**
     * @brief Calculate the magnitud of the Solution and save it in Solution_Magnitud
     */
    void Magnitud();
    /**
     * @brief Extract the scalar field of each component
     */
    void Components();
    /**
     * @brief Read the vtkArrayData and save the data in Solution
     * @param filename/path
     */
    void Lector(const char* filename);

};

#endif // FIELD_H

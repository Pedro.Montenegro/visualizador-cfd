#ifndef FILTER_H
#define FILTER_H

#include <QObject>

enum Filter_type
{
    Normal,
    StreamLines,
    VortexFinder
};

//Clase Generica
//Se define para poder usar un unico contenedor en la clase MainWindow
class Filter : public QObject
{
    Q_OBJECT
public:
    Filter();
    ~Filter();

    Filter_type type;
    std::string Filter_Name;

};

#endif // FILTER_H



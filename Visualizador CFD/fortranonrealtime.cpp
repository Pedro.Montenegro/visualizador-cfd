#include "fortranonrealtime.h"

Q_DECLARE_METATYPE(Field*);
Q_DECLARE_METATYPE(std::vector<Field*>);

//Constructors -----------------------------------------------------
FortranOnRealTime::FortranOnRealTime()
{
    //Don't need any initializating.
    PropertiesConfig();
    ParallelReader = new ReaderThreadGPFEP;
    UpdateFlag= true;
}
FortranOnRealTime::FortranOnRealTime(QString CasePath,vtkSmartPointer<vtkRenderWindow> renderWin)
{
    PropertiesConfig();
    this->render_Win = renderWin;
    ParallelReader = new ReaderThreadGPFEP;
    qRegisterMetaType<std::vector<Field*>>();
    UpdateFlag= true;

    SelectedField=0;
    CurrentTimeStep=0;

    ReadCase(CasePath);


    ParametersPropTabs();

}
//End of constructors ----------------------------------------------
//Destructors ------------------------------------------------------
FortranOnRealTime::~FortranOnRealTime()
{
    VisualTimer->stop();
    delete Update_GroupBox;
    delete MaterialParameters_Box;
    delete NumericalParameters_Box;
    delete Outputs_box;
    QObject::connect(ParallelReader,&ReaderThreadGPFEP::finished,ParallelReader,&ReaderThreadGPFEP::deleteLater);
    delete VisualTimer;
}
//End of Destructors -----------------------------------------------
//Functions for reading
void FortranOnRealTime::ReadCase(QString CasePath)
{
    Case_path = CasePath.toStdString();
    this->type = FortranGPFEP;
    Name = CasePath.mid(CasePath.lastIndexOf("/") +1 ).toStdString();
    std::string path = CasePath.toStdString();

    //Reading config file
    std::string file = "gpfep.cfg";
    std::string gpfep = path + "/" + file;


    std::string line = "";
    std::ifstream config_file(gpfep.c_str());

    if(!config_file.is_open())
    {
        std::cout << "fail reading config file" << std::endl;
        return;
    }
    //DIMENSIONES
    while (line != "*SPACE_DIMENSIONS")
    {
        std::getline(config_file, line);
    }
    std::getline(config_file, line);
    num_dimension = std::stoi(line);
    if (num_dimension != 1 && num_dimension != 2 && num_dimension != 3)
    {
        std::cout << "SPACE_DIMENSIONS no tiene un valor correcto" << std::endl;
        return;
    }

    while (line != "*FILES_CONTAINING_MESHES")
    {
        std::getline(config_file, line);
    }
    std::getline(config_file, line);
    std::string vwmPath = path + "/" + std::string(line);
    ReadGeometryVWM(vwmPath);
    Mapper->SetInputData(Grid);
    Mapper->Update();

    while (line != "*INITIAL_CONTINUATION_PARAMETER")
    {
        std::getline(config_file, line);
    }
    std::getline(config_file, line);
    double initial_time = std::stod(line);

    while (line != "*FINAL_CONTINUATION_PARAMETER")
    {
        std::getline(config_file, line);
    }
    std::getline(config_file, line);
    double Final_time = std::stod(line);

    while (line != "*NUMBER_OF_CONTINUATION_STEPS")
    {
        std::getline(config_file, line);
    }
    std::getline(config_file, line);
    double Steps = std::stod(line);

    Time_step = (Final_time-initial_time)/Steps;

    while (line != "*MATERIAL_PARAMETERS")
    {
        std::getline(config_file, line);
    }
    std::getline(config_file, line);
    int num_mat_parameters = std::stoi(line);
    Material_paramaters.resize(num_mat_parameters);
    for(int i=0; i< num_mat_parameters;i++)
    {
        std::getline(config_file, line);
        line = line.substr(0,line.find('!'));
        Material_paramaters[i] = std::stod(line);
    }

    while (line != "*NUMERICAL_PARAMETERS")
    {
        std::getline(config_file, line);
    }
    std::getline(config_file, line);
    int num_numerical_parameters = std::stoi(line);
    Numerical_parameters.resize(num_numerical_parameters);
    for(int i=0; i< num_numerical_parameters;i++)
    {
        std::getline(config_file, line);
        line = line.substr(0,line.find('!')); // Saco los comentarios
        Numerical_parameters[i] = std::stod(line);
    }
    while (line != "*STEPS_BETWEEN_OUTPUTS")
    {
        std::getline(config_file, line);
    }
    std::getline(config_file, line);
    Steps_Between_outputs = std::stoi(line);

    //End reading config file.
    config_file.close();
    GPSteer(Case_path);

    visualPath = path + "/VisualCFD.txt";

    //This lines are for avoiding overwriting by the solver
    std::ifstream    inFile(visualPath);
    std::string newpath = Case_path + "/copyVisualCFD.txt";
    std::ofstream    outFile(newpath);
    outFile << inFile.rdbuf();
    outFile.close();
    inFile.close();
    //------------------
    QObject::connect(ParallelDownStream,&ParallelSignalsDownStream::EndUpdating,this,&FortranOnRealTime::VisualTimerRestart);

    QObject::connect(ParallelReader,&ReaderThreadGPFEP::resultReady,this,&FortranOnRealTime::ChangeFields);
    ParallelReader->SetCase(Case_path,num_dimension,Grid);
    ParallelReader->start();
    //ReadVisualCFD();



    VisualTimer = new QTimer;
    QTimer::connect(VisualTimer,SIGNAL(timeout()),this,SLOT(UpdateRead()));
    RefreshTime = 200; //cada 0.2 segundos por defecto
    Update_RefreshTime_LineEdit->setText("200");
    //VisualTimer->start(RefreshTime);

    QLineEdit::connect(Update_RefreshTime_LineEdit,SIGNAL(editingFinished()),this,SLOT(ChangeRefreshTime()));

}

void FortranOnRealTime::ReadGeometryVWM(std::string vwmPath)
{

    std::string line;
    std::ifstream filestream(vwmPath.c_str());
    if(!filestream.is_open())
        return;

    // Points ------------------------
    vtkSmartPointer<vtkPoints> points =
        vtkSmartPointer<vtkPoints>::New();

    std::getline(filestream, line); // Tomo la linea *Coordenadas
    std::getline(filestream, line); // Numero de nodos
    std::getline(filestream, line);
    if (num_dimension == 1)
    {
        while (line != "" && line != "*ELEMENT_GROUPS")
        {
            int i;
            double x;
            std::stringstream linestream;
            linestream << line;
            linestream >> i >> x;

            points->InsertNextPoint(x, 0, 0);

            std::getline(filestream, line);
        }
    }
    else if (num_dimension == 2)
    {
        while (line != "" && line != "*ELEMENT_GROUPS")
        {
            int i;
            double x, y;
            std::stringstream linestream;
            linestream << line;
            linestream >> i >> x >> y;

            points->InsertNextPoint(x, y, 0);

            std::getline(filestream, line);
        }
    }
    else
    {
        while (line != "" && line != "*ELEMENT_GROUPS")
        {

            int i;
            double x, y, z;
            std::stringstream linestream;
            linestream << line;
            linestream >> i >> x >> y >> z;

            points->InsertNextPoint(x, y, z);

            std::getline(filestream, line);
        }
    }

    Grid->SetPoints(points);
    //End Points -----------
    //Cells ----------------
    while (line != "<NONE>")
    {
        std::getline(filestream, line);
    }

    std::getline(filestream, line);
    if (num_dimension == 1)
    {
        while (line != "" && line != "*END")
        {
            int i, x;
            std::stringstream linestream;
            linestream << line;
            linestream >> i >> x;

            vtkIdType ptIds[] = {i - 1, x - 1};
            Grid->InsertNextCell(VTK_LINE, 2, ptIds);

            std::getline(filestream, line);
        }
    }
    else if (num_dimension == 2)
    {
        while (line != "" && line != "*END")
        {
            int i, x, y;
            std::stringstream linestream;
            linestream << line;
            linestream >> i >> x >> y;

            vtkIdType ptIds[] = {i - 1, x - 1, y - 1, 0};
            Grid->InsertNextCell(VTK_TRIANGLE, 3, ptIds);

            std::getline(filestream, line);
        }
    }
    else
    {
        while (line != "" && line != "*END")
        {
            int i, x, y, z;
            std::stringstream linestream;
            linestream << line;
            linestream >> i >> x >> y >> z;

            vtkIdType ptIds[] = {i - 1, x - 1, y - 1, z - 1};
            Grid->InsertNextCell(VTK_TETRA, 4, ptIds);

            std::getline(filestream, line);
        }
    }
    //End Cells ----

    filestream.close();
}

//Crea el archivo de configuración on runtime
void FortranOnRealTime::GPSteer(std::string casePath)
{
    std::string gpSteerPath = casePath + "/gpsteer.cfg";
    std::string line = "";
    std::ifstream gpSteerReader(gpSteerPath);
    if(!gpSteerReader.is_open())         //Si no existe, lo creo
    {
        Visual_Update_rate=1;
        WriteGPSteer();

    }
    else
    {
        int num_mat_parameters=0, num_numerical_parameters=0;

        //Si esta, lo leo. Agrego lo que le falte y lo reescribo.
        while (line != "*MATERIAL_PARAMETERS")
        {
            std::getline(gpSteerReader, line);
            if(line == "*END")
            {
                goto writeMatParam;
            }
        }
        //Si esta, los leo
        std::getline(gpSteerReader, line);
        num_mat_parameters = std::stoi(line);
        Material_paramaters.resize(num_mat_parameters);
        for(int i=0; i< num_mat_parameters;i++)
        {
            std::getline(gpSteerReader, line);
            line = line.substr(0,line.find('!')); // Saco los comentarios
            /*
            for(unsigned long f =0 ; f<line.length();f++)
            {
                if(line[f] == '.')
                    line[f] = ',';
            }
            */
            Material_paramaters[i] = std::stod(line);
        }

        writeMatParam:
        gpSteerReader.close();
        gpSteerReader.open(gpSteerPath); //Lo cierro y lo abro porque No se en que orden se escribio el archivo.

        //Si esta, lo leo. Agrego lo que le falte
        while (line != "*NUMERICAL_PARAMETERS")
        {
            std::getline(gpSteerReader, line);
            if(line == "*END")
            {
                goto writeNumParam;
            }
        }
        //Si esta, los leo
        std::getline(gpSteerReader, line);

        num_numerical_parameters = std::stoi(line);
        Numerical_parameters.resize(num_numerical_parameters);
        for(int i=0; i< num_numerical_parameters;i++)
        {
            std::getline(gpSteerReader, line);
            line = line.substr(0,line.find('!')); // Saco los comentarios
            /*
            for(unsigned long f =0 ; f<line.length();f++)
            {
                if(line[f] == '.')
                    line[f] = ',';
            }
            */
            Numerical_parameters[i] = std::stod(line);
        }

        writeNumParam:
        gpSteerReader.close();
        gpSteerReader.open(gpSteerPath); //Lo cierro y lo abro porque No se en que orden se escribio el archivo.

        while (line != "*STEPS_BETWEEN_OUTPUTS")
        {
            std::getline(gpSteerReader, line);
            if(line == "*END")
            {
              goto writeSteps;
            }
        }
        std::getline(gpSteerReader, line);
        Steps_Between_outputs = std::stoi(line);


        writeSteps:
        gpSteerReader.close();
        gpSteerReader.open(gpSteerPath); //Lo cierro y lo abro porque No se en que orden se escribio el archivo.


        while (line != "*VISUAL_UPDATE")
        {
            std::getline(gpSteerReader, line);
            if(line == "*END")
            {
                gpSteerReader.close();
                gpSteerReader.open(gpSteerPath); //Lo cierro y lo abro porque No se en que orden se escribio el archivo.
                Visual_Update_rate=1;
                goto writeVisual;
            }
        }
        std::getline(gpSteerReader, line);
        Visual_Update_rate = std::stoi(line);

        writeVisual:

        gpSteerReader.close();
        WriteGPSteer();

    }

}


//Escribe el archivo con la información guardada. Si no hay información, la lee de gpfep.cfg y la copia
void FortranOnRealTime::WriteGPSteer()
{
    mute.lock();
    std::string gpSteerPath = Case_path + "/gpsteer.cfg";
    std::ofstream gpSteer(gpSteerPath);

    //Write TimeStep

    /* Por ahora no
    gpSteer << "*PARCON" << std::endl;
    gpSteer << Time_step << std::endl;
    gpSteer << std::endl;
    */

    //Write STEPS_BETWEEN_OUTPUTS
    gpSteer << "*STEPS_BETWEEN_OUTPUTS" << std::endl;
    gpSteer << Steps_Between_outputs << std::endl;
    gpSteer << std::endl;

    //Write material parameters
    gpSteer << "*MATERIAL_PARAMETERS" << std::endl;
    gpSteer << Material_paramaters.size() << std::endl;
    for(unsigned long i=0; i< Material_paramaters.size() ; i++)
    {
        gpSteer << Material_paramaters[i] << std::endl;
    }
    gpSteer << std::endl;

    //Write numerical parameters
    gpSteer << "*NUMERICAL_PARAMETERS" << std::endl;
    gpSteer << Numerical_parameters.size() << std::endl;
    for(unsigned long i=0; i< Numerical_parameters.size() ; i++)
    {
        gpSteer << Numerical_parameters[i] << std::endl;
    }
    gpSteer << std::endl;

    //Write Visual_Update
    gpSteer << "*VISUAL_UPDATE" << std::endl;
    gpSteer << Visual_Update_rate << std::endl;
    gpSteer << std::endl;

    gpSteer << "*END" << std::endl;



    gpSteer.close();
    mute.unlock();
}

//End of Functions for reading -------------------------------------
//Overriding of time functions -------------------------------------
void FortranOnRealTime::ChangeToThisTime(int i)
{
    //Do nothing. Just and override of Geometry to prevent problems.
}
//End of overriding of time functions ------------------------------
//Output Functions -------------------------------------------------
void FortranOnRealTime::GetOutput(vtkSmartPointer<vtkUnstructuredGrid> GridData)
{
    mute.lock();
    GridData->ShallowCopy(Grid);
    mute.unlock();
}

vtkDataSet *FortranOnRealTime::GetGeoDataOutput()
{
    return Grid;
}
//End of Output functions -------------------------------------------
//Interfase functions -----------------------------------------------
void FortranOnRealTime::PropertiesConfig()
{
    QVBoxLayout *GeoPropLayout = List_Properties[0];
    Update_GroupBox = new QGroupBox;
    Update_GroupBox->setTitle("AutoUpdate");
    Update_GroupBox->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");

    Update_Layout = new QGridLayout(Update_GroupBox);
    Update_CheckBox = new QCheckBox(Update_GroupBox);
    Update_RefreshTime_label = new QLabel(Update_GroupBox);
    Update_RefreshTime_label->setText("Refresh Time [ms]");
    Update_RefreshTime_LineEdit = new QLineEdit(Update_GroupBox);
    Update_RefreshTime_LineEdit->setValidator(new QIntValidator(200,1000000000,Update_RefreshTime_LineEdit));
    Update_CheckBox->setText("Refresh");
    Update_CheckBox->setChecked(true);

    Update_Layout->addWidget(Update_RefreshTime_label,0,0);
    Update_Layout->addWidget(Update_RefreshTime_LineEdit,0,1);
    Update_Layout->addWidget(Update_CheckBox,1,0,1,4);

    Update_GroupBox->setLayout(Update_Layout);
}

void FortranOnRealTime::ParametersPropTabs()
{
    std::vector<QString> materials_names;
    materials_names.push_back("Densidad ref.");
    materials_names.push_back("Viscosidad dinamica ref.");
    materials_names.push_back("Densidad gas");
    materials_names.push_back("Viscosidad dinamica gas");
    materials_names.push_back("Fuerza volumetrica X");
    materials_names.push_back("Fuerza volumetrica Y");
    materials_names.push_back("Fuerza volumetrica Z");
    materials_names.push_back("Prandtl"); //8
    for(auto i=materials_names.size();i<40 ; i++)
        materials_names.push_back("");

    std::vector<QString> numerical_names;
    numerical_names.push_back("");
    numerical_names.push_back("upwf");
    numerical_names.push_back("estab1");
    numerical_names.push_back("estab2");
    numerical_names.push_back("theta (NS)");
    numerical_names.push_back("");
    numerical_names.push_back("");
    numerical_names.push_back("");
    numerical_names.push_back("");
    numerical_names.push_back("theta (transport)");
    numerical_names.push_back("upwf (transport)");
    numerical_names.push_back("rlevel");
    numerical_names.push_back("dismax");
    numerical_names.push_back("hinflow");
    numerical_names.push_back("d2wall");
    numerical_names.push_back("rlx");
    numerical_names.push_back("rly");
    numerical_names.push_back("rlz");
    numerical_names.push_back("Steps bet. reinitial");
    numerical_names.push_back("Imposed vertical surface force");
    numerical_names.push_back("group with vertical force imposed");
    for(auto i=numerical_names.size();i<40 ; i++)
        numerical_names.push_back("");

    MaterialParameters_Box = new QGroupBox;
    MaterialParameters_Box->setTitle("Material Parameters");
    MaterialParameters_Box->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");

    MaterialParameters_Layout = new QFormLayout(MaterialParameters_Box);
    for(unsigned long i=0;i<Material_paramaters.size();i++)
    {
        MaterialParameters_LineEdits.push_back(new QLineEdit(MaterialParameters_Box));
        MaterialParameters_LineEdits[i]->setValidator(new QDoubleValidator(MaterialParameters_LineEdits[i]));
        MaterialParameters_LineEdits[i]->setText(QString::fromStdString(std::to_string(Material_paramaters[i])));
        MaterialParameters_Layout->addRow(materials_names[i],MaterialParameters_LineEdits[i]);
        QLineEdit::connect(MaterialParameters_LineEdits[i],SIGNAL(editingFinished()),this,SLOT(GPSteerChange()));
    }
    MaterialParameters_Box->setLayout(MaterialParameters_Layout);

    NumericalParameters_Box = new QGroupBox;
    NumericalParameters_Box->setTitle("Numerical Parameters");
    NumericalParameters_Box->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");

    NumericalParameters_Layout = new QFormLayout(NumericalParameters_Box);
    for(unsigned long i=0;i<Numerical_parameters.size();i++)
    {
        NumericalParameters_LineEdits.push_back(new QLineEdit(NumericalParameters_Box));
        NumericalParameters_LineEdits[i]->setValidator(new QDoubleValidator(NumericalParameters_LineEdits[i]));
        NumericalParameters_LineEdits[i]->setText(QString::fromStdString(std::to_string(Numerical_parameters[i])));
        NumericalParameters_Layout->addRow(numerical_names[i],NumericalParameters_LineEdits[i]);
        QLineEdit::connect(NumericalParameters_LineEdits[i],SIGNAL(editingFinished()),this,SLOT(GPSteerChange()));
    }
    NumericalParameters_Box->setLayout(NumericalParameters_Layout);

    Outputs_box = new QGroupBox;
    Outputs_box->setTitle("Outputs");
    Outputs_box->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");

    Outputs_layout = new QFormLayout(Outputs_box);
    Visual_lineEdit = new QLineEdit(Outputs_box);
    Visual_lineEdit->setValidator(new QIntValidator(0,1000000,Visual_lineEdit));
    Visual_lineEdit->setText(QString::fromStdString(std::to_string(Visual_Update_rate)));
    Steps_output_lineEdit = new QLineEdit(Outputs_box);
    Steps_output_lineEdit->setValidator(new QIntValidator(0,1000000,Steps_output_lineEdit));
    Steps_output_lineEdit->setText(QString::fromStdString(std::to_string(Steps_Between_outputs)));

    Outputs_layout->addRow("Steps for visual output",Visual_lineEdit);
    Outputs_layout->addRow("Steps between outputs",Steps_output_lineEdit);

    QLineEdit::connect(Visual_lineEdit,SIGNAL(editingFinished()),this,SLOT(GPSteerChange()));
    QLineEdit::connect(Steps_output_lineEdit,SIGNAL(editingFinished()),this,SLOT(GPSteerChange()));
    QCheckBox::connect(Update_CheckBox,SIGNAL(released()),this,SLOT(AutoUpdate()));

    QVBoxLayout *Gpfep_Parameters = new QVBoxLayout;
    Gpfep_Parameters->addWidget(Update_GroupBox);
    Gpfep_Parameters->addWidget(Outputs_box);
    Gpfep_Parameters->addWidget(MaterialParameters_Box);
    Gpfep_Parameters->addWidget(NumericalParameters_Box);
    AddFilterProperties(Gpfep_Parameters);
    Properties_names.push_back("GPFEP parameters");
}
//End of Interfase functions ----------------------------------------
//Public slots/Update functions -------------------------------------
void FortranOnRealTime::UpdateRead()
{
    mute.lock();
    std::string line;
    std::ifstream FieldFile(visualPath);

    if(!FieldFile.is_open())
    {
        mute.unlock();
        return;
    }

    std::string aux_time;
    double testTime;

    while(aux_time != "Continuation parameter:")
    {
        if(!std::getline(FieldFile, line))
        {
            mute.unlock();
            return;
        }
        if(line.find("Continuation parameter:") != std::string::npos)
        {
            aux_time = line.substr(0,23);
            try
            {
                testTime = std::stod(line.substr(24));
            }
            catch (const std::invalid_argument&)
            {
                mute.unlock();
                return;
            }
        }
    }

    FieldFile.close();

    if(testTime==CurrentTime)
    {
        mute.unlock();
        return;
    }

    VisualTimer->stop();
    CurrentTime=testTime;
    //This lines are for avoiding overwriting by the solver

    std::string newpath = Case_path + "/copyVisualCFD.txt";
    try {
        std::ifstream    inFile(visualPath);
        std::ofstream    outFile(newpath);
        outFile << inFile.rdbuf();
        outFile.close();
        inFile.close();
    }  catch (...) {
        mute.unlock();
        return;
    }

    ParallelReader->start();
    mute.unlock();

}
void FortranOnRealTime::AutoUpdate()
{
    mute.lock();
    if(!Update_CheckBox->isChecked())
    {
        VisualTimer->stop();
        emit PlayAndStop(false);
        UpdateFlag = false;
    }
    else
    {
        VisualTimer->start(RefreshTime);
        emit PlayAndStop(true);
        UpdateFlag = true;
    }
    mute.unlock();
}

void FortranOnRealTime::ChangeRefreshTime()
{
    mute.lock();
    if(Update_RefreshTime_LineEdit->text().isEmpty())
    {
        mute.unlock();
        return;
    }
    RefreshTime = std::stod(Update_RefreshTime_LineEdit->text().toStdString());
    if(UpdateFlag)
    {
        VisualTimer->start(RefreshTime);
    }
    mute.unlock();
}
void FortranOnRealTime::GPSteerChange()
{
    mute.lock();
    if(!Visual_lineEdit->text().isEmpty())
        Visual_Update_rate = std::stoi(Visual_lineEdit->text().toStdString());
    if(!Steps_output_lineEdit->text().isEmpty())
        Steps_Between_outputs = std::stoi(Steps_output_lineEdit->text().toStdString());
    for(unsigned long i=0;i<MaterialParameters_LineEdits.size();i++)
        if(!MaterialParameters_LineEdits[i]->text().isEmpty())
            Material_paramaters[i] = std::stod(MaterialParameters_LineEdits[i]->text().toStdString());
    for(unsigned long i=0;i<NumericalParameters_LineEdits.size();i++)
        if(!NumericalParameters_LineEdits[i]->text().isEmpty())
            Numerical_parameters[i] = std::stod(NumericalParameters_LineEdits[i]->text().toStdString());
    mute.unlock();
    WriteGPSteer();

}
void FortranOnRealTime::ChangeUpdate_CheckBox(bool value)
{
    mute.lock();
    if(value)
    {
        Update_CheckBox->setCheckState(Qt::Checked);
    }
    else
    {
        Update_CheckBox->setCheckState(Qt::Unchecked);
    }
    UpdateFlag = value;
    mute.unlock();
    AutoUpdate();

}
void FortranOnRealTime::ChangeFields(std::vector<Field *> new_Fields_List)
{
    mute.lock();

    //Primero comparo los nombres buscando igualdades para machear los componentes
    int ind;
    if(new_Fields_List.empty() && UpdateFlag)
    {
        VisualTimer->start(RefreshTime);
        mute.unlock();
        return;
    }

    try {
        std::vector<Field *> aux_field_list;
        for(unsigned long i=0; i<new_Fields_List.size();i++)
        {
            ind=0;
            for(unsigned long j=0;j<Field_list.size();j++)
            {
                if(new_Fields_List[i]->Field_Name == Field_list[j]->Field_Name)
                {
                    new_Fields_List[i]->selectedComponent = Field_list[j]->selectedComponent;
                    delete Field_list[j];
                    Field_list[j] = new Field(new_Fields_List[i]);
                    ind =1;
                    break;
                }
            }
            if(ind==0)
            {
                Field_list.push_back(new_Fields_List[i]);
            }

        }

        for(unsigned long i=0; i<Field_list.size(); i++)
        {
            AddFields(i);
        }

        SetVectors();
        Mapper->Update();
        SendSignalsDownStream();

        this->Times->SetValue(0,CurrentTime);
    }  catch (...) {
        mute.unlock();
        if(new_Fields_List.empty() && UpdateFlag)
        {
            VisualTimer->start(RefreshTime);
            return;
        }
    }
    emit UpdateInterfaseSignal();

    mute.unlock();


}
void FortranOnRealTime::VisualTimerRestart()
{
    mute.lock();
    render_Win->Render();
    if(UpdateFlag)
    {
        VisualTimer->start(RefreshTime);
    }
    mute.unlock();
}
//End of Public slots/Update functions ----------------------------------

//-----------------------ReaderThreadGPFEP ------------------------------

void ReaderThreadGPFEP::run()
{
    try {
        ReadVisualCFD();
        emit resultReady(new_Field_list);
    }  catch (...) {
        std::vector<Field*> n; //vacio. Vuelve a empezar
        emit resultReady(n);
    }

}

void ReaderThreadGPFEP::ReadVisualCFD()
{
    //initialize

    int NUM_FIELDS = 6 + num_dimension * 2;

    vtkSmartPointer<vtkDoubleArray> velocity = vtkSmartPointer<vtkDoubleArray>::New();
    velocity->SetName("Velocity");
    velocity->SetNumberOfComponents(3);

    vtkSmartPointer<vtkDoubleArray> Presure = vtkSmartPointer<vtkDoubleArray>::New();
    Presure->SetName("Presure");
    Presure->SetNumberOfComponents(1);

    vtkSmartPointer<vtkDoubleArray> Presure_Gradient = vtkSmartPointer<vtkDoubleArray>::New();
    Presure_Gradient->SetName("Presure_Gradient");
    Presure_Gradient->SetNumberOfComponents(3);

    vtkSmartPointer<vtkDoubleArray> Temperature = vtkSmartPointer<vtkDoubleArray>::New();
    Temperature->SetName("Temperature");
    Temperature->SetNumberOfComponents(1);

    std::string line;
    //---------------------
    std::string newpath = Case_path + "/copyVisualCFD.txt";
    std::ifstream FieldFile(newpath);
    if(!FieldFile.is_open())
        return;

    std::string aux_time;
    while(aux_time != "Continuation parameter:")
    {
        std::getline(FieldFile, line);
        if(line.find("Continuation parameter:") != std::string::npos)
        {
            aux_time = line.substr(0,23);

            try
            {
                CurrentTime = std::stod(line.substr(24));
            }
            catch (const std::invalid_argument&)
            {
                return;
            }
        }
    }

    while (line != "<NONE>")
    {
        std::getline(FieldFile, line);
    }
    int count = 0;
    int index = 0;
    double au[3] = {0, 0, 0};

    while (std::getline(FieldFile, line, '\n'))
    {
        if(line == "*END")
        {
            break;
        }
        double z;
        std::stringstream linestream;
        linestream << line;
        linestream >> z;
        if (count < num_dimension)
        {
            au[count] = z;
            if (count == (num_dimension - 1))
            {
                velocity->InsertNextTuple(au);
            }
        }
        else if (count == num_dimension)
        {
            Presure->InsertNextValue(z);
        }
        else if (count > num_dimension && count < (num_dimension * 2 + 1))
        {
            au[count - num_dimension - 1] = z;
            if (count == num_dimension * 2)
            {
                Presure_Gradient->InsertNextTuple(au);
            }
        }
        else if(count == num_dimension*2+5)
        {
            Temperature->InsertNextValue(z);
        }
        count++;
        if(count == NUM_FIELDS)
        {
            count=0;
            index++;
        }
    }

    //This is necesary because how the interfase works, I should remove it in the future.
    //Its also calculate the magnitud, wich is usefull for coloring.
    if(Presure->GetNumberOfTuples() != Grid->GetNumberOfPoints())
        return;
    if(velocity->GetNumberOfTuples() != Grid->GetNumberOfPoints())
        return;
    if(Presure_Gradient->GetNumberOfTuples() != Grid->GetNumberOfPoints())
        return;
    if(Temperature->GetNumberOfTuples() != Grid->GetNumberOfPoints())
        return;

    new_Field_list.clear();

    new_Field_list.push_back(new Field(velocity));
    new_Field_list.push_back(new Field(Presure));
    new_Field_list.push_back(new Field(Presure_Gradient));
    new_Field_list.push_back(new Field(Temperature));

    FieldFile.close();

    // Code to execute


}

void ReaderThreadGPFEP::SetCase(std::string path, int dimension, vtkUnstructuredGrid *CaseGrid)
{
    this->Case_path=path;
    visualPath = path + "/VisualCFD.txt"; //No estoy usando esto
    num_dimension=dimension;
    Grid = CaseGrid;
    qRegisterMetaType<std::vector<Field*>>();


}

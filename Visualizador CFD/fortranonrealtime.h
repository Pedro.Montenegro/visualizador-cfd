#ifndef FORTRANONREALTIME_H
#define FORTRANONREALTIME_H

#include <QObject>
#include "geometry.h"

#include <QTimer>
#include <QCheckBox>
#include <QFormLayout>

#include <sstream>
#include <iostream>
/**
 * @brief ReaderThreadGPFEP. clase creada para leer en paralelo durante la visualización online
 */
class ReaderThreadGPFEP: public QThread
{
    Q_OBJECT

public:
    /**
     * @brief función corrida por el thread
     */
    void run() override;
    /**
     * @brief Case_path
     */
    std::string Case_path;
    /**
     * @brief visualPath
     */
    std::string visualPath;
    /**
     * @brief Lista de campos leidos a emitir.
     */
    std::vector<Field*> new_Field_list;
    /**
     * @brief numero de dimensiones.
     */
    int num_dimension;
    /**
     * @brief pointer to grid. It is used to check that the number of values of a field and the number of points are the same
     */
    vtkUnstructuredGrid* Grid;
    /**
     * @brief Función de lectura de campos.
     */
    void ReadVisualCFD();
    /**
     * @brief SetCase. Configura el reader thread.
     * @param path
     * @param dimension
     * @param CaseGrid
     */
    void SetCase(std::string path,int dimension,vtkUnstructuredGrid *CaseGrid);
    /**
     * @brief CurrentTime
     */
    double CurrentTime;    
signals:
    /**
     * @brief resultReady. Emite los resultados para el procesador principal
     * @param new_Field_list
     */
    void resultReady(std::vector<Field*> new_Field_list);
};

/**
 * @brief The FortranOnRealTime class. Class for conecting with GPFEP on run-time
 */
class FortranOnRealTime : public Geometry
{
    Q_OBJECT
public:
    //Constructors
    FortranOnRealTime();
    /**
     * @brief CaseConstructor.
     * @param CasePath
     * @param renderWin
     */
    FortranOnRealTime(QString CasePath,vtkSmartPointer<vtkRenderWindow> renderWin);
    //Destructors
    ~FortranOnRealTime();
    /**
     * @brief RefreshTime. Time between the check for a new time step
     */
    unsigned long RefreshTime;
    /**
     * @brief CurrentTime of the simulation
     */
    double CurrentTime;
    /**
     * @brief Path to visualCFD.txt file
     */
    std::string visualPath;
    /**
     * @brief VisualTimer. Clocked used to call the reader thread.
     */
    QTimer *VisualTimer;
    /**
     * @brief UpdateFlag
     */
    bool UpdateFlag;
    /**
     * @brief num_dimension
     */
    int num_dimension;

    //Functions
    /**
     * @brief ReadCase. Use when initializated
     * @param CasePath
     */
    void ReadCase(QString CasePath);
    /**
     * @brief Clase implementada para leer los campos en paralelo
     */
    ReaderThreadGPFEP *ParallelReader;

    //GPFEP parameters
    /**
     * @brief Material_paramaters array
     */
    std::vector<double> Material_paramaters;
    /**
     * @brief Numerical_parameters array
     */
    std::vector<double> Numerical_parameters;
    /**
     * @brief Steps_Between_outputs
     */
    int Steps_Between_outputs;
    /**
     * @brief Time_step. Readed but the change in the timestep is not implemented.
     */
    double Time_step;
    /**
     * @brief Numero de pasos calculados entre los output visuales de GPFEP. Es adaptable por el usuario
     * Un valor negativo implica que no hay output visual.
     */
    int Visual_Update_rate;
    /**
     * @brief Case_path
     */
    std::string Case_path;

    //TimeFunctions. Overrides to do nothing. Only see one time.
    /**
     * @brief Se sobreescribe esta función para evitar errores.
     * @param i
     */
    void ChangeToThisTime(int i) override;
    /**
     * @brief GetOutput
     * @param copy the output in GridData
     */
    void GetOutput(vtkSmartPointer<vtkUnstructuredGrid> GridData) override;
    /**
     * @brief GetGeoDataOutput
     * @return Grid with the data
     */
    vtkDataSet* GetGeoDataOutput() override;

    QCheckBox *Update_CheckBox;
private:
    //Interfase variables
    QGroupBox *Update_GroupBox;
    QGridLayout *Update_Layout;
    QLabel *Update_RefreshTime_label;
    QLineEdit *Update_RefreshTime_LineEdit;

    QGroupBox *MaterialParameters_Box;
    QFormLayout *MaterialParameters_Layout;
    std::vector<QLineEdit*> MaterialParameters_LineEdits;

    QGroupBox *NumericalParameters_Box;
    QFormLayout *NumericalParameters_Layout;
    std::vector<QLineEdit*> NumericalParameters_LineEdits;

    QGroupBox *Outputs_box;
    QFormLayout *Outputs_layout;
    QLineEdit *Visual_lineEdit;
    QLineEdit *Steps_output_lineEdit;


    //Interfase functions
    /**
     * @brief Config properties.
     */
    void PropertiesConfig(); //Add properties to Geometries properties.
    /**
     * @brief Configura la pestaña de propiedades de los parametros
     */
    void ParametersPropTabs(); //Add tabs for Materials and NUmerical parameters
    /**
     * @brief ReadGeometry
     * @param vwmPath. Path to geometry file
     */
    void ReadGeometryVWM(std::string vwmPath);
    /**
     * @brief Lee el achivo gpsteer y si no existe lo crea.
     * @param casePath
     */
    void GPSteer(std::string casePath);
    /**
     * @brief Escribe el archivo gpsteer. Permite cambiar los parametros desde la interfaz.
     */
    void WriteGPSteer();
private slots:
    /**
     * @brief Check if a new output exist. If exist, start the reader thread for reading it.
     */
    void UpdateRead(); //calls ReadVisualCFD if something has changed
    /**
     * @brief Change the updateflagValue and the plat and stop button on the interfase
     */
    void AutoUpdate(); //disconnect or connect Timer With UpdateRead
    /**
     * @brief Change the refresh time. (Time for the check of a new output from the solver)
     */
    void ChangeRefreshTime();
    /**
     * @brief Read the input parametros from the user and re-write the gpster.cfg file
     */
    void GPSteerChange(); // Update gpsteer.cfg after lineedit change
    /**
     * @brief Function call by the reader thread when the new fields are ready. It load this fields to the Grid.
     * @param new_field_list
     */
    void ChangeFields(std::vector<Field*> new_field_list);
    /**
     * @brief Restart the clock is the updateFlag is on.
     */
    void VisualTimerRestart();

public slots:
    /**
     * @brief Slot for changing the checkbox value after the play button on the interace is pressed/realesed
     * @param value
     */
    void ChangeUpdate_CheckBox(bool value);

signals:
    /**
     * @brief signal por pressing/releaseing the play button in the interface
     * @param True = press. False = realese
     */
    void PlayAndStop(bool a);
};

#endif // FORTRANONREALTIME_H

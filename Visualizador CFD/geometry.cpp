﻿#include "geometry.h"

#include <vtkXMLUnstructuredGridReader.h>

//Constructos and destructors ------------------------------------
//Base constructor
Geometry::Geometry()
{
    Grid = vtkSmartPointer<vtkUnstructuredGrid>::New();
    Mapper = vtkSmartPointer<vtkDataSetMapper>::New();
    Mapper->ScalarVisibilityOn();
    Actor = vtkSmartPointer<vtkActor>::New();
    type = Filter_type::Normal;
    UseScalarBar = false;
    Times =vtkSmartPointer<vtkDoubleArray>::New();
    Times->SetName("TIME");
    Times->InsertNextValue(0);

    Actor->SetMapper(Mapper);
    edgeVisibility=false;
    CreateGeometryConfig(); //inicia y configura la pestaña de propiedades
    Input = nullptr;
    ParallelDownStream = new ParallelSignalsDownStream;
}

//Reader constructor
Geometry::Geometry(QString FilePath)
{
    type = Filter_type::Normal;

    Grid = vtkSmartPointer<vtkUnstructuredGrid>::New();
    Mapper = vtkSmartPointer<vtkDataSetMapper>::New();
    Mapper->ScalarVisibilityOn();
    Actor = vtkSmartPointer<vtkActor>::New();
    edgeVisibility=false;


    CreateGeometryConfig(); //inicia y configura la pestaña de propiedades


    vtkSmartPointer<vtkXMLUnstructuredGridReader> reader = vtkSmartPointer<vtkXMLUnstructuredGridReader>::New();

    if(FilePath.isEmpty())
        return;
    if(!reader->CanReadFile(FilePath.toStdString().c_str()))
    {
        return;
    }
    reader->SetFileName(FilePath.toStdString().c_str());
    reader->Update();

    Grid->ShallowCopy(reader->GetOutput());

    if(Grid->GetFieldData()->GetNumberOfArrays()!=0)
    {
        Times->ShallowCopy(Grid->GetFieldData()->GetArray(0));
    }
    Mapper->SetInputData(Grid);
    Mapper->Update();
    Actor->SetMapper(Mapper);

    CurrentTimeStep=0;
    SelectedField=0;

    Name = FilePath.mid(FilePath.lastIndexOf("/") +1 ).toStdString();

    Field *Solid = new Field;
    Solid->Field_Name = "Solid";
    Solid->type = Type_of_Field::Solid;

    Field_list.push_back(Solid);
    ChangeField("Solid");

    ParallelDownStream = new ParallelSignalsDownStream;
}
//Destructor
Geometry::~Geometry()
{
    delete Geometry_transform_GroupBox;
    delete Opacity_box;
    unsigned long i;
    for(i=0;i<Field_list.size();i++)
            delete Field_list[i]; // Tal vez deberia ir un -1
    Field_list.clear();

    for(i=0;i<List_Properties.size();i++)
    {
        delete List_Properties[i];
    }
    List_Properties.clear();
    delete ParallelDownStream;

}
//End of constructors and destructors ----------------------------

//Functions of time change----------------------------------------
//Especific time
void Geometry::ChangeToThisTime(int i)
{
    if(i == (int)CurrentTimeStep)
        return;
    CurrentTimeStep = i;
    if(Field_list.size()==0 || i<0 || i>=(int)Field_list[SelectedField]->Solution.size())
        return;
    for(int j=1; j<(int)Field_list.size();j++)
    {
        AddFields(j);
    }
    SetVectors();
    //SetScalar();
    SendSignalsDownStream();

}
//End of functions of time change --------------------------------
//Functions over fields ------------------------------------------
//Insert new field
bool Geometry::InsertNewField(QString Field_Name)
{
    if(Field_Name.isEmpty())
        return false;
    Field *New_solution = new Field;
    New_solution->ReadAll(Field_Name);
    if(New_solution->Solution.empty() || New_solution->Solution[0]->GetNumberOfTuples() != Grid->GetNumberOfPoints())
    {
        delete New_solution;
        return false;
    }
    Field_list.push_back(New_solution);
    SelectedField = Field_list.size()-1;
    AddFields(SelectedField);
    SetVectors();
    SendSignalsDownStream();
    return true;
}

//Change the selected field for the input field
void Geometry::ChangeField(std::string field_name)
{
    unsigned long i=0;
    for(i=0;i<Field_list.size();i++)
    {
        if(field_name == Field_list[i]->Field_Name)
            break;
    }

    if(i == Field_list.size())
        return;
    SelectedField = i;
    if(Field_list[SelectedField]->Solution.size()<CurrentTimeStep+1 && Field_list[SelectedField]->type != Solid)
        CurrentTimeStep=0;

    SetVectors();
}

void Geometry::ChangeComponent(std::string component_name)
{
    if(Field_list.empty())
        return;
    if(component_name.find("Magnitud")!= std::string::npos)
    {
        Field_list[SelectedField]->selectedComponent =-1;
        SetScalar();
    }
    else if(component_name[0]>='X')
    {
        Field_list[SelectedField]->selectedComponent = component_name[0]-'X';
        SetScalar();
    }
}

void Geometry::SetVectors()
{
    if(Field_list.empty())
        return;
    if(Field_list[SelectedField]->type == Vector)
    {
        Grid->GetPointData()->SetActiveVectors(Field_list[SelectedField]->Field_Name.c_str());
    }
    else if(Field_list[SelectedField]->type == Scalar)
    {

    }
    else
    {

        Mapper->ScalarVisibilityOff();
        Mapper->Update();
        if(UseScalarBar)
        {
            ScalarBar->VisibilityOff();
            ScalarWidget->Off();
        }
        return;
    }
    SetScalar();

}
void Geometry::SetScalar()
{

    if(Field_list.size()==0)
        return;
    if(Field_list[SelectedField]->type==Solid)
        return;

    Mapper->ScalarVisibilityOn();
    Grid->GetPointData()->SetActiveScalars(Field_list[SelectedField]->GetCurrentComponentName().c_str());

    if(Grid->GetPointData()->GetNumberOfArrays()!=0 && AutoScale_Checkbox->isChecked())
    {
        Mapper->SetScalarRange(Grid->GetPointData()->GetScalars()->GetRange());
    }

    if(UseScalarBar)
    {

        ScalarBar->SetTitle(Field_list[SelectedField]->GetCurrentComponentName().c_str());

        if(Actor->GetVisibility())
        {
            ScalarBar->VisibilityOn();
            ScalarWidget->On();
            ScalarBar->SetLookupTable(Mapper->GetLookupTable());
        }
    }

    Mapper->Update();
}
void Geometry::AddFields(int Field_number)
{
    if(Field_list.empty())
        return;
    if(Field_list.size()<=(unsigned long)Field_number)
        return;
    if(Field_list[Field_number]->Solution.size()>CurrentTimeStep)
    {

        Grid->GetPointData()->AddArray(Field_list[Field_number]->Solution[CurrentTimeStep]);
        Grid->GetPointData()->AddArray(Field_list[Field_number]->Solution_Magnitud[CurrentTimeStep]);

        if(!Field_list[Field_number]->Solution_components.empty())
        {
            for(int i=0; i<(int)Field_list[Field_number]->Solution_components[CurrentTimeStep].size();i++)
            {
                Grid->GetPointData()->AddArray(Field_list[Field_number]->Solution_components[CurrentTimeStep][i]);
            }
        }
    }
    else
    {
        Grid->GetPointData()->AddArray(Field_list[Field_number]->Solution[0]);
        Grid->GetPointData()->AddArray(Field_list[Field_number]->Solution_Magnitud[0]);

        if(!Field_list[Field_number]->Solution_components.empty())
        {
            for(int i=0; i<(int)Field_list[Field_number]->Solution_components[0].size();i++)
            {
                Grid->GetPointData()->AddArray(Field_list[Field_number]->Solution_components[0][i]);
            }
        }
    }


}
//End of Functions over fields -----------------------------------

//Functions that change or update visualization-------------------
//Rescale to the visible data range
void Geometry::ReScaleToCurrentData()
{
    if(Field_list.empty())
        return;
    if(Grid->GetPointData()->GetNumberOfArrays() ==0)
        return;
    if(Field_list[SelectedField]->type ==Solid)
        return;
    if(Field_list[SelectedField]->Solution.size()!=1)
    {
        if(Field_list[SelectedField]->selectedComponent==-1)
        {
            Mapper->SetScalarRange(Field_list[SelectedField]->Solution_Magnitud[CurrentTimeStep]->GetRange());
        }
        else
        {
            int selec = Field_list[SelectedField]->selectedComponent;
            Mapper->SetScalarRange(Field_list[SelectedField]->Solution_components[CurrentTimeStep][selec]->GetRange());
        }
    }
    else if(Field_list[SelectedField]->Solution.size()==1)
    {
        if(Field_list[SelectedField]->selectedComponent==-1)
        {
            Mapper->SetScalarRange(Field_list[SelectedField]->Solution_Magnitud[0]->GetRange());
        }
        else
        {
            int selec = Field_list[SelectedField]->selectedComponent;
            Mapper->SetScalarRange(Field_list[SelectedField]->Solution_components[0][selec]->GetRange());
        }
    }

    Mapper->Update();
}
//Rescale to a custom range // Implementada aca pero no incorporada al programa
void Geometry::ReScaleToSetRange(double a, double b)
{
    Mapper->SetScalarRange(a,b);
    Mapper->Update();
}
//Update the mapper
void Geometry::Update()
{
    Mapper->Update();
}
//End of Functions that change or update visualization------------

//End of Functions use in the properties--------------------------

//Functions of the properties-------------------------------------
void Geometry::CreateGeometryConfig()
{

    Geometry_transform_GroupBox = new QGroupBox;
    Geometry_transform_layout = new QGridLayout(Geometry_transform_GroupBox);
    //posicion
    XCenterLabel = new QLabel(Geometry_transform_GroupBox);
    YCenterLabel = new QLabel(Geometry_transform_GroupBox);
    ZCenterLabel = new QLabel(Geometry_transform_GroupBox);
    Center_Geometry = new QLabel(Geometry_transform_GroupBox);
    XCenterLine_Edit = new QLineEdit(Geometry_transform_GroupBox);
    YCenterLine_Edit = new QLineEdit(Geometry_transform_GroupBox);
    ZCenterLine_Edit = new QLineEdit(Geometry_transform_GroupBox);
    //Scale
    XScaleLabel = new QLabel(Geometry_transform_GroupBox);
    YScaleLabel = new QLabel(Geometry_transform_GroupBox);
    ZScaleLabel = new QLabel(Geometry_transform_GroupBox);
    Scale_Geometry = new QLabel(Geometry_transform_GroupBox);
    XScaleLine_Edit = new QLineEdit(Geometry_transform_GroupBox);
    YScaleLine_Edit = new QLineEdit(Geometry_transform_GroupBox);
    ZScaleLine_Edit = new QLineEdit(Geometry_transform_GroupBox);
    //Angle
    XAngleLabel = new QLabel(Geometry_transform_GroupBox);
    YAngleLabel = new QLabel(Geometry_transform_GroupBox);
    ZAngleLabel = new QLabel(Geometry_transform_GroupBox);
    Angle_Geometry = new QLabel(Geometry_transform_GroupBox);
    XAngleLine_Edit = new QLineEdit(Geometry_transform_GroupBox);
    YAngleLine_Edit = new QLineEdit(Geometry_transform_GroupBox);
    ZAngleLine_Edit = new QLineEdit(Geometry_transform_GroupBox);
    //Transform Layout config-------------------------------------
    //Posicion
    Center_Geometry->setText("Translate:");
    XCenter = 0;
    XCenterLabel->setText("x");
    XCenterLine_Edit->setValidator(new QDoubleValidator(-100000,100000,4,Geometry_transform_GroupBox));
    XCenterLine_Edit->setText("0");
    YCenter = 0;
    YCenterLabel->setText("y");
    YCenterLine_Edit->setValidator(new QDoubleValidator(-100000,100000,4,Geometry_transform_GroupBox));
    YCenterLine_Edit->setText("0");
    ZCenter = 0;
    ZCenterLabel->setText("z");
    ZCenterLine_Edit->setValidator(new QDoubleValidator(-100000,100000,4,Geometry_transform_GroupBox));
    ZCenterLine_Edit->setText("0");

    QLineEdit::connect(XCenterLine_Edit,SIGNAL(textChanged(QString)),this,SLOT(UpdateXCenter(QString)));
    QLineEdit::connect(YCenterLine_Edit,SIGNAL(textChanged(QString)),this,SLOT(UpdateYCenter(QString)));
    QLineEdit::connect(ZCenterLine_Edit,SIGNAL(textChanged(QString)),this,SLOT(UpdateZCenter(QString)));

    //Scale
    Scale_Geometry->setText("Scale:");
    XScale = 1;
    XScaleLabel->setText("x");
    XScaleLine_Edit->setValidator(new QDoubleValidator(0,10000000,4,Geometry_transform_GroupBox));
    XScaleLine_Edit->setText("1");
    YScale = 1;
    YScaleLabel->setText("y");
    YScaleLine_Edit->setValidator(new QDoubleValidator(0,10000000,4,Geometry_transform_GroupBox));
    YScaleLine_Edit->setText("1");
    ZScale = 1;
    ZScaleLabel->setText("z");
    ZScaleLine_Edit->setValidator(new QDoubleValidator(0,10000000,4,Geometry_transform_GroupBox));
    ZScaleLine_Edit->setText("1");

    QLineEdit::connect(XScaleLine_Edit,SIGNAL(textChanged(QString)),this,SLOT(UpdateXScale(QString)));
    QLineEdit::connect(YScaleLine_Edit,SIGNAL(textChanged(QString)),this,SLOT(UpdateYScale(QString)));
    QLineEdit::connect(ZScaleLine_Edit,SIGNAL(textChanged(QString)),this,SLOT(UpdateZScale(QString)));

    //Angle
    Angle_Geometry->setText("Angle:");
    XAngle = 0;
    XAngleLabel->setText("x");
    XAngleLine_Edit->setValidator(new QDoubleValidator(-10000,10000,4,Geometry_transform_GroupBox));
    XAngleLine_Edit->setText("0");
    YAngle = 0;
    YAngleLabel->setText("y");
    YAngleLine_Edit->setValidator(new QDoubleValidator(-10000,10000,4,Geometry_transform_GroupBox));
    YAngleLine_Edit->setText("0");
    ZAngle = 0;
    ZAngleLabel->setText("z");
    ZAngleLine_Edit->setValidator(new QDoubleValidator(-10000,10000,4,Geometry_transform_GroupBox));
    ZAngleLine_Edit->setText("0");

    QLineEdit::connect(XAngleLine_Edit,SIGNAL(textChanged(QString)),this,SLOT(RotateX(QString)));
    QLineEdit::connect(YAngleLine_Edit,SIGNAL(textChanged(QString)),this,SLOT(RotateY(QString)));
    QLineEdit::connect(ZAngleLine_Edit,SIGNAL(textChanged(QString)),this,SLOT(RotateZ(QString)));

    //Adding to QGridLayout and to the QGroupBox
    //Position
    Geometry_transform_layout->addWidget(Center_Geometry,0,0);
    Geometry_transform_layout->addWidget(XCenterLabel,0,1);
    Geometry_transform_layout->addWidget(XCenterLine_Edit,0,2);
    Geometry_transform_layout->addWidget(YCenterLabel,0,3);
    Geometry_transform_layout->addWidget(YCenterLine_Edit,0,4);
    Geometry_transform_layout->addWidget(ZCenterLabel,0,5);
    Geometry_transform_layout->addWidget(ZCenterLine_Edit,0,6);

    //Scale
    Geometry_transform_layout->addWidget(Scale_Geometry,1,0);
    Geometry_transform_layout->addWidget(XScaleLabel,1,1);
    Geometry_transform_layout->addWidget(XScaleLine_Edit,1,2);
    Geometry_transform_layout->addWidget(YScaleLabel,1,3);
    Geometry_transform_layout->addWidget(YScaleLine_Edit,1,4);
    Geometry_transform_layout->addWidget(ZScaleLabel,1,5);
    Geometry_transform_layout->addWidget(ZScaleLine_Edit,1,6);

    //Angle
    Geometry_transform_layout->addWidget(Angle_Geometry,2,0);
    Geometry_transform_layout->addWidget(XAngleLabel,2,1);
    Geometry_transform_layout->addWidget(XAngleLine_Edit,2,2);
    Geometry_transform_layout->addWidget(YAngleLabel,2,3);
    Geometry_transform_layout->addWidget(YAngleLine_Edit,2,4);
    Geometry_transform_layout->addWidget(ZAngleLabel,2,5);
    Geometry_transform_layout->addWidget(ZAngleLine_Edit,2,6);

    Geometry_transform_GroupBox->setLayout(Geometry_transform_layout);
    Geometry_transform_GroupBox->setTitle("Transform");
    Geometry_transform_GroupBox->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    Geometry_transform_GroupBox->setMaximumHeight(150);

    //Opacity
    Opacity_box = new QGroupBox;
    Opacity_layout = new QVBoxLayout(Opacity_box);
    Opacity_slider = new QSlider(Opacity_box);
    //Opacity_LineEdit = new QLineEdit(Opacity_box);

    //Opacity
    Opacity_box->setTitle("Opacity");
    Opacity_box->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    Opacity_box->setMaximumHeight(100);
    Opacity_slider->setRange(0,100);
    Opacity_slider->valueChanged(1);
    Opacity_slider->setValue(100);
    Opacity_slider->setOrientation(Qt::Orientation::Horizontal);
    Opacity_layout->addWidget(Opacity_slider);
    Opacity_box->setLayout(Opacity_layout);

    QSlider::connect(Opacity_slider,SIGNAL(sliderReleased()),this,SLOT(UpdateOpacity()));

    Field_AutoScale_GroupBox = new QGroupBox;
    Field_AutoScale_GroupBox->setTitle("Auto Scale");
    Field_AutoScale_GroupBox->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");

    Field_AutoScale_Layout = new QVBoxLayout(Field_AutoScale_GroupBox);
    AutoScale_Checkbox = new QCheckBox(Field_AutoScale_GroupBox);
    AutoScale_Checkbox->setText("AutoScale on Change");
    AutoScale_Checkbox->setChecked(true);

    Field_AutoScale_Layout->addWidget(AutoScale_Checkbox);
    Field_AutoScale_GroupBox->setLayout(Field_AutoScale_Layout);


    //End of transform layout config------------------------------------

    QVBoxLayout *Property = new QVBoxLayout;
    Property->addWidget(Geometry_transform_GroupBox);
    Property->addWidget(Opacity_box);
    Property->addWidget(Field_AutoScale_GroupBox);
    Property->setAlignment(Qt::AlignmentFlag::AlignTop);

    AddFilterProperties(Property);
    Properties_names.push_back("Geometry Prop.");
    //Adding all to the layout

}

void Geometry::AddFilterProperties(QVBoxLayout *Filter_properties)
{
    List_Properties.push_back(Filter_properties);
}

//End of functions of the properties------------------------------

//Other Functions-------------------------------------------------

//Read the data from a geometry file. Similar to reader constructor
void Geometry::ReadFile(QString FilePath)
{

    vtkSmartPointer<vtkXMLUnstructuredGridReader> reader = vtkSmartPointer<vtkXMLUnstructuredGridReader>::New();

    if(FilePath.isEmpty())
        return;
    if(!reader->CanReadFile(FilePath.toStdString().c_str()))
    {
        return;
    }
    reader->SetFileName(FilePath.toStdString().c_str());
    reader->Update();

    Grid->ShallowCopy(reader->GetOutput());

    if(Grid->GetFieldData()->GetNumberOfArrays()!=0)
    {
        Times->ShallowCopy(Grid->GetFieldData()->GetArray(0));
    }

    Mapper->SetInputData(Grid);
    Mapper->Update();
    Actor->SetMapper(Mapper);
    CurrentTimeStep=0;
    SelectedField=0;

    Name = FilePath.mid(FilePath.lastIndexOf("/") +1 ).toStdString();

    Field *Solid = new Field;
    Solid->Field_Name = "Solid";
    Solid->type = Type_of_Field::Solid;

    Field_list.push_back(Solid);
    ChangeField("Solid");
}

//End of Other Functions------------------------------------------

//Update Functions------------------------------------------------

//Posicion
void Geometry::UpdateXCenter(QString x)
{
    XCenter = x.toDouble();
    Actor->SetPosition(XCenter,YCenter,ZCenter);
}
void Geometry::UpdateYCenter(QString y)
{
    YCenter = y.toDouble();
    Actor->SetPosition(XCenter,YCenter,ZCenter);
}
void Geometry::UpdateZCenter(QString z)
{
    ZCenter = z.toDouble();
    Actor->SetPosition(XCenter,YCenter,ZCenter);
}
//Scale
void Geometry::UpdateXScale(QString x)
{
    XScale = x.toDouble();
    Actor->SetScale(XScale,YScale,ZScale);
}
void Geometry::UpdateYScale(QString y)
{
    YScale = y.toDouble();
    Actor->SetScale(XScale,YScale,ZScale);
}
void Geometry::UpdateZScale(QString z)
{
    ZScale = z.toDouble();
    Actor->SetScale(XScale,YScale,ZScale);
}

void Geometry::RotateX(QString x)
{
    Actor->RotateX(-XAngle);
    XAngle = x.toDouble();
    Actor->RotateX(XAngle);
}
void Geometry::RotateY(QString y)
{
    Actor->RotateY(-YAngle);
    YAngle = y.toDouble();
    Actor->RotateY(YAngle);
}
void Geometry::RotateZ(QString z)
{
    Actor->RotateZ(-ZAngle);
    ZAngle = z.toDouble();
    Actor->RotateZ(ZAngle);
}


void Geometry::UpdateOpacity()
{
    double val = Opacity_slider->value();
    this->Actor->GetProperty()->SetOpacity(val/100);
    render_Win->Render();
}
//End of Update Functions-----------------------------------------

bool Geometry::HaveVector()
{
    unsigned long i;
    for(i=0;i<Field_list.size();i++)
    {
        if(Field_list[i]->type == Vector)
            return true;
    }
    return false;

}

void Geometry::HideOrShow(int i)
{
    if(i==0)
    {
        Actor->VisibilityOff();
        HideWidgets();
    }
    else
    {
        if(!Actor->GetVisibility())
        {
            Actor->VisibilityOn();
        }
        ShowWidgets();
    }
}

void Geometry::GetOutput(vtkSmartPointer<vtkUnstructuredGrid> GridData)
{
    mute.lock();
    GridData->ShallowCopy(Grid);
    mute.unlock();
}
void Geometry::GetOutput(vtkSmartPointer<vtkPolyData> GridData)
{
    mute.lock();
    GridData->ShallowCopy(Grid);
    mute.unlock();
}
void Geometry::UpstreamChange() // No hace nada porque no hay nada arriba del main.
{

}

void Geometry::SendSignalsDownStream()
{
    ParallelDownStream->setFilterList(NextFilter);
    ParallelDownStream->start();
}

vtkDataSet* Geometry::GetGeoDataOutput()
{
    return Grid;
}

void Geometry::HideWidgets()
{
    if(UseScalarBar)
    {
        ScalarBar->VisibilityOff();
        ScalarWidget->Off();
    }
}
void Geometry::ShowWidgets()
{
    if(UseScalarBar && Actor->GetVisibility())
    {
        if(!(Field_list[SelectedField]->type == Solid))
        {
            ScalarWidget->On();
            ScalarBar->VisibilityOn();
        }
    }
}

void Geometry::SetScalarBarWidget()
{
    ScalarBar = vtkSmartPointer<vtkScalarBarActor>::New();
    ScalarBar->SetNumberOfLabels(5);
    ScalarBar->SetHeight(0.4);
    ScalarBar->DragableOn();
    //ScalarBar->SetLookupTable(Mapper->GetLookupTable());
    ScalarWidget = vtkSmartPointer<vtkScalarBarWidget>::New();
    ScalarWidget->SetScalarBarActor(ScalarBar);
    ScalarWidget->SetInteractor(this->render_Win->GetInteractor());


    UseScalarBar = true;
    ScalarBar->VisibilityOff();
    ScalarWidget->Off();
}

void Geometry::DisconectSonsWidgets()
{
    foreach(Geometry *geo, NextFilter)
    {
        geo->DisconectSonsWidgets();
    }
}
void Geometry::ReconectSonsWidgets()
{
    foreach(Geometry *geo, NextFilter)
    {
        geo->ReconectSonsWidgets();
    }
}
double* Geometry::GetBounds()
{
    return Actor->GetBounds();
}
double* Geometry::GetCenter()
{
    return Grid->GetCenter();
}
//---------------- Parallel signals Down Stream ------------------------

void ParallelSignalsDownStream::setFilterList(std::vector<Geometry *> Nexts)
{
    this->NextFilter=Nexts;
}
void ParallelSignalsDownStream::run()
{
    //emit RenderWindowUpdate();
    NumberOfFilters = NextFilter.size();
    count =0;
    if(NumberOfFilters==0)
    {
        emit EndUpdating();
    }
    unsigned long i;
    try
    {
        for(i=0;i<NextFilter.size();i++)
        {
            QObject::connect(NextFilter[i]->ParallelDownStream,&ParallelSignalsDownStream::EndUpdating,this,&ParallelSignalsDownStream::CatchUpdateSignal);
            NextFilter[i]->UpstreamChange();
        }
    }  catch (...) {
        return;
    }
}

void ParallelSignalsDownStream::CatchUpdateSignal()
{
    count++;
    if(count==NumberOfFilters)
    {
        for(unsigned long i=0;i<NextFilter.size();i++)
        {
            QObject::disconnect(NextFilter[i]->ParallelDownStream,&ParallelSignalsDownStream::EndUpdating,this,&ParallelSignalsDownStream::CatchUpdateSignal);
        }
        emit EndUpdating();
    }
}

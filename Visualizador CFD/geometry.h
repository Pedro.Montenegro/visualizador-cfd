#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <vtkPolyDataMapper.h>

#include <vtkSmartPointer.h>
#include <vtkDataSetMapper.h>
#include <vtkActor.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkCommand.h>
#include <vtkScalarBarActor.h>
#include <vtkScalarBarWidget.h>
#include <vtkLookupTable.h>
#include <vtkTextProperty.h>
#include <vtkAppendFilter.h>

#include <QString>
#include <QComboBox>
#include <QGroupBox>
#include "field.h"
#include <QVBoxLayout>

#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QGridLayout>
#include <QSlider>
#include <QThread>
#include <QCheckBox>

#include <QMutex>
/**
* List of Types of Filters
*/
enum Filter_type
{
    /**
    * Main. is the root in a tree of filters
    */
    Main,
    Normal,
    Slice,
    StreamLines,
    VortexFinder,
    PathLines,
    PuntosEstancamiento,
    CaudalPlano,
    FortranGPFEP,
    ViscousForces,
    OpenFOAMcase
};

class GeoCallback;
class ParallelSignalsDownStream;

/**
 * @brief Clase base para contener y visualizar una geometria. \n
 * Contiene un vector<Field*> para guardar los campos solucion \n
 * Posee mappers y actos para la visualización. \n
 * Es la clase base para el resto de los filtros \n
 */
class Geometry : public QObject
{
    //Macro necesario para hacer las conecciones con Qt
    Q_OBJECT

public:
    //------------------------------------------------------------------------------------------
    //-----------------------------------Atributes----------------------------------------------
    /**
    * Name of the geometry to identify it
    */
    std::string Name;
    /**
    * Tiempos de la geometria
    */
    vtkSmartPointer<vtkDoubleArray> Times;
    /**
    * Field List to save all the OUTPUTS fields in this geometry.
    */
    std::vector<Field *> Field_list;
    /**
    * Selected field in Field_List
    */
    unsigned long SelectedField;
    /**
    * Current time step in Field_List
    */
    unsigned long CurrentTimeStep;
    /**
    * Edge visibility off/on
    */
    bool edgeVisibility;
    /**
     * @brief Use or not the scalar bar
     */
    bool UseScalarBar;
    /**
     * @brief Class implemented to update the widgets after a changes. Only inicializated when needed
     */
    GeoCallback *callback;
    /**
     * @brief Class to provide an update of the branches without slowing the interface
     */
    ParallelSignalsDownStream *ParallelDownStream;
    /**
     * @brief mute. Used to lock other processors to avoid crash for race condition.
     */
    QMutex mute;
    //-----------------------------------For Filters----------------------------------------------
    /**
    * Tipo. Para saber de que filtro proviene (o si es una geometria normal)
    */
    Filter_type type;
    /**
    * Branches of each geometry.
    * Geometrias "Creadas" utilizando la actual como input.
    * Al actualizar esta geometria por cualquier motivo es necesario
    * actualizar sus "hijos"
    */
    std::vector<Geometry *> NextFilter;
    /**
    * Input del filtro.
    * Se usa para actualizar la pipeline.
    * No tiene aplicación en la geometria base.(main)
    */
    Geometry *Input;
    //-----------------------------------For visualization----------------------------------------------
    /**
     * @brief Actor
     */
    vtkSmartPointer<vtkActor> Actor;
    /**
     * @brief Mapper. Transform scalars into colors.
     */
    vtkSmartPointer<vtkDataSetMapper> Mapper;
    /**
    * A copy of the pointer to the renderWindows. Usefull to update the view after a change.
    */
    vtkRenderWindow *render_Win;
    //-----------------------------------For interface----------------------------------------------
    //Variable use to save al important properties of filters and geometries.
    /**
     * @brief Saves the properties names of each tab. It is used to load the names when the interace is update
     */
    std::vector<std::string> Properties_names;
    /**
     * @brief Saves the properties layout of each tab. It is used to load the QObjects of the interace when the interace is update.
     */
    std::vector<QVBoxLayout *> List_Properties;

    //------------------------------------------------------------------------------------------
    //-----------------------------------Methods----------------------------------------------
    /**
    * Base constructor
    */
    Geometry();
    /**
    * Reader constructor. FilePath is the path to geometry.vtu file
    */
    Geometry(QString FilePath);
    /**
    * Destructor
    */
    ~Geometry();

    /**
     * @brief Use in the Reader constructor. Read the geometry inside the path
     * @param FilePath
     */
    void ReadFile(QString FilePath);
    //-----------------------------------For manipuling the geometry and fields----------------------------------------------

    /**
     * @brief Change to time i
     * @param Time step.
     */
    virtual void ChangeToThisTime(int i);
    /**
     * @brief Create a new field to visualizate from the base format vtkArrayData
     * @param Path to vtkArrayData Field
     * @return True if it is readed correctly, false otherwise
     */
    bool InsertNewField(QString Field_Name);
    /**
     * @brief Change the visualization to this field. Do nothing is field_name doesn't exist inside Field_List
     * @param Field name.
     */
    virtual void ChangeField(std::string field_name);
    /**
     * @brief Change visualization to this component of the current field. The MainWindows class usually provide this name
     * @param component_name
     */
    virtual void ChangeComponent(std::string component_name);
    /**
     * @brief Internal function for setting the scalar bar(if it is on) and the active fields
     */
    virtual void SetVectors();
    /**
     * @brief Internal function to add the fields i of the current time step.
     * @param i is the positión of this Field inside the std::vector<Field*> Field_List
     */
    virtual void AddFields(int Field_number);
    /**
     * @brief Function usefull to check if a geometry is valid for a filter that need a Vector input.
     * @return Return true if the geometry have a vector type inside Field_List
     */
    virtual bool HaveVector();
    /**
     * @brief DisconectSonsWidgets. Implemented for avoid widget interaction during update process in online mode.
     * This function is meant to be override for classes with widgets
     */
    virtual void DisconectSonsWidgets();
    /**
     * @brief ReconectSonsWidgets. Reconect all the widgets. Implemeted to call after update is complete.
     * This function is meant to be override for classes with widgets
     */
    virtual void ReconectSonsWidgets();
    /**
     * @brief GetBounds
     * @return the bounds of the geometry
     */
    virtual double* GetBounds();
    /**
     * @brief GetCenter
     * @return the center of the geometry.
     */
    virtual double* GetCenter();
    //-----------------------------------For visualization----------------------------------------------
    /**
     * @brief Function to inicializate the scalar bar.
     * Use is there is a field to watch in the filter
     */
    virtual void SetScalarBarWidget();
    /**
     * @brief Re escale the mapper range to the current field range.
     */
    virtual void ReScaleToCurrentData();
    /**
     * @brief Re escale mapper to a custom range
     * @param Min range
     * @param Max range
     */
    void ReScaleToSetRange(double a, double b);
    /**
     * @brief Hide Widgets
     */
    virtual void HideWidgets();
    /**
     * @brief Show Widgets
     */
    virtual void ShowWidgets();
    /**
     * @brief Hide Or Show actor and widgets
     * @param i = 1 -> show. i = 0 -> Hide
     */
    virtual void HideOrShow(int i);

    //-----------------------------------For Update--------------------------------------------------------------------
    /**
     * @brief Funcion que actualiza la visualización luego de un cambio en la geometria padre.
     * Usualmente la llama la función SendSignalsDownStream.
     */
    virtual void UpstreamChange();

    //-----------------------------------For output- Conecction with other filters----------------------------------------------
    /**
     * @brief Create a copy of Grid in GridData
     * @param contenedor para la copia
     */
    virtual void GetOutput(vtkSmartPointer<vtkUnstructuredGrid> GridData);
    /**
     * @brief Create a copy of a main polydata in  PolyData
     * @param contenedor para la copia
     */
    virtual void GetOutput(vtkSmartPointer<vtkPolyData> Polydata);
    /**
     * @brief Devuelve la geometria
     * @return vtkDataSet puede ser VtkUnstructuredGrid o VtkPolyData
     */
    virtual vtkDataSet* GetGeoDataOutput();
    //-----------------------------------For manipuling the interface----------------------------------------------

protected:
    //------------------------------------------------------------------------------------------
    //------------------------------------------Internal atributes------------------------------------------------
    /**
    * Grid to save the input of the father geometry. Only inicialiated when needed
    */
    vtkSmartPointer<vtkUnstructuredGrid> grid_copy;
    /**
    * PolyData to save the input of the father geometry. Only inicialiated when needed
    */
    vtkSmartPointer<vtkPolyData> poly_copy;
    /**
    * Output grid.
    */
    vtkSmartPointer<vtkUnstructuredGrid> Grid;
    /**
    * Output polyData. Only used in some filters and inicializated when needed.
    */
    vtkSmartPointer<vtkPolyData> PolyData;
    //Variables of the scalar bar
    /**
     * @brief ScalarBar Actor.
     */
    vtkSmartPointer<vtkScalarBarActor> ScalarBar;
    /**
     * @brief ScalarWidget. make the scalar bar actor movable.
     */
    vtkSmartPointer<vtkScalarBarWidget> ScalarWidget;

    //Variables necesary to configure the properties of the geometry.
    QGroupBox *Geometry_transform_GroupBox;
    QGridLayout *Geometry_transform_layout;
    //Variables used to move the geometry
    double XCenter, YCenter, ZCenter;
    QLabel *XCenterLabel, *YCenterLabel, *ZCenterLabel, *Center_Geometry;
    QLineEdit *XCenterLine_Edit, *YCenterLine_Edit, *ZCenterLine_Edit;
    //Variable used for scaling
    double XScale, YScale, ZScale;
    QLabel *XScaleLabel, *YScaleLabel, *ZScaleLabel, *Scale_Geometry;
    QLineEdit *XScaleLine_Edit, *YScaleLine_Edit, *ZScaleLine_Edit;
    //rotate
    double XAngle, YAngle, ZAngle;
    QLabel *XAngleLabel, *YAngleLabel, *ZAngleLabel, *Angle_Geometry;
    QLineEdit *XAngleLine_Edit, *YAngleLine_Edit, *ZAngleLine_Edit;
    //AutoScaling
    QGroupBox *Field_AutoScale_GroupBox;
    QVBoxLayout *Field_AutoScale_Layout;
    QCheckBox *AutoScale_Checkbox;

    //Opacity
    QGroupBox *Opacity_box;
    QVBoxLayout *Opacity_layout;
    QSlider *Opacity_slider;
    QLineEdit *Opacity_LineEdit;
    //--------------------------------------------Interal functions----------------------------------------------
    /**
     * @brief Internal function for setting the scalar bar(if it is on) and the active fields
     */
    virtual void SetScalar();
    /**
     * @brief Set up properties in the interface
     */
    void CreateGeometryConfig();
    /**
     * @brief Add the layout into the list of properties
     * @param Layout to add
     */
    void AddFilterProperties(QVBoxLayout *Filter_properties);
public slots:
    //Update pipeline
    void SendSignalsDownStream();
    virtual void Update();
private slots:
    //Functions use to update the properties of the geometry
    //Posicion
    void UpdateXCenter(QString x);
    void UpdateYCenter(QString y);
    void UpdateZCenter(QString z);

    //Scale
    void UpdateXScale(QString x);
    void UpdateYScale(QString y);
    void UpdateZScale(QString z);

    //angle
    void RotateX(QString x);
    void RotateY(QString y);
    void RotateZ(QString z);
    //Opacity
    void UpdateOpacity();

signals:
    void UpdateInterfaseSignal();
};
/**
 * @brief Callback para actualizar luego de mover los widgets. ej. seedswidget
 */
class GeoCallback : public vtkCommand
{
public:
    static GeoCallback *New()
    {
        return new GeoCallback;
    }
    virtual void Execute(vtkObject *caller, unsigned long, void *)
    {
        mute.lock();
        Geo->Update();
        mute.unlock();
    }
    Geometry *Geo;
    QMutex mute;
};

/**
 * @brief Clase creada para actualizar en parallelo sin que se lagee la interfaz
 */
class ParallelSignalsDownStream : public QThread
{
    Q_OBJECT

public:
    void run() override;
    std::vector<Geometry*> NextFilter;
    void setFilterList(std::vector<Geometry*> Nexts);
    int NumberOfFilters;
    int count;

public slots:
    void CatchUpdateSignal();
signals:
    void EndUpdating();
};



#endif // GEOMETRY_H



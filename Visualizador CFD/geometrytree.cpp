#include "geometrytree.h"
#include <QDebug>
GeometryTree::GeometryTree()
{
    Tree = new QTreeWidget;
}
GeometryTree::GeometryTree(QTreeWidget *tree)
{
    Tree = tree;
}
GeometryTree::~GeometryTree()
{
    delete Tree;
}
void GeometryTree::addTreeRoot(QString name)
{
    // QTreeWidgetItem(QTreeWidget * parent, int type = Type)
    QTreeWidgetItem *treeItem = new QTreeWidgetItem(Tree);

    // QTreeWidgetItem::setText(int column, const QString & text)
    treeItem->setText(0, name);

    treeItem->setCheckState(0,Qt::Checked);
    //addTreeChild(treeItem, name);

}

void GeometryTree::addTreeChild(QTreeWidgetItem *parent,QString name)
{
    // QTreeWidgetItem(QTreeWidget * parent, int type = Type)
    QTreeWidgetItem *treeItem = new QTreeWidgetItem();
    // QTreeWidgetItem::setText(int column, const QString & text)
    treeItem->setText(0, name);
    treeItem->setFlags(treeItem->flags() | Qt::ItemIsUserCheckable);
    treeItem->setCheckState(0,Qt::Checked);
    // QTreeWidgetItem::addChild(QTreeWidgetItem * child)
    parent->addChild(treeItem);
    parent->setExpanded(true);
}

void GeometryTree::RemoveItem(QTreeWidgetItem *item)
{
    if(item->parent()!=nullptr)
    {
        QTreeWidgetItem *pp = item->parent();
        pp->removeChild(item);
        delete item;
    }
    else
    {
        delete item;
    }
}

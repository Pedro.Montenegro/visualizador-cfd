#ifndef GEOMETRYTREE_H
#define GEOMETRYTREE_H

#include <QTreeWidget>
#include <QTreeWidgetItem>

#include <QDialog>
#include <QString>
#include <QTreeWidget>


//Clase creada para manejar el arbol de geometrias.
//NO CONTIENE las geometrias en si, solo sus nombres
class GeometryTree : public QObject
{
    Q_OBJECT
public:
    //Base Constructor- Not used -
    GeometryTree();
    /**
     * @brief Tree constructor
     * @param Qtree
     */
    GeometryTree(QTreeWidget *tree);
    //Destructor
    ~GeometryTree();

    /**
     * @brief Tree pointer
     */
    QTreeWidget *Tree;

    /**
     * @brief addTreeRoot == addMainGeometry case
     * @param geometry name.
     */
    void addTreeRoot(QString name);
    /**
     * @brief addTreeChild. Agrega una nueva rama
     * @param parent == filtro padre
     * @param filter name
     */
    void addTreeChild(QTreeWidgetItem *parent,
                      QString name);
    /**
     * @brief Remueve una objeto del arbol (nodo o raiz).
     * @param item a remover.
     */
    void RemoveItem(QTreeWidgetItem *item);
};

#endif // GEOMETRYTREE_H

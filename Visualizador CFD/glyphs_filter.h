#ifndef GYLPHS_FILTER_H
#define GYLPHS_FILTER_H

#include <QObject>

#include <vtkGlyph3D.h>
#include <vtkArrowSource.h>

#include "geometry.h"
#include "input_handler.h"

class Glyphs_filter :public Geometry
{
public:
    //Constructor
    Glyphs_filter();

    //Destructor
    ~Glyphs_filter();

    //Flags
    double ScaleFactor;
    bool ScalingFlag;

    //Functions
    void SetInputData(Geometry *Source);
    void FilterPropertiesConfig();
    void ChangeToThisTime(int i) override;

    vtkSmartPointer<vtkGlyph3D> Glyphs_Calculator;
    vtkSmartPointer<vtkArrowSource> arrow_source;
    Input_handler *InputHandler;

    void UpstreamChange() override;
    void Update() override;
};

#endif // GYLPHS_FILTER_H

#include "gpfepwriter.h"

void GPFEPWriter::run()
{
    Write();
}
GPFEPWriter::GPFEPWriter()
{
    qRegisterMetaType<QList<QString>>();
}
GPFEPWriter::~GPFEPWriter()
{
}
void GPFEPWriter::Write()
{

    std::string path = file_name.toStdString();
    std::string file = "/gpfep.cfg";
    std::string gpfep = path + file;
    std::string line = "";
    std::ifstream config_file(gpfep.c_str());

    emit textToBar("Reading Config File");

    //DIMENSIONES
    while (line != "*SPACE_DIMENSIONS")
    {
        std::getline(config_file, line);
    }
    std::getline(config_file, line);
    int num_dimension = std::stoi(line);

    if (num_dimension != 1 && num_dimension != 2 && num_dimension != 3)
    {
        std::cout << "SPACE_DIMENSIONS no tiene un valor correcto" << std::endl;
        return;
    }
    int NUM_FIELDS = 6 + num_dimension * 2;
    //NODOS
    while (line != "*FILES_CONTAINING_MESHES")
    {
        std::getline(config_file, line);
    }

    std::getline(config_file, line);

    std::string aux_filename = std::string(line);
    std::string filename = path +"/"+ std::string(line);
    std::ifstream filestream(filename.c_str());

    double t_inicial, t_final;
    while (line != "*INITIAL_CONTINUATION_PARAMETER")
    {
        std::getline(config_file, line);
    }
    std::getline(config_file, line);

    t_inicial = std::stod(line);

    while (line != "*FINAL_CONTINUATION_PARAMETER")
    {
        std::getline(config_file, line);
    }
    std::getline(config_file, line);

    t_final = std::stod(line);

    while (line != "*NUMBER_OF_CONTINUATION_STEPS")
    {
        std::getline(config_file, line);
    }
    std::getline(config_file, line);
    int pasos = std::stod(line);

    while (line != "*FIRST_OUTPUT")
    {
        std::getline(config_file, line);
    }
    std::getline(config_file, line);
    int first_output = std::stod(line);

    while (line != "*STEPS_BETWEEN_OUTPUTS")
    {
        std::getline(config_file, line);
    }
    std::getline(config_file, line);
    int outputs_steps = std::stod(line);

    double delta_t = (t_final - t_inicial) / pasos;

    vtkSmartPointer<vtkDoubleArray> time =
            vtkSmartPointer<vtkDoubleArray>::New();
    time->SetNumberOfComponents(1);
    time->SetName("TIME");

    //Valores. Solo velocidad por ahora.

    while (line != "*RESTART_FILE")
    {
        std::getline(config_file, line);
    }
    std::getline(config_file, line); // line = "res__.dat"
    int indice = line.find("_");

    std::string restart_file_name=line.substr(0,indice);

    int num_of_digits = 1;
    while (line[indice + num_of_digits] == '_')
    {
        num_of_digits++;
    }
    int i, j;
    double auxiliar = t_inicial;

    vtkSmartPointer<vtkDenseArray<double>> velocity = vtkSmartPointer<
            vtkDenseArray<double>>::New();

    vtkSmartPointer<vtkArrayData> Velocity_field =
            vtkSmartPointer<vtkArrayData>::New();

    vtkSmartPointer<vtkDenseArray<double>> Presure = vtkSmartPointer<
            vtkDenseArray<double>>::New();

    vtkSmartPointer<vtkArrayData> Presure_field =
            vtkSmartPointer<vtkArrayData>::New();

    vtkSmartPointer<vtkDenseArray<double>> Presure_Gradient = vtkSmartPointer<
            vtkDenseArray<double>>::New();

    vtkSmartPointer<vtkArrayData> Presure_Gradient_field =
            vtkSmartPointer<vtkArrayData>::New();

    vtkSmartPointer<vtkDenseArray<double>> Temperature = vtkSmartPointer<
            vtkDenseArray<double>>::New();

    vtkSmartPointer<vtkArrayData> Temperature_field =
            vtkSmartPointer<vtkArrayData>::New();

    vtkSmartPointer<vtkArrayDataWriter> ArrayWriter = vtkSmartPointer<vtkArrayDataWriter>::New();
    vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();


    boost::filesystem::path p(path);
    std::vector<std::string> paths;

    for(auto& entry : boost::make_iterator_range(boost::filesystem::directory_iterator(p), {}))
    {
        std::string aux = entry.path().string();

        if((aux.find(restart_file_name) != std::string::npos) && (aux.find(".dat") != std::string::npos))
        {
            paths.push_back(aux);
        }
    }
    std::sort(paths.begin(),paths.end());

    emit numberOfFiles(paths.size()+1);
    emit textToBar("Reading Geometry");

    //-----------------------------------Leo Geometria----------------------------------------------
    vtkSmartPointer<vtkPoints> points =
            vtkSmartPointer<vtkPoints>::New();

    std::getline(filestream, line); // Tomo la liena *Coordenadas
    std::getline(filestream, line); // Numero de nodos
    std::getline(filestream, line);
    if (num_dimension == 1)
    {
        while (line != "" && line != "*ELEMENT_GROUPS")
        {
            int i;
            double x;
            std::stringstream linestream;
            linestream << line;
            linestream >> i >> x;

            points->InsertNextPoint(x, 0, 0);

            std::getline(filestream, line);
        }
    }
    else if (num_dimension == 2)
    {
        while (line != "" && line != "*ELEMENT_GROUPS")
        {
            int i;
            double x, y;
            std::stringstream linestream;
            linestream << line;
            linestream >> i >> x >> y;

            points->InsertNextPoint(x, y, 0);

            std::getline(filestream, line);
        }
    }
    else
    {
        while (line != "" && line != "*ELEMENT_GROUPS")
        {

            int i;
            double x, y, z;
            std::stringstream linestream;
            linestream << line;
            linestream >> i >> x >> y >> z;

            points->InsertNextPoint(x, y, z);

            std::getline(filestream, line);
        }
    }

    //ID de celdas
    vtkSmartPointer<vtkUnstructuredGrid> unstructuredGrid1 =
            vtkSmartPointer<vtkUnstructuredGrid>::New();

    unstructuredGrid1->SetPoints(points);

    while (line != "<NONE>")
    {
        std::getline(filestream, line);
    }

    std::getline(filestream, line);
    if (num_dimension == 1)
    {
        while (line != "" && line != "*END")
        {
            int i, x;
            std::stringstream linestream;
            linestream << line;
            linestream >> i >> x;

            vtkIdType ptIds[] = {i - 1, x - 1};
            unstructuredGrid1->InsertNextCell(VTK_LINE, 2, ptIds);

            std::getline(filestream, line);
        }
    }
    else if (num_dimension == 2)
    {
        while (line != "" && line != "*END")
        {
            int i, x, y;
            std::stringstream linestream;
            linestream << line;
            linestream >> i >> x >> y;

            vtkIdType ptIds[] = {i - 1, x - 1, y - 1, 0};
            unstructuredGrid1->InsertNextCell(VTK_TRIANGLE, 3, ptIds);

            std::getline(filestream, line);
        }
    }
    else
    {
        while (line != "" && line != "*END")
        {
            int i, x, y, z;
            std::stringstream linestream;
            linestream << line;
            linestream >> i >> x >> y >> z;

            vtkIdType ptIds[] = {i - 1, x - 1, y - 1, z - 1};
            unstructuredGrid1->InsertNextCell(VTK_TETRA, 4, ptIds);

            std::getline(filestream, line);
        }
    }

    filestream.close();
    emit fileNumber(1);
    emit textToBar("Reading Fields");
    //----------------------------------Leo los campos----------------------------------------------

    velocity->Resize(paths.size(), unstructuredGrid1->GetNumberOfPoints(), 3);
    std::string velocity_name = "Velocity";
    velocity->SetName(velocity_name.c_str());

    Presure->Resize(paths.size(), unstructuredGrid1->GetNumberOfPoints(),1);
    std::string Presure_name = "Presure";
    Presure->SetName(Presure_name.c_str());

    Presure_Gradient->Resize(paths.size(), unstructuredGrid1->GetNumberOfPoints(), 3);
    std::string Presure_Gradient_name = "Presure_Gradient";
    Presure_Gradient->SetName(Presure_Gradient_name.c_str());

    Temperature->Resize(paths.size(), unstructuredGrid1->GetNumberOfPoints(),1);
    std::string Temperature_name = "Temperature";
    Temperature->SetName(Temperature_name.c_str());

    for (i = 0; i <(int)paths.size(); i++)
    {

        std::string data = paths[i];
        std::string line2;

        boost::iostreams::filtering_istream data_stream;
        data_stream.push(boost::iostreams::gzip_decompressor());
        data_stream.push(boost::iostreams::file_source(data));

        std::string aux_time;
        double current_time;
        while(aux_time != "Continuation parameter:")
        {
            std::getline(data_stream, line2);
            if(line2.find("Continuation parameter:") != std::string::npos)
            {
                aux_time = line2.substr(0,23);
                std::stringstream linestream;
                linestream << line2.substr(24);
                linestream >> current_time;
            }
        }
        std::string textt = "Reading file: " + std::to_string(i) + "/" +std::to_string(paths.size()) + ". Time: " + std::to_string(current_time);
        emit textToBar(QString::fromStdString(textt));
        time->InsertNextValue(current_time);
        while (line2 != "<NONE>")
        {
            std::getline(data_stream, line2);
        }

        int count = 0;
        int index = 0;
        double au[3] = {0, 0, 0};

        while (std::getline(data_stream, line2, '\n'))
        {
            if(line2 == "*END")
            {
                break;
            }
            double z;
            std::stringstream linestream;
            linestream << line2;
            linestream >> z;
            if (count < num_dimension)
            {
                au[count] = z;
                if (count == (num_dimension - 1))
                {
                    velocity->SetValue(i,index, 0, au[0]);
                    velocity->SetValue(i,index, 1, au[1]);
                    velocity->SetValue(i,index, 2, au[2]);
                }
            }
            else if (count == num_dimension)
            {
                Presure->SetValue(i, index,0, z);
            }
            else if (count > num_dimension && count < (num_dimension * 2 + 1))
            {
                au[count - num_dimension - 1] = z;
                if (count == num_dimension * 2)
                {
                    Presure_Gradient->SetValue(i, index, 0, au[0]);
                    Presure_Gradient->SetValue(i, index, 1, au[1]);
                    Presure_Gradient->SetValue(i, index, 2, au[2]);
                }
            }
            else if(count == num_dimension*2+5)
            {
                Temperature->SetValue(i,index,0,z);
            }
            count++;
            if(count == NUM_FIELDS)
            {
                count=0;
                index++;
            }
        }
        emit fileNumber(i+1);
    }
    unstructuredGrid1->GetFieldData()->AddArray(time);
    //FIELDS WRITE

    emit textToBar("Writing Files");
    std::string PathToWrite = WriteIn.toStdString() + "/Output/";
    if(!dirExist(PathToWrite))
    {
        mkdir(PathToWrite.c_str(),0777);
    }
    //Velocidad
    Velocity_field->AddArray(velocity);
    std::string vtu = PathToWrite + "Velocity_field";

    ArrayWriter->SetInputData(Velocity_field);
    ArrayWriter->SetFileName(vtu.c_str());
    ArrayWriter->Write();

    //Presion
    Presure_field->AddArray(Presure);
    vtu = PathToWrite + "Presure_field";

    ArrayWriter->SetInputData(Presure_field);
    ArrayWriter->SetFileName(vtu.c_str());
    ArrayWriter->Write();

    //Gradiente de presion
    Presure_Gradient_field->AddArray(Presure_Gradient);
    vtu =PathToWrite +  "Presure_gradient_field";

    ArrayWriter->SetInputData(Presure_Gradient_field);
    ArrayWriter->SetFileName(vtu.c_str());
    ArrayWriter->Write();

    Temperature_field->AddArray(Temperature);
    vtu = PathToWrite + "Temperature_field";

    ArrayWriter->SetInputData(Temperature_field);
    ArrayWriter->SetFileName(vtu.c_str());
    ArrayWriter->Write();

    //Geometry Write
    vtu = PathToWrite + "geometry.vtu";
    writer->SetFileName(vtu.c_str());
    writer->SetInputData(unstructuredGrid1);
    writer->Write();

    config_file.close();

    QList<QString> fields_names;
    fields_names.push_back("Velocity_field");
    fields_names.push_back("Presure_field");
    fields_names.push_back("Presure_gradient_field");
    fields_names.push_back("Temperature_field");
    QString Case = QString::fromStdString(PathToWrite);
    emit ArrayNames(fields_names,Case);
}

bool GPFEPWriter::dirExist(std::string path)
{
    struct stat info;

    if( stat( path.c_str(), &info ) != 0 )
        return false;
    else if( info.st_mode & S_IFDIR )  // S_ISDIR() doesn't exist on my windows
        return true;
    else
        return false;
}

void GPFEPWriter::SetPaths(QString file_namee, QString WriteInn)
{
    this->file_name = file_namee;
    this->WriteIn = WriteInn;
}

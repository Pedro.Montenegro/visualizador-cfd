#ifndef GPFEPWRITER_H
#define GPFEPWRITER_H

#include <QThread>
#include <QMessageBox>
#include <QFileDialog>

#include <vtkSmartPointer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkArrayDataWriter.h>
#include <vtkDenseArray.h>
#include <vtkArrayData.h>
#include <vtkDoubleArray.h>
#include <vtkUnstructuredGrid.h>
#include <vtkFieldData.h>

#include <boost/iostreams/device/file.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>

#include <sys/types.h>
#include <sys/stat.h>

// Clase implemented for reading a GPFEP case and save the data in the used format.
// Se hace con un thread en paralelo
class GPFEPWriter : public QThread
{
    Q_OBJECT
public:
    //Constructor
    GPFEPWriter();
    //Destructor
    ~GPFEPWriter();

    /**
     * @brief Paralel loop to do.
     */
    void run() override;
    /**
     * @brief Read the data from "file_namee" and write it in "WriteIn"
     */
    void Write();
    /**
     * @brief Check is the directory exist.
     * @param path to directory
     * @return true or false
     */
    bool dirExist(std::string path);
    /**
     * @brief Configure the case path and the write path.
     * @param file_namee == case path
     * @param WriteInn == where to write the output
     */
    void SetPaths(QString file_namee,QString WriteInn);

    QString file_name;
    QString WriteIn;

signals:
    /**
     * @brief Slot to update the text in the load bar
     * @param text
     */
    void textToBar(QString text);
    /**
     * @brief pass the current file number to the load bar
     * @param file_number
     */
    void fileNumber(int file);
    /**
     * @brief pass the number of files to load to the load bar
     * @param total number of files to read
     */
    void numberOfFiles(int files);
    /**
     * @brief Pass the array names and the path to the folter to the main Windows. It is use to load the case after reading.
     * @param List with the names of the arrays to load
     * @param Case == path to "where to write" == WriteIn +"/Output/"
     */
    void ArrayNames(QList<QString> names,QString Case);


};

#endif // GPFEPWRITER_H

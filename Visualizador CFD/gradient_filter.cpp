#include "gradient_filter.h"

Gradient_Filter::Gradient_Filter()
{
    GradFilter = vtkSmartPointer<vtkGradientFilter>::New();
    grid_copy = vtkSmartPointer<vtkUnstructuredGrid>::New();
    InputHandler = new Input_handler(1);
    FilterPropertiesConfig();
    ComputeDivergency = false;
    ComputeGradient = true;
    ComputeQCriterion = false;
    ComputeVorticity = false;
}

Gradient_Filter::~Gradient_Filter()
{
    mute.lock();
    unsigned long i;
    for( i= 0 ; i< Input->NextFilter.size();i++)
    {
        if(Input->NextFilter[i]->Name == this->Name)
        {
            break;
        }
    }
    Input->NextFilter.erase(Input->NextFilter.begin()+i);
    delete Calculate_groupBox;
    delete InputHandler;
    for(unsigned long i=0;i<Field_list.size();i++)
    {
        delete Field_list[i];
    }
    Field_list.clear();
    mute.unlock();
}
void Gradient_Filter::SetInputData(Geometry *Source)
{
    mute.lock();
    this->Times->ShallowCopy(Source->Times);
    this->Name = Source->Name + "/Gradient";
    this->type = Filter_type::Normal;
    Input = Source;
    render_Win = Input->render_Win;
    Input->GetOutput(grid_copy);
    Grid->CopyStructure(grid_copy);
    SelectedField=0;
    CurrentTimeStep = Input->CurrentTimeStep;

    InputHandler->AddAllFields(Source->Field_list);
    InputHandler->SetInputName(0,"Field");
    InputHandler->AddItemsToComboBox(0);
    InputHandler->SetInputTimeStep(Source->CurrentTimeStep);

    std::string prev_field_name = Source->Field_list[Source->SelectedField]->Field_Name;
    InputHandler->SetSelectedInput(prev_field_name);
    InputHandler->ComboBoxes[0]->setCurrentIndex(InputHandler->SelectedInput[0]);

    QComboBox::connect(InputHandler->ComboBoxes[0],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputField(QString)));

    Update();
    Mapper->SetInputData(Grid);
    Mapper->Update();
    Input->NextFilter.push_back(this);
    mute.unlock();

}

void Gradient_Filter::Update()
{
    mute.lock();
    for(unsigned long i=0;i<Field_list.size();i++)
    {
        delete Field_list[i];
    }
    Field_list.clear();

    std::vector<std::string> Inputs_names = InputHandler->GetFieldsNames();

    if(InputHandler->GetSelectedField()->type == Vector)
    {
        if(ComputeGradient)
        {
            int num_of_comp = InputHandler->GetSelectedSolution()->GetNumberOfComponents();
            for(int j =0 ; j<num_of_comp ; j++)
            {
                InputHandler->GetSelectedField()->selectedComponent =j;
                grid_copy->GetPointData()->SetActiveScalars(InputHandler->GetSelectedField()->GetCurrentComponentName().c_str());

                GradFilter->SetInputData(grid_copy);
                char a[] = "X";
                a[0] = a[0] + j;
                std::string gradient_name =  + "Gradient_Comp_";
                gradient_name = gradient_name+ a;
                GradFilter->SetInputData(grid_copy);
                GradFilter->SetComputeDivergence(false);
                GradFilter->SetComputeGradient(ComputeGradient);
                GradFilter->SetComputeQCriterion(false);
                GradFilter->SetComputeVorticity(false);

                GradFilter->SetResultArrayName(gradient_name.c_str());
                GradFilter->Update();

                vtkSmartPointer<vtkDataArray> i_gradient = vtkSmartPointer<vtkDataArray>(GradFilter->GetOutput()->GetPointData()->GetArray(gradient_name.c_str()));

                Field *N_Field = new Field(i_gradient);
                Field_list.push_back(N_Field);
                AddFields(Field_list.size()-1);
            }
        }
        grid_copy->GetPointData()->SetActiveScalars(InputHandler->GetSelectedField()->Field_Name.c_str());
        if(ComputeDivergency || ComputeVorticity || ComputeQCriterion)
        {
            GradFilter->SetInputData(grid_copy);
            GradFilter->SetComputeDivergence(ComputeDivergency);
            GradFilter->SetComputeGradient(false);
            GradFilter->SetComputeQCriterion(ComputeQCriterion);
            GradFilter->SetComputeVorticity(ComputeVorticity);

            GradFilter->SetQCriterionArrayName("QCriterion");
            GradFilter->SetVorticityArrayName("Vorticity");
            GradFilter->SetDivergenceArrayName("Divergency");


            GradFilter->Update();

            if(ComputeDivergency)
            {

                vtkSmartPointer<vtkDataArray> i_gradient = vtkSmartPointer<vtkDataArray>(GradFilter->GetOutput()->GetPointData()->GetArray("Divergency"));

                Field *N_Field = new Field(i_gradient);
                Field_list.push_back(N_Field);
                AddFields(Field_list.size()-1);
            }
            if(ComputeQCriterion)
            {
                vtkSmartPointer<vtkDataArray> i_gradient = vtkSmartPointer<vtkDataArray>(GradFilter->GetOutput()->GetPointData()->GetArray("QCriterion"));

                Field *N_Field = new Field(i_gradient);
                Field_list.push_back(N_Field);
                AddFields(Field_list.size()-1);
            }
            if(ComputeVorticity)
            {

                vtkSmartPointer<vtkDataArray> i_gradient = vtkSmartPointer<vtkDataArray>(GradFilter->GetOutput()->GetPointData()->GetArray("Vorticity"));

                Field *N_Field = new Field(i_gradient);
                Field_list.push_back(N_Field);
                AddFields(Field_list.size()-1);

            }
        }
    }
    else
    {
        grid_copy->GetPointData()->SetActiveScalars(InputHandler->GetSelectedField()->Field_Name.c_str());

        GradFilter->SetInputData(grid_copy);
        std::string gradient_name =  "Gradient";
        GradFilter->SetInputData(grid_copy);
        GradFilter->SetComputeDivergence(false);
        GradFilter->SetComputeGradient(ComputeGradient);
        GradFilter->SetComputeQCriterion(false);
        GradFilter->SetComputeVorticity(false);

        GradFilter->SetResultArrayName(gradient_name.c_str());
        GradFilter->Update();

        vtkSmartPointer<vtkDataArray> i_gradient = vtkSmartPointer<vtkDataArray>(GradFilter->GetOutput()->GetPointData()->GetArray(gradient_name.c_str()));

        Field *N_Field = new Field(i_gradient);
        Field_list.push_back(N_Field);
        AddFields(Field_list.size()-1);
    }
    emit UpdateInterfaseSignal();
    SetVectors();
    SendSignalsDownStream();
    mute.unlock();
}

void Gradient_Filter::UpstreamChange()
{
    mute.lock();
    CurrentTimeStep=Input->CurrentTimeStep;
    Input->GetOutput(grid_copy);

    QComboBox::disconnect(InputHandler->ComboBoxes[0],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputField(QString)));
    std::string prev_name = InputHandler->GetSelectedFieldName(0);

    InputHandler->RemoveItemsFromComboBox();
    InputHandler->SetInputTimeStep(CurrentTimeStep);

    InputHandler->UpdateInputHandler(Input->Field_list);
    InputHandler->SetInputTimeStep(Input->CurrentTimeStep);

    InputHandler->AddItemsToComboBox(0);

    if(!InputHandler->SetSelectedInput(0,prev_name))
    {
        InputHandler->SetSelectedInput(0,InputHandler->GetFieldsNames()[0]);
        InputHandler->SetComboBoxIndex(0,InputHandler->GetFieldsNames()[0]);
    }
    mute.unlock();

    Update();
    QComboBox::connect(InputHandler->ComboBoxes[0],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputField(QString)));

}

void Gradient_Filter::ChangeToThisTime(int i)
{
    Input->ChangeToThisTime(i);
}
void Gradient_Filter::ChangeInputField(QString name)
{
    InputHandler->SetSelectedInput(name.toStdString());
    Update();
}
void Gradient_Filter::UpdateGradientOptions()
{
    if(Gradient_CheckBox->isChecked())
        ComputeGradient = true;
    else
    {
        ComputeGradient = false;
    }
    if(Divergency_CheckBox->isChecked())
        ComputeDivergency = true;
    else
    {
        ComputeDivergency = false;
    }
    if(QCriterion_CheckBox->isChecked())
        ComputeQCriterion = true;
    else
    {
        ComputeQCriterion = false;
    }
    if(Vorticity_CheckBox->isChecked())
        ComputeVorticity = true;
    else
    {
        ComputeVorticity = false;
    }
    Update();
}

void Gradient_Filter::FilterPropertiesConfig()
{
    Calculate_groupBox = new QGroupBox;
    Calculate_groupBox->setTitle("Options");
    Calculate_groupBox->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    Calculate_groupBox->setMinimumHeight(80);
    Calculate_groupBox->setMaximumHeight(130);

    Calculate_layout = new QVBoxLayout(Calculate_groupBox);

    Gradient_CheckBox = new QCheckBox("Compute Gradient",Calculate_groupBox);
    Gradient_CheckBox->setCheckState(Qt::CheckState::Checked);
    Divergency_CheckBox = new QCheckBox("Compute Divergency",Calculate_groupBox);
    Divergency_CheckBox->setCheckState(Qt::CheckState::Unchecked);
    QCriterion_CheckBox = new QCheckBox("Compute QCriterion",Calculate_groupBox);
    QCriterion_CheckBox->setCheckState(Qt::CheckState::Unchecked);
    Vorticity_CheckBox = new QCheckBox("Compute Vorticity",Calculate_groupBox);
    Vorticity_CheckBox->setCheckState(Qt::CheckState::Unchecked);

    Calculate_layout->addWidget(Gradient_CheckBox);
    Calculate_layout->addWidget(Divergency_CheckBox);
    Calculate_layout->addWidget(QCriterion_CheckBox);
    Calculate_layout->addWidget(Vorticity_CheckBox);

    Calculate_groupBox->setLayout(Calculate_layout);
    QVBoxLayout *properties = new QVBoxLayout;
    InputHandler->AddQBoxToLayout(properties);
    properties->addWidget(Calculate_groupBox);

    AddFilterProperties(properties);
    Properties_names.push_back("Filter Prop.");

    QObject::connect(Gradient_CheckBox,SIGNAL(released()),this,SLOT(UpdateGradientOptions()));
    Gradient_CheckBox->hide();
    QObject::connect(Divergency_CheckBox,SIGNAL(released()),this,SLOT(UpdateGradientOptions()));
    QObject::connect(QCriterion_CheckBox,SIGNAL(released()),this,SLOT(UpdateGradientOptions()));
    QObject::connect(Vorticity_CheckBox,SIGNAL(released()),this,SLOT(UpdateGradientOptions()));

}

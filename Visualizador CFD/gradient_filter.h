#ifndef GRADIENT_FILTER_H
#define GRADIENT_FILTER_H

#include <vtkGradientFilter.h>

#include <QCheckBox>
#include <QVBoxLayout>

#include "geometry.h"
#include "input_handler.h"

class Gradient_Filter : public Geometry
{
    Q_OBJECT
public:
    //Base constructor
    Gradient_Filter();
    //Destructor
    ~Gradient_Filter();

    //Flags for the filter
    bool ComputeDivergency;
    bool ComputeGradient;
    bool ComputeQCriterion;
    bool ComputeVorticity;

    /**
     * @brief Sirve como input para la clase
     * @param Geometria input
     */
    void SetInputData(Geometry *Source);
    /**
     * @brief Crea la pestaña de propiedades del filtro
     */
    void FilterPropertiesConfig();
    /**
     * @brief Cambia el arbol de geometrias a este paso de tiempo
     * @param i = paso de tiempo
     */
    void ChangeToThisTime(int i) override;

    /**
     * @brief vtkFilter for gradient calculations.
     */
    vtkSmartPointer<vtkGradientFilter> GradFilter;

    /**
     * @brief Clase para manejar los distintos inputs. Controla inputs vectoriales en este caso
     */
    Input_handler *InputHandler;

    /**
     * @brief UpstreamChange. Se usa en caso ed que el input del filtro cambie. Usualmente se llama mediante "SendSignalsDownStream"
     */
    void UpstreamChange() override;
    /**
     * @brief Actualiza el filtro a nivel objeto. Por ejemplo, cuando el plano cambia de lugar.
     */
    void Update() override;
    //------------------------------------------------------------------------------------------

private:

    //Variables for the interface
    QGroupBox *Calculate_groupBox;
    QVBoxLayout *Calculate_layout;
    QCheckBox *Gradient_CheckBox;
    QCheckBox *Divergency_CheckBox;
    QCheckBox *QCriterion_CheckBox;
    QCheckBox *Vorticity_CheckBox;

public slots:
    /**
     * @brief Slot to change inputFields.
     * @param field name
     */
    virtual void ChangeInputField(QString name);
    /**
     * @brief Change the flags of the filter.
     */
    void UpdateGradientOptions();

};

#endif // GRADIENT_FILTER_H

#include "images.h"

Images::Images()
{
    volume = vtkSmartPointer<vtkVolume>::New();
}

Images::~Images()
{
    delete Images_prop_groupbox;
    foreach(QVBoxLayout *lay,List_Properties)
    {
        delete lay;
    }
    List_Properties.clear();
    Properties_names.clear();
}

Images::Images(QString Path_to_image,vtkSmartPointer<vtkRenderWindow> render_Wind)
{
    //Estas lineas son por un error en vtkFixedPointVolumeRayCastMapper
    vtkObjectFactory::RegisterFactory(vtkRenderingOpenGL2ObjectFactory::New());
    vtkObjectFactory::RegisterFactory(vtkRenderingVolumeOpenGL2ObjectFactory::New());
    //-------
    this->renderWindow = render_Wind;
    std::string path = Path_to_image.toStdString();
    fileName = path.c_str();
    std::string term = Path_to_image.mid(Path_to_image.lastIndexOf(".")).toStdString();

    if(term != ".mha" && term != ".vti" && term != ".xml")
    {
        dirname = term;
        std::cout << dirname << std::endl;
    }
    else if(term == ".mha")
    {
        fileType = MHA_FILETYPE;
    }
    else if(term == ".vti" || term == ".xml")
    {
        fileType = VTI_FILETYPE;
    }
    else
    {
        input = nullptr;
        return;
    }


    Name = Path_to_image.mid(Path_to_image.lastIndexOf("/") +1 ).toStdString();

    volume = vtkSmartPointer<vtkVolume>::New();
    mapper = vtkSmartPointer<vtkSmartVolumeMapper>::New();
    colorFun = vtkSmartPointer<vtkColorTransferFunction>::New();
    opacityFun = vtkSmartPointer<vtkPiecewiseFunction>::New();
    volumeProperty= vtkSmartPointer<vtkVolumeProperty>::New();
    volumeProperty->SetIndependentComponents(independentComponents);
    volumeProperty->SetColor(colorFun);
    volumeProperty->SetScalarOpacity(opacityFun);
    volumeProperty->SetInterpolationTypeToLinear();
    resample = vtkSmartPointer<vtkImageResample>::New();


    PropertiesConfig();

    ReadPath(Path_to_image);
    configVolumeRendering();

    //Volmapper->SetInputConnection();
    volume->SetProperty(volumeProperty);
    mapper->SetBlendModeToComposite();
    volume->SetMapper(mapper);
    mapper->Update();


}

void Images::ReadPath(QString path)
{
    if (!dirname.empty())
      {
        vtkNew<vtkDICOMImageReader> dicomReader;
        dicomReader->SetDirectoryName(dirname.c_str());
        dicomReader->Update();
        input = dicomReader->GetOutput();
        reader = dicomReader;
      }
      else if (fileType == VTI_FILETYPE)
      {
        vtkNew<vtkXMLImageDataReader> xmlReader;
        xmlReader->SetFileName(fileName);
        xmlReader->Update();
        input = xmlReader->GetOutput();
        reader = xmlReader;
      }
      else if (fileType == MHA_FILETYPE)
      {
        vtkNew<vtkMetaImageReader> metaReader;
        metaReader->SetFileName(fileName);
        metaReader->Update();
        input = metaReader->GetOutput();
        reader = metaReader;
      }
    // Verify that we actually have a volume
    int dim[3];
    input->GetDimensions(dim);

    if (dim[0] < 2 || dim[1] < 2 || dim[2] < 2)
    {
      cout << "Error loading data!" << endl;
      input = nullptr;
      return;
    }

}

void Images::configVolumeRendering()
{
    colorFun->RemoveAllPoints();
    opacityFun->RemoveAllPoints();
    volumeProperty->SetIndependentComponents(independentComponents);
    if (reductionFactor < 1.0)
    {
        resample->SetInputConnection(reader->GetOutputPort());
        resample->SetAxisMagnificationFactor(0, reductionFactor);
        resample->SetAxisMagnificationFactor(1, reductionFactor);
        resample->SetAxisMagnificationFactor(2, reductionFactor);
    }
    if (reductionFactor < 1.0)
    {
        mapper->SetInputConnection(resample->GetOutputPort());
    }
    else
    {
        mapper->SetInputConnection(reader->GetOutputPort());
    }
    double spacing[3];
    if (reductionFactor < 1.0)
    {
      resample->GetOutput()->GetSpacing(spacing);
    }
    else
    {
      input->GetSpacing(spacing);
    }
    switch (blendType)
    {
    // MIP
    // Create an opacity ramp from the window and level values.
    // Color is white. Blending is MIP.
    case 0:
        colorFun->AddRGBSegment(0.0, 1.0, 1.0, 1.0, 255.0, 1.0, 1.0, 1.0);
        opacityFun->AddSegment(opacityLevel - 0.5 * opacityWindow, 0.0,
                               opacityLevel + 0.5 * opacityWindow, 1.0);
        mapper->SetBlendModeToMaximumIntensity();
        break;

        // CompositeRamp
        // Create a ramp from the window and level values. Use compositing
        // without shading. Color is a ramp from black to white.
    case 1:
        colorFun->AddRGBSegment(opacityLevel - 0.5 * opacityWindow, 0.0, 0.0, 0.0,
                                opacityLevel + 0.5 * opacityWindow, 1.0, 1.0, 1.0);
        opacityFun->AddSegment(opacityLevel - 0.5 * opacityWindow, 0.0,
                               opacityLevel + 0.5 * opacityWindow, 1.0);
        mapper->SetBlendModeToComposite();
        volumeProperty->ShadeOff();
        break;

        // CompositeShadeRamp
        // Create a ramp from the window and level values. Use compositing
        // with shading. Color is white.
    case 2:
        colorFun->AddRGBSegment(0.0, 1.0, 1.0, 1.0, 255.0, 1.0, 1.0, 1.0);
        opacityFun->AddSegment(opacityLevel - 0.5 * opacityWindow, 0.0,
                               opacityLevel + 0.5 * opacityWindow, 1.0);
        mapper->SetBlendModeToComposite();
        volumeProperty->ShadeOn();
        break;

        // CT_Skin
        // Use compositing and functions set to highlight skin in CT data
        // Not for use on RGB data
    case 3:
        colorFun->AddRGBPoint(-3024, 0, 0, 0, 0.5, 0.0);
        colorFun->AddRGBPoint(-1000, .62, .36, .18, 0.5, 0.0);
        colorFun->AddRGBPoint(-500, .88, .60, .29, 0.33, 0.45);
        colorFun->AddRGBPoint(3071, .83, .66, 1, 0.5, 0.0);

        opacityFun->AddPoint(-3024, 0, 0.5, 0.0);
        opacityFun->AddPoint(-1000, 0, 0.5, 0.0);
        opacityFun->AddPoint(-500, 1.0, 0.33, 0.45);
        opacityFun->AddPoint(3071, 1.0, 0.5, 0.0);

        mapper->SetBlendModeToComposite();
        volumeProperty->ShadeOn();
        volumeProperty->SetAmbient(0.1);
        volumeProperty->SetDiffuse(0.9);
        volumeProperty->SetSpecular(0.2);
        volumeProperty->SetSpecularPower(10.0);
        volumeProperty->SetScalarOpacityUnitDistance(0.8919);
        break;

        // CT_Bone
        // Use compositing and functions set to highlight bone in CT data
        // Not for use on RGB data
    case 4:
        colorFun->AddRGBPoint(-3024, 0, 0, 0, 0.5, 0.0);
        colorFun->AddRGBPoint(-16, 0.73, 0.25, 0.30, 0.49, .61);
        colorFun->AddRGBPoint(641, .90, .82, .56, .5, 0.0);
        colorFun->AddRGBPoint(3071, 1, 1, 1, .5, 0.0);

        opacityFun->AddPoint(-3024, 0, 0.5, 0.0);
        opacityFun->AddPoint(-16, 0, .49, .61);
        opacityFun->AddPoint(641, .72, .5, 0.0);
        opacityFun->AddPoint(3071, .71, 0.5, 0.0);

        mapper->SetBlendModeToComposite();
        volumeProperty->ShadeOn();
        volumeProperty->SetAmbient(0.1);
        volumeProperty->SetDiffuse(0.9);
        volumeProperty->SetSpecular(0.2);
        volumeProperty->SetSpecularPower(10.0);
        volumeProperty->SetScalarOpacityUnitDistance(0.8919);
        break;

        // CT_Muscle
        // Use compositing and functions set to highlight muscle in CT data
        // Not for use on RGB data
    case 5:
        colorFun->AddRGBPoint(-3024, 0, 0, 0, 0.5, 0.0);
        colorFun->AddRGBPoint(-155, .55, .25, .15, 0.5, .92);
        colorFun->AddRGBPoint(217, .88, .60, .29, 0.33, 0.45);
        colorFun->AddRGBPoint(420, 1, .94, .95, 0.5, 0.0);
        colorFun->AddRGBPoint(3071, .83, .66, 1, 0.5, 0.0);

        opacityFun->AddPoint(-3024, 0, 0.5, 0.0);
        opacityFun->AddPoint(-155, 0, 0.5, 0.92);
        opacityFun->AddPoint(217, .68, 0.33, 0.45);
        opacityFun->AddPoint(420, .83, 0.5, 0.0);
        opacityFun->AddPoint(3071, .80, 0.5, 0.0);

        mapper->SetBlendModeToComposite();
        volumeProperty->ShadeOn();
        volumeProperty->SetAmbient(0.1);
        volumeProperty->SetDiffuse(0.9);
        volumeProperty->SetSpecular(0.2);
        volumeProperty->SetSpecularPower(10.0);
        volumeProperty->SetScalarOpacityUnitDistance(0.8919);
        break;

        // RGB_Composite
        // Use compositing and functions set to highlight red/green/blue regions
        // in RGB data. Not for use on single component data
    case 6:
        opacityFun->AddPoint(0, 0.0);
        opacityFun->AddPoint(5.0, 0.0);
        opacityFun->AddPoint(30.0, 0.05);
        opacityFun->AddPoint(31.0, 0.0);
        opacityFun->AddPoint(90.0, 0.0);
        opacityFun->AddPoint(100.0, 0.3);
        opacityFun->AddPoint(110.0, 0.0);
        opacityFun->AddPoint(190.0, 0.0);
        opacityFun->AddPoint(200.0, 0.4);
        opacityFun->AddPoint(210.0, 0.0);
        opacityFun->AddPoint(245.0, 0.0);
        opacityFun->AddPoint(255.0, 0.5);

        mapper->SetBlendModeToComposite();
        volumeProperty->ShadeOff();
        volumeProperty->SetScalarOpacityUnitDistance(1.0);
        break;
    default:
        vtkGenericWarningMacro("Unknown blend type.");
        break;
    }
    mapper->Update();

}

void Images::PropertiesConfig()
{
    Images_prop_groupbox = new QGroupBox;
    Images_prop_groupbox->setTitle("Blend Type");
    Images_prop_groupbox->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    Images_prop_layout = new QGridLayout(Images_prop_groupbox);
    Blend_type_comboBox = new QComboBox(Images_prop_groupbox);

    Blend_type_comboBox->addItem(QString::fromStdString("White"));
    Blend_type_comboBox->addItem(QString::fromStdString("Black to White"));
    Blend_type_comboBox->addItem(QString::fromStdString("White whit shading"));
    Blend_type_comboBox->addItem(QString::fromStdString("CT Skin"));
    Blend_type_comboBox->addItem(QString::fromStdString("CT Bone"));
    Blend_type_comboBox->addItem(QString::fromStdString("CT Muscle"));
    Blend_type_comboBox->addItem(QString::fromStdString("Highlight Red, Green and Blue"));

    Blend_type_comboBox->setCurrentIndex(0);

    Images_prop_layout->addWidget(Blend_type_comboBox);
    Images_prop_groupbox->setLayout(Images_prop_layout);

    //Opacity
    Opacity_box = new QGroupBox;
    Opacity_layout = new QGridLayout(Opacity_box);
    Opacity_slider = new QSlider(Opacity_box);
    //Opacity_LineEdit = new QLineEdit(Opacity_box);

    //Opacity
    Opacity_box->setTitle("ScalarOpacityUnitDistance");
    Opacity_box->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    Opacity_box->setMaximumHeight(100);
    Opacity_slider->setRange(0,10000);
    Opacity_slider->valueChanged(1);
    Opacity_slider->setValue(1);
    Opacity_slider->setOrientation(Qt::Orientation::Horizontal);

    OpacityValue_LineEdit = new QLineEdit(Opacity_box);
    OpacityValue_LineEdit->setValidator(new QIntValidator(0,10000));
    OpacityValue_LineEdit->setText("1");

    Opacity_layout->addWidget(Opacity_slider,0,0,0,2);
    Opacity_layout->addWidget(OpacityValue_LineEdit,0,2,0,1);

    Opacity_box->setLayout(Opacity_layout);



    QObject::connect(OpacityValue_LineEdit,SIGNAL(editingFinished()),this,SLOT(OpacityLineEditChanged()));

    QSlider::connect(Opacity_slider,SIGNAL(sliderReleased()),this,SLOT(UpdateOpacity()));

    QVBoxLayout *Property = new QVBoxLayout;
    Property->addWidget(Images_prop_groupbox);
    Property->addWidget(Opacity_box);
    Property->setAlignment(Qt::AlignmentFlag::AlignTop);

    List_Properties.push_back(Property);
    Properties_names.push_back("Images Prop.");

    QObject::connect(Blend_type_comboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(ReadConfig()));
}
void Images::ReadConfig()
{
    blendType = Blend_type_comboBox->currentIndex();
    configVolumeRendering();
}

void Images::UpdateOpacity()
{
    int val = Opacity_slider->value();
    OpacityValue_LineEdit->setText(QString::fromStdString(std::to_string(val)));
    this->volumeProperty->SetScalarOpacityUnitDistance(val);
    //this->volume->GetProperty()->SetOpacity(val/100);
    renderWindow->Render();
}

void Images::OpacityLineEditChanged()
{
    if(OpacityValue_LineEdit->text().isEmpty())
        return;
    int val = OpacityValue_LineEdit->text().toInt();
    this->volumeProperty->SetScalarOpacityUnitDistance(val);
    Opacity_slider->setValue(val);
}

#ifndef IMAGES_H
#define IMAGES_H

#include "geometry.h"
#include <vtkObjectFactory.h>

#include <vtkRenderingOpenGL2ObjectFactory.h>
#include <vtkRenderingVolumeOpenGL2ObjectFactory.h>
#include <vtkBoxWidget.h>
#include <vtkCamera.h>
#include <vtkColorTransferFunction.h>
#include <vtkCommand.h>
#include <vtkDICOMImageReader.h>
#include <vtkFixedPointVolumeRayCastMapper.h>
#include <vtkSmartVolumeMapper.h>
#include <vtkImageData.h>
#include <vtkImageResample.h>
#include <vtkMetaImageReader.h>
#include <vtkGPUVolumeRayCastMapper.h>
#include <vtkOpenGLGPUVolumeRayCastMapper.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkPiecewiseFunction.h>
#include <vtkPlanes.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkVolume.h>
#include <vtkVolumeProperty.h>
#include <vtkXMLImageDataReader.h>
#include <vtkSTLReader.h>
#include <vtkNrrdReader.h>
#include <vtkAutoInit.h>
#include <vtkRayCastImageDisplayHelper.h>

#include <QString>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QPointF>

#define VTI_FILETYPE 1
#define MHA_FILETYPE 2

/**
 * @brief class implemented for rendering volume of images.
 */
class Images : public QObject
{
    Q_OBJECT
public:
    //Constructor
    Images();

    Images(QString Path_to_image,vtkSmartPointer<vtkRenderWindow> render_Win);
    //~Destructor
    ~Images();

    vtkSmartPointer<vtkRenderWindow> renderWindow;
    std::string Name;

    void ReadPath(QString path);

    vtkSmartPointer<vtkAlgorithm> reader;
    vtkSmartPointer<vtkImageData> input;
    // Create our volume and mapper
    vtkSmartPointer<vtkVolume> volume;
    vtkSmartPointer<vtkSmartVolumeMapper> mapper;
    vtkSmartPointer<vtkColorTransferFunction> colorFun;
    vtkSmartPointer<vtkPiecewiseFunction> opacityFun;
    vtkSmartPointer<vtkVolumeProperty> volumeProperty;
    vtkSmartPointer<vtkImageResample> resample;

    void configVolumeRendering();
    void PropertiesConfig();
    //Variables
    std::string dirname;
    double opacityWindow = 4096;
    double opacityLevel = 2048;
    int blendType = 0;
    int clip = 0;
    double reductionFactor = 1.0;
    double frameRate = 10.0;
    const char* fileName = 0;
    int fileType = 0;

    bool independentComponents = true;

    //Variable atributes
    std::vector<std::string> Properties_names;
    std::vector<QVBoxLayout *> List_Properties;

    QGroupBox *Images_prop_groupbox;
    QGridLayout *Images_prop_layout;
    QComboBox *Blend_type_comboBox;
    QLineEdit *OpacityValue_LineEdit;

    //Opacity
    QGroupBox *Opacity_box;
    QGridLayout *Opacity_layout;
    QSlider *Opacity_slider;
    QLineEdit *Opacity_LineEdit;


public slots:
    void ReadConfig();
    void UpdateOpacity();
    void OpacityLineEditChanged();
};

#endif // IMAGES_H

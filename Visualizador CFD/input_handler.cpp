#include "input_handler.h"

Input_handler::Input_handler()
{
    //Create for 1 input
    SelectedInput.resize(1);
    SelectedInput[0]=0;
    InputTimeStep=0;
    numberOfInputsToUse=1;
    Input_Box = new QGroupBox;
    Input_Box->setTitle("Input");
    Input_Box->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    Input_Layout = new QGridLayout;
    labels.push_back(new QLabel(Input_Box));
    ComboBoxes.push_back(new QComboBox(Input_Box));
    Input_Box->setMaximumHeight(70);
    int j=0;
    Input_Layout->addWidget(labels[j],j,0,j,0);
    Input_Layout->addWidget(ComboBoxes[j],j,1,j,2);
    Input_Box->setLayout(Input_Layout);
}

Input_handler::Input_handler(int i)
{
    //Create for i input
    if(i<=0)
    {
        i=1;
    }
    SelectedInput.resize(i);
    for(auto j=0;j<i;j++)
    {
        SelectedInput[j]=0;
    }
    InputTimeStep=0;
    numberOfInputsToUse=i;
    Input_Box = new QGroupBox;
    Input_Box->setTitle("Input");
    Input_Box->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    Input_Layout = new QGridLayout;
    Input_Box->setMaximumHeight(70 + 50*(i-1));

    for(int j=0;j<i;j++)
    {
        labels.push_back(new QLabel(Input_Box));
        ComboBoxes.push_back(new QComboBox(Input_Box));
        Input_Layout->addWidget(labels[j],j*2,0,j*2+1,0);
        Input_Layout->addWidget(ComboBoxes[j],j*2,1,j*2+1,2);
    }
    Input_Box->setLayout(Input_Layout);

}

Input_handler::Input_handler(int i, std::vector<Field *> Field_List)
{
    SelectedInput.resize(i);
    for(auto j=0;j<i;j++)
    {
        SelectedInput[j]=0;
    }
    InputTimeStep=0;
    numberOfInputsToUse=i;
    Input_Box = new QGroupBox;
    Input_Layout = new QGridLayout;
    Input_Box->setTitle("Input");
    Input_Box->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    Input_Box->setMaximumHeight(50*i);
    for(int j=0;j<i;j++)
    {

        labels.push_back(new QLabel(Input_Box));
        ComboBoxes.push_back(new QComboBox(Input_Box));
        Input_Layout->addWidget(labels[j],j*2,0,j*2,0);
        Input_Layout->addWidget(ComboBoxes[j],j*2,1,j*2,2);
    }
    Input_Box->setLayout(Input_Layout);
    AddAllFields(Field_List);
}

Input_handler::~Input_handler()
{
    delete Input_Box;
    for(unsigned long i=0;i<Inputs_Fields.size();i++)
    {
        delete Inputs_Fields[i];
    }
    Inputs_Fields.clear();
    SelectedInput.clear();

}

void Input_handler::AddField(Field *FieldToAdd)
{
    if(FieldToAdd->type == Solid)
        return;
    Inputs_Fields.push_back(new Field(FieldToAdd));
}
void Input_handler::AddAllFields(std::vector<Field *> Field_List)
{
    for(unsigned long i=0; i<Field_List.size();i++)
    {
        AddField(Field_List[i]);
    }
}
void Input_handler::AddAllFields(std::vector<Field *> Field_List, Type_of_Field Type)
{
    for(unsigned long i=0; i<Field_List.size();i++)
    {
        if(Field_List[i]->type==Type)
        {
            AddField(Field_List[i]);
        }
    }
}


void Input_handler::UpdateInputHandler(std::vector<Field *> Field_List)
{
    while(Inputs_Fields.size()!=0)
    {
        delete Inputs_Fields[0];
        Inputs_Fields.erase(Inputs_Fields.begin());
    }
    AddAllFields(Field_List);
}

void Input_handler::UpdateInputHandler(std::vector<Field *> Field_List, Type_of_Field Type)
{
    while(Inputs_Fields.size()!=0)
    {
        delete Inputs_Fields[0];
        Inputs_Fields.erase(Inputs_Fields.begin());
    }
    AddAllFields(Field_List,Type);
}

std::vector<std::string> Input_handler::GetFieldsNames()
{
    std::vector<std::string> output;
    for(unsigned long i=0;i<Inputs_Fields.size();i++)
    {
        output.push_back(Inputs_Fields[i]->Field_Name);
    }
    return output;
}
std::vector<std::string> Input_handler::GetVectorsFieldsNames()
{
    std::vector<std::string> output;
    for(unsigned long i=0;i<Inputs_Fields.size();i++)
    {
        if(Inputs_Fields[i]->type==Vector)
        {
            output.push_back(Inputs_Fields[i]->Field_Name);
        }
    }
    return output;
}
std::vector<std::string> Input_handler::GetScalarFieldsNames()
{
    std::vector<std::string> output;
    for(unsigned long i=0;i<Inputs_Fields.size();i++)
    {
        if(Inputs_Fields[i]->type==Scalar)
        {
            output.push_back(Inputs_Fields[i]->Field_Name);
        }
    }
    return output;
}
std::string Input_handler::GetSelectedFieldName()
{
    return Inputs_Fields[SelectedInput[0]]->Field_Name;
}
std::string Input_handler::GetSelectedFieldName(int i)
{
    if(i>=numberOfInputsToUse)
        return NULL;
    return Inputs_Fields[SelectedInput[i]]->Field_Name;
}
std::string Input_handler::GetSelectedFieldMagnitudName()
{
    return Inputs_Fields[SelectedInput[0]]->Solution_Magnitud[0]->GetName();
}
std::string Input_handler::GetSelectedFieldMagnitudName(int i)
{
    if(i>=numberOfInputsToUse)
        return NULL;
    return Inputs_Fields[SelectedInput[i]]->Solution_Magnitud[0]->GetName();
}
Field *Input_handler::GetSelectedField()
{
    return Inputs_Fields[SelectedInput[0]];
}

Field *Input_handler::GetSelectedField(int i)
{
    if(i>=numberOfInputsToUse)
        return nullptr;
    return Inputs_Fields[SelectedInput[i]];
}

Field *Input_handler::GetSelectedField(std::string name)
{
    unsigned long i=0;
    while (i<Inputs_Fields.size() || Inputs_Fields[i]->Field_Name != name)
    {
        i++;
    }
    if(i== Inputs_Fields.size())
        return nullptr;
    return Inputs_Fields[i];
}

vtkSmartPointer<vtkDoubleArray> Input_handler::GetSelectedSolution()
{
    if(Inputs_Fields.empty())
        return nullptr;
    if(Inputs_Fields[SelectedInput[0]]->Solution.size()>InputTimeStep)
        return Inputs_Fields[SelectedInput[0]]->Solution[InputTimeStep];
    return Inputs_Fields[SelectedInput[0]]->Solution[0];
}
vtkSmartPointer<vtkDoubleArray> Input_handler::GetSelectedSolutionMagnitud()
{    if(Inputs_Fields.empty())
        return nullptr;
    if(Inputs_Fields[SelectedInput[0]]->Solution_Magnitud.size()>InputTimeStep)
        return Inputs_Fields[SelectedInput[0]]->Solution_Magnitud[InputTimeStep];
    return Inputs_Fields[SelectedInput[0]]->Solution_Magnitud[0];
}
vtkSmartPointer<vtkDoubleArray> Input_handler::GetSelectedSolutionComponent()
{
    if(Inputs_Fields.empty())
        return nullptr;
    if(Inputs_Fields[SelectedInput[0]]->Solution.size()>InputTimeStep)
        return Inputs_Fields[SelectedInput[0]]->Solution[InputTimeStep];
    int Selec = Inputs_Fields[SelectedInput[0]]->selectedComponent;
    return Inputs_Fields[SelectedInput[0]]->Solution_components[0][Selec];
}
vtkSmartPointer<vtkDoubleArray> Input_handler::GetSelectedSolution(int i)
{
    if(i>=numberOfInputsToUse)
        return NULL;
    if(Inputs_Fields.empty())
        return nullptr;
    if(Inputs_Fields[SelectedInput[i]]->Solution.size()>InputTimeStep)
        return Inputs_Fields[SelectedInput[i]]->Solution[InputTimeStep];
    return Inputs_Fields[SelectedInput[i]]->Solution[0];
}

vtkSmartPointer<vtkDoubleArray> Input_handler::GetSolution(int Field, int TimeStep)
{
    if((unsigned long)Field>= Inputs_Fields.size())
        return NULL;
    if((int)Inputs_Fields[Field]->Solution.size()<=TimeStep)
        return NULL;
    return Inputs_Fields[Field]->Solution[TimeStep];
}

vtkSmartPointer<vtkDoubleArray> Input_handler::GetSolution(std::string name, int TimeStep)
{
    for(unsigned long i=0; i<Inputs_Fields.size();i++)
        if(Inputs_Fields[i]->Field_Name==name)
            return GetSolution(i,TimeStep);
    return NULL;
}
void Input_handler::SetSelectedInput(int Selection)
{
    if((unsigned long)Selection >=Inputs_Fields.size())
        return;
    SelectedInput[0]=Selection;
}
void Input_handler::SetSelectedInput(int Selection,int input)
{
    if(input>=numberOfInputsToUse)
        return;
    if((unsigned long)Selection >=Inputs_Fields.size())
        return;
    SelectedInput[input]=Selection;
}
void Input_handler::SetInputTimeStep(int time)
{
     InputTimeStep=time;
}
bool Input_handler::SetSelectedInput(std::string Input_name)
{
    if(Input_name.empty())
        return false;
    unsigned long i=0;
    while (i<Inputs_Fields.size() && Inputs_Fields[i]->Field_Name!=Input_name)
    {
        i++;
    }
    if(Inputs_Fields.size()==i)
        return false;
    SelectedInput[0]=i;
    return true;
}

bool Input_handler::SetSelectedInput(int input,std::string Input_name)
{
    if(input>=numberOfInputsToUse)
        return false;
    if(Input_name.empty())
        return false;
    unsigned long i=0;
    while (i<Inputs_Fields.size() && Inputs_Fields[i]->Field_Name!=Input_name)
    {
        i++;
    }
    if(Inputs_Fields.size()==i)
        return false;
    SelectedInput[input]=i;
    return true;
}

void Input_handler::AddQBoxToLayout(QLayout *lay)
{
    lay->addWidget(Input_Box);
}

void Input_handler::SetInputName(int input_number, std::string name)
{
    labels[input_number]->setText(QString::fromStdString(name));
}

void Input_handler::AddItemsToComboBox(int input_number)
{
    if(input_number>=numberOfInputsToUse)
        return;
    for(unsigned long i=0; i<Inputs_Fields.size();i++)
    {
        ComboBoxes[input_number]->addItem(QString::fromStdString(Inputs_Fields[i]->Field_Name));
    }
}
void Input_handler::AddItemsToComboBox(int input_number, Type_of_Field type)
{
    if(input_number>=numberOfInputsToUse)
        return;
    for(unsigned long i=0; i<Inputs_Fields.size();i++)
    {
        if(Inputs_Fields[i]->type==type)
        {
            ComboBoxes[input_number]->addItem(QString::fromStdString(Inputs_Fields[i]->Field_Name));
        }
    }
}

void Input_handler::RemoveItemsFromComboBox()
{
    for(unsigned long i=0;i<ComboBoxes.size();i++)
    {
        while(ComboBoxes[i]->count()!=0)
        {
            ComboBoxes[i]->removeItem(0);
        }
    }
}

void Input_handler::SetComboBoxIndex(int comboBox, int index)
{
    if(comboBox>=numberOfInputsToUse)
        return;
    if(index >= ComboBoxes[comboBox]->count())
        return;
    ComboBoxes[comboBox]->setCurrentIndex(index);
}

void Input_handler::SetComboBoxIndex(int comboBox, std::string name)
{
    if(comboBox>=numberOfInputsToUse)
        return;
    int size=ComboBoxes[comboBox]->count();
    for(int i=0;i<size;i++)
    {
        if(ComboBoxes[comboBox]->itemText(i).toStdString()==name)
        {
            ComboBoxes[comboBox]->setCurrentIndex(i);
            break;
        }
    }
}

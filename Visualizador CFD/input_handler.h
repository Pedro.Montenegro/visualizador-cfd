#ifndef INPUT_HANDLER_H
#define INPUT_HANDLER_H

#include "field.h"

#include <QLayout>
#include <QGridLayout>
#include <QGroupBox>
#include <QLabel>
#include <QComboBox>

/**
 * @brief Clase para manejar los input de los filtros.
 */
class Input_handler : public QObject
{
    Q_OBJECT
public:
    //Base constructor
    Input_handler();
    /**
     * @brief Constructor for number of inputs
     * @param i = Number of inputs of the filter.
     */
    Input_handler(int i);
    /**
     * @brief Constructor for number of inputs and all fields in FieldList
     * @param i = Number of inputs of the filter.
     * @param Field_List
     */
    Input_handler(int i, std::vector<Field *> Field_List);
    //Destructor
    ~Input_handler();

    //Functions over field
    /**
     * @brief Agrega un campo a Inputs_Fields. Crea una copia del campo
     * @param FieldToAdd
     */
    void AddField(Field *FieldToAdd);
    /**
     * @brief Agrega una lista de campos a Inputs_Fields-
     * @param Field_List
     */
    void AddAllFields(std::vector<Field *> Field_List);
    /**
     * @brief Agrega una lista de campos a Inputs_Fields. SOLO agrega los del tipo "type"
     * @param Field_List
     * @param type == tipo de campos a agregar
     */
    void AddAllFields(std::vector<Field *> Field_List,Type_of_Field type);
    /**
     * @brief remove the olds fields and change them for new ones.
     * @param Field_List
     */
    void UpdateInputHandler(std::vector<Field *> Field_List);
    /**
     * @brief remove the olds fields and change them for new ones of the input type.
     * @param Field_List
     */
    void UpdateInputHandler(std::vector<Field *> Field_List, Type_of_Field type);

    //Get Functions

    //get names
    /**
     * @brief GetFieldsNames
     * @return Retunr a list with the names of all fields in "input_Fields"
     */
    std::vector<std::string> GetFieldsNames();
    /**
     * @brief GetVectorsFieldsNames
     * @return Return a list with the names of all fields in "input_Fields" of the type Vector
     */
    std::vector<std::string> GetVectorsFieldsNames();
    /**
     * @brief GetScalarFieldsNames
     * @return Return a list with the names of all fields in "input_Fields" of the type Scalar
     */
    std::vector<std::string> GetScalarFieldsNames();
    /**
     * @brief GetSelectedFieldName
     * @return Return the field name of the input "0". Default for most filters
     */
    std::string GetSelectedFieldName();
    /**
     * @brief GetSelectedFieldName
     * @param input number
     * @return Return the field name of the input number. Return NULL if input > NumberOfInputs
     */
    std::string GetSelectedFieldName(int input);
    /**
     * @brief GetSelectedFieldMagnitudName
     * @return Return the field magnitud name of the input "0". Default for most filters
     */
    std::string GetSelectedFieldMagnitudName();
    /**
     * @brief GetSelectedFieldMagnitudName
     * @return Return the field magnitud name of the input number. Return NULL if input > NumberOfInputs
     */
    std::string GetSelectedFieldMagnitudName(int input);

    //get fields
    /**
     * @brief GetSelectedField
     * @return Return the selected Field of the input "0". Default for most filters
     */
    Field *GetSelectedField();
    /**
     * @brief GetSelectedField
     * @return Return the selected Field of the input number. Return nullptr if input > NumberOfInputs
     * @return
     */
    Field *GetSelectedField(int input);
    /**
     * @brief GetSelectedField
     * @param name
     * @return Return the Field with this name. Return nullptr if there isn't any field with this name
     */
    Field *GetSelectedField(std::string name);

    //get solutions

    /**
     * @brief GetSelectedSolution
     * @return Return the solution of the selected field "0" and inputTimeStep.
     */
    vtkSmartPointer<vtkDoubleArray> GetSelectedSolution();
    /**
     * @brief GetSelectedSolutionMagnitud
     * @return Return the solution Magnitud of the selected field "0" and inputTimeStep.
     */
    vtkSmartPointer<vtkDoubleArray> GetSelectedSolutionMagnitud();
    /**
     * @return Return the solution selected component of the selected field "0" and inputTimeStep.
     * @return
     */
    vtkSmartPointer<vtkDoubleArray> GetSelectedSolutionComponent();
    /**
     * @brief GetSelectedSolution
     * @return Return the solution of the selected field input and inputTimeStep.
     */
    vtkSmartPointer<vtkDoubleArray> GetSelectedSolution(int input);
    /**
     * @brief GetSelectedSolutionMagnitud
     * @param Field number
     * @param timeStep
     * @return Return the solution Magnitud of the selected field input and TimeStep.
     */
    vtkSmartPointer<vtkDoubleArray> GetSolution(int Field,int TimeStep);
    /**
     * @brief GetSolution with this field name at this TimeStep
     * @param name
     * @param TimeStep
     * @return Solution with this name and timestep
     */
    vtkSmartPointer<vtkDoubleArray> GetSolution(std::string name, int TimeStep);

    //Set Functions
    /**
     * @brief Selected the input field for the input "0" (default).
     * @param Selection is the index of the selected field in "Input_fields".
     */
    void SetSelectedInput(int Selection);
    /**
     * @brief Selected the input field for the input.
     * @param input -> Input number of the filter
     * @param Selection is the index of the selected field in "Input_fields".
     */
    void SetSelectedInput(int input,int Selection);
    /**
     * @brief Change the "inputTimeStep"
     * @param time
     */
    void SetInputTimeStep(int time);
    /**
     * @brief Set the selected input of the input number "0".
     * @param Input_name. The name is search in the "Input_fields"
     * @return true if there is a field with this name. False otherwise
     */
    bool SetSelectedInput(std::string Input_name);
    /**
     * @brief Set the selected input of the input number.
     * @param input
     * @param Input_name. The name is search in the "Input_fields"
     * @return true if there is a field with this name. False otherwise
     */
    bool SetSelectedInput(int input, std::string Input_name);

    //Functions over interfase
    /**
     * @brief Add the QGroupBox to a QLayout.
     * @param lay == layout
     */
    void AddQBoxToLayout(QLayout *lay);
    /**
     * @brief Set the name of the QLabel for the input
     * @param input_number
     * @param name
     */
    void SetInputName(int input_number,std::string name);
    /**
     * @brief Add the fields that match the type to the selected combo box
     * @param input_number ==  selected combo box
     * @param type
     */
    void AddItemsToComboBox(int input_number,Type_of_Field type);
    /**
     * @brief Add all the fields to the combo box number
     * @param input_number ==  selected combo box
     */
    void AddItemsToComboBox(int input_number);
    /**
     * @brief RemoveItemsFromComboBox
     */
    void RemoveItemsFromComboBox();
    /**
     * @brief SetComboBoxIndex.
     * @param comboBox == selected input
     * @param index -> field index in Inputs_Fields
     */
    void SetComboBoxIndex(int comboBox,int index);
    /**
     * @brief SetComboBoxIndex
     * @param comboBox == selected input
     * @param name -> field name in Inputs_Fields
     */
    void SetComboBoxIndex(int comboBox,std::string name);
    /**
     * @brief Vector de campos seleccionados. Su longitud es igual al numero de inputs.
     */
    std::vector<int> SelectedInput;

    std::vector<QComboBox*> ComboBoxes;

private:
    //Variables for the interfase
    QGroupBox *Input_Box;
    QGridLayout *Input_Layout;
    std::vector<QLabel*> labels;

    /**
     * @brief Contenedor de los campos que sirven como input
     */
    std::vector<Field *> Inputs_Fields;

    /**
     * @brief InputTimeStep
     */
    int InputTimeStep;
    /**
     * @brief numero de inputs
     */
    int numberOfInputsToUse;
};

#endif // INPUT_HANDLER_H

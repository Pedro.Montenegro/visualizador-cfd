#include "loadbar.h"
#include "ui_loadbar.h"

LoadBar::LoadBar(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoadBar)
{
    ui->setupUi(this);
    this->ui->progressBar->setRange(0,100);
    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    this->setWindowFlags(this->windowFlags() & ~Qt::WindowMinimizeButtonHint & ~Qt::WindowCloseButtonHint);
    NumberOfFiles=1;
}

LoadBar::~LoadBar()
{
    delete ui;
}

void LoadBar::SetValue(int i)
{
    this->ui->progressBar->setValue(100*i/NumberOfFiles);
}

void LoadBar::SetText(QString text)
{
    this->ui->label->setText(text);
}

void LoadBar::SetDialogTitle(QString title)
{
    this->setWindowTitle(title);
}

void LoadBar::SetNumberOfFiles(int i)
{
    this->NumberOfFiles = i;
}

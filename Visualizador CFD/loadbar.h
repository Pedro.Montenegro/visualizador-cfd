#ifndef LOADBAR_H
#define LOADBAR_H

#include <QDialog>

namespace Ui {
class LoadBar;
}
//Class implemented to see the progress of the GPFEPWriter and OpenFOAMWriter
class LoadBar : public QDialog
{
    Q_OBJECT

public:

    explicit LoadBar(QWidget *parent = nullptr);
    ~LoadBar();
    void SetDialogTitle(QString title);
    int NumberOfFiles;

private:
    Ui::LoadBar *ui;

public slots:
    void SetValue(int file_number);
    void SetText(QString text);
    void SetNumberOfFiles(int i);
};

#endif // LOADBAR_H

#include "mainwindow.h"

#include <QtGui/QSurfaceFormat>
#include <QtWidgets/QApplication>
#include "QVTKOpenGLStereoWidget.h"
#include <QValidator>

extern int qInitResources_icons();

//Main del programa
//Simplemente define que es una aplicacion de Qt y el tipo de formato
int main(int argc, char *argv[])
{
    QSurfaceFormat::setDefaultFormat(QVTKOpenGLStereoWidget::defaultFormat());

    QApplication a(argc, argv);
    QLocale curLocale(QLocale(QLocale::English,QLocale::UnitedStates));
    QLocale::setDefault(curLocale);
    QApplication::setStyle("fusion");
    setlocale(LC_NUMERIC,"C"); //Line added to fix bug in OpenFOAMReader
    qInitResources_icons();

    MainWindow w;
    w.showMaximized();

    return a.exec();
}


#include "mainwindow.h"
#include "./ui_mainwindow.h"

//Constructor
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ren = vtkSmartPointer<vtkRenderer>::New();
    autoUpdate = vtkSmartPointer<vtkEventQtSlotConnect>::New();
    renderWindow = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();
    FieldList = new QComboBox;
    TimeList = new QComboBox;
    ComponentsList = new QComboBox;
    qRegisterMetaType<QList<QString>>();


    GeometriesList = new QTreeWidget;
    GeometriesList->header()->hide();
    TreeHandeler = new GeometryTree(GeometriesList);
    ActorShow = new QComboBox;
    ActorShow->addItem("Surface");
    ActorShow->addItem("Surface with edges");

    ren->SetBackground(0.1,0.3,0.5);
    renderWindow->AddRenderer(ren);
    ui->QVtkWidget->setRenderWindow(renderWindow);

    ui->toolBar_3->addWidget(TimeList);
    ui->toolBar_3->addWidget(FieldList);
    ui->toolBar_3->addWidget(ComponentsList);
    ui->toolBar_3->addWidget(ActorShow);
    ui->tabWidget->removeTab(1);
    ui->tabWidget->removeTab(0);
    ui->tabWidget->insertTab(0,GeometriesList,"Geometries");
    ui->tabWidget->setCurrentIndex(0);

    QComboBox::connect(TimeList,SIGNAL(currentIndexChanged(int)),this,SLOT(ChangeTimeSolution(int)));
    QComboBox::connect(FieldList,SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeField(QString)));
    QTreeWidget::connect(GeometriesList,SIGNAL(itemSelectionChanged()),this,SLOT(itemPressed()));
    QTreeWidget::connect(GeometriesList,SIGNAL(itemChanged(QTreeWidgetItem*,int)),this,SLOT(itemChanged(QTreeWidgetItem*,int)));
    QComboBox::connect(ActorShow,SIGNAL(currentIndexChanged(QString)),this,SLOT(ActorShowChange(QString)));
    QComboBox::connect(ComponentsList,SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeComponent(QString)));
    DisableEnable();
    //renderWindow->GetInteractor()->SetInteractorStyle()
    renderWindow->Render();

    this->colors = vtkSmartPointer<vtkNamedColors>::New();
    this->axes = vtkSmartPointer<vtkAxesActor>::New();
    this->axes->DragableOff();
    double rgba[4]{0.0, 0.0, 0.0, 0.0};
    this->colors->GetColor("Red",rgba);
    this->axes->GetXAxisShaftProperty()->SetColor(rgba);
    this->axes->GetXAxisTipProperty()->SetColor(rgba);
    this->colors->GetColor("Yellow",rgba);
    this->axes->GetYAxisShaftProperty()->SetColor(rgba);
    this->axes->GetYAxisTipProperty()->SetColor(rgba);
    this->colors->GetColor("Green",rgba);
    this->axes->GetZAxisShaftProperty()->SetColor(rgba);
    this->axes->GetZAxisTipProperty()->SetColor(rgba);

    this->widget_for_Axes = vtkSmartPointer<vtkOrientationMarkerWidget>::New();
    this->colors->GetColor("Carrot", rgba);
    this->widget_for_Axes->SetOrientationMarker(this->axes);
    this->widget_for_Axes->SetInteractor(this->renderWindow->GetInteractor());
    this->widget_for_Axes->SetViewport(0.0, 0.0, 0.2, 0.2);
    this->widget_for_Axes->SetOutlineColor(rgba[0], rgba[1], rgba[2]);
    this->widget_for_Axes->EnabledOn();
    this->widget_for_Axes->InteractiveOff();

    //detalles
    QList<QAction*> actions = ui->toolBar->actions();
    foreach (QAction* ac, actions)
    {
        if(ac->text()=="PlayAndPause Online Mode")
        {
            ac->setEnabled(false);
            ac->setCheckable(true);
        }
    }

    //Imagenes
    ui->tabWidget_2->setTabText(0,"Imagenes");
    ui->tabWidget_2->hide();
    ImagesListWidget = ui->tabWidget_2->findChild<QListWidget *>("ImagesListWidget");
    QObject::connect(ImagesListWidget,SIGNAL(itemSelectionChanged()),this,SLOT(ImagelistWidget_itemSelectionChanged()));


    //views
    QMenu* views_menu = this->menuBar()->findChild<QMenu *>("menuViews");

    Axes_item = new QAction(this);
    Axes_item->setText("Axes");
    Axes_item->setCheckable(true);
    Axes_item->setChecked(true);
    QObject::connect(Axes_item,&QAction::triggered,this,&MainWindow::HideOrShowAxesTab);
    views_menu->addAction(Axes_item);

    Geometry_item = new QAction(this);
    Geometry_item->setText("Geometries");
    Geometry_item->setCheckable(true);
    Geometry_item->setChecked(true);
    QObject::connect(Geometry_item,&QAction::triggered,this,&MainWindow::HideOrShowPropertiesTab);
    views_menu->addAction(Geometry_item);


    Images_item = new QAction(this);
    Images_item->setText("Images");
    Images_item->setCheckable(true);
    Images_item->setChecked(false);
    QObject::connect(Images_item,&QAction::triggered,this,&MainWindow::HideOrShowImagesTab);
    views_menu->addAction(Images_item);



}
//Destructor
MainWindow::~MainWindow()
{
    delete FieldList;
    delete TimeList;
    int i;
    for(i=Geometries.size()-1;i<=0;i--)
    {
        delete Geometries[i];
    }
    delete TreeHandeler;
    delete ui;
}

//Funciones que tienen que ver con la geometria y/o sus campos----------------------------------
//Carga la geometria.
void MainWindow::on_actionOpen_Geometry_triggered()
{
    QString file_name = QFileDialog::getOpenFileName(this,"Open geometry.vtu file","/","*.vtu");

    if(file_name.isEmpty())
        return;

    Geometry *NewGeometry = new Geometry;
    NewGeometry->ReadFile(file_name);
    NewGeometry->render_Win = this->renderWindow;
    unsigned long i;
    int a =1;
    std::string name(NewGeometry->Name);
    next:
    for(i=0;i<Geometries.size();i++)
    {
        if(NewGeometry->Name == Geometries[i]->Name)
        {
            NewGeometry->Name = name + std::to_string(a);
            a++;
            goto next;
        }
    }

    if(Geometries.size()!=0)
        Geometries[SelectedGeometry]->HideWidgets();

    Geometries.push_back(NewGeometry);

    SelectedGeometry = Geometries.size()-1;

    Geometries[SelectedGeometry]->ShowWidgets();

    ren->AddActor(NewGeometry->Actor);
    NewGeometry->SetScalarBarWidget();
    ren->ResetCamera(NewGeometry->GetBounds());
    renderWindow->Render();
    NewGeometry->type = Main;

    this->ui->tab_2->setLayout(Geometries[SelectedGeometry]->List_Properties[0]);
    QComboBox::disconnect(FieldList,SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeField(QString)));

    TreeHandeler->addTreeRoot(QString::fromStdString(NewGeometry->Name));

    UpdateInterface();

    QComboBox::connect(FieldList,SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeField(QString)));
    int num_of_geom = TreeHandeler->Tree->topLevelItemCount();
    TreeHandeler->Tree->selectionModel()->clearSelection();
    TreeHandeler->Tree->topLevelItem(num_of_geom - 1)->setSelected(true);

}
//Carga el campo sobre la geometria seleccionada.
void MainWindow::on_actionAdd_Field_triggered()
{
    if(Geometries.empty())
        return;
    QString field_name = QFileDialog::getOpenFileName(this,"Open field file","/");
    if(field_name.isEmpty())
        return;
    if(!Geometries[SelectedGeometry]->InsertNewField(field_name))
    {
        return;
    }

    field_name = field_name.mid(field_name.lastIndexOf("/") +1 );

    FieldList->addItem(field_name);
    FieldList->setCurrentIndex(FieldList->count()-1);
    UpdateInterface();
    renderWindow->Render();

}
//Cambia la solucion Actual
void MainWindow::ChangeField(QString Field_Name)
{
    Geometries[SelectedGeometry]->ChangeField(Field_Name.toStdString());
    UpdateComponentsList();
    renderWindow->Render();
}
//Muestra o oculta una geometria. i =0 oculta, i=1 muestra.
void MainWindow::HideOrShowGeometry(int state,int geo)
{
    if(geo<0 || geo>=(int)Geometries.size())
        return;

    Geometries[geo]->HideOrShow(state);
    Geometries[geo]->HideWidgets();
    renderWindow->Render();
}

//Cambia el paso de tiempo de la solucion al paso de tiempo seleccionado.
void MainWindow::ChangeTimeSolution(int i)
{
    Geometries[SelectedGeometry]->ChangeToThisTime(i);
    renderWindow->Render();
}

//Cambia el componente del campo que se esta observando
void MainWindow::ChangeComponent(QString Component_name)
{
    Geometries[SelectedGeometry]->ChangeComponent(Component_name.toStdString());
    renderWindow->Render();
}
//Cambia el paso de tiempo hacia atras.
void MainWindow::on_actionTime_Backwards_triggered()
{

    //Geometries[SelectedGeometry]->TimeBackward();
    if(TimeList->currentIndex()!=0)
    {
        TimeList->setCurrentIndex(TimeList->currentIndex()-1);
    }
    //renderWindow->Render();

}
//Cambia el paso de tiempo hacia adelante.
void MainWindow::on_actionTime_Fowards_triggered()
{

    //Geometries[SelectedGeometry]->TimeFoward();
    if(TimeList->currentIndex()!= TimeList->count()-1)
    {
        TimeList->setCurrentIndex(TimeList->currentIndex()+1);
    }
    //renderWindow->Render();
}

//End Funciones que tienen que ver con la geometria y/o sus campos----------------------------------

//Funciones que actualizan la interfaz -------------------------------------------------------------
//Actualiza la interfaz al seleccionar otra geometria.
void MainWindow::UpdateInterface()
{
    mute.lock();
    DisableEnable();
    UpdateTimeList();
    UpdateFieldList();
    UpdateComponentsList();
    UpdatePropertiesTabs();
    UpdateActorShow();
    //renderWindow->Render();
    mute.unlock();
}

void MainWindow::UpdateComponentsList()
{

    QComboBox::disconnect(ComponentsList,SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeComponent(QString)));
    while(ComponentsList->count()!=0)
    {
        ComponentsList->removeItem(0);
    }
    if(Geometries.empty())
    {
        QComboBox::connect(ComponentsList,SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeComponent(QString)));
        return;
    }
    if(Geometries[SelectedGeometry]->Field_list.empty())
    {
        QComboBox::connect(ComponentsList,SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeComponent(QString)));
        return;
    }
    if(Geometries[SelectedGeometry]->Field_list[Geometries[SelectedGeometry]->SelectedField]->type== Type_of_Field::Solid)
    {
        QComboBox::connect(ComponentsList,SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeComponent(QString)));
        return;
    }
    ComponentsList->addItem("Magnitud");
    if(Geometries[SelectedGeometry]->Field_list[Geometries[SelectedGeometry]->SelectedField]->Solution[0]->GetNumberOfComponents()==1)
    {
        QComboBox::connect(ComponentsList,SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeComponent(QString)));
        return;
    }
    for(int i=0;i<(int)Geometries[SelectedGeometry]->Field_list[Geometries[SelectedGeometry]->SelectedField]->Solution[0]->GetNumberOfComponents();i++)
    {
        char a = 'X'+i;
        QString aux = "";
        aux.append(a);
        ComponentsList->addItem(aux);
    }
    QComboBox::connect(ComponentsList,SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeComponent(QString)));
    if(Geometries[SelectedGeometry]->Field_list[Geometries[SelectedGeometry]->SelectedField]->selectedComponent == -1)
        ComponentsList->setCurrentIndex(0);
    else
    {
        ComponentsList->setCurrentIndex(Geometries[SelectedGeometry]->Field_list[Geometries[SelectedGeometry]->SelectedField]->selectedComponent+1);
    }

}
void MainWindow::itemPressed()
{
   unsigned long i;

   QList<QTreeWidgetItem *>items = GeometriesList->selectedItems();

   if(items.empty())
       return;
   ImagesListWidget->selectionModel()->clearSelection();

   QTreeWidgetItem* item = items.at(0);
   for(i=0;i<Geometries.size();i++)
   {
       if(Geometries[i]->Name == item->text(0).toStdString())
           break;
   }
   if(SelectedGeometry == i) return;

   Geometries[SelectedGeometry]->HideWidgets();

   SelectedGeometry=i;

   Geometries[SelectedGeometry]->ShowWidgets();

   UpdateInterface();

   renderWindow->Render();
}

//Actualiza la interfaz al cambiar al agregar geometria

void MainWindow::itemChanged(QTreeWidgetItem *item,int)
{
    unsigned long i;
    for(i=0;i<Geometries.size();i++)
    {
        if(item->text(0).toStdString() ==Geometries[i]->Name)
            break;
    }
    int j=0;
    if(item->checkState(0) == Qt::Checked)
    {
        j=1;
    }
    HideOrShowGeometry(j,i);
    renderWindow->Render();


}

//Rescala la visualizacion segun la magnitud del campo actual.
void MainWindow::on_actionRescaleToCurrentData_triggered()
{
    if(Geometries.empty())
        return;
    Geometries[SelectedGeometry]->ReScaleToCurrentData();
    renderWindow->Render();
}
//Rescala la visualizacion segun dos valores proporcionados.
void MainWindow::on_actionRescaleToCustom_triggered()
{
    if(Geometries.empty())
        return;
    CustomScale *message = new CustomScale;
    double *a;
    a=Geometries[SelectedGeometry]->Mapper->GetScalarRange();
    message->SetValues(a);
    int accepted = message->exec();
    if(!(accepted ==0))
    {
        Geometries[SelectedGeometry]->ReScaleToSetRange(message->minvalue,message->maxvalue);
    }
    else
    {

    }
    renderWindow->Render();
    delete message;

}
//Actualiza la informacion en la comboBox que guarda los pasos de tiempo.
void MainWindow::UpdateTimeList()
{
    vtkIdType i;
    QComboBox::disconnect(TimeList,SIGNAL(currentIndexChanged(int)),this,SLOT(ChangeTimeSolution(int)));


    while(TimeList->count()!=0)
    {
        TimeList->removeItem(0);
    }
    if(Geometries.size()==0)
        return;
    if(Geometries[SelectedGeometry]->Times->GetNumberOfValues() !=0)
    {
        for(i=0;i<Geometries[SelectedGeometry]->Times->GetNumberOfValues();i++)
        {
            TimeList->addItem(QString::fromStdString(std::to_string(Geometries[SelectedGeometry]->Times->GetVariantValue(i).ToDouble())));
        }
    }
    TimeList->setCurrentIndex(Geometries[SelectedGeometry]->CurrentTimeStep);

    QComboBox::connect(TimeList,SIGNAL(currentIndexChanged(int)),this,SLOT(ChangeTimeSolution(int)));


}
//Actualiza el comboBoxQue guarda la informacion de los campos.
void MainWindow::UpdateFieldList()
{
    unsigned long i;
    QComboBox::disconnect(FieldList,SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeField(QString)));


    while(FieldList->count()!=0)
    {
        FieldList->removeItem(0);
    }
    if(Geometries.size()==0)
        return;
    for(i=0;i<Geometries[SelectedGeometry]->Field_list.size();i++)
    {
        if(Geometries[SelectedGeometry]->Field_list[i]->Field_Name.find("Magnitud") != std::string::npos)
            continue;
        FieldList->addItem(QString::fromStdString(Geometries[SelectedGeometry]->Field_list[i]->Field_Name));
    }

    FieldList->setCurrentIndex(Geometries[SelectedGeometry]->SelectedField);
    Geometries[SelectedGeometry]->SetVectors();

    QComboBox::connect(FieldList,SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeField(QString)));
}

void MainWindow::UpdateActorShow()
{
    if(Geometries.size()==0)
        return;
   QComboBox::disconnect(ActorShow,SIGNAL(currentIndexChanged(QString)),this,SLOT(ActorShowChange(QString)));

   if(Geometries[SelectedGeometry]->edgeVisibility)
   {
       ActorShow->setCurrentIndex(1);
   }
   else
   {
       ActorShow->setCurrentIndex(0);
   }
   QComboBox::connect(ActorShow,SIGNAL(currentIndexChanged(QString)),this,SLOT(ActorShowChange(QString)));
}
//Actualiza las pestañas de las propiedades
void MainWindow::UpdatePropertiesTabs()
{
    int prev_index = ui->tabWidget->currentIndex();
    while(ui->tabWidget->count()>1)
    {
        ui->tabWidget->removeTab(ui->tabWidget->count()-1);
    }
    if(Geometries.size()==0)
        return;
    unsigned long i;
    for(i = 0; i < Geometries[SelectedGeometry]->List_Properties.size();i++)
    {
        QScrollArea *NewTab = new QScrollArea(ui->tabWidget);
        NewTab->setWidgetResizable(true);
        NewTab->setVerticalScrollBar(new QScrollBar(NewTab));
        NewTab->setVerticalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOn);
        NewTab->setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
        Geometries[SelectedGeometry]->List_Properties[i]->setSizeConstraint(QLayout::SetMinAndMaxSize);
        NewTab->setLayout(Geometries[SelectedGeometry]->List_Properties[i]);
        QWidget *container = new QWidget(NewTab);
        container->setLayout(Geometries[SelectedGeometry]->List_Properties[i]);
        NewTab->setWidget(container);
        this->ui->tabWidget->insertTab(i+1,NewTab,QString::fromStdString(Geometries[SelectedGeometry]->Properties_names[i]));
    }
    if(ui->tabWidget->count()>prev_index)
    {
        ui->tabWidget->setCurrentIndex(prev_index);
    }
    else
    {
        ui->tabWidget->setCurrentIndex(ui->tabWidget->count()-1);
    }
}

void MainWindow::UpdateTimeAndFields()
{
    mute.lock();
    UpdateTimeList();
    UpdateFieldList();
    UpdateComponentsList();
    //renderWindow->Render();
    mute.unlock();
}


//End Funciones que actualizan la interfaz ---------------------------------------------------------

void MainWindow::ActorShowChange(QString type)
{
    if(type =="Surface")
    {
        Geometries[SelectedGeometry]->Actor->GetProperty()->EdgeVisibilityOff();
        Geometries[SelectedGeometry]->edgeVisibility = false;
    }
    else if(type =="Surface with edges")
    {
        Geometries[SelectedGeometry]->Actor->GetProperty()->EdgeVisibilityOn();
        Geometries[SelectedGeometry]->edgeVisibility = true;
    }
    renderWindow->Render();
}
void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Delete)
    {
        QList<QTreeWidgetItem *> items = GeometriesList->selectedItems();
        if(items.size()==1)
        {
            DeleteGeometry(items.at(0));
        }
        QList<QListWidgetItem*> item_imag = ImagesListWidget->selectedItems();
        if(item_imag.size()==1)
        {
            DeleteImage(item_imag.at(0));
        }
    }
}
void MainWindow::DeleteGeometry(QTreeWidgetItem* item)
{
    if(item->childCount()!=0)//Solo elimino las geometrias sin hijos
        return;
    std::string geo_name = item->text(0).toStdString();
    unsigned long i=0;
    for(i=0;i<Geometries.size();i++)
    {
        if(Geometries[i]->Name==geo_name)
            break;
    }

    if(i==Geometries.size())
        return;

    //Finding father
    Geometry* A = Geometries[SelectedGeometry]->Input;
    if(A == NULL || A ==nullptr)
        goto skip;
    while(A->Input !=nullptr || A->Input != NULL )
    {
        A = A->Input;
    }
    if(A->type == Filter_type::FortranGPFEP)
    {
        FortranOnRealTime* a = (FortranOnRealTime*) A;
        if(a->Update_CheckBox->isChecked())
        {
            QMessageBox *error = new QMessageBox(QMessageBox::Warning,"Error","Solo se pueden eliminar filtros cuando el \"Auto Update\" esta desactivado");
            error->setWindowFlags(error->windowFlags() & ~Qt::WindowContextHelpButtonHint);
            error->setWindowFlags(error->windowFlags() & ~Qt::WindowMinimizeButtonHint);
            error->exec();
            delete error;
            return;
        }
    }
    else if(A->type == Filter_type::OpenFOAMcase)
    {
        OpenFOAMRunTime* a = (OpenFOAMRunTime*) A;
        if(a->Update_CheckBox->isChecked())
        {
            QMessageBox *error = new QMessageBox(QMessageBox::Warning,"Error","Solo se pueden eliminar filtros cuando el \"Auto Update\" esta desactivado");
            error->setWindowFlags(error->windowFlags() & ~Qt::WindowContextHelpButtonHint);
            error->setWindowFlags(error->windowFlags() & ~Qt::WindowMinimizeButtonHint);
            error->exec();
            delete error;
            return;
        }
    }
    skip:

    ren->RemoveActor(Geometries[SelectedGeometry]->Actor);

    if(Geometries[SelectedGeometry]->type == Filter_type::StreamLines)
    {
        Stream_Lines_Filter* a = (Stream_Lines_Filter*) Geometries[SelectedGeometry];
        ren->RemoveActor(a->GeneradorDeSemillas->PointSourceActor);
    }

    SelectedGeometry =0;

    QTreeWidget::disconnect(GeometriesList,SIGNAL(itemSelectionChanged()),this,SLOT(itemPressed()));
    QTreeWidget::disconnect(GeometriesList,SIGNAL(itemChanged(QTreeWidgetItem*,int)),this,SLOT(itemChanged(QTreeWidgetItem*,int)));

    delete Geometries[i];

    Geometries.erase(Geometries.begin()+i);

    TreeHandeler->RemoveItem(item);

    QTreeWidget::connect(GeometriesList,SIGNAL(itemSelectionChanged()),this,SLOT(itemPressed()));
    QTreeWidget::connect(GeometriesList,SIGNAL(itemChanged(QTreeWidgetItem*,int)),this,SLOT(itemChanged(QTreeWidgetItem*,int)));



    UpdateInterface();
    renderWindow->Render();

    int num_of_geom = TreeHandeler->Tree->topLevelItemCount();
    TreeHandeler->Tree->selectionModel()->clearSelection();
    if(num_of_geom == 0)
        return;
    TreeHandeler->Tree->topLevelItem(num_of_geom - 1)->setSelected(true);
}


//NO FUNCIONAL TODAVIA
void MainWindow::on_actionChangeColor_triggered()
{
    for(unsigned long i=0;i<Geometries.size();i++)
    {
        vtkSmartPointer<vtkScalarsToColors> aux_lookup = Geometries[i]->Mapper->GetLookupTable();
        double *range = Geometries[i]->Mapper->GetScalarRange();
        double rgba[4]{0.0, 0.0, 0.0, 0.0};
        this->colors->GetColor("Green",rgba);
        aux_lookup->GetColor(range[0],rgba);
        this->colors->GetColor("Blue",rgba);
        aux_lookup->GetColor(range[1],rgba);
        aux_lookup->Build();
        Geometries[i]->Mapper->SetLookupTable(aux_lookup);
        Geometries[i]->Mapper->Update();
        this->renderWindow->Render();
    }
}

void MainWindow::ChangeStatePlayAndPause(bool a)
{
    QList<QAction*> actions = ui->toolBar->actions();
    foreach (QAction* ac, actions)
    {
        if(ac->text()=="PlayAndPause Online Mode")
        {
            ac->setChecked(a);
        }
    }
}

void MainWindow::on_actionPlayAndPause_Online_Mode_triggered()
{
    if(Geometries.empty())
        return;
    if(Geometries[SelectedGeometry]->type != Filter_type::OpenFOAMcase && Geometries[SelectedGeometry]->type != Filter_type::FortranGPFEP)
        return;

    QList<QAction*> actions = ui->toolBar->actions();
    foreach (QAction* ac, actions)
    {
        if(ac->text()=="PlayAndPause Online Mode")
        {
            emit ChangeUpdateCheckbox(ac->isChecked());
        }
    }
}

void MainWindow::ImagelistWidget_itemSelectionChanged()
{
    QList<QListWidgetItem*> items= ImagesListWidget->selectedItems();
    if(items.isEmpty())
        return;
    TreeHandeler->Tree->selectionModel()->clearSelection();
    std::string Image_Name = items[0]->text().toStdString();
    int prev_index = ui->tabWidget->currentIndex();
    while(ui->tabWidget_2->count()>1)
    {
        ui->tabWidget_2->removeTab(ui->tabWidget_2->count()-1);
    }
    if(Images_List.size()==0)
        return;
    unsigned long j;
    for(j=0; j<Images_List.size();j++)
    {
        if(Images_List[j]->Name==Image_Name)
        {
            break;
        }
    }
    if(j==Images_List.size())
        return;
    SelectedImage = j;
    unsigned long i;
    for(i = 0; i < Images_List[SelectedImage]->List_Properties.size();i++)
    {
        QScrollArea *NewTab = new QScrollArea(ui->tabWidget_2);
        NewTab->setWidgetResizable(true);
        NewTab->setVerticalScrollBar(new QScrollBar(NewTab));
        NewTab->setVerticalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOn);
        NewTab->setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
        Images_List[SelectedImage]->List_Properties[i]->setSizeConstraint(QLayout::SetMinAndMaxSize);
        NewTab->setLayout(Images_List[SelectedImage]->List_Properties[i]);
        QWidget *container = new QWidget(NewTab);
        container->setLayout(Images_List[SelectedImage]->List_Properties[i]);
        NewTab->setWidget(container);
        this->ui->tabWidget_2->insertTab(i+1,NewTab,QString::fromStdString(Images_List[SelectedImage]->Properties_names[i]));
    }
    if(ui->tabWidget_2->count()>prev_index)
    {
        ui->tabWidget_2->setCurrentIndex(prev_index);
    }
    else
    {
        ui->tabWidget_2->setCurrentIndex(ui->tabWidget_2->count()-1);
    }
}

void MainWindow::DeleteImage(QListWidgetItem *item)
{
    std::string image_name = item->text().toStdString();
    unsigned long i=0;
    for(i=0;i<Images_List.size();i++)
    {
        if(Images_List[i]->Name==image_name)
            break;
    }

    if(i==Images_List.size())
        return;

    ren->RemoveVolume(Images_List[SelectedImage]->volume);

    delete Images_List[SelectedImage];
    Images_List.erase(Images_List.begin()+SelectedImage);
    delete item;

    SelectedImage=0;
    ImagelistWidget_itemSelectionChanged();
    renderWindow->Render();

}

void MainWindow::HideOrShowImagesTab()
{
    if(Images_item->isChecked())
    {
        ui->tabWidget_2->show();
    }
    else
    {
        ui->tabWidget_2->hide();
    }
}
void MainWindow::HideOrShowPropertiesTab()
{
    if(Geometry_item->isChecked())
    {
        ui->tabWidget->show();
    }
    else
    {
        ui->tabWidget->hide();
    }
}
void MainWindow::HideOrShowAxesTab()
{
    if(Axes_item->isChecked())
    {
        this->axes->VisibilityOn();
    }
    else
    {
        this->axes->VisibilityOff();
    }
}

void MainWindow::on_actionAdd_image_triggered()
{
    QString file_name = QFileDialog::getOpenFileName(this,"Open image file","/");

    if(file_name.isEmpty())
        return;
    ui->tabWidget_2->show();

    Images *New_Image = new Images(file_name, this->renderWindow);
    if( New_Image->input == nullptr || New_Image->input == NULL)
    {
        return;
    }
    ren->AddVolume(New_Image->volume);
    ren->ResetCamera(New_Image->volume->GetBounds());
    renderWindow->Render();

    Images_List.push_back(New_Image);
    SelectedImage = Images_List.size()-1;

    unsigned long i;
    std::string name(New_Image->Name);
    int a =1;
    next:
    for(i=0;i<Geometries.size();i++)
    {
        if(New_Image->Name == Geometries[i]->Name)
        {
            New_Image->Name = name + std::to_string(a);
            a++;
            goto next;
        }
    }
    ImagesListWidget->addItem(QString::fromStdString(New_Image->Name));
    ImagesListWidget->selectionModel()->clearSelection();
    ImagesListWidget->item(ImagesListWidget->count()-1)->setSelected(true);
}

void MainWindow::on_actionOpen_DICOM_directory_triggered()
{
    QString file_name = QFileDialog::getExistingDirectory(this,"Open image file","/");

    if(file_name.isEmpty())
        return;
    ui->tabWidget_2->show();

    Images *New_Image = new Images(file_name, this->renderWindow);
    if( New_Image->input == nullptr || New_Image->input == NULL)
    {
        return;
    }
    ren->AddVolume(New_Image->volume);
    ren->ResetCamera(New_Image->volume->GetBounds());
    renderWindow->Render();

    Images_List.push_back(New_Image);
    SelectedImage = Images_List.size()-1;

    unsigned long i;
    std::string name(New_Image->Name);
    int a =1;
    next:
    for(i=0;i<Geometries.size();i++)
    {
        if(New_Image->Name == Geometries[i]->Name)
        {
            New_Image->Name = name + std::to_string(a);
            a++;
            goto next;
        }
    }

    ImagesListWidget->addItem(QString::fromStdString(New_Image->Name));
    ImagesListWidget->selectionModel()->clearSelection();
    ImagesListWidget->item(ImagesListWidget->count()-1)->setSelected(true);
}

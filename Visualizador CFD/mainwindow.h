#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include <QCheckBox>
#include <QMessageBox>
#include <QMenu>
#include <QAction>
#include <QList>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QKeyEvent>
#include <QScrollArea>
#include <QChar>
#include <QFileDialog>
#include <QTabWidget>
#include <QStandardItemModel>
#include <QMessageBox>
#include <QDebug>
#include <QMutex>

#include <vtkSmartPointer.h>
#include <vtkRenderer.h>
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkEventQtSlotConnect.h>
#include <vtkInteractorStyleImage.h>
#include <vtkCamera.h>
#include <vtkAxesActor.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkNamedColors.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkArrayDataWriter.h>
#include <vtkDenseArray.h>
#include <vtkArrayData.h>


#include "customscale.h"


#include "geometry.h"
#include "stream_lines_filter.h"
#include "vortex_filter.h"
#include "pathlines_filter.h"
#include "puntos_de_estancamiento.h"
#include "caudal_en_plano.h"
#include "geometrytree.h"
#include "clipfilter.h"
#include "fortranonrealtime.h"
#include "viscous_forces.h"
#include "openfoamruntime.h"
#include "gpfepwriter.h"
#include "loadbar.h"
#include "openfoamwriter.h"
#include "slice_filter.h"
#include "gradient_filter.h"
#include "glyphs_filter.h"
#include "contour_filter.h"
#include "images.h"

#include <boost/iostreams/device/file.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

//Clase Principal del programa
//Contiene la ui y los contenedores necesarios para visualizar las soluciones
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    //Funcion implementada para actualizar los campos que se observan en la ComboBox al clickear otra geometria.
    void UpdateFieldList();
    //Funcion para actualizar los componentes disponibles en el campo seleccionado.
    void UpdateComponentsList();
    //Muestra o oculta una determinada geometria. i = 0 ocultar, i = 1 mostrar.
    void HideOrShowGeometry(int i,int geo);
    //Funcion implemetada para actualizar los pasos de tiempo que se muestran en la comboBox al clickear otra geometria.
    void UpdateTimeList();
    //Update ActorShow combo box;
    void UpdateActorShow();
    //Update TabsProperties
    void UpdatePropertiesTabs();
    //Deshabilita/habilita los botones
    void DisableEnable();
    //Delete an specific Geometry from GeometryList
    void DeleteGeometry(QTreeWidgetItem* item);

    //Variable que sirve para saber cual es la geometria seleccionada actualmente
    unsigned long SelectedGeometry;
    //Contenedor de todas las geometrias cargadas
    std::vector<Geometry *> Geometries;
    //Render. Necesario para visualizacion
    vtkSmartPointer<vtkRenderer> ren;
    //RenderWindow. Necesario para visualizacion
    vtkSmartPointer<vtkGenericOpenGLRenderWindow> renderWindow;
    //Axes
    vtkSmartPointer<vtkAxesActor> axes;
    //MarkerWidget
    vtkSmartPointer<vtkOrientationMarkerWidget> widget_for_Axes;
    //colors
    vtkSmartPointer<vtkNamedColors> colors;

    //Conneciones
    vtkSmartPointer<vtkEventQtSlotConnect> autoUpdate;
    //ComboBox en el que se observan/Seleccionan los campos cargados
    QComboBox *FieldList;
    //ComboBox en el que se observan/Seleccionan los pasos de tiempo
    QComboBox *TimeList;
    //TipeOfRepresentation
    QComboBox *ActorShow;
    //Component
    QComboBox *ComponentsList;
    //Geometry tree
    QTreeWidget *GeometriesList;
    GeometryTree *TreeHandeler;

    //Images List
    std::vector <Images* > Images_List;
    unsigned long SelectedImage;
    QListWidget *ImagesListWidget;
    void DeleteImage(QListWidgetItem *item);
    /**
     * @brief mute. Class for avoiding processors crash
     */
    QMutex mute;

    //Action views
    QAction *Axes_item;
    QAction *Geometry_item;
    QAction *Images_item;

//Funciones utilizadas para hacer conexiones de eventos. Las conexiones de estas funciones se hacen on runtime.
public slots:
    //Main Function for updating the interface
    void UpdateInterface();
    //Cambia el campo solucion al cambiar el campo selecionado en la interfaz
    virtual void ChangeField(QString Field_Name);
    //Cambia el paso de tiempo de la solucion al cambiar el paso de tiempo seleccionado en la interfaz
    virtual void ChangeTimeSolution(int i);
    //Cambia el componente del campo que se esta observando
    virtual void ChangeComponent(QString Component_name);

    void UpdateTimeAndFields();
//Funciones utilizadas para hacer conexiones. Estas conexiones se hacen desde el designer.
private slots:
    //Carga la geometria
    void on_actionOpen_Geometry_triggered();
    //Añade un campo a la geometria
    void on_actionAdd_Field_triggered();
    //Cambia el tiempo de la solucion hacia atras
    void on_actionTime_Backwards_triggered();
    //Cambia el tiempo de la solucion hacia adelante
    void on_actionTime_Fowards_triggered();
    //Actualiza la interfaz (pestaña de propiedades, comboBox que maneja los campos, etc). Falta terminar su implementacion (A medias)
    void itemPressed();
    //Funcion implementada junto con "HideOrShowGeometry()" para mostrar/ocultar una geometria segun su casilla este checkeada o no.
    void itemChanged(QTreeWidgetItem *item,int col);

    //Funcion Filtro StreamLines. Crea el objeto y hace las conexiones.
    void on_actionStreamLines_triggered();
    //Funcion Filtro Vortex_finder. Crea el objeto y hace las conexiones.
    void on_actionVortex_finder_triggered();
    //Funcion Filtro Path_Lines. Crea el objeto y hace las conexiones.
    void on_actionPath_Lines_triggered();
    //Funcion Filtro Puntos de estancamiento. Crea el objeto y hace las conexiones.
    void on_actionPuntos_de_estancamiento_triggered();
    //Funcion Filtro Caudal en plano. Crea el objeto y hace las conexiones.
    void on_actionCaudal_en_plano_triggered();
    //Funcion Filtro Clip. Crea el objeto y hace las conexiones.
    void on_actionClip_triggered();
    //Funcion Filtro Conexion con GPFEP en tiempo real. Crea el objeto y hace las conexiones.
    void on_actionConnect_with_GPFEP_triggered();
    //Funcion Filtro Conexion con OpenFOAM en tiempo real. Crea el objeto y hace las conexiones.
    void on_actionConnect_with_OpenFOAM_triggered();
    //Funcion Filtro Fuerzas viscosas. Crea el objeto y hace las conexiones.
    void on_actionViscous_Forces_triggered();

    //Funciones para el manejo de la cámara. Distintas perspectivas
    void on_actionX_Cam_triggered();
    void on_actionX_Cam_2_triggered();
    void on_actionY_Cam_triggered();
    void on_actionY_Cam_2_triggered();
    void on_actionZ_Cam_triggered();
    void on_actionZ_Cam_2_triggered();
    //Girar la camara
    void on_actionTurnRight_triggered();
    void on_actionTurnLeft_triggered();

    //Rescala la solucion al rango ingresado por el usuario
    void on_actionRescaleToCustom_triggered();
    //Rescala la solucion segun la magnitud del campo actual.
    void on_actionRescaleToCurrentData_triggered();
    //Cambia la forma en que se visualiza la solución, por ahora solo dos modos. superficie y superficie con bordes
    void ActorShowChange(QString type);
    //NO FUNCIONAL POR AHORA. Cambia los colores con los que se visualiza la geometria
    void on_actionChangeColor_triggered();
    //Crea los archivos para la visualización off-line y los abre para un caso de GPFEP
    void on_actionGPFEP_case_conversor_triggered();
    //Crea los archivos para la visualización on-line y los abre para un caso de OpenFOAM
    void on_actionOpenFOAM_case_conversor_triggered();
    //Load the geometry and fields of a case
    void Load_Case(QList<QString> ArrayNames,QString Case);
    //Funcion Filtro Slice. Crea el objeto y hace las conexiones.
    void on_actionSlice_triggered();
    //Funcion Filtro Gradient. Crea el objeto y hace las conexiones.
    void on_actionGradient_triggered();

    //Functions to stop and play the online filters.
    void ChangeStatePlayAndPause(bool a);
    void on_actionPlayAndPause_Online_Mode_triggered();

    /**
     * @brief Open a dialog to add the path to an image. In case is a valid image it open it.
     */
    void on_actionAdd_image_triggered();

    void ImagelistWidget_itemSelectionChanged();

    void HideOrShowImagesTab();

    void HideOrShowPropertiesTab();

    void HideOrShowAxesTab();


    void on_actionOpen_DICOM_directory_triggered();

private:
    Ui::MainWindow *ui;

protected:
    //Maneja los eventos de presion de teclas, por ahora solo elimina las geometrias o filtros.
    void keyPressEvent(QKeyEvent *event);
signals:
    //Change the value of the "Update checkbox" inside the online filters.
    void ChangeUpdateCheckbox(bool value);
};
#endif // MAINWINDOW_H

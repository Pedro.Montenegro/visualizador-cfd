#include "mainwindow.h"
#include "ui_mainwindow.h"

void MainWindow::on_actionX_Cam_triggered() //X-
{
    ren->GetActiveCamera()->SetPosition(-10000,0,0);
    ren->GetActiveCamera()->SetViewUp(0,0,1);
    renderWindow->Render();

    if(Geometries.empty())
    {
        return;
    }
    double *bounds = Geometries[SelectedGeometry]->GetBounds();
    ren->GetActiveCamera()->SetFocalPoint(Geometries[SelectedGeometry]->GetCenter());
    ren->ResetCamera(bounds);
    renderWindow->Render();
}

void MainWindow::on_actionX_Cam_2_triggered() //X+
{
    ren->GetActiveCamera()->SetPosition(10000,0,0);
    ren->GetActiveCamera()->SetViewUp(0,0,1);
    renderWindow->Render();

    if(Geometries.empty())
    {
        return;
    }
    double *bounds = Geometries[SelectedGeometry]->GetBounds();
    ren->GetActiveCamera()->SetFocalPoint(Geometries[SelectedGeometry]->GetCenter());
    ren->ResetCamera(bounds);
    renderWindow->Render();

}

void MainWindow::on_actionY_Cam_triggered() //Y-
{
    ren->GetActiveCamera()->SetPosition(0,-10000,0);
    ren->GetActiveCamera()->SetViewUp(0,0,1);
    renderWindow->Render();

    if(Geometries.empty())
    {
        return;
    }
    double *bounds = Geometries[SelectedGeometry]->GetBounds();
    ren->GetActiveCamera()->SetFocalPoint(Geometries[SelectedGeometry]->GetCenter());
    ren->ResetCamera(bounds);
    renderWindow->Render();

}

void MainWindow::on_actionY_Cam_2_triggered() //Y+
{
    ren->GetActiveCamera()->SetPosition(0,10000,0);
    ren->GetActiveCamera()->SetViewUp(0,0,1);
    renderWindow->Render();

    if(Geometries.empty())
    {
        return;
    }
    double *bounds = Geometries[SelectedGeometry]->GetBounds();
    ren->GetActiveCamera()->SetFocalPoint(Geometries[SelectedGeometry]->GetCenter());
    ren->ResetCamera(bounds);
    renderWindow->Render();

}

void MainWindow::on_actionZ_Cam_triggered() //Z-
{
    ren->GetActiveCamera()->SetPosition(0,0,10000);
    ren->GetActiveCamera()->SetViewUp(0,1,0);
    renderWindow->Render();

    if(Geometries.empty())
    {
        return;
    }
    double *bounds = Geometries[SelectedGeometry]->GetBounds();
    ren->GetActiveCamera()->SetFocalPoint(Geometries[SelectedGeometry]->GetCenter());
    ren->ResetCamera(bounds);
    renderWindow->Render();


}

void MainWindow::on_actionZ_Cam_2_triggered() //Z+
{
    ren->GetActiveCamera()->SetPosition(0,0,-10000);
    ren->GetActiveCamera()->SetViewUp(0,1,0);
    renderWindow->Render();

    if(Geometries.empty())
    {
        return;
    }
    double *bounds = Geometries[SelectedGeometry]->GetBounds();
    ren->GetActiveCamera()->SetFocalPoint(Geometries[SelectedGeometry]->GetCenter());
    ren->ResetCamera(bounds);
    renderWindow->Render();
}


void MainWindow::on_actionTurnRight_triggered()
{
    ren->GetActiveCamera()->Roll(-90);
    renderWindow->Render();
}

void MainWindow::on_actionTurnLeft_triggered()
{
    ren->GetActiveCamera()->Roll(90);
    renderWindow->Render();
}


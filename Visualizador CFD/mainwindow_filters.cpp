#include "mainwindow.h"
#include "ui_mainwindow.h"

void MainWindow::on_actionStreamLines_triggered()
{
    if(Geometries.empty())
        return;
    std::string new_name =this->Geometries[this->SelectedGeometry]->Name + "/StreamLines";
    std::string name = new_name;
    unsigned long i;
    int a=1;

    next:
    for(i=0;i<Geometries.size();i++)
    {
        if(new_name == Geometries[i]->Name)
        {
            new_name = name + std::to_string(a);
            a++;
            goto next;
        }
    }

    QString geo_name= QString::fromStdString(new_name);

    if(Geometries[SelectedGeometry]->Field_list.size()==0 || !Geometries[SelectedGeometry]->HaveVector())
    {
        return;
    }
    int previous_selected = SelectedGeometry;

    Stream_Lines_Filter *New_Stream_Lines = new Stream_Lines_Filter;

    Geometries.push_back(New_Stream_Lines);
    New_Stream_Lines->SetInputData(Geometries[SelectedGeometry]);

    Geometries[SelectedGeometry]->HideWidgets();

    this->SelectedGeometry = Geometries.size()-1;

    Geometries[SelectedGeometry]->ShowWidgets();

    this->ren->AddActor(this->Geometries[this->SelectedGeometry]->Actor);
    this->ren->AddActor(New_Stream_Lines->GeneradorDeSemillas->PointSourceActor);

    New_Stream_Lines->Name = new_name;

    QList<QTreeWidgetItem *>item = GeometriesList->findItems(QString::fromStdString(Geometries[previous_selected]->Name), Qt::MatchFlag::MatchExactly |Qt::MatchFlag::MatchRecursive);
    item.at(0)->setCheckState(0,Qt::Unchecked);
    TreeHandeler->addTreeChild(item.at(0),geo_name);
    TreeHandeler->Tree->selectionModel()->clearSelection();
    item.at(0)->child(item.at(0)->childCount()-1)->setSelected(true);

    HideOrShowGeometry(0,previous_selected);
    //Interfaz update
    UpdateInterface();

    this->renderWindow->Render();
}

void MainWindow::on_actionVortex_finder_triggered()
{
    if(Geometries.empty())
        return;
    std::string new_name =this->Geometries[this->SelectedGeometry]->Name + "/VortexFinder";
    std::string name = new_name;
    unsigned long i;
    int a=1;
    next:
    for(i=0;i<Geometries.size();i++)
    {
        if(new_name == Geometries[i]->Name)
        {
            new_name = name + std::to_string(a);
            a++;
            goto next;
        }
    }

    QString geo_name= QString::fromStdString(new_name);

    if(Geometries[SelectedGeometry]->Field_list.size()==0 || !Geometries[SelectedGeometry]->HaveVector())
    {
        return;
    }
    int previous_selected = SelectedGeometry;

    Vortex_Filter *New_Vortex_finder = new Vortex_Filter;

    this->Geometries.push_back(New_Vortex_finder);

    New_Vortex_finder->SetInputData(Geometries[SelectedGeometry]);

    Geometries[SelectedGeometry]->HideWidgets();

    this->SelectedGeometry = Geometries.size()-1;

    Geometries[SelectedGeometry]->ShowWidgets();

    this->ren->AddActor(this->Geometries[this->SelectedGeometry]->Actor);

    New_Vortex_finder->Name = new_name;

    QList<QTreeWidgetItem *>item = GeometriesList->findItems(QString::fromStdString(Geometries[previous_selected]->Name), Qt::MatchFlag::MatchExactly |Qt::MatchFlag::MatchRecursive);
    item.at(0)->setCheckState(0,Qt::Unchecked);
    TreeHandeler->addTreeChild(item.at(0),geo_name);
    TreeHandeler->Tree->selectionModel()->clearSelection();
    item.at(0)->child(item.at(0)->childCount()-1)->setSelected(true);

    HideOrShowGeometry(0,previous_selected);

    //Interfaz update
    UpdateInterface();

    this->renderWindow->Render();

}

void MainWindow::on_actionPath_Lines_triggered()
{
    if(Geometries.empty())
        return;
    std::string new_name =this->Geometries[this->SelectedGeometry]->Name + "/PathLine";
    std::string name = new_name;
    unsigned long i;
    int a=1;
    next:
    for(i=0;i<Geometries.size();i++)
    {
        if(new_name == Geometries[i]->Name)
        {
            new_name = name + std::to_string(a);
            a++;
            goto next;
        }
    }

    QString geo_name= QString::fromStdString(new_name);

    if(Geometries[SelectedGeometry]->Field_list.size()==0 || !Geometries[SelectedGeometry]->HaveVector())
    {
        return;
    }
    int previous_selected = SelectedGeometry;

    PathLines_filter *New_PathLine = new PathLines_filter;
    this->Geometries.push_back(New_PathLine);

    New_PathLine->SetInputData(Geometries[SelectedGeometry]);

    Geometries[SelectedGeometry]->HideWidgets();

    this->SelectedGeometry = Geometries.size()-1;
    Geometries[SelectedGeometry]->ShowWidgets();

    this->ren->AddActor(this->Geometries[this->SelectedGeometry]->Actor);
    this->ren->AddActor(New_PathLine->SeedsWidget->PointSourceActor);

    New_PathLine->Name = new_name;

    QList<QTreeWidgetItem *>item = GeometriesList->findItems(QString::fromStdString(Geometries[previous_selected]->Name), Qt::MatchFlag::MatchExactly |Qt::MatchFlag::MatchRecursive);
    item.at(0)->setCheckState(0,Qt::Unchecked);
    TreeHandeler->addTreeChild(item.at(0),geo_name);
    TreeHandeler->Tree->selectionModel()->clearSelection();
    item.at(0)->child(item.at(0)->childCount()-1)->setSelected(true);

    HideOrShowGeometry(0,previous_selected);


    //Interfaz update
    UpdateInterface();

    this->renderWindow->Render();

}

void MainWindow::on_actionPuntos_de_estancamiento_triggered()
{
    if(Geometries.empty())
        return;
    std::string new_name =this->Geometries[this->SelectedGeometry]->Name + "/PuntosDeEstancamiento";
    std::string name = new_name;
    unsigned long i;
    int a=1;
    next:
    for(i=0;i<Geometries.size();i++)
    {
        if(new_name == Geometries[i]->Name)
        {
            new_name = name + std::to_string(a);
            a++;
            goto next;
        }
    }

    QString geo_name= QString::fromStdString(new_name);

    if(Geometries[SelectedGeometry]->Field_list.size()==0 || !Geometries[SelectedGeometry]->HaveVector())
    {
        return;
    }
    int previous_selected = SelectedGeometry;

    Puntos_de_estancamiento *New_points = new Puntos_de_estancamiento;
    this->Geometries.push_back(New_points);
    New_points->SetInputData(this->Geometries[SelectedGeometry]);

    Geometries[SelectedGeometry]->HideWidgets();

    this->SelectedGeometry = Geometries.size()-1;

    Geometries[SelectedGeometry]->ShowWidgets();

    this->ren->AddActor(this->Geometries[this->SelectedGeometry]->Actor);
    New_points->Name = new_name;

    QList<QTreeWidgetItem *>item = GeometriesList->findItems(QString::fromStdString(Geometries[previous_selected]->Name), Qt::MatchFlag::MatchExactly |Qt::MatchFlag::MatchRecursive);
    item.at(0)->setCheckState(0,Qt::Unchecked);
    TreeHandeler->addTreeChild(item.at(0),geo_name);
    TreeHandeler->Tree->selectionModel()->clearSelection();
    item.at(0)->child(item.at(0)->childCount()-1)->setSelected(true);

    HideOrShowGeometry(0,previous_selected);


    //Interfaz update
    UpdateInterface();

    this->renderWindow->Render();
}

void MainWindow::on_actionCaudal_en_plano_triggered()
{
    if(Geometries.empty())
        return;
    std::string new_name =this->Geometries[this->SelectedGeometry]->Name + "/CaudalEnPlano";
    std::string name = new_name;
    unsigned long i;
    int a=1;
    next:
    for(i=0;i<Geometries.size();i++)
    {
        if(new_name == Geometries[i]->Name)
        {
            new_name = name + std::to_string(a);
            a++;
            goto next;
        }
    }

    QString geo_name= QString::fromStdString(new_name);

    if(Geometries[SelectedGeometry]->Field_list.size()==0 || !Geometries[SelectedGeometry]->HaveVector())
    {
        return;
    }
    int previous_selected = SelectedGeometry;

    Caudal_en_plano *New_caudal = new Caudal_en_plano;

    this->Geometries.push_back(New_caudal);

    this->renderWindow->Render();


    New_caudal->SetInputData(Geometries[SelectedGeometry]);

    Geometries[SelectedGeometry]->HideWidgets();

    this->SelectedGeometry = Geometries.size()-1;

    Geometries[SelectedGeometry]->ShowWidgets();

    this->ren->AddActor(this->Geometries[this->SelectedGeometry]->Actor);
    New_caudal->Name = new_name;

    QList<QTreeWidgetItem *>item = GeometriesList->findItems(QString::fromStdString(Geometries[previous_selected]->Name), Qt::MatchFlag::MatchExactly |Qt::MatchFlag::MatchRecursive);
    item.at(0)->setCheckState(0,Qt::Unchecked);
    TreeHandeler->addTreeChild(item.at(0),geo_name);
    TreeHandeler->Tree->selectionModel()->clearSelection();
    item.at(0)->child(item.at(0)->childCount()-1)->setSelected(true);

    HideOrShowGeometry(0,previous_selected);


    //Interfaz update
    UpdateInterface();

    this->renderWindow->Render();
}

void MainWindow::on_actionClip_triggered()
{
    if(Geometries.empty())
        return;
    std::string new_name =this->Geometries[this->SelectedGeometry]->Name + "/Clipped";
    std::string name = new_name;
    unsigned long i;
    int a=1;
    next:
    for(i=0;i<Geometries.size();i++)
    {
        if(new_name == Geometries[i]->Name)
        {
            new_name = name + std::to_string(a);
            a++;
            goto next;
        }
    }

    QString geo_name= QString::fromStdString(new_name);

    int previous_selected = SelectedGeometry;

    ClipFilter *New_clip = new ClipFilter;
    New_clip->render_Win = this->renderWindow;
    this->Geometries.push_back(New_clip);

    New_clip->SetInputData(Geometries[SelectedGeometry]);
    New_clip->SetScalarBarWidget();

    Geometries[SelectedGeometry]->HideWidgets();

    this->SelectedGeometry = Geometries.size()-1;

    Geometries[SelectedGeometry]->ShowWidgets();
    New_clip->SetVectors();

    this->ren->AddActor(this->Geometries[this->SelectedGeometry]->Actor);
    New_clip->Name = new_name;

    QList<QTreeWidgetItem *>item = GeometriesList->findItems(QString::fromStdString(Geometries[previous_selected]->Name), Qt::MatchFlag::MatchExactly |Qt::MatchFlag::MatchRecursive);
    item.at(0)->setCheckState(0,Qt::Unchecked);
    TreeHandeler->addTreeChild(item.at(0),geo_name);
    TreeHandeler->Tree->selectionModel()->clearSelection();
    item.at(0)->child(item.at(0)->childCount()-1)->setSelected(true);

    HideOrShowGeometry(0,previous_selected);

    //Interfaz update
    UpdateInterface();

    this->renderWindow->Render();
    QObject::connect(New_clip,&ClipFilter::UpdateInterfaseSignal,this,&MainWindow::UpdateInterface);
}

void MainWindow::on_actionConnect_with_GPFEP_triggered()
{
    QString file_name = QFileDialog::getExistingDirectory(this,"Open case directory","/");

    if(file_name.isEmpty())
        return;
    std::string path = file_name.toStdString()+"/";
    std::string file = "gpfep.cfg";
    std::string gpfep = path + file;
    std::string line = "";
    std::ifstream config_file(gpfep.c_str());
    if(!config_file.is_open())
    {
        QMessageBox *error = new QMessageBox(QMessageBox::Warning,"Error","No se encontro el archivo de configuración gpfep.cfg");
        error->setWindowFlags(error->windowFlags() & ~Qt::WindowContextHelpButtonHint);
        error->setWindowFlags(error->windowFlags() & ~Qt::WindowMinimizeButtonHint);
        error->exec();
        delete error;
        return;
    }
    config_file.close();
    FortranOnRealTime *New_fortran = new FortranOnRealTime(file_name,this->renderWindow);

    Geometries.push_back(New_fortran);
    SelectedGeometry = Geometries.size()-1;
    New_fortran->SetScalarBarWidget();
    New_fortran->SetVectors();
    ren->AddActor(New_fortran->Actor);


    ren->ResetCamera(New_fortran->Actor->GetBounds());

    renderWindow->Render();

    QTreeWidget::disconnect(GeometriesList,SIGNAL(itemChanged(QTreeWidgetItem*,int)),this,SLOT(itemChanged(QTreeWidgetItem*,int)));

    TreeHandeler->addTreeRoot(QString::fromStdString(New_fortran->Name));
    QTreeWidget::connect(GeometriesList,SIGNAL(itemChanged(QTreeWidgetItem*,int)),this,SLOT(itemChanged(QTreeWidgetItem*,int)));

    UpdateInterface();

    int num_of_geom = TreeHandeler->Tree->topLevelItemCount();
    TreeHandeler->Tree->selectionModel()->clearSelection();
    TreeHandeler->Tree->topLevelItem(num_of_geom - 1)->setSelected(true);
    QObject::connect(New_fortran,&FortranOnRealTime::UpdateInterfaseSignal,this,&MainWindow::UpdateTimeAndFields);
    QObject::connect(New_fortran,&FortranOnRealTime::PlayAndStop,this,&MainWindow::ChangeStatePlayAndPause);
    QObject::connect(this,&MainWindow::ChangeUpdateCheckbox,New_fortran,&FortranOnRealTime::ChangeUpdate_CheckBox);
}

void MainWindow::on_actionViscous_Forces_triggered()
{
    if(Geometries.empty())
        return;
    std::string new_name =this->Geometries[this->SelectedGeometry]->Name + "/Viscous Forces";
    std::string name = new_name;
    unsigned long i;
    int a=1;
    next:
    for(i=0;i<Geometries.size();i++)
    {
        if(new_name == Geometries[i]->Name)
        {
            new_name = name + std::to_string(a);
            a++;
            goto next;
        }
    }

    QString geo_name= QString::fromStdString(new_name);

    int previous_selected = SelectedGeometry;

    Viscous_Forces *New_Viscous_Forces = new Viscous_Forces;
    this->Geometries.push_back(New_Viscous_Forces);


    New_Viscous_Forces->SetInputData(Geometries[SelectedGeometry]);

    this->SelectedGeometry = Geometries.size()-1;

    this->ren->AddActor(this->Geometries[this->SelectedGeometry]->Actor);
    New_Viscous_Forces->Name = new_name;
    New_Viscous_Forces->SetScalarBarWidget();


    QList<QTreeWidgetItem *>item = GeometriesList->findItems(QString::fromStdString(Geometries[previous_selected]->Name), Qt::MatchFlag::MatchExactly |Qt::MatchFlag::MatchRecursive);
    item.at(0)->setCheckState(0,Qt::Unchecked);
    TreeHandeler->addTreeChild(item.at(0),geo_name);
    TreeHandeler->Tree->selectionModel()->clearSelection();
    item.at(0)->child(item.at(0)->childCount()-1)->setSelected(true);

    HideOrShowGeometry(0,previous_selected);

    New_Viscous_Forces->SetVectors();

    //Interfaz update
    UpdateInterface();

    this->renderWindow->Render();
}

void MainWindow::on_actionConnect_with_OpenFOAM_triggered()
{
    QString file_name = QFileDialog::getExistingDirectory(this,"Open case directory","/");

    if(file_name.isEmpty())
        return;

    std::string path = file_name.toStdString()+"/";
    std::string system = path + "system";
    std::string file = "system/controlDict";
    std::string ControlDict = path + file;
    std::string line = "";
    std::ifstream config_file(ControlDict.c_str());
    //check directory
    struct stat info;

    if( stat( path.c_str(), &info ) != 0 )
    {
        QMessageBox *error = new QMessageBox(QMessageBox::Warning,"Error","No se encontro el directorio system");
        error->setWindowFlags(error->windowFlags() & ~Qt::WindowContextHelpButtonHint);
        error->setWindowFlags(error->windowFlags() & ~Qt::WindowMinimizeButtonHint);
        error->exec();
        delete error;
        return;
    }
    else if( info.st_mode & S_IFDIR )  // S_ISDIR() doesn't exist on my windows
    {

    }
    else
    {
        QMessageBox *error = new QMessageBox(QMessageBox::Warning,"Error","No se encontro el directorio system");
        error->setWindowFlags(error->windowFlags() & ~Qt::WindowContextHelpButtonHint);
        error->setWindowFlags(error->windowFlags() & ~Qt::WindowMinimizeButtonHint);
        error->exec();
        delete error;
        return;
    }
    //Check inside the directory
    if(!config_file.is_open())
    {
        QMessageBox *error = new QMessageBox(QMessageBox::Warning,"Error","No se encontro el archivo de configuración ControlDict");
        error->setWindowFlags(error->windowFlags() & ~Qt::WindowContextHelpButtonHint);
        error->setWindowFlags(error->windowFlags() & ~Qt::WindowMinimizeButtonHint);
        error->exec();
        delete error;
        return;
    }
    config_file.close();
    std::string dir_path = boost::filesystem::initial_path().string();
    QDir dir(dir_path.c_str());

    OpenFOAMRunTime *New_OpenF = new OpenFOAMRunTime(file_name,this->renderWindow);

    Geometries.push_back(New_OpenF);
    SelectedGeometry = Geometries.size()-1;

    New_OpenF->SetScalarBarWidget();
    New_OpenF->SetVectors();

    ren->AddActor(New_OpenF->Actor);

    ren->ResetCamera(New_OpenF->Actor->GetBounds());

    renderWindow->Render();

    QTreeWidget::disconnect(GeometriesList,SIGNAL(itemChanged(QTreeWidgetItem*,int)),this,SLOT(itemChanged(QTreeWidgetItem*,int)));

    TreeHandeler->addTreeRoot(QString::fromStdString(New_OpenF->Name));
    QTreeWidget::connect(GeometriesList,SIGNAL(itemChanged(QTreeWidgetItem*,int)),this,SLOT(itemChanged(QTreeWidgetItem*,int)));

    UpdateInterface();

    int num_of_geom = TreeHandeler->Tree->topLevelItemCount();
    TreeHandeler->Tree->selectionModel()->clearSelection();
    TreeHandeler->Tree->topLevelItem(num_of_geom - 1)->setSelected(true);
    QObject::connect(New_OpenF,&OpenFOAMRunTime::UpdateInterfaseSignal,this,&MainWindow::UpdateTimeAndFields);
    QObject::connect(New_OpenF,&OpenFOAMRunTime::PlayAndStop,this,&MainWindow::ChangeStatePlayAndPause);
    QObject::connect(this,&MainWindow::ChangeUpdateCheckbox,New_OpenF,&OpenFOAMRunTime::ChangeUpdate_CheckBox);
}

void MainWindow::on_actionSlice_triggered()
{
    if(Geometries.empty())
        return;
    std::string new_name =this->Geometries[this->SelectedGeometry]->Name + "/Slice";
    std::string name = new_name;
    unsigned long i;
    int a=1;
    next:
    for(i=0;i<Geometries.size();i++)
    {
        if(new_name == Geometries[i]->Name)
        {
            new_name = name + std::to_string(a);
            a++;
            goto next;
        }
    }

    QString geo_name= QString::fromStdString(new_name);

    int previous_selected = SelectedGeometry;

    Slice_filter *New_clip = new Slice_filter;
    New_clip->render_Win = this->renderWindow;
    this->Geometries.push_back(New_clip);

    New_clip->SetInputData(Geometries[SelectedGeometry]);
    New_clip->SetScalarBarWidget();

    Geometries[SelectedGeometry]->HideWidgets();

    this->SelectedGeometry = Geometries.size()-1;

    Geometries[SelectedGeometry]->ShowWidgets();
    New_clip->SetVectors();

    this->ren->AddActor(this->Geometries[this->SelectedGeometry]->Actor);
    New_clip->Name = new_name;

    QList<QTreeWidgetItem *>item = GeometriesList->findItems(QString::fromStdString(Geometries[previous_selected]->Name), Qt::MatchFlag::MatchExactly |Qt::MatchFlag::MatchRecursive);
    item.at(0)->setCheckState(0,Qt::Unchecked);
    TreeHandeler->addTreeChild(item.at(0),geo_name);
    TreeHandeler->Tree->selectionModel()->clearSelection();
    item.at(0)->child(item.at(0)->childCount()-1)->setSelected(true);

    HideOrShowGeometry(0,previous_selected);

    //Interfaz update
    UpdateInterface();
    this->renderWindow->Render();
    QObject::connect(New_clip,&Slice_filter::UpdateInterfaseSignal,this,&MainWindow::UpdateInterface);
}

void MainWindow::on_actionGradient_triggered()
{
    if(Geometries.empty())
        return;
    std::string new_name =this->Geometries[this->SelectedGeometry]->Name + "/Gradient";
    std::string name = new_name;
    unsigned long i;
    int a=1;
    next:
    for(i=0;i<Geometries.size();i++)
    {
        if(new_name == Geometries[i]->Name)
        {
            new_name = name + std::to_string(a);
            a++;
            goto next;
        }
    }

    QString geo_name= QString::fromStdString(new_name);

    int previous_selected = SelectedGeometry;

    Gradient_Filter *New_Grad = new Gradient_Filter;
    New_Grad->render_Win = this->renderWindow;
    this->Geometries.push_back(New_Grad);

    New_Grad->SetInputData(Geometries[SelectedGeometry]);
    New_Grad->SetScalarBarWidget();

    Geometries[SelectedGeometry]->HideWidgets();

    this->SelectedGeometry = Geometries.size()-1;

    New_Grad->SetVectors();

    this->ren->AddActor(this->Geometries[this->SelectedGeometry]->Actor);
    New_Grad->Name = new_name;

    QList<QTreeWidgetItem *>item = GeometriesList->findItems(QString::fromStdString(Geometries[previous_selected]->Name), Qt::MatchFlag::MatchExactly |Qt::MatchFlag::MatchRecursive);
    item.at(0)->setCheckState(0,Qt::Unchecked);
    TreeHandeler->addTreeChild(item.at(0),geo_name);
    TreeHandeler->Tree->selectionModel()->clearSelection();
    item.at(0)->child(item.at(0)->childCount()-1)->setSelected(true);

    HideOrShowGeometry(0,previous_selected);

    //Interfaz update
    UpdateInterface();

    this->renderWindow->Render();
    QObject::connect(New_Grad,&Gradient_Filter::UpdateInterfaseSignal,this,&MainWindow::UpdateInterface);

}

#include "mainwindow.h"
#include "ui_mainwindow.h"


void MainWindow::on_actionGPFEP_case_conversor_triggered()
{
    QString file_name = QFileDialog::getExistingDirectory(NULL,"Open case directory","/");

    if(file_name.isEmpty())
        return;
    std::string path = file_name.toStdString()+"/";
    std::string file = "gpfep.cfg";
    std::string gpfep = path + file;
    std::string line = "";
    std::ifstream config_file(gpfep.c_str());
    if(!config_file.is_open())
    {
        QMessageBox *error = new QMessageBox(QMessageBox::Warning,"Error","No se encontro el archivo de configuración gpfep.cfg");
        error->setWindowFlags(error->windowFlags() & ~Qt::WindowContextHelpButtonHint);
        error->setWindowFlags(error->windowFlags() & ~Qt::WindowMinimizeButtonHint);
        error->exec();
        delete error;
        return;
    }
    QString WriteIn = QFileDialog::getExistingDirectory(NULL,"Write in ...",file_name);
    if(WriteIn.isEmpty())
        WriteIn = file_name;
    config_file.close();

    LoadBar *Bar = new LoadBar;
    Bar->SetValue(0);
    Bar->SetText("Reading Config file");
    Bar->SetDialogTitle("GPFEP Writer");
    GPFEPWriter *workerThread = new GPFEPWriter;
    workerThread->SetPaths(file_name,WriteIn);
    QObject::connect(workerThread,&GPFEPWriter::textToBar,Bar,&LoadBar::SetText);
    QObject::connect(workerThread,&GPFEPWriter::fileNumber,Bar,&LoadBar::SetValue);
    QObject::connect(workerThread,&GPFEPWriter::numberOfFiles,Bar,&LoadBar::SetNumberOfFiles);
    QObject::connect(workerThread, &GPFEPWriter::finished, workerThread, &QObject::deleteLater);
    QObject::connect(workerThread, &GPFEPWriter::destroyed,Bar,&LoadBar::close);
    QObject::connect(Bar,&LoadBar::finished,Bar,&LoadBar::deleteLater);
    QObject::connect(workerThread,&GPFEPWriter::ArrayNames,this,&MainWindow::Load_Case);

    workerThread->start();
    Bar->exec();
}

void MainWindow::on_actionOpenFOAM_case_conversor_triggered()
{
    QString file_name = QFileDialog::getExistingDirectory(this,"Open case directory","/");

    if(file_name.isEmpty())
        return;

    std::string dir_path = boost::filesystem::initial_path().string();
    QDir dir(dir_path.c_str());

    QString relative_path = dir.relativeFilePath(file_name);

    QString WriteIn = QFileDialog::getExistingDirectory(NULL,"Write in ...",file_name);
    if(WriteIn.isEmpty())
        WriteIn = file_name;

    LoadBar *Bar = new LoadBar;
    Bar->SetValue(0);
    Bar->SetText("Reading Config file");
    Bar->SetDialogTitle("OpenFOAM Writer");

    OpenFOAMWriter *workerThread = new OpenFOAMWriter;
    workerThread->SetPaths(file_name,WriteIn);
    QObject::connect(workerThread,&OpenFOAMWriter::textToBar,Bar,&LoadBar::SetText);
    QObject::connect(workerThread,&OpenFOAMWriter::fileNumber,Bar,&LoadBar::SetValue);
    QObject::connect(workerThread,&OpenFOAMWriter::numberOfFiles,Bar,&LoadBar::SetNumberOfFiles);
    QObject::connect(workerThread, &OpenFOAMWriter::finished, workerThread, &QObject::deleteLater);
    QObject::connect(workerThread, &OpenFOAMWriter::destroyed,Bar,&LoadBar::close);
    QObject::connect(Bar,&LoadBar::finished,Bar,&LoadBar::deleteLater);
    QObject::connect(workerThread,&OpenFOAMWriter::ArrayNames,this,&MainWindow::Load_Case);
    workerThread->start();
    Bar->exec();
}

void MainWindow::Load_Case(QList<QString> ArrayNames,QString Case)
{
    //Leo la geometria
    QString filee =Case + "geometry.vtu";

    Geometry *NewGeometry = new Geometry;
    NewGeometry->ReadFile(filee);
    NewGeometry->render_Win = this->renderWindow;
    unsigned long i;
    int a =1;
    std::string name(NewGeometry->Name);
    next:
    for(i=0;i<Geometries.size();i++)
    {
        if(NewGeometry->Name == Geometries[i]->Name)
        {
            NewGeometry->Name = name + std::to_string(a);
            a++;
            goto next;
        }
    }

    if(Geometries.size()!=0)
        Geometries[SelectedGeometry]->HideWidgets();

    Geometries.push_back(NewGeometry);

    SelectedGeometry = Geometries.size()-1;

    Geometries[SelectedGeometry]->ShowWidgets();

    ren->AddActor(NewGeometry->Actor);
    NewGeometry->SetScalarBarWidget();
    ren->ResetCamera(NewGeometry->GetBounds());
    renderWindow->Render();
    NewGeometry->type = Main;

    this->ui->tab_2->setLayout(Geometries[SelectedGeometry]->List_Properties[0]);
    QComboBox::disconnect(FieldList,SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeField(QString)));

    TreeHandeler->addTreeRoot(QString::fromStdString(NewGeometry->Name));

    UpdateInterface();

    QComboBox::connect(FieldList,SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeField(QString)));
    int num_of_geom = TreeHandeler->Tree->topLevelItemCount();
    TreeHandeler->Tree->selectionModel()->clearSelection();
    TreeHandeler->Tree->topLevelItem(num_of_geom - 1)->setSelected(true);

    //Agrego los campos

    if(Geometries.empty())
        return;

    foreach (QString field_name, ArrayNames)
    {
        if(!Geometries[SelectedGeometry]->InsertNewField(Case +field_name))
        {
            continue;
        }
        field_name = field_name.mid(field_name.lastIndexOf("/") +1 );

        FieldList->addItem(field_name);

    }
    FieldList->setCurrentIndex(FieldList->count()-1);
    UpdateInterface();
    renderWindow->Render();
}

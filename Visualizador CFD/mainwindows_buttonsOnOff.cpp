#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <QMenu>
void MainWindow::DisableEnable()
{
    QMenu* filtermenu = this->menuBar()->findChild<QMenu *>("menuFilters");
    QMenu* fileMenu = this->menuBar()->findChild<QMenu *>("menuFILE");


    int i;
    if(Geometries.empty())
    {
        for(i=0;i<filtermenu->actions().size();i++)
        {
            filtermenu->actions().at(i)->setEnabled(false);
        }
        for(i=0;i<fileMenu->actions().size();i++)
        {
            if(fileMenu->actions().at(i)->text().toStdString() == "Add Field")
            {
                fileMenu->actions().at(i)->setEnabled(false);
            }
        }
        ActorShow->setEnabled(false);
        return;
    }
    ActorShow->setEnabled(true);

    if(Geometries[SelectedGeometry]->type == Filter_type::Main)
    {
        if(Geometries[SelectedGeometry]->Field_list.size()!=1 && Geometries[SelectedGeometry]->NextFilter.size()!=0)
        {
            for(i=0;i<fileMenu->actions().size();i++)
            {
                if(fileMenu->actions().at(i)->text().toStdString() == "Add Field")
                {
                    fileMenu->actions().at(i)->setEnabled(false);
                }
            }
        }
        else
        {
            for(i=0;i<fileMenu->actions().size();i++)
            {
                fileMenu->actions().at(i)->setEnabled(true);
            }
        }
    }
    else
    {
        for(i=0;i<fileMenu->actions().size();i++)
        {
            if(fileMenu->actions().at(i)->text().toStdString() == "Add Field")
            {
                fileMenu->actions().at(i)->setEnabled(false);
            }
        }
    }
    QList<QAction*> actions = ui->toolBar->actions();

    if(Geometries[SelectedGeometry]->type == Filter_type::OpenFOAMcase)
    {
        OpenFOAMRunTime* a = (OpenFOAMRunTime *)Geometries[SelectedGeometry];
        foreach (QAction* ac, actions)
        {
            if(ac->text()=="PlayAndPause Online Mode")
            {
                ac->setEnabled(true);
                ac->setChecked(a->Update_CheckBox->isChecked());
            }
        }
    }
    else if(Geometries[SelectedGeometry]->type == Filter_type::FortranGPFEP)
    {
        FortranOnRealTime* a = (FortranOnRealTime *)Geometries[SelectedGeometry];
        foreach (QAction* ac, actions)
        {
            if(ac->text()=="PlayAndPause Online Mode")
            {
                ac->setEnabled(true);
                ac->setChecked(a->Update_CheckBox->isChecked());
            }
        }
    }
    else
    {
        foreach (QAction* ac, actions)
        {
            if(ac->text()=="PlayAndPause Online Mode")
            {
                ac->setEnabled(false);
                ac->setChecked(false);
            }
        }
    }

    switch (Geometries[SelectedGeometry]->type)
    {

    case Filter_type::Main:
    {
        if(Geometries[SelectedGeometry]->Field_list.size()==1)
        {
            for(i=0;i<filtermenu->actions().size();i++)
            {
                if(filtermenu->actions().at(i)->text().toStdString() == "Clip")
                {
                    filtermenu->actions().at(i)->setEnabled(true);
                }
                else if(filtermenu->actions().at(i)->text().toStdString() == "Slice")
                {
                    filtermenu->actions().at(i)->setEnabled(true);
                }
                else
                {
                    filtermenu->actions().at(i)->setEnabled(false);
                }
            }
        }
        else if(Geometries[SelectedGeometry]->HaveVector())
        {
            for(i=0;i<filtermenu->actions().size();i++)
            {
                filtermenu->actions().at(i)->setEnabled(true);
            }
        }
        else
        {
            for(i=0;i<filtermenu->actions().size();i++)
            {
                if(filtermenu->actions().at(i)->text().toStdString() == "Clip")
                {
                    filtermenu->actions().at(i)->setEnabled(true);
                }
                else if(filtermenu->actions().at(i)->text().toStdString() == "Slice")
                {
                    filtermenu->actions().at(i)->setEnabled(true);
                }
                else
                {
                    filtermenu->actions().at(i)->setEnabled(false);
                }
            }
        }
        break;
    }
    case Filter_type::Normal:
    {
        if(Geometries[SelectedGeometry]->Field_list.size()==0)
        {
            for(i=0;i<filtermenu->actions().size();i++)
            {
                if(filtermenu->actions().at(i)->text().toStdString() == "Clip")
                {
                    filtermenu->actions().at(i)->setEnabled(true);
                }
                else if(filtermenu->actions().at(i)->text().toStdString() == "Slice")
                {
                    filtermenu->actions().at(i)->setEnabled(true);
                }
                else
                {
                    filtermenu->actions().at(i)->setEnabled(false);
                }
            }
        }
        else if(Geometries[SelectedGeometry]->HaveVector())
        {
            for(i=0;i<filtermenu->actions().size();i++)
            {
                if(filtermenu->actions().at(i)->text().toStdString() == "Path Lines")
                {
                    filtermenu->actions().at(i)->setEnabled(false);
                }
                else
                {
                    filtermenu->actions().at(i)->setEnabled(true);
                }
            }
        }
        else
        {
            for(i=0;i<filtermenu->actions().size();i++)
            {
                if(filtermenu->actions().at(i)->text().toStdString() == "Clip")
                {
                    filtermenu->actions().at(i)->setEnabled(true);
                }
                else if(filtermenu->actions().at(i)->text().toStdString() == "Gradient")
                {
                    filtermenu->actions().at(i)->setEnabled(true);
                }
                else
                {
                    filtermenu->actions().at(i)->setEnabled(false);
                }
            }
        break;
        }
    }
    case Filter_type::StreamLines:
    {
        for(i=0;i<filtermenu->actions().size();i++)
        {
            if(filtermenu->actions().at(i)->text().toStdString() == "Clip")
            {
                filtermenu->actions().at(i)->setEnabled(true);
            }
            else if(filtermenu->actions().at(i)->text().toStdString() == "Slice")
            {
                filtermenu->actions().at(i)->setEnabled(true);
            }
            else
            {
                filtermenu->actions().at(i)->setEnabled(false);
            }
        }
        break;

    }
    case Filter_type::CaudalPlano:
    {
        for(i=0;i<filtermenu->actions().size();i++)
        {
            filtermenu->actions().at(i)->setEnabled(false);
        }
        break;

    }
    case Filter_type::PathLines:
    {
        for(i=0;i<filtermenu->actions().size();i++)
        {
            if(filtermenu->actions().at(i)->text().toStdString() == "Clip")
            {
                filtermenu->actions().at(i)->setEnabled(true);
            }
            else if(filtermenu->actions().at(i)->text().toStdString() == "Slice")
            {
                filtermenu->actions().at(i)->setEnabled(true);
            }
            else
            {
                filtermenu->actions().at(i)->setEnabled(false);
            }
        }
        break;
    }
    case Filter_type::Slice:
    {
        if(Geometries[SelectedGeometry]->Field_list.size()==0)
        {
            for(i=0;i<filtermenu->actions().size();i++)
            {
                if(filtermenu->actions().at(i)->text().toStdString() == "Clip")
                {
                    filtermenu->actions().at(i)->setEnabled(true);
                }
                else if(filtermenu->actions().at(i)->text().toStdString() == "Slice")
                {
                    filtermenu->actions().at(i)->setEnabled(true);
                }
                else
                {
                    filtermenu->actions().at(i)->setEnabled(false);
                }
            }
        }
        else if(Geometries[SelectedGeometry]->HaveVector())
        {
            for(i=0;i<filtermenu->actions().size();i++)
            {
                if(filtermenu->actions().at(i)->text().toStdString() == "Path Lines")
                {
                    filtermenu->actions().at(i)->setEnabled(false);
                }
                else
                {
                    filtermenu->actions().at(i)->setEnabled(true);
                }
            }
        }
        else
        {
            for(i=0;i<filtermenu->actions().size();i++)
            {
                if(filtermenu->actions().at(i)->text().toStdString() == "Clip")
                {
                    filtermenu->actions().at(i)->setEnabled(true);
                }
                else if(filtermenu->actions().at(i)->text().toStdString() == "Gradient")
                {
                    filtermenu->actions().at(i)->setEnabled(true);
                }
                else if(filtermenu->actions().at(i)->text().toStdString() == "Slice")
                {
                    filtermenu->actions().at(i)->setEnabled(true);
                }
                else
                {
                    filtermenu->actions().at(i)->setEnabled(false);
                }
            }
        }
        break;
    }
    case Filter_type::PuntosEstancamiento:
    {
        for(i=0;i<filtermenu->actions().size();i++)
        {
            if(filtermenu->actions().at(i)->text().toStdString() == "Clip")
            {
                filtermenu->actions().at(i)->setEnabled(true);
            }
            else if(filtermenu->actions().at(i)->text().toStdString() == "Slice")
            {
                filtermenu->actions().at(i)->setEnabled(true);
            }
            else
            {
                filtermenu->actions().at(i)->setEnabled(false);
            }
        }
        break;

    }
    case Filter_type::VortexFinder:
    {
        for(i=0;i<filtermenu->actions().size();i++)
        {
            if(filtermenu->actions().at(i)->text().toStdString() == "Clip")
            {
                filtermenu->actions().at(i)->setEnabled(true);
            }
            else if(filtermenu->actions().at(i)->text().toStdString() == "Slice")
            {
                filtermenu->actions().at(i)->setEnabled(true);
            }
            else
            {
                filtermenu->actions().at(i)->setEnabled(false);
            }
        }
        break;

    }
    case Filter_type::FortranGPFEP:
    {
        for(i=0;i<filtermenu->actions().size();i++)
        {
            if(filtermenu->actions().at(i)->text().toStdString()=="Path Lines")
            {
                filtermenu->actions().at(i)->setEnabled(false);
            }
            else
                filtermenu->actions().at(i)->setEnabled(true);
        }
        break;
    }
    case Filter_type::OpenFOAMcase:
    {
        for(i=0;i<filtermenu->actions().size();i++)
        {
            if(filtermenu->actions().at(i)->text().toStdString()=="Path Lines")
            {
                filtermenu->actions().at(i)->setEnabled(false);
            }
            else
                filtermenu->actions().at(i)->setEnabled(true);
        }
        break;
    }
    case Filter_type::ViscousForces:
    {
        for(i=0;i<filtermenu->actions().size();i++)
        {
            if(filtermenu->actions().at(i)->text().toStdString() == "Clip")
            {
                filtermenu->actions().at(i)->setEnabled(true);
            }
            else if(filtermenu->actions().at(i)->text().toStdString() == "Slice")
            {
                filtermenu->actions().at(i)->setEnabled(true);
            }
            else
            {
                filtermenu->actions().at(i)->setEnabled(false);
            }
        }
        break;
    }
    }

}

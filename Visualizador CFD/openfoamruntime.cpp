#include "openfoamruntime.h"

#include <boost/algorithm/string.hpp>

Q_DECLARE_METATYPE(Field*);
Q_DECLARE_METATYPE(std::vector<Field*>);

OpenFOAMRunTime::OpenFOAMRunTime()
{
    PropertiesConfig();
    ParallelReader = new ReaderThread;
    UpdateFlag= true;

}

OpenFOAMRunTime::OpenFOAMRunTime(QString CasePath,vtkSmartPointer<vtkRenderWindow> renderWin)
{
    this->Case_path = CasePath.toStdString();
    PropertiesConfig();
    this->render_Win = renderWin;
    Name = CasePath.mid(CasePath.lastIndexOf("/") +1 ).toStdString();
    ParallelReader = new ReaderThread; //Para leer en paralelo

    SelectedField =0;
    CurrentTimeStep =0;
    UpdateFlag= true;

    ReadCase(CasePath);


    if(Grid->GetPointData()->GetNumberOfArrays()!=0)
    {
        Mapper->SetScalarRange(Grid->GetPointData()->GetScalars()->GetRange());
    }
}

OpenFOAMRunTime::~OpenFOAMRunTime()
{
    VisualTimer->stop();
    QObject::disconnect(ParallelReader,&ReaderThread::resultReady,this,&OpenFOAMRunTime::ChangeFields);
    QObject::connect(ParallelReader,&ReaderThread::finished,ParallelReader,&ReaderThread::deleteLater);
    delete VisualTimer;
    delete Update_GroupBox;

}

void OpenFOAMRunTime::ReadCase(QString CasePath)
{
    this->type = OpenFOAMcase;
    this->Case_path = CasePath.toStdString();

    ReadGeometry();
    if(Grid->GetNumberOfCells()==0)
        return;

    std::string line;
    std::string visualPath = Case_path + "/VisualCFD/VisualCFD.txt";

    std::ifstream FieldFile(visualPath);

    if(!FieldFile.is_open())
        return;

    std::getline(FieldFile,line);
    if(line == "parRun")
    {
        std::getline(FieldFile,line);
        std::getline(FieldFile,line);
    }
    try
    {
        CurrentTime = std::stod(line);
        vtkSmartPointer<vtkDoubleArray> TIME = vtkSmartPointer<vtkDoubleArray>::New();
        TIME->SetName("TIME");
        TIME->InsertNextValue(CurrentTime);

        Grid->GetFieldData()->AddArray(TIME);
    }
    catch(const std::invalid_argument&)
    {
    }
    FieldFile.close();
    qRegisterMetaType<std::vector<Field*>>();
    QObject::connect(ParallelReader,&ReaderThread::resultReady,this,&OpenFOAMRunTime::ChangeFields);
    QLineEdit::connect(Update_RefreshTime_LineEdit,SIGNAL(editingFinished()),this,SLOT(ChangeRefreshTime()));
    QObject::connect(ParallelDownStream,&ParallelSignalsDownStream::EndUpdating,this,&OpenFOAMRunTime::VisualTimerRestart);
    ParallelReader->SetGridAndPath(Grid,Case_path);
    //ParallelReader->run();

    VisualTimer = new QTimer;
    QTimer::connect(VisualTimer,SIGNAL(timeout()),this,SLOT(UpdateRead()));
    RefreshTime = 200; //cada 0.2 segundos por defecto
    Update_RefreshTime_LineEdit->setText("200");
    ParallelReader->run();


}

void OpenFOAMRunTime::ReadGeometry()
{
    vtkSmartPointer<vtkOpenFOAMReader> reader = vtkSmartPointer<vtkOpenFOAMReader>::New();
    std::string path = Case_path + "/";
    if(!reader->CanReadFile(path.c_str()))
    {
        return;
    }
    reader->SetFileName(path.c_str());
    reader->Update();
    //reader->Update();

    vtkCompositeDataSet *input =
        dynamic_cast<vtkCompositeDataSet *>(reader->GetOutput());


    vtkSmartPointer<vtkDataObjectTreeIterator> iter = vtkSmartPointer<vtkDataObjectTreeIterator>::New();
    iter->SetDataSet(input);
    iter->SkipEmptyNodesOn();
    iter->VisitOnlyLeavesOn();


    vtkDataObject *dso = iter->GetCurrentDataObject();

    vtkUnstructuredGrid *grid = dynamic_cast<vtkUnstructuredGrid *>(dso);

    this->Grid->CopyStructure(grid);
    Mapper->SetInputData(Grid);
    Mapper->Update();


}

void OpenFOAMRunTime::ChangeToThisTime(int i)
{
    //Override to do nothing. Only have 1 timestep
}

void OpenFOAMRunTime::GetOutput(vtkSmartPointer<vtkUnstructuredGrid> GridData)
{
   mute.lock();
   GridData->ShallowCopy(Grid);
   mute.unlock();
}
vtkDataSet* OpenFOAMRunTime::GetGeoDataOutput()
{
    return Grid;
}

void OpenFOAMRunTime::PropertiesConfig()
{
    QVBoxLayout *GeoPropLayout = List_Properties[0];
    Update_GroupBox = new QGroupBox;
    Update_GroupBox->setTitle("AutoUpdate");
    Update_GroupBox->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");

    Update_Layout = new QGridLayout(Update_GroupBox);
    Update_CheckBox = new QCheckBox(Update_GroupBox);
    Update_RefreshTime_label = new QLabel(Update_GroupBox);
    Update_RefreshTime_label->setText("Refresh Time [ms]");
    Update_RefreshTime_LineEdit = new QLineEdit(Update_GroupBox);
    Update_RefreshTime_LineEdit->setValidator(new QIntValidator(200,1000000000,Update_RefreshTime_LineEdit));
    Update_CheckBox->setText("Refresh");
    Update_CheckBox->setChecked(true);

    Update_Layout->addWidget(Update_RefreshTime_label,0,0);
    Update_Layout->addWidget(Update_RefreshTime_LineEdit,0,1);
    Update_Layout->addWidget(Update_CheckBox,1,0,1,4);

    Update_GroupBox->setLayout(Update_Layout);
    QCheckBox::connect(Update_CheckBox,SIGNAL(released()),this,SLOT(AutoUpdate()));

    GeoPropLayout->addWidget(Update_GroupBox);

    //Parametros dentro de const directory
    std::string transportPropPath = Case_path + "/constant/transportProperties";
    std::string line;
    std::ifstream TransPropFile(transportPropPath);
    if(!TransPropFile.is_open())
    {
        return;
        //goto SkipTransProp;
    }
    TransportProperties_GroupBox = new QGroupBox;
    TransportProperties_Layout = new QGridLayout(TransportProperties_GroupBox);
    TransportProperties_Layout->setAlignment(Qt::AlignmentFlag::AlignTop);
    while(std::getline(TransPropFile,line))
    {
        if(line.find('[')!= std::string::npos && line.find(']')!= std::string::npos)
        {
            int a = line.find('['), b = line.find(']');
            std::string prop = line.substr(0,line.find(" "));
            TransProperties_Label.push_back(new QLabel(QString::fromStdString(prop),TransportProperties_GroupBox));
            std::string unidades_Aux = line.substr(a,b+1);
            unidades_Aux = unidades_Aux.substr(0,unidades_Aux.find(']')+1);
            Unidades.push_back(unidades_Aux);
            std::string value = line.substr(b+2);
            value = value.substr(0,value.find(";"));
            QLineEdit *aux_lineEdit = new QLineEdit(QString::fromStdString(value),TransportProperties_GroupBox);
            QDoubleValidator *validator= new QDoubleValidator(aux_lineEdit);
            validator->setNotation(QDoubleValidator::StandardNotation);
            aux_lineEdit->setValidator(validator);
            QObject::connect(aux_lineEdit,SIGNAL(editingFinished()),this,SLOT(WriteTransportProperties()));
            TransProperties_LineEdit.push_back(aux_lineEdit);
        }
    }
    for(unsigned long i=0; i< TransProperties_Label.size();i++)
    {
        TransportProperties_Layout->addWidget(TransProperties_Label[i],i,0);
        TransportProperties_Layout->addWidget(TransProperties_LineEdit[i],i,1);
    }
    TransportProperties_GroupBox->setLayout(TransportProperties_Layout);
    QVBoxLayout *transp_prop_tab = new QVBoxLayout;
    transp_prop_tab->addWidget(TransportProperties_GroupBox);
    AddFilterProperties(transp_prop_tab);
    Properties_names.push_back("Transport Prop.");

    TransPropFile.close();
    //SkipTransProp:

}
void OpenFOAMRunTime::WriteTransportProperties()
{
    std::string transportPropPath = Case_path + "/constant/transportProperties";
    std::string tempTransProp = transportPropPath + "temp";
    std::string contenido;
    std::ifstream fs(transportPropPath); //leer de este archivo
    std::ofstream fstemp(tempTransProp); //escribir en este archivo

    if(!fs || !fstemp) //no se pudo abrir alguno de los 2
    {
        return;
    }

    //modificar linea a linea
    unsigned long i=0;
    std::string label = TransProperties_Label[i]->text().toStdString();

    while(std::getline(fs,contenido))
    {
        if(contenido.find(label)!=std::string::npos){  //se encontro

            contenido = label +"      " + Unidades[i] + " " +TransProperties_LineEdit[i]->text().toStdString()+";"; //reemplazar
            i++;
            if(i<TransProperties_Label.size())
            {
                label = TransProperties_Label[i]->text().toStdString();
            }
        }
        fstemp << contenido << std::endl;
    }
    //reemplazar un archivo por otro
    fs.close();
    fstemp.close();

    std::remove(transportPropPath.c_str());
    // borrar el original
    std::rename(tempTransProp.c_str(), transportPropPath.c_str());  // renombrar el temporal

    //siguiendo la logica que usaste en el resto
}
void OpenFOAMRunTime::UpdateRead()
{
    mute.lock();
    std::string line;
    std::string visualPath = Case_path + "/VisualCFD/VisualCFD.txt";
    std::ifstream FieldFile(visualPath);

    if(!FieldFile.is_open())
    {
        mute.unlock();
        return;
    }
    if(!std::getline(FieldFile,line))
    {
        mute.unlock();
        return;
    }
    if(line == "parRun")
    {
        if(!std::getline(FieldFile,line))
        {
            mute.unlock();
            return;
        }
        if(!std::getline(FieldFile,line))
        {
            mute.unlock();
            return;
        }
    }
    double testTime;
    try
    {
        testTime = std::stod(line);
    }
    catch(const std::invalid_argument&)
    {
        FieldFile.close();
        mute.unlock();
        return;
    }

    FieldFile.close();

    if(testTime==CurrentTime)
    {
        mute.unlock();
        return;
    }
    CurrentTime=testTime;
    VisualTimer->stop();
    ParallelReader->start();
    mute.unlock();

}

void OpenFOAMRunTime::AutoUpdate()
{
    mute.lock();
    if(!Update_CheckBox->isChecked())
    {
        VisualTimer->stop();
        emit PlayAndStop(false);
        UpdateFlag=false;
    }
    else
    {
        VisualTimer->start(RefreshTime);
        emit PlayAndStop(true);
        UpdateFlag= true;
    }
    mute.unlock();

}
void OpenFOAMRunTime::ChangeRefreshTime()
{
    mute.lock();
    if(Update_RefreshTime_LineEdit->text().isEmpty())
        return;
    RefreshTime = std::stod(Update_RefreshTime_LineEdit->text().toStdString());
    if(UpdateFlag)
    {
        VisualTimer->start(RefreshTime);
    }
    mute.unlock();
}
void OpenFOAMRunTime::ChangeUpdate_CheckBox(bool value)
{
    mute.lock();
    if(value)
    {
        Update_CheckBox->setCheckState(Qt::Checked);
    }
    else
    {
        Update_CheckBox->setCheckState(Qt::Unchecked);
    }
    UpdateFlag = value;
    mute.unlock();
    AutoUpdate();
}
void OpenFOAMRunTime::ChangeFields(std::vector<Field*> new_Fields_List)
{
    mute.lock();
    if(new_Fields_List.empty() && UpdateFlag)
    {
        VisualTimer->start(RefreshTime);
        mute.unlock();
        return;
    }
    //Primero comparo los nombres buscando igualdades para machear los componentes
    int ind;
    std::vector<Field *> aux_field_list;
    this->DisconectSonsWidgets();
    try
    {
        for(unsigned long i=0; i<new_Fields_List.size();i++)
        {
            ind=0;
            for(unsigned long j=0;j<Field_list.size();j++)
            {
                if(new_Fields_List[i]->Field_Name == Field_list[j]->Field_Name)
                {
                    new_Fields_List[i]->selectedComponent = Field_list[j]->selectedComponent;
                    delete Field_list[j];
                    Field_list[j] =new Field(new_Fields_List[i]);
                    ind =1;
                    break;
                }
            }
            if(ind==0)
            {
                Field_list.push_back(new Field(new_Fields_List[i]));
            }

        }
        for(unsigned long i=0; i<Field_list.size(); i++)
        {
            AddFields(i);
        }
        SetVectors();
        Mapper->Update();
        SendSignalsDownStream();
        this->Times->SetValue(0,CurrentTime);
    }  catch (...) {
        mute.unlock();
        if(new_Fields_List.empty() && UpdateFlag)
        {
            VisualTimer->start(RefreshTime);
            return;
        }
    }
    emit UpdateInterfaseSignal();
    mute.unlock();

}
void OpenFOAMRunTime::VisualTimerRestart()
{
    mute.lock();
    render_Win->Render();
    this->ReconectSonsWidgets();
    if(UpdateFlag)
    {
        VisualTimer->start(RefreshTime);
    }
    mute.unlock();
}

//----------------------------------------------------------------------------

//ReaderThread class --- Interal functions

void ReaderThread::SetGridAndPath(vtkUnstructuredGrid *grid, std::string path)
{
    Grid = grid;
    Case_path = path;
    qRegisterMetaType<std::vector<Field*>>();
}

void ReaderThread::ReadVisualCFD()
{
    std::string visualPath = Case_path + "/VisualCFD/VisualCFD.txt";
    //This lines are for avoiding overwriting by the solver
    std::ifstream    inFile(visualPath);
    std::string newpath = Case_path + "/VisualCFD/copyVisualCFD.txt";
    std::ofstream    outFile(newpath);
    try {
        outFile << inFile.rdbuf();
        inFile.close();
        outFile.close();
    }  catch (...) {
        std::cout << "error copia" << std::endl;
        return;
    }
    // -----------------

    std::ifstream FieldFile(newpath);
    if(!FieldFile.is_open())
        return;
    std::string line="";

    std::vector<vtkSmartPointer<vtkDoubleArray>> fields_celldata;

    //Reading fields -------------------------------------------
    if(!std::getline(FieldFile,line))
        return;
    if(line == "parRun")
    {
        if(!std::getline(FieldFile,line))
            return;
        int processors_number;
        try
        {
            processors_number = std::stoi(line);
        }
        catch(const std::invalid_argument&)
        {
            return;
        }
        cell_id_per_processor.resize(processors_number);
        if(reducedIDs.size()==0) //Solo los leo una vez
        {
            if(cell_id_per_processor[0].size()==0)
            {
                if(!ReadProcessorsCellIDs()) //Obtengo las celdas que maneja cada procesador
                    return;
            }
            reduceCellIDs();
        }

        return ReadParallelVisualCFD(); //Leo los campos de cada procesador y los matcheo con la celda correcta.
    }

    std::vector<std::vector<std::vector<double>>> fieldss;
    std::vector<std::string> namess;

    start:

    while(std::getline(FieldFile,line))
    {
        if(line.find("Field")!=std::string::npos)
        {

            std::string FieldName=line.substr(7); // Field: U
            if(!std::getline(FieldFile,line))
            {
                FieldFile.close();
                goto endreading; //Incomplete field
            }
            std::vector<std::vector<double>> newfield;
            int number_of_values;
            try
            {
                number_of_values = std::stoi(line);
            }
            catch(const std::invalid_argument&)
            {
                FieldFile.close();
                goto endreading;
            }

            if(number_of_values != Grid->GetNumberOfCells())
            {
                while(std::getline(FieldFile,line))
                {
                    if(line.find("End of Field:")!=std::string::npos)
                    {
                        goto start;
                    }
                }
                FieldFile.close();
                goto endreading;
            }
            if(!std::getline(FieldFile,line))
            {
                FieldFile.close();
                goto endreading; //Incomplete field
            }
            int number_of_components;
            try
            {
                number_of_components = std::stoi(line);
            }
            catch(const std::invalid_argument&)
            {
                FieldFile.close();
                goto endreading;
            }
            if(number_of_components==1)
            {
                for(int i=0; i<number_of_values;i++)
                {
                    if(!std::getline(FieldFile,line))
                    {
                        FieldFile.close();
                        goto endreading; //Incomplete field
                    }
                    std::vector<double> value;
                    try
                    {
                        value.push_back(std::stod(line));
                    }
                    catch(const std::invalid_argument&)
                    {
                        FieldFile.close();
                        goto endreading;
                    }
                    newfield.push_back(value);
                }
            }
            else if(number_of_components==3)
            {
                for(int i=0; i<number_of_values;i++)
                {
                    if(!std::getline(FieldFile,line))
                    {
                        FieldFile.close();
                        goto endreading;
                    }
                    std::vector<std::string> U;
                    try {
                        boost::split(U,line,boost::is_any_of("\t"));
                    }  catch (...) {
                        FieldFile.close();
                        goto endreading;
                    }
                    std::vector<double> value;
                    for(int j=0; j<3;j++)
                    {
                        try
                        {
                            double aa = std::stod(U[j]);
                            value.push_back(aa);
                        }
                        catch(...)
                        {
                            FieldFile.close();
                            goto endreading;
                        }
                    }
                    newfield.push_back(value);
                }
            }
            else //Invalid field
            {
                while(std::getline(FieldFile,line))
                {
                    if(line.find("End of Field:")!=std::string::npos)
                    {
                        goto start;
                    }
                }
                FieldFile.close();
                goto endreading;

            }

            fieldss.push_back(newfield);
            namess.push_back(FieldName);

            if(!std::getline(FieldFile,line))
            {
                FieldFile.close();
                goto endreading; //Incomplete field
            }
        }
    }
    endreading:
    while(Fieldss.size()!=0)
        Fieldss.erase(Fieldss.end());
    while(Namess.size()!=0)
        Namess.erase(Namess.end());
    Fieldss= fieldss;
    Namess =namess;

    if(Fieldss.size()!=0)
    {
        for(unsigned long i=0; i < Fieldss.size();i++)
        {
            vtkSmartPointer<vtkDoubleArray> array = vtkSmartPointer<vtkDoubleArray>::New();
            array->SetName(Namess[i].c_str());
            array->SetNumberOfComponents(Fieldss[i][0].size());
            //array->Resize(Fieldss[i].size());
            for(unsigned long j=0;j< Fieldss[i].size();j++)
            {
                double aux[array->GetNumberOfComponents()];
                for(int l=0;l<array->GetNumberOfComponents();l++)
                {
                    aux[l]=Fieldss[i][j][l];
                }
                array->InsertNextTuple(aux);
            }
            fields_celldata.push_back(array);
        }
    }
    else
    {
        return;
    }

    //Changing cell data to point data ----------------------

    vtkSmartPointer<vtkCellDataToPointData> CellToPoint = vtkSmartPointer<vtkCellDataToPointData>::New();
    vtkSmartPointer<vtkUnstructuredGrid> auxilary_grid = vtkSmartPointer<vtkUnstructuredGrid>::New();
    auxilary_grid->CopyStructure(Grid);
    for(unsigned long i=0; i<fields_celldata.size();i++)
    {
        auxilary_grid->GetCellData()->AddArray(fields_celldata[i]);
    }
    CellToPoint->SetInputData(auxilary_grid);
    CellToPoint->PassCellDataOff();
    CellToPoint->Update();

    //No se elimina aca sino en la función changeFields de OpenFOAMonRuntime
    //while(new_Field_list.size()!=0)
    //{
    //    new_Field_list.erase(new_Field_list.begin());
    //}
    std::vector<Field *> aux_field_list;

    for(int i=0; i<CellToPoint->GetOutput()->GetPointData()->GetNumberOfArrays();i++)
    {
        vtkSmartPointer<vtkDataArray> array = CellToPoint->GetOutput()->GetPointData()->GetArray(i);
        Field *aux_field = new Field(array);
        aux_field_list.push_back(aux_field);
    }
    new_Field_list = aux_field_list;

}

void ReaderThread::ReadParallelVisualCFD()
{

    //Here I create the fields to read and size the vector who contain it.
    //Then I call ReadVisualCFDProcessorI which read the file from processor I and return the fields as a vector<vector<double>>
    //After that I create de vtkDoubleArray and match the cell id of each processor with their field values
    std::vector<vtkSmartPointer<vtkDoubleArray>> fields_celldata;

    //Libero los campos de antes

    while(Fields_per_processor.size()!=0)
    {
        Fields_per_processor.erase(Fields_per_processor.end());
    }
    while(Fields_names_per_processor.size()!=0)
    {
        Fields_names_per_processor.erase(Fields_names_per_processor.end());
    }


    Fields_per_processor.resize(cell_id_per_processor.size());
    Fields_names_per_processor.resize(cell_id_per_processor.size());
    for(unsigned long i=0;i<cell_id_per_processor.size();i++)
    {
        ReadVisualCFDProcessorI(i);
    }
    reduceFields();

    if(Fieldss.size()!=0)
    {
        for(unsigned long i=0; i < Fieldss.size();i++)
        {
            vtkSmartPointer<vtkDoubleArray> array = vtkSmartPointer<vtkDoubleArray>::New();
            array->SetName(Namess[i].c_str());
            array->SetNumberOfComponents(Fieldss[i][0].size());
            array->Resize(reducedIDs.size());
            for(unsigned long j=0;j< Fieldss[i].size();j++)
            {
                double aux[array->GetNumberOfComponents()];
                for(int l=0;l<array->GetNumberOfComponents();l++)
                {
                    aux[l]=Fieldss[i][j][l];
                }
                array->InsertTuple(reducedIDs[j],aux);
            }
            fields_celldata.push_back(array);
        }
    }

    //Changing cell data to point data ----------------------

    vtkSmartPointer<vtkCellDataToPointData> CellToPoint = vtkSmartPointer<vtkCellDataToPointData>::New();
    vtkSmartPointer<vtkUnstructuredGrid> auxilary_grid = vtkSmartPointer<vtkUnstructuredGrid>::New();
    auxilary_grid->CopyStructure(Grid);
    for(unsigned long i=0; i<fields_celldata.size();i++)
    {
        auxilary_grid->GetCellData()->AddArray(fields_celldata[i]);
    }
    CellToPoint->SetInputData(auxilary_grid);
    CellToPoint->PassCellDataOff();
    CellToPoint->Update();


    while(new_Field_list.size()!=0)
    {
        new_Field_list.erase(new_Field_list.begin());
    }

    for(int i=0; i<CellToPoint->GetOutput()->GetPointData()->GetNumberOfArrays();i++)
    {
        vtkSmartPointer<vtkDataArray> array = CellToPoint->GetOutput()->GetPointData()->GetArray(i);
        Field *aux_field = new Field(array);
        new_Field_list.push_back(aux_field);
    }

}

bool ReaderThread::ReadProcessorsCellIDs()
{    
    for(unsigned long i=0;i<cell_id_per_processor.size();i++)
    {
        std::string file = Case_path + "/processor" + std::to_string(i) +"/constant/polyMesh/cellProcAddressing";
        std::ifstream cellsfile(file);
        std::string line;
        if(!cellsfile.is_open())
        {
            return false; //No se encontro el archivo de celdas
        }
        while(std::getline(cellsfile,line))
        {

            if(line == "(")
            {
                while(std::getline(cellsfile,line))
                {
                    if(line == ")")
                    {
                        break;
                    }
                    cell_id_per_processor[i].push_back(std::stoul(line));
                }
            }
            if(line==")")
                break;
        }
        cellsfile.close();
    }
    return true;
}

void ReaderThread::ReadVisualCFDProcessorI(int i)
{

    std::vector<std::vector<std::vector<double>>> processor_fields;
    std::vector<std::string> processor_fields_names;
    std::string filepath = Case_path + "/VisualCFD/processor" + std::to_string(i) +".txt";
    //This lines are for avoiding overwriting by the solver
    std::ifstream    inFile(filepath);
    std::string newpath = Case_path + "/VisualCFD/copyprocessor" + std::to_string(i) +".txt";
    std::ofstream    outFile(newpath);

    if(!inFile || !outFile)
    {
        Fields_per_processor[i] = processor_fields;
        Fields_names_per_processor[i] = processor_fields_names;
        return;
    }
    try {
        outFile << inFile.rdbuf();
        inFile.close();
        outFile.close();
    }  catch (...) {
        return;
    }

    // -----------------
    std::ifstream file(newpath);
    std::string line;

    if(!file.is_open())
    {
        return;
    }

    if(!std::getline(file,line))
    {
        goto endreading;
    }

    start:
    try {

    while(std::getline(file,line))
    {
        if(line.find("Field")!=std::string::npos && line.find("End of Field")==std::string::npos)
        {
            std::string FieldName=line.substr(7); // |Field: |U

            if(!std::getline(file,line))
            {
                goto endreading;
            }

            int number_of_values;
            try
            {
                number_of_values = std::stoi(line);
            }            
            catch(const std::invalid_argument&)
            {
                goto endreading;
            }

            if(!std::getline(file,line))
            {
                goto endreading;
            }

            int number_of_components;
            try
            {
                number_of_components = std::stoi(line);
            }            
            catch(const std::invalid_argument&)
            {
                goto endreading;
            }

            std::vector<std::vector<double>> field_;
            if(number_of_components==1)
            {

                for(int i=0; i<number_of_values;i++)
                {
                    if(!std::getline(file,line))
                    {
                        goto endreading; //Incomplete field
                    }
                    std::vector<double> value;
                    try
                    {
                        double aa = std::stod(line);
                        value.push_back(aa);
                    }
                    catch(const std::invalid_argument&)
                    {
                        goto endreading;
                    }
                    field_.push_back(value);
                }
            }   
            else if(number_of_components==3)
            {

                for(int i=0; i<number_of_values;i++)
                {
                    if(!std::getline(file,line))
                    {
                        goto endreading;
                    }
                    std::vector<std::string> U;
                    try {
                        boost::split(U,line,boost::is_any_of("\t"));
                    }  catch (...) {
                        goto endreading;
                    }
                    if(U.size()!=3)
                        goto endreading;
                    std::vector<double> values;
                    for(int j=0; j<3;j++)
                    {
                        try
                        {
                            double aa = std::stod(U[j]);
                            values.push_back(aa);
                        }
                        catch(...)
                        {
                            goto endreading;
                        }
                    }
                    field_.push_back(values);
                }
            }
            else //Invalid field
            {

                while(std::getline(file,line))
                {
                    if(line.find("End of Field:")!=std::string::npos)
                    {
                        goto start;
                    }
                }
                file.close();
                goto endreading;
            }
            if(!std::getline(file,line)) //End of Field
            {
                goto endreading;
            }
            processor_fields_names.push_back(FieldName);
            processor_fields.push_back(field_);
        }

    }
    }  catch (...) {
        file.close();
        goto endreading;
    }

    endreading:

    file.close();

    Fields_per_processor[i] = processor_fields;
    Fields_names_per_processor[i] = processor_fields_names;

}
void ReaderThread::reduceCellIDs()
{
    for(unsigned long i=0; i <cell_id_per_processor.size();i++)
    {
        reducedIDs.insert(reducedIDs.end(),cell_id_per_processor[i].begin(),cell_id_per_processor[i].end());
    }
}

void ReaderThread::reduceFields()
{

    std::vector<std::vector<std::vector<double>>> f_out;
    std::vector<std::string> f_Namess;
    if(Fields_names_per_processor.size()!=0 && Fields_per_processor.size()!=0)
    {
        for(unsigned long i=0;i<Fields_names_per_processor[0].size();i++)
        {
            nextField:
            std::string fieldname = Fields_names_per_processor[0][i];
            std::vector<std::vector<double>> aux_field;
            for(unsigned long j=0; j<Fields_names_per_processor.size();j++)
            {
                unsigned long k=0;
                while(k<Fields_names_per_processor[j].size() && fieldname != Fields_names_per_processor[j][k])
                    k++;
                if(k==Fields_names_per_processor[j].size())
                {
                    i++;
                    if(i==Fields_names_per_processor[0].size())
                        goto endreading;
                    else
                        goto nextField;
                }
                if(Fields_per_processor[j].size()<=k)
                {
                    i++;
                    if(i==Fields_names_per_processor[0].size())
                        goto endreading;
                    else
                        goto nextField;
                }
                aux_field.insert(aux_field.end(),Fields_per_processor[j][k].begin(),Fields_per_processor[j][k].end());
            }
            f_Namess.push_back(fieldname);
            f_out.push_back(aux_field);
        }
    }
    endreading:
    while(Fieldss.size()!=0)
        Fieldss.erase(Fieldss.end());
    while(Namess.size()!=0)
        Namess.erase(Namess.end());

    Fieldss= f_out;
    Namess =f_Namess;

}

void ReaderThread::run()
{
    mute.lock();
    ReadVisualCFD();
    emit resultReady(this->new_Field_list);
    mute.unlock();
}

ReaderThread::~ReaderThread()
{
    reducedIDs.clear();
    for(unsigned long i=0;i<cell_id_per_processor.size();i++)
        cell_id_per_processor[i].clear();
    cell_id_per_processor.clear();
    for(unsigned long i=0;i<Fields_per_processor.size();i++)
    {
        for(unsigned long j=0; j<Fields_per_processor[i].size();j++)
        {
            for(unsigned long k=0; k<Fields_per_processor[i][j].size();k++)
            {
                Fields_per_processor[i][j][k].clear();
            }
            Fields_per_processor[i][j].clear();
        }
        Fields_per_processor[i].clear();
    }
    Fields_per_processor.clear();
    for(unsigned long i=0;i<Fields_names_per_processor.size();i++)
    {
        Fields_names_per_processor[i].clear();
    }
    Fields_names_per_processor.clear();
    for(unsigned long i=0;i<Fieldss.size();i++)
    {
        for(unsigned long j=0;j<Fieldss[i].size();j++)
        {
            Fieldss[i][j].clear();
        }
        Fieldss[i].clear();
    }
    Fieldss.clear();
    Namess.clear();

}

#ifndef OPENFOAMRUNTIME_H
#define OPENFOAMRUNTIME_H


#include <QObject>
#include "geometry.h"

#include <QTimer>
#include <QCheckBox>
#include <QFormLayout>
#include <QString>

#include <vtkCellDataToPointData.h>
#include <vtkCellData.h>

#include <vtkDataObjectTreeIterator.h>
#include <vtkCompositeDataSet.h>
#include <vtkOpenFOAMReader.h>
#include <vtkCompositeDataSet.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkDataObjectTreeIterator.h>
#include <vtkDataObjectTree.h>
#include <vtkDataObject.h>
#include <vtkCollection.h>

#include <boost/lexical_cast.hpp>

//Clase creada para leer en paralelo sin ocupar el procesador principal
class ReaderThread: public QThread
{
    Q_OBJECT

public:
    //Destructor
    ~ReaderThread();
    /**
     * @brief paralel loop
     */
    void run() override;

    /**
     * @brief Pass the grid and the case path to the reader thread
     * @param grid
     * @param path
     */
    void SetGridAndPath(vtkUnstructuredGrid* grid,std::string path);

    QMutex mute;
private:
    /**
     * @brief function for reading. Calls "ReadParallelVisualCFD" if the case is running in parallel.
     */
    void ReadVisualCFD();
    /**
     * @brief function for reading in parallel. Calls ReadVisualCFDProcessorI for each processor and order the fields after all processor have been read.
     */
    void ReadParallelVisualCFD();
    /**
     * @brief Read the cells that each processor have. It is used to match the cells and the fields of each processor
     * @return true is cells where readed, false otherwise
     */
    bool ReadProcessorsCellIDs();
    /**
     * @brief Lee los campos del procesador i
     * @param i
     */
    void ReadVisualCFDProcessorI(int i);
    /**
     * @brief Reduce each processor cell array to a single array.
     */
    void reduceCellIDs();
    /**
     * @brief reduce each processor fields to a single field
     */
    void reduceFields();
    /**
     * @brief Pointer to grid, used to compare number of points and number tuples in each field
     */
    vtkUnstructuredGrid* Grid;
    /**
     * @brief Case_path
     */
    std::string Case_path;
    /**
     * @brief List of fields to be passed to the case
     */
    std::vector<Field*> new_Field_list;

    // Variables auxiliares para guardar.
    std::vector<unsigned long> reducedIDs;
    std::vector<std::vector<unsigned long>> cell_id_per_processor;
    //Variables para leer
    //Por procesador
    std::vector<std::vector<std::vector<std::vector<double>>>> Fields_per_processor;
    std::vector<std::vector<std::string>> Fields_names_per_processor;
    //Reducidos
    std::vector<std::vector<std::vector<double>>> Fieldss;
    std::vector<std::string> Namess;
signals:
    /**
     * @brief Signal for sending the new fields.
     * @param new_Field_list
     */
    void resultReady(std::vector<Field*> new_Field_list);
};

class OpenFOAMRunTime : public Geometry
{
    Q_OBJECT
public:
    //Constructors
    OpenFOAMRunTime();
    /**
     * @brief Case constructor
     * @param CasePath
     * @param renderWin
     */
    OpenFOAMRunTime(QString CasePath,vtkSmartPointer<vtkRenderWindow> renderWin);
    //Destructors
    ~OpenFOAMRunTime();
    /**
     * @brief Time between checks
     */
    unsigned long RefreshTime; //in miliSeconds
    /**
     * @brief Current time of the simulation
     */
    double CurrentTime;
    /**
     * @brief Clock que demanda el readerThread
     */
    QTimer *VisualTimer;
    /**
     * @brief UpdateFlag
     */
    bool UpdateFlag;
    //Functions
    /**
     * @brief ReadCase
     * @param CasePath
     */
    void ReadCase(QString CasePath);
    /**
     * @brief Case_path
     */
    std::string Case_path;
    /**
     * @brief override to do nothing. It only handle one time step
     * @param i
     */
    void ChangeToThisTime(int i) override;
    /**
     * @brief copy output in gridData
     * @param GridData
     */
    void GetOutput(vtkSmartPointer<vtkUnstructuredGrid> GridData) override;
    /**
     * @brief GetGeoDataOutput
     * @return vtkDataSet with output
     */
    vtkDataSet* GetGeoDataOutput() override;

    QCheckBox *Update_CheckBox;

private:
    //Interfase variables
    QGroupBox *Update_GroupBox;
    QGridLayout *Update_Layout;
    QLabel *Update_RefreshTime_label;
    QLineEdit *Update_RefreshTime_LineEdit;

    //Transport Properties
    QGroupBox *TransportProperties_GroupBox;
    QGridLayout *TransportProperties_Layout;
    std::vector<QLabel *> TransProperties_Label;
    std::vector<std::string> Unidades;
    std::vector<QLineEdit *> TransProperties_LineEdit;
    /**
     * @brief Class implemented for reading without using the main thread
     */
    ReaderThread *ParallelReader;
    //Interfase functions
    /**
     * @brief Config properties interfase
     */
    void PropertiesConfig(); //Add properties to Geometries properties.
    /**
     * @brief ReadGeometry
     */
    void ReadGeometry();
private slots:
    /**
     * @brief Function call by the clock to check if there is a new time step and start readerthread in case there is one
     */
    void UpdateRead();
    /**
     * @brief disconnect or connect Timer With UpdateRead
     */
    void AutoUpdate();
    /**
     * @brief ChangeRefreshTime
     */
    void ChangeRefreshTime();
    /**
     * @brief Load the new fields to the grid
     * @param new_field_list
     */
    void ChangeFields(std::vector<Field*> new_field_list);
    /**
     * @brief Write gpsteer file with the new values.
     */
    void WriteTransportProperties();
    /**
     * @brief Restart the clock is the updateFlag is on.
     */
    void VisualTimerRestart();
public slots:
    /**
     * @brief Slot for changing the checkbox value after the play button on the interace is pressed/realesed
     * @param value
     */
    void ChangeUpdate_CheckBox(bool value);

signals:
    /**
     * @brief signal por pressing/releaseing the play button in the interface
     * @param True = press. False = realese
     */
    void PlayAndStop(bool a);

};


#endif // OPENFOAMRUNTIME_H

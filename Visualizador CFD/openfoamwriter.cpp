#include "openfoamwriter.h"

OpenFOAMWriter::OpenFOAMWriter()
{
    reader = vtkSmartPointer<vtkOpenFOAMReader>::New();
    qRegisterMetaType<QList<QString>>();

}
OpenFOAMWriter::~OpenFOAMWriter()
{

}
void OpenFOAMWriter::Write()
{
    emit fileNumber(0);
    emit textToBar("Opening case folder");

    std::string path = file_name.toStdString() + "/";
    this->reader->SetFileName(path.c_str());
    this->reader->Update();

    vtkSmartPointer<vtkDoubleArray> TIMES = vtkSmartPointer<vtkDoubleArray>(reader->GetTimeValues());
    TIMES->SetNumberOfComponents(1);
    TIMES->SetName("TIME");

    emit numberOfFiles(TIMES->GetNumberOfValues()+1);


    emit textToBar("Reading Geometry");
    vtkCompositeDataSet *input =
            dynamic_cast<vtkCompositeDataSet *>(reader->GetOutput());

    auto iter = vtkSmartPointer<vtkDataObjectTreeIterator>::New();
    iter->SetDataSet(input);
    iter->SkipEmptyNodesOn();
    iter->VisitOnlyLeavesOn();

    vtkDataObject *dso = iter->GetCurrentDataObject();
    vtkUnstructuredGrid *grid = dynamic_cast<vtkUnstructuredGrid *>(dso);
    //Leo los campos dentro de cada directorio



    std::vector<vtkSmartPointer<vtkDenseArray<double>>> fields;

    int i;
    QList<QString> Names;

    emit fileNumber(1);
    for(i=0;i<grid->GetPointData()->GetNumberOfArrays();i++)
    {
        Names.push_back(grid->GetPointData()->GetArrayName(i));
        vtkSmartPointer<vtkDenseArray<double>> aux = vtkSmartPointer<vtkDenseArray<double>>::New();
        aux->SetName(grid->GetPointData()->GetArrayName(i));
        aux->Resize(TIMES->GetNumberOfValues(),grid->GetNumberOfPoints(),grid->GetPointData()->GetArray(i)->GetNumberOfComponents());
        fields.push_back(aux);
    }
    int j,k,l;
    for (i = 0; i < TIMES->GetNumberOfValues(); i++)
    {
        reader->UpdateTimeStep(TIMES->GetValue(i));
        reader->Update();

        std::string a = "Reading Times folders. Folder : "+ std::to_string(i) +"/" + std::to_string(TIMES->GetNumberOfValues());
        emit fileNumber(1+i);
        emit textToBar(QString::fromStdString(a));


        vtkCompositeDataSet *in =
                dynamic_cast<vtkCompositeDataSet *>(reader->GetOutput());

        auto it = vtkSmartPointer<vtkDataObjectTreeIterator>::New();
        it->SetDataSet(in);
        it->SkipEmptyNodesOn();
        it->VisitOnlyLeavesOn();




        vtkDataObject *ds = it->GetCurrentDataObject();
        vtkUnstructuredGrid *pd = dynamic_cast<vtkUnstructuredGrid *>(ds);

        int c;
        for (j = 0; j < pd->GetPointData()->GetNumberOfArrays(); j++)
        {
            c=0;

            while((c)< fields.size() && (pd->GetPointData()->GetArrayName(j)!= fields[c]->GetName()))
            {
                c++;
            }
            if(c==fields.size())
            {
                continue;
            }

            for (l = 0; l < pd->GetNumberOfPoints(); l++)
            {
                double *a = pd->GetPointData()->GetArray(j)->GetTuple(l);

                for (k = 0; k < pd->GetPointData()->GetArray(j)->GetNumberOfComponents(); k++)
                {
                    fields[c]->SetValue(i,l,k,a[k]);
                }
            }
        }
    }

    //Escribo cada campo y la geometria
    emit textToBar("Writing files");

    vtkSmartPointer<vtkArrayDataWriter> ArrayWriter = vtkSmartPointer<vtkArrayDataWriter>::New();
    vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();

    std::string PathToWrite = WriteIn.toStdString() + "/Output/";
    if(!dirExist(PathToWrite))
    {
        mkdir(PathToWrite.c_str(),0777);
    }

    for(i=0;i<fields.size();i++)
    {
        vtkSmartPointer<vtkArrayData> field_array =
                vtkSmartPointer<vtkArrayData>::New();
        field_array->AddArray(fields[i]);
        std::string out = PathToWrite + fields[i]->GetName();

        ArrayWriter->SetInputData(field_array);
        ArrayWriter->SetFileName(out.c_str());
        ArrayWriter->Write();
    }
    vtkSmartPointer<vtkUnstructuredGrid> geometry = vtkSmartPointer<vtkUnstructuredGrid>::New();
    geometry->CopyStructure(grid);
    geometry->GetFieldData()->AddArray(TIMES);

    std::string vtu = PathToWrite+ "geometry.vtu";
    writer->SetFileName(vtu.c_str());
    writer->SetInputData(geometry);
    writer->Write();
    QString Case = WriteIn + "/Output/";
    emit ArrayNames(Names,Case);

}
void OpenFOAMWriter::run()
{
    Write();
}
bool OpenFOAMWriter::dirExist(std::string path)
{
    struct stat info;

    if( stat( path.c_str(), &info ) != 0 )
        return false;
    else if( info.st_mode & S_IFDIR )  // S_ISDIR() doesn't exist on my windows
        return true;
    else
        return false;
}

void OpenFOAMWriter::SetPaths(QString file_namee, QString WriteInn)
{
    this->file_name = file_namee;
    this->WriteIn = WriteInn;
}

#ifndef OPENFOAMWRITER_H
#define OPENFOAMWRITER_H

#include <QThread>

#include <sys/types.h>
#include <sys/stat.h>

#include <sstream>
#include <iostream>
#include <zlib.h>
#include <stdio.h>
#include <iostream>
#include <boost/iostreams/device/file.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <fstream>
#define BOOST_NO_CXX11_SCOPED_ENUMS
#include <boost/filesystem.hpp>
#undef BOOST_NO_CXX11_SCOPED_ENUMS

#include <vtkOpenFOAMReader.h>

#include <vtkArrayData.h>
#include <vtkArray.h>
#include <vtkArrayDataWriter.h>
#include <vtkDenseArray.h>
#include <vtkArrayWriter.h>
#include <vtkDoubleArray.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkDataArray.h>
#include <vtkCollection.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkDataObject.h>
#include <vtkCompositeDataSet.h>
#include <vtkDataObjectTreeIterator.h>
#include <vtkPolyData.h>
#include <vtkDataObjectTree.h>
#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkTetra.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCellType.h>

namespace fs = boost::filesystem;
namespace io = boost::iostreams;

// Clase implemented for reading an OpenFOAM case and save the data in the used format.
// Se hace con un thread en paralelo
class OpenFOAMWriter : public QThread
{
    Q_OBJECT
public:
    //Constructor
    OpenFOAMWriter();
    //Destructor
    ~OpenFOAMWriter();

    /**
     * @brief vtkClass for reading and openFOAM case.
     */
    vtkSmartPointer<vtkOpenFOAMReader> reader;
    /**
     * @brief Paralel lopp to do
     */
    void run() override;
    /**
     * @brief Read the data from "file_name" and write it in "WriteIn"
     */
    void Write();
    /**
     * @brief Check is the directory exist.
     * @param path to directory
     * @return true or false
     */
    bool dirExist(std::string path);
    /**
     * @brief Configure the case path and the write path.
     * @param file_namee == case path
     * @param WriteInn == where to write the output
     */
    void SetPaths(QString file_namee,QString WriteInn);

    QString file_name;
    QString WriteIn;

signals:
    /**
     * @brief Slot to update the text in the load bar
     * @param text
     */
    void textToBar(QString text);
    /**
     * @brief pass the current file number to the load bar
     * @param file_number
     */
    void fileNumber(int file);
    /**
     * @brief pass the number of files to load to the load bar
     * @param total number of files to read
     */
    void numberOfFiles(int files);
    /**
     * @brief Pass the array names and the path to the folter to the main Windows. It is use to load the case after reading.
     * @param List with the names of the arrays to load
     * @param Case == path to "where to write" == WriteIn +"/Output/"
     */
    void ArrayNames(QList<QString> names,QString Case);

};

#endif // OPENFOAMWRITER_H

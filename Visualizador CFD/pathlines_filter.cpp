#include "pathlines_filter.h"
#include <QValidator>

PathLines_filter::PathLines_filter()
{
    Line = vtkSmartPointer<vtkPolyData>::New();
    SeedsWidget = new SeedsWidgets;
    UpdateConect = vtkSmartPointer<vtkEventQtSlotConnect>::New();
    Tubes = vtkSmartPointer<vtkTubeFilter>::New();
    InputHandler = new Input_handler(1);
    type = PathLines;
    FilterPropertiesConfig();
    callback = new GeoCallback;
}
PathLines_filter::~PathLines_filter()
{
    mute.lock();
    unsigned long i;
    for( i= 0 ; i< Input->NextFilter.size();i++)
    {
        if(Input->NextFilter[i]->Name == this->Name)
        {
            break;
        }
    }
    Input->NextFilter.erase(Input->NextFilter.begin()+i);
    delete Times_box;
    delete SeedsWidget;
    delete InputHandler;
    mute.unlock();
}

void PathLines_filter::SetInputData(Geometry *Source)
{
    mute.lock();
    this->Times->ShallowCopy(Source->Times);
    Input = Source;
    render_Win = Input->render_Win;
    this->Name = Source->Name + "/PathLines";
    this->SelectedField = Source->SelectedField;
    std::string prev_field_name = Source->Field_list[Source->SelectedField]->Field_Name;
    this->CurrentTimeStep = Source->CurrentTimeStep;

    InputHandler->AddAllFields(Source->Field_list,Vector);
    InputHandler->SetInputName(0,"Velocity");

    InputHandler->AddItemsToComboBox(0,Vector);
    InputHandler->SetInputTimeStep(Source->CurrentTimeStep);
    InputHandler->SetSelectedInput(prev_field_name);

    Input->GetOutput(Grid);

    SeedsWidget->SetInteractor(this->render_Win->GetInteractor());

    SeedsWidget->WidgetConfig(Grid->GetPoints(),Grid->GetBounds(),Grid->GetLength(),Grid->GetCenter());

    maxTimeOfIntegration = this->Grid->GetFieldData()->GetArray(0)->GetVariantValue(this->Grid->GetFieldData()->GetArray(0)->GetNumberOfValues()-1).ToDouble();
    maxTimeOfIntegration_LineEdit->setText(QString::fromStdString(std::to_string(maxTimeOfIntegration)));

    time_step = this->Grid->GetFieldData()->GetArray(0)->GetVariantValue(1).ToDouble()- this->Grid->GetFieldData()->GetArray(0)->GetVariantValue(0).ToDouble();
    time_step_LineEdit->setText(QString::fromStdString(std::to_string(time_step)));

    minTimeOfIntegration = this->Grid->GetFieldData()->GetArray(0)->GetVariantValue(0).ToDouble();
    minTimeOfIntegration_LineEdit->setText(QString::fromStdString(std::to_string(minTimeOfIntegration)));

    double p1[3];
    double p2[3];
    this->Grid->GetCell(1)->GetPoints()->GetPoint(0,p1);
    this->Grid->GetCell(1)->GetPoints()->GetPoint(1,p2);

    double cell_length = ((p1[0] - p2[0]) * (p1[0] - p2[0]) + (p1[1] - p2[1]) * (p1[1] - p2[1]) +
           (p1[2] - p2[2]) * (p1[2] - p2[2]));
    cell_length =sqrt(cell_length);

    //El ancho de los tubos es una parte del ancho de una celda. Puede ser necesario cambiar si la celda es muy chica.
    radio_of_tubes = cell_length/4;

    Tube_radio_LineEdit->setText(QString::fromStdString(std::to_string(radio_of_tubes)));
    number_of_sides = 10; //default
    Tube_sides_LineEdit->setText(QString::fromStdString(std::to_string(number_of_sides)));

    //SomeConections
    QLineEdit::connect(Tube_radio_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdateTubeRadio()));
    QLineEdit::connect(Tube_sides_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdateTubeNumberOfSides()));

    QLineEdit::connect(time_step_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdateTimeStep()));
    QLineEdit::connect(maxTimeOfIntegration_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdateMaxTime()));
    QLineEdit::connect(minTimeOfIntegration_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdateMinTime()));

    callback->Geo = this;

    SeedsWidget->SphereWidget->AddObserver(vtkCommand::EndInteractionEvent,callback);
    SeedsWidget->Line_Widget->AddObserver(vtkCommand::EndInteractionEvent,callback);
    SeedsWidget->MaskPoints->AddObserver(vtkCommand::EndEvent,callback);
    QComboBox::connect(InputHandler->ComboBoxes[0],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputField(QString)));

    QObject::connect(SeedsWidget,&SeedsWidgets::UpdateFilter,this,&PathLines_filter::Update);
    PathLine_calc(this->Grid,InputHandler->GetSelectedField()->Solution);
    Tubes->SetInputData(Line);
    Tubes->SetRadius(radio_of_tubes);
    Tubes->SetNumberOfSides(number_of_sides);
    Tubes->Update();
    Mapper->SetInputConnection(Tubes->GetOutputPort());
    Actor->SetMapper(Mapper);
    Input->NextFilter.push_back(this);

    mute.unlock();

}


void PathLines_filter::ChangeField(std::string Field_name)
{
    InputHandler->SetSelectedInput(Field_name);
    Update();
}

void PathLines_filter::PathLine_calc(vtkSmartPointer<vtkUnstructuredGrid> grid, std::vector<vtkSmartPointer<vtkDoubleArray>> velocity_field)
{
    seeds = SeedsWidget->Seeds;
    double time;
    int i=1;
    Line = vtkSmartPointer<vtkPolyData>::New(); //Borro la informacion anterior.  Como es un smartPointer lo anterior se elimina solo
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    vtkSmartPointer<vtkCellArray> lines_aux = vtkSmartPointer<vtkCellArray>::New();
    double number_of_time_steps = grid->GetFieldData()->GetArray(0)->GetNumberOfValues();
    double t_final = maxTimeOfIntegration;
    if (maxTimeOfIntegration > grid->GetFieldData()->GetArray(0)->GetVariantValue(number_of_time_steps-1).ToDouble())
    {
        t_final =grid->GetFieldData()->GetArray(0)->GetVariantValue(number_of_time_steps-1).ToDouble();
    }
    double inicial_time = minTimeOfIntegration;

    double step_of_solution=0;
    while(inicial_time>= grid->GetFieldData()->GetArray(0)->GetVariantValue(i).ToDouble())
    {
      i++;
      step_of_solution++;
      if(i==number_of_time_steps)
      {
        std::cout << "Inicial time is out of the time range of the solutión" << std::endl;
        return;
      }
    }
    double inicial_time_step = step_of_solution;
    double t_of_current_time_step = grid->GetFieldData()->GetArray(0)->GetVariantValue(step_of_solution).ToDouble();
    double t_of_next_time_step = grid->GetFieldData()->GetArray(0)->GetVariantValue(step_of_solution + 1).ToDouble();
    double interpolator_factor_1, interpolator_factor_2;
    vtkIdType NextId;

    int num_of_points = 0;
    int number_of_seeds = this->seeds->GetNumberOfPoints();
    for (i = 0; i < number_of_seeds; i++)
    {
      NextId = 1;
      double *new_point = this->seeds->GetPoint(i);
      num_of_points++;
      points->InsertNextPoint(new_point); //primer punto

      for (time = inicial_time; time < t_final; time += this->time_step)
      {
        if (time >= t_of_next_time_step)
        {
          step_of_solution++;
          t_of_current_time_step = t_of_next_time_step;
          t_of_next_time_step = grid->GetFieldData()->GetArray(0)->GetVariantValue(step_of_solution + 1).ToDouble();
        }
        interpolator_factor_1 = (time - t_of_current_time_step) / (t_of_next_time_step - t_of_current_time_step);
        interpolator_factor_2 = (time + this->time_step - t_of_current_time_step) / (t_of_next_time_step - t_of_current_time_step);
        //v_actual = v_current * (1-interpolator_Factor) + v_next * interpolator_Factor
        new_point = GetNextPoint(new_point, grid, velocity_field[step_of_solution], velocity_field[step_of_solution + 1], interpolator_factor_1, interpolator_factor_2,grid->GetCell(NextId),NextId,&NextId);

        if (new_point == NULL)
        {
          break;
        }
        points->InsertNextPoint(new_point);
        num_of_points++;
        vtkSmartPointer<vtkLine> line_aux = vtkSmartPointer<vtkLine>::New();
        line_aux->GetPointIds()->SetId(0, num_of_points -2);
        line_aux->GetPointIds()->SetId(1, num_of_points-1);
        lines_aux->InsertNextCell(line_aux);
      }
      step_of_solution=inicial_time_step;
      t_of_current_time_step = grid->GetFieldData()->GetArray(0)->GetVariantValue(step_of_solution).ToDouble();
      t_of_next_time_step = grid->GetFieldData()->GetArray(0)->GetVariantValue(step_of_solution + 1).ToDouble();
    }
    Line->SetPoints(points);
    Line->SetLines(lines_aux);

}

double *PathLines_filter::GetNextPoint(double previous_point[3], vtkSmartPointer<vtkUnstructuredGrid> grid, vtkSmartPointer<vtkDoubleArray> velocity_field_1, vtkSmartPointer<vtkDoubleArray> velocity_field_2, double interpolator_factor_1, double interpolator_factor_2,vtkSmartPointer<vtkCell> Previous_cell,vtkIdType Previous_cell_id,vtkIdType* NextId)
{
    static double point_result[3];
    double aux_point[3];
    double Bounds[6];
    grid->GetBounds(Bounds); // xmin, xmax, ymin, ymax, zmin , zmax
    int i;
    //Calculamos con Runge-Kutta de orden 2
    std::vector<double> vel_first_step = Interpolate_in_grid(previous_point, grid, velocity_field_1,Previous_cell,Previous_cell_id,NextId);
    std::vector<double> vel_second_step = Interpolate_in_grid(previous_point, grid, velocity_field_2,Previous_cell,Previous_cell_id,NextId);
    std::vector<double> Current_vel;
    if(vel_first_step.empty() || vel_second_step.empty())
    {
      return NULL;
    }
    for (i = 0; i < 3; i++)
    {
      Current_vel.push_back(vel_first_step[i] * (1 - interpolator_factor_1) + vel_second_step[i] * interpolator_factor_1);
      aux_point[i] = previous_point[i] + Current_vel[i] * this->time_step;
      if (aux_point[i] < Bounds[2 * i] || aux_point[i] > Bounds[2 * i + 1]) // Si el punto se va de la grilla se termina el calculo
      {
        return NULL;
      }
    }

    std::vector<double> vel_first_step_Aux = Interpolate_in_grid(aux_point, grid, velocity_field_1,Previous_cell,Previous_cell_id, NextId);
    std::vector<double> vel_second_step_Aux = Interpolate_in_grid(aux_point, grid, velocity_field_2,Previous_cell,Previous_cell_id, NextId);
    std::vector<double> Current_vel_Aux;
    if(vel_first_step_Aux.empty() || vel_second_step_Aux.empty())
    {
      return NULL;
    }
    for (i = 0; i < 3; i++)
    {
      Current_vel_Aux.push_back(vel_first_step_Aux[i] * (1 - interpolator_factor_2) + vel_second_step_Aux[i] * interpolator_factor_2);
      point_result[i] = previous_point[i] + (Current_vel[i] + Current_vel_Aux[i]) * this->time_step / 2;
      if (point_result[i] < Bounds[2 * i] || point_result[i] > Bounds[2 * i + 1]) // Si el punto se va de la grilla se termina el calculo
      {
        return NULL;
      }
    }
    return point_result;

}

std::vector<double> PathLines_filter::Interpolate_in_grid(double point[3],vtkSmartPointer<vtkUnstructuredGrid> grid,vtkSmartPointer<vtkDoubleArray> field, vtkSmartPointer<vtkCell> cell, vtkIdType id, vtkIdType* NextId)
{
    int num_of_components = field->GetNumberOfComponents();
    int num_of_points_in_cell = grid->GetCell(1)->GetNumberOfPoints();
    //Necesarios para usar la funcion Evaluate
    int subid;
    double tol2=0.001;
    double par_coords[3];
    double weight[num_of_points_in_cell];

    std::vector<double> result;

    vtkIdType Cell_ID = grid->FindCell(point, cell, id, tol2, subid, par_coords, weight);
    if(Cell_ID <0)
    {
      return result; // Lo devuelve vacio
    }
    NextId = &Cell_ID;
    vtkIdList *ids = grid->GetCell(Cell_ID)->GetPointIds();

    int i, j;
    for (i = 0; i < num_of_components; i++)
    {
      result.push_back(0);
    }
    double auxiliar[num_of_components];
    for (i = 0; i < num_of_points_in_cell; i++)
    {
      field->GetTuple(ids->GetId(i), auxiliar);
      for (j = 0; j < num_of_components; j++)
      {
        result[j] += auxiliar[j] * weight[i];
      }
    }
    return result;
}

void PathLines_filter::FilterPropertiesConfig()
{

    //Variables sobre el tiempo del filtro
    Times_box = new QGroupBox;
    Times_box->setTitle("Time variables");
    Times_box->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    Times_layout = new QGridLayout(Times_box);
    time_step_label = new QLabel(Times_box);
    time_step_label->setText("Time Step");
    time_step_LineEdit = new QLineEdit(Times_box);
    time_step_LineEdit->setValidator(new QDoubleValidator(0,1000000,5,Times_box));
    maxTimeOfIntegration_label = new QLabel(Times_box);
    maxTimeOfIntegration_label->setText("Tiempo final");
    maxTimeOfIntegration_LineEdit = new QLineEdit(Times_box);
    maxTimeOfIntegration_LineEdit->setValidator(new QDoubleValidator(0,1000000,5,Times_box));
    minTimeOfIntegration_label = new QLabel(Times_box);
    minTimeOfIntegration_label->setText("Tiempo inicial");
    minTimeOfIntegration_LineEdit = new QLineEdit(Times_box);
    minTimeOfIntegration_LineEdit->setValidator(new QDoubleValidator(0,1000000,5,Times_box));

    Times_layout->addWidget(time_step_label,0,0);
    Times_layout->addWidget(time_step_LineEdit,0,1);
    Times_layout->addWidget(minTimeOfIntegration_label,1,0);
    Times_layout->addWidget(minTimeOfIntegration_LineEdit,1,1);
    Times_layout->addWidget(maxTimeOfIntegration_label,2,0);
    Times_layout->addWidget(maxTimeOfIntegration_LineEdit,2,1);
    Times_box->setLayout(Times_layout); //NOTA: las conneciones se hacen en la funcion inputdata.

    TubesBox = new QGroupBox;
    TubesBox->setTitle("Tubes");
    TubesBox->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    TubesBox->setMinimumHeight(100);

    TubesLayout = new QGridLayout(TubesBox);

    QLabel *Tube_radio_label = new QLabel(TubesBox);
    Tube_radio_LineEdit = new QLineEdit(TubesBox);
    Tube_radio_LineEdit->setValidator(new QDoubleValidator(0.00000000001,100000000000,8,TubesBox));
    Tube_radio_label->setText("Radio");

    Tube_sides_LineEdit = new QLineEdit(TubesBox);
    QLabel *Tube_sides_label = new QLabel(TubesBox);
    Tube_sides_LineEdit->setValidator(new QIntValidator(1,1000000,TubesBox));
    Tube_sides_label->setText("Number of sides");

    TubesLayout->addWidget(Tube_radio_label,0,0);
    TubesLayout->addWidget(Tube_radio_LineEdit,0,1);
    TubesLayout->addWidget(Tube_sides_label,1,0);
    TubesLayout->addWidget(Tube_sides_LineEdit,1,1);

    TubesBox->setLayout(TubesLayout);
    TubesBox->setMaximumHeight(100);
    //Add all
    QVBoxLayout *FilterProp = new QVBoxLayout;
    InputHandler->AddQBoxToLayout(FilterProp);
    FilterProp->addWidget(Times_box);
    SeedsWidget->AddPropertiesBoxToLayout(FilterProp);
    FilterProp->addWidget(TubesBox);
    AddFilterProperties(FilterProp);
    Properties_names.push_back("Filter prop.");
}

//UPDATE FUNCTIONS-------------------------------------------------------
//Actualiza el radio de los tubos al que el usuario solicite.
void PathLines_filter::UpdateTubeRadio()
{
    if(Tube_radio_LineEdit->text().isEmpty())
        return;
    radio_of_tubes = std::stod(Tube_radio_LineEdit->text().toStdString());
    Tubes->SetRadius(radio_of_tubes);
    Tubes->Update();
}
//Actualiza el numero de lado de los tubos.
void PathLines_filter::UpdateTubeNumberOfSides()
{
    if(Tube_sides_LineEdit->text().isEmpty())
        return;
    number_of_sides = std::stoi(Tube_sides_LineEdit->text().toStdString());
    Tubes->SetNumberOfSides(number_of_sides);
    Tubes->Update();
}


void PathLines_filter::UpdateTimeStep()
{
    if(time_step_LineEdit->text().isEmpty())
        return;
    time_step = std::stod(time_step_LineEdit->text().toStdString());
    if(time_step==0)
        return;
    Update();
}
void PathLines_filter::UpdateMaxTime()
{
    if(maxTimeOfIntegration_LineEdit->text().isEmpty())
        return;
    maxTimeOfIntegration = std::stod(maxTimeOfIntegration_LineEdit->text().toStdString());
    Update();

}
void PathLines_filter::UpdateMinTime()
{
    if(minTimeOfIntegration_LineEdit->text().isEmpty())
        return;
    minTimeOfIntegration = std::stod(minTimeOfIntegration_LineEdit->text().toStdString());
    Update();
}

void PathLines_filter::HideOrShow(int i)
{
    if(i==0)
    {
        Actor->VisibilityOff();
    }
    else
    {
        Actor->VisibilityOn();
    }
    SeedsWidget->HideOrShow(i);
}
void PathLines_filter::Calculate()
{
    PathLine_calc(this->Grid,InputHandler->GetSelectedField()->Solution);
    Tubes->SetInputData(Line);
    Tubes->Update();
    Mapper->Update();
}

void PathLines_filter::HideWidgets()
{
    SeedsWidget->HideOrShow(0);
}

void PathLines_filter::ShowWidgets()
{
    if(Actor->GetVisibility())
        SeedsWidget->HideOrShow(1);
}

vtkDataSet* PathLines_filter::GetGeoDataOutput()
{
    mute.lock();
    Tubes->Update();
    mute.unlock();
    return Tubes->GetOutput();
}

void PathLines_filter::ChangeToThisTime(int i) //No hace nada intencionalmente
{
    Input->ChangeToThisTime(i);
}

void PathLines_filter::UpstreamChange()
{
    mute.lock();
    SeedsWidget->DisconectAll();
    CurrentTimeStep=Input->CurrentTimeStep;

    std::string prev_velocity_name = InputHandler->GetSelectedFieldName(0);

    InputHandler->RemoveItemsFromComboBox();
    InputHandler->UpdateInputHandler(Input->Field_list);
    InputHandler->AddItemsToComboBox(0,Vector);

    if(!InputHandler->SetSelectedInput(0,prev_velocity_name))
    {
        InputHandler->SetSelectedInput(0,InputHandler->GetVectorsFieldsNames()[0]);
        InputHandler->SetComboBoxIndex(0,InputHandler->GetVectorsFieldsNames()[0]);
    }
    else
    {
        InputHandler->SetComboBoxIndex(0,prev_velocity_name);
    }
    SeedsWidget->Reconect();
    mute.unlock();

    Update();
}

void PathLines_filter::Update()
{
    mute.lock();
    SeedsWidget->DisconectAll();
    Calculate();
    SendSignalsDownStream();
    SeedsWidget->Reconect();
    mute.unlock();
}
void PathLines_filter::ChangeInputField(QString name)
{
    ChangeField(name.toStdString());
    render_Win->Render();
}

void PathLines_filter::ReScaleToCurrentData()
{
    //NothingToDoHere. Override to avoid crash
}

void PathLines_filter::GetOutput(vtkSmartPointer<vtkUnstructuredGrid> GridData)
{
    mute.lock();
    vtkSmartPointer<vtkAppendFilter> toUnstrGrid = vtkSmartPointer<vtkAppendFilter>::New();
    toUnstrGrid->SetInputConnection(Tubes->GetOutputPort());
    toUnstrGrid->Update();
    GridData->ShallowCopy(toUnstrGrid->GetOutput());
    mute.unlock();
}



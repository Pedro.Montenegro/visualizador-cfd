#ifndef PATHLINES_FILTER_H
#define PATHLINES_FILTER_H

#include "geometry.h"
#include "seedswidgets.h"
#include "input_handler.h"

#include <vtkMath.h>
#include <vtkLine.h>
#include <vtkCommand.h>
#include <vtkEventQtSlotConnect.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderWindow.h>
#include <vtkTubeFilter.h>

#include <QRadioButton>
#include <QFormLayout>
#include <QCheckBox>

class PathLines_filter : public Geometry
{
    Q_OBJECT
public:
    //Consctructors
    PathLines_filter();
    //Destructor
    ~PathLines_filter();
    //Necessary functions for the filter.
    /**
     * @brief Sirve como input para la clase
     * @param Geometria input
     */
    void SetInputData(Geometry *Source);
    /**
     * @brief Cambia el campo sobre el cual se calculan las lineas de trayectoria
     * @param field_name
     */
    void ChangeField(std::string Field_name) override;
    /**
     * @brief Esconde o muestra la geometria y los widgets
     * @param i = 1 muestra, i =0 oculta
     */
    void HideOrShow(int i) override;
    /**
     * @brief Esconde los widgets
     */
    void HideWidgets() override;
    /**
     * @brief Muestra los widgets
     */
    void ShowWidgets() override;
    /**
     * @brief Cambia el arbol de geometrias a este paso de tiempo
     * @param i = paso de tiempo
     */
    void ChangeToThisTime(int i) override;
    /**
     * @brief GetGeoDataOutput
     * @return pathlines polydata
     */
    virtual vtkDataSet* GetGeoDataOutput() override;
    /**
     * @brief GetOutput
     * @param return the output as a vtkUnstructuredGrid
     */
    virtual void GetOutput(vtkSmartPointer<vtkUnstructuredGrid> GridData);
    /**
     * @brief Re escalea la solución al campo actual.
     */
    void ReScaleToCurrentData() override;
    /**
     * @brief UpstreamChange. Se usa en caso ed que el input del filtro cambie. Usualmente se llama mediante "SendSignalsDownStream"
     */
    void UpstreamChange() override;
    /**
     * @brief Actualiza el filtro a nivel objeto. Por ejemplo, cuando el plano cambia de lugar.
     */
    void Update() override;
    /**
     * @brief Slot Use for calculate the pathlines.
     */
    void Calculate();
    /**
     * @brief SeedsWidget. Class used to generate the seeds.
     */
    SeedsWidgets *SeedsWidget;
private:
    /**
     * @brief Configure the filter properties
     */
    void FilterPropertiesConfig();
    /**
     * @brief Calcula las lineas de corriente
     * @param Input grid for the calculation
     * @param Input field for the calc.
     */
    void PathLine_calc(vtkSmartPointer<vtkUnstructuredGrid> grid, std::vector<vtkSmartPointer<vtkDoubleArray>> velocity_field);
    /**
     * @brief Interal function of "PathLine_calc"
     * @param previous_point == Start Point
     * @param grid
     * @param velocity_field_1 == Velocity_field at step_time i
     * @param velocity_field_2 == Velocity_field at step_time i+1
     * @param interpolator_factor_1 == first temporal interpolation factor (There are two because is a Runge-Kutta 2 method)
     * @param interpolator_factor_2 == second temporal interpolation factor
     * @param Previous_cell. Used to acelerate the search speed
     * @param Previous_cell_id. Used to acelerate the search speed
     * @param NextId. Save the ID where the point was found.
     * @return double *p. coordenates of the new point.
     */
    double *GetNextPoint(double previous_point[3], vtkSmartPointer<vtkUnstructuredGrid> grid, vtkSmartPointer<vtkDoubleArray> velocity_field_1, vtkSmartPointer<vtkDoubleArray> velocity_field_2, double interpolator_factor_1, double interpolator_factor_2,vtkSmartPointer<vtkCell> Previous_cell,vtkIdType Previous_cell_id,vtkIdType* NextId);
    /**
     * @brief Function that interpolate the velocity field in a given point
     * @param point
     * @param grid
     * @param field
     * @param cell . used to acelerate the search
     * @param id. used to acelerate the search.
     * @param NextId. Save the ID where the point was found.
     * @return values of the field at the given point.
     */
    std::vector<double> Interpolate_in_grid(double point[3],vtkSmartPointer<vtkUnstructuredGrid> grid,vtkSmartPointer<vtkDoubleArray> field, vtkSmartPointer<vtkCell> cell, vtkIdType id, vtkIdType* NextId);
    /**
     * @brief Clase para manejar los distintos inputs. Controla inputs vectoriales en este caso
     */
    Input_handler *InputHandler;
    /**
     * @brief vtkPolyData used for saving the pathlines
     */
    vtkSmartPointer<vtkPolyData> Line;
    /**
     * @brief Contenedor para las semillas.
     */
    vtkSmartPointer<vtkPolyData> seeds;
    /**
     * @brief Class used to connect vtkObject with QtObjects
     */
    vtkSmartPointer<vtkEventQtSlotConnect> UpdateConect;
    /**
     * @brief VtkTubeFilter. Use to improve visualization.
     */
    vtkSmartPointer<vtkTubeFilter> Tubes;


    //Variables sobre el tiempo del filtro
    QGroupBox *Times_box;
    QGridLayout *Times_layout;
    double time_step;
    QLabel *time_step_label;
    QLineEdit *time_step_LineEdit;
    double minTimeOfIntegration;
    QLabel *minTimeOfIntegration_label;
    QLineEdit *minTimeOfIntegration_LineEdit;
    double maxTimeOfIntegration;
    QLabel *maxTimeOfIntegration_label;
    QLineEdit *maxTimeOfIntegration_LineEdit;

    //Variables para el filtro tubes.
    QGroupBox *TubesBox;
    QGridLayout *TubesLayout;
    double radio_of_tubes; // Radio de los tubos
    QLineEdit *Tube_radio_LineEdit;
    double number_of_sides; // Numbero de lados == resolucion de los tubos.
    QLineEdit *Tube_sides_LineEdit;
private slots:
    //General slots
    /**
     * @brief Slot for UpdateTimeStep from the QLineEdit
     */
    void UpdateTimeStep();
    /**
     * @brief Slot for UpdateMaxTime from the QLineEdit
     */
    void UpdateMaxTime();
    /**
     * @brief Slot for UpdateMixTime from the QLineEdit
     */
    void UpdateMinTime();
    /**
     * @brief Slot for UpdateTubeRadio from the QLineEdit
     */
    void UpdateTubeRadio();
    /**
     * @brief Slot for update the tube number of sides.
     */
    void UpdateTubeNumberOfSides();
    /**
     * @brief Slot for changing input field
     * @param name of the field
     */
    void ChangeInputField(QString name);
};

#endif // PATHLINES_FILTER_H

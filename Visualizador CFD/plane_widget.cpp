#include "plane_widget.h"

Plane_Widget::Plane_Widget()
{
    plane_function = vtkSmartPointer<vtkPlane>::New();
    plane_callback = vtkSmartPointer<vtkPlaneCallback>::New();
    plane_representation = vtkSmartPointer<vtkImplicitPlaneRepresentation>::New();
    plane_Widget = vtkSmartPointer<vtkImplicitPlaneWidget2>::New();
    Update_connector = vtkSmartPointer<vtkEventQtSlotConnect>::New();
    WidgetPropConfig();
}
Plane_Widget::~Plane_Widget()
{
    delete Plane_box_normals;

    delete Plane_box_origin;

    delete options;

    plane_function->RemoveAllObservers();

    plane_Widget->Off();
    plane_Widget->RemoveAllObservers();

    plane_representation->RemoveAllObservers();

    Update_connector->RemoveAllObservers();

}
void Plane_Widget::WidgetPropConfig()
{

    Plane_box_normals = new QGroupBox;
    Plane_box_normals->setTitle("Plane normal components");
    Plane_box_normals->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");

    Plane_layout_normals = new QGridLayout(Plane_box_normals);
    Plane_box_normals->setLayout(Plane_layout_normals);

    Plane_x_label = new QLabel(Plane_box_normals);
    Plane_x_label->setText("x");
    Plane_x_LineEdit = new QLineEdit(Plane_box_normals);
    Plane_x_LineEdit->setValidator(new QDoubleValidator(Plane_x_LineEdit));

    Plane_y_label = new QLabel(Plane_box_normals);
    Plane_y_label->setText("y");
    Plane_y_LineEdit = new QLineEdit(Plane_box_normals);
    Plane_y_LineEdit->setValidator(new QDoubleValidator(Plane_y_LineEdit));

    Plane_z_label = new QLabel(Plane_box_normals);
    Plane_z_label->setText("z");
    Plane_z_LineEdit = new QLineEdit(Plane_box_normals);
    Plane_z_LineEdit->setValidator(new QDoubleValidator(Plane_z_LineEdit));

    Plane_box_origin = new QGroupBox;
    Plane_layout_origin = new QGridLayout(Plane_box_origin);
    Plane_box_origin->setTitle("Plane origin components");
    Plane_box_origin->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    Plane_box_origin->setLayout(Plane_layout_origin);

    Origin_x_label = new QLabel(Plane_box_origin);
    Origin_x_label->setText("x");
    Origin_x_LineEdit = new QLineEdit(Plane_box_origin);
    Origin_x_LineEdit->setValidator(new QDoubleValidator(-100000,100000,4,Plane_x_LineEdit));

    Origin_y_label = new QLabel(Plane_box_origin);
    Origin_y_label->setText("y");
    Origin_y_LineEdit = new QLineEdit(Plane_box_origin);
    Origin_y_LineEdit->setValidator(new QDoubleValidator(-100000,100000,4,Plane_y_LineEdit));

    Origin_z_label = new QLabel(Plane_box_origin);
    Origin_z_label->setText("z");
    Origin_z_LineEdit = new QLineEdit(Plane_box_origin);
    Origin_z_LineEdit->setValidator(new QDoubleValidator(-100000,100000,4,Plane_z_LineEdit));

    Plane_layout_normals->addWidget(Plane_x_label,0,0);
    Plane_layout_normals->addWidget(Plane_x_LineEdit,0,1);
    Plane_layout_normals->addWidget(Plane_y_label,0,2);
    Plane_layout_normals->addWidget(Plane_y_LineEdit,0,3);
    Plane_layout_normals->addWidget(Plane_z_label,0,4);
    Plane_layout_normals->addWidget(Plane_z_LineEdit,0,5);

    Plane_layout_origin->addWidget(Origin_x_label,0,0);
    Plane_layout_origin->addWidget(Origin_x_LineEdit,0,1);
    Plane_layout_origin->addWidget(Origin_y_label,0,2);
    Plane_layout_origin->addWidget(Origin_y_LineEdit,0,3);
    Plane_layout_origin->addWidget(Origin_z_label,0,4);
    Plane_layout_origin->addWidget(Origin_z_LineEdit,0,5);

    //OPtions
    options = new QGroupBox;
    options->setTitle("Options");
    options->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    options_layout = new QVBoxLayout(options);
    ShowOrHidePlane_CheckBox = new QCheckBox(options);
    ShowOrHidePlane_CheckBox->setText("Show plane");
    ShowOrHidePlane_CheckBox->setChecked(true);
    options_layout->addWidget(ShowOrHidePlane_CheckBox);

    Plane_box_normals->setMaximumHeight(100);
    Plane_box_origin->setMaximumHeight(100);
    options->setMaximumHeight(100);
}

void Plane_Widget::WidgetConfig(vtkRenderWindow* ren_win, double *ActorBounds, double *center)
{
    plane_Widget->SetInteractor(ren_win->GetInteractor());

    bounds = ActorBounds;
    plane_callback->Plane = plane_function;
    plane_callback->myPlaneWidget = this;
    plane_function->SetNormal(0,1,0);
    plane_function->SetOrigin(center);
    plane_callback->is2D=0;

    plane_representation->SetPlaceFactor(1.25);
    plane_representation->PlaceWidget(ActorBounds);
    plane_representation->SetOrigin(center);
    plane_representation->SetNormal(plane_function->GetNormal());

    plane_Widget->SetRepresentation(plane_representation);
    plane_Widget->AddObserver(vtkCommand::EndInteractionEvent,plane_callback,1);
    plane_Widget->On();

    plane_function->GetNormal(plane_callback->plane_normal);
    plane_function->GetOrigin(plane_callback->plane_origin);

    Plane_x_LineEdit->setText(QString::fromStdString(std::to_string(plane_callback->plane_normal[0])));
    Plane_y_LineEdit->setText(QString::fromStdString(std::to_string(plane_callback->plane_normal[1])));
    Plane_z_LineEdit->setText(QString::fromStdString(std::to_string(plane_callback->plane_normal[2])));

    Origin_x_LineEdit->setText(QString::fromStdString(std::to_string(plane_callback->plane_origin[0])));
    Origin_y_LineEdit->setText(QString::fromStdString(std::to_string(plane_callback->plane_origin[1])));
    Origin_z_LineEdit->setText(QString::fromStdString(std::to_string(plane_callback->plane_origin[2])));

    QLineEdit::connect(Plane_x_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdatePlaneNormal()));
    QLineEdit::connect(Plane_y_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdatePlaneNormal()));
    QLineEdit::connect(Plane_z_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdatePlaneNormal()));

    QLineEdit::connect(Origin_x_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdatePlanePoint()));
    QLineEdit::connect(Origin_y_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdatePlanePoint()));
    QLineEdit::connect(Origin_z_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdatePlanePoint()));

    QCheckBox::connect(ShowOrHidePlane_CheckBox,SIGNAL(released()),this,SLOT(UpdateOptions()));
}

void Plane_Widget::UpdatePlanePoint()
{
    if(Origin_x_LineEdit->text().isEmpty() || Origin_y_LineEdit->text().isEmpty() || Origin_z_LineEdit->text().isEmpty())
        return;
    plane_callback->plane_origin[0] = std::stod(Origin_x_LineEdit->text().toStdString());
    plane_callback->plane_origin[1] = std::stod(Origin_y_LineEdit->text().toStdString());
    plane_callback->plane_origin[2] = std::stod(Origin_z_LineEdit->text().toStdString());

    plane_representation->SetOrigin(plane_callback->plane_origin);
    plane_Widget->SetRepresentation(plane_representation);
    plane_function->SetOrigin(plane_callback->plane_origin);
}

void Plane_Widget::UpdatePlaneNormal()
{
    if(Plane_x_LineEdit->text().isEmpty() || Plane_y_LineEdit->text().isEmpty() || Plane_z_LineEdit->text().isEmpty())
        return;
    //Renormalizo los valores que hay en los LineEdit
    double provisory_normal[3];
    provisory_normal[0] = std::stod(Plane_x_LineEdit->text().toStdString());
    provisory_normal[1] = std::stod(Plane_y_LineEdit->text().toStdString());
    provisory_normal[2] = std::stod(Plane_z_LineEdit->text().toStdString());
    double module = std::sqrt(provisory_normal[0]*provisory_normal[0] +
            provisory_normal[1]*provisory_normal[1] +
            provisory_normal[2]*provisory_normal[2]);
    if(module==0)
        return;
    plane_callback->plane_normal[0] = provisory_normal[0]/module;
    plane_callback->plane_normal[1] = provisory_normal[1]/module;
    plane_callback->plane_normal[2] = provisory_normal[2]/module;

    plane_representation->SetNormal(plane_callback->plane_normal);
    plane_Widget->SetRepresentation(plane_representation);
    plane_function->SetNormal(plane_callback->plane_normal);
}

void Plane_Widget::UpdatePlaneProperties()
{
    if(plane_callback->is2D==1)
    {
        if(bounds[0]==bounds[1])
        {
            plane_callback->plane_normal[0]=0;
        }
        else if(bounds[2]==bounds[3])
        {
            plane_callback->plane_normal[1]=0;
        }
        else if(bounds[4]==bounds[5])
        {
            plane_callback->plane_normal[2]=0;
        }
        double module = std::sqrt(plane_callback->plane_normal[0]*plane_callback->plane_normal[0]+
                plane_callback->plane_normal[1]*plane_callback->plane_normal[1]+
                plane_callback->plane_normal[2]*plane_callback->plane_normal[2]);
        plane_callback->plane_normal[0]=plane_callback->plane_normal[0]/module;
        plane_callback->plane_normal[1]=plane_callback->plane_normal[1]/module;
        plane_callback->plane_normal[2]=plane_callback->plane_normal[2]/module;

        plane_representation->SetNormal(plane_callback->plane_normal);
        plane_representation->GetPlane(plane_function);
    }
    Plane_x_LineEdit->setText(QString::fromStdString(std::to_string(plane_callback->plane_normal[0])));
    Plane_y_LineEdit->setText(QString::fromStdString(std::to_string(plane_callback->plane_normal[1])));
    Plane_z_LineEdit->setText(QString::fromStdString(std::to_string(plane_callback->plane_normal[2])));
    Origin_x_LineEdit->setText(QString::fromStdString(std::to_string(plane_callback->plane_origin[0])));
    Origin_y_LineEdit->setText(QString::fromStdString(std::to_string(plane_callback->plane_origin[1])));
    Origin_z_LineEdit->setText(QString::fromStdString(std::to_string(plane_callback->plane_origin[2])));

}

void Plane_Widget::UpdateOptions()
{
    if(ShowOrHidePlane_CheckBox->isChecked())
    {
        HideOrShowPlane(true);
    }
    else
    {
        HideOrShowPlane(false);
    }
}
void Plane_Widget::HideOrShowPlane(bool i)
{
    if(i)
    {
        plane_Widget->On();
    }
    else
    {
        plane_Widget->Off();
    }
}

void Plane_Widget::AddPropertiesBoxToLayout(QVBoxLayout *layout)
{
    layout->addWidget(Plane_box_normals);
    layout->addWidget(Plane_box_origin);
    layout->addWidget(options);
}

void Plane_Widget::DisconectAll()
{
    mute.lock();
    plane_Widget->RemoveObserver(plane_callback);

}

void Plane_Widget::ReconectAll()
{
    plane_Widget->AddObserver(vtkCommand::EndInteractionEvent,plane_callback,1);
    mute.unlock();
}

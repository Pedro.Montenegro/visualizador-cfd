#ifndef PLANE_WIDGET_H
#define PLANE_WIDGET_H

#include <QObject>
#include <vtkCommand.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkCutter.h>
#include <vtkImplicitPlaneWidget2.h>
#include <vtkImplicitPlaneRepresentation.h>
#include <vtkPlane.h>
#include <vtkEventQtSlotConnect.h>
#include <vtkClipDataSet.h>
#include <vtkCutter.h>
#include <vtkRenderWindow.h>

#include <QGroupBox>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QCheckBox>
#include <QDoubleValidator>
#include <QMutex>


class vtkPlaneCallback;
/**
 * @brief The Plane_Widget class.
 * Clase que crea y configura todas las variables necesarias para usar un plano en un filtro
 */
class Plane_Widget : public QObject
{
    Q_OBJECT
public:
    //Constructor
    Plane_Widget();
    //Destructor
    ~Plane_Widget();
    /**
     * @brief WidgetConfig
     * @param ren_win == render window to place the widget
     * @param ActorBounds == to limit the extend of the widget
     * @param center == to place the widget center
     */
    void WidgetConfig(vtkRenderWindow* ren_win,double *ActorBounds, double *center);
    /**
     * @brief HideOrShowPlane
     * @param i = true show, i = false hide
     */
    void HideOrShowPlane(bool i);
    /**
     * @brief Add the interface items to the input layout
     * @param layout where to add
     */
    void AddPropertiesBoxToLayout(QVBoxLayout *layout);
    /**
     * @brief Disconect all conections. This function is to avoid problems in online mode
     */
    void DisconectAll();
    /**
     * @brief ReconectAll. Call after filter update.
     */
    void ReconectAll();
    //options
    QGroupBox *options;
    QCheckBox *ShowOrHidePlane_CheckBox;
    //Variables para la interfaz
    //Plano
    QGridLayout *Plane_layout_normals;
    QGroupBox *Plane_box_normals;
    // componentes de la normal del plano
    QLabel *Plane_x_label, *Plane_y_label, *Plane_z_label;
    QLineEdit *Plane_x_LineEdit, *Plane_y_LineEdit, *Plane_z_LineEdit;
    //Punto perteneciente al plano

    QGridLayout *Plane_layout_origin;
    QGroupBox *Plane_box_origin;

    QLabel *Origin_x_label, *Origin_y_label, *Origin_z_label;
    QLineEdit *Origin_x_LineEdit, *Origin_y_LineEdit, *Origin_z_LineEdit;

    QVBoxLayout *options_layout;
    QMutex mute;
    /**
     * @brief Class implemented to manage the update of some variables.
     */
    vtkSmartPointer<vtkPlaneCallback> plane_callback;

    /**
     * @brief vtkClass for representing a plane in the renderWindow.
     */
    vtkSmartPointer<vtkImplicitPlaneRepresentation> plane_representation;
    /**
     * @brief vtkClass used to interact with a plane in the renderwindow
     */
    vtkSmartPointer<vtkImplicitPlaneWidget2> plane_Widget;
    /**
     * @brief VtkClass for representing the implicit function of a plane.
     */
    vtkSmartPointer<vtkPlane> plane_function;

private:

    /**
     * @brief Class used to make connections between Qt and VTK objects
     */
    vtkSmartPointer<vtkEventQtSlotConnect> Update_connector;
    /**
     * @brief Configure the interface of the widget
     */
    void WidgetPropConfig();
    /**
     * @brief bounds of the grid. Used to limit the widget extend
     */
    double *bounds;


private slots:
    /**
     * @brief Slot for updating the plane normal
     */
    void UpdatePlaneNormal();
    /**
     * @brief Slot for updating the plane origin
     */
    void UpdatePlanePoint();
    /**
     * @brief Slot for updating plane options
     */
    void UpdateOptions();
public slots:
    /**
     * @brief Slot for updating plane properties.
     */
    void UpdatePlaneProperties();
};

/**
 * @brief Class implemented for managing the update of the plane after and interaction event
 */
class vtkPlaneCallback : public vtkCommand
{
public:
  static vtkPlaneCallback *New()
    { return new vtkPlaneCallback; }
  virtual void Execute(vtkObject *caller, unsigned long, void*)
  {
    vtkImplicitPlaneWidget2 *planeWidget =
      reinterpret_cast<vtkImplicitPlaneWidget2*>(caller);
    vtkImplicitPlaneRepresentation *rep =
      reinterpret_cast<vtkImplicitPlaneRepresentation*>(planeWidget->GetRepresentation());
    rep->GetPlane(this->Plane);
    rep->GetNormal(this->plane_normal);
    rep->GetOrigin(this->plane_origin);
    myPlaneWidget->UpdatePlaneProperties();

  }
  vtkPlaneCallback():Plane(0) {}
  vtkPlane *Plane;
  double plane_normal[3];
  double plane_origin[3];
  Plane_Widget *myPlaneWidget;
  int is2D; //0 si es 3D , 1 si es 2D
};

#endif // PLANE_WIDGET_H

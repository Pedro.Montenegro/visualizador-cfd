#include "puntos_de_estancamiento.h"

Puntos_de_estancamiento::Puntos_de_estancamiento()
{
    Puntos = vtkSmartPointer<vtkPolyData>::New();
    grid_copy = vtkSmartPointer<vtkUnstructuredGrid>::New();
    GeometryFilter = vtkSmartPointer<vtkGeometryFilter>::New();
    OuterSurface = vtkSmartPointer<vtkPolyData>::New();
    this->type = PuntosEstancamiento;
    InputHandler = new Input_handler(1);
    Triangulator = vtkSmartPointer<vtkDataSetTriangleFilter>::New();

    FilterPropertiesConfig();

}

Puntos_de_estancamiento::~Puntos_de_estancamiento()
{
    mute.lock();
    unsigned long i;
    for( i= 0 ; i< Input->NextFilter.size();i++)
    {
        if(Input->NextFilter[i]->Name == this->Name)
        {
            break;
        }
    }
    Input->NextFilter.erase(Input->NextFilter.begin()+i);
    delete PointsProp_box;
    delete List_of_points_box;
    delete InputHandler;
    mute.unlock();

}

void Puntos_de_estancamiento::SetInputData(Geometry *Source)
{
    mute.lock();
    this->Times->ShallowCopy(Source->Times);

    Input = Source;
    render_Win = Input->render_Win;
    this->Name = Source->Name + "/PuntosDeEstancamiento";
    this->SelectedField = Source->SelectedField;
    std::string prev_field_name = Source->Field_list[Source->SelectedField]->Field_Name;
    this->CurrentTimeStep = Source->CurrentTimeStep;

    InputHandler->AddAllFields(Source->Field_list,Vector);
    InputHandler->SetInputName(0,"Velocity");

    InputHandler->AddItemsToComboBox(0,Vector);
    InputHandler->SetInputTimeStep(Source->CurrentTimeStep);
    InputHandler->SetSelectedInput(prev_field_name);

    Input->GetOutput(grid_copy);

    Triangulator->SetInputData(grid_copy);
    Triangulator->Update();
    Grid->ShallowCopy(Triangulator->GetOutput());

    GeometryFilter->SetInputData(this->Grid);
    GeometryFilter->Update();

    OuterSurface = GeometryFilter->GetOutput();

    Puntos->SetPoints(Points_with_velocity_zero());
    ExtractSurfacePoints();

    AddVerts();
    Mapper->SetInputData(Puntos);
    PointSize = 10; //Debe haber una forma mas generica de definir este valor
    PointSize_LineEdit->setText(QString::fromStdString(std::to_string(PointSize)));
    this->Actor->GetProperty()->SetPointSize(PointSize);
    Mapper->Update();
    TableConfigure();

    QLineEdit::connect(PointSize_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdatePointSize()));
    QCheckBox::connect(ShowSurfacePoints,SIGNAL(released()),this,SLOT(UpdateOnSurfaceCheck()));
    QComboBox::connect(InputHandler->ComboBoxes[0],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputField(QString)));

    Input->NextFilter.push_back(this);

    mute.unlock();

}

void Puntos_de_estancamiento::FilterPropertiesConfig()
{
    //Points properties
    PointsProp_box = new QGroupBox;
    PointsProp_box->setMaximumHeight(100);
    PointsProp_box->setTitle("Points Properties");
    PointsProp_box->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    PointsProp_layout = new QGridLayout(PointsProp_box);
    PointSize_LineEdit = new QLineEdit(PointsProp_box);
    PointSize_LineEdit->setValidator(new QDoubleValidator(0,10000,4,PointsProp_box));
    PointSize_label = new QLabel(PointsProp_box);
    PointSize_label->setText("Point size");
    ShowSurfacePoints = new QCheckBox(PointsProp_box);
    ShowSurfacePoints->setText("Show Surface points");


    PointsProp_layout->addWidget(PointSize_label,0,0);
    PointsProp_layout->addWidget(PointSize_LineEdit,0,1);
    PointsProp_layout->addWidget(ShowSurfacePoints,1,0);

    PointsProp_box->setLayout(PointsProp_layout);


    //Points list
    List_of_points_box = new QGroupBox;
    List_of_points_box->setTitle("Points List");
    List_of_points_box->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    List_of_points_layout = new QVBoxLayout(List_of_points_box);
    List_of_points_scroll = new QScrollBar(List_of_points_box);
    Points_table = new QTableWidget(List_of_points_box);
    Points_table->setVerticalScrollBar(List_of_points_scroll);
    Points_table->scrollBarWidgets(Qt::AlignmentFlag::AlignRight);
    Points_table->setColumnCount(3);
    QTableWidgetItem *header1 = new QTableWidgetItem;
    header1->setText("x");
    Points_table->setHorizontalHeaderItem(0,header1);
    QTableWidgetItem *header2 = new QTableWidgetItem;
    header2->setText("y");
    Points_table->setHorizontalHeaderItem(1,header2);
    QTableWidgetItem *header3 = new QTableWidgetItem;
    header3->setText("z");
    Points_table->setHorizontalHeaderItem(2,header3);

    List_of_points_layout->addWidget(Points_table);
    List_of_points_box->setLayout(List_of_points_layout);
    Points_table->setEditTriggers(QAbstractItemView::NoEditTriggers);

    QVBoxLayout *FilterProp = new QVBoxLayout;
    InputHandler->AddQBoxToLayout(FilterProp);
    FilterProp->addWidget(PointsProp_box);
    FilterProp->addWidget(List_of_points_box);
    AddFilterProperties(FilterProp);
    Properties_names.push_back("Filter prop.");
}

void Puntos_de_estancamiento::TableConfigure()
{
    int i,j;
    j=0;
    double aux[3];
    Points_table->setRowCount(Puntos->GetNumberOfPoints());
    for(i=0;i<Puntos->GetNumberOfPoints();i++)
    {
        Puntos->GetPoint(i,aux);
        for(j=0;j<3;j++)
        {
            QTableWidgetItem *item = new QTableWidgetItem(QString::fromStdString(std::to_string(aux[j])));
            Points_table->setItem(i,j,item);
        }
    }
}

//Time or field functions -----------------------------------------------
void Puntos_de_estancamiento::ChangeToThisTime(int i)
{
    Input->ChangeToThisTime(i);
}
void Puntos_de_estancamiento::ChangeField(std::string field_name)
{
    InputHandler->SetSelectedInput(field_name);
    Update();
}

//Update Functions

void Puntos_de_estancamiento::UpdatePointSize()
{
    PointSize= std::stof(PointSize_LineEdit->text().toStdString());
    Actor->GetProperty()->SetPointSize(PointSize);
}
void Puntos_de_estancamiento::UpdateOnSurfaceCheck()
{
    Update();
}

//Functions use to calculate the points ---------------------------------------------------------
bool Puntos_de_estancamiento::swap(std::vector<std::vector<double>> *A,std::vector<double> *b,int i)
{
  int j=i+1;
  int n= (*A).size();
  while(j<n && (*A)[j][i]==0)
  {
    j++;
  }
  if(j==n)
  {
    return false;
  }
  std::vector<double> aux_A = (*A)[i];
  double aux_b= (*b)[i];
  (*A)[i] = (*A)[j];
  (*A)[j] = aux_A;
  (*b)[i] = (*b)[j];
  (*b)[j] = aux_b;
  return true;

}

std::vector<double> Puntos_de_estancamiento::SolveAxb(std::vector<std::vector<double>> *A, std::vector<double> *b)
{
  int i,j,k;
  double div,s;
  int n = (*A).size();
  std::vector<double> result;
  result.resize(n);
  for(i=0;i<n;i++)
  {
    s=(*A)[i][i];
    if(s==0 && i!=(n-1))
    {

      if(!swap(A,b,i))
      {
        continue;
      }

      s=(*A)[i][i];
    }
    for(j=i+1;j<n;j++)
    {
      div=(*A)[j][i]/s;
      for(k=i;k<n;k++)
      {
        (*A)[j][k]=(*A)[j][k]-(*A)[i][k]*div;
      }
      (*b)[j]=(*b)[j]-(*b)[i]*div;
    }
  }

  result[n-1]=(*b)[n-1]/(*A)[n-1][n-1];
  double sum=0;

  for(i=n-1;i>=0;i--)
  {
    sum= (*b)[i];
    for(j=i+1;j<n;j++)
    {
      sum -= (*A)[i][j]*result[j];
    }
      result[i] = sum/(*A)[i][i];
  }
  return result;
}

bool Puntos_de_estancamiento::Point_calc(vtkSmartPointer<vtkCell> cell,double *p)
{
  //primero calculo los pesos necesarios
  //Los pesos son iguales a las coordenadas parametricas
  //Luego calculo las coordenadas generales a partir de las parametricas
  //A * weights =b
  std::vector<std::vector<double>> A;
  std::vector<double> b;
  unsigned long num_of_components =cell->GetNumberOfPoints()-1;
  unsigned long num_of_points = cell->GetNumberOfPoints();
  vtkSmartPointer<vtkIdList> ids = cell->GetPointIds();

  unsigned long i,j;
  //construyo b
  //b es 0,0,0,1
  double velocidades[num_of_points][3];

  for(i=0;i<num_of_points;i++)
  {
    double *f;
    f = Grid->GetPointData()->GetArray(InputHandler->GetSelectedFieldName().c_str())->GetTuple(ids->GetId(i));
    for(j=0;j<num_of_components;j++)
    {
      velocidades[i][j] = f[j];
    }
  }
  for(i=0;i<num_of_components;i++)
  {
    b.push_back(0);
  }
  b.push_back(1);
  //contruyo A
  std::vector<double> auxiliar;
  auxiliar.resize(num_of_points);
  for(i=0; i<num_of_components; i++)
  {
    for(j=0; j<num_of_points; j++)
    {
      auxiliar[j]=velocidades[j][i];
    }
    A.push_back(auxiliar);
  }
  for(i=0;i<num_of_points;i++)
  {
    auxiliar[i]=1;
  }
  A.push_back(auxiliar);

  std::vector<double> weights = SolveAxb(&A,&b);
  //Si alguno de los pesos es mayor que 1, menor que 1 o 1 - sum(pesos) no esta entre 0 o 1, salgo de la funcion
  for(i=0;i<weights.size();i++)
  {
    if(weights[i]<0 || weights[i]>1)
    {
      return false;
    }
  }
  vtkSmartPointer<vtkPoints> vtkpoints = cell->GetPoints();
  double puntos[num_of_points][3];

  for(i=0;i<num_of_points;i++)
  {
    double *f;
    f= vtkpoints->GetPoint(i);
    for(j=0;j<3;j++)
    {
      puntos[i][j] = f[j];
    }
  }

  p[0]=0;
  p[1]=0;
  p[2]=0;

  for(i=0;i<weights.size();i++)
  {
    for(j=0;j<3;j++)
    {
      p[j]+=(puntos[i][j])*weights[i];
    }
  }
  std::vector<double> vel;
  vel.resize(3);
  for(i=0;i<weights.size();i++)
  {
    for(j=0;j<3;j++)
    {
      vel[j]+=(velocidades[i][j])*weights[i];
    }
  }
 return true;

}

bool Puntos_de_estancamiento::Cell_check(vtkSmartPointer<vtkCell> cell,double point[3])
{
  //Para que una celda tenga velocidad cero necesita 1 punto con signo negativo
  // y otro punto con signo positivo para cada componente.
  int positivo[3] ={0,0,0};
  int negativo[3] ={0,0,0};
  int i,j;

  vtkIdList *ids= cell->GetPointIds();

  int number_of_components = 3;
  double aux[3];

  for(i=0;i<ids->GetNumberOfIds();i++)
  {
    Grid->GetPointData()->GetArray(InputHandler->GetSelectedFieldName().c_str())->GetTuple(ids->GetId(i),aux);
    if(aux[0]==0 && aux[1]==0 && aux[2]==0)
    {
      this->Grid->GetPoints()->GetPoint(ids->GetId(i),point);
      return true;
    }
    for(j=0;j<number_of_components;j++)
    {
      if(positivo[j]!=1)
      {
        if(aux[j]>0)
        {
          positivo[j]=1;
        }
      }
      if(negativo[j]!=1)
      {
        if(aux[j]<0)
        {
          negativo[j]=1;
        }
      }
    }
  }

  if(positivo[0]==1 && positivo[1]==1 && positivo[2]==1)
  {
    if(negativo[0]==1 && negativo[1]==1 && negativo[2]==1)
    {
      return Point_calc(cell,point);
    }
  }
  return false;

}

vtkSmartPointer<vtkPoints> Puntos_de_estancamiento::Points_with_velocity_zero()
{
  int number_of_cells = this->Grid->GetNumberOfCells();
  vtkSmartPointer<vtkPoints> Points_v0 =vtkSmartPointer<vtkPoints>::New();
  vtkIdType i =0;
  double aux[3];
  for(i=0;i<number_of_cells;i++)
  {
    if(Cell_check(this->Grid->GetCell(i),aux))
    {
      Points_v0->InsertNextPoint(aux[0],aux[1],aux[2]);
    }
  }
  return Points_v0;

}

void Puntos_de_estancamiento::AddVerts()
{
    // Create the topology of the point (a vertex)
    vtkSmartPointer<vtkCellArray> vertices =
    vtkSmartPointer<vtkCellArray>::New();
    // We need an an array of point id's for InsertNextCell.
    vtkIdType a[1];
    int s;
    for(s=0;s<Puntos->GetNumberOfPoints();s++)
    {
      a[0]=s;
      vertices->InsertNextCell(1,a);
    }
    Puntos->SetVerts(vertices);
}

void Puntos_de_estancamiento::ExtractSurfacePoints()
{
    vtkSmartPointer<vtkPoints> Filtred_points = vtkSmartPointer<vtkPoints>::New();

    int i;
    double aux[3],aux2[3];

    for(i=0;i<Puntos->GetNumberOfPoints();i++)
    {
        Puntos->GetPoint(i,aux);
        vtkIdType  a = OuterSurface->FindPoint(Puntos->GetPoint(i));
        if(a <0)
            Filtred_points->InsertNextPoint(Puntos->GetPoint(i));
        OuterSurface->GetPoint(a,aux2);
        if( aux[0] != aux2[0] && aux[1] != aux2[1] && aux[2] != aux2[2])
        {
            Filtred_points->InsertNextPoint(Puntos->GetPoint(i));
        }
    }

    Puntos->SetPoints(Filtred_points);
}

void Puntos_de_estancamiento::UpstreamChange()
{
    mute.lock();
    CurrentTimeStep=Input->CurrentTimeStep;
    QComboBox::disconnect(InputHandler->ComboBoxes[0],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputField(QString)));

    std::string prev_velocity_name = InputHandler->GetSelectedFieldName(0);
    InputHandler->RemoveItemsFromComboBox();
    InputHandler->UpdateInputHandler(Input->Field_list);
    InputHandler->AddItemsToComboBox(0,Vector);

    if(!InputHandler->SetSelectedInput(0,prev_velocity_name))
    {
        InputHandler->SetSelectedInput(0,InputHandler->GetVectorsFieldsNames()[0]);
        InputHandler->SetComboBoxIndex(0,InputHandler->GetVectorsFieldsNames()[0]);
    }
    else
    {
        InputHandler->SetComboBoxIndex(0,prev_velocity_name);
    }

    Input->GetOutput(grid_copy);
    Triangulator->SetInputData(grid_copy);
    Triangulator->Update();
    Grid->ShallowCopy(Triangulator->GetOutput());

    QComboBox::connect(InputHandler->ComboBoxes[0],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputField(QString)));
    mute.unlock();

    Update();
}

void Puntos_de_estancamiento::GetOutput(vtkSmartPointer<vtkPolyData> GridData)
{
    GridData->ShallowCopy(Puntos);
}

void Puntos_de_estancamiento::Update()
{
    mute.lock();
    Puntos->SetPoints(Points_with_velocity_zero());
    if(!ShowSurfacePoints->isChecked())
        ExtractSurfacePoints();
    AddVerts();
    Mapper->SetInputData(Puntos);
    Mapper->Update();
    TableConfigure();
    SendSignalsDownStream();
    mute.unlock();
}

vtkDataSet* Puntos_de_estancamiento::GetGeoDataOutput()
{
    return Puntos;
}

void Puntos_de_estancamiento::ChangeInputField(QString name)
{
    ChangeField(name.toStdString());
}

void Puntos_de_estancamiento::ReScaleToCurrentData()
{
    //No need to rescale points (?)
}

#ifndef PUNTOS_DE_ESTANCAMIENTO_H
#define PUNTOS_DE_ESTANCAMIENTO_H

#include "geometry.h"
#include "input_handler.h"

#include <vtkDataSetTriangleFilter.h>
#include <vtkGeometryFilter.h>
#include <QCheckBox>
#include <QScrollBar>
#include <QTableWidget>
#include <QHeaderView>

class Puntos_de_estancamiento: public Geometry
{
    Q_OBJECT
public:
    //Constructor
    Puntos_de_estancamiento();
    //Destructor
    ~Puntos_de_estancamiento();
    /**
     * @brief Sirve como input para la clase
     * @param Geometria input
     */
    void SetInputData(Geometry *Source);
    /**
     * @brief Cambia el campo sobre el cual se calculan los puntos de estancamiento.
     * @param field_name
     */
    void ChangeField(std::string field_name) override;
    /**
     * @brief Cambia el arbol de geometrias a este paso de tiempo
     * @param i = paso de tiempo
     */
    void ChangeToThisTime(int i) override;
    //Points
    float PointSize;
    /**
     * @brief Save the filter output in GridData
     * @param GridData
     */
    void GetOutput(vtkSmartPointer<vtkPolyData> GridData) override;
    /**
     * @brief GetGeoDataOutput
     * @return Filter output
     */
    vtkDataSet* GetGeoDataOutput() override;
    /**
     * @brief UpstreamChange. Se usa en caso ed que el input del filtro cambie. Usualmente se llama mediante "SendSignalsDownStream"
     */
    void UpstreamChange() override;
    /**
     * @brief Actualiza el filtro a nivel objeto.
     */
    void Update() override;
    /**
     * @brief Re escalea la solución al campo actual.
     */
    void ReScaleToCurrentData() override;
private:
    //Funciones necesarias para el filtro
    /**
     * @brief Internal function used to avoid division by 0
     * @param A == matrix
     * @param b == vector
     * @param i == row number
     * @return true if file was changed, false otherwise.
     */
    bool swap(std::vector<std::vector<double>> *A,std::vector<double> *b,int i); //Se usa para evitar divisiones por cero
    //Eliminacion de gauss. Resuelve un sistema Ax = b
    /**
     * @brief SolveAxb by gauss elimination. Ax=b
     * @param A
     * @param b
     * @return x vector "solution"
     */
    std::vector<double> SolveAxb(std::vector<std::vector<double>> *A,std::vector<double> *b);
    /**
     * @brief Calcula el punto de estancamiento en la celda
     * @param cell
     * @param p. If there is a statlement point, is save it in p
     * @return return true if found a point, false otherwise
     */
    bool Point_calc(vtkSmartPointer<vtkCell> cell,double *p);
    /**
     * @brief Cell_check. Check the cell to see if it is posible to have V=(0,0,0)
     * @param cell where to find
     * @param Coordenates of the point found(in case that a point was found)
     * @return true if a point exist, false otherwise
     */
    bool Cell_check(vtkSmartPointer<vtkCell> cell,double point[3]);
    /**
     * @brief Calculate the statlement point of the selected field
     * @return Array with a list of points
     */
    vtkSmartPointer<vtkPoints> Points_with_velocity_zero();
    /**
     * @brief Add the necesaries atributes for visualization
     */
    void AddVerts();
    /**
     * @brief Extract the point in the mesh surface.
     */
    void ExtractSurfacePoints();
    /**
     * @brief Configure the filter properties
     */
    void FilterPropertiesConfig();
    /**
     * @brief Configure the QtTable that shows the point coordenates.
     */
    void TableConfigure();
    /**
     * @brief Clase para manejar los distintos inputs. Controla inputs vectoriales en este caso
     */
    Input_handler *InputHandler;
    /**
     * @brief vtkPolyData que guarda los puntos y permite su visualizacion
     */
    vtkSmartPointer<vtkPolyData> Puntos;
    /**
     * @brief vtkFilter which extract the surface geometry.
     */
    vtkSmartPointer<vtkGeometryFilter> GeometryFilter;
    /**
     * @brief vtkPolyData for saving the outerSurface.
     */
    vtkSmartPointer<vtkPolyData> OuterSurface;
    /**
     * @brief vtkFilter for changin the cell type to tetra (3D) or trinagles(2D).
     */
    vtkSmartPointer<vtkDataSetTriangleFilter> Triangulator;

    QGroupBox *PointsProp_box;
    QGridLayout *PointsProp_layout;
    QLabel *PointSize_label;
    QLineEdit *PointSize_LineEdit;

    QCheckBox *ShowSurfacePoints;

    QGroupBox *List_of_points_box;
    QVBoxLayout *List_of_points_layout;
    QScrollBar *List_of_points_scroll;
    QTableWidget *Points_table;
private slots:
    /**
     * @brief Slot for changin the point size
     */
    void UpdatePointSize();
    /**
     * @brief Slot for activate/deactivate the removal of the surface points.
     */
    void UpdateOnSurfaceCheck();
    /**
     * @brief Slot for changing the input field
     * @param name
     */
    void ChangeInputField(QString name);

};

#endif // PUNTOS_DE_ESTANCAMIENTO_H

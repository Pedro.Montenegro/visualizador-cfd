#include "seedswidgets.h"

SeedsWidgets::SeedsWidgets()
{
    Seeds = vtkSmartPointer<vtkPolyData>::New();
    Connections = vtkSmartPointer<vtkEventQtSlotConnect>::New();

    Mask_input = vtkSmartPointer<vtkPolyPointSource>::New();
    MaskPoints = vtkSmartPointer<vtkMaskPoints>::New();

    PointSource = vtkSmartPointer<vtkPointSource>::New();
    PointSourceMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    PointSourceActor = vtkSmartPointer<vtkActor>::New();
    SphereWidget = vtkSmartPointer<vtkSphereWidget>::New();

    Line_Widget = vtkSmartPointer<vtkLineWidget2>::New();
    Line_Representation = vtkSmartPointer<vtkLineRepresentation>::New();
    Line_source = vtkSmartPointer<vtkLineSource>::New();

    Sphere_position= new double;
    point1 = new double;
    point2 = new double;
    PropertiesConfig();
}
SeedsWidgets::~SeedsWidgets()
{
    Line_Widget->RemoveAllObservers();
    MaskPoints->RemoveAllObservers();
    Connections->RemoveAllObservers();
    SphereWidget->RemoveAllObservers();
    Line_Widget->Off();
    SphereWidget->Off();
    PointSourceActor->VisibilityOff();
    delete Sphere_position;
    delete point1;
    delete point2;
    delete Type_of_seed_box;
    delete MaskPoints_box;
    delete SphereGroupBox;
    delete Line_position_groupbox;
}
void SeedsWidgets::AddPropertiesBoxToLayout(QVBoxLayout *layout)
{
    layout->addWidget(Type_of_seed_box);
    layout->addWidget(SphereGroupBox);
    layout->addWidget(Line_position_groupbox);
    layout->addWidget(MaskPoints_box);
}
void SeedsWidgets::SetInteractor(vtkRenderWindowInteractor* interactor)
{
    Line_Widget->SetInteractor(interactor);
    SphereWidget->SetInteractor(interactor);
}
void SeedsWidgets::WidgetConfig(vtkSmartPointer<vtkPoints> GridPoints, double *GridBounds, double GridLength, double *GridCenter)
{
    this->Grid_bounds = GridBounds;
    //Mask settings
    Mask_input->SetPoints(GridPoints);
    MaskPoints->SetInputConnection(Mask_input->GetOutputPort());
    Max_Seeds = GridPoints->GetNumberOfPoints();
    Ratio_points = Max_Seeds/20; // 0.5% de los puntos
    Ratio_points_LineEdit->setText("20");
    MaskPoints->SetOnRatio(Ratio_points);


    //Sphere Settings

    Sphere_Radio =GridLength/7;
    Sphere_Resolution = 20;
    SphereWidget->SetCenter(GridCenter);
    SphereWidget->SetThetaResolution(Sphere_Resolution/2);
    SphereWidget->SetPhiResolution(Sphere_Resolution/2);
    SphereWidget->SetRadius(Sphere_Radio);
    SphereWidget->On();

    PointSource->SetCenter(GridCenter);
    PointSource->SetRadius(Sphere_Radio);

    SphereWidget->GetCenter(Sphere_position);

    Sphere_x_position_LineEdit->setText(QString::fromStdString(std::to_string(Sphere_position[0])));
    Sphere_y_position_LineEdit->setText(QString::fromStdString(std::to_string(Sphere_position[1])));
    Sphere_z_position_LineEdit->setText(QString::fromStdString(std::to_string(Sphere_position[2])));

    Sphere_radio_LineEdit->setText(QString::fromStdString(std::to_string(Sphere_Radio)));
    PointSource->SetNumberOfPoints(Sphere_Resolution);
    Sphere_Resolution_LineEdit->setText(QString::fromStdString(std::to_string(Sphere_Resolution)));
    PointSource->Update();

    PointSourceMapper->SetInputConnection(PointSource->GetOutputPort());
    PointSourceActor->SetMapper(PointSourceMapper);


    //LineSettings
    Line_Widget->CreateDefaultRepresentation();
    Line_Widget->SetRepresentation(Line_Representation);
    Line_Representation->PlaceWidget(GridBounds);
    Line_Representation->GetPoint1WorldPosition(point1);
    Line_Representation->GetPoint2WorldPosition(point2);

    Line_resolution = 20;
    is2D=0;


    Point1_x_LineEdit->setText(QString::fromStdString(std::to_string(point1[0])));
    Point1_y_LineEdit->setText(QString::fromStdString(std::to_string(point1[1])));
    Point1_z_LineEdit->setText(QString::fromStdString(std::to_string(point1[2])));
    Point2_x_LineEdit->setText(QString::fromStdString(std::to_string(point2[0])));
    Point2_y_LineEdit->setText(QString::fromStdString(std::to_string(point2[1])));
    Point2_z_LineEdit->setText(QString::fromStdString(std::to_string(point2[2])));
    Line_Resolution_LineEdit->setText(QString::fromStdString(std::to_string(Line_resolution)));

    //SomeConections
    this->Connections->Connect(SphereWidget,vtkCommand::EndInteractionEvent,this,SLOT(RecalculateOnSphereChange()),NULL,1);

    QRadioButton::connect(mask_seeds,SIGNAL(released()),this,SLOT(ChangeTypeOfSeedMethodToMask()));
    QRadioButton::connect(sphere_seeds,SIGNAL(released()),this,SLOT(ChangeTypeOfSeedMethodToSphere()));
    QRadioButton::connect(Line_seeds,SIGNAL(released()),this,SLOT(ChangeTypeOfSeedMethodToLine()));

    QRadioButton::connect(normal_mask,SIGNAL(pressed()),this,SLOT(UpdateToNormalMaskPoint()));
    QRadioButton::connect(Aleatory_mask,SIGNAL(pressed()),this,SLOT(UpdateToRandomMaskPoint()));

    QLineEdit::connect(Ratio_points_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdateRatioPoints()));

    QLineEdit::connect(Sphere_Resolution_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdateResolutionOfSphere()));
    QLineEdit::connect(Sphere_radio_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdateRadioOfSphere()));

    QLineEdit::connect(Sphere_x_position_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdatePositionOfSphere()));
    QLineEdit::connect(Sphere_y_position_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdatePositionOfSphere()));
    QLineEdit::connect(Sphere_z_position_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdatePositionOfSphere()));

    QLineEdit::connect(Point1_x_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdatePointsOfLine()));
    QLineEdit::connect(Point1_y_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdatePointsOfLine()));
    QLineEdit::connect(Point1_z_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdatePointsOfLine()));

    QLineEdit::connect(Point2_x_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdatePointsOfLine()));
    QLineEdit::connect(Point2_y_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdatePointsOfLine()));
    QLineEdit::connect(Point2_z_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdatePointsOfLine()));

    QLineEdit::connect(Line_Resolution_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdateResolutionOfLine()));

    QCheckBox::connect(is2D_CheckBox,SIGNAL(pressed()),this,SLOT(Is2DChange()));

    Seeds = PointSource->GetOutput();
}

void SeedsWidgets::PropertiesConfig()
{

    //Variables sobre el tipo de semilla.
    Type_of_seed_box = new QGroupBox;
    Type_of_seed_box->setTitle("Seed generator method");
    Type_of_seed_box->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    Type_of_seed_box->setMinimumHeight(100);

    Type_of_seed_layout = new QVBoxLayout(Type_of_seed_box);
    mask_seeds = new QRadioButton(Type_of_seed_box);
    mask_seeds->setText("Mask method");
    sphere_seeds = new QRadioButton(Type_of_seed_box);
    sphere_seeds->setText("Sphere method");
    sphere_seeds->setChecked(true);
    Line_seeds = new QRadioButton(Type_of_seed_box);
    Line_seeds->setText("Line method");

    Type_of_seed_layout->addWidget(mask_seeds);
    Type_of_seed_layout->addWidget(sphere_seeds);
    Type_of_seed_layout->addWidget(Line_seeds);
    Type_of_seed_box->setLayout(Type_of_seed_layout);

    //Variables sobre maskPoints
    MaskPoints_box = new QGroupBox;
    MaskPoints_box->setTitle("MaskPoints");
    MaskPoints_box->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    MaskPoints_box->setMinimumHeight(150);

    MaskPoints_layout = new QGridLayout(MaskPoints_box);
    Ratio_points_label = new QLabel(MaskPoints_box);
    Ratio_points_label->setText("Seeds");
    Ratio_points_LineEdit = new QLineEdit(MaskPoints_box);
    Ratio_points_LineEdit->setValidator(new QIntValidator(0,1000000,MaskPoints_box));
    normal_mask = new QRadioButton(MaskPoints_box);
    normal_mask->setText("Simple sample");
    normal_mask->setChecked(true);
    Aleatory_mask = new QRadioButton(MaskPoints_box);
    Aleatory_mask->setText("Random sample");

    MaskPoints_layout->addWidget(Ratio_points_label,0,0);
    MaskPoints_layout->addWidget(Ratio_points_LineEdit,0,1);
    MaskPoints_layout->addWidget(normal_mask,1,0);
    MaskPoints_layout->addWidget(Aleatory_mask,2,0);

    MaskPoints_box->setLayout(MaskPoints_layout);
    MaskPoints_box->hide();

    //Sphere widget
    SphereGroupBox = new QGroupBox;
    SphereGroupBox->setMinimumHeight(150);
    SphereLayout = new QGridLayout(SphereGroupBox);
    SphereGroupBox->setTitle("PointSource properties");
    SphereGroupBox->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    Sphere_Resolution_Label = new QLabel(SphereGroupBox);
    Sphere_Resolution_Label->setText("Seeds");
    Sphere_Resolution_LineEdit = new QLineEdit(SphereGroupBox);
    Sphere_Resolution_LineEdit->setValidator(new QIntValidator(0,100000,SphereGroupBox));
    Sphere_radio_label = new QLabel(SphereGroupBox);
    Sphere_radio_label->setText("Radio");
    Sphere_radio_LineEdit = new QLineEdit(SphereGroupBox);
    Sphere_radio_LineEdit->setValidator(new QDoubleValidator(0,1000000,8,SphereGroupBox));

    Sphere_x_position_label = new QLabel(SphereGroupBox);
    Sphere_x_position_label->setText("x");
    Sphere_y_position_label = new QLabel(SphereGroupBox);
    Sphere_y_position_label->setText("y");
    Sphere_z_position_label = new QLabel(SphereGroupBox);
    Sphere_z_position_label->setText("z");


    Sphere_x_position_LineEdit = new QLineEdit(SphereGroupBox);
    Sphere_x_position_LineEdit->setValidator(new QDoubleValidator(SphereGroupBox));
    Sphere_y_position_LineEdit = new QLineEdit(SphereGroupBox);
    Sphere_y_position_LineEdit->setValidator(new QDoubleValidator(SphereGroupBox));
    Sphere_z_position_LineEdit = new QLineEdit(SphereGroupBox);
    Sphere_z_position_LineEdit->setValidator(new QDoubleValidator(SphereGroupBox));

    SphereLayout->addWidget(Sphere_x_position_label,0,0,Qt::AlignmentFlag::AlignRight);
    SphereLayout->addWidget(Sphere_x_position_LineEdit,0,1);
    SphereLayout->addWidget(Sphere_y_position_label,0,2);
    SphereLayout->addWidget(Sphere_y_position_LineEdit,0,3);
    SphereLayout->addWidget(Sphere_z_position_label,0,4);
    SphereLayout->addWidget(Sphere_z_position_LineEdit,0,5);

    SphereLayout->addWidget(Sphere_radio_label,1,0,1,1);
    SphereLayout->addWidget(Sphere_radio_LineEdit,1,1,1,5);
    SphereLayout->addWidget(Sphere_Resolution_Label,2,0,2,1);
    SphereLayout->addWidget(Sphere_Resolution_LineEdit,2,1,2,5);

    SphereGroupBox->setLayout(SphereLayout);

    //Line
    Line_position_groupbox = new QGroupBox;
    Line_position_layout = new QGridLayout(Line_position_groupbox);
    Line_position_groupbox->setTitle("Line Properties");
    Line_position_groupbox->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    Line_position_groupbox->setMinimumHeight(150);

    Point1_x_Label = new QLabel(Line_position_groupbox);
    Point1_x_Label->setText("1:x");
    Point1_y_Label = new QLabel(Line_position_groupbox);
    Point1_y_Label->setText("1:y");
    Point1_z_Label = new QLabel(Line_position_groupbox);
    Point1_z_Label->setText("1:z");

    Point2_x_Label = new QLabel(Line_position_groupbox);
    Point2_x_Label->setText("2:x");
    Point2_y_Label = new QLabel(Line_position_groupbox);
    Point2_y_Label->setText("2:y");
    Point2_z_Label = new QLabel(Line_position_groupbox);
    Point2_z_Label->setText("2:z");

    Point1_x_LineEdit = new QLineEdit(Line_position_groupbox);
    Point1_x_LineEdit->setValidator(new QDoubleValidator(-10000000,10000000,3,Point1_x_LineEdit));
    Point1_y_LineEdit = new QLineEdit(Line_position_groupbox);
    Point1_y_LineEdit->setValidator(new QDoubleValidator(-10000000,10000000,3,Point1_y_LineEdit));
    Point1_z_LineEdit = new QLineEdit(Line_position_groupbox);
    Point1_z_LineEdit->setValidator(new QDoubleValidator(-10000000,10000000,3,Point1_z_LineEdit));

    Point2_x_LineEdit = new QLineEdit(Line_position_groupbox);
    Point2_x_LineEdit->setValidator(new QDoubleValidator(-10000000,10000000,3,Point2_x_LineEdit));
    Point2_y_LineEdit = new QLineEdit(Line_position_groupbox);
    Point2_y_LineEdit->setValidator(new QDoubleValidator(-10000000,10000000,3,Point2_y_LineEdit));
    Point2_z_LineEdit = new QLineEdit(Line_position_groupbox);
    Point2_z_LineEdit->setValidator(new QDoubleValidator(-10000000,10000000,3,Point2_z_LineEdit));

    Line_Resolution_LineEdit = new QLineEdit(Line_position_groupbox);
    Line_Resolution_label = new QLabel(Line_position_groupbox);
    Line_Resolution_label->setText("Seeds");

    is2D_CheckBox = new QCheckBox(Line_position_groupbox);
    is2D_CheckBox->setText("is2D");
    is2D_CheckBox->setCheckState(Qt::Unchecked);

    Line_position_layout->addWidget(Point1_x_Label,0,0);
    Line_position_layout->addWidget(Point1_x_LineEdit,0,1);
    Line_position_layout->addWidget(Point1_y_Label,0,2);
    Line_position_layout->addWidget(Point1_y_LineEdit,0,3);
    Line_position_layout->addWidget(Point1_z_Label,0,4);
    Line_position_layout->addWidget(Point1_z_LineEdit,0,5);

    Line_position_layout->addWidget(Point2_x_Label,1,0);
    Line_position_layout->addWidget(Point2_x_LineEdit,1,1);
    Line_position_layout->addWidget(Point2_y_Label,1,2);
    Line_position_layout->addWidget(Point2_y_LineEdit,1,3);
    Line_position_layout->addWidget(Point2_z_Label,1,4);
    Line_position_layout->addWidget(Point2_z_LineEdit,1,5);

    Line_position_layout->addWidget(Line_Resolution_label,2,0,2,2);

    Line_position_layout->addWidget(Line_Resolution_LineEdit,2,3,2,5);

    Line_position_layout->addWidget(is2D_CheckBox,3,0,3,2);

    Line_position_groupbox->setLayout(Line_position_layout);
    Line_position_groupbox->hide();

}
//MASK----------------------------
void SeedsWidgets::ChangeTypeOfSeedMethodToMask()
{
    MaskPoints_box->show();
    SphereGroupBox->hide();
    Line_position_groupbox->hide();
    Line_Widget->Off();
    SphereWidget->Off();
    if(sphere_seeds->isChecked())
    {
        this->Connections->Disconnect(SphereWidget,vtkCommand::EndInteractionEvent,this,SLOT(RecalculateOnSphereChange()));
    }
    if(Line_seeds->isChecked())
    {
        this->Connections->Disconnect(Line_Widget,vtkCommand::EndInteractionEvent,this,SLOT(RecalculateOnLineChange()));
    }
    PointSourceActor->VisibilityOff();
    Seeds = MaskPoints->GetOutput();
    MaskPoints->Update();

}
void SeedsWidgets::UpdateRatioPoints()
{
    if(Ratio_points_LineEdit->text().isEmpty())
        return;
    Ratio_points = std::stoi(Ratio_points_LineEdit->text().toStdString());
    Ratio_points = Max_Seeds/Ratio_points;
    MaskPoints->SetOnRatio(Ratio_points);
    Seeds = MaskPoints->GetOutput();
    MaskPoints->Update();

}

void SeedsWidgets::UpdateToNormalMaskPoint()
{
    MaskPoints->SetRandomMode(false);
    Seeds = MaskPoints->GetOutput();
    MaskPoints->Update();
}
void SeedsWidgets::UpdateToRandomMaskPoint()
{
    MaskPoints->SetRandomMode(true);
    MaskPoints->SetRandomSeed(MaskPoints->RANDOM_SAMPLING);
    Seeds = MaskPoints->GetOutput();
    MaskPoints->Update();

}

//SPHERE------------------------------------
void SeedsWidgets::RecalculateOnSphereChange()
{
    PointSource->SetCenter(SphereWidget->GetCenter());
    PointSource->SetRadius(SphereWidget->GetRadius());
    PointSource->Update();
    Sphere_Radio = SphereWidget->GetRadius();
    Sphere_radio_LineEdit->setText(QString::fromStdString(std::to_string(Sphere_Radio)));

    SphereWidget->GetCenter(Sphere_position);
    Sphere_x_position_LineEdit->setText(QString::fromStdString(std::to_string(Sphere_position[0])));
    Sphere_y_position_LineEdit->setText(QString::fromStdString(std::to_string(Sphere_position[1])));
    Sphere_z_position_LineEdit->setText(QString::fromStdString(std::to_string(Sphere_position[2])));
    Seeds = PointSource->GetOutput();

}
void SeedsWidgets::ChangeTypeOfSeedMethodToSphere()
{
    SphereGroupBox->show();
    MaskPoints_box->hide();
    Line_position_groupbox->hide();
    SphereWidget->On();
    Line_Widget->Off();
    if(Line_seeds->isChecked())
    {
        this->Connections->Disconnect(Line_Widget,vtkCommand::EndInteractionEvent,this,SLOT(RecalculateOnLineChange()));
    }
    PointSourceActor->VisibilityOn();
    PointSource->SetCenter(SphereWidget->GetCenter());
    PointSource->SetRadius(SphereWidget->GetRadius());
    PointSource->Update();
    Sphere_Radio = SphereWidget->GetRadius();
    Sphere_radio_LineEdit->setText(QString::fromStdString(std::to_string(Sphere_Radio)));
    this->Connections->Connect(SphereWidget,vtkCommand::EndInteractionEvent,this,SLOT(RecalculateOnSphereChange()),NULL,1);
    Seeds = PointSource->GetOutput();
    emit UpdateFilter();

}

void SeedsWidgets::UpdateResolutionOfSphere()
{
    Sphere_Resolution = std::stoi(Sphere_Resolution_LineEdit->text().toStdString());
    PointSource->SetNumberOfPoints(Sphere_Resolution);
    PointSource->Update();
    Seeds = PointSource->GetOutput();
}

void SeedsWidgets::UpdatePositionOfSphere()
{
    if(Sphere_x_position_LineEdit->text().isEmpty() || Sphere_y_position_LineEdit->text().isEmpty() || Sphere_z_position_LineEdit->text().isEmpty())
        return;
    Sphere_position[0]=std::stod(Sphere_x_position_LineEdit->text().toStdString());
    Sphere_position[1]=std::stod(Sphere_y_position_LineEdit->text().toStdString());
    Sphere_position[2]=std::stod(Sphere_z_position_LineEdit->text().toStdString());
    SphereWidget->SetCenter(Sphere_position);
    PointSource->SetCenter(Sphere_position);
    PointSource->Update();
    Seeds = PointSource->GetOutput();
}
void SeedsWidgets::UpdateRadioOfSphere()
{
    Sphere_Radio = std::stod(Sphere_radio_LineEdit->text().toStdString());
    SphereWidget->SetRadius(Sphere_Radio);
    PointSource->SetRadius(Sphere_Radio);
    PointSource->Update();

}

//LINE--------------------------------------------------
void SeedsWidgets::RecalculateOnLineChange()
{
    Line_Representation->GetPoint1WorldPosition(point1);
    Line_Representation->GetPoint2WorldPosition(point2);

    if(is2D==1)
    {
        double *bounds;
        bounds = Grid_bounds;
        if(bounds[0]==bounds[1])
        {
            point1[0] = bounds[0];
            point2[0] = bounds[0];
        }
        else if(bounds[2]==bounds[3])
        {
            point1[1] = bounds[2];
            point2[1] = bounds[2];
        }
        else if(bounds[4]==bounds[5])
        {
            point1[2] = bounds[4];
            point2[2] = bounds[4];
        }
        else // Por defecto z, si se selecciona
        {
            point1[2] = bounds[4];
            point2[2] = bounds[4];
        }

        Line_Representation->SetPoint1WorldPosition(point1);
        Line_Representation->SetPoint2WorldPosition(point2);
    }


    Line_source->SetPoint1(point1);
    Line_source->SetPoint2(point2);

    Line_source->SetResolution(Line_resolution);
    Line_source->Update();
    Seeds = Line_source->GetOutput();


    Point1_x_LineEdit->setText(QString::fromStdString(std::to_string(point1[0])));
    Point1_y_LineEdit->setText(QString::fromStdString(std::to_string(point1[1])));
    Point1_z_LineEdit->setText(QString::fromStdString(std::to_string(point1[2])));
    Point2_x_LineEdit->setText(QString::fromStdString(std::to_string(point2[0])));
    Point2_y_LineEdit->setText(QString::fromStdString(std::to_string(point2[1])));
    Point2_z_LineEdit->setText(QString::fromStdString(std::to_string(point2[2])));


}
void SeedsWidgets::ChangeTypeOfSeedMethodToLine()
{
    MaskPoints_box->hide();
    Line_position_groupbox->show();
    SphereGroupBox->hide();
    SphereWidget->Off();
    Line_Widget->On();
    PointSourceActor->VisibilityOff();

    if(sphere_seeds->isChecked())
    {
        this->Connections->Disconnect(SphereWidget,vtkCommand::EndInteractionEvent,this,SLOT(RecalculateOnSphereChange()));
    }
    this->Connections->Connect(Line_Widget,vtkCommand::EndInteractionEvent,this,SLOT(RecalculateOnLineChange()),NULL,1);

    Line_source->SetPoint1(Line_Representation->GetPoint1WorldPosition());
    Line_Representation->GetPoint1WorldPosition(point1);
    Line_source->SetPoint2(Line_Representation->GetPoint2WorldPosition());
    Line_Representation->GetPoint2WorldPosition(point2);
    Line_source->SetResolution(Line_resolution);
    Line_source->Update();
    Seeds = Line_source->GetOutput();
    emit UpdateFilter();

}

void SeedsWidgets::UpdateResolutionOfLine()
{
    if(Line_Resolution_LineEdit->text().isEmpty())
        return;

    Line_resolution = std::stod(Line_Resolution_LineEdit->text().toStdString());
    Line_source->SetResolution(Line_resolution);
    Line_source->Update();
    Seeds = Line_source->GetOutput();


}

void SeedsWidgets::UpdatePointsOfLine()
{
    if(Point1_x_LineEdit->text().isEmpty() || Point1_y_LineEdit->text().isEmpty() || Point1_z_LineEdit->text().isEmpty() || Point2_x_LineEdit->text().isEmpty() || Point2_y_LineEdit->text().isEmpty() || Point2_z_LineEdit->text().isEmpty())
    {
        return;
    }
    point1[0] = std::stod(Point1_x_LineEdit->text().toStdString());
    point1[1] = std::stod(Point1_y_LineEdit->text().toStdString());
    point1[2] = std::stod(Point1_z_LineEdit->text().toStdString());

    point2[0] = std::stod(Point2_x_LineEdit->text().toStdString());
    point2[1] = std::stod(Point2_y_LineEdit->text().toStdString());
    point2[2] = std::stod(Point2_z_LineEdit->text().toStdString());

    Line_Representation->SetPoint1WorldPosition(point1);
    Line_Representation->SetPoint2WorldPosition(point2);

    Line_source->SetPoint1(point1);
    Line_source->SetPoint2(point2);


    Line_source->Update();
    Seeds = Line_source->GetOutput();
}

void SeedsWidgets::Is2DChange()
{
    if(is2D_CheckBox->isChecked())
    {
        is2D=0;
    }
    else
    {
        is2D=1;
    }

    Line_Representation->GetPoint1WorldPosition(point1);
    Line_Representation->GetPoint2WorldPosition(point2);

    if(is2D==1)
    {
        double *bounds = Grid_bounds;
        if(bounds[0]==bounds[1])
        {
            point1[0] = bounds[0];
            point2[0] = bounds[0];
        }
        else if(bounds[2]==bounds[3])
        {
            point1[1] = bounds[2];
            point2[1] = bounds[2];
        }
        else if(bounds[4]==bounds[5])
        {
            point1[2] = bounds[4];
            point2[2] = bounds[4];
        }
        else // Por defecto z, si se selecciona
        {
            point1[2] = bounds[4];
            point2[2] = bounds[4];
        }
    }
    Line_Representation->PlaceWidget(Grid_bounds);

    Line_Representation->SetPoint1WorldPosition(point1);
    Line_Representation->SetPoint2WorldPosition(point2);

    Line_source->SetPoint1(point1);
    Line_source->SetPoint2(point2);


    Line_source->Update();
    Seeds = Line_source->GetOutput();

    Point1_x_LineEdit->setText(QString::fromStdString(std::to_string(point1[0])));
    Point1_y_LineEdit->setText(QString::fromStdString(std::to_string(point1[1])));
    Point1_z_LineEdit->setText(QString::fromStdString(std::to_string(point1[2])));
    Point2_x_LineEdit->setText(QString::fromStdString(std::to_string(point2[0])));
    Point2_y_LineEdit->setText(QString::fromStdString(std::to_string(point2[1])));
    Point2_z_LineEdit->setText(QString::fromStdString(std::to_string(point2[2])));
}

void SeedsWidgets::HideOrShow(int i)
{
    if(i==0)
    {
        PointSourceActor->VisibilityOff();
        Line_Widget->Off();
        SphereWidget->Off();
    }
    else
    {
        if(sphere_seeds->isChecked())
        {
            SphereWidget->On();
            PointSourceActor->VisibilityOn();
        }
        else if(Line_seeds->isChecked())
            Line_Widget->On();
    }
}

void SeedsWidgets::DisconectAll()
{
    mute.lock();
    if(sphere_seeds->isChecked())
    {
        this->Connections->Disconnect(SphereWidget,vtkCommand::EndInteractionEvent,this,SLOT(RecalculateOnSphereChange()));
    }
    else if(Line_seeds->isChecked())
    {
        this->Connections->Disconnect(Line_Widget,vtkCommand::EndInteractionEvent,this,SLOT(RecalculateOnLineChange()));
    }
}

void SeedsWidgets::Reconect()
{
    if(sphere_seeds->isChecked())
    {
        this->Connections->Connect(SphereWidget,vtkCommand::EndInteractionEvent,this,SLOT(RecalculateOnSphereChange()));
    }
    else if(Line_seeds->isChecked())
    {
        this->Connections->Connect(Line_Widget,vtkCommand::EndInteractionEvent,this,SLOT(RecalculateOnLineChange()));
    }
    mute.unlock();

}

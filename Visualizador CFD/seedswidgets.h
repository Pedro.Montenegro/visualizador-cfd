#ifndef SEEDSWIDGETS_H
#define SEEDSWIDGETS_H

#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QGridLayout>
#include <QRadioButton>
#include <QDoubleValidator>
#include <QMutex>

#include <vtkSmartPointer.h>
#include <vtkActor.h>
#include <vtkSphereWidget.h>
#include <vtkEventQtSlotConnect.h>
#include <vtkPointSource.h>
#include <vtkLineWidget2.h>
#include <vtkLineRepresentation.h>
#include <vtkLineSource.h>
#include <vtkMaskPoints.h>
#include <vtkPolyPointSource.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>

/**
 * @brief The SeedsWidgets class
 * Clase que crea y configura todas las variables necesarias para generar semillas de puntos.
 * Posee 3 metodos distintos
 */
class SeedsWidgets: public QObject
{
    Q_OBJECT
public:
    //Constructor
    SeedsWidgets();
    //Destructor
    ~SeedsWidgets();
    /**
     * @brief Configura los distintos widgets
     * @param GridPoints. Puntos de la grilla, necesarios para MaskPoints
     * @param GridBounds. Bordes de la grilla, para posicionar el widget
     * @param GridLength. Longitud de la grilla, para posicionar el widget.
     * @param GridCenter. Centro de la grilla, para posiconar el widget.
     */
    void WidgetConfig(vtkSmartPointer<vtkPoints> GridPoints, double *GridBounds, double GridLength,double *GridCenter);
    /**
     * @brief Configura el renderwindow interactor, necesario para los widgets
     * @param interactor
     */
    void SetInteractor(vtkRenderWindowInteractor* interactor);
    /**
     * @brief vtkPolyData que guarda las semillas
     */
    vtkSmartPointer<vtkPolyData> Seeds;
    /**
     * @brief Disconect all. Use to update filters without crashing.
     */
    void DisconectAll();
    /**
     * @brief Reconect. Reestructure after filter update.
     */
    void Reconect();

    //Variables de configuración
    double *Grid_bounds;
    unsigned long Max_Seeds;
    //Functions
    /**
     * @brief Esconde o muestra los widgets
     * @param i=1 muestra, i=0 esconde
     */
    void HideOrShow(int i);
    /**
     * @brief Adhiere la gruopbox de propiedades al layout
     * @param layout
     */
    void AddPropertiesBoxToLayout(QVBoxLayout *layout);
    /**
     * @brief MaskPoints. Filtro de vtk que selecciona puntos de manera ordenada.
     */
    vtkSmartPointer<vtkMaskPoints> MaskPoints;
    /**
     * @brief Actor usado para ver los puntos de la esfera
     */
    vtkSmartPointer<vtkActor> PointSourceActor;
    /**
     * @brief SphereWidget que permite mover una esfera en la interfaz
     */
    vtkSmartPointer<vtkSphereWidget> SphereWidget;
    /**
     * @brief Clase de vtk para generar una linea movil en la interfaz.
     */
    vtkSmartPointer<vtkLineWidget2> Line_Widget;
private:
    QMutex mute;
    /**
     * @brief Configura las propiedades de la interfaz
     */
    void PropertiesConfig();
    /**
     * @brief Mask_input. Filtro de vtk para usar nodos de la geometria como input a maskpoints
     */
    vtkSmartPointer<vtkPolyPointSource> Mask_input;
    /**
     * @brief Filtro de vtk que genera una nube de puntos dentro de una esfera
     */
    vtkSmartPointer<vtkPointSource> PointSource;
    /**
     * @brief Mapper usado para ver los puntos de la esfera
     */
    vtkSmartPointer<vtkPolyDataMapper> PointSourceMapper;
    /**
     * @brief Clase que permite hacer conexiones entre objetos de QT y VTK
     */
    vtkSmartPointer<vtkEventQtSlotConnect> Connections;
    /**
     * @brief Representación de la linea movil en la interfaz
     */
    vtkSmartPointer<vtkLineRepresentation> Line_Representation;
    /**
     * @brief Genera semillas equidistnates sobre la linea de puntos
     */
    vtkSmartPointer<vtkLineSource> Line_source;

    //Pestaña de propiedades
    //Variables sobre el tipo de semilla.
    QGroupBox *Type_of_seed_box;
    QVBoxLayout *Type_of_seed_layout;
    QRadioButton *mask_seeds;
    QRadioButton *sphere_seeds;
    QRadioButton *Line_seeds;


    //Variables sobre maskPoints (en caso que se use este metodo)
    QGroupBox *MaskPoints_box;
    QGridLayout *MaskPoints_layout;
    QLabel *Ratio_points_label;
    QLineEdit *Ratio_points_LineEdit;
    int Ratio_points;

    QRadioButton *normal_mask;
    QRadioButton *Aleatory_mask;


    //Variables sobre sphereWidget
    QGroupBox *SphereGroupBox;
    QGridLayout *SphereLayout;
    QLabel *Sphere_Resolution_Label;
    QLineEdit *Sphere_Resolution_LineEdit;
    double Sphere_Resolution;
    QLabel *Sphere_radio_label;
    QLineEdit *Sphere_radio_LineEdit;
    double Sphere_Radio;
    QLabel *Sphere_x_position_label, *Sphere_y_position_label, *Sphere_z_position_label;
    QLineEdit *Sphere_x_position_LineEdit,*Sphere_y_position_LineEdit,*Sphere_z_position_LineEdit;
    double *Sphere_position;


    //Variables de la linea
    QGroupBox *Line_position_groupbox;
    QGridLayout *Line_position_layout;
    QLabel *Point1_x_Label, *Point1_y_Label, *Point1_z_Label;
    QLabel *Point2_x_Label, *Point2_y_Label, *Point2_z_Label;
    QLineEdit *Point1_x_LineEdit, *Point1_y_LineEdit, *Point1_z_LineEdit;
    QLineEdit *Point2_x_LineEdit, *Point2_y_LineEdit, *Point2_z_LineEdit;
    QLabel *Line_Resolution_label;
    QLineEdit *Line_Resolution_LineEdit;
    QCheckBox *is2D_CheckBox;
    int is2D; //0 si es 2D , 1 si es 3D

    double *point1;
    double *point2;
    double Line_resolution;
private slots:
    //Type of seed slots
    /**
     * @brief ChangeTypeOfSeedMethodToSphere
     */
    void ChangeTypeOfSeedMethodToSphere();
    /**
     * @brief ChangeTypeOfSeedMethodToMask
     */
    void ChangeTypeOfSeedMethodToMask();
    /**
     * @brief ChangeTypeOfSeedMethodToLine
     */
    void ChangeTypeOfSeedMethodToLine();
    //Sphere slots
    /**
     * @brief Re calculate the seeds after a change on the sphere
     */
    void RecalculateOnSphereChange();
    /**
     * @brief Actualiza el número de semillas de la esfera
     */
    void UpdateResolutionOfSphere();
    /**
     * @brief Actualiza la posicion de la esfera luego de que el usuario escribe las coordenadas en la pestaña de propiedades
     */
    void UpdatePositionOfSphere();
    /**
     * @brief cambia el radio de la esfera
     */
    void UpdateRadioOfSphere();
    //Line slots
    /**
     * @brief Recalcula las semillas de la linea luego de un cambio en el widget
     */
    void RecalculateOnLineChange();
    /**
     * @brief Actualiza el numero de semillas generada por la linea
     */
    void UpdateResolutionOfLine();
    /**
     * @brief Cambia la posición del widget y las semillas ante un input del usuario en la pestaña de propiedades
     */
    void UpdatePointsOfLine();
    /**
     * @brief Cambia el modo de los widgets a 2D
     */
    void Is2DChange();
    /**
     * @brief Actualiza el numero de semillas de maskpoints
     */
    void UpdateRatioPoints();
    /**
     * @brief Cambia el metodo de generación de semillas de maskpoint a normal
     */
    void UpdateToNormalMaskPoint();
    /**
     * @brief Cambia el metodo de generación de semillas de maskpoint a random
     */
    void UpdateToRandomMaskPoint();
signals:
    void UpdateFilter();
};

#endif // SEEDSWIDGETS_H

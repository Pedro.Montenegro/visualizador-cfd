#include "slice_filter.h"

Slice_filter::Slice_filter()
{
    Cutter = vtkSmartPointer<vtkCutter>::New();
    toUnstrGrid = vtkSmartPointer<vtkAppendFilter>::New();
    Plane = new Plane_Widget;
    callback = new GeoCallback;
    type = Filter_type::Slice;
    FilterPropConfig();
    grid_copy = vtkSmartPointer<vtkUnstructuredGrid>::New();
}
Slice_filter::~Slice_filter()
{
    mute.lock();
    unsigned long i;
    for( i= 0 ; i< Input->NextFilter.size();i++)
    {
        if(Input->NextFilter[i]->Name == this->Name)
        {
            break;
        }
    }
    Input->NextFilter.erase(Input->NextFilter.begin()+i);
    Plane->plane_Widget->RemoveObserver(callback);
    delete Plane;
    callback->Delete();

    for(unsigned long i=0;i<Field_list.size();i++)
    {
        delete Field_list[i];
    }
    Field_list.clear();
    mute.unlock();
}

void Slice_filter::SetInputData(Geometry *Source)
{
    mute.lock();
    //render_Win = Source->render_Win;
    this->Times->ShallowCopy(Source->Times);

    this->Name = Source->Name + "/Slice";
    //this->type = Source->type;
    if(type == PuntosEstancamiento)
    {
        Actor->GetProperty()->SetPointSize(10);
    }
    this->SelectedField =0;
    this->CurrentTimeStep = Source->CurrentTimeStep;

    Input = Source;

    callback->Geo = this;
    Input->GetOutput(grid_copy);
    Cutter->SetInputData(grid_copy);

    //Plane settings
    Plane->WidgetConfig(render_Win,Input->Mapper->GetBounds(),Input->Mapper->GetCenter());
    //Clipper->SetInputConnection(Source->Mapper->GetInputConnection(0,0));
    //render_Win->Render();

    Cutter->SetCutFunction(Plane->plane_function);

    Cutter->Update();

    toUnstrGrid->SetInputData(Cutter->GetOutput());
    toUnstrGrid->Update();
    Grid->ShallowCopy(toUnstrGrid->GetOutput());

    for(int i=0;i<Grid->GetPointData()->GetNumberOfArrays();i++)
    {
        unsigned long j;
        vtkSmartPointer<vtkDataArray> aux = Grid->GetPointData()->GetArray(i);
        const std::string name1 = aux->GetName();
        if(name1.find("comp") !=std::string::npos)
            continue;
        for(j=0; j< Input->Field_list.size();j++)
        {
            if(Input->Field_list[j]->Field_Name ==name1)
            {
                break;
            }
        }
        if(j==Input->Field_list.size())
            continue;
        if(i!=0)
        {
            std::string name2 =Grid->GetPointData()->GetArray(i-1)->GetName();
            name2 = name2 +"Magnitud";
            if(name1 ==name2)
                continue;
        }
        this->Field_list.push_back(new Field(aux));
    }


    Mapper->SetInputData(Grid);
    Mapper->Update();
    Actor->SetMapper(Mapper);
    Plane->plane_Widget->AddObserver(vtkCommand::EndInteractionEvent,callback);
    Cutter->Update();

    SetVectors();

    Input->NextFilter.push_back(this);

    mute.unlock();

}

void Slice_filter::FilterPropConfig()
{
    QVBoxLayout *FilterProp = new QVBoxLayout;
    Plane->AddPropertiesBoxToLayout(FilterProp);
    AddFilterProperties(FilterProp);
    FilterProp->setAlignment(Qt::AlignmentFlag::AlignTop);
    Properties_names.push_back("Filter prop.");
}

void Slice_filter::UpstreamChange()
{
    mute.lock();
    CurrentTimeStep = Input->CurrentTimeStep;
    Input->GetOutput(grid_copy);
    mute.unlock();
    Update();
}
void Slice_filter::ReScaleToCurrentData()
{
    if(Grid->GetPointData()->GetNumberOfArrays() ==0)
        return;
    if(Field_list[SelectedField]->type ==Solid)
        return;
    if(Field_list[SelectedField]->selectedComponent==-1)
    {
        Mapper->SetScalarRange(Field_list[SelectedField]->Solution_Magnitud[0]->GetRange());
    }
    else
    {
        int selec = Field_list[SelectedField]->selectedComponent;
        Mapper->SetScalarRange(Field_list[SelectedField]->Solution_components[0][selec]->GetRange());
    }
    Mapper->Update();
}
void Slice_filter::ChangeToThisTime(int i)
{
    Input->ChangeToThisTime(i);
}
void Slice_filter::GetOutput(vtkSmartPointer<vtkUnstructuredGrid> GridData)
{
    mute.lock();
    GridData->ShallowCopy(Grid);
    mute.unlock();
}
void Slice_filter::GetOutput(vtkSmartPointer<vtkPolyData> PolyData)
{
    mute.lock();
    Cutter->Update();
    PolyData->ShallowCopy(Cutter->GetOutput());
    mute.unlock();
}
void Slice_filter::Update()
{
    mute.lock();
    Cutter->SetInputData(grid_copy);
    Cutter->Update();

    toUnstrGrid->Update();

    Grid->ShallowCopy(toUnstrGrid->GetOutput());
    Mapper->Update();

    std::vector<std::string> names;
    std::vector<unsigned long> selectedComponent;
    for(unsigned long i=0; i<Field_list.size();i++)
    {
        names.push_back(Field_list[i]->Field_Name);
        selectedComponent.push_back(Field_list[i]->selectedComponent);
        delete Field_list[i];
    }
    Field_list.clear();

    for(int i=0;i<Grid->GetPointData()->GetNumberOfArrays();i++)
    {
        unsigned long j;
        vtkSmartPointer<vtkDataArray> aux = Grid->GetPointData()->GetArray(i);
        const std::string name1 = aux->GetName();
        if(name1.find("comp") !=std::string::npos)
            continue;
        for(j=0; j< Input->Field_list.size();j++)
        {
            if(Input->Field_list[j]->Field_Name ==name1)
            {
                break;
            }
        }
        if(j==Input->Field_list.size())
            continue;
        this->Field_list.push_back(new Field(aux));
    }
    for(int i =0; i<Field_list.size();i++)
    {
        for(unsigned long j=0;j<names.size();j++)
        {
            if(names[j] == Field_list[i]->Field_Name)
            {
                Field_list[i]->selectedComponent = selectedComponent[j];
                break;;
            }
        }
    }
    emit UpdateInterfaseSignal();
    SendSignalsDownStream();
    mute.unlock();
}

void Slice_filter::ShowWidgets()
{
    if(Actor->GetVisibility() && Plane->ShowOrHidePlane_CheckBox->isChecked())
        Plane->plane_Widget->On();
    if(UseScalarBar && Actor->GetVisibility())
    {
        ScalarBar->VisibilityOn();
        ScalarWidget->On();
    }
}
void Slice_filter::HideWidgets()
{
    Plane->plane_Widget->Off();
    if(UseScalarBar)
    {
        ScalarWidget->Off();
        ScalarBar->VisibilityOff();
    }
}

void Slice_filter::HideOrShow(int i)
{
    if(i==1)
    {
        Actor->VisibilityOn();
    }
    else
    {
        Actor->VisibilityOff();
    }
    Plane->HideOrShowPlane(i);
}

void Slice_filter::SetScalarBarWidget()
{
    if(Field_list.empty())
        return;
    ScalarBar = vtkSmartPointer<vtkScalarBarActor>::New();
    ScalarBar->SetNumberOfLabels(5);
    ScalarBar->SetHeight(0.4);
    ScalarBar->DragableOn();
    ScalarWidget = vtkSmartPointer<vtkScalarBarWidget>::New();
    ScalarWidget->SetScalarBarActor(ScalarBar);
    ScalarWidget->SetInteractor(this->render_Win->GetInteractor());
    UseScalarBar = true;
    ScalarBar->VisibilityOff();
    ScalarWidget->Off();
}
vtkDataSet* Slice_filter::GetGeoDataOutput()
{
    return Grid;
}

void Slice_filter::DisconectSonsWidgets()
{
    Plane->plane_Widget->RemoveObserver(callback);
    Plane->DisconectAll();
    foreach(Geometry *geo, NextFilter)
    {
        geo->DisconectSonsWidgets();
    }
}

void Slice_filter::ReconectSonsWidgets()
{
    Plane->plane_Widget->AddObserver(vtkCommand::EndInteractionEvent,callback);
    Plane->ReconectAll();
    foreach(Geometry *geo, NextFilter)
    {
        geo->ReconectSonsWidgets();
    }

}

#ifndef SLICE_FILTER_H
#define SLICE_FILTER_H

#include <QObject>

#include "geometry.h"
#include "input_handler.h"
#include "plane_widget.h"

#include <vtkCutter.h>
#include <vtkCommand.h>
#include <vtkAppendFilter.h>

class Slice_filter : public Geometry
{
    Q_OBJECT
public:
    //Constructor
    Slice_filter();
    //Destructor
    ~Slice_filter();

    /**
     * @brief Sirve como input para la clase
     * @param Source = Input
     */
    void SetInputData(Geometry *Source);
    /**
     * @brief Esconde o muestra la geometria y los widgets
     * @param i = 1 muestra, i =0 oculta
     */
    void HideOrShow(int i) override;
    /**
     * @brief Crea la pestaña de propiedades del filtro
     */
    void FilterPropConfig();
    /**
     * @brief Clase implementada para manejar el plano. Provee tanto una pestaña de propiedades como todas las clases necesarias.
     */
    Plane_Widget *Plane;
    /**
     * @brief Clase de vtk que calcula la intersección de la geometria con el plano.
     */
    vtkSmartPointer<vtkCutter> Cutter;
    /**
     * @brief filtro de vtk para pasar de vtkPolyData a vtkUnstructured grid (preferible para la pipeline).
     */
    vtkSmartPointer<vtkAppendFilter> toUnstrGrid;
    /**
     * @brief Cambia el arbol de geometrias a este paso de tiempo
     * @param i = paso de tiempo
     */
    void ChangeToThisTime(int i) override;
    /**
     * @brief GetGeoDataOutput
     * @return vtkDataSet with the output
     */
    vtkDataSet* GetGeoDataOutput() override;
    /**
     * @brief Crea una copia de la geometria dentro de GridData
     * @param GridData = Contenedor donde se va a copiar
     */
    void GetOutput(vtkSmartPointer<vtkUnstructuredGrid> GridData) override;
    /**
     * @brief Crea una copia de la geometria dentro de PolyData
     * @param PolyData = Contenedor donde se va a copiar
     */
    void GetOutput(vtkSmartPointer<vtkPolyData> PolyData) override;
    /**
     * @brief UpstreamChange. Se usa en caso ed que el input del filtro cambie. Usualmente se llama mediante "SendSignalsDownStream"
     */
    void UpstreamChange() override;
    /**
     * @brief Actualiza el filtro a nivel objeto. Por ejemplo, cuando el plano cambia de lugar.
     */
    void Update() override;
    /**
     * @brief Esconde los widgets
     */
    void HideWidgets() override;
    /**
     * @brief Muestra los widgets
     */
    void ShowWidgets() override;
    /**
     * @brief Configura la barra de escala de colores
     */
    void SetScalarBarWidget() override;
    /**
     * @brief Re escalea la solución al campo actual.
     */
    void ReScaleToCurrentData() override;
    /**
     * @brief ReconectSonsWidgets. Reconect all the widgets. Implemeted to call after update is complete.
     * This function is meant to be override for classes with widgets
     */
    virtual void ReconectSonsWidgets();
    /**
     * @brief DisconectSonsWidgets. Implemented for avoid widget interaction during update process in online mode.
     * This function is meant to be override for classes with widgets
     */
    virtual void DisconectSonsWidgets();


};

#endif // SLICE_FILTER_H

#include "stream_lines_filter.h"

//Constructor
Stream_Lines_Filter::Stream_Lines_Filter()
{
    //Inicializo las variables.-----------------------
    //StreamLines
    GeneradorDeSemillas = new SeedsWidgets;
    streamTracer = vtkSmartPointer<vtkStreamTracer>::New();
    tubes = vtkSmartPointer<vtkTubeFilter>::New();

    InputHandler = new Input_handler(1);

    FilterPropertiesConfig();
    callback = new GeoCallback;


}
//Destructor. Constultar a rene si es necesario destruir los layout creados en el cosntructor anterior. Ej.Tube_layout
Stream_Lines_Filter::~Stream_Lines_Filter()
{
    mute.lock();
    unsigned long i;
    for( i= 0 ; i< Input->NextFilter.size();i++)
    {
        if(Input->NextFilter[i]->Name == this->Name)
        {
            break;
        }
    }
    Input->NextFilter.erase(Input->NextFilter.begin()+i);
    delete GeneradorDeSemillas;
    delete Integration_properties;
    delete Direction_of_integration_properties;
    delete Times_of_integration_properties;
    delete TubesProperties;
    mute.unlock();
}

void Stream_Lines_Filter::FilterPropertiesConfig()
{

    Integration_properties = new QGroupBox;
    Integration_properties->setTitle("Integral method");
    Integration_properties->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    Integration_properties->setMinimumHeight(100);

    Direction_of_integration_properties = new QGroupBox;
    Direction_of_integration_properties->setTitle("Integral direction");
    Direction_of_integration_properties->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    Direction_of_integration_properties->setMinimumHeight(100);

    Times_of_integration_properties = new QGroupBox;
    Times_of_integration_properties->setTitle("Time step of integration method");
    Times_of_integration_properties->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    Times_of_integration_properties->setMinimumHeight(150);

    TubesProperties = new QGroupBox;
    TubesProperties->setTitle("Tube filter properties");
    TubesProperties->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    TubesProperties->setMinimumHeight(100);

    this->type = Filter_type::StreamLines;
    //------------------------------------------------

    //Configuracion de las propiedades----------------
    //Tipo de integral numerica
    QVBoxLayout *Integration_layout = new QVBoxLayout(Integration_properties);
    runge_kutta2 = new QRadioButton(Integration_properties);
    runge_kutta4 = new QRadioButton(Integration_properties);
    runge_kutta45 = new QRadioButton(Integration_properties);
    runge_kutta2->setText("Runge Kutta 2");
    runge_kutta4->setText("Runge Kutta 4");
    runge_kutta45->setText("Runge Kutta 45");
    Integration_layout->addWidget(runge_kutta2);
    Integration_layout->addWidget(runge_kutta4);
    Integration_layout->addWidget(runge_kutta45);
    runge_kutta2->setChecked(true);
    Integration_properties->setLayout(Integration_layout);

    //Dirrecion
    QVBoxLayout *Direction_layout = new QVBoxLayout(Direction_of_integration_properties);
    dFoward = new QRadioButton(Direction_of_integration_properties);
    dBackward = new QRadioButton(Direction_of_integration_properties);
    dBoth = new QRadioButton(Direction_of_integration_properties);
    dFoward->setText("Foward");
    dBackward->setText("Backward");
    dBoth->setText("Both");
    Direction_layout->addWidget(dFoward);
    Direction_layout->addWidget(dBackward);
    Direction_layout->addWidget(dBoth);
    dBoth->setChecked(true);
    Direction_of_integration_properties->setLayout(Direction_layout);


    //Time step
    QGridLayout *Time_step_layout = new QGridLayout(Times_of_integration_properties);

    StartTime_LineEdit = new QLineEdit(Times_of_integration_properties);
    QLabel *StartTime_label = new QLabel(Times_of_integration_properties);
    StartTime_LineEdit->setValidator(new QDoubleValidator(0.000000001,1000000,8,Times_of_integration_properties));
    StartTime_label->setText("Inicial Time Step");

    MinTime_LineEdit = new QLineEdit(Times_of_integration_properties);
    QLabel *MinTime_label = new QLabel(Times_of_integration_properties);
    MinTime_LineEdit->setValidator(new QDoubleValidator(0.000000001,1000000,8,Times_of_integration_properties));
    MinTime_label->setText("Min Time Step");

    MaxTime_LineEdit = new QLineEdit(Times_of_integration_properties);
    QLabel *MaxTime_label = new QLabel(Times_of_integration_properties);
    MaxTime_LineEdit->setValidator(new QDoubleValidator(0.000000001,1000000,8,Times_of_integration_properties));
    MaxTime_label->setText("Max Time Step");
    QLabel *Max_Integration_Label = new QLabel(Times_of_integration_properties);
    Max_Integration_Label->setText("Max Propagation");
    Max_Integration_LineEdit = new QLineEdit(Times_of_integration_properties);
    Max_Integration_LineEdit->setValidator(new QDoubleValidator(0,1000000000,8,Times_of_integration_properties));



    Time_step_layout->addWidget(StartTime_label,0,0,Qt::AlignLeft);
    Time_step_layout->addWidget(StartTime_LineEdit,0,1,Qt::AlignRight);
    Time_step_layout->addWidget(MinTime_label,1,0,Qt::AlignLeft);
    Time_step_layout->addWidget(MinTime_LineEdit,1,1,Qt::AlignRight);
    Time_step_layout->addWidget(MaxTime_label,2,0,Qt::AlignLeft);
    Time_step_layout->addWidget(MaxTime_LineEdit,2,1,Qt::AlignRight);
    Time_step_layout->addWidget(Max_Integration_Label,3,0);
    Time_step_layout->addWidget(Max_Integration_LineEdit,3,1);

    Times_of_integration_properties->setLayout(Time_step_layout);

    //Tube filter properties
    QGridLayout *Tube_layout = new QGridLayout(TubesProperties);

    QLabel *Tube_radio_label = new QLabel(TubesProperties);
    Tube_radio_LineEdit = new QLineEdit(TubesProperties);
    Tube_radio_LineEdit->setValidator(new QDoubleValidator(0.00000000001,100000000000,8,TubesProperties));
    Tube_radio_label->setText("Radio");

    Tube_sides_LineEdit = new QLineEdit(TubesProperties);
    QLabel *Tube_sides_label = new QLabel(TubesProperties);
    Tube_sides_LineEdit->setValidator(new QIntValidator(1,1000000,TubesProperties));
    Tube_sides_label->setText("Number of sides");

    Tube_layout->addWidget(Tube_radio_label,0,0);
    Tube_layout->addWidget(Tube_radio_LineEdit,0,1);
    Tube_layout->addWidget(Tube_sides_label,1,0);
    Tube_layout->addWidget(Tube_sides_LineEdit,1,1);

    TubesProperties->setLayout(Tube_layout);
    TubesProperties->setMaximumHeight(100);

    //-----------------------------------------------
    //Adding all the widgets to the properties layout
    QVBoxLayout *properties = new QVBoxLayout;
    InputHandler->AddQBoxToLayout(properties);
    properties->addWidget(Integration_properties);
    properties->addWidget(Direction_of_integration_properties);
    properties->addWidget(Times_of_integration_properties);
    properties->addWidget(TubesProperties);

    GeneradorDeSemillas->AddPropertiesBoxToLayout(properties);

    AddFilterProperties(properties);
    Properties_names.push_back("Filter Prop.");
}
//Input of the filter
void Stream_Lines_Filter::SetInputData(Geometry *Source)
{
    mute.lock();
    this->Times->ShallowCopy(Source->Times);
    this->CurrentTimeStep = Source->CurrentTimeStep;

    Input = Source;
    render_Win = Input->render_Win;
    Input->GetOutput(Grid);

    InputHandler->AddAllFields(Source->Field_list,Vector);
    InputHandler->SetInputName(0,"Velocity");

    InputHandler->AddItemsToComboBox(0,Vector);
    InputHandler->SetInputTimeStep(Source->CurrentTimeStep);

    InputHandler->SetSelectedInput(0);
    std::string prev_field_name = Source->Field_list[Source->SelectedField]->Field_Name;

    InputHandler->SetSelectedInput(prev_field_name);

    InputHandler->ComboBoxes[0]->setCurrentIndex(InputHandler->SelectedInput[0]);

    QComboBox::connect(InputHandler->ComboBoxes[0],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputField(QString)));

    Grid->GetPointData()->SetActiveVectors(InputHandler->GetSelectedFieldName().c_str());
    Grid->GetPointData()->SetActiveScalars(InputHandler->GetSelectedFieldMagnitudName().c_str());
    GeneradorDeSemillas->SetInteractor(this->render_Win->GetInteractor());

    GeneradorDeSemillas->WidgetConfig(Grid->GetPoints(),Grid->GetBounds(),Grid->GetLength(),Grid->GetCenter());

    this->streamTracer->SetInputData(this->Grid);
    this->streamTracer->SetSourceData(GeneradorDeSemillas->Seeds);
    this->tubes->SetInputConnection(streamTracer->GetOutputPort());


    //Se usa como paso de tiempo inicial el paso de tiempo de la solucion.
    if(Times->GetNumberOfTuples()>=2)
        InitialIntegrationStep = std::abs(Times->GetVariantValue(1).ToDouble() - Times->GetVariantValue(0).ToDouble());
    else
        InitialIntegrationStep=0.01;
    StartTime_LineEdit->setText(QString::fromStdString(std::to_string(InitialIntegrationStep)));
    MinimunIntegrationStep = InitialIntegrationStep/100;
    MinTime_LineEdit->setText(QString::fromStdString(std::to_string(MinimunIntegrationStep)));
    MaximunIntegrationStep = InitialIntegrationStep*100;
    MaxTime_LineEdit->setText(QString::fromStdString(std::to_string(MaximunIntegrationStep)));


    //Util cuando al grilla es mas o menos uniforme.
    double p1[3];
    double p2[3];
    this->Grid->GetCell(1)->GetPoints()->GetPoint(0,p1);
    this->Grid->GetCell(1)->GetPoints()->GetPoint(1,p2);

    double cell_length = ((p1[0] - p2[0]) * (p1[0] - p2[0]) + (p1[1] - p2[1]) * (p1[1] - p2[1]) +
           (p1[2] - p2[2]) * (p1[2] - p2[2]));
    cell_length =sqrt(cell_length);

    //El ancho de los tubos es una parte del ancho de una celda. Puede ser necesario cambiar si la celda es muy chica.
    radio_of_tubes = cell_length/4;

    ComputeVorticity = 1; // Se calcula la vorticidad por default
    Tube_radio_LineEdit->setText(QString::fromStdString(std::to_string(radio_of_tubes)));
    number_of_sides = 10; //default
    Tube_sides_LineEdit->setText(QString::fromStdString(std::to_string(number_of_sides)));

    auto maxVelocity = InputHandler->GetSelectedSolution()->GetMaxNorm();


    maxTime = 500 * cell_length / maxVelocity;
    Max_Integration_LineEdit->setText(QString::fromStdString(std::to_string(maxTime)));


    this->streamTracer->SetIntegrationDirectionToBoth();

    this->streamTracer->SetInitialIntegrationStep(InitialIntegrationStep);
    this->streamTracer->SetMinimumIntegrationStep(MinimunIntegrationStep);
    this->streamTracer->SetMaximumIntegrationStep(MaximunIntegrationStep);
    this->streamTracer->SetIntegratorTypeToRungeKutta2();
    this->streamTracer->SetComputeVorticity(ComputeVorticity);
    this->tubes->SetNumberOfSides(number_of_sides);
    this->tubes->SetRadius(radio_of_tubes);
    this->streamTracer->SetMaximumPropagation(maxTime);
    this->streamTracer->Update();
    this->tubes->SetRadius(radio_of_tubes);
    this->Mapper->SetInputConnection(tubes->GetOutputPort());
    this->Mapper->Update();


    //Connections // Ante un cambio en cualquiera de los items, se actualiza el filtro

    QLineEdit::connect(Tube_radio_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdateTubeRadio()));
    QLineEdit::connect(Tube_sides_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdateTubeNumberOfSides()));

    QRadioButton::connect(runge_kutta2,SIGNAL(released()),this,SLOT(UpdateTypeOfIntegration()));
    QRadioButton::connect(runge_kutta4,SIGNAL(released()),this,SLOT(UpdateTypeOfIntegration()));
    QRadioButton::connect(runge_kutta45,SIGNAL(released()),this,SLOT(UpdateTypeOfIntegration()));

    QLineEdit::connect(StartTime_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdateStartTimeOfIntegration()));
    QLineEdit::connect(MinTime_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdateMinTimeOfIntegration()));
    QLineEdit::connect(MaxTime_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdateMaxTimeOfIntegration()));

    QRadioButton::connect(dFoward,SIGNAL(released()),this,SLOT(UpdateDirectionOfIntegration()));
    QRadioButton::connect(dBackward,SIGNAL(released()),this,SLOT(UpdateDirectionOfIntegration()));
    QRadioButton::connect(dBoth,SIGNAL(released()),this,SLOT(UpdateDirectionOfIntegration()));

    QLineEdit::connect(Max_Integration_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdateMaxTime()));

    callback->Geo = this;
    GeneradorDeSemillas->SphereWidget->AddObserver(vtkCommand::EndInteractionEvent,callback);
    GeneradorDeSemillas->Line_Widget->AddObserver(vtkCommand::EndInteractionEvent,callback);
    GeneradorDeSemillas->MaskPoints->AddObserver(vtkCommand::EndEvent,callback);

    QObject::connect(GeneradorDeSemillas,&SeedsWidgets::UpdateFilter,this,&Stream_Lines_Filter::Update);

    Input->NextFilter.push_back(this);
    mute.unlock();
}


//Actualiza el radio de los tubos al que el usuario solicite.
void Stream_Lines_Filter::UpdateTubeRadio()
{
    if(Tube_radio_LineEdit->text().isEmpty())
        return;
    radio_of_tubes = std::stod(Tube_radio_LineEdit->text().toStdString());
    tubes->SetRadius(radio_of_tubes);
    tubes->Update();
}
//Actualiza el numero de lado de los tubos.
void Stream_Lines_Filter::UpdateTubeNumberOfSides()
{
    if(Tube_sides_LineEdit->text().isEmpty())
        return;
    number_of_sides = std::stoi(Tube_sides_LineEdit->text().toStdString());
    tubes->SetNumberOfSides(number_of_sides);
    tubes->Update();
}
//Actualiza el tipo de integracion del filtro StreamTracer
void Stream_Lines_Filter::UpdateTypeOfIntegration()
{
    if(runge_kutta2->isChecked())
    {
        Tipo_integral = RungeKutta2;
        this->streamTracer->SetIntegratorTypeToRungeKutta2();


    }
    else if(runge_kutta4->isChecked())
    {
        Tipo_integral = RungeKutta4;
        this->streamTracer->SetIntegratorTypeToRungeKutta4();

    }
    else
    {
        Tipo_integral = RungeKutta45;
        this->streamTracer->SetIntegratorTypeToRungeKutta45();
    }
    Update();
}
//Actualiza la direccion de la integracion del filtro StreamTracer.
void Stream_Lines_Filter::UpdateDirectionOfIntegration()
{
    if(dBoth->isChecked())
    {
        Direction = Direction_of_integration::both;
        this->streamTracer->SetIntegrationDirectionToBoth();
    }
    else if(dBackward->isChecked())
    {
        Direction = Direction_of_integration::Backward;
        this->streamTracer->SetIntegrationDirectionToBackward();
    }
    else
    {
        Direction = Direction_of_integration::Foward;
        this->streamTracer->SetIntegrationDirectionToForward();
    }
    Update();
}

//Actualiza el tiempo inicial de integracion del filtro streamTracer.
void Stream_Lines_Filter::UpdateStartTimeOfIntegration()
{
    if(StartTime_LineEdit->text().isEmpty())
        return;
    InitialIntegrationStep = std::stod(StartTime_LineEdit->text().toStdString());
    streamTracer->SetInitialIntegrationStep(InitialIntegrationStep);
    Update();
}
//Actualiza el tiempo minimo de integracion del filtro streamTracer.
void Stream_Lines_Filter::UpdateMinTimeOfIntegration()
{
    if(MinTime_LineEdit->text().isEmpty())
        return;
    MinimunIntegrationStep = std::stod(MinTime_LineEdit->text().toStdString());
    streamTracer->SetMaximumIntegrationStep(MinimunIntegrationStep);
    Update();
}
//Actualiza el tiempo maximo de integracion del filtro streamTracer.
void Stream_Lines_Filter::UpdateMaxTimeOfIntegration()
{
    if(MaxTime_LineEdit->text().isEmpty())
        return;
    MinimunIntegrationStep = std::stod(MaxTime_LineEdit->text().toStdString());
    streamTracer->SetMaximumIntegrationStep(MaximunIntegrationStep);
    Update();
}

void Stream_Lines_Filter::UpdateMaxTime()
{
    if(Max_Integration_LineEdit->text().isEmpty())
        return;
    maxTime=std::stod(Max_Integration_LineEdit->text().toStdString());
    streamTracer->SetMaximumPropagation(maxTime);
    Update();
}

void Stream_Lines_Filter::HideOrShow(int i)
{
    if(i==0)
    {
        Actor->VisibilityOff();
    }
    else
    {
        Actor->VisibilityOn();
    }
    GeneradorDeSemillas->HideOrShow(i);
}

void Stream_Lines_Filter::HideWidgets()
{
    GeneradorDeSemillas->HideOrShow(0);
}

void Stream_Lines_Filter::ShowWidgets()
{
    if(Actor->GetVisibility())
        GeneradorDeSemillas->HideOrShow(1);
}

void Stream_Lines_Filter::GetOutput(vtkSmartPointer<vtkPolyData> GridData)
{
    mute.lock();
    GridData->ShallowCopy(tubes->GetOutput());
    mute.unlock();
}
vtkDataSet* Stream_Lines_Filter::GetGeoDataOutput()
{
    //Update();
    return  Mapper->GetInput();
}

void Stream_Lines_Filter::ChangeToThisTime(int i)
{
    Input->ChangeToThisTime(i);
}

void Stream_Lines_Filter::UpstreamChange()
{
    mute.lock();

    GeneradorDeSemillas->DisconectAll();

    CurrentTimeStep=Input->CurrentTimeStep;

    QComboBox::disconnect(InputHandler->ComboBoxes[0],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputField(QString)));
    std::string prev_velocity_name = InputHandler->GetSelectedFieldName(0);

    InputHandler->RemoveItemsFromComboBox();
    InputHandler->UpdateInputHandler(Input->Field_list);
    InputHandler->AddItemsToComboBox(0,Vector);

    if(!InputHandler->SetSelectedInput(0,prev_velocity_name))
    {
        InputHandler->SetSelectedInput(0,InputHandler->GetVectorsFieldsNames()[0]);
        InputHandler->SetComboBoxIndex(0,InputHandler->GetVectorsFieldsNames()[0]);
    }
    else
    {
        InputHandler->SetComboBoxIndex(0,prev_velocity_name);
    }

    Input->GetOutput(Grid);

    Grid->GetPointData()->SetActiveVectors(InputHandler->GetSelectedFieldName().c_str());
    Grid->GetPointData()->SetActiveScalars(InputHandler->GetSelectedFieldMagnitudName().c_str());

    //streamTracer->SetInputData(Grid);

    this->streamTracer->SetSourceData(GeneradorDeSemillas->Seeds);

    //streamTracer->Update();
    tubes->SetInputConnection(streamTracer->GetOutputPort());

    //tubes->Update();

    SendSignalsDownStream();

    QComboBox::connect(InputHandler->ComboBoxes[0],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputField(QString)));

    GeneradorDeSemillas->Reconect();

    mute.unlock();


}

void Stream_Lines_Filter::Update()
{

    mute.lock();

    //streamTracer->SetInputData(Grid);
    this->streamTracer->SetSourceData(GeneradorDeSemillas->Seeds);
    streamTracer->Update();

    tubes->Update();

    SendSignalsDownStream();

    mute.unlock();
}

void Stream_Lines_Filter::ChangeField(std::string field_name)
{
    InputHandler->SetSelectedInput(field_name);
    Grid->GetPointData()->SetActiveVectors(InputHandler->GetSelectedFieldName().c_str());
    Grid->GetPointData()->SetActiveScalars(InputHandler->GetSelectedFieldMagnitudName().c_str());
    tubes->Update();
}

void Stream_Lines_Filter::ChangeInputField(QString name)
{
    ChangeField(name.toStdString());
}

void Stream_Lines_Filter::ReScaleToCurrentData()
{
    if(Grid->GetPointData()->GetNumberOfArrays() ==0)
        return;
    Mapper->SetScalarRange(InputHandler->GetSelectedSolutionMagnitud()->GetRange());
    Mapper->Update();
}
void Stream_Lines_Filter::GetOutput(vtkSmartPointer<vtkUnstructuredGrid> GridData)
{
    mute.lock();
    vtkSmartPointer<vtkAppendFilter> toUnstrGrid = vtkSmartPointer<vtkAppendFilter>::New();
    toUnstrGrid->SetInputConnection(tubes->GetOutputPort());
    toUnstrGrid->Update();
    GridData->ShallowCopy(toUnstrGrid->GetOutput());
    mute.unlock();
}


#ifndef STREAM_LINES_FILTER_H
#define STREAM_LINES_FILTER_H

#include "geometry.h"
#include "seedswidgets.h"
#include "input_handler.h"

#include <vtkPolyData.h>
#include <vtkMaskPoints.h>
#include <vtkStreamTracer.h>
#include <vtkTubeFilter.h>
#include <vtkPolyPointSource.h>
#include <vtkRenderWindow.h>

#include <QLabel>
#include <QRadioButton>
#include <QLineEdit>
#include <QGridLayout>
#include <QCheckBox>
//Tipos de integrales aplicadas en el filtro
enum Integration_type
{
    RungeKutta2,
    RungeKutta4,
    RungeKutta45
};

//Direcciones de la integracion temporal.
enum Direction_of_integration
{
    Foward,
    Backward,
    both
};
//Filtro de Lineas de corriente. Crea las lineas utilizando como puntos de muestra algunos puntos de la grilla.
class Stream_Lines_Filter : public Geometry
{
    Q_OBJECT

public:
    //Base constructor
    Stream_Lines_Filter();
    //Destructor
    ~Stream_Lines_Filter();
    /**
     * @brief Sirve como input para la clase
     * @param Source = Input
     */
    void SetInputData(Geometry *Source);
    /**
     * @brief Esconde o muestra la geometria y los widgets
     * @param i = 1 muestra, i =0 oculta
     */
    void HideOrShow(int i) override;
    /**
     * @brief Esconde los widgets
     */
    void HideWidgets() override;
    /**
     * @brief Muestra los widgets
     */
    void ShowWidgets() override;
    /**
     * @brief Cambia el arbol de geometrias a este paso de tiempo
     * @param i = paso de tiempo
     */
    void ChangeToThisTime(int i) override;
    /**
     * @brief Change the field which is used to calculate the streamlines
     * @param field_name
     */
    void ChangeField(std::string field_name) override;
    //Generador de semillas
    /**
     * @brief Clase implementada para generar las semillas de calculo.
     */
    SeedsWidgets *GeneradorDeSemillas;
    /**
     * @brief Crea una copia de la geometria dentro de GridData
     * @param GridData = Contenedor donde se va a copiar
     */
    void GetOutput(vtkSmartPointer<vtkPolyData> GridData) override;
    /**
     * @brief GetGeoDataOutput
     * @return vtkDataSet with the output
     */
    vtkDataSet* GetGeoDataOutput() override;
    /**
     * @brief UpstreamChange. Se usa en caso ed que el input del filtro cambie. Usualmente se llama mediante "SendSignalsDownStream"
     */
    void UpstreamChange() override;
    /**
     * @brief Actualiza el filtro a nivel objeto.
     */
    void Update() override;
    /**
     * @brief Re escalea la solución al campo actual.
     */
    void ReScaleToCurrentData() override;
    /**
     * @brief GetOutput
     * @param return the output as a vtkUnstructuredGrid
     */
    virtual void GetOutput(vtkSmartPointer<vtkUnstructuredGrid> GridData);

    //Slots para hacer las conexiones con Qt

private:
    /**
     * @brief Clase para manejar los distintos inputs. Controla inputs vectoriales en este caso
     */
    Input_handler *InputHandler;

    //Filtros de vtk Necesarios
    /**
     * @brief streamTracer. clase de vtk que se encarga de calcular las lineas de corriente
     */
    vtkSmartPointer<vtkStreamTracer> streamTracer;
    /**
     * @brief tubes. Filtro de vtk para cambiar la visualización de lineas a tubos
     */
    vtkSmartPointer<vtkTubeFilter> tubes;
    /**
     * @brief Crea la pestaña de propiedades del filtro
     */
    void FilterPropertiesConfig();

    //Variables definidas para crear la pestaña de propiedades del filtro.
    //Se dividen en QGroupBox segun el tipo de propiedades.
    QGroupBox *Integration_properties;
    QGroupBox *Direction_of_integration_properties;
    QGroupBox *Times_of_integration_properties;
    QGroupBox *TubesProperties;

    //Se definen las variables de las propiedades y los LineEdit necesarios para hacer las conexiones con Qt y actualizar los filtros.
    //Sample ratio of MaskPoints.

    //Tiempos de integracion de los StreamTracer.
    double InitialIntegrationStep;
    QLineEdit *StartTime_LineEdit;
    double MinimunIntegrationStep;
    QLineEdit *MinTime_LineEdit;
    double MaximunIntegrationStep;
    QLineEdit *MaxTime_LineEdit;
    double maxTime;
    QLineEdit *Max_Integration_LineEdit;

    //Calcular o no la vorticidad en el filtro StreamLines. Cambian un poco segun se activa o no. Falta implementar su cambio.
    int ComputeVorticity = 1; // True

    //Variable usadas para el tipo de integracion
    Integration_type Tipo_integral = RungeKutta2;
    QRadioButton *runge_kutta2;
    QRadioButton *runge_kutta4;
    QRadioButton *runge_kutta45;

    //Variables utilizadas para la direccion de la integracion
    Direction_of_integration Direction = both;
    QRadioButton *dFoward;
    QRadioButton *dBackward;
    QRadioButton *dBoth;

    //Variables para el filtro tubes.
    double radio_of_tubes; // Radio de los tubos
    QLineEdit *Tube_radio_LineEdit;
    double number_of_sides; // Numbero de lados == resolucion de los tubos.
    QLineEdit *Tube_sides_LineEdit;


private slots:
    /**
     * @brief Actualiza el radio de los tubos.
     */
    virtual void UpdateTubeRadio();
    /**
     * @brief Actualiza el numero de lados de los tubos
     */
    virtual void UpdateTubeNumberOfSides();
    /**
     * @brief Actualiza el tipo de integracion
     */
    virtual void UpdateTypeOfIntegration();
    /**
     * @brief Actualiza la direccion de la integracion
     */
    virtual void UpdateDirectionOfIntegration();
    //Actualiza los tiempos de integracion-----
    /**
     * @brief Actualiza el paso de tiempo inicial de calculo
     */
    virtual void UpdateStartTimeOfIntegration();//incial step time
    /**
     * @brief Actualiza el paso de tiempo minimo de calculo
     */
    virtual void UpdateMinTimeOfIntegration(); //min step time
    /**
     * @brief Actualiza el paso de tiempo maximo de calculo
     */
    virtual void UpdateMaxTimeOfIntegration(); //max step time
    /**
     * @brief Actualiza la maxima propagación de las lineas de corriente
     */
    virtual void UpdateMaxTime(); //Rango de la integracion
    //-----------------------------------------
    /**
     * @brief ChangeInputField
     * @param name
     */
    virtual void ChangeInputField(QString name);
};

#endif // STREAM_LINES_FILTER_H

#include "tensor.h"


//TENSOR CLASS-------------------------------------------------------------
//Base constructur
Tensor::Tensor()
{
}
Tensor::~Tensor()
{
}
//Field constructor
Tensor::Tensor(vtkSmartPointer<vtkUnstructuredGrid> grid, vtkSmartPointer<vtkDoubleArray> Vector)
{
    //Esto esta hecho asi porque el filtro de gradientes de vtk solo admite scalares
    //Ademas solo computa el filtro del ultimo campo scalar ingresado
    int i, j,k;

    std::vector<vtkSmartPointer<vtkDoubleArray>> Vector_field;
    for(i =0 ; i<Vector->GetNumberOfComponents();i++)
    {
        vtkSmartPointer<vtkDoubleArray> vector_component = vtkSmartPointer<vtkDoubleArray>::New();
        std::string name = "component_" + std::to_string(i);
        vector_component->SetNumberOfComponents(1);
        vector_component->SetName(name.c_str());
        Vector_field.push_back(vector_component);
    }
    vtkSmartPointer<vtkUnstructuredGrid> grid_copy = vtkSmartPointer<vtkUnstructuredGrid>::New();
    grid_copy->CopyStructure(grid);
    double *auxiliar;
    for (i = 0; i < Vector->GetNumberOfTuples(); i++)
    {
      auxiliar = Vector->GetTuple(i);
      for(j=0; j <Vector->GetNumberOfComponents();j++ )
      {
          Vector_field[j]->InsertNextValue(auxiliar[j]);
      }
    }
    std::vector<vtkSmartPointer<vtkDataArray>> Gradients;
    vtkSmartPointer<vtkGradientFilter> Vel_gradient_Filter = vtkSmartPointer<vtkGradientFilter>::New();
    Vel_gradient_Filter->ComputeDivergenceOff();
    Vel_gradient_Filter->ComputeGradientOn();
    Vel_gradient_Filter->ComputeVorticityOff();
    Vel_gradient_Filter->ComputeQCriterionOff();
    for(i=0; i<Vector->GetNumberOfComponents(); i++)
    {
        std::string gradient_name = std::to_string(i) + "_gradient";
        grid_copy->GetPointData()->SetScalars(Vector_field[i]);
        Vel_gradient_Filter->SetInputData(grid_copy);
        Vel_gradient_Filter->ComputeGradientOn();
        Vel_gradient_Filter->SetResultArrayName(gradient_name.c_str());
        Vel_gradient_Filter->Update();

        vtkSmartPointer<vtkDataArray> i_gradient = vtkSmartPointer<vtkDataArray>(Vel_gradient_Filter->GetOutput()->GetPointData()->GetArray(gradient_name.c_str()));
        Gradients.push_back(i_gradient);
    }

    //Construimos el tensor
    for (i = 0; i < Vector->GetNumberOfTuples(); i++)
    {
      std::vector<std::vector<double>> Auxiliar;
      Auxiliar.resize(Vector->GetNumberOfComponents());
      for(k =0; k <Vector->GetNumberOfComponents(); k++)
      {
          double *aux = Gradients[k]->GetTuple(i);
          for(j=0; j <Vector->GetNumberOfComponents(); j++)
          {
              Auxiliar[k].push_back(aux[j]);
          }
      }
      Values.push_back(Auxiliar);
    }
    if(this->Is2D(Vector))
        dimension=2;
    else
        dimension=3;
}

Tensor::Tensor(vtkSmartPointer<vtkUnstructuredGrid> grid, vtkDataArray* Vector)
{
    //Paso el vtkDataArray a vtkDoubleArray y llamo a CalculatedTensor
    int i;

    vtkSmartPointer<vtkDoubleArray> aux_array = vtkSmartPointer<vtkDoubleArray>::New();
    aux_array->SetName(Vector->GetName());
    aux_array->SetNumberOfComponents(Vector->GetNumberOfComponents());

    for(i=0;i<Vector->GetNumberOfTuples();i++)
    {
        double *aux;
        aux = Vector->GetTuple(i);
        aux_array->InsertNextTuple(aux);
    }

    CalculateTensor(grid,aux_array);
}
//Similar to field constructor
void Tensor::CalculateTensor(vtkSmartPointer<vtkUnstructuredGrid> grid, vtkSmartPointer<vtkDoubleArray> Vector)
{
    //Esto esta hecho asi porque el filtro de gradientes de vtk solo admite scalares
    //Ademas solo computa el filtro del ultimo campo scalar ingresado
    int i, j,k;

    std::vector<vtkSmartPointer<vtkDoubleArray>> Vector_field;
    for(i =0 ; i<Vector->GetNumberOfComponents();i++)
    {
        vtkSmartPointer<vtkDoubleArray> vector_component = vtkSmartPointer<vtkDoubleArray>::New();
        std::string name = "component_" + std::to_string(i);
        vector_component->SetNumberOfComponents(1);
        vector_component->SetName(name.c_str());
        Vector_field.push_back(vector_component);
    }
    vtkSmartPointer<vtkUnstructuredGrid> grid_copy = vtkSmartPointer<vtkUnstructuredGrid>::New();
    grid_copy->CopyStructure(grid);
    double *auxiliar;
    for (i = 0; i < Vector->GetNumberOfTuples(); i++)
    {
      auxiliar = Vector->GetTuple(i);
      for(j=0; j <Vector->GetNumberOfComponents();j++ )
      {
          Vector_field[j]->InsertNextValue(auxiliar[j]);
      }
    }
    std::vector<vtkSmartPointer<vtkDataArray>> Gradients;
    vtkSmartPointer<vtkGradientFilter> Vel_gradient_Filter = vtkSmartPointer<vtkGradientFilter>::New();

    for(i=0; i<Vector->GetNumberOfComponents(); i++)
    {
        std::string gradient_name = std::to_string(i) + "_gradient";
        grid_copy->GetPointData()->SetScalars(Vector_field[i]);
        Vel_gradient_Filter->SetInputData(grid_copy);
        Vel_gradient_Filter->ComputeGradientOn();
        Vel_gradient_Filter->SetResultArrayName(gradient_name.c_str());
        Vel_gradient_Filter->Update();

        vtkSmartPointer<vtkDataArray> i_gradient = vtkSmartPointer<vtkDataArray>(Vel_gradient_Filter->GetOutput()->GetPointData()->GetArray(gradient_name.c_str()));
        Gradients.push_back(i_gradient);
    }

    //Construimos el tensor
    for (i = 0; i < Vector->GetNumberOfTuples(); i++)
    {
      std::vector<std::vector<double>> Auxiliar;
      Auxiliar.resize(Vector->GetNumberOfComponents());
      for(k =0; k <Vector->GetNumberOfComponents(); k++)
      {
          double *aux = Gradients[k]->GetTuple(i);
          //Esto presupone que si la geometria es 2D. La cordenada que sobra siempre es la Z.
          for(j=0; j <Vector->GetNumberOfComponents(); j++)
          {
              Auxiliar[k].push_back(aux[j]);
          }
      }
      Values.push_back(Auxiliar);
    }
}
std::vector<std::complex<double>> Tensor::Autovalores_2x2(std::vector<double> coef)
{
    double a = coef[0], b = coef[1], c = coef[2];
    std::vector<std::complex<double>> result;
    result.resize(2);
    result[0] = (-b +std::__complex_sqrt(std::complex<double>(b*b-4*a*c)))/(2*a);
    result[1] = (-b -std::__complex_sqrt(std::complex<double>(b*b-4*a*c)))/(2*a);
    return result;
}
std::vector<std::complex<double>> Tensor::Autovalores_3x3(std::vector<double> coef_of_pol)
{
    std::vector<std::complex<double>> result;
    result.resize(3);
    double a = coef_of_pol[0];
    double b = coef_of_pol[1];
    double c = coef_of_pol[2];
    double d = coef_of_pol[3];

    //double discriminante = 18 * a * b * c * d - 4 * b * b * b * d + b * b * c * c - 4 * a * c * c * c - 27 * a * a * d * d;

    std::complex<double> delta_uno = b*b-3*a*c;
    std::complex<double> delta_dos = 2*b*b*b -9*a*b*c + 27*a*a*d;
    std::complex<double> un_tercio = 1.0/3.0;

    std::complex<double> W = std::__complex_pow((delta_dos+std::__complex_sqrt(delta_dos*delta_dos-delta_uno*delta_uno*delta_uno*4.0))/2.0,un_tercio);
    if(W ==std::complex<double>(0,0))
    {
      W = std::__complex_pow((delta_dos-std::__complex_sqrt(delta_dos*delta_dos-delta_uno*delta_uno*delta_uno*4.0))/2.0,un_tercio);
    }
    std::complex<double> omega = std::complex<double>(-1.0/2.0 , sqrt(3.0)/2.0);

    result[0] = -un_tercio*(b+W+delta_uno/W)/a;
    result[1] = -un_tercio*(b+W*omega + delta_uno/(W*omega))/a;
    result[2] = -un_tercio*(b+W*omega*omega + delta_uno/(W*omega*omega))/a;

    return result;

}
void Tensor::AddEigenVal(std::vector<std::vector<std::complex<double>>> &Eigenvals,const std::vector<std::complex<double>> &Autovalor)
{
    Eigenvals.push_back(Autovalor);
}


bool Tensor::Is2D(vtkSmartPointer<vtkDoubleArray> vector)
{
    unsigned long number_of_comp = vector->GetNumberOfComponents();
    unsigned long number_of_tuples = vector->GetNumberOfTuples();
    unsigned long i,j;
    double *aux;
    int a[number_of_comp];
    for(i=0;i<number_of_comp;i++)
    {
        a[i]=0;
        aux= vector->GetTuple(0);
        for(j=0;j<number_of_tuples;j++)
        {
            aux = vector->GetTuple(j);
            if(std::abs(aux[i])>=1e-16)
            {
                a[i]=1;
                break;
            }
        }
        if(a[i]==0)
            return true;
    }
    return false;
}
void Tensor::CalculateAllSimAsimEigenVals()
{
    if(AutovaloresSimAsim.size()!=0) //Ya esta calculado
        return;

    //CalculateSimAndAsimMatrix();
    //CalculateSimCuadradoAndAsimCuadrado();

    SimCuadPlusAsimCuad = QtConcurrent::blockingMappedReduced(Values,&Tensor::CalculateSCyAC,&Tensor::reduceMatrix,QtConcurrent::ReduceOption::OrderedReduce);

    AutovaloresSimAsim = QtConcurrent::blockingMappedReduced(SimCuadPlusAsimCuad,&Tensor::SimCuadradoPlusAsimCuadradoEigenVals,&Tensor::AddEigenVal,QtConcurrent::ReduceOption::OrderedReduce);
    /*
    unsigned long i;
    for(i=0;i<SimCuadPlusAsimCuad.size();i++)
    {
        std::vector<std::complex<double>> aux = CalculateSimCuadradoPlusAsimCuadradoEigenVals(i);
        AutovaloresSimAsim.push_back(aux);
    }
    */
}

void Tensor::CalculateAllNormalEigenVals()
{
    if(AutovaloresTensor.size()!=0)
        return;

    if(dimension==2)
    {
        AutovaloresTensor = QtConcurrent::blockingMappedReduced(Values,&Tensor::Autovalores2D,&Tensor::AddEigenVal,QtConcurrent::ReduceOption::OrderedReduce);
    }
    else if (dimension ==3)
    {
        AutovaloresTensor = QtConcurrent::blockingMappedReduced(Values,&Tensor::Autovalores3D,&Tensor::AddEigenVal,QtConcurrent::ReduceOption::OrderedReduce);
    }
    /*
    unsigned long i;
    for(i=0;i<Values.size();i++)
    {
        std::vector<std::complex<double>> aux = Autovalores(i);
        AutovaloresTensor.push_back(aux);
    }
    */
}

std::vector<std::complex<double>> Tensor::Autovalores2D(const std::vector<std::vector<double>> &a)
{
    std::vector<double> coef = Coef_Ec_caracteristica_2x2(a);
    return Autovalores_2x2(coef);
}
std::vector<std::complex<double>> Tensor::Autovalores3D(const std::vector<std::vector<double>> &a)
{
    std::vector<double> coef = Coef_Ec_caracteristica_3x3(a);
    return Autovalores_3x3(coef);
}

std::vector<double> Tensor::Coef_Ec_caracteristica_2x2(const std::vector<std::vector<double>> &valor)
{
    std::vector<double> result;
    double a = valor[0][0], b = valor[0][1];
    double c = valor[1][0], d = valor[1][1];
    result.push_back(1);
    result.push_back(-(a + d));
    result.push_back(a * d - b * c);

    return result;
}
std::vector<double> Tensor::Coef_Ec_caracteristica_3x3(const std::vector<std::vector<double>> &valor)
{
    //defino con letras para facilitar la escritura
    double a = valor[0][0], b = valor[0][1], c = valor[0][2];
    double d = valor[1][0], e = valor[1][1], f = valor[1][2];
    double g = valor[2][0], h = valor[2][1], i = valor[2][2];
    std::vector<double> result;
    result.resize(4);
    result[0] = -1;
    result[1] = a + e + i;
    result[2] = c * g + f * h + b * d - a * i - e * i - a * e;
    result[3] = a * e * i + d * h * c + g * b * f - c * e * g - f * h * a - i * b * d;

    return result;
}

std::vector<std::complex<double>> Tensor::SimCuadradoPlusAsimCuadradoEigenVals(const std::vector<std::vector<double>> &a)
{
    std::vector<double> coef = Coef_Ec_caracteristica_3x3(a);
    std::vector<std::complex<double>> EigenVals = Autovalores_3x3(coef);

    //Los ordeno ya que lambda_1 >lambda_2 > lambda_3
    for(int k =0; k<2;k++)
    {
        if(EigenVals[k].real()<EigenVals[k+1].real())
        {
            std::complex<double> aux = EigenVals[k];
            EigenVals[k] = EigenVals[k+1];
            EigenVals[k+1] = aux;
            k=-1;
        }
    }
    return EigenVals;
}

std::vector<std::vector<double>> Tensor::CalculateSCyAC(std::vector<std::vector<double>> Val)
{
    unsigned long j, k, l;
    std::vector<std::vector<double>> SimAux;
    std::vector<std::vector<double>> ASimAux;
    for(j=0;j<Val.size();j++)
    {
        std::vector<double> auxSimRow;
        std::vector<double> auxAsimRow;
        for(k=0;k<Val[j].size();k++)
        {
            auxSimRow.push_back((Val[j][k]+Val[k][j])/2);
            auxAsimRow.push_back((Val[j][k]-Val[k][j])/2);
        }
        SimAux.push_back(auxSimRow);
        ASimAux.push_back(auxAsimRow);
    }
    std::vector<std::vector<double>> Aux;
    for(j=0;j<SimAux.size();j++)
    {
        std::vector<double> auxRow;
        for(k=0;k<SimAux[j].size();k++)
        {
            double a1=0;
            for(l=0;l<SimAux[j].size();l++)
            {
                a1 += SimAux[j][l]*SimAux[l][k] + ASimAux[j][l]*ASimAux[l][k];
            }
            auxRow.push_back(a1);
        }
        Aux.push_back(auxRow);
    }
    return Aux;
}

void Tensor::reduceMatrix(std::vector<std::vector<std::vector<double>>> &Matrix,const  std::vector<std::vector<double>> &valor)
{
    Matrix.push_back(valor);
}

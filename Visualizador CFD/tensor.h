#ifndef TENSOR_H
#define TENSOR_H


#include "field.h"
#include "geometry.h"
#include <complex>
#include <vector>
#include <QtConcurrent/QtConcurrentMap>
#include <QtConcurrent/QtConcurrent>
#include <QtConcurrent/QtConcurrentRun>
#include <vtkGradientFilter.h>

//Guarda los valores del tensor para cada punto de la grilla
//Ademas permite calcular los autovalores, las partes simetricas y antisimetricas, y los autovalroes de S^2+A^2
class Tensor
{
public:
    Tensor();
    Tensor(vtkSmartPointer<vtkUnstructuredGrid> grid, vtkSmartPointer<vtkDoubleArray> Vector);
    Tensor(vtkSmartPointer<vtkUnstructuredGrid> grid, vtkDataArray *Vector);
    ~Tensor();
    /**
     * @brief CalculateTensor. Calcula el tensor gradiente del vector y lo guarda en "Values"
     * @param grid
     * @param Vector
     */
    void CalculateTensor(vtkSmartPointer<vtkUnstructuredGrid> grid, vtkSmartPointer<vtkDoubleArray> Vector);

    //Necesario para saber si la geometria es 2D o 3D. Necesito hacerlo de esta forma debido a que los dos solvers
    //utilizan geometrias distintas. Ademas OpenFoam utiliza geometrias 3D para representar casos 2D.
    int dimension; // 2 o 3

    // tensor de 3x3 o 2x2 segun el input sea 3D o 2D para todos los puntos de la grilla.
    // solo para un paso de tiempo
    std::vector<std::vector<std::vector<double>>> Values;
    /**
     * @brief Autovalores del Tensor
     */
    std::vector<std::vector<std::complex<double>>> AutovaloresTensor;

    //Algunas cosas para implementar el metodo lambda2
    /**
     * @brief Matrix S^2 + Omega^2 para todos los puntos
     */
    std::vector<std::vector<std::vector<double>>> SimCuadPlusAsimCuad;
    /**
     * @brief Autovalores de la matriz Matrix S^2 + Omega^2 para todos los puntos
     */
    std::vector<std::vector<std::complex<double>>> AutovaloresSimAsim;

    bool Is2D(vtkSmartPointer<vtkDoubleArray> vector);
    /**
     * @brief Calcula los autovalores del tensor gradiente y los guarda en AutovaloresTensor
     */
    void CalculateAllNormalEigenVals();
    /**
     * @brief Calcula los autovalores de S^2 + Omega^2 y los guarda en AutovaloresSimAsim
     */
    void CalculateAllSimAsimEigenVals();




    //------------------------------------------------------------------------------------------
    //Metodos staticos para utiliar Qt::concurrent (trabaja en paralelo y acelera el cálculo)
private:

    //Paralel functions for enhanced Swirling method
    /**
     * @brief Calcula el autovalor de la matriz a. Sirve para el caso 2D
     * @param a == tensor gradiente en un punto
     * @return autovalor del tensor gradiente
     */
    static std::vector<std::complex<double>> Autovalores2D(const std::vector<std::vector<double>> &a);
    /**
     * @brief Calcula el autovalor de la matriz a. Sirve para el caso 3D
     * @param a == tensor gradiente en un punto
     * @return autovalor del tensor gradiente
     */
    static std::vector<std::complex<double>> Autovalores3D(const std::vector<std::vector<double>> &a);
    /**
     * @brief AddEigenVal. Reduce function for Qt::current
     * @param Eigenvals = contenedor donde se guardaran los autovalores
     * @param Autovalor a guardar
     */
    static void AddEigenVal(std::vector<std::vector<std::complex<double>>> &Eigenvals,const  std::vector<std::complex<double>> &Autovalor);


    //Paralel functions for lambda 2
    /**
     * @brief Calcula el autovalor de matriz S^2 + Omega^2
     * @param a = matriz S^2 + Omega^2
     * @return autovalor complejo
     */
    static std::vector<std::complex<double>> SimCuadradoPlusAsimCuadradoEigenVals(const std::vector<std::vector<double>> &a);
    /**
     * @brief reduceMatrix. Reduce function for Qt::current
     * @param Matrix == contenedor de matrices
     * @param valor == matriz a guardar
     */
    static void reduceMatrix(std::vector<std::vector<std::vector<double>>> &Matrix,const  std::vector<std::vector<double>> &valor);
    /**
     * @brief Calcula la matriz S^2 + Omega^2 para el tensor argumento
     * @param Val = tensor gradiente
     * @return matriz S^2 + Omega^2
     */
    static std::vector<std::vector<double>> CalculateSCyAC(std::vector<std::vector<double>> Val);


    //Parallel functions for eigenvals
    /**
     * @brief calcula los coeficientes de la ec caracteristica para un tensor de 2x2
     * @param tensor
     * @return coeficientes
     */
    static std::vector<double> Coef_Ec_caracteristica_2x2(const std::vector<std::vector<double>> &a);
    /**
     * @brief calcula los coeficientes de la ec caracteristica para un tensor de 3x3
     * @param tensor
     * @return coeficientes
     */
    static std::vector<double> Coef_Ec_caracteristica_3x3(const std::vector<std::vector<double>> &a);

    /**
     * @brief calcula los autovalores para un tensor de 2x2
     * @param coeficientes ec caracteristica
     * @return autovalores
     */
    static std::vector<std::complex<double>> Autovalores_2x2(std::vector<double> coef);
    /**
     * @brief calcula los autovalores para un tensor de 3x3
     * @param coeficientes ec caracteristica
     * @return autovalores
     */
    static std::vector<std::complex<double>> Autovalores_3x3(std::vector<double> coef); // a b c d


};
#endif // TENSOR_H

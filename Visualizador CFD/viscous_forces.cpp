#include "viscous_forces.h"

//--------------------- Constructors and destructors -----------------------------
Viscous_Forces::Viscous_Forces()
{
    nu =0.01; //Agua, se puede cambiar. Solo para probar
    referencePresure = 0;

    OuterSurface = vtkSmartPointer<vtkGeometryFilter>::New();
    grid_copy = vtkSmartPointer<vtkUnstructuredGrid>::New();
    SurfaceNormals = vtkSmartPointer<vtkPolyDataNormals>::New();
    PolyDataOutput = vtkSmartPointer<vtkPolyData>::New();
    inputHandler = new Input_handler(2);
    UsePresureField= true;
    FilterPropertiesConfig();
}

Viscous_Forces::~Viscous_Forces()
{
    mute.lock();
    unsigned long i;
    for( i= 0 ; i< Input->NextFilter.size();i++)
    {
        if(Input->NextFilter[i]->Name == this->Name)
        {
            break;
        }
    }
    Input->NextFilter.erase(Input->NextFilter.begin()+i);
    delete inputHandler;
    delete calculated_tensor;
    delete Parameters_Box;
    for(unsigned long i=0;i<Field_list.size();i++)
    {
        delete Field_list[i];
    }
    Field_list.clear();
    mute.unlock();
}

//------------------- End of constructors and destructors ------------------------
//--------------------------- Necessary Functions --------------------------------
void Viscous_Forces::SetInputData(Geometry *Source)
{
    mute.lock();
    this->Times->ShallowCopy(Source->Times);

    Input = Source;
    render_Win = Source->render_Win;
    this->type = ViscousForces;
    this->Name = Source->Name + "/Viscous Forces";


    inputHandler->SetInputTimeStep(Source->CurrentTimeStep);
    inputHandler->AddAllFields(Source->Field_list);
    inputHandler->SetInputName(0,"Velocity");
    inputHandler->AddItemsToComboBox(0,Vector);
    inputHandler->SetInputName(1,"Presure");
    inputHandler->AddItemsToComboBox(1,Scalar);


    inputHandler->SetSelectedInput(0,inputHandler->GetVectorsFieldsNames()[0]);
    inputHandler->SetComboBoxIndex(0,inputHandler->GetVectorsFieldsNames()[0]);
    if(inputHandler->GetScalarFieldsNames().size()==0)
    {
        //NO hay campos scalares-->no hay campo de presiones.
        UsePresureField=false;
    }
    if(UsePresureField)
    {
        inputHandler->SetSelectedInput(1,inputHandler->GetScalarFieldsNames()[0]);
        inputHandler->SetComboBoxIndex(1,inputHandler->GetScalarFieldsNames()[0]);
    }
    this->SelectedField = 0;
    this->CurrentTimeStep = Source->CurrentTimeStep;
    inputHandler->SetInputTimeStep(CurrentTimeStep);

    Input->GetOutput(grid_copy);

    OuterSurface->SetInputData(grid_copy);
    OuterSurface->Update();

    SurfaceNormals->SetInputConnection(OuterSurface->GetOutputPort());
    SurfaceNormals->ComputeCellNormalsOff();
    SurfaceNormals->ComputePointNormalsOn();
    SurfaceNormals->Update();

    PolyDataOutput->CopyStructure(OuterSurface->GetOutput());

    CalculateForcesField();

    Mapper->SetInputData(PolyDataOutput);
    Mapper->SetScalarModeToUsePointData();
    Mapper->ScalarVisibilityOn();
    Mapper->Update();

    Mapper->SetScalarRange(PolyDataOutput->GetPointData()->GetArray(0)->GetRange());

    QComboBox::connect(inputHandler->ComboBoxes[0],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputVelocity(QString)));
    QComboBox::connect(inputHandler->ComboBoxes[1],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputPresure(QString)));
    QLineEdit::connect(Nu_LineEdit,SIGNAL(editingFinished()),this,SLOT(ChangeNu()));
    QLineEdit::connect(Presure_LineEdit,SIGNAL(editingFinished()),this,SLOT(ChangeReferencePresure()));
    Input->NextFilter.push_back(this);

    mute.unlock();

}

void Viscous_Forces::ChangeField(std::string Field_name)
{
    for(unsigned long i=0;i<Field_list.size();i++)
    {
        if(Field_list[i]->Field_Name==Field_name)
        {
            SelectedField=i;
            SetVectors();
            return;
        }
    }

}

void Viscous_Forces::ChangeToThisTime(int i)
{
    Input->ChangeToThisTime(i);

}

void Viscous_Forces::SetScalar()
{
    if(Field_list.size()==0)
        return;
    if(Field_list[SelectedField]->type==Solid)
        return;
    Mapper->ScalarVisibilityOn();
    PolyDataOutput->GetPointData()->SetActiveScalars(Field_list[SelectedField]->GetCurrentComponentName().c_str());
    Mapper->SetScalarRange(PolyDataOutput->GetPointData()->GetScalars()->GetRange());
    if(UseScalarBar)
    {
        ScalarBar->SetTitle(Field_list[SelectedField]->GetCurrentComponentName().c_str());

        if(Actor->GetVisibility())
        {
            ScalarBar->VisibilityOn();
            ScalarWidget->On();
            ScalarBar->SetLookupTable(Mapper->GetLookupTable());
        }
    }
    Mapper->Update();
}

void Viscous_Forces::SetVectors()
{
    if(Field_list.size()==0)
        return;
    if(Field_list[SelectedField]->type == Vector)
    {
        PolyDataOutput->GetPointData()->SetActiveVectors(Field_list[SelectedField]->Field_Name.c_str());
    }
    else if(Field_list[SelectedField]->type == Scalar)
    {

    }
    else
    {
        Mapper->ScalarVisibilityOff();
        Mapper->Update();
        if(UseScalarBar)
        {
            ScalarBar->VisibilityOff();
            ScalarWidget->Off();
        }
        return;
    }
    SetScalar();

}

void Viscous_Forces::AddFields(int Field_number)
{
    if(Field_list.empty())
        return;
    PolyDataOutput->GetPointData()->AddArray(Field_list[Field_number]->Solution[0]);
    PolyDataOutput->GetPointData()->AddArray(Field_list[Field_number]->Solution_Magnitud[0]);
    if(!Field_list[Field_number]->Solution_components.empty())
    {
        for(int i=0; i<(int)Field_list[Field_number]->Solution_components[0].size();i++)
        {
            PolyDataOutput->GetPointData()->AddArray(Field_list[Field_number]->Solution_components[0][i]);
        }
    }
}
void Viscous_Forces::Update()
{
    mute.lock();
    CalculateForcesField();
    SendSignalsDownStream();
    mute.unlock();
}

void Viscous_Forces::GetOutput(vtkSmartPointer<vtkPolyData> GridData)
{
    mute.lock();
    GridData->ShallowCopy(PolyDataOutput);
    mute.unlock();

}
void Viscous_Forces::GetOutput(vtkSmartPointer<vtkUnstructuredGrid> GridData)
{
    mute.lock();
    vtkSmartPointer<vtkAppendFilter> toUGrid = vtkSmartPointer<vtkAppendFilter>::New();
    toUGrid->SetInputData(PolyDataOutput);
    toUGrid->Update();
    GridData->ShallowCopy(toUGrid->GetOutput());
    mute.unlock();
}

vtkDataSet* Viscous_Forces::GetGeoDataOutput()
{
    return PolyDataOutput;
}

void Viscous_Forces::UpstreamChange()
{
    mute.lock();
    CurrentTimeStep=Input->CurrentTimeStep;
    Input->GetOutput(grid_copy);
    QComboBox::disconnect(inputHandler->ComboBoxes[0],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputVelocity(QString)));
    QComboBox::disconnect(inputHandler->ComboBoxes[1],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputPresure(QString)));

    std::string prev_velocity_name = inputHandler->GetSelectedFieldName(0);
    std::string prev_presure_name ="";
    if(UsePresureField)
    {
        prev_presure_name = inputHandler->GetSelectedFieldName(1);
    }
    inputHandler->RemoveItemsFromComboBox();
    inputHandler->SetInputTimeStep(CurrentTimeStep);

    inputHandler->UpdateInputHandler(Input->Field_list);

    inputHandler->AddItemsToComboBox(0,Vector);
    inputHandler->AddItemsToComboBox(1,Scalar);
    if(inputHandler->GetScalarFieldsNames().size()==0)
    {
        //NO hay campos scalares-->no hay campo de presiones.
        UsePresureField=false;
    }
    else
    {
        UsePresureField=true;
    }


    if(!inputHandler->SetSelectedInput(0,prev_velocity_name))
    {
        inputHandler->SetSelectedInput(0,inputHandler->GetVectorsFieldsNames()[0]);
        inputHandler->SetComboBoxIndex(0,inputHandler->GetVectorsFieldsNames()[0]);
    }
    else
    {
        inputHandler->SetComboBoxIndex(0,prev_velocity_name);
    }

    if(UsePresureField)
    {
        if(!inputHandler->SetSelectedInput(1,prev_presure_name))
        {
            inputHandler->SetSelectedInput(1,inputHandler->GetScalarFieldsNames()[0]);
            inputHandler->SetComboBoxIndex(1,inputHandler->GetScalarFieldsNames()[0]);
        }
        else
        {
            inputHandler->SetComboBoxIndex(1,prev_presure_name);
        }
    }

    OuterSurface->SetInputData(grid_copy);
    OuterSurface->Update();

    SurfaceNormals->Update();

    PolyDataOutput->CopyStructure(OuterSurface->GetOutput());

    CalculateForcesField();

    QComboBox::connect(inputHandler->ComboBoxes[0],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputVelocity(QString)));
    QComboBox::connect(inputHandler->ComboBoxes[1],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputPresure(QString)));

    SendSignalsDownStream();
    mute.unlock();
}

void Viscous_Forces::ChangeInputVelocity(QString name)
{
    inputHandler->SetSelectedInput(0,name.toStdString());
    Update();
}

void Viscous_Forces::ChangeInputPresure(QString name)
{
    inputHandler->SetSelectedInput(1,name.toStdString());
    Update();

}

//----------------------- End of Necessary Functions -----------------------------
//--------------------------- Internal functions ---------------------------------

void Viscous_Forces::CalculateForcesField()
{
    //El input se guarda en grid_copy. El output va a Grid.

//    if(calculated_tensor!= NULL)
//        delete  calculated_tensor;

    calculated_tensor = new Tensor(grid_copy,inputHandler->GetSelectedSolution(0));


    vtkSmartPointer<vtkDoubleArray> Forces = vtkSmartPointer<vtkDoubleArray>::New();
    Forces->SetNumberOfComponents(3);
    Forces->SetName("Viscous Forces");
    int i;

    vtkDataArray* Normals =SurfaceNormals->GetOutput()->GetPointData()->GetNormals();

    for(i=0;i<OuterSurface->GetOutput()->GetNumberOfPoints();i++)
    {
        double *Normal;
        Normal = Normals->GetTuple(i);
        int point_id = grid_copy->FindPoint(OuterSurface->GetOutput()->GetPoint(i));
        double result[calculated_tensor->Values[point_id].size()];

        TensorProyectionOverNormal(Normal,calculated_tensor->Values[point_id],result);
        Forces->InsertNextTuple(result);
    }

    for(unsigned long j; j< Field_list.size();j++)
    {
        delete Field_list[j];
    }
    Field_list.clear();
    Field_list.push_back(new Field(Forces));

    AddFields(Field_list.size()-1);

    if(UsePresureField)
    {
        vtkSmartPointer<vtkDoubleArray> Presure_Forces = vtkSmartPointer<vtkDoubleArray>::New();
        Presure_Forces->SetNumberOfComponents(3);
        Presure_Forces->SetName("Presure Forces");

        vtkSmartPointer<vtkDoubleArray> PresureField = inputHandler->GetSelectedSolution(1);

        vtkDataArray* Normals =SurfaceNormals->GetOutput()->GetPointData()->GetNormals();

        for(i=0;i<OuterSurface->GetOutput()->GetNumberOfPoints();i++)
        {
            double *Normal;
            Normal = Normals->GetTuple(i);
            double result[3];
            int point_id = grid_copy->FindPoint(OuterSurface->GetOutput()->GetPoint(i));
            ScalarDotNormal(Normal,PresureField->GetValue(point_id)-referencePresure,result);
            Presure_Forces->InsertNextTuple(result);
        }

        Field_list.push_back(new Field(Presure_Forces));
        AddFields(Field_list.size()-1);

        vtkSmartPointer<vtkDoubleArray> Total_Forces = vtkSmartPointer<vtkDoubleArray>::New();
        Total_Forces->SetNumberOfComponents(3);
        Total_Forces->SetName("Net Forces");

        for(i=0;i<OuterSurface->GetOutput()->GetNumberOfPoints();i++)
        {
            double aux[3];
            double *presure_force = Presure_Forces->GetTuple(i);
            double *visco_force = Forces->GetTuple(i);
            for(int j=0;j<3;j++)
            {
                aux[j] = presure_force[j]+visco_force[j];
            }
            Total_Forces->InsertNextTuple(aux);
        }

        Field_list.push_back(new Field(Total_Forces));
        AddFields(Field_list.size()-1);
    }

    SetVectors();
}

//La proyección del vector es basicamente la fuerza en esa celda, salvo por una constante nu.
void Viscous_Forces::TensorProyectionOverNormal(double *Normal, std::vector<std::vector<double> > CellTensor,double *result)
{
    unsigned long i,j;
    for(i=0;i<CellTensor.size();i++)
    {
        double aux =0;
        for(j=0;j<CellTensor[i].size();j++)
        {
            aux+= -(CellTensor[i][j]+CellTensor[j][i])*Normal[j]*nu; //El signo menos es porque la normal se define como positiva hacia afuera
        }
        result[i]=aux;
    }
}

void Viscous_Forces::ScalarDotNormal(double *Normal, double Scalar, double *result)
{
    for(int i=0;i<3;i++)
    {
        result[i]=Scalar*Normal[i];
    }

}

void Viscous_Forces::FilterPropertiesConfig()
{
    Parameters_Box = new QGroupBox;
    Parameters_Box->setTitle("Parameters");
    Parameters_Box->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    Parameters_Box->setMinimumHeight(80);
    Parameters_Box->setMaximumHeight(100);
    Parameters_Layout = new QFormLayout(Parameters_Box);
    Nu_LineEdit = new QLineEdit(Parameters_Box);
    Nu_LineEdit->setValidator(new QDoubleValidator(0,100000000,6,Nu_LineEdit));
    Presure_LineEdit = new QLineEdit(Parameters_Box);
    Presure_LineEdit->setValidator(new QDoubleValidator(0,100000000,6,Presure_LineEdit));
    Nu_LineEdit->setText("0.01");
    Presure_LineEdit->setText("0");

    Parameters_Layout->addRow("Dinamic Viscosity",Nu_LineEdit);
    Parameters_Layout->addRow("Presure Reference",Presure_LineEdit);

    QVBoxLayout *properties = new QVBoxLayout;
    inputHandler->AddQBoxToLayout(properties);
    properties->addWidget(Parameters_Box);

    AddFilterProperties(properties);
    Properties_names.push_back("Filter Prop.");
}
//------------------------- End Internal functions -------------------------------

void Viscous_Forces::ChangeNu()
{
    if(Nu_LineEdit->text().isEmpty())
        return;
    nu = std::stod(Nu_LineEdit->text().toStdString());
    Update();

}

void Viscous_Forces::ChangeReferencePresure()
{
    if(Presure_LineEdit->text().isEmpty())
        return;
    referencePresure = std::stod(Presure_LineEdit->text().toStdString());
    Update();
}

void Viscous_Forces::ReScaleToCurrentData()
{
    if(Field_list[SelectedField]->type ==Solid)
        return;
    if(Field_list[SelectedField]->selectedComponent==-1)
    {
        Mapper->SetScalarRange(Field_list[SelectedField]->Solution_Magnitud[0]->GetRange());
    }
    else
    {
        int selec = Field_list[SelectedField]->selectedComponent;
        Mapper->SetScalarRange(Field_list[SelectedField]->Solution_components[0][selec]->GetRange());
    }
    Mapper->Update();
}

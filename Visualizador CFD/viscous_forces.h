#ifndef VISCOUS_FORCES_H
#define VISCOUS_FORCES_H

#include "geometry.h"
#include "tensor.h"
#include "input_handler.h"

#include <vtkGeometryFilter.h>

#include <vtkCellData.h>
#include <vtkPolyDataNormals.h>

#include <QFormLayout>
#include <QGroupBox>
#include <QLineEdit>

//Calcula las fuerzas viscosas sobre la superficie de la malla.
//La salida del filtro es la superficie con el campo vectorial de las fuerzas.
class Viscous_Forces : public Geometry
{
    Q_OBJECT
public:
    //Constructors
    Viscous_Forces();
    //Destructors
    ~Viscous_Forces();
    /**
     * @brief Sirve como input para la clase
     * @param Source = Input
     */
    void SetInputData(Geometry *Source);
    /**
     * @brief Change the field that is visualizate.
     * @param field_name
     */
    void ChangeField(std::string Field_name) override;
    /**
     * @brief Cambia el arbol de geometrias a este paso de tiempo
     * @param i = paso de tiempo
     */
    void ChangeToThisTime(int i) override;
    /**
     * @brief Actualiza el filtro a nivel objeto.
     */
    void Update() override;
    /**
     * @brief Crea una copia de la geometria dentro de GridData
     * @param PolyData = Contenedor donde se va a copiar
     */
    void GetOutput(vtkSmartPointer<vtkPolyData> PolyData) override;
    /**
     * @brief Crea una copia de la geometria dentro de GridData
     * @param GridData
     */
    void GetOutput(vtkSmartPointer<vtkUnstructuredGrid> GridData) override;
    /**
     * @brief GetGeoDataOutput
     * @return vtkDataSet with the output
     */
    vtkDataSet* GetGeoDataOutput() override;
    /**
     * @brief UpstreamChange. Se usa en caso ed que el input del filtro cambie. Usualmente se llama mediante "SendSignalsDownStream"
     */
    void UpstreamChange() override;

    /**
     * @brief vtkPolyData for saving the the output.
     */
    vtkSmartPointer<vtkPolyData> PolyDataOutput;
    /**
     * @brief Re escalea la solución al campo actual.
     */
    void ReScaleToCurrentData() override;
    /**
     * @brief Set vector propities. Internal function override from geometry class. The override is for using PolyDataOutput instead of Grid.
     */
    void SetVectors() override;
private:

    //Interfase variables
    QGroupBox *Parameters_Box;
    QFormLayout *Parameters_Layout;
    QLineEdit *Nu_LineEdit, *Presure_LineEdit;
    /**
     * @brief Set scalar properties.Internal function override from geometry class. The override is for using PolyDataOutput instead of Grid.
     */
    void SetScalar() override;

    /**
     * @brief AddFields. Internal function override from geometry class. The override is for using PolyDataOutput instead of Grid.
     */
    void AddFields(int Field_number) override;
    /**
     * @brief Calculate el campo de fuerzas
     */
    void CalculateForcesField();
    /**
     * @brief TensorProyectionOverNormal. Funcion interna de CalculateForcesField
     * @param Normal
     * @param CellTensor
     * @param result
     */
    void TensorProyectionOverNormal(double *Normal,std::vector<std::vector<double>> CellTensor,double *result);
    /**
     * @brief ScalarDotNormal. Funcion interna de CalculateForcesField
     * @param Normal
     * @param Scalar
     * @param result
     */
    void ScalarDotNormal(double *Normal, double Scalar,double *result);
    /**
     * @brief Crea la pestaña de propiedades del filtro
     */
    void FilterPropertiesConfig();
    //Necessary variables.
    /**
     * @brief Dinamic viscosity. Needed to calculate the force
     */
    double nu;
    /**
     * @brief referencePresure. The force on the surfce is calculate as P - Pref
     */
    double referencePresure;
    /**
     * @brief velocity tensor at this time step. For all points
     */
    Tensor *calculated_tensor;
    /**
     * @brief VtkFilter for extrating the outer surface.
     */
    vtkSmartPointer<vtkGeometryFilter> OuterSurface;
    /**
     * @brief vtkFilter for getting the normals.
     */
    vtkSmartPointer<vtkPolyDataNormals> SurfaceNormals;
    /**
     * @brief Clase para manejar los distintos inputs.
     */
    Input_handler *inputHandler;
    /**
     * @brief Flag para calcular o no con un campo de presiones
     */
    bool UsePresureField;
private slots:
    /**
     * @brief Change Input Velocity field
     * @param name
     */
    void ChangeInputVelocity(QString name);
    /**
     * @brief Change Input Presure field
     * @param name
     */
    void ChangeInputPresure(QString name);
    /**
     * @brief Change Reference Presure
     */
    void ChangeReferencePresure();
    /**
     * @brief Change dinamic viscosity.
     */
    void ChangeNu();
};

#endif // VISCOUS_FORCES_H

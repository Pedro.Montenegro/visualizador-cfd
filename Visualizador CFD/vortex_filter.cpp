#include "vortex_filter.h"

#include <ctime>
Vortex_Filter::Vortex_Filter()
{
    this->grid_copy = vtkSmartPointer<vtkUnstructuredGrid>::New();
    this->type = Filter_type::VortexFinder;
    cutter =vtkSmartPointer<vtkContourFilter>::New();
    SolutionArray = vtkSmartPointer<vtkDoubleArray>::New();
    lambda_two = true;
    PolyData = vtkSmartPointer<vtkPolyData>::New();
    InputHandler = new Input_handler(1);
    FilterPropertiesConfig();
}


Vortex_Filter::~Vortex_Filter()
{
    mute.lock();
    unsigned long i;
    for( i= 0 ; i< Input->NextFilter.size();i++)
    {
        if(Input->NextFilter[i]->Name == this->Name)
        {
            break;
        }
    }
    Input->NextFilter.erase(Input->NextFilter.begin()+i);
    delete calculated_tensor;
    delete Eigen_values_box;
    delete Method_Box;
    delete Lambda_Two_box;
    delete InputHandler;
    for(unsigned long i=0;i<Field_list.size();i++)
    {
        delete Field_list[i];
    }
    Field_list.clear();
    mute.unlock();

}

void Vortex_Filter::SetInputData(Geometry *Source)
{
    mute.lock();
    this->Times->ShallowCopy(Source->Times);

    Input = Source;
    Input->GetOutput(grid_copy);

    this->render_Win = Source->render_Win;
    this->Name = Source->Name + "/VortexFinder";
    this->SelectedField = Source->SelectedField;
    this->CurrentTimeStep = Source->CurrentTimeStep;

    InputHandler->AddAllFields(Source->Field_list,Vector);
    InputHandler->SetInputName(0,"Velocity");
    InputHandler->AddItemsToComboBox(0,Vector);
    InputHandler->SetInputTimeStep(Source->CurrentTimeStep);
    InputHandler->SetSelectedInput(0);

    std::string prev_field_name = Source->Field_list[Source->SelectedField]->Field_Name;

    InputHandler->SetSelectedInput(prev_field_name);
    InputHandler->SetComboBoxIndex(0,InputHandler->GetSelectedFieldName());

    InputHandler->ComboBoxes[0]->setCurrentIndex(InputHandler->SelectedInput[0]);


    vtkSmartPointer<vtkDataArray> array = vtkSmartPointer<vtkDataArray>(grid_copy->GetPointData()->GetArray(InputHandler->GetSelectedFieldName().c_str()));
    SolutionArray->SetNumberOfComponents(array->GetNumberOfComponents());
    //SolutionArray->SetNumberOfTuples(array->GetNumberOfTuples());

    for(auto j=0;j<array->GetNumberOfTuples();j++)
    {
        SolutionArray->InsertNextTuple(array->GetTuple(j));
    }

    Grid->ShallowCopy(grid_copy);
    calculated_tensor = new Tensor(grid_copy,SolutionArray);

    min_ratio_eigen_val = -1/sqrt(3);
    min_ratio_LineEdit->setText(QString::fromStdString(std::to_string( min_ratio_eigen_val)));
    max_ratio_eigen_val = 1/sqrt(3);
    max_ratio_LineEdit->setText(QString::fromStdString(std::to_string( max_ratio_eigen_val)));
    abs_value_eigen_val = 0.0001; //VALOR sin mucha justificacion. Solo tiene que ser mayor que cero
    abs_value_LineEdit->setText(QString::fromStdString(std::to_string( abs_value_eigen_val)));
    Lambda2 = -1; //Puede cambiar segun el vortice.
    Lambda_Two_LineEdit->setText(QString::fromStdString(std::to_string(Lambda2)));

    Calculate_Vortex_core();


    cutter->SetInputData(Grid);
    cutter->ComputeScalarsOn();
    cutter->ComputeNormalsOff();
    cutter->GenerateValues(1,abs_value_eigen_val,abs_value_eigen_val*1.000001);
    cutter->Update();

    PolyData->ShallowCopy(cutter->GetOutput());

    Mapper->SetInputData(PolyData);
    Actor->SetMapper(Mapper);
    Mapper->ScalarVisibilityOn();
    Mapper->Update();

    QLineEdit::connect(min_ratio_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdateMinRatio()));
    QLineEdit::connect(max_ratio_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdateMaxRatio()));
    QLineEdit::connect(abs_value_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdateAbsValue()));
    QLineEdit::connect(Lambda_Two_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdateLambdaTwoValue()));
    QSlider::connect(Lambda_Two_Slider,SIGNAL(sliderReleased()),this,SLOT(UpdateLineEditBySlider()));
    QLineEdit::connect(Lambda_Two_LineEdit,SIGNAL(editingFinished()),this,SLOT(UpdateSliderByLineEdit()));
    QRadioButton::connect(Lambda_Two_Method,SIGNAL(released()),this,SLOT(ChangeMethod()));
    QRadioButton::connect(Swirling_Method,SIGNAL(released()),this,SLOT(ChangeMethod()));
    QComboBox::connect(InputHandler->ComboBoxes[0],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputField(QString)));

    Lambda2Range =Grid->GetPointData()->GetArray(0)->GetRange();
    Lambda_Two_Slider->setRange(0,100000);
    Min_Slider_Value->setText(QString::fromStdString(std::to_string(Lambda2Range[0])));
    Max_Slider_Value->setText(QString::fromStdString(std::to_string(0)));
    Lambda_Two_Slider->valueChanged(1);
    Source->NextFilter.push_back(this);

    mute.unlock();
}
//Es necesario corregir el tipo de celda en OpenFoam para que sea valido en casos mas generales
void Vortex_Filter::Calculate_Vortex_core()
{
    std::vector<std::complex<double>> Autovalores;
    unsigned long number_of_points = grid_copy->GetNumberOfPoints();
    unsigned long i,j;

    vtkSmartPointer<vtkIdList> lst=vtkSmartPointer<vtkIdList>::New() ;
    double a;
    vtkSmartPointer<vtkDoubleArray> lambdaField = vtkSmartPointer<vtkDoubleArray>::New();
    lambdaField->SetNumberOfComponents(1);
    //lambdaField->Resize(grid_copy->GetNumberOfPoints());
    lambdaField->SetName("Field");
    if(lambda_two)
    {
        calculated_tensor->CalculateAllSimAsimEigenVals();


        for(i=0;i<calculated_tensor->AutovaloresSimAsim.size();i++)
        {

            lambdaField->InsertNextValue(calculated_tensor->AutovaloresSimAsim[i][1].real());
            //lambdaField->InsertNextValue(current_tensor->AutovaloresSimAsim[i][1].real());
        }
        Lambda2Range = lambdaField->GetRange();
    }
    else
    {
        calculated_tensor->CalculateAllNormalEigenVals();
        for (i = 0; i < number_of_points; i++)
        {
            Autovalores = calculated_tensor->AutovaloresTensor[i];
            a=0;
            for(j=0;j<Autovalores.size();j++)
            {
                if(Autovalores[j].imag()!=0)
                {
                    if((Autovalores[j].real()/std::abs(Autovalores[j].imag()))>=min_ratio_eigen_val &&
                            (Autovalores[j].real()/std::abs(Autovalores[j].imag()))<=max_ratio_eigen_val) //&&
                            //std::abs(Autovalores[j].imag()) >= abs_value_eigen_val &&
                            //std::abs(Autovalores[j].imag()) <= max_abs_value_eigen_val)
                    {
                        a=1;
                        break;
                    }
                }
            }

            if(a==1)
            {
                lambdaField->InsertNextValue(abs(Autovalores[j].imag()));
            }
            else
            {
                lambdaField->InsertNextValue(-1000);
            }
        }

    }

    Grid->GetPointData()->AddArray(lambdaField);

    Grid->GetPointData()->SetActiveScalars(lambdaField->GetName());

    Min_Slider_Value->setText(QString::fromStdString(std::to_string(Lambda2Range[0])));

}
void Vortex_Filter::ChangeField(std::string field_name)
{
    InputHandler->SetSelectedInput(field_name);
    delete calculated_tensor;
    unsigned long j;
    vtkDataArray *array = grid_copy->GetPointData()->GetArray(InputHandler->GetSelectedFieldName().c_str());
    SolutionArray = vtkSmartPointer<vtkDoubleArray>::New();
    SolutionArray->SetNumberOfComponents(array->GetNumberOfComponents());
    for(j=0;(int)j<array->GetNumberOfTuples();j++)
    {
        SolutionArray->InsertNextTuple(array->GetTuple(j));
    }
    calculated_tensor = new Tensor(grid_copy,SolutionArray);
    Update();
}
void Vortex_Filter::ChangeToThisTime(int i)
{
    Input->ChangeToThisTime(i);
}

void Vortex_Filter::FilterPropertiesConfig()
{
    Method_Box = new QGroupBox;
    Method_Box->setTitle("Método");
    Method_Box->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    Method_Layout = new QVBoxLayout(Method_Box);
    Lambda_Two_Method = new QRadioButton(Method_Box);
    Lambda_Two_Method->setText(QString::fromUtf8("\u03BB") + "2");
    Swirling_Method = new QRadioButton(Method_Box);
    Swirling_Method->setText("Enhanced Swirling");

    Lambda_Two_Method->setChecked(true); //Lambda 2 por defecto

    Method_Layout->addWidget(Lambda_Two_Method);
    Method_Layout->addWidget(Swirling_Method);
    Method_Box->setLayout(Method_Layout);

    Eigen_values_box = new QGroupBox;
    Eigen_values_box->setTitle("Parametros del filtro");
    Eigen_values_box->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    Eigen_values_box->setMaximumHeight(150);
    Eigen_layout = new QGridLayout(Eigen_values_box);
    min_ratio_label = new QLabel(Eigen_values_box);
    min_ratio_label->setText("min value");
    max_ratio_label = new QLabel(Eigen_values_box);
    max_ratio_label->setText("max value");
    abs_value_label = new QLabel(Eigen_values_box);
    abs_value_label->setText("abs value");
    min_ratio_LineEdit = new QLineEdit(Eigen_values_box);
    min_ratio_LineEdit->setValidator(new QDoubleValidator(-1000000,1000000,8,Eigen_values_box));
    max_ratio_LineEdit = new QLineEdit(Eigen_values_box);
    max_ratio_LineEdit->setValidator(new QDoubleValidator(-1000000,1000000,8,Eigen_values_box));
    abs_value_LineEdit = new QLineEdit(Eigen_values_box);
    abs_value_LineEdit->setValidator(new QDoubleValidator(0,1000000,8,Eigen_values_box));

    Eigen_layout->addWidget(min_ratio_label,0,0);
    Eigen_layout->addWidget(min_ratio_LineEdit,0,1);
    Eigen_layout->addWidget(max_ratio_label,1,0);
    Eigen_layout->addWidget(max_ratio_LineEdit,1,1);
    Eigen_layout->addWidget(abs_value_label,2,0);
    Eigen_layout->addWidget(abs_value_LineEdit,2,1);

    Eigen_values_box->setLayout(Eigen_layout);

    Lambda_Two_box = new QGroupBox;
    Lambda_Two_box->setTitle("Parametros del filtro");
    Lambda_Two_box->setStyleSheet("QGroupBox { font-weight: bold; text-decoration: underline;} ");
    Lambda_Two_layout = new QGridLayout(Lambda_Two_box);
    Lambda_Two_Label = new QLabel(Lambda_Two_box);
    Lambda_Two_Label->setText(QString::fromUtf8("\u03BB") + "value");
    Lambda_Two_LineEdit = new QLineEdit(Lambda_Two_box);
    Lambda_Two_LineEdit->setValidator(new QDoubleValidator(-10000000000,0,10,Lambda_Two_LineEdit));
    Lambda_Two_Slider = new QSlider(Lambda_Two_box);
    Lambda_Two_Slider->setOrientation(Qt::Orientation::Horizontal);
    Min_Slider_Value = new QLabel(Lambda_Two_box);
    Max_Slider_Value = new QLabel(Lambda_Two_box);
    QLabel *RangeLabel = new QLabel(Lambda_Two_box);
    RangeLabel->setText("Range:");

    Lambda_Two_layout->addWidget(Lambda_Two_Label,0,0,1,1);
    Lambda_Two_layout->addWidget(Lambda_Two_LineEdit,0,2,1,3);
    Lambda_Two_layout->addWidget(RangeLabel,2,0,1,1);
    Lambda_Two_layout->addWidget(Min_Slider_Value,2,1,1,3,Qt::AlignmentFlag::AlignRight);
    Lambda_Two_layout->addWidget(Max_Slider_Value,2,4,1,1,Qt::AlignmentFlag::AlignRight);
    Lambda_Two_layout->addWidget(Lambda_Two_Slider,5,0,5,5);


    QVBoxLayout *Filter_prop = new QVBoxLayout;
    InputHandler->AddQBoxToLayout(Filter_prop);
    Filter_prop->setAlignment(Qt::AlignmentFlag::AlignTop);
    Filter_prop->addWidget(Method_Box);
    Filter_prop->addWidget(Lambda_Two_box);
    Filter_prop->addWidget(Eigen_values_box);
    Eigen_values_box->hide();
    Properties_names.push_back("Filter prop.");
    AddFilterProperties(Filter_prop);

}

void Vortex_Filter::UpdateMinRatio()
{
    if(min_ratio_LineEdit->text().isEmpty())
        return;
    min_ratio_eigen_val = std::stod(min_ratio_LineEdit->text().toStdString());
    Update();
}
void Vortex_Filter::UpdateMaxRatio()
{
    if(max_ratio_LineEdit->text().isEmpty())
        return;
    max_ratio_eigen_val = std::stod(max_ratio_LineEdit->text().toStdString());
    Update();

}
void Vortex_Filter::UpdateAbsValue()
{
    if(abs_value_LineEdit->text().isEmpty())
        return;
    abs_value_eigen_val = std::stod(abs_value_LineEdit->text().toStdString());
    Update();

}

void Vortex_Filter::UpdateLambdaTwoValue()
{
    if(Lambda_Two_LineEdit->text().isEmpty())
        return;
    Lambda2 = std::stod(Lambda_Two_LineEdit->text().toStdString());
    Update();
}

void Vortex_Filter::ChangeMethod()
{
    if(Lambda_Two_Method->isChecked())
    {
        lambda_two = true;
        Lambda_Two_box->show();
        Eigen_values_box->hide();
        //cutter->GenerateValues(1,Lambda2,Lambda2*1.000001);

    }
    else
    {
        lambda_two = false;
        Lambda_Two_box->hide();
        Eigen_values_box->show();
        //cutter->GenerateValues(1,abs_value_eigen_val,abs_value_eigen_val*1.000001);

    }
    Update();

}

void Vortex_Filter::UpdateLineEditBySlider()
{
    double SliderValue = Lambda_Two_Slider->value();
    double LineValue = Lambda2Range[0]*(1-SliderValue/100000);
    Lambda_Two_LineEdit->setText(QString::fromStdString(std::to_string(LineValue)));
    Lambda2 = LineValue;
    Update();
}
void Vortex_Filter::UpdateSliderByLineEdit()
{
    if(Lambda_Two_LineEdit->text().isEmpty())
        return;
    Lambda2 = std::stod(Lambda_Two_LineEdit->text().toStdString());
    int val =100000*(1-Lambda2/Lambda2Range[0]);
    Lambda_Two_Slider->setValue(val);
    Update();
}

void Vortex_Filter::Update()
{
    mute.lock();
    Calculate_Vortex_core();
    if(lambda_two)
    {
        cutter->GenerateValues(1,Lambda2,Lambda2*1.000001);
    }
    else
    {
        cutter->GenerateValues(1,abs_value_eigen_val,abs_value_eigen_val*1.000001);
    }
    cutter->Update();
    PolyData->ShallowCopy(cutter->GetOutput());
    Mapper->Update();
    SendSignalsDownStream();
    mute.unlock();
}


void Vortex_Filter::GetOutput(vtkSmartPointer<vtkUnstructuredGrid> GridData)
{
    mute.lock();
    vtkSmartPointer<vtkAppendFilter> toUGrid = vtkSmartPointer<vtkAppendFilter>::New();
    toUGrid->SetInputData(cutter->GetOutput());
    toUGrid->Update();
    GridData->ShallowCopy(toUGrid->GetOutput());
    mute.unlock();
}
void Vortex_Filter::GetOutput(vtkSmartPointer<vtkPolyData> polyData)
{
    mute.lock();
    polyData->ShallowCopy(cutter->GetOutput());
    mute.unlock();
}
void Vortex_Filter::UpstreamChange()//Borro los campos calculados porque cambio el input
{
    mute.lock();
    CurrentTimeStep=Input->CurrentTimeStep;
    delete calculated_tensor;

    QComboBox::disconnect(InputHandler->ComboBoxes[0],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputField(QString)));

    std::string prev_velocity_name = InputHandler->GetSelectedFieldName(0);

    InputHandler->RemoveItemsFromComboBox();
    InputHandler->UpdateInputHandler(Input->Field_list);
    InputHandler->AddItemsToComboBox(0,Vector);

    if(!InputHandler->SetSelectedInput(0,prev_velocity_name))
    {
        InputHandler->SetSelectedInput(0,InputHandler->GetVectorsFieldsNames()[0]);
        InputHandler->SetComboBoxIndex(0,InputHandler->GetVectorsFieldsNames()[0]);
    }
    else
    {
        InputHandler->SetComboBoxIndex(0,prev_velocity_name);
    }

    CurrentTimeStep = Input->CurrentTimeStep;

    grid_copy = vtkSmartPointer<vtkUnstructuredGrid>::New();
    Input->GetOutput(grid_copy);

    unsigned long j;
    vtkDataArray *array = grid_copy->GetPointData()->GetArray(InputHandler->GetSelectedFieldName().c_str());
    SolutionArray = vtkSmartPointer<vtkDoubleArray>::New();
    SolutionArray->SetNumberOfComponents(array->GetNumberOfComponents());
    for(j=0;(int)j<array->GetNumberOfTuples();j++)
    {
        SolutionArray->InsertNextTuple(array->GetTuple(j));
    }

    calculated_tensor = new Tensor(grid_copy,SolutionArray);
    Grid->ShallowCopy(grid_copy);
    //Calculate_Vortex_core();

    //cutter->SetInputData(Grid);
    mute.unlock();

    Update();
    //render_Win->Render();

    QComboBox::connect(InputHandler->ComboBoxes[0],SIGNAL(currentIndexChanged(QString)),this,SLOT(ChangeInputField(QString)));

}

vtkDataSet* Vortex_Filter::GetGeoDataOutput()
{
    return cutter->GetOutput();
}

void Vortex_Filter::ChangeInputField(QString name)
{
    ChangeField(name.toStdString());
}

void Vortex_Filter::ReScaleToCurrentData()
{
    Mapper->SetScalarRange(Grid->GetPointData()->GetScalars()->GetRange());
    Mapper->Update();
}

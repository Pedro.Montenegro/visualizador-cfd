#ifndef VORTEX_FILTER_H
#define VORTEX_FILTER_H

#include "geometry.h"
#include "tensor.h"
#include "input_handler.h"

#include <vtkDataArray.h>
#include <vtkAlgorithmOutput.h>
#include <vtkPolyData.h>
#include <vtkGeometryFilter.h>
#include <vtkContourFilter.h>
#include <QRadioButton>
class Vortex_Filter : public Geometry
{
    Q_OBJECT
public:
    //Consctructors
    Vortex_Filter();
    //Destructor
    ~Vortex_Filter();
    //Necessary functions for the filter.
    /**
     * @brief Sirve como input para la clase
     * @param Source = Input
     */
    void SetInputData(Geometry *Source);
    /**
     * @brief Change the field that is used to calculate the vortex.
     * @param field_name
     */
    void ChangeField(std::string Field_name) override;
    /**
     * @brief Cambia el arbol de geometrias a este paso de tiempo
     * @param i = paso de tiempo
     */
    void ChangeToThisTime(int i) override;
    /**
     * @brief Actualiza el filtro a nivel objeto.
     */
    void Update() override;

    /**
     * @brief Copia la salida del filtro a GridData
     * @param GridData
     */
    void GetOutput(vtkSmartPointer<vtkPolyData> PolyData) override;
    void GetOutput(vtkSmartPointer<vtkUnstructuredGrid> GridData) override;
    /**
     * @brief GetGeoDataOutput
     * @return VtkDataSet con la salida del filtro.
     */
    vtkDataSet* GetGeoDataOutput() override;
    /**
     * @brief UpstreamChange. Se usa en caso de que el input del filtro cambie. Usualmente se llama mediante "SendSignalsDownStream"
     */
    void UpstreamChange() override;
    /**
     * @brief Re escalea la solución al campo actual.
     */
    void ReScaleToCurrentData() override;
private:

    //Filter properties configure
    /**
     * @brief Configura la pestaña de propiedades del filtro
     */
    void FilterPropertiesConfig();

    //Variables definidas para crear la pestaña de propiedades del filtro.
    QGroupBox *Eigen_values_box;
    QGridLayout *Eigen_layout;
    double min_ratio_eigen_val;
    QLabel *min_ratio_label;
    QLineEdit *min_ratio_LineEdit;
    double max_ratio_eigen_val;
    QLabel *max_ratio_label;
    QLineEdit *max_ratio_LineEdit;
    double abs_value_eigen_val;
    QLabel *abs_value_label;
    QLineEdit *abs_value_LineEdit;

    QGroupBox *Lambda_Two_box;
    QGridLayout *Lambda_Two_layout;
    QLabel *Lambda_Two_Label;
    QLineEdit *Lambda_Two_LineEdit;
    QSlider *Lambda_Two_Slider;
    QLabel *Min_Slider_Value;
    QLabel *Max_Slider_Value;
    double Lambda2;
    double *Lambda2Range;

    QGroupBox *Method_Box;
    QVBoxLayout *Method_Layout;
    QRadioButton *Lambda_Two_Method;
    QRadioButton *Swirling_Method;

    /**
     * @brief Calculate_Vortex_core
     */
    void Calculate_Vortex_core();
    /**
     * @brief Campo de autovalores
     */
    vtkSmartPointer<vtkDoubleArray> SolutionArray;
    /**
     * @brief velocity tensor for each point
     */
    Tensor *calculated_tensor;
    /**
     * @brief flag for selected method
     */
    bool lambda_two;
    /**
     * @brief countor filter used for drawing the vortex
     */
    vtkSmartPointer<vtkContourFilter> cutter;
    /**
     * @brief Clase para manejar los distintos inputs.
     */
    Input_handler *InputHandler;
private slots:
    /**
     * @brief UpdateMinRatio of eigenvalues -> used for swirlingh strengh method
     */
    void UpdateMinRatio();
    /**
     * @brief UpdateMaxRatio of eigenvalues-> used for swirlingh strengh method
     */
    void UpdateMaxRatio();
    /**
     * @brief UpdateAbsValue -> used for swirlingh strengh method
     */
    void UpdateAbsValue();
    /**
     * @brief UpdateLambdaTwoValue
     */
    void UpdateLambdaTwoValue();
    /**
     * @brief ChangeMethod
     */
    void ChangeMethod();
    /**
     * @brief actualiza la line edit luego de un cambio en el slider
     */
    void UpdateLineEditBySlider();
    /**
     * @brief actualiza el slider luego de un cambio en el lineedit
     */
    void UpdateSliderByLineEdit();
    /**
     * @brief cambia el campo con el que se calculan los vortices
     * @param name
     */
    void ChangeInputField(QString name);
};

#endif // VORTEX_FILTER_H
